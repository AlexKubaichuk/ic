﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'dsRegConst.pas' rev: 28.00 (Windows)

#ifndef DsregconstHPP
#define DsregconstHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dsregconst
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _icsErrorMsgCaption;
#define Dsregconst_icsErrorMsgCaption System::LoadResourceString(&Dsregconst::_icsErrorMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsErrorMsgCaptionSys;
#define Dsregconst_icsErrorMsgCaptionSys System::LoadResourceString(&Dsregconst::_icsErrorMsgCaptionSys)
extern DELPHI_PACKAGE System::ResourceString _icsMessageMsgCaption;
#define Dsregconst_icsMessageMsgCaption System::LoadResourceString(&Dsregconst::_icsMessageMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsInputErrorMsgCaption;
#define Dsregconst_icsInputErrorMsgCaption System::LoadResourceString(&Dsregconst::_icsInputErrorMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRecCount1;
#define Dsregconst_icsRecCount1 System::LoadResourceString(&Dsregconst::_icsRecCount1)
extern DELPHI_PACKAGE System::ResourceString _icsRecCount2;
#define Dsregconst_icsRecCount2 System::LoadResourceString(&Dsregconst::_icsRecCount2)
extern DELPHI_PACKAGE System::ResourceString _icsRecCount3;
#define Dsregconst_icsRecCount3 System::LoadResourceString(&Dsregconst::_icsRecCount3)
extern DELPHI_PACKAGE System::ResourceString _icsCommonRecCount;
#define Dsregconst_icsCommonRecCount System::LoadResourceString(&Dsregconst::_icsCommonRecCount)
extern DELPHI_PACKAGE System::ResourceString _icsFieldNotFound;
#define Dsregconst_icsFieldNotFound System::LoadResourceString(&Dsregconst::_icsFieldNotFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegListCreate;
#define Dsregconst_icsRegListCreate System::LoadResourceString(&Dsregconst::_icsRegListCreate)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorInsertRec;
#define Dsregconst_icsRegErrorInsertRec System::LoadResourceString(&Dsregconst::_icsRegErrorInsertRec)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorInsertRecSys;
#define Dsregconst_icsRegErrorInsertRecSys System::LoadResourceString(&Dsregconst::_icsRegErrorInsertRecSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorEditRec;
#define Dsregconst_icsRegErrorEditRec System::LoadResourceString(&Dsregconst::_icsRegErrorEditRec)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorEditRecSys;
#define Dsregconst_icsRegErrorEditRecSys System::LoadResourceString(&Dsregconst::_icsRegErrorEditRecSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorDeleteRec;
#define Dsregconst_icsRegErrorDeleteRec System::LoadResourceString(&Dsregconst::_icsRegErrorDeleteRec)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorDeleteRecSys;
#define Dsregconst_icsRegErrorDeleteRecSys System::LoadResourceString(&Dsregconst::_icsRegErrorDeleteRecSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorSearch;
#define Dsregconst_icsRegErrorSearch System::LoadResourceString(&Dsregconst::_icsRegErrorSearch)
extern DELPHI_PACKAGE System::ResourceString _icsRegRecFound;
#define Dsregconst_icsRegRecFound System::LoadResourceString(&Dsregconst::_icsRegRecFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorConfirmDeleteRec;
#define Dsregconst_icsRegErrorConfirmDeleteRec System::LoadResourceString(&Dsregconst::_icsRegErrorConfirmDeleteRec)
extern DELPHI_PACKAGE System::ResourceString _icsRegErrorConfirmDeleteRecMsgCaption;
#define Dsregconst_icsRegErrorConfirmDeleteRecMsgCaption System::LoadResourceString(&Dsregconst::_icsRegErrorConfirmDeleteRecMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorNoRegDefXML;
#define Dsregconst_icsRegTransportErrorNoRegDefXML System::LoadResourceString(&Dsregconst::_icsRegTransportErrorNoRegDefXML)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorNoEIDataProvider;
#define Dsregconst_icsRegTransportErrorNoEIDataProvider System::LoadResourceString(&Dsregconst::_icsRegTransportErrorNoEIDataProvider)
extern DELPHI_PACKAGE System::ResourceString _icsDTFormatsD;
#define Dsregconst_icsDTFormatsD System::LoadResourceString(&Dsregconst::_icsDTFormatsD)
extern DELPHI_PACKAGE System::ResourceString _icsDTFormatsDT;
#define Dsregconst_icsDTFormatsDT System::LoadResourceString(&Dsregconst::_icsDTFormatsDT)
extern DELPHI_PACKAGE System::ResourceString _icsDTFormatsT;
#define Dsregconst_icsDTFormatsT System::LoadResourceString(&Dsregconst::_icsDTFormatsT)
extern DELPHI_PACKAGE System::ResourceString _iscRegEDErrorNoOwner;
#define Dsregconst_iscRegEDErrorNoOwner System::LoadResourceString(&Dsregconst::_iscRegEDErrorNoOwner)
extern DELPHI_PACKAGE System::ResourceString _iscRegEDErrorNoRef;
#define Dsregconst_iscRegEDErrorNoRef System::LoadResourceString(&Dsregconst::_iscRegEDErrorNoRef)
extern DELPHI_PACKAGE System::ResourceString _iscRegEDErrorRefObject;
#define Dsregconst_iscRegEDErrorRefObject System::LoadResourceString(&Dsregconst::_iscRegEDErrorRefObject)
extern DELPHI_PACKAGE System::ResourceString _icsRegItemErrorReqField;
#define Dsregconst_icsRegItemErrorReqField System::LoadResourceString(&Dsregconst::_icsRegItemErrorReqField)
extern DELPHI_PACKAGE System::ResourceString _icsriBinaryCheckTxt;
#define Dsregconst_icsriBinaryCheckTxt System::LoadResourceString(&Dsregconst::_icsriBinaryCheckTxt)
extern DELPHI_PACKAGE System::ResourceString _icsriBinaryUnCheckTxt;
#define Dsregconst_icsriBinaryUnCheckTxt System::LoadResourceString(&Dsregconst::_icsriBinaryUnCheckTxt)
extern DELPHI_PACKAGE System::ResourceString _icsriChoiceTreeSubClass;
#define Dsregconst_icsriChoiceTreeSubClass System::LoadResourceString(&Dsregconst::_icsriChoiceTreeSubClass)
extern DELPHI_PACKAGE System::ResourceString _icsriDigitValRange;
#define Dsregconst_icsriDigitValRange System::LoadResourceString(&Dsregconst::_icsriDigitValRange)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorNoOwner;
#define Dsregconst_dsRegEDContainerErrorNoOwner System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorNoOwner)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorNoParent;
#define Dsregconst_dsRegEDContainerErrorNoParent System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorNoParent)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorNoTmpQuery;
#define Dsregconst_dsRegEDContainerErrorNoTmpQuery System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorNoTmpQuery)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorTmpQueryTypeADO;
#define Dsregconst_dsRegEDContainerErrorTmpQueryTypeADO System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorTmpQueryTypeADO)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorTmpQueryTypeFIB;
#define Dsregconst_dsRegEDContainerErrorTmpQueryTypeFIB System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorTmpQueryTypeFIB)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorNoDataSrc;
#define Dsregconst_dsRegEDContainerErrorNoDataSrc System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorNoDataSrc)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorNoDataDef;
#define Dsregconst_dsRegEDContainerErrorNoDataDef System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorNoDataDef)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorNoDataDefId;
#define Dsregconst_dsRegEDContainerErrorNoDataDefId System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorNoDataDefId)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorDSTypeADO;
#define Dsregconst_dsRegEDContainerErrorDSTypeADO System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorDSTypeADO)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorDSTypeFIB;
#define Dsregconst_dsRegEDContainerErrorDSTypeFIB System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorDSTypeFIB)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorEDNo;
#define Dsregconst_dsRegEDContainerErrorEDNo System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorEDNo)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorEDExists;
#define Dsregconst_dsRegEDContainerErrorEDExists System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorEDExists)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorEDCreate;
#define Dsregconst_dsRegEDContainerErrorEDCreate System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorEDCreate)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerSysErrorEDCreate;
#define Dsregconst_dsRegEDContainerSysErrorEDCreate System::LoadResourceString(&Dsregconst::_dsRegEDContainerSysErrorEDCreate)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorTmplNo;
#define Dsregconst_dsRegEDContainerErrorTmplNo System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorTmplNo)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorTmplExists;
#define Dsregconst_dsRegEDContainerErrorTmplExists System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorTmplExists)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorTmplCreate;
#define Dsregconst_dsRegEDContainerErrorTmplCreate System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorTmplCreate)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerSysErrorTmplCreate;
#define Dsregconst_dsRegEDContainerSysErrorTmplCreate System::LoadResourceString(&Dsregconst::_dsRegEDContainerSysErrorTmplCreate)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorLabNo;
#define Dsregconst_dsRegEDContainerErrorLabNo System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorLabNo)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorLabExists;
#define Dsregconst_dsRegEDContainerErrorLabExists System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorLabExists)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerErrorLabCreate;
#define Dsregconst_dsRegEDContainerErrorLabCreate System::LoadResourceString(&Dsregconst::_dsRegEDContainerErrorLabCreate)
extern DELPHI_PACKAGE System::ResourceString _dsRegEDContainerSysErrorLabCreate;
#define Dsregconst_dsRegEDContainerSysErrorLabCreate System::LoadResourceString(&Dsregconst::_dsRegEDContainerSysErrorLabCreate)
extern DELPHI_PACKAGE System::ResourceString _icsRegEDFuncErrorGetValues;
#define Dsregconst_icsRegEDFuncErrorGetValues System::LoadResourceString(&Dsregconst::_icsRegEDFuncErrorGetValues)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorClassesExport;
#define Dsregconst_icsRegEIErrorClassesExport System::LoadResourceString(&Dsregconst::_icsRegEIErrorClassesExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorClassExport;
#define Dsregconst_icsRegEIErrorClassExport System::LoadResourceString(&Dsregconst::_icsRegEIErrorClassExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorUnitsExport;
#define Dsregconst_icsRegEIErrorUnitsExport System::LoadResourceString(&Dsregconst::_icsRegEIErrorUnitsExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorUnitExport;
#define Dsregconst_icsRegEIErrorUnitExport System::LoadResourceString(&Dsregconst::_icsRegEIErrorUnitExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorClassesImport;
#define Dsregconst_icsRegEIErrorClassesImport System::LoadResourceString(&Dsregconst::_icsRegEIErrorClassesImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorClassImport;
#define Dsregconst_icsRegEIErrorClassImport System::LoadResourceString(&Dsregconst::_icsRegEIErrorClassImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorLastInqDate;
#define Dsregconst_icsRegEIErrorLastInqDate System::LoadResourceString(&Dsregconst::_icsRegEIErrorLastInqDate)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorUnitImport;
#define Dsregconst_icsRegEIErrorUnitImport System::LoadResourceString(&Dsregconst::_icsRegEIErrorUnitImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegEIErrorUnitsImport;
#define Dsregconst_icsRegEIErrorUnitsImport System::LoadResourceString(&Dsregconst::_icsRegEIErrorUnitsImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassesExport;
#define Dsregconst_icsRegTransportErrorClassesExport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassesExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassesExportSys;
#define Dsregconst_icsRegTransportErrorClassesExportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassesExportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassExport;
#define Dsregconst_icsRegTransportErrorClassExport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassExportSys;
#define Dsregconst_icsRegTransportErrorClassExportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassExportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitsExport;
#define Dsregconst_icsRegTransportErrorUnitsExport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitsExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitsExportSys;
#define Dsregconst_icsRegTransportErrorUnitsExportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitsExportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitExport;
#define Dsregconst_icsRegTransportErrorUnitExport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitExport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitExportSys;
#define Dsregconst_icsRegTransportErrorUnitExportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitExportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassesImport;
#define Dsregconst_icsRegTransportErrorClassesImport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassesImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassesImportSys;
#define Dsregconst_icsRegTransportErrorClassesImportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassesImportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassImport;
#define Dsregconst_icsRegTransportErrorClassImport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorClassImportSys;
#define Dsregconst_icsRegTransportErrorClassImportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorClassImportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorLastInqDate;
#define Dsregconst_icsRegTransportErrorLastInqDate System::LoadResourceString(&Dsregconst::_icsRegTransportErrorLastInqDate)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitsImport;
#define Dsregconst_icsRegTransportErrorUnitsImport System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitsImport)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitsImportSys;
#define Dsregconst_icsRegTransportErrorUnitsImportSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitsImportSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitsImportData;
#define Dsregconst_icsRegTransportErrorUnitsImportData System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitsImportData)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorUnitsImportDataSys;
#define Dsregconst_icsRegTransportErrorUnitsImportDataSys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorUnitsImportDataSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorGetClsKey;
#define Dsregconst_icsRegTransportErrorGetClsKey System::LoadResourceString(&Dsregconst::_icsRegTransportErrorGetClsKey)
extern DELPHI_PACKAGE System::ResourceString _icsRegTransportErrorGetClsKeySys;
#define Dsregconst_icsRegTransportErrorGetClsKeySys System::LoadResourceString(&Dsregconst::_icsRegTransportErrorGetClsKeySys)
extern DELPHI_PACKAGE System::ResourceString _icsRegXMLEIDataProviderRecNotFound;
#define Dsregconst_icsRegXMLEIDataProviderRecNotFound System::LoadResourceString(&Dsregconst::_icsRegXMLEIDataProviderRecNotFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegXMLEIDataProviderIEDataNotFound;
#define Dsregconst_icsRegXMLEIDataProviderIEDataNotFound System::LoadResourceString(&Dsregconst::_icsRegXMLEIDataProviderIEDataNotFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegXMLEIDataProviderErrorCreateSect;
#define Dsregconst_icsRegXMLEIDataProviderErrorCreateSect System::LoadResourceString(&Dsregconst::_icsRegXMLEIDataProviderErrorCreateSect)
extern DELPHI_PACKAGE System::ResourceString _icsRegXMLEIDataProviderErrorSectNotFound;
#define Dsregconst_icsRegXMLEIDataProviderErrorSectNotFound System::LoadResourceString(&Dsregconst::_icsRegXMLEIDataProviderErrorSectNotFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegFIBEIDataProviderErrorExecProcSys;
#define Dsregconst_icsRegFIBEIDataProviderErrorExecProcSys System::LoadResourceString(&Dsregconst::_icsRegFIBEIDataProviderErrorExecProcSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegFIBEIDataProviderErrorExecProc;
#define Dsregconst_icsRegFIBEIDataProviderErrorExecProc System::LoadResourceString(&Dsregconst::_icsRegFIBEIDataProviderErrorExecProc)
extern DELPHI_PACKAGE System::ResourceString _icsRegFIBEIDataProviderErrorExecSQLSys;
#define Dsregconst_icsRegFIBEIDataProviderErrorExecSQLSys System::LoadResourceString(&Dsregconst::_icsRegFIBEIDataProviderErrorExecSQLSys)
extern DELPHI_PACKAGE System::ResourceString _icsRegFIBEIDataProviderErrorExecSQL;
#define Dsregconst_icsRegFIBEIDataProviderErrorExecSQL System::LoadResourceString(&Dsregconst::_icsRegFIBEIDataProviderErrorExecSQL)
extern DELPHI_PACKAGE System::ResourceString _icsRegFIBEIDataProviderNoQueryDef;
#define Dsregconst_icsRegFIBEIDataProviderNoQueryDef System::LoadResourceString(&Dsregconst::_icsRegFIBEIDataProviderNoQueryDef)
extern DELPHI_PACKAGE System::ResourceString _icsRegFIBEIDataProviderErrorSectNotFound;
#define Dsregconst_icsRegFIBEIDataProviderErrorSectNotFound System::LoadResourceString(&Dsregconst::_icsRegFIBEIDataProviderErrorSectNotFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorDBNameNotDef;
#define Dsregconst_icsRegistryErrorDBNameNotDef System::LoadResourceString(&Dsregconst::_icsRegistryErrorDBNameNotDef)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorNoRegDef;
#define Dsregconst_icsRegistryErrorNoRegDef System::LoadResourceString(&Dsregconst::_icsRegistryErrorNoRegDef)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorDBConnect;
#define Dsregconst_icsRegistryErrorDBConnect System::LoadResourceString(&Dsregconst::_icsRegistryErrorDBConnect)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorCommDBErrorMsgCaption;
#define Dsregconst_icsRegistryErrorCommDBErrorMsgCaption System::LoadResourceString(&Dsregconst::_icsRegistryErrorCommDBErrorMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorDBErrorMsgCaption;
#define Dsregconst_icsRegistryErrorDBErrorMsgCaption System::LoadResourceString(&Dsregconst::_icsRegistryErrorDBErrorMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorDBNotExist;
#define Dsregconst_icsRegistryErrorDBNotExist System::LoadResourceString(&Dsregconst::_icsRegistryErrorDBNotExist)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorNoServer;
#define Dsregconst_icsRegistryErrorNoServer System::LoadResourceString(&Dsregconst::_icsRegistryErrorNoServer)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorLoadRegDef;
#define Dsregconst_icsRegistryErrorLoadRegDef System::LoadResourceString(&Dsregconst::_icsRegistryErrorLoadRegDef)
extern DELPHI_PACKAGE System::ResourceString _icsRegistryErrorLoadDM;
#define Dsregconst_icsRegistryErrorLoadDM System::LoadResourceString(&Dsregconst::_icsRegistryErrorLoadDM)
extern DELPHI_PACKAGE System::ResourceString _icsTemplateReqField;
#define Dsregconst_icsTemplateReqField System::LoadResourceString(&Dsregconst::_icsTemplateReqField)
extern DELPHI_PACKAGE System::ResourceString _icsTemplateNoClassDef;
#define Dsregconst_icsTemplateNoClassDef System::LoadResourceString(&Dsregconst::_icsTemplateNoClassDef)
extern DELPHI_PACKAGE System::ResourceString _icsTemplateNoFltDef;
#define Dsregconst_icsTemplateNoFltDef System::LoadResourceString(&Dsregconst::_icsTemplateNoFltDef)
extern DELPHI_PACKAGE System::ResourceString _icsTemplateRefError;
#define Dsregconst_icsTemplateRefError System::LoadResourceString(&Dsregconst::_icsTemplateRefError)
extern DELPHI_PACKAGE System::ResourceString _icsTemplateReqEmpty;
#define Dsregconst_icsTemplateReqEmpty System::LoadResourceString(&Dsregconst::_icsTemplateReqEmpty)
extern DELPHI_PACKAGE System::ResourceString _icsITemplateCaption;
#define Dsregconst_icsITemplateCaption System::LoadResourceString(&Dsregconst::_icsITemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsITemplateApplyBtn;
#define Dsregconst_icsITemplateApplyBtn System::LoadResourceString(&Dsregconst::_icsITemplateApplyBtn)
extern DELPHI_PACKAGE System::ResourceString _icsITemplateCancelBtn;
#define Dsregconst_icsITemplateCancelBtn System::LoadResourceString(&Dsregconst::_icsITemplateCancelBtn)
extern DELPHI_PACKAGE System::ResourceString _icsIErrTreeFormMsg;
#define Dsregconst_icsIErrTreeFormMsg System::LoadResourceString(&Dsregconst::_icsIErrTreeFormMsg)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchEndMsg;
#define Dsregconst_icsRegSearchEndMsg System::LoadResourceString(&Dsregconst::_icsRegSearchEndMsg)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchCaption;
#define Dsregconst_icsRegSearchCaption System::LoadResourceString(&Dsregconst::_icsRegSearchCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchSettingBtnCaption;
#define Dsregconst_icsRegSearchSettingBtnCaption System::LoadResourceString(&Dsregconst::_icsRegSearchSettingBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchFindBtnCaption;
#define Dsregconst_icsRegSearchFindBtnCaption System::LoadResourceString(&Dsregconst::_icsRegSearchFindBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchCancelBtnCaption;
#define Dsregconst_icsRegSearchCancelBtnCaption System::LoadResourceString(&Dsregconst::_icsRegSearchCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchNotFound;
#define Dsregconst_icsRegSearchNotFound System::LoadResourceString(&Dsregconst::_icsRegSearchNotFound)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchSettingCaption;
#define Dsregconst_icsRegSearchSettingCaption System::LoadResourceString(&Dsregconst::_icsRegSearchSettingCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchSettingFieldListLabCaption;
#define Dsregconst_icsRegSearchSettingFieldListLabCaption System::LoadResourceString(&Dsregconst::_icsRegSearchSettingFieldListLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchSettingSaveBtnCaption;
#define Dsregconst_icsRegSearchSettingSaveBtnCaption System::LoadResourceString(&Dsregconst::_icsRegSearchSettingSaveBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSearchSettingCancelBtnCaption;
#define Dsregconst_icsRegSearchSettingCancelBtnCaption System::LoadResourceString(&Dsregconst::_icsRegSearchSettingCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListSelectAllCaption;
#define Dsregconst_icsRegSelListSelectAllCaption System::LoadResourceString(&Dsregconst::_icsRegSelListSelectAllCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListSelectAllHint;
#define Dsregconst_icsRegSelListSelectAllHint System::LoadResourceString(&Dsregconst::_icsRegSelListSelectAllHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListSelectAllShortCut;
#define Dsregconst_icsRegSelListSelectAllShortCut System::LoadResourceString(&Dsregconst::_icsRegSelListSelectAllShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListUnSelectAllCaption;
#define Dsregconst_icsRegSelListUnSelectAllCaption System::LoadResourceString(&Dsregconst::_icsRegSelListUnSelectAllCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListUnSelectAllHint;
#define Dsregconst_icsRegSelListUnSelectAllHint System::LoadResourceString(&Dsregconst::_icsRegSelListUnSelectAllHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListUnSelectAllShortCut;
#define Dsregconst_icsRegSelListUnSelectAllShortCut System::LoadResourceString(&Dsregconst::_icsRegSelListUnSelectAllShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListApplyCaption;
#define Dsregconst_icsRegSelListApplyCaption System::LoadResourceString(&Dsregconst::_icsRegSelListApplyCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListApplyHint;
#define Dsregconst_icsRegSelListApplyHint System::LoadResourceString(&Dsregconst::_icsRegSelListApplyHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListApplyShortCut;
#define Dsregconst_icsRegSelListApplyShortCut System::LoadResourceString(&Dsregconst::_icsRegSelListApplyShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListSetTemplateCaption;
#define Dsregconst_icsRegSelListSetTemplateCaption System::LoadResourceString(&Dsregconst::_icsRegSelListSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListSetTemplateHint;
#define Dsregconst_icsRegSelListSetTemplateHint System::LoadResourceString(&Dsregconst::_icsRegSelListSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListSetTemplateShortCut;
#define Dsregconst_icsRegSelListSetTemplateShortCut System::LoadResourceString(&Dsregconst::_icsRegSelListSetTemplateShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListReSetTemplateCaption;
#define Dsregconst_icsRegSelListReSetTemplateCaption System::LoadResourceString(&Dsregconst::_icsRegSelListReSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListReSetTemplateHint;
#define Dsregconst_icsRegSelListReSetTemplateHint System::LoadResourceString(&Dsregconst::_icsRegSelListReSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegSelListReSetTemplateShortCut;
#define Dsregconst_icsRegSelListReSetTemplateShortCut System::LoadResourceString(&Dsregconst::_icsRegSelListReSetTemplateShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListInsertCaption;
#define Dsregconst_icsRegListInsertCaption System::LoadResourceString(&Dsregconst::_icsRegListInsertCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListInsertHint;
#define Dsregconst_icsRegListInsertHint System::LoadResourceString(&Dsregconst::_icsRegListInsertHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListInsertShortCut;
#define Dsregconst_icsRegListInsertShortCut System::LoadResourceString(&Dsregconst::_icsRegListInsertShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListEditCaption;
#define Dsregconst_icsRegListEditCaption System::LoadResourceString(&Dsregconst::_icsRegListEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListEditHint;
#define Dsregconst_icsRegListEditHint System::LoadResourceString(&Dsregconst::_icsRegListEditHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListEditShortCut;
#define Dsregconst_icsRegListEditShortCut System::LoadResourceString(&Dsregconst::_icsRegListEditShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListDeleteCaption;
#define Dsregconst_icsRegListDeleteCaption System::LoadResourceString(&Dsregconst::_icsRegListDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListDeleteHint;
#define Dsregconst_icsRegListDeleteHint System::LoadResourceString(&Dsregconst::_icsRegListDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListDeleteShortCut;
#define Dsregconst_icsRegListDeleteShortCut System::LoadResourceString(&Dsregconst::_icsRegListDeleteShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListSetTemplateCaption;
#define Dsregconst_icsRegListSetTemplateCaption System::LoadResourceString(&Dsregconst::_icsRegListSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListSetTemplateHint;
#define Dsregconst_icsRegListSetTemplateHint System::LoadResourceString(&Dsregconst::_icsRegListSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListSetTemplateShortCut;
#define Dsregconst_icsRegListSetTemplateShortCut System::LoadResourceString(&Dsregconst::_icsRegListSetTemplateShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListReSetTemplateCaption;
#define Dsregconst_icsRegListReSetTemplateCaption System::LoadResourceString(&Dsregconst::_icsRegListReSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListReSetTemplateHint;
#define Dsregconst_icsRegListReSetTemplateHint System::LoadResourceString(&Dsregconst::_icsRegListReSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListReSetTemplateShortCut;
#define Dsregconst_icsRegListReSetTemplateShortCut System::LoadResourceString(&Dsregconst::_icsRegListReSetTemplateShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListOpenCaption;
#define Dsregconst_icsRegListOpenCaption System::LoadResourceString(&Dsregconst::_icsRegListOpenCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListOpenHint;
#define Dsregconst_icsRegListOpenHint System::LoadResourceString(&Dsregconst::_icsRegListOpenHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListOpenShortCut;
#define Dsregconst_icsRegListOpenShortCut System::LoadResourceString(&Dsregconst::_icsRegListOpenShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListExtCaption;
#define Dsregconst_icsRegListExtCaption System::LoadResourceString(&Dsregconst::_icsRegListExtCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListExtHint;
#define Dsregconst_icsRegListExtHint System::LoadResourceString(&Dsregconst::_icsRegListExtHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListExtShortCut;
#define Dsregconst_icsRegListExtShortCut System::LoadResourceString(&Dsregconst::_icsRegListExtShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListRefreshCaption;
#define Dsregconst_icsRegListRefreshCaption System::LoadResourceString(&Dsregconst::_icsRegListRefreshCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListRefreshHint;
#define Dsregconst_icsRegListRefreshHint System::LoadResourceString(&Dsregconst::_icsRegListRefreshHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListRefreshShortCut;
#define Dsregconst_icsRegListRefreshShortCut System::LoadResourceString(&Dsregconst::_icsRegListRefreshShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListFindCaption;
#define Dsregconst_icsRegListFindCaption System::LoadResourceString(&Dsregconst::_icsRegListFindCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListFindHint;
#define Dsregconst_icsRegListFindHint System::LoadResourceString(&Dsregconst::_icsRegListFindHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListFindShortCut;
#define Dsregconst_icsRegListFindShortCut System::LoadResourceString(&Dsregconst::_icsRegListFindShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListFindNextCaption;
#define Dsregconst_icsRegListFindNextCaption System::LoadResourceString(&Dsregconst::_icsRegListFindNextCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListFindNextHint;
#define Dsregconst_icsRegListFindNextHint System::LoadResourceString(&Dsregconst::_icsRegListFindNextHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListFindNextShortCut;
#define Dsregconst_icsRegListFindNextShortCut System::LoadResourceString(&Dsregconst::_icsRegListFindNextShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListViewUnitParamCaption;
#define Dsregconst_icsRegListViewUnitParamCaption System::LoadResourceString(&Dsregconst::_icsRegListViewUnitParamCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListViewUnitParamHint;
#define Dsregconst_icsRegListViewUnitParamHint System::LoadResourceString(&Dsregconst::_icsRegListViewUnitParamHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListViewUnitParamShortCut;
#define Dsregconst_icsRegListViewUnitParamShortCut System::LoadResourceString(&Dsregconst::_icsRegListViewUnitParamShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListViewTemplateParamCaption;
#define Dsregconst_icsRegListViewTemplateParamCaption System::LoadResourceString(&Dsregconst::_icsRegListViewTemplateParamCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListViewTemplateParamHint;
#define Dsregconst_icsRegListViewTemplateParamHint System::LoadResourceString(&Dsregconst::_icsRegListViewTemplateParamHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListViewTemplateParamShortCut;
#define Dsregconst_icsRegListViewTemplateParamShortCut System::LoadResourceString(&Dsregconst::_icsRegListViewTemplateParamShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListPrintListBICaption;
#define Dsregconst_icsRegListPrintListBICaption System::LoadResourceString(&Dsregconst::_icsRegListPrintListBICaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListPrintListBIHint;
#define Dsregconst_icsRegListPrintListBIHint System::LoadResourceString(&Dsregconst::_icsRegListPrintListBIHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegListPrintListBIShortCut;
#define Dsregconst_icsRegListPrintListBIShortCut System::LoadResourceString(&Dsregconst::_icsRegListPrintListBIShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegListEditApplyBtnCaption;
#define Dsregconst_icsRegListEditApplyBtnCaption System::LoadResourceString(&Dsregconst::_icsRegListEditApplyBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegListEditCancelBtnCaption;
#define Dsregconst_icsRegListEditCancelBtnCaption System::LoadResourceString(&Dsregconst::_icsRegListEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifInsertCaption;
#define Dsregconst_icsRegClassifInsertCaption System::LoadResourceString(&Dsregconst::_icsRegClassifInsertCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifInsertHint;
#define Dsregconst_icsRegClassifInsertHint System::LoadResourceString(&Dsregconst::_icsRegClassifInsertHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifInsertShortCut;
#define Dsregconst_icsRegClassifInsertShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifInsertShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifEditCaption;
#define Dsregconst_icsRegClassifEditCaption System::LoadResourceString(&Dsregconst::_icsRegClassifEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifEditHint;
#define Dsregconst_icsRegClassifEditHint System::LoadResourceString(&Dsregconst::_icsRegClassifEditHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifEditShortCut;
#define Dsregconst_icsRegClassifEditShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifEditShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifDeleteCaption;
#define Dsregconst_icsRegClassifDeleteCaption System::LoadResourceString(&Dsregconst::_icsRegClassifDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifDeleteHint;
#define Dsregconst_icsRegClassifDeleteHint System::LoadResourceString(&Dsregconst::_icsRegClassifDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifDeleteShortCut;
#define Dsregconst_icsRegClassifDeleteShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifDeleteShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifSetTemplateCaption;
#define Dsregconst_icsRegClassifSetTemplateCaption System::LoadResourceString(&Dsregconst::_icsRegClassifSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifSetTemplateHint;
#define Dsregconst_icsRegClassifSetTemplateHint System::LoadResourceString(&Dsregconst::_icsRegClassifSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifSetTemplateShortCut;
#define Dsregconst_icsRegClassifSetTemplateShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifSetTemplateShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifReSetTemplateCaption;
#define Dsregconst_icsRegClassifReSetTemplateCaption System::LoadResourceString(&Dsregconst::_icsRegClassifReSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifReSetTemplateHint;
#define Dsregconst_icsRegClassifReSetTemplateHint System::LoadResourceString(&Dsregconst::_icsRegClassifReSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifReSetTemplateShortCut;
#define Dsregconst_icsRegClassifReSetTemplateShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifReSetTemplateShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifRefreshCaption;
#define Dsregconst_icsRegClassifRefreshCaption System::LoadResourceString(&Dsregconst::_icsRegClassifRefreshCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifRefreshHint;
#define Dsregconst_icsRegClassifRefreshHint System::LoadResourceString(&Dsregconst::_icsRegClassifRefreshHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifRefreshShortCut;
#define Dsregconst_icsRegClassifRefreshShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifRefreshShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifFindCaption;
#define Dsregconst_icsRegClassifFindCaption System::LoadResourceString(&Dsregconst::_icsRegClassifFindCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifFindHint;
#define Dsregconst_icsRegClassifFindHint System::LoadResourceString(&Dsregconst::_icsRegClassifFindHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifFindShortCut;
#define Dsregconst_icsRegClassifFindShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifFindShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifFindNextCaption;
#define Dsregconst_icsRegClassifFindNextCaption System::LoadResourceString(&Dsregconst::_icsRegClassifFindNextCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifFindNextHint;
#define Dsregconst_icsRegClassifFindNextHint System::LoadResourceString(&Dsregconst::_icsRegClassifFindNextHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifFindNextShortCut;
#define Dsregconst_icsRegClassifFindNextShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifFindNextShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifViewClassParamCaption;
#define Dsregconst_icsRegClassifViewClassParamCaption System::LoadResourceString(&Dsregconst::_icsRegClassifViewClassParamCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifViewClassParamHint;
#define Dsregconst_icsRegClassifViewClassParamHint System::LoadResourceString(&Dsregconst::_icsRegClassifViewClassParamHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifViewClassParamShortCut;
#define Dsregconst_icsRegClassifViewClassParamShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifViewClassParamShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifViewTemplateParamCaption;
#define Dsregconst_icsRegClassifViewTemplateParamCaption System::LoadResourceString(&Dsregconst::_icsRegClassifViewTemplateParamCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifViewTemplateParamHint;
#define Dsregconst_icsRegClassifViewTemplateParamHint System::LoadResourceString(&Dsregconst::_icsRegClassifViewTemplateParamHint)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifViewTemplateParamShortCut;
#define Dsregconst_icsRegClassifViewTemplateParamShortCut System::LoadResourceString(&Dsregconst::_icsRegClassifViewTemplateParamShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifEditApplyBtnCaption;
#define Dsregconst_icsRegClassifEditApplyBtnCaption System::LoadResourceString(&Dsregconst::_icsRegClassifEditApplyBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegClassifEditCancelBtnCaption;
#define Dsregconst_icsRegClassifEditCancelBtnCaption System::LoadResourceString(&Dsregconst::_icsRegClassifEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegTreeClassLoadMsg;
#define Dsregconst_icsRegTreeClassLoadMsg System::LoadResourceString(&Dsregconst::_icsRegTreeClassLoadMsg)
extern DELPHI_PACKAGE System::ResourceString _icsRegTreeClassLoadError;
#define Dsregconst_icsRegTreeClassLoadError System::LoadResourceString(&Dsregconst::_icsRegTreeClassLoadError)
extern DELPHI_PACKAGE System::ResourceString _icsRegTreeClassSelGroupCaption;
#define Dsregconst_icsRegTreeClassSelGroupCaption System::LoadResourceString(&Dsregconst::_icsRegTreeClassSelGroupCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegTreeClassFindLabCaption;
#define Dsregconst_icsRegTreeClassFindLabCaption System::LoadResourceString(&Dsregconst::_icsRegTreeClassFindLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegTreeClassApplyBtnCaption;
#define Dsregconst_icsRegTreeClassApplyBtnCaption System::LoadResourceString(&Dsregconst::_icsRegTreeClassApplyBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsRegTreeClassCancelBtnCaption;
#define Dsregconst_icsRegTreeClassCancelBtnCaption System::LoadResourceString(&Dsregconst::_icsRegTreeClassCancelBtnCaption)
}	/* namespace Dsregconst */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DSREGCONST)
using namespace Dsregconst;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DsregconstHPP
