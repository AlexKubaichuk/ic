﻿// ---------------------------------------------------------------------------
#include <vcl.h>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegTmplChoiceTree.h"
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #pragma link "cxButtonEdit"
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// #define ED       dynamic_cast<TcxButtonEdit*>(ED)
// ---------------------------------------------------------------------------
__fastcall TriChoiceTreeTmpl::TriChoiceTreeTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  FTreeClName = FCtrlOwner->GetNode(FNode->AV["ref"])->AV["tblname"];
  // Создаём метку контрола
  FLab              = new TLabel(this);
  FLab->Parent      = this;
  FLab->Transparent = true;
  FLab->Caption     = FNode->AV["name"] + ":";
  // Создаём метку для текстового значения
  FExtLab              = new TLabel(this);
  FExtLab->Parent      = this;
  FExtLab->Transparent = true;
  FExtLab->AutoSize    = false;
  FExtLab->ParentFont  = true;
  FExtLab->WordWrap    = true;
  FExtLab->Color       = clInactiveBorder;
  // Создаём контрол
  ED                                      = new TcxButtonEdit(this);
  ED->Parent                              = this;
  ED->ShowHint                            = true;
  ED->Style->StyleController              = FCtrlOwner->StyleController;
  ED->Properties->ClickKey                = ShortCut(VK_DOWN, TShiftState() << ssAlt);
  ED->Properties->ReadOnly                = true;
  ED->Properties->ViewStyle               = vsHideCursor;
  ED->Properties->Buttons->Items[0]->Kind = bkGlyph;
  ED->Properties->Buttons->Items[0]->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\tree.bmp"));
  /*
   }
   tmpBtn = ED->Properties->Buttons->Add();
   */
  // tmpBtn->Kind = bkText;
  // tmpBtn->Caption = "X";
  // tmpBtn->Glyph->LoadFromResourceName((int)HInstance,"CLEAR");
  ED->Properties->OnButtonClick = TreeBtnClick;
  // прочие настройки
  if (Required)
   ED->Style->Color = FReqColor;
  FLab->FocusControl = ED;
  // if (SrcDS()->Active)
  // {
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   {
    UnicodeString FVal = VarToStr(FCtrlOwner->TemplateData->Value[_UID_]);
    FSetEditValue(FGetIsGroup(FVal), FGetBeginCode(FVal), FGetEndCode(FVal));
   }
  // }
  // расположение и размеры
  FLab->Top = 2;
  FLab->Left = 2;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Constraints->MinWidth = 60;
  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
    ED->Left = FLab->Width + 5;
    ED->Top  = 0;
   }
  else
   {
    ED->Left = 2;
    ED->Top  = FLab->Height + 2;
    TwoLine  = true;
   }
  ED->Width = AParent->Width - 4 - ED->Left;
  if ((ED->Left + ED->Width) > (AParent->Width - 4))
   {
    TwoLine   = true;
    ED->Left  = 4;
    ED->Top   = FLab->Height;
    ED->Width = AParent->Width - 4 - ED->Left;
   }
  FExtLab->Left = FLab->Left;
  FExtLab->Width  = Width - 4;
  FExtLab->Height = (FExtLab->Height - 2) * (FNode->AV["linecount"].ToIntDef(1) - 1);
  FExtLab->Top    = ED->Top + ED->Height;
  CMaxH           = FExtLab->Top + FExtLab->Height + 2;
  CMinH           = CMaxH;
  Height          = CMaxH;
  // ***************************************************************************
  FClearBtn->Top = ED->Top + ED->Height / 2 - 10;
  ED->OnKeyDown  = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FClearClick(TObject * ASender)
 {
  FSetEditValue(0, "");
  // FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxButtonEdit * __fastcall TriChoiceTreeTmpl::FGetCastED()
 {
  return (TcxButtonEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FSetCastED(TcxButtonEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetOnDependValues()
 {
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::TreeBtnClick(TObject * Sender, int AButtonIndex)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString SrcCode = "";
  if (AButtonIndex == 0)
   {
    TdsRegTreeClassForm * Dlg = FCtrlOwner->GetTreeFormDlg(FTreeClName);
    Dlg->TemplateMode = true;
    Dlg->Caption      = FNode->AV["dlgtitle"];
    __int64 FClCode = FCtrlOwner->GetCtrlData(FNode, _ES_);
    if (FClCode)
     Dlg->clCode = FClCode;
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      FSetEditValue(Dlg->clSelectGroup, IntToStr(Dlg->clCode), IntToStr(Dlg->clCodeLast));
      // if (FSetEditValue(Dlg->clSelectGroup, IntToStr(Dlg->clCode), IntToStr(Dlg->clCodeLast)))
      // FCtrlOwner->TemplateData->Value[_UID_] = Dlg->clCode;
      // else
      // FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
     }
   }
  else
   {
    // if (SrcDSEditable())
    // {
    // FSetEditValue(0, "");
    // FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
    // }
   }
 }
// ---------------------------------------------------------------------------
__fastcall TriChoiceTreeTmpl::~TriChoiceTreeTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnButtonClick = NULL;
  delete FExtLab;
  delete ED;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::FSetEditValue(bool AIsGroup, UnicodeString AValue, UnicodeString ALastValue)
 {
  bool RC = false;
  try
   {
    if (AValue.ToIntDef(-1) != -1)
     {
      try
       {
        UnicodeString FNameVal, FExtCodeVal;
        FNameVal                               = FCtrlOwner->GetTreeValue(FTreeClName, AValue, FExtCodeVal);
        FExtLab->Caption                       = FNameVal;
        ED->Hint                               = FNameVal;
        ED->Text                               = FExtCodeVal;
        FchIsGroup                             = AIsGroup;
        FchCodeBeg                             = AValue;
        FchCodeEnd                             = ALastValue;
        FchExtCode                             = FExtCodeVal;
        FchText                                = FNameVal;
        FCtrlOwner->TemplateData->Value[_UID_] = FGetTemplateVal(FchIsGroup, FchCodeBeg, FchCodeEnd);
        RC                                     = true;
       }
      __finally
       {
       }
     }
    else
     {
      FExtLab->Caption                       = "";
      ED->Text                               = "";
      FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
     }
    DataChange(this);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  FExtLab->Width = ED->Left + ED->Width;
  Width          = ED->Left + ED->Width + 4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine && ED)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine && FLab)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    FExtLab->Left  = 2;
    FExtLab->Width = Width - 4;
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4 - _TMPL_CLEAR_WIDTH_;
     }
    else
     ED->Left = Width - ED->Width - 2 - _TMPL_CLEAR_WIDTH_;
    FClearBtn->Left = ED->Left + ED->Width + 2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  xVal = ED->Text;
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit;
  ED->Enabled        = FLab->Enabled;
  FExtLab->Enabled   = FLab->Enabled;
  FClearBtn->Enabled = ED->Enabled;
  if ( /* isEdit&&SrcDSEditable() && */ !isTemplate)
   {
    FSetEditValue(0, "");
    // FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit;
  ED->Enabled        = FLab->Enabled;
  FExtLab->Enabled   = FLab->Enabled;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  // if (SrcDSEditable())
  // {
  if (FSetEditValue(0, AVal))
   FCtrlOwner->TemplateData->Value[_UID_] = AVal;
  else
   FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  // }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN) // (Key == 'F') && Shift.Contains(ssCtrl)
   {
    // if (FEDModify)
    // {
    // FEDBtnClick(NULL, 0);
    // Key = 0;
    // }
   }
  else if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->TemplateData->Value[_UID_];
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceTreeTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceTreeTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return FCtrlOwner->GetCtrlData(FNode, _ES_);
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriChoiceTreeTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " ' tagtype not supported'");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED)
       {
        if (ED->Enabled)
         {
          if (!((TcxButtonEdit *)ED)->Text.Length())
           RC = false;
         }
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::FGetIsGroup(UnicodeString ASrc)
 {
  return (bool)(GetPart1(ASrc, ':').ToIntDef(0));
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetBeginCode(UnicodeString ASrc)
 {
  return GetPart1(GetPart2(ASrc, ':'), '#');
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetEndCode(UnicodeString ASrc)
 {
  return GetPart2(ASrc, '#');
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetTemplateVal(bool AIsGroup, int ABegin, int AEnd)
 {
  return FGetTemplateVal(AIsGroup, IntToStr(ABegin), IntToStr(AEnd));
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetTemplateVal(bool AIsGroup, UnicodeString ABegin, UnicodeString AEnd)
 {
  return IntToStr((int)AIsGroup) + ":" + ABegin + "#" + AEnd;
 }
// ---------------------------------------------------------------------------
