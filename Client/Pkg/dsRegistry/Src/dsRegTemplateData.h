//---------------------------------------------------------------------------

#ifndef dsRegTemplateDataH
#define dsRegTemplateDataH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <map>
//---------------------------------------------------------------------------

//#include "dsRegConstDef.h"
//#include "cxContainer.hpp"
//#include "dsRegTypeDef.h"
//#include "XMLContainer.h"
//#include "dsRegEDFunc.h"
//#include "dsRegTreeClass.h"
//---------------------------------------------------------------------------
using namespace std;
class PACKAGE TdsRegTemplateData : public TObject
{

typedef map<UnicodeString, Variant> TdsRegTemplateDataMap;
protected:
  TdsRegTemplateDataMap FDataMap1;
  TdsRegTemplateDataMap FDataMap2;
private:
  bool  FNoLock;

  Variant __fastcall FGetDataValue1(UnicodeString AId);
  void __fastcall FSetDataValue1(UnicodeString AId, Variant AValue);

  Variant __fastcall FGetDataValue2(UnicodeString AId);
  void __fastcall FSetDataValue2(UnicodeString AId, Variant AValue);
public:
  __fastcall TdsRegTemplateData();
  __fastcall ~TdsRegTemplateData();


  __property bool NoLock = {read=FNoLock, write=FNoLock};
  __property Variant Value[UnicodeString] = {read=FGetDataValue1, write=FSetDataValue1};
  __property Variant Value1[UnicodeString] = {read=FGetDataValue1, write=FSetDataValue1};
  __property Variant Value2[UnicodeString] = {read=FGetDataValue2, write=FSetDataValue2};

  void __fastcall Copy(TdsRegTemplateData *AValue);
  void __fastcall Clear();
  bool __fastcall IsTemplate(UnicodeString AId);
  void __fastcall Erase(UnicodeString AId);
  UnicodeString __fastcall ToString();
  UnicodeString __fastcall ToJSONString(bool AFull = false);
};
//---------------------------------------------------------------------------
#endif
