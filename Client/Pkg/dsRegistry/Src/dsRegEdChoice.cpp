﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdChoice.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriChoice::TriChoice(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  FLab                              = new TLabel(this);
  FLab->Parent                      = this;
  FLab->Transparent                 = true;
  FLab->Top                         = 2;
  FLab->Left                        = 4;
  FLab->Caption                     = FNode->AV["name"] + ":";
  ED                                = new TcxComboBox(this);
  ED->Properties->DropDownListStyle = lsEditFixedList;
  ED->Style->StyleController        = FCtrlOwner->StyleController;
  if (Required)
   ED->Style->Color = FReqColor;
  ED->Parent = this;
  ED->Properties->Items->Clear();
  TTagNode * itxNode = FNode->GetFirstChild();
  int MaxW = 16;
  while (itxNode)
   {
    if (itxNode->CmpName("choicevalue"))
     {
      ED->Properties->Items->AddObject(itxNode->AV["name"].c_str(), (TObject *)itxNode->AV["value"].ToInt());
      if (MaxW < ED->Canvas->TextWidth(itxNode->AV["name"]))
       MaxW = ED->Canvas->TextWidth(itxNode->AV["name"]);
     }
    itxNode = itxNode->GetNext();
   }
  int FVal = -1;
  if (IsAppend)
   {
    if (FCtrlOwner->IsTemplate(_UID_))
     FVal = FCtrlOwner->TemplateData->Value[_UID_];
    else if (FNode->AV["default"].ToIntDef(-1) != -1)
     FVal = FNode->AV["default"].ToInt();
   }
  else
   {
    Variant tmpVal = FCtrlOwner->GetCtrlData(FNode, _ES_);
    if (!(tmpVal.IsNull() || tmpVal.IsEmpty()))
     FVal = (int)tmpVal;
   }
  int xind = ED->Properties->Items->IndexOfObject((TObject *)FVal);
  if (xind != -1)
   {
    ED->ItemIndex = -1;
    ED->ItemIndex = xind;
    // ED->Text = "empty";
    FCtrlOwner->SetCtrlData(FNode, FVal, ED->Properties->Items->Strings[xind]);
   }
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Properties->OnChange  = FDataChange;
  ED->Constraints->MinWidth = 60;
  ED->Parent                = this;
  ED->Width                 = MaxW + GetSystemMetrics(SM_CXVSCROLL) + GetSystemMetrics(SM_CXBORDER) * 2 + 10;
  if (ED->Width < 60)
   ED->Width = 60;
  ED->Top    = 0;
  ED->Height = 21;
  Height     = 25;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Left = FLab->Left + FLab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
    TwoLine   = true;
    FLab->Top = 0;
    ED->Left  = FLab->Left + 5;
    ED->Top   = FLab->Height;
    CMaxH     = 38;
    CMinH     = 38;
    Height    = CMaxH;
    if ((ED->Left + ED->Width) > (AParent->Width - 2))
     ED->Width = AParent->Width - 4 - ED->Left;
   }
  FLab->FocusControl = ED;
  ED->OnKeyDown = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
TcxComboBox * __fastcall TriChoice::FGetCastED()
 {
  return (TcxComboBox *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::FSetCastED(TcxComboBox * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriChoice::~TriChoice()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   {
    if ((ALeft + ED->Width) < Width - 6)
     ED->Left = ALeft;
   }
  Width = ED->Left + ED->Width;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (TwoLine)
   {
    *AEDLeft = -1;
    *ALabR   = -1;
   }
  else
   {
    *AEDLeft = ED->Left;
    *ALabR   = FLab->Left + FLab->Width;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4;
     }
    else
     ED->Left = Width - ED->Width - 2;
    ED->Properties->OnChange = NULL;
    int FSaveIndex = ED->ItemIndex;
    ED->ItemIndex            = -1;
    ED->ItemIndex            = FSaveIndex;
    ED->Properties->OnChange = DataChange;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoice::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  DataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoice::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
    if (ED->ItemIndex >= 0)
     xVal += ED->Properties->Items->Strings[ED->ItemIndex];
   }
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  FLab->Enabled = isEnabled && tmpClearVal;
  ED->Enabled   = isEnabled && tmpClearVal;
  if (!isEnabled && tmpClearVal)
   {
    FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // value=NULL
    ED->ItemIndex = -1;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  int xind = ED->Properties->Items->IndexOfObject((TObject *)AVal.ToIntDef(0));
  if (xind != -1)
   {
    ED->ItemIndex = -1;
    ED->ItemIndex = xind;
    ED->Text      = AVal;
    // if (SrcDSEditable())
    // {
    FCtrlOwner->SetCtrlData(FNode, AVal, ED->Properties->Items->Strings[xind]);
    // irquSetAsStr(SrcDS(),FFlName,AVal);
    // }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    ED->ItemIndex = -1;
    ED->Text      = "";
    // FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown && !ED->DroppedDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetOnLab(TdsRegEDItem * AEDItem)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FLabList[AEDItem->DefNode->AV["uid"]] = AEDItem;
  DataChange(this);
  /*
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetLabList->Add(ANode);
   //  ED->Properties->OnChange = DataChange;
   DataChange(this); */
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  // ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoice::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled)
   {
    if (ED->ItemIndex >= 0)
     FCtrlOwner->SetCtrlData(FNode, (int)ED->Properties->Items->Objects[ED->ItemIndex],
      ED->Properties->Items->Strings[ED->ItemIndex]);
    else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
     FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
   }
  else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
   FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
  // присвоение меток --------------------------------------------------------------------------------
  if (FLabList.size()) // Получение текста для Label возможно от choice или choiceDB
   {
    int ItmIndex;
    ItmIndex = ED->ItemIndex;
    if (ItmIndex >= 0)
     {
      for (TRegEDMap::iterator i = FLabList.begin(); i != FLabList.end(); i++)
       {
        // xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
        if (FNode->CmpAV("uid", i->second->DefNode->AV["ref"])) // ref == uid.choice
           i->second->SetLabel(ED->Properties->Items->Strings[ED->ItemIndex]);
       }
     }
   }
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoice::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoice::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoice::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->ItemIndex >= 0)
   return IntToStr(((int)ED->Properties->Items->Objects[ED->ItemIndex]));
  return "";
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriChoice::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
    if (FCtrlOwner->DataProvider->OnGetCompValue)
     {
      if (ED->ItemIndex >= 0)
       RC = Variant((int)(int)ED->Properties->Items->Objects[ED->ItemIndex]);
      FCtrlOwner->DataProvider->OnGetCompValue(FNode, RC);
     }
    else
     {
      if (ED->ItemIndex >= 0)
       RC = Variant((int)(int)ED->Properties->Items->Objects[ED->ItemIndex]);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoice::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled)
       if (ED->ItemIndex < 0)
        RC = false;
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
