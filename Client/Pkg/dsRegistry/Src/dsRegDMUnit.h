//---------------------------------------------------------------------------
#ifndef dsRegDMUnitH
#define dsRegDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Datasnap.DSClientRest.hpp>
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
#include "dsRegED.h"
#include "dsRegConstDef.h"
//#include "ICSClassData.h"
#include "dsRegTypeDef.h"
#include "ExtUtils.h"
#include "cxClasses.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
/*
//---------------------------------------------------------------------------
#include "RegED.h"
#include "dsRegConstDef.h"
#include "ICSClassData.h"  */
//---------------------------------------------------------------------------
//namespace IcsReg
//{
//---------------------------------------------------------------------------
class PACKAGE TdsRegDM : public TDataModule
{
__published:	// IDE-managed Components
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);

private:
  int           FDefaultClassWidth,FULEditWidth;
  bool          FFullEdit;
  bool          FClassEditable;
  bool          FUnitListEditable;
  bool          FOpenCardEnabled;

  TdsRegFetch FFetchAll;
  TdsRegFetch FClassFetchAll;
  int FFetchRecCount;
  int FClassFetchRecCount;

  UnicodeString FClassShowExpanded;
  UnicodeString FDataPath, FXMLPath;

  TColor         FRequiredColor;

  UnicodeString FRegistryKey;
  TTagNode      *FRegDefNode;
  TTagNode      *FClassDefNode;
  TAxeXMLContainer *FXMLList;

  TGetCompValue         FOnGetCompValue;
  TExtBtnGetBitmapEvent FOnGetExtBtnGetBitmap;
  TRegGetFormatEvent    FOnGetFormat;
  TExtBtnClick          FExtBtnClick;
  TExtValidateData      FOnGetDefValues;
  TExtValidateData      FOnExtValidate;
  TExtBtnClick          FOnCtrlDataChange;
  TdsRegExtFilterSetEvent FOnExtFilterSet;
  TExtCountEvent        FExtCount;

  TExtEditData          FExtInsertData;
  TExtEditData          FExtEditData;

  TdsExportData          FOnExportData;
  TdsImportData          FOnImportData;
  TdsExportData          FOnInsPriv;
  TdsInsEditData         FOnInsPTCData;
  TdsInsEditData         FOnEditPTCData;

  TClassEditEvent       FOnClassAfterInsert;
  TClassEditEvent       FOnClassAfterEdit;
  TClassEditEvent       FOnClassAfterDelete;

  TUnitEvent            FOnOpenCard;
  TGetReportListEvent   FOnGetQuickFilterList;
  bool                  FUseQuickFilterSetting;

  bool                   FUseReportSetting;
  TdsRegReportClickEvent FPrintReportClick;
  TGetReportListEvent    FOnGetReportList;

  TUnitEvent            FOnPrintF63;
  TUnitEvent            FOnShowPrePlan;

  TcxEditStyleController *FStyleController;

  TdsGetClassXMLEvent FOnGetClassXML;
  TdsGetCountEvent    FOnGetCount;
  TdsGetIdListEvent   FOnGetIdList;
  TdsGetValByIdEvent  FOnGetValById;
  TdsGetValById10Event  FOnGetValById10;
  TdsFindEvent        FOnFind;
  TdsInsertDataEvent  FOnInsertData;
  TdsEditDataEvent    FOnEditData;
  TdsDeleteDataEvent  FOnDeleteData;
  TdsRegListType      FUnitListOpenType;
  TdsRegListType      FClassListOpenType;



//  TdsICClassClient   *FAppClient;

  //    TClassHashTable *FClassHash;
  TTagNode* __fastcall FGetClassDef();
//  TTagNode* __fastcall FGetRegDefNode();
  bool               FSingleEditCol;
  bool               FClsSingleEditCol;

public:
//  TICSClassData *FClassData;
  UnicodeString   DTFormats[3];
  TForm      *UnitList;
  TForm      *UnitCard;
  TTagNode     *SelRegUnit;
  UnicodeString __fastcall NewGUID();
  __fastcall TdsRegDM(TComponent* Owner);

  __property TAxeXMLContainer *XMLList = {read=FXMLList, write=FXMLList};
  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ADef);
  bool __fastcall GetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData);

  UnicodeString __fastcall kabCDSGetClassXML(UnicodeString AId);
  __int64           __fastcall kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  __fastcall kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSInsert(UnicodeString AId, TJSONObject* AValues, System::UnicodeString &ARecID);
  UnicodeString __fastcall kabCDSEdit(UnicodeString AId, TJSONObject* AValues, System::UnicodeString &ARecID);
  UnicodeString __fastcall kabCDSDelete(UnicodeString AId, UnicodeString ARecId);


  __property int DefaultClassWidth = {read=FDefaultClassWidth, write=FDefaultClassWidth};
  __property int ULEditWidth = {read=FULEditWidth, write=FULEditWidth};

  __property bool FullEdit = {read=FFullEdit, write=FFullEdit};
  __property UnicodeString RegistryKey = {read=FRegistryKey, write=FRegistryKey};
  __property TTagNode* ClassDef = {read=FGetClassDef};
  __property TTagNode* RegDef = {read=FRegDefNode, write = FRegDefNode};

//  __property TdsICClassClient   *Connection = {read=FAppClient, write=FAppClient};

  __property bool ClassEditable = {read=FClassEditable, write=FClassEditable};
  __property bool UnitListEditable = {read=FUnitListEditable, write=FUnitListEditable};

  __property bool OpenCardEnabled = {read=FOpenCardEnabled, write=FOpenCardEnabled};



  __property UnicodeString ClassShowExpanded = {read=FClassShowExpanded, write=FClassShowExpanded};

  __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};
  __property TColor RequiredColor                    = {read=FRequiredColor,  write=FRequiredColor};

  __property TGetCompValue         OnGetCompValue       = {read=FOnGetCompValue, write=FOnGetCompValue};
  __property TExtBtnGetBitmapEvent OnGetExtBtnGetBitmap = {read=FOnGetExtBtnGetBitmap, write=FOnGetExtBtnGetBitmap};
  __property TRegGetFormatEvent    OnGetFormat          = {read=FOnGetFormat, write=FOnGetFormat};
  __property TExtBtnClick          OnExtBtnClick        = {read=FExtBtnClick, write=FExtBtnClick};
  __property TExtValidateData      OnGetDefValues       = {read=FOnGetDefValues, write=FOnGetDefValues};
  __property TExtValidateData      OnExtValidate        = {read=FOnExtValidate, write=FOnExtValidate};
  __property TExtBtnClick          OnCtrlDataChange     = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};

  __property TdsExportData          OnExportData         = {read=FOnExportData, write=FOnExportData};
  __property TdsImportData          OnImportData         = {read=FOnImportData, write=FOnImportData};
  __property TdsExportData          OnInsPriv            = {read=FOnInsPriv, write=FOnInsPriv};
  __property TdsInsEditData         OnInsPTCData         = {read=FOnInsPTCData, write=FOnInsPTCData};
  __property TdsInsEditData         OnEditPTCData        = {read=FOnEditPTCData, write=FOnEditPTCData};

  __property TExtEditData          OnExtInsertData      = {read=FExtInsertData, write=FExtInsertData};
  __property TExtEditData          OnExtEditData        = {read=FExtEditData, write=FExtEditData};

  __property TdsRegFetch FetchAll        = {read=FFetchAll,write=FFetchAll};
  __property TdsRegFetch ClassFetchAll   = {read=FClassFetchAll,write=FClassFetchAll};
  __property int FetchRecCount        = {read=FFetchRecCount,write=FFetchRecCount};
  __property int ClassFetchRecCount   = {read=FClassFetchRecCount,write=FClassFetchRecCount};

  __property TClassEditEvent OnClassAfterInsert = {read=FOnClassAfterInsert,write=FOnClassAfterInsert};
  __property TClassEditEvent OnClassAfterEdit   = {read=FOnClassAfterEdit,write=FOnClassAfterEdit};
  __property TClassEditEvent OnClassAfterDelete = {read=FOnClassAfterDelete,write=FOnClassAfterDelete};


  __property TUnitEvent          OnOpenCard           = {read=FOnOpenCard,write=FOnOpenCard};
  __property TGetReportListEvent OnGetQuickFilterList = {read=FOnGetQuickFilterList, write=FOnGetQuickFilterList};
  __property bool UseQuickFilterSetting               = {read=FUseQuickFilterSetting, write=FUseQuickFilterSetting};

  __property TdsRegReportClickEvent OnPrintReportClick = {read=FPrintReportClick, write=FPrintReportClick};
  __property TGetReportListEvent    OnGetReportList    = {read=FOnGetReportList, write=FOnGetReportList};
  __property bool UseReportSetting                     = {read=FUseReportSetting, write=FUseReportSetting};

  __property TUnitEvent          OnPrintF63 = {read=FOnPrintF63, write=FOnPrintF63};
  __property TUnitEvent          OnShowPrePlan = {read=FOnShowPrePlan, write=FOnShowPrePlan};

  __property TdsGetClassXMLEvent OnGetClassXML  = {read=FOnGetClassXML,write=FOnGetClassXML};
  __property TdsGetCountEvent OnGetCount  = {read=FOnGetCount,write=FOnGetCount};
  __property TdsGetIdListEvent OnGetIdList  = {read=FOnGetIdList,write=FOnGetIdList};
  __property TdsGetValByIdEvent OnGetValById  = {read=FOnGetValById,write=FOnGetValById};
  __property TdsGetValById10Event OnGetValById10  = {read=FOnGetValById10,write=FOnGetValById10};
  __property TdsFindEvent OnFind  = {read=FOnFind,write=FOnFind};
  __property TdsInsertDataEvent OnInsertData  = {read=FOnInsertData,write=FOnInsertData};
  __property TdsEditDataEvent OnEditData  = {read=FOnEditData,write=FOnEditData};
  __property TdsDeleteDataEvent OnDeleteData  = {read=FOnDeleteData,write=FOnDeleteData};
  __property TdsRegExtFilterSetEvent OnExtFilterSet= {read=FOnExtFilterSet, write=FOnExtFilterSet};
  __property TExtCountEvent OnExtCount = {read=FExtCount, write=FExtCount};

  __property TdsRegListType      UnitListOpenType = {read=FUnitListOpenType, write=FUnitListOpenType};
  __property TdsRegListType      ClassListOpenType = {read=FClassListOpenType, write=FClassListOpenType};
  __property UnicodeString       XMLPath = {read=FXMLPath, write=FXMLPath};
  __property UnicodeString       DataPath = {read=FDataPath, write=FDataPath};
  __property bool SingleEditCol = {read=FSingleEditCol, write=FSingleEditCol};
  __property bool ClsSingleEditCol = {read=FClsSingleEditCol, write=FClsSingleEditCol};
};
//}; // end of namespace DataExchange
//using namespace IcsReg;
//---------------------------------------------------------------------------
extern PACKAGE TdsRegDM *dsRegDM;
//---------------------------------------------------------------------------
#endif

