﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegTmplDigit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define ED         ((TcxDBMaskEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriDigitTmpl::TriDigitTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  // TRect xSize;
  FLab                = new TLabel(this);
  FLab->Parent        = this;
  FLab->Transparent   = true;
  FLab->Top           = 2;
  FLab->Left          = 4;
  FLab->Caption       = FNode->AV["name"] + ":";
  FFrLab              = new TLabel(this);
  FFrLab->Parent      = this;
  FFrLab->Transparent = true;
  FFrLab->Caption     = "от";
  FToLab              = new TLabel(this);
  FToLab->Parent      = this;
  FToLab->Transparent = true;
  FToLab->Caption     = "до";
  FIsFloat            = (FNode->AV["decimal"].ToIntDef(-1) != -1);
  // ed1
  ED                              = new TcxMaskEdit(this);
  ED->Parent                      = this;
  ED->Style->StyleController      = FCtrlOwner->StyleController;
  ED->Properties->OnChange        = FDataChange;
  ED->Properties->Alignment->Horz = taRightJustify;
  ED->Properties->MaskKind        = emkRegExpr;
  ED->Properties->EditMask = icsCalcMask(FIsFloat, FNode->AV["digits"].ToIntDef(0), FNode->AV["decimal"].ToIntDef(0),
    FNode->AV["min"]);
  ED->Properties->OnValidate = FEditValidate;
  // ed2
  FED2                              = new TcxMaskEdit(this);
  FED2->Parent                      = this;
  FED2->Style->StyleController      = FCtrlOwner->StyleController;
  FED2->Properties->OnChange        = FDataChange;
  FED2->Properties->Alignment->Horz = taRightJustify;
  FED2->Properties->MaskKind        = emkRegExpr;
  FED2->Properties->EditMask        = ED->Properties->EditMask;
  FED2->Properties->OnValidate      = FEditValidate;
  // ED->DataBinding->DataField = FFlName; ED->DataBinding->DataSource = FSrc;
  if (Required)
   {
    ED->Style->Color   = FReqColor;
    FED2->Style->Color = FReqColor;
   }
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   FSetEDValue(FCtrlOwner->TemplateData->Value[_UID_]);
  ED->Enabled     = isEnabled && isEdit && (!isTemplate || FCtrlOwner->TemplateNoLock());
  FED2->Enabled   = ED->Enabled;
  FFrLab->Enabled = ED->Enabled;
  FToLab->Enabled = ED->Enabled;
  // ED->Properties->MaxLength = FNode->AV["digits"].ToInt();
  ED->Constraints->MinWidth   = 40;
  ED->Width                   = 40;
  FED2->Constraints->MinWidth = 40;
  FED2->Width                 = 40;
  FToLab->AlignWithMargins    = true;
  FFrLab->AlignWithMargins    = true;
  FFrLab->Left                = 0;
  ED->Left                    = 100;
  FToLab->Left                = 200;
  FED2->Left                  = 300;
  FClearBtn->Left             = 400;
  FFrLab->Align               = alRight;
  ED->Align                   = alRight;
  FToLab->Align               = alRight;
  FED2->Align                 = alRight;
  FClearBtn->Align            = alRight;
  /* ED->Top = xSize.Top;
   ED->Height = xSize.Bottom;

   if (FLab->Width > (AParent->Width-2))
   {
   FLab->Font->Size = FLab->Font->Size - 1;
   FLab->Update();
   if (FLab->Width > (AParent->Width - 2))
   FLab->Width = AParent->Width-2;
   }
   ED->Left = FLab->Left + FLab->Width + 5;
   if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
   TwoLine = true;  FLab->Top = 0;
   ED->Left = FLab->Left+5;  ED->Top = FLab->Height;
   CMaxH = 34; CMinH = 34;
   Height = CMaxH;
   if ((ED->Left + ED->Width) > (AParent->Width - 2))
   ED->Width = AParent->Width - 4 - ED->Left;
   if (FNode->CmpName("text"))
   ED->Width = AParent->Width - 12 - ED->Left;
   }

   FLab->FocusControl = ED;
   FClearBtn->Top = ED->Top+ED->Height/2 - 10; */
  ED->OnKeyDown = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FClearClick(TObject * ASender)
 {
  ED->Text                               = "";
  FED2->Text                             = "";
  FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxMaskEdit * __fastcall TriDigitTmpl::FGetCastED()
 {
  return (TcxMaskEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FSetCastED(TcxMaskEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriDigitTmpl::~TriDigitTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  // if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  // if (ED)                                Width = ED->Left+ED->Width+4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine && ED)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine && FLab)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    // if (TwoLine) {ED->Left = 2; ED->Width = Width-4 - _TMPL_CLEAR_WIDTH_;}
    // else         {ED->Left = Width - ED->Width - 2 - _TMPL_CLEAR_WIDTH_;}
    // FClearBtn->Left = ED->Left+ED->Width+2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDigitTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  DataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDigitTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString RC = "";
  if (ED->Enabled)
   {
    if (ED->Text.Length())
     RC += " от " + ED->Text;
    if (FED2->Text.Length())
     RC += " до " + FED2->Text;
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit && !isTemplate;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FED2->Enabled      = ED->Enabled;
  FFrLab->Enabled    = ED->Enabled;
  FToLab->Enabled    = ED->Enabled;
  FClearBtn->Enabled = ED->Enabled;
  if (isEnabled && isEdit && !isTemplate)
   {
    // ED->Style->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
    // ED->Style->Color = clBtnFace;
    if (isEdit && !isTemplate)
     {
      ED->Text   = "";
      FED2->Text = "";
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit && !isTemplate;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FED2->Enabled      = ED->Enabled;
  FFrLab->Enabled    = ED->Enabled;
  FToLab->Enabled    = ED->Enabled;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  FSetEDValue(AVal);
  FCtrlOwner->TemplateData->Value[_UID_] = FGetValue();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled)
   {
    if (ED->Text.Trim().Length() || FED2->Text.Trim().Length())
     FCtrlOwner->TemplateData->Value[_UID_] = FGetValue();
    else if (isEdit && !isTemplate)
     FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
  else if (isEdit && !isTemplate)
   FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->TemplateData->Value[_UID_];
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriDigitTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriDigitTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDigitTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return FGetValue();
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriDigitTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Text.Length() || FED2->Text.Length())
   return Variant(FGetValue());
  else
   return Variant::Empty();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FSetEDValue(UnicodeString AVal)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (AVal.Pos(";"))
   {
    ED->Text = GetLPartB(AVal.Trim(), ';');
    try
     {
      if (ED->Text.Length())
       ED->EditValue = ICSStrToFloat(ED->Text);
     }
    catch (...)
     {
      ED->Text = "";
     }
    FED2->Text = GetRPartB(AVal.Trim(), ';');
    try
     {
      if (FED2->Text.Length())
       FED2->EditValue = ICSStrToFloat(FED2->Text);
     }
    catch (...)
     {
      FED2->Text = "";
     }
   }
  else
   {
    FED2->Text = "";
    ED->Text   = AVal.Trim();
    try
     {
      if (ED->Text.Length())
       ED->EditValue = ICSStrToFloat(ED->Text);
     }
    catch (...)
     {
      ED->Text = "";
     }
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDigitTmpl::FGetValue()
 {
  return ED->Text + ";" + FED2->Text;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDigitTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  TcxMaskEdit * FFocusCtrl = NULL;
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled)
       {
        if (!(ED->Text.Trim().Length() || FED2->Text.Trim().Length()))
         {
          FFocusCtrl = ED;
          RC         = false;
         }
       }
     }
    __finally
     {
      if (!RC)
       {
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
       }
     }
   }
  if (RC)
   {
    if (ED->Enabled && (ED->Text.Trim().Length() || FED2->Text.Trim().Length()))
     {
      if (ED->Text.Trim().Length())
       RC = CheckMinMax(ED->Text.Trim());
      else
       FFocusCtrl = ED;
      if (RC)
       {
        if (FED2->Text.Trim().Length())
         RC = CheckMinMax(FED2->Text.Trim());
        else
         FFocusCtrl = FED2;
       }
      if (!RC)
       {
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT3(icsriDigitValRange, ErrMessage, FNode->AV["min"], FNode->AV["max"]);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
       }
     }
   }
  if (!RC)
   {
    if (APC)
     APC->ActivePageIndex = this->Tag;
    if (FFocusCtrl && FFocusCtrl->Visible && Visible)
     FFocusCtrl->SetFocus();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDigitTmpl::CheckMinMax(UnicodeString AValue)
 {
  bool RC = false;
  try
   {
    if (!FIsFloat)
     { // целое
      int FMin, FMax, FVal;
      FMin = FNode->AV["min"].ToIntDef(0);
      FMax = FNode->AV["max"].ToIntDef(0);
      FVal = AValue.ToIntDef(0);
      RC   = !((FVal < FMin) || (FVal > FMax));
     }
    else
     { // вещественное  StrToFloat
      double FMin, FMax, FVal;
      FMin = ICSStrToFloatDef(FNode->AV["min"], 0);
      FMax = ICSStrToFloatDef(FNode->AV["max"], 0);
      FVal = ICSStrToFloatDef(AValue, 0);
      RC   = !((FVal < FMin) || (FVal > FMax));
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FEditValidate(TObject * Sender, Variant & DisplayValue, TCaption & ErrorText,
  bool & Error)
 {
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  if (FIsFloat)
   { // вещественное
    if (VarToWideStrDef(DisplayValue, "").Length())
     DisplayValue =
       Variant(System::Sysutils::StringReplace(System::Sysutils::StringReplace(VarToWideStrDef(DisplayValue, ""), ".",
      FormatSettings.DecimalSeparator, rFlag), ",", FormatSettings.DecimalSeparator, rFlag));
   }
  Error = false;
 }
// ---------------------------------------------------------------------------
