//---------------------------------------------------------------------------

#ifndef Registry_ICScompH
#define Registry_ICScompH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Db.hpp>
#include "dxBar.hpp"
#include "FIBQuery.hpp"
#include "pFIBQuery.hpp"
//#include "RegDefs.h"
#include "AxeUtil.h"
#include "RegEDFunc.h"
#include "ExtFilterSetTypes.h"

#include "cxGridCustomTableView.hpp"

//---------------------------------------------------------------------------
enum TRegClassType { ctAll = 0, ctList = 1, ctNone = 2};
//enum TExtFilterSetType { efsUnit, efsUnitSel, efsClass, efsExtUnit1, efsExtUnit2, efsExtUnit3, efsExtUnit4};
enum TRegistryDemoMessage
{
// ������ ������ �����
rdmUnitInsert,
rdmUnitEdit,
rdmUnitDelete,
rdmUnitSearch,
rdmUnitSearchNext,
rdmUnitTemplate,
rdmUnitFilter,
rdmUnitOpen,
rdmUnitExt,
rdmUnitRefresh,
rdmUnitInqPanel,

// ������ ���������������
rdmClassInsert,
rdmClassEdit,
rdmClassDelete,
rdmClassSearch,
rdmClassSearchNext,
rdmClassTemplate,
rdmClassFilter,
rdmClassList,
rdmClassRefresh
};
//---------------------------------------------------------------------------
class PACKAGE TRegEDContainer;
typedef void __fastcall (__closure *TRegistryDataEditEvent)(UnicodeString ATableName, UnicodeString ACode, UnicodeString AUID);
typedef void __fastcall (__closure *TRegistryDemoMessageEvent)(TRegistryDemoMessage AMsgType);
typedef void __fastcall (__closure *TUnitEvent)(__int64 Code, UnicodeString UnitStr);
typedef void __fastcall (__closure *TClassEditEvent)(TTagNode *ACls);
typedef void __fastcall (__closure *TInqUpdate)(__int64 Code, TControl* Sender, TDataSource *ASource);
typedef void __fastcall (__closure *TClassCallBack)(int Persent, UnicodeString Message);
typedef void __fastcall (__closure *TModifUnitEvent)(__int64 UnitCode, bool &CanModif);

typedef UnicodeString __fastcall (__closure *TGetAliasEvent)(TExtFilterSetType ASetType, TTagNode *ADefNode);

typedef UnicodeString __fastcall (__closure *TExtFilterSetEvent)(TExtFilterSetType ASetType, TTagNode *ADefNode, UnicodeString &ATab, bool &CanContinue, UnicodeString AFilterGUI);
typedef UnicodeString __fastcall (__closure *TExtEditDependFieldEvent)(TTagNode *ADefNode);
typedef void __fastcall (__closure *TExtFilterClearEvent)(TExtFilterSetType ASetType, TTagNode *ADefNode);
typedef void __fastcall (__closure *TExtFilterGetTextEvent)(TExtFilterSetType ASetType, TTagNode *ADefNode, TStrings *AValues, UnicodeString AFilterGUI);
typedef void __fastcall (__closure *TGetReportListEvent)(TStringList *AList);

typedef bool __fastcall (__closure *TBarItemGetEnabled)(__int64 UCode, TDataSource *ASource);
typedef void __fastcall (__closure *TBarItemClick)(__int64 UCode, TDataSource *ASource);

//typedef void __fastcall (__closure *TExtBtnClick)(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource, bool isClicked);
//typedef void __fastcall (__closure *TGetExtData)(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
typedef void __fastcall (__closure *TExtEditData)(TTagNode *AClsNode, TDataSource *ASource, __int64 UCode, bool &Continue, bool &ARefresh, __int64 &ACode);
typedef void __fastcall (__closure *TExtLoadXML)(TTagNode *ARegNode, TTagNode *ASelRegNode);
typedef void __fastcall (__closure *TClassEditCreate)( TForm* form, TRegEDContainer *FCtrList, TTagNode* AClassRoot, __int64 CODE);

typedef void __fastcall (__closure *TExtValidateData)(__int64 ARecCode, TTagNode *ANode, TRegEDContainer *ControlList, bool &Valid);
//---------------------------------------------------------------------------
class PACKAGE TExtBarItem : public TObject
{
private:
  UnicodeString FCapt;
  int FImagIdx;
  TBarItemGetEnabled FGetButtonEnabled;
  TBarItemClick      FExtItemClick;
  TShortCut FShortCut;

__published:

  __property TBarItemGetEnabled OnGetItemEnabled = {read=FGetButtonEnabled,write=FGetButtonEnabled};
  __property TBarItemClick OnItemClick = {read=FExtItemClick,write=FExtItemClick};
  __property UnicodeString Caption = {read=FCapt,write=FCapt};
  __property int ImageIndex = {read=FImagIdx,write=FImagIdx};
  __property TShortCut ShortCut = {read=FShortCut,write=FShortCut};

  __fastcall TExtBarItem();
};
//---------------------------------------------------------------------------
class PACKAGE TExtBarItemList : public TList
{
private:
  TExtBarItem* __fastcall FGetExtItem(int Idx);
public:
  TExtBarItem* __fastcall AddItem();
  __fastcall ~TExtBarItemList();
__published:
  __property TExtBarItem* Item[int Idx] = {read=FGetExtItem};
  __fastcall TExtBarItemList();
};
//---------------------------------------------------------------------------
class TicsREG_DM;
//class TRegTemplate;
class PACKAGE TRegistry_ICS : public TComponent
{
private:
        TImageList      *FImages;
        TImageList      *FLargeImages;
        TImageList      *FSelImages;
        TImageList      *FSelLargeImages;
        TImageList      *FHotImages;
        TExtBarItemList *FExtBarItems;
        TImageList      *FDisabledImages;
        TAttr           *xAtt;
        UnicodeString      FFindCaption,FInqCompName,FBuyCaption;
        UnicodeString      FDBUser,FDBPassword,FDBCharset,FUpperFunc,FDTFormat;
        bool            FActive;
        bool            FADO;
        bool            FFetchAll;
        bool            FClassEditable;
        bool            FRefreshList;
        bool            FCallEvtForInvisebleExtEdit;

        UnicodeString      FCInsertShortCut;
        UnicodeString      FCEditShortCut;
        UnicodeString      FCDeleteShortCut;
        bool            FCInsert;
        bool            FCEdit;
        bool            FCDelete;
        bool            FUseDisableControls;
        bool            FUseRefresh;
        bool            FUseReportSetting;
        bool            FUseQuickFilterSetting;

        bool            FUseTemplate;
        bool            FShowUniqueRec;
        bool            FUseFilter;
        bool            FUseClassDisControls;
        bool            FUseProgressBar;
        bool            FUseUnitGUID;
        bool            FUseClsGUID;
        bool            FUseAfterScroll;
        bool            FUseSCInfPanel;
        bool            FUseLargeImages;
        bool            FExtShortCutInfPanel;
        bool            FClassEnabled;
        bool            FListEditEnabled;
        bool            FOpenCardEnabled;
        bool            FFullEdit,FFindPanel;
        int             FULEditWidth;
        int             FUListTop;
        int             FUListLeft;
        int             FDefaultListWidth;
        int             FDefaultClassWidth;
        TControl        *FInqComp;
        TList           *tmpChTree;
        UnicodeString      FInsCapt;
        UnicodeString      FEditCapt;
        UnicodeString      FDelCapt;
        UnicodeString      FExtCapt;
        UnicodeString      FRegKey;

        UnicodeString      FClKeyName;
        UnicodeString      FDBName;
        UnicodeString      FTabPref;
        UnicodeString      FULCaption;
        UnicodeString      FXMLName;
        TNotifyEvent    FUnitListCreated;
        TNotifyEvent    FUnitListDeleted;
        TExtLoadXML     FLoadExtXML;
        TExtBtnClick    FExtBtnClick;
        TExtBtnClick    FGetExtWhere;
        TExtBtnClick    FOnCtrlDataChange;
        TGetExtData     FGetExtData;
        TClassCallBack  FOnInit;
        TClassCallBack  FDesOnInit;
        TRegistryDemoMessageEvent FDemoMsg;
        TExtEditData    FExtInsertData;
        TExtEditData    FExtEditData;
        TExtEditData    FExtEditExtData;
        TExtEditData    FExtDeleteData;
        TUnitEvent      FOnAfterInsert;
        TClassEditEvent FOnClassAfterInsert;
        TClassEditEvent FOnClassAfterEdit;
        TClassEditEvent FOnClassAfterDelete;
        TExtEditDependFieldEvent FExtEditGetDependField;
        TColor          FReqColor;
        TColor          FClassTreeColor;
        TModifUnitEvent FOnBeforeInsert;
        TClassEditCreate FClassEditCreate; // ��������� ����������� �.�. 7.10.08
        TUnitEvent      FOnAfterEdit;
        TModifUnitEvent FOnBeforeEdit;
        TUnitEvent      FOnAfterDelete;
        TModifUnitEvent FOnBeforeDelete;
        TGetAliasEvent  FGetAlias;
        TUnitEvent      FOnOpenCard;
        TInqUpdate      FOnInqUpdate;
        TNotifyEvent    FOnListClose;
        TNotifyEvent    FOnBeforeRefresh;
        TExtValidateData FOnExtValidate;
        TExtValidateData FOnGetDefValues;
        TGetCompValue   FGetCompValue;
        TRegEditButtonClickEvent  FOnRegEditButtonClick;
        TExtBtnGetBitmapEvent FExtBtnGetBitmap;

        TRegistryDataEditEvent FSQLAfterInsertEvt;
        TRegistryDataEditEvent FSQLBeforeEditEvt;
        TRegistryDataEditEvent FSQLAfterEditEvt;
        TRegistryDataEditEvent FSQLAfterDeleteEvt;

        TStartDragEvent  FOnUnitStartDrag;
        TDragOverEvent   FOnUnitDragOver;
        TDragDropEvent   FOnUnitDragDrop;
        TEndDragEvent    FOnUnitEndDrag;

        TExtFilterSetEvent FOnExtFilterSet;
        TExtFilterClearEvent FOnExtFilterClear;
        TExtFilterGetTextEvent FOnExtFilterGetText;

        TUnitEvent       FPrintReportClick;
        TGetReportListEvent FGetReportList;

//        TUnitEvent       FQuickFilterClick;
        TGetReportListEvent FGetQuickFilterList;

        TcxGridGetCellStyleEvent FListContentStyleEvent;
        TRegGetOrderEvent        FOnRegGetOrderEvent;
        TRegListGetColWidthEvent FOnGetListColumnWidth;

        TRegGetFormatEvent  FOnGetFormat;

        void __fastcall FSetActive(bool AActive);
        TStringList* __fastcall FGetTreeFormList(void);
        void __fastcall FSetClassEnabled(bool AClassEnabled);
        void __fastcall FSetListEditEnabled(bool AListEditEnabled);
        void __fastcall FSetOpenCardEnabled(bool AOpenCardEnabled);
        void __fastcall FSetFullEdit(bool AFullEdit);
        void __fastcall FSetUListTop(int ATop);
        void __fastcall FSetUListLeft(int ALeft);
        void __fastcall FSetDBName(UnicodeString  ADBName);
        void __fastcall FSetXMLName(UnicodeString  AXMLName);
        void __fastcall FSetInqComp(TControl *AInqComp);
        void __fastcall FSetInit(int APersent, UnicodeString AMess);
        void __fastcall FSetUpperFunc(UnicodeString  AParam);
        void __fastcall FSetDTFormat(UnicodeString  AParam);
protected:
        bool FNativeStyle;
        TcxLookAndFeelKind FLFKind;
        TObject   *PrF;
        bool __fastcall Connect(UnicodeString ADBName, UnicodeString AXMLName);
        bool __fastcall GetTreeCh(TTagNode *itTag, UnicodeString &UID);
        void __fastcall GetTreeChoice(TList *AList);
public:
        TicsREG_DM   *DM;
        TAnsiStrMap  HelpContextList;
        void       __fastcall ShowSelUnitList(UnicodeString ATmpl,TStringList *ACodeList);
        void       __fastcall ShowClasses(TRegClassType AShowType = ctList, UnicodeString AGroups = "", int AClassNum = 0);
        void       __fastcall ShowClasses(UnicodeString AClassID, TRegClassType AShowType = ctList, UnicodeString AGroups = "");
        void       __fastcall AddTreeFormList(UnicodeString ANodeRef, UnicodeString ANodeName, UnicodeString AClassRef);
        UnicodeString __fastcall GetUnitTabName();
        void       __fastcall ShowUnitList  ( bool isModal =false);
        void       __fastcall GetSelectedUnits(TStrings *ADest);
        void       __fastcall RefreshUnitList();
        bool       __fastcall IsListShowing();
        UnicodeString  FClassShowExpanded;
        void       __fastcall FSetClassShowExpanded(UnicodeString AClassShowExpanded);
        bool       __fastcall GetSelected(__int64 *ACode, UnicodeString &AUnitStr);
        TdxBar*    __fastcall GetListExtTB(void);
        void       __fastcall InvadeClass(TComboBox* AClassCB, __int64 AClassCode, bool AisFirstNULL, UnicodeString AClassUID, UnicodeString APref);

         /*************************************************************
         * �������� ������� ������ � ������� ������ �� ������������   *
         * ���������: ��������� �.�. 23.09.08                         *
         * ������: --->                                               *
         *************************************************************/
        __int64 __fastcall GetMCode     ( UnicodeString AGenName );
        void __fastcall qtExec       ( UnicodeString ASQL, bool aCommit = false );
        void __fastcall qtStart      ( UnicodeString ASQL );
        void __fastcall qtExecCommit ( );
        void __fastcall qtRollback   ( );
        TpFIBQuery* getQueryLink     ( );
         /*************************************************************
         * �������� ������� ������ � ������� ������ �� ������������   *
         * ���������: ��������� �.�. 23.09.08                         *
         *  <--- �����                                                *
         *************************************************************/

        __fastcall TRegistry_ICS(TComponent* Owner);
        __fastcall ~TRegistry_ICS();
__published:
        __property TRegistryDataEditEvent SQLAfterInsert  = {read=FSQLAfterInsertEvt, write=FSQLAfterInsertEvt};
        __property TRegistryDataEditEvent SQLBeforeEdit   = {read=FSQLBeforeEditEvt,  write=FSQLBeforeEditEvt};
        __property TRegistryDataEditEvent SQLAfterEdit    = {read=FSQLAfterEditEvt,   write=FSQLAfterEditEvt};
        __property TRegistryDataEditEvent SQLAfterDelete  = {read=FSQLAfterDeleteEvt, write=FSQLAfterDeleteEvt};

        __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

        __property bool NativeStyle = {read=FNativeStyle, write=FNativeStyle};
        __property TcxLookAndFeelKind LFKind= {read=FLFKind, write=FLFKind};

        __property TUnitEvent OnPrintReportClick    = {read=FPrintReportClick, write=FPrintReportClick};
        __property TGetReportListEvent OnGetReportList    = {read=FGetReportList, write=FGetReportList};

//        __property TUnitEvent OnQuickFilterClick    = {read=FQuickFilterClick, write=FQuickFilterClick};
        __property TGetReportListEvent OnGetQuickFilterList    = {read=FGetQuickFilterList, write=FGetQuickFilterList};

        __property bool UseReportSetting  = {read=FUseReportSetting, write=FUseReportSetting};
        __property bool UseQuickFilterSetting  = {read=FUseQuickFilterSetting, write=FUseQuickFilterSetting};

        __property bool UseDisableControls  = {read=FUseDisableControls, write=FUseDisableControls};
        __property bool UseUnitGUID  = {read=FUseUnitGUID, write=FUseUnitGUID};
        __property bool UseClsGUID  = {read=FUseClsGUID, write=FUseClsGUID};

        __property bool CallEvtForInvisebleExtEdit  = {read=FCallEvtForInvisebleExtEdit, write=FCallEvtForInvisebleExtEdit};

        __property TRegEditButtonClickEvent  OnRegEditButtonClick  = {read=FOnRegEditButtonClick, write=FOnRegEditButtonClick};
        __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

        __property int  DefaultListWidth  = {read=FDefaultListWidth, write=FDefaultListWidth};
        __property int  DefaultClassWidth  = {read=FDefaultClassWidth, write=FDefaultClassWidth};

        __property TGetExtData OnGetExtData    = {read=FGetExtData, write=FGetExtData};
        __property TExtBtnClick OnExtBtnClick  = {read=FExtBtnClick, write=FExtBtnClick};
        __property TRegistryDemoMessageEvent OnDemoMessage  = {read=FDemoMsg, write=FDemoMsg};
        __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
        __property TGetCompValue OnGetCompValue = {read=FGetCompValue, write=FGetCompValue};

        __property TExtBtnClick OnCtrlDataChange    = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};

        __property TExtEditData OnExtInsertData   = {read=FExtInsertData, write=FExtInsertData};
        __property TExtEditData OnExtEditData     = {read=FExtEditData, write=FExtEditData};
        __property TExtEditData OnExtEditExtData     = {read=FExtEditExtData, write=FExtEditExtData};
        __property TExtEditData OnExtDeleteData   = {read=FExtDeleteData, write=FExtDeleteData};

        __property TExtFilterSetEvent OnExtFilterSet   = {read=FOnExtFilterSet, write=FOnExtFilterSet};
        __property TExtFilterClearEvent OnExtFilterClear = {read=FOnExtFilterClear, write=FOnExtFilterClear};
        __property TExtFilterGetTextEvent OnExtFilterGetText = {read=FOnExtFilterGetText, write=FOnExtFilterGetText};

        __property TGetAliasEvent   OnGetAlias = {read=FGetAlias, write=FGetAlias};

        __property TStartDragEvent  OnUnitStartDrag = {read=FOnUnitStartDrag, write=FOnUnitStartDrag};
        __property TDragOverEvent   OnUnitDragOver = {read=FOnUnitDragOver, write=FOnUnitDragOver};
        __property TDragDropEvent   OnUnitDragDrop = {read=FOnUnitDragDrop, write=FOnUnitDragDrop};
        __property TEndDragEvent    OnUnitEndDrag = {read=FOnUnitEndDrag, write=FOnUnitEndDrag};

        // ��������� 7.10.08 ��������� �.�.
        // ������� ������������ �� �������� ����� �������������� ��������������
        // � �������� ������ ����������� ����� -->
        __property TClassEditCreate OnAfterClassEditCreate = {read=FClassEditCreate, write=FClassEditCreate};
        // <--

        __property TExtLoadXML OnLoadXML   = {read=FLoadExtXML, write=FLoadExtXML};


        __property bool UseTemplate        = {read=FUseTemplate,write=FUseTemplate};
        __property bool UseFilter        = {read=FUseFilter,write=FUseFilter};
        __property bool UseProgressBar  = {read=FUseProgressBar,write=FUseProgressBar};

        __property bool FindPanel        = {read=FFindPanel,write=FFindPanel};
        __property bool ShortCutInfPanel = {read=FUseSCInfPanel,write=FUseSCInfPanel};
        __property bool ExtShortCutInfPanel = {read=FExtShortCutInfPanel,write=FExtShortCutInfPanel};

        __property bool InsertBtnEnabled   = {read=FCInsert,write=FCInsert};
        __property bool EditBtnEnabled     = {read=FCEdit,write=FCEdit};
        __property bool DeleteBtnEnabled   = {read=FCDelete,write=FCDelete};

        __property UnicodeString InsertShortCut  = {read=FCInsertShortCut,write=FCInsertShortCut};
        __property UnicodeString EditShortCut    = {read=FCEditShortCut,write=FCEditShortCut};
        __property UnicodeString DeleteShortCut  = {read=FCDeleteShortCut,write=FCDeleteShortCut};

        __property bool FullEdit        = {read=FFullEdit,write=FSetFullEdit};
        __property bool ClassEditable   = {read=FClassEditable,write=FClassEditable};
        __property bool RefreshList     = {read=FRefreshList,write=FRefreshList};

        __property bool UseClassDisControls   = {read=FUseClassDisControls,write=FUseClassDisControls};
        __property bool ShowUniqueRec         = {read=FShowUniqueRec,write=FShowUniqueRec};

        __property bool ClassEnabled    = {read=FClassEnabled,write=FSetClassEnabled};
        __property bool ListEditEnabled = {read=FListEditEnabled,write=FSetListEditEnabled};
        __property bool OpenCardEnabled = {read=FOpenCardEnabled,write=FSetOpenCardEnabled};
        __property bool ADOSource       = {read=FADO,write=FADO};
        __property bool Active          = {read=FActive,write=FSetActive};
//        __property bool LargeIcons      = {read=FLargeIcons,write=FLargeIcons};
        __property bool FetchAll        = {read=FFetchAll,write=FFetchAll};
        __property bool UseAfterScroll  = {read=FUseAfterScroll,write=FUseAfterScroll};
        __property bool UseRefresh  = {read=FUseRefresh,write=FUseRefresh};
        __property bool UseLargeImages  = {read=FUseLargeImages,write=FUseLargeImages};

        __property TcxGridGetCellStyleEvent OnGetListContentStyle = {read=FListContentStyleEvent,write=FListContentStyleEvent};
        __property TRegGetOrderEvent        OnGetOrder = {read=FOnRegGetOrderEvent, write=FOnRegGetOrderEvent};
        __property TRegListGetColWidthEvent OnGetListColumnWidth = {read=FOnGetListColumnWidth, write=FOnGetListColumnWidth};

        __property int ULEditWidth      = {read=FULEditWidth,write=FULEditWidth};
        __property int UnitListTop      = {read=FUListTop,write=FSetUListTop, default=0};
        __property int UnitListLeft     = {read=FUListLeft,write=FSetUListLeft, default=0};

        __property TClassEditEvent OnClassAfterInsert = {read=FOnClassAfterInsert,write=FOnClassAfterInsert};
        __property TClassEditEvent OnClassAfterEdit   = {read=FOnClassAfterEdit,write=FOnClassAfterEdit};
        __property TClassEditEvent OnClassAfterDelete = {read=FOnClassAfterDelete,write=FOnClassAfterDelete};

        __property UnicodeString FindCaption    = {read=FFindCaption,write=FFindCaption};

        __property UnicodeString DBName    = {read=FDBName,write=FSetDBName};
        __property UnicodeString TablePrefix   = {read=FTabPref,write=FTabPref};
        __property UnicodeString PrefixCaption = {read=FULCaption,write=FULCaption};
        __property UnicodeString XML_AV1_Name  = {read=FXMLName,write=FSetXMLName};
        __property TClassCallBack OnInit    = {read=FOnInit,write=FOnInit};

        __property UnicodeString RegistryKey = {read=FRegKey,write=FRegKey};
        __property UnicodeString ClassShowExpanded = {read=FClassShowExpanded,write=FSetClassShowExpanded};
        __property UnicodeString InsertBtnCaption = {read=FInsCapt,write=FInsCapt};
        __property UnicodeString EditBtnCaption   = {read=FEditCapt,write=FEditCapt};
        __property UnicodeString DeleteBtnCaption = {read=FDelCapt,write=FDelCapt};
        __property UnicodeString ExtBtnCaption    = {read=FExtCapt,write=FExtCapt};
        __property UnicodeString UpperFunc        = {read=FUpperFunc,write=FSetUpperFunc};
        __property UnicodeString DTFormat         = {read=FDTFormat,write=FSetDTFormat};

        __property TModifUnitEvent OnBeforeInsert = {read=FOnBeforeInsert,write=FOnBeforeInsert};
        __property TModifUnitEvent OnBeforeEdit   = {read=FOnBeforeEdit,write=FOnBeforeEdit};
        __property TModifUnitEvent OnBeforeDelete = {read=FOnBeforeDelete,write=FOnBeforeDelete};

        __property TColor RequiredColor  = {read=FReqColor,write=FReqColor};
        __property TColor ClassTreeColor = {read=FClassTreeColor,write=FClassTreeColor};

        __property TExtEditDependFieldEvent OnExtEditGetDependField = {read=FExtEditGetDependField,write=FExtEditGetDependField};

        __property TExtValidateData OnExtValidate = {read=FOnExtValidate,write=FOnExtValidate};
        __property TExtValidateData OnGetDefValues = {read=FOnGetDefValues,write=FOnGetDefValues};


        __property TUnitEvent OnAfterInsert = {read=FOnAfterInsert,write=FOnAfterInsert};
        __property TUnitEvent OnAfterEdit   = {read=FOnAfterEdit,write=FOnAfterEdit};
        __property TUnitEvent OnAfterDelete = {read=FOnAfterDelete,write=FOnAfterDelete};

        __property TUnitEvent OnOpenCard        = {read=FOnOpenCard,write=FOnOpenCard};
        __property TInqUpdate OnGetInqData      = {read=FOnInqUpdate,write=FOnInqUpdate};
        __property TNotifyEvent OnUnitListClose = {read=FOnListClose,write=FOnListClose};
        __property TNotifyEvent OnBeforeRefresh = {read=FOnBeforeRefresh,write=FOnBeforeRefresh};

        __property TNotifyEvent AfterUnitListCreate = {read=FUnitListCreated,write=FUnitListCreated};
        __property TNotifyEvent BeforeUnitListDeleted = {read=FUnitListDeleted,write=FUnitListDeleted};

        __property TControl* InqComponent       = {read=FInqComp,write=FSetInqComp};
        __property UnicodeString InqComponentName  = {read=FInqCompName,write=FInqCompName};

        __property UnicodeString BuyCaption  = {read=FBuyCaption,write=FBuyCaption};

        __property UnicodeString DBUser  = {read=FDBUser,write=FDBUser};
        __property UnicodeString DBPassword  = {read=FDBPassword,write=FDBPassword};
        __property UnicodeString DBCharset  = {read=FDBCharset,write=FDBCharset};

        __property TImageList* Images  = {read=FImages,write=FImages};
        __property TImageList* LargeImages  = {read=FLargeImages,write=FLargeImages};
        __property TImageList* SelImages  = {read=FSelImages,write=FSelImages};
        __property TImageList* SelLargeImages  = {read=FSelLargeImages,write=FSelLargeImages};

        __property TImageList* HotImages  = {read=FHotImages,write=FHotImages};
        __property TImageList* DisabledImages  = {read=FDisabledImages,write=FDisabledImages};
        __property TExtBarItemList*  ExtBarItems  = {read=FExtBarItems};

        __property TStringList* TreeFormList = {read=FGetTreeFormList};
};
//---------------------------------------------------------------------------
extern PACKAGE UnicodeString __fastcall GetTemplateFields(TTagNode *ADescription);
#endif

