//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riDateTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_DE      ((TcxDateEdit*)ED)
//---------------------------------------------------------------------------
__fastcall TriDateTmpl::TriDateTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
  Lab = new TLabel(this);  Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Top = 2;            Lab->Left = 4;
  Lab->Caption = FNode->AV["name"]+":";
  ED = new TcxDateEdit(this);
  tED_DE->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_DE->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  tED_DE->Properties->ButtonGlyph->LoadFromResourceName(reinterpret_cast<int>(HInstance), "CALENDARBTN");
  if (Required) tED_DE->Style->Color = FReqColor;

  xSize = Rect(0,0,85,21);
  ED->Constraints->MaxWidth = 85;
//   tED_DE->YearDigits = dyFour;
  ED->Constraints->MinWidth = 60;
  if ((xSize.Right+Lab->Width+Lab->Left+10) >= 255)
   {
     TwoLine = true;  Lab->Top = 0;
     ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
     CMaxH = 34; CMinH = 34;
     Height = CMaxH;
   }
  else
   {
     ED->Left = Lab->Width+5;
     ED->Top = xSize.Top;
   }
  ED->Width = xSize.Right+10;
  ED->Height = xSize.Bottom;
  ED->Parent =  this;
  Width = ED->Left+ED->Width;
  if (!FCtrlOwner->TemplateField && !_TemplateValue(FNode).IsEmpty())
   tED_DE->Date = TDate(UnicodeString((wchar_t*)_TemplateValue(FNode)));
  Lab->FocusControl = ED;
  if (Parent->ClassNameIs("TriDateTmpl"))
   {
     Anchors.Clear();
     Anchors << akLeft << akRight << akTop;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
  tED_DE->Properties->OnChange = riDataChange;
}
//---------------------------------------------------------------------------
__fastcall TriDateTmpl::~TriDateTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   tED_DE->Properties->OnChange = NULL;
   delete tED_DE;
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  if (FNode->CmpName("digit,date")&&TmpCtrl)     ED2->Left = ALeft;
  if (ED)                                Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED&&(!FNode->CmpName("binary")))  *AEDLeft = ED->Left;
  else                                            *AEDLeft = -1;
  if (!TwoLine&&Lab&&(!FNode->CmpName("binary"))) *ALabR = Lab->Left+Lab->Width;
  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (FNode->CmpName("text,date,datetime,choice,extedit"))
      {
        if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
        else          ED->Left = Width - ED->Width - 2;
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriDateTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_DE->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDateTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->CmpName("text,datetime,time,digit,date,choice,binary,choiceTree"))
   {
     if (ED)
      {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (ED->Enabled)
         {
           if (FNode->CmpName("date"))
            {
              if ((int)tED_DE->Date != Cxdateutils::NullDate)      xVal += tED_DE->Text;
            }
         }
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Lab->Enabled = AEnable;
  ED->Enabled = AEnable;
  tED_DE->Style->Color = (AEnable)?((Required)? FReqColor:clWindow):clBtnFace;
  if (!AEnable)
   tED_DE->Date = Cxdateutils::NullDate;
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (FNode->CmpName("date")&&isFV)
   {
     tED_DE->Text = AVal;
   }
  else if (AVal.Length())
   {
     tED_DE->Date = StrToDate(AVal);
   }
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  tED_DE->Properties->OnChange  = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled||isVal)
      {
           if ((int)tED_DE->Date != Cxdateutils::NullDate)      xVal = Variant(tED_DE->Date);
      }
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if ((int)tED_DE->Date != Cxdateutils::NullDate)  Src = tED_DE->Date.FormatString("dd.mm.yyyy");
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
/*
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        if (TmpCtrl) ItmIndex  = tED_CB->ItemIndex;
        else         ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= (int)TmpCtrl)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               {
                 if (TmpCtrl) labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
                 else         labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
               }
            }
         }
      }
*/
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if ((int)tED_DE->Date != Cxdateutils::NullDate)  Src = tED_DE->Date.FormatString("dd.mm.yyyy");
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDateTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDateTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDateTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if ((int)tED_DE->Date != Cxdateutils::NullDate)
    return tED_DE->Text;
  else
    return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriDateTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  TDateTime FDate;
  if (TryStrToDate(tED_DE->Text,FDate)) return Variant(FDate);
  else                                  return Variant::Empty();

//         return Variant( tED_DE->Date.FormatString("'dd.mm.yyyy'"));
}
//---------------------------------------------------------------------------
bool __fastcall TriDateTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if ((int)tED_DE->Date == Cxdateutils::NullDate)  RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  tED_DE->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

