//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riChoiceBD.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define cLast        CBList->Count - 1
#define _CLASSCODE_  (__int64)((TStringList*)CB->Properties->Items->Objects[CB->ItemIndex])->Objects[0]
#define _CBCODE_     (__int64)CB->Properties->Items->Objects[CB->ItemIndex]
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TriChoiceBD::TriChoiceBD(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                  TRegEDItem(AOwner, AParent,ANode,AquFree,
                                             APref, AReqColor, ACtrlOwner,
                                             ASrc, IsApp)
{
//   this->Color = clGray;
   CBList = new TList;
   tmpUID = AVUp(FNode,"uid");
   tmpInt = 0;
   TTagNode *defNode;
   if (FNode->CmpName("class"))
    defNode = FNode;
   else
    defNode = FCtrlOwner->GetNode(AVUp(FNode,"ref"));
   if (defNode)
    defNode->ReIterateGroup(CreateClass,tmpUID);
   else
    ERegEDError(UnicodeString(__FUNC__)+" -> Error on create object");
   cBox(cLast)->Properties->OnChange = SetClassCode;
   int xW = 0;
   if (CBList->Count == 1)
    {
     cLab(0)->Caption = "  "+FNode->AV["name"]+":";
    }
   for (int i = 0; i <= cLast; i++)
    {
      if (xW < cLab(i)->Width)
       xW = cLab(i)->Width;
    }
   int xTop = 0;
   for (int i = 0; i <= cLast; i++)
    {
      cLab(i)->Top = xTop+1; cLab(i)->Left = 0;
      cBox(i)->Top = xTop;   cBox(i)->Left = xW+7;
      if (Required) cBox(i)->Style->Color = FReqColor;
      xTop += cBox(i)->Height-2;
      cBox(i)->Parent = this; cLab(i)->Parent = this;
      cLab(i)->FocusControl = cBox(i);
    }
   CMaxH = xTop;  Height = xTop+2;
   UnicodeString FDep,FDep2;
   FDep2 = FNode->AV["depend"].UpperCase();
   bool FCanFill = true;
   if (FDep2.Length() >= 9)
    {
      while (FDep2.Length())
       {
         FDep  = GetPart1(FDep2,';').Trim();
         if (!FDep.Length())
          FDep = FDep2;
         if (FDep.Length() == 9 && isRefMCtrl(_GUI(FDep)))
          {
             TTagNode *DepNode = FCtrlOwner->GetNode(_GUI(FDep));
             UnicodeString DepVal = RefMCtrl(_GUI(FDep))->GetValue(_GUI(FDep)+"."+DepNode->AV["ref"]);
             SetDependVal(DepNode->AV["ref"], DepVal.ToIntDef(-1), DepNode->AV["uid"]);
             FCanFill = false;
          }
         FDep2 = GetPart2B(FDep2,';').Trim();
       }
    }
   if (FCanFill)
    InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,"",FCtrlOwner->DefXML);
   cBox(0)->ItemIndex = -1;
   cBox(0)->Text = "";
   TStringList *TmplList = NULL;
   try
    {
      TmplList = new TStringList;
      if (IsTemplate(FNode))
       TmplList->Text = UnicodeString((wchar_t*)_TemplateValue(FNode)).Trim();
      for (int i = 0; i < CBList->Count; i++)
       {
          if ( i < TmplList->Count)
           ((TList*)CBList->Items[i])->Items[3] = (void*) 0;
          else
           ((TList*)CBList->Items[i])->Items[3] = (void*) (int)((!isDelegate) && isEdit && isEnabled);
       }
    }
   __finally
    {
      if (TmplList) delete TmplList;
    }
   if (IsAppend)
    {
      cBox(0)->Properties->OnChange((TObject*)cBox(0));
      if (IsTemplate(FNode))
       {
         try
          {
            TmplSetValue = true;
            SetChBDValue(_TemplateValue(FNode),false);
          }
         __finally
          {
            TmplSetValue = false;
          }
       }
    }
   else
    {
      UnicodeString  ClCode;
      if (SrcDS()->Active)
       {
         if (cLast > 0) GetClassCode(ClCode,irquAsInt(SrcDS(),FFlName),FNode);
         else           ClCode = irquAsStr(SrcDS(),FFlName)+"\n";
       }
      SetChBDValue(ClCode,isEnabled);
    }
//   if (cBox(0)->Width > 170) cBox(0)->Width = 170;
//   Width = xW+cBox(0)->Width+21;
   if (Parent->ClassNameIs("TriChoiceBD"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetOnDependValues()
{
  UnicodeString FDep,FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
     while (FDep2.Length())
      {
        FDep  = GetPart1(FDep2,';').Trim();
        if (!FDep.Length())
         FDep = FDep2;
        if (FDep.Length() == 9 && isRefMCtrl(_GUI(FDep)))
          RefMCtrl(_GUI(FDep))->SetOnValDepend(FNode);
        FDep2 = GetPart2B(FDep2,';').Trim();
      }
   }
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   irExecSelect("Select "+ARetFlName+" From "+ADepClName+" Where "+ADepFlVal, quFree);
   UnicodeString FRetVals = "";
   int FCount = 0;
   while (!irIsEof(quFree) && (FCount < 200))
    {
      FRetVals += irquAsStr(quFree,ARetFlName) + ",";
      FCount++;
      irquNext(quFree);
    }
   if (FCount >=199)
    {
      FRetVals = "Select "+ARetFlName+" From "+ADepClName+" Where "+ADepFlVal;
    }
   else
    {
      if (!FRetVals.Length()) FRetVals = "-1";
      else                    FRetVals[FRetVals.Length()] = ' ';
    }
   ARetVals = FRetVals;
   irExecClose(quFree,true);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (ANode->CmpName("choiceBD,class"))
    {
      UnicodeString DDD,xFrom;
      if (ANode->CmpName("class"))
       ANode->ReIterateGroup(getClassCode,DDD);
      else
       FCtrlOwner->GetNode(ANode->AV["ref"].UpperCase())->ReIterateGroup(getClassCode,DDD);
      TStringList *SSS = new TStringList;
      SSS->Text = DDD; DDD = ""; xFrom = "";
      for (int i = 0; i < SSS->Count; i++)
       {
         if (!DDD.Length()) DDD  += "CLASS_"+FTabPref+SSS->Strings[i]+".CODE as CODE"+IntToStr(i);
         else               DDD  += ", CLASS_"+FTabPref+SSS->Strings[i]+".CODE as CODE"+IntToStr(i);
         if (!xFrom.Length())       xFrom  += "CLASS_"+FTabPref+SSS->Strings[i];
         else
          if (i < SSS->Count-1) xFrom  += " join CLASS_"+FTabPref+SSS->Strings[i]+" on (CLASS_"+FTabPref+SSS->Strings[i-1]+".CODE=CLASS_"+FTabPref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+" )";
          else                  xFrom  += " join CLASS_"+FTabPref+SSS->Strings[i]+" on (CLASS_"+FTabPref+SSS->Strings[i-1]+".CODE=CLASS_"+FTabPref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+" and CLASS_"+FTabPref+SSS->Strings[i]+".CODE="+IntToStr(cCODE)+")";
       }
      irExecSelect("Select "+DDD+" From "+xFrom, quFree);
      UnicodeString XXX;
      for (int i = 0; i < SSS->Count; i++)
       {
         XXX = irquAsStr(quFree,"CODE"+IntToStr(i));
         if (!XXX.Length()) XXX = "-1";
         ACode += XXX+"\n";
       }
      if (SSS) delete SSS;
      SSS = NULL;
    }
}
//---------------------------------------------------------------------------
TLabel* __fastcall TriChoiceBD::cLab(int AIdx)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((TLabel*)((TList*)CBList->Items[AIdx])->Items[0]);
}
//---------------------------------------------------------------------------
TcxComboBox* __fastcall TriChoiceBD::cBox(int AIdx)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((TcxComboBox*)((TList*)CBList->Items[AIdx])->Items[1]);
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TriChoiceBD::cNode(int AIdx)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((TTagNode*)((TList*)CBList->Items[AIdx])->Items[2]);
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBD::cEnabled(int AIdx)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((bool)((TList*)CBList->Items[AIdx])->Items[3]);
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBD::getClassCode(TTagNode *itTag, UnicodeString &UID)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (itTag->CmpName("class"))
   if (!UID.Length())
    UID += AVUp(itTag,"uid");
   else
    UID += "\n"+AVUp(itTag,"uid");
  return false;
}
//---------------------------------------------------------------------------
__fastcall TriChoiceBD::~TriChoiceBD()
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   for (int i = 0; i <= cLast; i++)  cBox(i)->Properties->OnChange = NULL;
   for (int i = 0; i <= cLast; i++)  {ClearClassCBItems(cBox(i)->Properties->Items); delete cBox(i); delete cLab(i);}
   for (int i = 0; i < CBList->Count; i++)
    {
       if (((TList*)CBList->Items[i])) delete ((TList*)CBList->Items[i]);
       CBList->Items[i] = NULL;
    }
   CBList->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetEDLeft(int ALeft)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  for (int i = 0; i <= cLast; i++)
   {
     cBox(i)->Left = ALeft;
     cBox(i)->Width = Width - cBox(i)->Left - 5;
   }
//  Width = cBox(0)->Left+cBox(0)->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::GetEDLeft(int *ALabR, int *AEDLeft)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  *AEDLeft = cBox(0)->Left;
  *ALabR   = cBox(0)->Left-2;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::cResize(TObject *Sender)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     for (int i = 0; i <= cLast; i++)
      {
        cBox(i)->Width = Width - cBox(i)->Left - 5;
//        cBox(i)->Left = Width - cBox(i)->Width - 2;
        TNotifyEvent FSaveOnChange = cBox(i)->Properties->OnChange;
        cBox(i)->Properties->OnChange = NULL;
        int FSaveIndex = cBox(i)->ItemIndex;
        cBox(i)->ItemIndex = -1;
        cBox(i)->ItemIndex = FSaveIndex;
        cBox(i)->Properties->OnChange = FSaveOnChange;
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBD::CreateClass(TTagNode *itTag, UnicodeString &UID)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (itTag->CmpName("class"))
   {
     TLabel *tmpLab;
     TcxComboBox *tmpCB;
     CBList->Add(new TList);
     TList *DDD = (TList*)CBList->Items[CBList->Count-1];
     DDD->Add(new TLabel(Owner)); tmpLab = ((TLabel*)DDD->Items[DDD->Count-1]);
     tmpLab->Caption = "  "+itTag->GetChildByName("description")->AV["name"]+":";
     tmpLab->Transparent = true;
     tmpLab->Name = "LAB_"+itTag->AV["uid"]+UID;
     DDD->Add(new TcxComboBox(Owner)); tmpCB = ((TcxComboBox*)DDD->Items[DDD->Count-1]);

     tmpCB->Properties->DropDownListStyle = lsEditFixedList;
     tmpCB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
     tmpCB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
     tmpCB->Name = "CB_"+itTag->AV["uid"]+UID;
     tmpCB->Tag = tmpInt;
     tmpCB->Properties->OnChange = CBChange;
     tmpCB->Text = "";
     DDD->Add(itTag);
     DDD->Add((void*)1);
     tmpInt++;
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString FDep,FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
     UnicodeString FDepTab, FDepWhereVal, FRetFlName, FRetVals;
     UnicodeString FDepUID;
     TTagNode *FDepClDef, *FDepNode, *_tmp;
     FDepTab = "";
     bool IsEquDepend = true;
     while (FDep2.Length() && IsEquDepend)
      {
        FDep  = GetPart1(FDep2,';').Trim();
        if (!FDep.Length())
         FDep = FDep2;
        FDepUID = _UID(FDep);
        if (!FDepTab.Length())
         {
           FDepTab   = "CLASS_"+FTabPref+FDepUID;
           FDepClDef = FCtrlOwner->GetNode(FDepUID);
         }
        else
         {
           IsEquDepend &= (FDepTab == "CLASS_"+FTabPref+FDepUID);
         }
        FDep2 = GetPart2B(FDep2,';').Trim();
      }
     FDep2 = FNode->AV["depend"].UpperCase();
     UnicodeString FDepCode, FDepClCode;
     if (IsEquDepend)
      { // ��� ����������� � ����� �������, ����� ������� ����� where
        FRetVals = "";
        FDepWhereVal = "";
        if (ACode != -2)
         {
           while (FDep2.Length())
            {
              FDep  = GetPart1(FDep2,';').Trim();
              if (!FDep.Length())
               FDep = FDep2;
              if (_GUI(FDep) == ADepUID)
               {
                 FDepCode   = IntToStr(ACode);
                 FDepClCode = ADepClCode;
               }
              else
               {
                 _tmp = FCtrlOwner->GetNode(_GUI(FDep));
                 FDepClCode = _tmp->AV["ref"];
                 FDepCode   = FGetExtCtrl(_GUI(FDep))->GetValue("");
               }
              if (FDepWhereVal.Length())
               FDepWhereVal += " and ";

              FDepUID = _UID(FDep);
              FDepNode = FDepClDef->GetChildByAV("","ref",FDepClCode);
              FRetFlName = "CODE";
              if (FDepCode.ToIntDef(-1) == -1)
               {
                 FDepWhereVal += "(R"+FDepNode->AV["uid"]+" is NULL or R"+FDepNode->AV["uid"]+"=0)";
               }
              else
               {
                 if (FDepUID == FDepClCode)
                  {
                    FDepWhereVal += "CODE="+FDepCode;
                  }
                 else if (FDepUID == FNode->AV["ref"])
                  {
                    FDepWhereVal += "R"+FDepNode->AV["uid"]+"="+FDepCode;
                  }
                 else
                  {
                    FDepWhereVal += "R"+FDepNode->AV["uid"]+"="+FDepCode;
                    FRetFlName   = "R"+FDepClDef->GetChildByAV("","ref",FNode->AV["ref"])->AV["uid"];
                  }
               }
              FDep2 = GetPart2B(FDep2,';').Trim();
            }
           GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
         }
      }
     else
      {
        while (FDep2.Length())
         {
           FDep  = GetPart1(FDep2,';').Trim();
           if (!FDep.Length())
            FDep = FDep2;
           if (_GUI(FDep) == ADepUID)
            {
              FDepUID = _UID(FDep);
              FDepTab = "CLASS_"+FTabPref+FDepUID;
              FDepClDef = FCtrlOwner->GetNode(FDepUID);
              FDepNode = FDepClDef->GetChildByAV("","ref",ADepClCode);
              FRetVals = "";
              if (ACode != -2)
               {
                 if (ACode == -1)
                  {
                    FDepWhereVal = "(R"+FDepNode->AV["uid"]+" is NULL or R"+FDepNode->AV["uid"]+"=0)";
                    FRetFlName   = "CODE";
                  }
                 else
                  {
                    if (FDepUID == ADepClCode)
                     {
                       FDepWhereVal = "CODE="+IntToStr(ACode);
                     }
                    else if (FDepUID == FNode->AV["ref"])
                     {
                       FDepWhereVal = "R"+FDepNode->AV["uid"]+"="+IntToStr(ACode);
                       FRetFlName   = "CODE";
                     }
                    else
                     {
                       FDepWhereVal = "R"+FDepNode->AV["uid"]+"="+IntToStr(ACode);
                       FRetFlName   = "R"+FDepClDef->GetChildByAV("","ref",FNode->AV["ref"])->AV["uid"];
                     }
                  }
                 GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
               }
            }
           FDep2 = GetPart2B(FDep2,';').Trim();
         }
      }
     InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
     if (IsAppend)
      {
        if (cBox(0)->Properties->OnChange)
         cBox(0)->Properties->OnChange(cBox(0));
        if (cBox(0)->Properties->Items->Count > 1) cBox(0)->Text = "";
      }
     else
      riDataChange((TObject*)cBox(0));
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::CBChange(TObject *Sender)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  TcxComboBox *CB = (TcxComboBox*)Sender;
  if (CB->ItemIndex >= 0)
   {
     if ((CB->Tag+1) <= cLast)
      {
        bool tmpEnabled = isEnabled&&isEdit&&(!IsAppend || cEnabled(CB->Tag+1));
        cLab(CB->Tag+1)->Enabled = tmpEnabled;
        if (tmpEnabled)
         {
           if (Required) cBox(CB->Tag+1)->Style->Color = FReqColor;
           else          cBox(CB->Tag+1)->Style->Color = clWindow;
         }
        else
         cBox(CB->Tag+1)->Style->Color = clBtnFace;
        InvadeClassCB(cBox(CB->Tag+1),_CLASSCODE_, false, cNode(CB->Tag+1),quFree,FTabPref,"",FCtrlOwner->DefXML); //+++
        cBox(CB->Tag+1)->Enabled = tmpEnabled;
        cBox(CB->Tag+1)->Properties->OnChange((TObject*)cBox(CB->Tag+1));
      }
   }
  else
   {
     if ((CB->Tag+1) <= cLast)
      {
        cLab(CB->Tag+1)->Enabled = false;
        cBox(CB->Tag+1)->Enabled = false;
        cBox(CB->Tag+1)->Style->Color = clBtnFace;
//        if (isEdit&&cEnabled(CB->Tag+1))
//         cBox(CB->Tag+1)->Clear();
        cBox(CB->Tag+1)->Properties->OnChange((TObject*)cBox(CB->Tag+1));
      }
   }
  riDataChange((TObject*)CB);
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBD::UpdateChanges()
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  Variant vVal;
  vVal.Empty();
  for (int i = 0; i <= cLast; i++)
   {
     if (cBox(i)->ItemIndex >= 1)
      xVal += IntToStr((__int64)((TStringList*)cBox(i)->Properties->Items->Objects[cBox(i)->ItemIndex])->Objects[0])+"\n";
   }
  if (xVal.Length()) vVal = Variant(xVal);
  _TemplateValue(FNode) = vVal;
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceBD::GetValueStr(bool ABinaryNames)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  for (int i = 0; i <= cLast; i++)
   {
     if (cBox(i)->ItemIndex >= 1)
      xVal += " "+cBox(i)->Properties->Items->Strings[cBox(i)->ItemIndex];
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetIsTmpl(bool AVal)
{
  TmplSetValue = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetClassCode(TObject *Sender)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  TcxComboBox *CB = (TcxComboBox*)Sender;
  UnicodeString xFName = "R"+AVUp(FNode,"uid");
  if (SrcDSEditable())
   {
     if (TmplSetValue || CB->Enabled)
      {
        if (CB->ItemIndex >= 0) irquSetAsInt(SrcDS(),xFName,_CLASSCODE_);
        else                    irquSetAsNull(SrcDS(),xFName);//irquSetAsInt(SrcDS(),xFName,NULL);
      }
     else                       irquSetAsNull(SrcDS(),xFName);//irquSetAsInt(SrcDS(),xFName,NULL);
   }
  if (SetValList->Count > 0)
   {// ���� ����������� �� ������ � ��� ��� choiceBD ����������
     if (CB->ItemIndex >= 0)
      {
        UnicodeString FDepClCode = FNode->AV["ref"];
        for (int i = 0; i < SetValList->Count; i++)
         {
           depCtrl(i)->SetDependVal(FDepClCode, _CLASSCODE_, FNode->AV["uid"]);
         }
      }
     else
      {
        UnicodeString FDepClCode = FNode->AV["ref"];
        for (int i = 0; i < SetValList->Count; i++)
         {
           depCtrl(i)->SetDependVal(FDepClCode, -1, FNode->AV["uid"]);
         }
      }
   }
  riDataChange((TObject*)CB);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetIsVal(bool AEnable)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetEnable(bool AEnable)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  bool tmpEnabled;
  bool prevEnabled;
  for(int i = 0; i <= cLast; i++)
   {
     tmpEnabled = isEnabled&&isEdit&&(!IsAppend || cEnabled(i));
     if (!i)
      prevEnabled = tmpEnabled;
     else
      {
       if (prevEnabled)
        tmpEnabled &= (cBox(i)->ItemIndex != -1);
       prevEnabled |= tmpEnabled;
      }
     cBox(i)->Enabled = tmpEnabled;
     cBox(i)->Style->Color = (tmpEnabled)?((Required)? FReqColor : clWindow):clBtnFace;
     cLab(i)->Enabled = tmpEnabled;
     if (!AEnable && !tmpEnabled && isEdit&&(!IsAppend || cEnabled(i)))
      {
        cBox(i)->ItemIndex = -1;
        cBox(i)->Properties->OnChange(cBox(i));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetValue(UnicodeString AVal,bool AEnable)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//   SetEnable(AEnable);
   if (FNode->CmpName("choiceBD")) SetChBDValue(AVal,AEnable);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetLabel(UnicodeString xLab)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (CBList->Count == 1)
   cLab(0)->Caption = xLab;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetOnLab(TTagNode* ANode)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetLabList->Add(ANode);
  if (!IsAppend && SrcDS()->Active)
   {
     UnicodeString  ClCode;
     if (cLast > 0) GetClassCode(ClCode,irquAsInt(SrcDS(),"R"+AVUp(FNode,"uid")),FNode);
     else           ClCode = irquAsStr(SrcDS(),"R"+AVUp(FNode,"uid"))+"\n";
     SetChBDValue(ClCode,isEnabled);
   }
//  if (IsTemplate(FNode))
//   SetChBDValue(FNode->_TemplateValue,false);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetOnEnable(TTagNode* ANode)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetEnList->Add(ANode);
   if (FNode->CmpName("choiceBD"))
    {
      if (!IsAppend && SrcDS()->Active)
       {
         UnicodeString  ClCode;
         if (cLast > 0) GetClassCode(ClCode,irquAsInt(SrcDS(),"R"+AVUp(FNode,"uid")),FNode);
         else           ClCode = irquAsStr(SrcDS(),"R"+AVUp(FNode,"uid"))+"\n";
         SetChBDValue(ClCode,isEnabled);
       }
      if (IsTemplate(FNode))
       {
         for (int i = 0; i <= cLast; i++)
          {
            if (SetValList->Count > 0)
             {// ���� ����������� �� ������ � ��� ��� choiceBD ����������
               if (cBox(i)->ItemIndex >= 0)
                {
                  UnicodeString FDepClCode = FNode->AV["ref"];
                  for (int j = 0; j < SetValList->Count; j++)
                   {
                     depCtrl(j)->SetDependVal(FDepClCode, (__int64)((TStringList*)cBox(i)->Properties->Items->Objects[cBox(i)->ItemIndex])->Objects[0], FNode->AV["uid"]);
                   }
                }
             }
            riDataChange((TObject*)cBox(i));
          }
       }
    }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::riDataChange(TObject *Sender)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::FDataChange(TObject *Sender)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
     if (FNode->CmpName("choiceBD")) // ����� �� choiceBD
      {
        TcxComboBox *CB = (TcxComboBox*)Sender;
        xUID = CB->Name.SubString(4,4).UpperCase();
        TTagNode *xNode;
        TStringList *CBValues;
        if (CB->ItemIndex >= 0)  // ���� �������� ����, ��������� �����
         {
           CBValues = ((TStringList*)CB->Properties->Items->Objects[CB->ItemIndex]);
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xNode = ((TTagNode*)SetLabList->Items[i]);
              xRef = xNode->AV["depend"].UpperCase();     // ������ xxxx.yyyy
              // xxxx == uid.choiceBD yyyy == uid.��������������* ��� ref.choiceBD == uid.��������������
              if ((xRef.Length() == 9)&&(xRef.SubString(1,4) == FUID)&&(xRef.SubString(6,4) == xUID))
               {                                    // uid.��������������* - uid ����� ��� ������������ ��������������
                 xRef = "R"+AVUp(xNode,"ref");      // ref - uid ���� �������������� �������� �������� �������� ������
                 if (CBValues->IndexOfName(xRef) != -1)
                  {
                    xRef = CBValues->Values[xRef];
                    int iInd = xRef.Pos("_#_");
                    if (iInd) // ���� �� �������� �������� �������� ����� ��� "������"
                     xRef = xRef.SubString(iInd+3,xRef.Length()-iInd-2);
                    FGetExtCtrl(xNode->AV["uid"])->SetLabel(xRef);
                  }
               }
            }
         }
        else                 // ����� �������� �����
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xNode = ((TTagNode*)SetLabList->Items[i]);
              xRef = xNode->AV["depend"].UpperCase();
              if ((xRef.Length() == 9)&&(xRef.SubString(1,4) == FUID)&&(xRef.SubString(6,4) == xUID))
               FGetExtCtrl(xNode->AV["uid"])->SetLabel("");
            }
         }
      }
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::FDefaultDataChange(TObject *Sender)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceBD::GetControl(int AIdx)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   return (TControl*)cBox(AIdx);
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceBD::GetFirstEnabledControl()
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   for (int i = 0; i < CBList->Count; i++)
    {
      if (cBox(i)->Enabled&&isEdit)  return (TControl*)cBox(i);
    }
   return NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceBD::GetValue(UnicodeString ARef)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("choiceBD"))
    {
      UnicodeString clUID = _GUI(ARef);
      TcxComboBox *CB = NULL;
      if (!_UID(ARef).Length())
       {
         CB = cBox(cLast);
         if (CB->ItemIndex >= 0)
          return IntToStr(_CLASSCODE_);
         else
          return "";
       }
      for (int i = 0; i < CBList->Count; i++)
       {
         CB = cBox(i);
         if (CB->Name == "CB_"+_UID(ARef)+clUID)
          {
            if (CB->ItemIndex >= 0)
             return IntToStr(_CLASSCODE_);
            else
             return "";
          }
         if (CB->Name.SubString(4,4).UpperCase() == clUID) break;
         CB = NULL;
       }
      if (!CB) return "";
      TStringList *CBValues;
      if (CB->ItemIndex >= 0)
       {
         CBValues = ((TStringList*)CB->Properties->Items->Objects[CB->ItemIndex]);
         UnicodeString xRef = "R"+_UID(ARef);
         if (CBValues->IndexOfName(xRef) != -1)
          {
            xRef = CBValues->Values[xRef];
            int iInd = xRef.Pos("_#_");
            if (iInd) // ���� �� �������� �������� �������� ����� ��� "������"
             xRef = xRef.SubString(iInd+3,xRef.Length()-iInd-2);
            return xRef;
          }
       }
    }
   return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceBD::GetExtValue(UnicodeString ARef)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString RC = "";
  try
   {
     if (FNode->CmpName("choiceBD"))
      {
        TcxComboBox *CB = NULL;
        for (int i = 0; i < CBList->Count; i++)
         {
           CB = cBox(i);
           if (CB->ItemIndex >= 0)
            RC += IntToStr(_CLASSCODE_)+"\n";
           else
            break;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TStringList* __fastcall TriChoiceBD::GetStrings(UnicodeString AClRef)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("choiceBD"))
    {
      TcxComboBox *CB = NULL;
      for (int i = 0; i < CBList->Count; i++)
       {
         CB = cBox(i);
         if (CB->Name.SubString(4,4).UpperCase() == AClRef) break;
         CB = NULL;
       }
      if (CB) return (TStringList*)CB->Properties->Items;
    }
   return NULL;
}
//---------------------------------------------------------------------------
Variant __fastcall TriChoiceBD::GetCompValue(UnicodeString ARef)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");

  if ((ARef.Length())&&FNode->CmpName("choiceBD"))
   {
     TStringList *CBValues;
     for (int i = 0; i <= cLast; i++)
      {
        if (ARef.SubString(1,4) == cBox(i)->Name.SubString(4,4))
         {
           if (cBox(i)->ItemIndex >= 0)
            {
              CBValues = ((TStringList*)cBox(i)->Properties->Items->Objects[cBox(i)->ItemIndex]);
              UnicodeString __flName = "R"+ARef.SubString(6,4);
              if (CBValues->IndexOfName(__flName) != -1)
               {
                 __flName = CBValues->Values[__flName];
                 int iInd = __flName.Pos("_#_");
                 if (iInd)
                   __flName = __flName.SubString(1,iInd-1);
                 return Variant(__flName);
               }
            }
           else
            return Variant::Empty();
         }
      }
     return Variant::Empty();
   }
  else
   {
      ShowMessage(FMT1(icsRegEDErrorRefObject,ARef).c_str());
      return Variant::Empty();
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBD::SetChBDValue(UnicodeString AVal, bool AEnabled)
{
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TStringList *ValList = NULL;
   try
    {
      ValList = new TStringList;
      ValList->Text = AVal.Trim();
      while (ValList->Count > CBList->Count) // ����������� ���������� �������� � ��������
       ValList->Delete(ValList->Count-1);
      for (int i = 0; i < ValList->Count; i++)
       {
         for (int k = 0; k < cBox(i)->Properties->Items->Count; k++)
          {
            __int64 ItemCode = (__int64)((TStringList*)cBox(i)->Properties->Items->Objects[k])->Objects[0];
            if (ItemCode == (__int64)ValList->Strings[i].ToIntDef(-2))
             {
               cBox(i)->ItemIndex = k;
               cBox(i)->Properties->OnChange((TObject*)cBox(i));
               bool tmpEnabled = isEnabled&&isEdit&&(!IsAppend || cEnabled(i));
               if (!tmpEnabled)
                {
                  cLab(i)->Enabled = false;
                  cBox(i)->Enabled = false;
                  cBox(i)->Style->Color = clBtnFace;
                }
               break;
             }
          }
       }
    }
   __finally
    {
      if (ValList) delete ValList;
    }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBD::CheckReqValue(TcxPageControl *APC)
{
  if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     TWinControl *tmpCTRL = NULL;
     try
      {
        for (int i = 0; i <= cLast; i++)
         {
           if (cBox(i)->Enabled && (cBox(i)->ItemIndex < 0))
            {
              tmpCTRL = cBox(i);
              UnicodeString ErrMessage = FNode->AV["name"]+": ";
              ErrMessage += FCtrlOwner->GetNode(cBox(i)->Name.SubString(4,4).c_str())->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONINFORMATION);
              RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
           tmpCTRL->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

