//---------------------------------------------------------------------------

#ifndef RegSeparatorH
#define RegSeparatorH
//---------------------------------------------------------------------------
class PACKAGE TRegSeparator : public TPanel
{
private:
    TLabel *Lab;
    TBevel *ED;
public:
__published:
    __fastcall TRegSeparator::TRegSeparator (TComponent *AOwner, int AWidth, UnicodeString ACaption);
    __fastcall ~TRegSeparator();
};
//---------------------------------------------------------------------------
#endif
