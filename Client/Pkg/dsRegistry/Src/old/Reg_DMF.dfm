object icsREG_DM: TicsREG_DM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 457
  Width = 630
  object REG_DB: TpFIBDatabase
    DefaultTransaction = trDefault
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = []
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 29
    Top = 9
  end
  object quFree: TpFIBQuery
    Transaction = trFree
    Database = REG_DB
    Left = 179
    Top = 10
    qoTrimCharFields = True
  end
  object trDefault: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 26
    Top = 113
  end
  object trList: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 130
    Top = 62
  end
  object srcList: TDataSource
    DataSet = quList
    Left = 130
    Top = 113
  end
  object quReg: TpFIBDataSet
    AutoCalcFields = False
    Transaction = trReg
    Database = REG_DB
    DefaultFormats.DateTimeDisplayFormat = 'hh:mm  dd.mm.yyyy'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 85
    Top = 9
    poUseBooleanField = False
    poImportDefaultValues = False
    poGetOrderInfo = False
    poAskRecordCount = True
    poEmptyStrToNull = False
    oRefreshAfterPost = False
    oStartTransaction = False
    oAutoFormatFields = False
  end
  object trReg: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 79
    Top = 62
  end
  object srcReg: TDataSource
    DataSet = quReg
    Left = 79
    Top = 112
  end
  object trFree: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 179
    Top = 62
  end
  object quSearch: TpFIBQuery
    Transaction = trSearch
    Database = REG_DB
    Left = 279
    Top = 10
    qoTrimCharFields = True
  end
  object trSearch: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 279
    Top = 62
  end
  object xquFree: TpFIBQuery
    Transaction = xtrFree
    Database = REG_DB
    Left = 229
    Top = 10
    qoTrimCharFields = True
  end
  object xtrFree: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 229
    Top = 62
  end
  object quSelList: TpFIBDataSet
    UpdateRecordTypes = [cusUnmodified, cusModified, cusInserted, cusDeleted]
    AutoCalcFields = False
    Transaction = trSelList
    Database = REG_DB
    DefaultFormats.DateTimeDisplayFormat = 'hh:mm  dd.mm.yyyy'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 329
    Top = 10
    poUseBooleanField = False
    poImportDefaultValues = False
    poGetOrderInfo = False
    dcForceOpen = True
  end
  object trSelList: TpFIBTransaction
    DefaultDatabase = REG_DB
    Left = 329
    Top = 62
  end
  object srcSelList: TDataSource
    DataSet = quSelList
    Left = 329
    Top = 113
  end
  object srcAList: TDataSource
    Left = 155
    Top = 267
  end
  object srcAReg: TDataSource
    Left = 107
    Top = 267
  end
  object srcASelList: TDataSource
    Left = 347
    Top = 267
  end
  object quList: TpFIBDataSet
    Transaction = trList
    Database = REG_DB
    Left = 130
    Top = 11
    poUseBooleanField = False
    poImportDefaultValues = False
    poGetOrderInfo = False
    poAskRecordCount = True
    poEmptyStrToNull = False
    oRefreshAfterPost = False
    oStartTransaction = False
    oAutoFormatFields = False
  end
end
