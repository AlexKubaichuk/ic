//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SearchUnit.h"
#include "SearchSetting.h"
#include "irTreeClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
TSearchForm *SearchForm;
//---------------------------------------------------------------------------
__fastcall TSearchForm::TSearchForm(TComponent* Owner,UnicodeString ATabPref, TRegistry_ICS* ARegComp,
                                    TTagNode *ASearchDef, UnicodeString ATabName, UnicodeString ATabAlias, UnicodeString AExtWhere)
        : TForm(Owner)
{
  Top = 10;
  Left = Screen->Width/2 - Width;
  FUnitTableName = ATabName;
  FUnitTableAlias = ATabAlias;
//  TblName = AExtTabName;
  FExtWhere = AExtWhere;
  Caption = FMT(icsRegSearchCaption);
  SearchSettingBtn->Caption = FMT(icsRegSearchSettingBtnCaption);
  OkBtn->Caption = FMT(icsRegSearchFindBtnCaption);
  CancelBtn->Caption = FMT(icsRegSearchCancelBtnCaption);


  FTmpl = NULL;
  FExtSearch = false;
  RegComp  = ARegComp;
  HelpContext = RegComp->HelpContextList["reg_find"+RegComp->TablePrefix].ToIntDef(0);
  FTabPref = ATabPref;
  SearchED->Text = "";
  RegComp->DM->quSearch->Close();
  if (RegComp->DM->SearchList) delete[] RegComp->DM->SearchList;
  RegComp->DM->SearchList = NULL;
  RegComp->DM->SearchIndex = -1;
  RegComp->DM->SeachCount = 0;
  UnitNode = new TTagNode(NULL);
  UnitNode->Assign(ASearchDef->GetRoot(),true);
  DefNode = ASearchDef;

//  FUnitTableName = ATabName;
//  FUnitTableAlias = ATabAlias;

//  if (DefNode->CmpName("unit"))
//   FFullTN = FTabPref+DefNode->AV["tblname"];
//  else
//   FFullTN = "CLASS_"+FTabPref+DefNode->AV["uid"];

  FSearchOpt = new TSearchSettingOptions(RegComp->DM->quFree, FUnitTableName);
  FlCodeList = new TStringList;
  TTagNode *itNode;
  itNode = DefNode->GetChildByName("description")->GetFirstChild()->GetFirstChild();
  while (itNode)
   {
     if (itNode->GetTagByUID(itNode->AV["ref"]))
      {
        if (itNode->GetTagByUID(itNode->AV["ref"])->CmpName("text,digit"))
         {
           if (!SearchED->EditLabel->Caption.Length())
            SearchED->EditLabel->Caption = itNode->GetTagByUID(itNode->AV["ref"])->AV["name"];
           else
            SearchED->EditLabel->Caption = SearchED->EditLabel->Caption+" "+itNode->GetTagByUID(itNode->AV["ref"])->AV["name"];
           FlCodeList->Add(("R"+itNode->AV["ref"]).c_str());
         }
      }
     itNode = itNode->GetNext();
   }
  CreateExtFields();
  ExtBtn->Click();
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::SearchBtnClick(TObject *Sender)
{
   UnicodeString FTmplWhere = "";
//  FUnitTableName = ATabName;
//  FUnitTableAlias = ATabAlias;
/*
  if (FUnitTableAlias.Length())
   {
     FUnitTableName + " " + FUnitTableAlias;
     FUnitTableAlias += ".";
   }
*/
   if (FTmpl)
    {
      if (FUnitTableAlias.Length())
       FTmplWhere = FTmpl->GetWhere(FUnitTableAlias).Trim();
      else
       FTmplWhere = FTmpl->GetWhere(FUnitTableName).Trim();
    }
   if (!SearchED->Text.Trim().Length() &&  !FTmplWhere.Trim().Length())
    {
//      ActiveControl = SearchED;
      return;
    }
   int spInd;
   TStringList *sText = new TStringList;
   UnicodeString xSelect,xWhere,xOrder,SearchStr;
   try
    {
      SearchStr = SearchED->Text.UpperCase().Trim();
      if (FlCodeList->Count > 1)
       {
         spInd = SearchStr.Pos(" ");
         while (spInd)
          {
            sText->Add(SearchStr.SubString(1,spInd-1));
            SearchStr = SearchStr.SubString(spInd+1,SearchStr.Length()-spInd).Trim();
            spInd = SearchStr.Pos(" ");
          }
       }
      if (SearchStr.Trim().Length())
       sText->Add(SearchStr);
      while (sText->Count > FlCodeList->Count)
       sText->Delete(sText->Count-1);
      xSelect = ""; xWhere = ""; xOrder = "";
      for (int i = 0; i < sText->Count; i++)
       {
         if (i != 0) xWhere += " AND ";
         if (RegComp->UpperFunc.Length())
          xWhere  += RegComp->UpperFunc+"("+FlCodeList->Strings[i]+") Like '"+sText->Strings[i].UpperCase()+"%' ";
         else
          xWhere  += "U"+FlCodeList->Strings[i]+" Like '"+sText->Strings[i].UpperCase()+"%' ";
       }
    }
   __finally
    {
      if (sText) delete sText;
    }
   if (FTmplWhere.Length())
    {
      if (xWhere.Trim().Length())
       xWhere += " and "+FTmplWhere;
      else
       xWhere  = " "+FTmplWhere;
    }
   if (FExtWhere.Trim().Length())
    {
      if (xWhere.Trim().Length())
       xWhere += " and "+FExtWhere.Trim();
      else
       xWhere  = " "+FExtWhere.Trim();
    }
   xWhere = xWhere.Trim();
   if (xWhere.Length())
    xWhere = " Where "+xWhere;
   for (int i = 0; i < FlCodeList->Count; i++)
    {
      if (i == 0)
       {
         if (FUnitTableAlias.Length())
          xSelect = "Select "+FUnitTableAlias+".CODE, "+FlCodeList->Strings[i];
         else
          xSelect = "Select "+FUnitTableName+".CODE, "+FlCodeList->Strings[i];
         xOrder  = "Order By "+FlCodeList->Strings[i];
       }
      else
       {
         xOrder  += ", "+FlCodeList->Strings[i];
         if (!xSelect.Pos(FlCodeList->Strings[i]))
          xSelect += ", "+FlCodeList->Strings[i];
       }
    }
   RegComp->DM->quSearch->Close();
   RegComp->DM->quSearch->SQL->Clear();
   if (FUnitTableAlias.Length())
    {
      RegComp->DM->quSearch->SQL->Add("Select COUNT("+FUnitTableAlias+".CODE) AS MCODE ");
      RegComp->DM->quSearch->SQL->Add(("From "+FUnitTableName+" "+FUnitTableAlias).c_str());
    }
   else
    {
      RegComp->DM->quSearch->SQL->Add("Select COUNT("+FUnitTableName+".CODE) AS MCODE ");
      RegComp->DM->quSearch->SQL->Add(("From "+FUnitTableName).c_str());
    }
   RegComp->DM->quSearch->SQL->Add(xWhere.c_str());
   if (!RegComp->DM->trSearch->Active)  RegComp->DM->trSearch->StartTransaction();
   RegComp->DM->quSearch->Prepare();
   RegComp->DM->quSearch->ExecQuery();
   RegComp->DM->SeachCount = RegComp->DM->quSearch->FN("MCODE")->AsInteger;
   if (RegComp->DM->SeachCount > 0)
    {
      RegComp->DM->quSearch->Close();
      RegComp->DM->quSearch->SQL->Clear();
      RegComp->DM->quSearch->SQL->Add(xSelect.c_str());
      if (FUnitTableAlias.Length())
       RegComp->DM->quSearch->SQL->Add(("From "+FUnitTableName+" "+FUnitTableAlias).c_str());
      else
       RegComp->DM->quSearch->SQL->Add(("From "+FUnitTableName).c_str());
      RegComp->DM->quSearch->SQL->Add(xWhere.c_str());
      RegComp->DM->quSearch->SQL->Add(xOrder.c_str());
      if (!RegComp->DM->trSearch->Active)
       RegComp->DM->trSearch->StartTransaction();
      RegComp->DM->quSearch->Prepare();
      RegComp->DM->quSearch->ExecQuery();
      RegComp->DM->SearchList = new __int64[RegComp->DM->SeachCount];
      __int64 Ind = 0;
      while (!RegComp->DM->quSearch->Eof)
       {
         RegComp->DM->SearchList[Ind] = RegComp->DM->quSearch->FN("CODE")->AsInteger;
         RegComp->DM->quSearch->Next(); Ind++;
       }
      RegComp->DM->SearchIndex = 0;
      ModalResult = mrOk;
    }
   else
    {
      MessageBox(Handle,FMT(icsRegSearchNotFound).c_str(),FMT(icsMessageMsgCaption).c_str(),MB_ICONINFORMATION);
      if (RegComp->DM->SearchList) delete[] RegComp->DM->SearchList;
      RegComp->DM->SearchList = NULL;
      RegComp->DM->SearchIndex = -1;
      RegComp->DM->SeachCount = 0;
//      ActiveControl = SearchED;
//      SearchED->SelectAll();
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::SearchEDKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if (Key == VK_RETURN) SearchBtnClick(this);
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::FormDestroy(TObject *Sender)
{
  if (FTmpl) delete FTmpl;
  delete UnitNode;
  delete FSearchOpt;
  if (FlCodeList) delete FlCodeList;
  FlCodeList = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::CreateExtFields()
{
  FSTmplText = "";
  TTagNode *itNode;
  try
   {
     itNode = DefNode->GetChildByName("description")->GetFirstChild()->GetFirstChild();
     UnicodeString FSearchFields = "";
     while (itNode)
      {
        if (itNode->GetTagByUID(itNode->AV["ref"]))
         {
           if (itNode->GetTagByUID(itNode->AV["ref"])->CmpName("text,digit"))
            FSearchFields +=itNode->AV["ref"]+",";
         }
        itNode = itNode->GetNext();
      }
     if (DefNode)
      {
        if (DefNode->CmpName("class"))
         DefNode->Iterate1st(CreateElList,FSearchFields);
        else
         DefNode->Iterate(CreateElList,FSearchFields);
      }
     CompPanel->Visible = false;
     if (FTmpl) {delete FTmpl; FTmpl = NULL;}
     if (FSTmplText.Length())
      {
        ExtBtn->Enabled = true;
        FSTmplText = "<root>"+FSTmplText+"</root>";
        FTmpl = new TICSTemplate(FSTmplText,CompPanel,RegComp->DM->quFree,FTabPref,"",UnitNode,RegComp->RequiredColor,RegComp->NativeStyle,RegComp->LFKind);
        FTmpl->UpperFunc = RegComp->UpperFunc;
        FTmpl->OnTreeBtnClick = FOnTreeBtnClick;
        FTmpl->OnExtBtnClick = RegComp->OnExtBtnClick;
        FTmpl->OnGetExtWhere = RegComp->OnGetExtWhere;
      }
     else
      ExtBtn->Enabled = false;
     if (FTmpl)
      {
        if (FExtSearch)
         ClientHeight = 97+FTmpl->Height;
      }
     else
      ClientHeight = 97;
     CompPanel->Visible = true;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
bool __fastcall TSearchForm::CreateElList(TTagNode *itTag, UnicodeString &UID)
{
  TcxCheckListBoxItem *tmpItem;
  if (itTag->CmpName("text,binary,date,datetime,time,digit,choice,extedit,choiceBD,class,choiceTree"))
   {
     if (!itTag->CmpAV("uid",UID))
      {
        if (FSearchOpt->GetOpt(itTag->AV["uid"]))
         {
           if (itTag->CmpName("extedit") || (!itTag->CmpName("extedit")&&(itTag->CmpAV("inlist","list,list_ext")||itTag->CmpName("unitgroup"))))
            {
              if (itTag->CmpName("extedit") && !(itTag->AV["depend"].Length() && itTag->CmpAV("isedit","no")))
               {
                 if (RegComp->OnExtEditGetDependField)
                  FSTmplText += RegComp->OnExtEditGetDependField(itTag);
               }
              FSTmplText += "<fl ref='"+itTag->AV["uid"]+"'/>";
            }
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::SearchSettingBtnClick(TObject *Sender)
{
   TSearchSettingForm *Dlg = NULL;
   try
    {
      Dlg = new TSearchSettingForm(this,FTabPref,RegComp,DefNode);
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       {
         FSearchOpt->Load();
         CreateExtFields();
       }
    }
   __finally
    {
      if (Dlg) delete Dlg;
    }
}
//---------------------------------------------------------------------------
void __fastcall TSearchForm::ExtBtnClick(TObject *Sender)
{
  if (FTmpl)
   {
     ExtBtn->Caption = (FExtSearch)?">>":"<<";
     FExtSearch = !FExtSearch;
     if (FExtSearch)
      ClientHeight = 97+FTmpl->Height;
     else
      ClientHeight = 97;
   }
  else
   {
     ExtBtn->Caption = ">>";
     ClientHeight = 97;
     FExtSearch = false;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TSearchForm::FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked)
{
  bool RC = false;
  try
   {
     if (isClicked)
      {
        int clIndex = RegComp->DM->TreeClasses->IndexOf(ItTag->AV["uid"]);
        if (clIndex != -1)
         {
           TirTreeClassForm *Dlg = (TirTreeClassForm*)RegComp->DM->TreeClasses->Objects[clIndex];
           Dlg->Caption = ItTag->AV["dlgtitle"];
           Dlg->TemplateMode = true;
           Dlg->ShowModal();
           if (Dlg->ModalResult == mrOk)
            {
              RC = true;
              AIsGroup = Dlg->clSelectGroup;
              ACodeBeg = IntToStr(Dlg->clCode);
              ACodeEnd = IntToStr(Dlg->clCodeLast);
              AExtCode = Dlg->clExtCode;
              AText =    Dlg->clText;
            }
         }
        else
         {
           MessageBox(NULL,cFMT1(icsIErrTreeFormMsg,ItTag->AV["name"]),cFMT(icsErrorMsgCaption),MB_ICONSTOP);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

