object irTreeClassForm: TirTreeClassForm
  Left = 525
  Top = 232
  HelpContext = 3000
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeToolWin
  Caption = ':)'
  ClientHeight = 388
  ClientWidth = 568
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 570
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = True
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object ClassTree: TTreeView
    Left = 0
    Top = 25
    Width = 568
    Height = 306
    Align = alClient
    AutoExpand = True
    HideSelection = False
    HotTrack = True
    Indent = 19
    MultiSelectStyle = [msControlSelect, msShiftSelect]
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    OnChange = ClassTreeChange
    OnEditing = ClassTreeEditing
    ExplicitWidth = 554
    ExplicitHeight = 308
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 331
    Width = 568
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 333
    ExplicitWidth = 554
    DesignSize = (
      568
      38)
    object FindLab: TLabel
      Left = 11
      Top = 11
      Width = 35
      Height = 13
      Caption = #1055#1086#1080#1089#1082':'
    end
    object QSearchED: TcxButtonEdit
      Left = 51
      Top = 7
      Properties.Buttons = <
        item
          Default = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            04000000000080000000CE0E0000C40E00001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777777777777700000777770000070F000777770F00070F000777770F
            0007000000070000000700F000000F00000700F000700F00000700F000700F00
            00077000000000000077770F00070F0007777700000700000777777000777000
            77777770F07770F0777777700077700077777777777777777777}
          Kind = bkGlyph
        end>
      Properties.OnButtonClick = QSearchEDPropertiesButtonClick
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 342
    end
    object OkBtn: TcxButton
      Left = 399
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 1
      OnClick = BtnOkClick
    end
    object CancelBtn: TcxButton
      Left = 485
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 568
    Height = 25
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    ExplicitWidth = 554
    object SelGroupChB: TcxCheckBox
      Left = 3
      Top = 2
      Caption = #1042#1082#1083#1102#1095#1072#1103' &'#1074#1083#1086#1078#1077#1085#1085#1099#1077' '#1101#1083#1077#1084#1077#1085#1090#1099
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Visible = False
      Width = 374
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 369
    Width = 568
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = dxStatusBar1Container0
        Bevel = dxpbNone
        Width = 250
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    PaintStyle = stpsUseLookAndFeel
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ExplicitTop = 371
    ExplicitWidth = 554
    object dxStatusBar1Container0: TdxStatusBarContainerControl
      Left = 0
      Top = 2
      Width = 252
      Height = 17
      object PrBar: TcxProgressBar
        Left = 0
        Top = 0
        Align = alClient
        Style.BorderStyle = ebs3D
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        Visible = False
        Width = 252
      end
    end
  end
end
