//---------------------------------------------------------------------------

#ifndef riChoiceBDTmplH
#define riChoiceBDTmplH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
//#include "AxeUtil.h"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
#include <ADODB.hpp>
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3

//---------------------------------------------------------------------------
#include "RegEDContainer.h"
struct TriChoiceBDControls
{
  TTagNode *Node;
  TcxComboBox *CB;
  TLabel    *Lab;
};
//---------------------------------------------------------------------------
class PACKAGE TriChoiceBDTmpl: public TRegEDItem
{
private:
    bool     __fastcall CreateClass(TTagNode *itTag, UnicodeString &UID);
    void     __fastcall GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals);
    void     __fastcall CBChange(TObject *Sender);
    void     __fastcall GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode);
    void     __fastcall cResize(TObject *Sender);
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    void     __fastcall SetChBDValue(UnicodeString AVal, bool AEnabled);
    void     __fastcall FDefaultDataChange(TObject *Sender);
    void     __fastcall FDataChange(TObject *Sender);
    void     __fastcall SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID);
    void     __fastcall FSetEDRequired(bool AVal);
    TLabel*    __fastcall cLab(int AIdx);
    TcxComboBox* __fastcall cBox(int AIdx);
    TTagNode*  __fastcall cNode(int AIdx);
    TList     *FCtrlList;
    int        __fastcall cLast();
public:
    void         __fastcall SetOnDependValues();
    void         __fastcall riDataChange(TObject *Sender);
    bool         __fastcall getClassCode(TTagNode *itTag, UnicodeString &UID);
//    TStringList  *TreeClasses;
//    bool         TwoLine;
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetEnableEx(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString xLab);
    void         __fastcall SetOnLab(TTagNode* ANode);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
    TStringList* __fastcall GetStrings(UnicodeString AClRef);
__published:
    __fastcall TriChoiceBDTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                      UnicodeString APref,  TColor AReqColor,
                                      TRegEDContainer *ACtrlOwner,
                                      UnicodeString AVal);
    __fastcall ~TriChoiceBDTmpl();
};
//---------------------------------------------------------------------------
#endif
