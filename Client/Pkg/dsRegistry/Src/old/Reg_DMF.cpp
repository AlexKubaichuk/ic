//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "Reg_DMF.h"
#include "irTreeClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "FIBDatabase"
#pragma link "FIBDataSet"
#pragma link "FIBQuery"
#pragma link "pFIBDatabase"
#pragma link "pFIBDataSet"
#pragma link "pFIBQuery"
#pragma resource "*.dfm"

TicsREG_DM *icsREG_DM;
//---------------------------------------------------------------------------
__fastcall TicsREG_DM::TicsREG_DM(TComponent* Owner)
    : TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TicsREG_DM::DataModuleCreate(TObject *Sender)
{
  Classes = NULL;
  SearchIndex = -1;
  SeachCount = 0;
  SearchList = NULL;
  UnitList = NULL;
  RegUnit = NULL;
  SelRegUnit = NULL;
  Reg = NULL;
  TreeClasses = new TStringList;
  DTFormats[0] = FMT(icsDTFormatsD);
  DTFormats[1] = FMT(icsDTFormatsDT);
  DTFormats[2] = FMT(icsDTFormatsT);
}
//---------------------------------------------------------------------------
void __fastcall TicsREG_DM::DataModuleDestroy(TObject *Sender)
{
  delete FClassData;
  if (RegUnit)   delete RegUnit;
  RegUnit = NULL;
  if (SelRegUnit)   delete SelRegUnit;
  SelRegUnit = NULL;
  if (REG_DB->Connected) REG_DB->Close();
  if (SearchList) delete[] SearchList;
  SearchList = NULL;
  if (TreeClasses)
   {
     for (int i = 0;i< TreeClasses->Count; i++)
      {
        if (TreeClasses->Objects[i]) delete (TirTreeClassForm*)TreeClasses->Objects[i];
        TreeClasses->Objects[i] = NULL;
      }
     delete TreeClasses;
   }
  TreeClasses = NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall CorrectCount(int ACount)
{
 UnicodeString RC;
 RC = IntToStr(ACount);
 int xpCount = StrToInt(RC[RC.Length()]);
 RC = IntToStr(ACount)+" ";
 if ((ACount == 0)||((ACount > 4)&&(ACount < 21))||((ACount > 20)&&(xpCount > 4)))
  RC += FMT(icsRecCount3);
 else
  {
    if (xpCount == 1)                    RC += FMT(icsRecCount1);
    else if (xpCount > 1 && xpCount < 5) RC += FMT(icsRecCount2);
    else                                 RC += FMT(icsRecCount1);
  }
 return RC;
}
//---------------------------------------------------------------------------
void __fastcall TicsREG_DM::qtExec(UnicodeString ASQL, bool aCommit)
{
   if (!quFree->Transaction->Active) quFree->Transaction->StartTransaction();
   quFree->Close();
   quFree->SQL->Clear();
   quFree->SQL->Text = ASQL;
   quFree->Prepare();
   quFree->ExecQuery();
   if (aCommit) quFree->Transaction->Commit();
}
//---------------------------------------------------------------------------
void __fastcall TicsREG_DM::qtStart(UnicodeString ASQL)
{
   if (!quFree->Transaction->Active) quFree->Transaction->StartTransaction();
   quFree->Close();
   quFree->SQL->Clear();
   quFree->SQL->Text = ASQL;
}
//---------------------------------------------------------------------------
void __fastcall TicsREG_DM::qtExecCommit()
{
   quFree->Prepare();
   quFree->ExecQuery();
   if (quFree->Transaction->Active) quFree->Transaction->Commit();
}
//---------------------------------------------------------------------------
void __fastcall TicsREG_DM::qtRollback()
{
   if (quFree->Transaction->Active) quFree->Transaction->Rollback();
}
//---------------------------------------------------------------------------
__int64 __fastcall TicsREG_DM::GetMCode(UnicodeString AGenName)
{
   __int64 RC = 0;
   try
    {
      quFree->Close();
      if (!trFree->Active) trFree->StartTransaction();
      quFree->ExecProcedure(AGenName.c_str());
      RC = quFree->FN("MCODE")->AsInteger;
      trFree->Commit();
    }
   __finally
    {
    }
      return RC;
}
//---------------------------------------------------------------------------
#include <rpc.h>
UnicodeString __fastcall TicsREG_DM::NewGUID()
{
  UnicodeString RC = "";
  try
   {
     UUID FGUI;
     if (UuidCreate(&FGUI) == RPC_S_OK)
      {
        unsigned short * strGUID[1];
        if (UuidToString(&FGUI,strGUID) == RPC_S_OK)
         RC = UnicodeString(AnsiString((char*)(strGUID[0])));
      }
   }
  __finally
   {
   }
  if (RC.Trim().Length())
   return RC.Trim().UpperCase();
  else
   throw Exception("������ ������������ GUID");
}
//---------------------------------------------------------------------------

