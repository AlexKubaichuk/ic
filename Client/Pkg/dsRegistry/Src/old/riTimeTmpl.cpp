//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riTimeTmpl.h"

//---------------------------------------------------------------------------

//#include "TreeClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define dbED         ((TDBEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED         ((TEdit*)ED)
#define tED2        ((TEdit*)ED2)
//---------------------------------------------------------------------------
__fastcall TriTimeTmpl::TriTimeTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
  Lab = new TLabel(this);  Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Top = 2;            Lab->Left = 4;
  Lab->Caption = FNode->AV["name"]+":";
  ED = new TEdit(this);
  xSize = Rect(0,0,70,21);
  ED->Constraints->MinWidth = 60;
  if ((xSize.Right+Lab->Width+Lab->Left+10) >= 255)
   {
     TwoLine = true;  Lab->Top = 0;
     ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
     CMaxH = 34; CMinH = 34;
     Height = CMaxH;
   }
  else
   {
     ED->Left = Lab->Width+5;
     ED->Top = xSize.Top;
   }
  ED->Width = xSize.Right+10;
  ED->Height = xSize.Bottom;
  ED->Parent =  this;
  if (!FCtrlOwner->TemplateField && !_TemplateValue(FNode).IsEmpty())
   {
     if (!isFV)
      tED->Text = UnicodeString((wchar_t*)_TemplateValue(FNode));
   }
  Width = ED->Left+ED->Width;
  Lab->FocusControl = ED;
  if (Parent->ClassNameIs("TriTimeTmpl"))
   {
     Anchors.Clear();
     Anchors << akLeft << akRight << akTop;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
  tED->OnChange    = riDataChange;
}
//---------------------------------------------------------------------------
__fastcall TriTimeTmpl::~TriTimeTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   tED->OnChange    = NULL;
   delete tED;
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  if (FNode->CmpName("digit,date")&&TmpCtrl)     ED2->Left = ALeft;
  if (ED)                                Width = ED->Left+ED->Width+4;
  if (TmpCtrl)
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4+ED2->Width;
  else
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED&&(!FNode->CmpName("binary")))  *AEDLeft = ED->Left;
  else                                            *AEDLeft = -1;
  if (!TwoLine&&Lab&&(!FNode->CmpName("binary"))) *ALabR = Lab->Left+Lab->Width;
  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (FNode->CmpName("digit,time"))
      {
        if (TmpCtrl)
         {
           if (TwoLine) {ED->Left = 2; ED->Width = Width-4;ED2->Left = 2; ED2->Width = Width-4;}
           else         {ED->Left = Width - ED->Width - 2; ED2->Left = Width - ED2->Width - 2;}
         }
        else
         {
           if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
           else         {ED->Left = Width - ED->Width - 2;}
         }
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriTimeTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FNode->CmpName("text,datetime,time,digit")&&!isFV) tED->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriTimeTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->CmpName("text,datetime,time,digit,date,choice,binary,choiceTree"))
   {
     if (ED)
      {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (ED->Enabled)
         {
           if (!isFV)
            {
              if (tED->Text.Length())            xVal += tED->Text;
            }
         }
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Lab->Enabled = AEnable;
  ED->Enabled = AEnable;
  tED->Color = (AEnable)?((Required)? FReqColor:clWindow):clBtnFace;
  if (!AEnable)
   tED->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (!isFV)  tED->Text = AVal;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetEnList->Add(ANode);
   if (FNode->CmpName("text,datetime,time,digit")&&!isFV)
    {
      if (TmpCtrl) tED->OnChange  = riDataChange;
      else         dbED->OnChange = riDataChange;
    }
    riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled||isVal)
      {
           if (tED->Text.Length())            xVal = Variant(tED->Text);
      }
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (tED->Text.Trim().Length())     Src = tED->Text.Trim();
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (tED->Text.Trim().Length())     Src = tED->Text.Trim();
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriTimeTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,date,datetime,time,digit,extedit,binary,choice,choiceTree"))
    return (TControl*)ED;
   else
    return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriTimeTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriTimeTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,datetime,time"))
    {
      if (TmpCtrl)
       return tED->Text;
      else
       return dbED->Text;
    }
   return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriTimeTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  TDateTime FTime;
  if (TryStrToTime(tED->Text,FTime)) return Variant(FTime);
  else                               return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriTimeTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (FNode->CmpName("text,datetime,time,digit"))
               {
                 if (!dbED->Text.Length())  RC = false;
               }
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriTimeTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  tED->Color = (tED->Enabled)?((Required)? FReqColor:clWindow):clBtnFace;
}
//---------------------------------------------------------------------------

