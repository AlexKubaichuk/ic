//---------------------------------------------------------------------------
#ifndef SearchSettingH
#define SearchSettingH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include "AxeUtil.h"
#include "Reg_DMF.h"
#include "Registry_ICScomp.h"
/*#include <Classes.hpp>
#include "cxButtons.hpp"
#include "cxCheckListBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include <Registry.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cxButtons.hpp"
#include "cxCheckListBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include "pFIBQuery.hpp"
#include "dxSkinXmas2008Blue.hpp"  */
//---------------------------------------------------------------------------
typedef map<UnicodeString, bool> TBoolMap;
typedef map<UnicodeString, TcxTreeListNode*> TTLNodeMap;
//---------------------------------------------------------------------------
class PACKAGE TSearchSettingOptions : public TObject
{
private:	// User declarations
        TBoolMap FOpt;
        TpFIBQuery *FQuery;
        UnicodeString FGrName;
public:		// User declarations
        __fastcall TSearchSettingOptions(TpFIBQuery *AQuery, UnicodeString AGrName);
        virtual __fastcall ~TSearchSettingOptions();
        bool __fastcall GetOpt(UnicodeString AId, bool ADef = false);
        void __fastcall SetOpt(UnicodeString AId, bool AVal);
        void __fastcall Load();
        void __fastcall Save();
};
//---------------------------------------------------------------------------
class PACKAGE TSearchSettingForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TPanel *CompPanel;
        TcxLabel *FieldListLab;
        TcxTreeList *sElemenList;
        TcxTreeListColumn *NameCol;
        TcxTreeListColumn *ValCol;
        TcxTreeListColumn *IDCol;
        TcxTreeListColumn *CanEditCol;
        TcxStyleRepository *cxStyleRepository1;
        TcxStyle *headStyle;
        TcxCheckBox *SetValChB;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall sElemenListKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ValColGetEditProperties(TcxTreeListColumn *Sender,
          TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
  void __fastcall sElemenListStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
  void __fastcall sElemenListEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
private:	// User declarations
        void __fastcall NodeToTreeNode(TTagNode *ASrc, TcxTreeListNode *ADest, UnicodeString ASearchFields);
        bool __fastcall CheckInput();
        bool __fastcall CheckNode(UnicodeString UID);
        bool __fastcall GetNodeValue(UnicodeString UID, UnicodeString AName, bool ASilent = false);
        TSearchSettingOptions *FSearchOpt;
        TTLNodeMap     TreeNodes;
        TRegistry_ICS* FRegComp;
        UnicodeString FGroupName;
        TTagNode    *FDef;
public:		// User declarations
        __fastcall TSearchSettingForm(TComponent* Owner, UnicodeString ATabPref,
                                      TRegistry_ICS* ARegComp, TTagNode *ASearchDef);
};
//---------------------------------------------------------------------------
extern PACKAGE TSearchSettingForm *SearchSettingForm;
//---------------------------------------------------------------------------
#endif
