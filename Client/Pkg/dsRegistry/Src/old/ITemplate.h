//---------------------------------------------------------------------------
#ifndef ITemplateH
#define ITemplateH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include "AxeUtil.h"
#include "ICSTemplate.h"
#include "Registry_ICScomp.h"
/*#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
//---------------------------------------------------------------------------
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"     */
//---------------------------------------------------------------------------
class PACKAGE TITemplateForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TBevel *BtnBevel;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall CancelBtnClick(TObject *Sender);
private:
        TICSTemplate *Tmpl;
        TTagNode *FRoot;
        TTagNode *FUnitNode;
        TStringList *FTreeFormList;
        bool  __fastcall FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked);
        bool __fastcall SetcValue(TTagNode *itTag, UnicodeString &UID);
public:		// User declarations
        UnicodeString  __fastcall GetWhere(UnicodeString ATabAlias = "");
        UnicodeString  __fastcall GetSelectedText(bool CanField = false);
        void  __fastcall Clear();
        bool  __fastcall IsEmpty();
        __fastcall TITemplateForm(TComponent* Owner,TRegistry_ICS* ARegComp, UnicodeString ATemplXML);
};
//---------------------------------------------------------------------------
extern PACKAGE TITemplateForm *ITemplateForm;
//---------------------------------------------------------------------------
#endif
