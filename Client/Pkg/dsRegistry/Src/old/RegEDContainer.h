//---------------------------------------------------------------------------

#ifndef RegEDContainerH
#define RegEDContainerH
//---------------------------------------------------------------------------
//#include <Classes.hpp>
#include <DB.hpp>
//#include <DBCtrls.hpp>
//#include "AxeUtil.h"
//#include "FIBDataSet.hpp"
//#include "FIBQuery.hpp"
//#include "pFIBDataSet.hpp"
//#include "pFIBQuery.hpp"
//#include <ADODB.hpp>
#include "icsRegConstDef.h"

//---------------------------------------------------------------------------
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxCheckBox.hpp"
#include "cxCalendar.hpp"
#include "cxDBEdit.hpp"
#include "cxGroupBox.hpp"
//---------------------------------------------------------------------------
#include "RegEDFunc.h"
#include "ICSClassData.h"
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class PACKAGE ERegEDContainerError : public Exception
{
public:
     __fastcall ERegEDContainerError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ERegEDContainerDSError : public Exception
{
public:
     __fastcall ERegEDContainerDSError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ERegEDError : public Exception
{
public:
     __fastcall ERegEDError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ERegEDTemplateError : public Exception
{
public:
     __fastcall ERegEDTemplateError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class TRegEDContainer;
class PACKAGE TRegEDItem : public TPanel //TcxGroupBox
{
private:
protected:
    TRegEDContainer *FCtrlOwner;
    TDataSet     *quFree;
    TDataSource  *FSrc;
    TExtBtnClick  FExtBtnClick;
    TTreeBtnClick FTreeBtnClick;
    TGetExtData   FGetDataChange;
    TExtBtnGetBitmapEvent  FExtBtnGetBitmap;
    TRegGetFormatEvent  FOnGetFormat;


    TDataSet*    __fastcall SrcDS();
    bool         __fastcall SrcDSEditable();

    virtual void     __fastcall GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode);
    virtual void     __fastcall cResize(TObject *Sender);
    bool     __fastcall Condition(TTagNode *CondNode);
    virtual Variant  __fastcall GetCompValue(UnicodeString ARef);
    void     __fastcall CalcParam(TTagNode *AFirst,Variant* AParam);
    virtual void     __fastcall FDefaultDataChange(TObject *Sender);
    virtual void     __fastcall FDataChange(TObject *Sender);
    virtual void     __fastcall FSetExtBtnClick(TExtBtnClick AExtBtnClick);
    virtual void     __fastcall FSetTreeBtnClick(TTreeBtnClick ATreeBtnClick);

    bool     __fastcall isRefCtrl(TTagNode *ANode);
    bool     __fastcall isRefMCtrl(UnicodeString ADepend);
    TList      *CBList;
    TList      *SetLabList;
    TList      *SetValList;
    TList      *SetEnList;
    TColor     FReqColor;
    UnicodeString FFlName,tmpUID,FTabPref;
    int        tmpInt;
    TRegEDItem* __fastcall RefCtrl(TTagNode* ARef);
    TRegEDItem* __fastcall RefMCtrl(UnicodeString ADepend);
    TRegEDItem* __fastcall labCtrl(int AIdx);
    TRegEDItem* __fastcall depCtrl(int AIdx);
    TRegEDItem* __fastcall FGetExtCtrl(UnicodeString AUID);
    virtual void __fastcall FSetEDRequired(bool AVal);
    bool       IsAlign, IsAppend, FRequired, TmpCtrl, is3D, isTemplate, isEnabled,isVal, isFV, isEdit, isDelegate;
    TTagNode     *FNode;
    TLabel       *Lab;
    TWinControl  *ED;
    TWinControl  *ED2;
    void         __fastcall FSetOnDependValues();
public:
    TStringList  *TreeClasses;
    bool         TwoLine;
    virtual void         __fastcall SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID);
    virtual void         __fastcall SetOnDependValues();
    virtual void         __fastcall SetOnLab(TTagNode* ANode);
    virtual void         __fastcall riDataChange(TObject *Sender);
    bool                 __fastcall getClassCode(TTagNode *itTag, UnicodeString &UID);
    virtual UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    virtual TStringList* __fastcall GetStrings(UnicodeString AClRef);
    virtual TControl*    __fastcall GetControl(int AIdx = 0);
    virtual TControl*    __fastcall GetFirstEnabledControl();
    virtual void         __fastcall SetEnable(bool AEnable);
    virtual void         __fastcall SetEnableEx(bool AEnable);
    virtual void         __fastcall SetIsVal(bool AEnable);
    virtual void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    virtual void         __fastcall SetLabel(UnicodeString xLab);
    virtual void         __fastcall SetOnValDepend(TTagNode* ANode);
    virtual void         __fastcall SetOnEnable(TTagNode* ANode);
    virtual void         __fastcall SetEDLeft(int ALeft);
    virtual bool         __fastcall UpdateChanges();
    virtual bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL);
    virtual void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    virtual UnicodeString   __fastcall GetValue(UnicodeString ARef);
    virtual UnicodeString   __fastcall GetExtValue(UnicodeString ARef);
    virtual void         __fastcall SetFixedVar(UnicodeString AVals);
    UnicodeString           __fastcall GetPart2B(UnicodeString ARef, char Sep = '.');
__published:
    __property TTagNode*     DefNode        = {read=FNode};

    __property bool Required  = {read=FRequired, write=FSetEDRequired};

    __property TExtBtnClick  OnExtBtnClick  = {read=FExtBtnClick, write=FSetExtBtnClick};
    __property TTreeBtnClick OnTreeBtnClick = {read=FTreeBtnClick, write=FSetTreeBtnClick};
    __property TGetExtData   OnDataChange   = {read=FGetDataChange, write=FGetDataChange};
    __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

    __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

    // DB
    __fastcall TRegEDItem::TRegEDItem(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                      UnicodeString APref, TColor AReqColor,
                                      TRegEDContainer *ACtrlOwner,
                                      TDataSource* ASrc, bool IsApp);
    // Template
    __fastcall TRegEDItem::TRegEDItem(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                      UnicodeString APref,  TColor AReqColor,
                                      TRegEDContainer *ACtrlOwner,
                                      UnicodeString AVal);
    virtual __fastcall ~TRegEDItem();
};
//---------------------------------------------------------------------------
class PACKAGE ERegLabError : public Exception
{
public:
     __fastcall ERegLabError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TRegLabItem : public TPanel
{
private:
    TRegEDContainer *FCtrlOwner;
    TDataSet    *quFree;
    TDataSource *FSrc;

    TGetExtData FGetExtData;

    UnicodeString FFlName,tmpUID,FTabPref;
//    TStringList  *CBList;
    TList  *SetLabList;
    TList  *SetEnList;
    bool   TwoLine;
    bool   isTmpVal;
    int    tmpInt;

    bool         __fastcall Condition(TTagNode *CondNode);
    Variant      __fastcall GetCompValue(UnicodeString ARef);
    void         __fastcall CalcParam(TTagNode *AFirst,Variant* AParam);
    UnicodeString   __fastcall GetClValue(UnicodeString ARef);
    UnicodeString   __fastcall GetExtData();
    bool         __fastcall isRefLabCtrl(TTagNode *ANode);
    bool         __fastcall isRefMLabCtrl(TTagNode *ANode);
    TRegLabItem* __fastcall FGetExtCtrl(UnicodeString AUID);
    TRegLabItem* __fastcall RefLabCtrl(TTagNode* ARef);
    TRegLabItem* __fastcall RefMLabCtrl(TTagNode* ARef);
    TRegLabItem* __fastcall labLabCtrl(int AIdx);
    TDataSet*    __fastcall SrcDS();
    bool         __fastcall SrcDSEditable();
    void         __fastcall FOnLabResize(TObject *Sender);
public:
    TLabel      *Lab;
    TControl *ED;
    TTagNode    *FNode;
    void __fastcall SetOnDependValues();
    void __fastcall riDataChange();
    void __fastcall SetEnable(bool AEnable);
    void __fastcall SetLabel(UnicodeString xLab);
    void __fastcall SetOnLab(TTagNode* ANode);
    void __fastcall SetOnEnable(TTagNode* ANode);
__published:
    __property TGetExtData OnGetExtData  = {read=FGetExtData, write=FGetExtData};

    __fastcall TRegLabItem::TRegLabItem (TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                         UnicodeString APref,
                                         TRegEDContainer *ACtrlOwner,
                                         TDataSource* ASrc, int AWidth);
    __fastcall ~TRegLabItem();
};
//---------------------------------------------------------------------------
typedef  map<UnicodeString, TRegEDItem*>  TRegEDMap;
typedef  map<UnicodeString, TRegLabItem*> TRegLabMap;
//---------------------------------------------------------------------------
class PACKAGE TRegEDContainer : public TObject
{
private:
    bool FADOSrc;
    bool FNativeStyle;
    TcxLookAndFeelKind FLFKind;

    TComponent  *FOwner;
    TWinControl *FPlaceCtrl;
    TGetExtData   FCtrlDataChange;

    TDataSet    *FTmplDS;
    TDataSource *FDataSrc;

    TRegEDMap   FEditMap;        // ������ RegED
    TRegEDMap   FTmplEditMap;    // ������ template RegED
    TRegLabMap  FLabMap;         // ������ RegLab
    TGetCompValue FGetCompValue;
    TExtBtnGetBitmapEvent  FExtBtnGetBitmap;

    TTagNode     *FDefXML;
    TAxeXMLContainer *FXMLList;
    TRegGetFormatEvent  FOnGetFormat;

    TColor      FReqColor;
    UnicodeString  FTablePref;

    TRegEDItem*  __fastcall FGetEditItems(UnicodeString AUID);
    TRegEDItem*  __fastcall FGetTemplateItems(UnicodeString AUID);
    TRegLabItem* __fastcall FGetLabItems(UnicodeString AUID);

    int __fastcall FGetEditCount();
    int __fastcall FGetTemplateEditCountCount();
    int __fastcall FGetLabCount();

    TpFIBQuery*  __fastcall FGetFIBTemplateQuery();
    void __fastcall FSetFIBTemplateQuery(TpFIBQuery* ATmplSrc);

    TADODataSet* __fastcall FGetADOTemplateQuery();
    void __fastcall FSetADOTemplateQuery(TADODataSet* ATmplSrc);
    TICSClassData *FClassData;
    bool FIsTemplateField;
//    TClassHashTable *FClassHash;
public:
    TRegEDItem*  __fastcall AddEditItem(TTagNode* ANode, bool AIsApp,TWinControl *AParent = NULL);
    TRegEDItem*  __fastcall AddTemplateItem(TTagNode* ANode, UnicodeString AVal,TWinControl *AParent = NULL);
    TRegLabItem* __fastcall AddLabItem(TTagNode* ANode, int AWidth,TWinControl *AParent = NULL);

    void __fastcall ReiterateClear(TTagNode *ANode, TRegEDMap AMap);
    void __fastcall ClearEditItems();
    void __fastcall ClearTemplateItems();
    void __fastcall ClearLabItems();

    void __fastcall EditDataChange();
    void __fastcall TemplateDataChange();
    void __fastcall LabDataChange();

    TTagNode*  __fastcall GetRef(UnicodeString ARef);
    TTagNode*  __fastcall GetNode(UnicodeString ARef);

    TRegEDItem*  __fastcall GetEDControl(UnicodeString AUID);
    TRegEDItem*  __fastcall GetTemplateControl(UnicodeString AUID);
    TRegLabItem* __fastcall GetLabControl(UnicodeString AUID);
    UnicodeString __fastcall GetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString ARef = "");
    void __fastcall SetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString AValue);

    void __fastcall SetOnCtrlDataChange(int AType);
    void __fastcall ClearOnCtrlDataChange(int AType);
//    __property TClassHashTable *ClassHash = {read=FClassHash, write=FClassHash};

__published:
    __property TTagNode* DefXML = {read=FDefXML, write=FDefXML};
    __property TAxeXMLContainer *XMLList = {read=FXMLList, write=FXMLList};
    __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

    __property TICSClassData *ClassData = {read=FClassData, write=FClassData};

    __property TGetExtData   OnDataChange   = {read=FCtrlDataChange, write=FCtrlDataChange};
    __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

    __property TColor ReqColor = {read=FReqColor, write=FReqColor};
    __property UnicodeString TablePref = {read=FTablePref, write=FTablePref};
    __property bool ADOSrc = {read=FADOSrc, write=FADOSrc};

    __property int EditCount = {read=FGetEditCount};
    __property int TemplateEditCount = {read=FGetTemplateEditCountCount};
    __property int LabCount = {read=FGetLabCount};
    __property TGetCompValue OnGetCompValue = {read=FGetCompValue, write=FGetCompValue};

    __property TRegEDItem*  EditItems[UnicodeString AUID]     = {read=FGetEditItems};
    __property TRegEDItem*  TemplateItems[UnicodeString AUID] = {read=FGetTemplateItems};
    __property TRegLabItem* LabItems[UnicodeString AUID]      = {read=FGetLabItems};

    __property TDataSource* DataSource = {read=FDataSrc, write=FDataSrc};
    __property TpFIBQuery*  FIBTemplateQuery = {read=FGetFIBTemplateQuery, write=FSetFIBTemplateQuery};
    __property TADODataSet* ADOTemplateQuery = {read=FGetADOTemplateQuery, write=FSetADOTemplateQuery};

    __property bool NativeStyle = {read=FNativeStyle, write=FNativeStyle};
    __property TcxLookAndFeelKind LFKind= {read=FLFKind, write=FLFKind};

    __property bool TemplateField = {read=FIsTemplateField, write=FIsTemplateField};

    __fastcall TRegEDContainer(TComponent *AOwner,TWinControl *AParent);
    __fastcall ~TRegEDContainer();
};
//---------------------------------------------------------------------------
#endif
