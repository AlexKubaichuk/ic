//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riChoiceBDTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define _CLASSCODE_  (__int64)((TStringList*)CB->Properties->Items->Objects[CB->ItemIndex])->Objects[0]
#define _CBCODE_     (__int64)CB->Properties->Items->Objects[CB->ItemIndex]
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_CB      ((TcxComboBox*)ED)
//---------------------------------------------------------------------------
__fastcall TriChoiceBDTmpl::TriChoiceBDTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   if (FNode->CmpName("choiceBD,class"))
    {
      FCtrlList = new TList;
      tmpUID = AVUp(FNode,"uid");
      tmpInt = 0;
      if (FNode->CmpName("class"))
       FNode->ReIterateGroup(CreateClass,tmpUID);
      else
       FCtrlOwner->GetNode(AVUp(FNode,"ref"))->ReIterateGroup(CreateClass,tmpUID);
      int xW = 0;
      if (FCtrlList->Count == 1)
       {
        cLab(0)->Caption = FNode->AV["name"]+":";
       }
      for (int i = 0; i <= cLast(); i++)
       {if (xW < cLab(i)->Width) xW = cLab(i)->Width;}
      int xTop = 0;
      for (int i = 0; i <= cLast(); i++)
       {
         cLab(i)->Top = xTop+1; cLab(i)->Left = 0;
         cBox(i)->Top = xTop;   cBox(i)->Left = xW+7;
         xTop += cBox(i)->Height-2;
         cBox(i)->Parent =  this;
         cLab(i)->Parent =  this;
         cLab(i)->FocusControl = cBox(i);
       }
      CMaxH = xTop;  Height = xTop+2;
/*
  //  � ������ ������ ���� ���
  UnicodeString FDep,FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
     while (FDep2.Length())
      {
        FDep  = GetPart1(FDep2,';').Trim();
        if (!FDep.Length())
         FDep = FDep2;
        if (FDep.Length() == 9 && isRefMCtrl(_GUI(FDep)))
         RefMCtrl(_GUI(FDep))->SetOnEnable(FNode);
        FDep2 = GetPart2B(FDep2,';').Trim();
      }
   }
*/
      UnicodeString FDep = _GUI(FNode->AV["depend"].UpperCase());
      if (FDep.Length() && isRefMCtrl(FDep))
        InvadeClassCB(cBox(0),0, true, cNode(0),quFree,FTabPref,"-1",FCtrlOwner->DefXML);
      else
        InvadeClassCB(cBox(0),0, true, cNode(0),quFree,FTabPref,"",FCtrlOwner->DefXML);
      cBox(0)->ItemIndex = -1;
      cBox(0)->Text = "";
      cBox(0)->Properties->OnChange((TObject*)cBox(0));
      xSize = Rect(0,0,xW+cBox(0)->Width+7,xTop);
    }
   Width = xSize.Right+10;
   if (Parent->ClassNameIs("TriChoiceBDTmpl"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetOnDependValues()
{
  UnicodeString FDep,FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
     while (FDep2.Length())
      {
        FDep  = GetPart1(FDep2,';').Trim();
        if (!FDep.Length())
         FDep = FDep2;
        if (FDep.Length() == 9 && isRefMCtrl(_GUI(FDep)))
         RefMCtrl(_GUI(FDep))->SetOnValDepend(FNode);
        FDep2 = GetPart2B(FDep2,';').Trim();
      }
   }
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   irExecSelect("Select "+ARetFlName+" From "+ADepClName+" Where "+ADepFlVal, quFree);
   UnicodeString FRetVals = "";
   int FCount = 0;
   while (!irIsEof(quFree) && (FCount < 200))
    {
      FRetVals += irquAsStr(quFree,ARetFlName) + ",";
      FCount++;
      irquNext(quFree);
    }
   if (FCount >=199)
    {
      FRetVals = "Select "+ARetFlName+" From "+ADepClName+" Where "+ADepFlVal;
    }
   else
    {
      if (!FRetVals.Length()) FRetVals = "-1";
      else                    FRetVals[FRetVals.Length()] = ' ';
    }
   ARetVals = FRetVals;
   irExecClose(quFree,true);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (ANode->CmpName("choiceBD,class"))
    {
      UnicodeString DDD,xFrom;
      if (ANode->CmpName("class"))
       ANode->ReIterateGroup(getClassCode,DDD);
      else
       FCtrlOwner->GetNode(ANode->AV["ref"].UpperCase())->ReIterateGroup(getClassCode,DDD);
      TStringList *SSS = new TStringList;
      SSS->Text = DDD; DDD = ""; xFrom = "";
      for (int i = 0; i < SSS->Count; i++)
       {
         if (!DDD.Length()) DDD  += "CLASS_"+FTabPref+SSS->Strings[i]+".CODE as CODE"+IntToStr(i);
         else               DDD  += ", CLASS_"+FTabPref+SSS->Strings[i]+".CODE as CODE"+IntToStr(i);
         if (!xFrom.Length())       xFrom  += "CLASS_"+FTabPref+SSS->Strings[i];
         else
          if (i < SSS->Count-1) xFrom  += " join CLASS_"+FTabPref+SSS->Strings[i]+" on (CLASS_"+FTabPref+SSS->Strings[i-1]+".CODE=CLASS_"+FTabPref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+" )";
          else                  xFrom  += " join CLASS_"+FTabPref+SSS->Strings[i]+" on (CLASS_"+FTabPref+SSS->Strings[i-1]+".CODE=CLASS_"+FTabPref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+" and CLASS_"+FTabPref+SSS->Strings[i]+".CODE="+IntToStr(cCODE)+")";
       }
      irExecSelect("Select "+DDD+" From "+xFrom, quFree);
      UnicodeString XXX;
      for (int i = 0; i < SSS->Count; i++)
       {
         XXX = irquAsStr(quFree,"CODE"+IntToStr(i));
         if (!XXX.Length()) XXX = "-1";
         ACode += XXX+"\n";
       }
      if (SSS) delete SSS;
      SSS = NULL;
    }
}
//---------------------------------------------------------------------------
TLabel* __fastcall TriChoiceBDTmpl::cLab(int AIdx)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((TriChoiceBDControls*)FCtrlList->Items[AIdx])->Lab;
}
//---------------------------------------------------------------------------
int __fastcall TriChoiceBDTmpl::cLast()
{
  return (FCtrlList->Count - 1);
}
//---------------------------------------------------------------------------
TcxComboBox* __fastcall TriChoiceBDTmpl::cBox(int AIdx)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((TriChoiceBDControls*)FCtrlList->Items[AIdx])->CB;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TriChoiceBDTmpl::cNode(int AIdx)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return ((TriChoiceBDControls*)FCtrlList->Items[AIdx])->Node;
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBDTmpl::getClassCode(TTagNode *itTag, UnicodeString &UID)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (itTag->CmpName("class"))
   if (!UID.Length())
    UID += AVUp(itTag,"uid");
   else
    UID += "\n"+AVUp(itTag,"uid");
  return false;
}
//---------------------------------------------------------------------------
__fastcall TriChoiceBDTmpl::~TriChoiceBDTmpl()
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//   for (int i = 0; i <= cLast(); i++)
   TStringList *tmp;
   for (int i = 0; i <= cLast(); i++)
    {
/*
       cBox(i)->OnChange = NULL;
       AXBox = cBox(i);
       AXBox->Items->Count;
       tmp = (TStringList*)AXBox->Items;
*/
       cBox(i)->Properties->OnChange = NULL;
       ClearClassCBItems(cBox(i)->Properties->Items);
       delete cBox(i);
       delete cLab(i);
    }
   for (int i = 0; i < FCtrlList->Count; i++)
    {
       if (FCtrlList->Items[i]) delete ((TriChoiceBDControls*)FCtrlList->Items[i]);
       FCtrlList->Items[i] = NULL;
    }
   FCtrlList->Clear();
   delete FCtrlList;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetEDLeft(int ALeft)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  if (FNode->CmpName("choiceBD,class"))
   {
     for (int i = 0; i <= cLast(); i++)
      cBox(i)->Left = ALeft;
   }
  if (ED)                                Width = ED->Left+ED->Width+4;
  if (FNode->CmpName("choiceBD,class"))  Width = cBox(0)->Left+cBox(0)->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FNode->CmpName("choiceBD,class"))
   {
     *AEDLeft = cBox(0)->Left;
     *ALabR   = cBox(0)->Left-2;
     return;
   }
  if (!TwoLine&&ED&&(!FNode->CmpName("binary")))  *AEDLeft = ED->Left;
  else                                            *AEDLeft = -1;
  if (!TwoLine&&Lab&&(!FNode->CmpName("binary"))) *ALabR = Lab->Left+Lab->Width;
  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::cResize(TObject *Sender)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (FNode->CmpName("choiceBD,class"))
      {
        for (int i = 0; i <= cLast(); i++)
         {
           cBox(i)->Left = Width - cBox(i)->Width - 2;
           TNotifyEvent FSaveOnChange = cBox(i)->Properties->OnChange;
           cBox(i)->Properties->OnChange = NULL;
           int FSaveIndex = cBox(i)->ItemIndex;
           cBox(i)->ItemIndex = -1;
           cBox(i)->ItemIndex = FSaveIndex;
           cBox(i)->Properties->OnChange = FSaveOnChange;
         }
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBDTmpl::CreateClass(TTagNode *itTag, UnicodeString &UID)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (itTag->CmpName("class"))
   {
     TLabel *tmpLab;
     TcxComboBox *tmpCB;

     FCtrlList->Add(new TriChoiceBDControls);
     TriChoiceBDControls *DDD = (TriChoiceBDControls*)FCtrlList->Items[FCtrlList->Count-1];
     DDD->Lab = new TLabel(Owner);
     DDD->Lab->Caption = "  "+itTag->GetChildByName("description")->AV["name"]+":";
     DDD->Lab->Name = "LAB_"+itTag->AV["uid"]+UID;
     DDD->Lab->Transparent = true;
     DDD->CB = new TcxComboBox(Owner);
     DDD->CB->Properties->DropDownListStyle = lsEditFixedList;
     DDD->CB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
     DDD->CB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
     DDD->CB->Style->Color = (Required)? FReqColor:clWindow;
     DDD->CB->Name = "CB_"+itTag->AV["uid"]+UID;
     DDD->CB->Tag = tmpInt;
     DDD->CB->Properties->OnChange = CBChange;
     DDD->CB->Text = "";
     DDD->Node = itTag;
     tmpInt++;
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString FDep,FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
     UnicodeString FDepTab, FDepWhereVal, FRetFlName, FRetVals;
     UnicodeString FDepUID;
     TTagNode *FDepClDef, *FDepNode, *_tmp;
     FDepTab = "";
     bool IsEquDepend = true;
     while (FDep2.Length() && IsEquDepend)
      {
        FDep  = GetPart1(FDep2,';').Trim();
        if (!FDep.Length())
         FDep = FDep2;
        FDepUID = _UID(FDep);
        if (!FDepTab.Length())
         {
           FDepTab   = "CLASS_"+FTabPref+FDepUID;
           FDepClDef = FCtrlOwner->GetNode(FDepUID);
         }
        else
         {
           IsEquDepend &= (FDepTab == "CLASS_"+FTabPref+FDepUID);
         }
        FDep2 = GetPart2B(FDep2,';').Trim();
      }
     FDep2 = FNode->AV["depend"].UpperCase();
     UnicodeString FDepCode, FDepClCode;
     if (IsEquDepend)
      { // ��� ����������� � ����� �������, ����� ������� ����� where
        FRetVals = "";
        FDepWhereVal = "";
        if (ACode != -2)
         {
           while (FDep2.Length())
            {
              FDep  = GetPart1(FDep2,';').Trim();
              if (!FDep.Length())
               FDep = FDep2;
              if (_GUI(FDep) == ADepUID)
               {
                 FDepCode   = IntToStr(ACode);
                 FDepClCode = ADepClCode;
               }
              else
               {
                 _tmp = FCtrlOwner->GetNode(_GUI(FDep));
                 FDepClCode = _tmp->AV["ref"];
                 FDepCode   = FGetExtCtrl(_GUI(FDep))->GetValue("");
               }
              if (FDepWhereVal.Length())
               FDepWhereVal += " and ";

              FDepUID = _UID(FDep);
              FDepNode = FDepClDef->GetChildByAV("","ref",FDepClCode);
              FRetFlName = "CODE";
              if (FDepCode.ToIntDef(-1) == -1)
               {
                 FDepWhereVal += "(R"+FDepNode->AV["uid"]+" is NULL or R"+FDepNode->AV["uid"]+"=0)";
               }
              else
               {
                 if (FDepUID == FDepClCode)
                  {
                    FDepWhereVal += "CODE="+FDepCode;
                  }
                 else if (FDepUID == FNode->AV["ref"])
                  {
                    FDepWhereVal += "R"+FDepNode->AV["uid"]+"="+FDepCode;
                  }
                 else
                  {
                    FDepWhereVal += "R"+FDepNode->AV["uid"]+"="+FDepCode;
                    FRetFlName   = "R"+FDepClDef->GetChildByAV("","ref",FNode->AV["ref"])->AV["uid"];
                  }
               }
              FDep2 = GetPart2B(FDep2,';').Trim();
            }
           GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
         }
      }
     else
      {
        while (FDep2.Length())
         {
           FDep  = GetPart1(FDep2,';').Trim();
           if (!FDep.Length())
            FDep = FDep2;
           if (_GUI(FDep) == ADepUID)
            {
              FDepUID = _UID(FDep);
              FDepTab = "CLASS_"+FTabPref+FDepUID;
              FDepClDef = FCtrlOwner->GetNode(FDepUID);
              FDepNode = FDepClDef->GetChildByAV("","ref",ADepClCode);
              FRetVals = "";
              if (ACode != -2)
               {
                 if (ACode == -1)
                  {
                    FDepWhereVal = "(R"+FDepNode->AV["uid"]+" is NULL or R"+FDepNode->AV["uid"]+"=0)";
                    FRetFlName   = "CODE";
                  }
                 else
                  {
                    if (FDepUID == ADepClCode)
                     {
                       FDepWhereVal = "CODE="+IntToStr(ACode);
                     }
                    else if (FDepUID == FNode->AV["ref"])
                     {
                       FDepWhereVal = "R"+FDepNode->AV["uid"]+"="+IntToStr(ACode);
                       FRetFlName   = "CODE";
                     }
                    else
                     {
                       FDepWhereVal = "R"+FDepNode->AV["uid"]+"="+IntToStr(ACode);
                       FRetFlName   = "R"+FDepClDef->GetChildByAV("","ref",FNode->AV["ref"])->AV["uid"];
                     }
                  }
                 GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
               }
            }
           FDep2 = GetPart2B(FDep2,';').Trim();
         }
      }
     InvadeClassCB(cBox(0),0, true, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
     if (IsAppend)
      {
        if (cBox(0)->Properties->OnChange)
         cBox(0)->Properties->OnChange(cBox(0));
        if (cBox(0)->Properties->Items->Count > 1) cBox(0)->Text = "";
      }
     else
      riDataChange((TObject*)cBox(0));
   }

/*  UnicodeString FDep,FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
     while (FDep2.Length())
      {
        FDep  = GetPart1(FDep2,';').Trim();
        if (!FDep.Length())
         FDep = FDep2;
        if (_GUI(FDep) == ADepUID)
         {
           UnicodeString FDepTab, FDepFlVal, FRetFlName, FRetVals;
           UnicodeString FDepUID = _UID(FDep);
           FDepTab = "CLASS_"+FTabPref+FDepUID;
           TTagNode *_tmp = FCtrlOwner->GetNode(FDepUID);
           if (FDepUID == ADepClCode)
            {
              FDepFlVal = "CODE="+IntToStr(ACode);
            }
           else if (FDepUID == FNode->AV["ref"])
            {
              FDepFlVal  = "R"+_tmp->GetChildByAV("","ref",ADepClCode)->AV["uid"]+"="+IntToStr(ACode);
              FRetFlName = "CODE";
            }
           else
            {
              FDepFlVal  = "R"+_tmp->GetChildByAV("","ref",ADepClCode)->AV["uid"]+"="+IntToStr(ACode);
              FRetFlName = "R"+_tmp->GetChildByAV("","ref",FNode->AV["ref"])->AV["uid"];
            }
           FRetVals = "";
           GetDependClassCode(FDepTab, FDepFlVal, FRetFlName, FRetVals);
           InvadeClassCB(cBox(0),0, 1, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
           if (cBox(0)->Properties->Items->Count > 1) cBox(0)->Text = "";
           riDataChange((TObject*)cBox(0));
         }
        FDep2 = GetPart2B(FDep2,';').Trim();
      }
   }
   */
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::CBChange(TObject *Sender)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  TcxComboBox *CB = (TcxComboBox*)Sender;
  if (CB->ItemIndex >= 1)
   {
     if (((CB->Tag+1) <= cLast())||(!TmpCtrl))
      {
        cLab(CB->Tag+1)->Enabled = isEdit;
        if (isEdit)
         {
           cBox(CB->Tag+1)->Style->Color = (Required)? FReqColor:clWindow;
         }
        else
         cBox(CB->Tag+1)->Style->Color = clBtnFace;
        cBox(CB->Tag+1)->Enabled = isEdit;
        InvadeClassCB(cBox(CB->Tag+1),_CLASSCODE_, TmpCtrl, cNode(CB->Tag+1),quFree,FTabPref,"",FCtrlOwner->DefXML); //+++
        cBox(CB->Tag+1)->Properties->OnChange((TObject*)cBox(CB->Tag+1));
      }
   }
  else
   {
     if (((CB->Tag+1) <= cLast())||(!TmpCtrl))
      {
        cLab(CB->Tag+1)->Enabled = false;
        cBox(CB->Tag+1)->Enabled = false;
        cBox(CB->Tag+1)->Style->Color = clBtnFace;
        if (!(!IsAppend && !isEdit))
         ClearClassCBItems(cBox(CB->Tag+1)->Properties->Items);
        cBox(CB->Tag+1)->Properties->OnChange((TObject*)cBox(CB->Tag+1));
      }
   }
  UnicodeString xVal = "";
  for (int i = 0; i <= cLast(); i++)
   {
     if (cBox(i)->ItemIndex >= 1)
      xVal += IntToStr((__int64)((TStringList*)cBox(i)->Properties->Items->Objects[cBox(i)->ItemIndex])->Objects[0])+"\n";
   }
  if (!xVal.Length()) _TemplateValue(FNode).Empty();
  else            _TemplateValue(FNode) = Variant(xVal);
  if (SetValList->Count > 0 && CB->Tag == cLast())
   {// ���� ����������� �� ������ � ��� ��� choiceBD ����������
     if (CB->ItemIndex >= 1)
      {
        UnicodeString FDepClCode = FNode->AV["ref"];
        for (int i = 0; i < SetValList->Count; i++)
         {
           depCtrl(i)->SetDependVal(FDepClCode, _CLASSCODE_, FNode->AV["uid"]);
         }
      }
     else
      {
        UnicodeString FDepClCode = FNode->AV["ref"];
        for (int i = 0; i < SetValList->Count; i++)
         {
           depCtrl(i)->SetDependVal(FDepClCode, -1, FNode->AV["uid"]);
         }
      }
   }
  riDataChange((TObject*)CB);
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBDTmpl::UpdateChanges()
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FNode->CmpName("choiceBD,class"))
   {
     UnicodeString xVal = "";
     Variant vVal;
     vVal.Empty();
     for (int i = 0; i <= cLast(); i++)
      {
        if (cBox(i)->ItemIndex >= 1)
         xVal += IntToStr((__int64)((TStringList*)cBox(i)->Properties->Items->Objects[cBox(i)->ItemIndex])->Objects[0])+"\n";
      }
     if (xVal.Length()) vVal = Variant(xVal);
     _TemplateValue(FNode) = vVal;
   }
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceBDTmpl::GetValueStr(bool ABinaryNames)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->CmpName("choiceBD,class"))
   {
     for (int i = 0; i <= cLast(); i++)
      {
        if (cBox(i)->ItemIndex >= 1)
         xVal += " "+cBox(i)->Properties->Items->Strings[cBox(i)->ItemIndex];
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetIsVal(bool AEnable)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetEnable(bool AEnable)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (AEnable)
   {
     if (FNode->CmpName("choiceBD,class"))
      {
        cBox(0)->Enabled = true;  cLab(0)->Enabled = true;
        cBox(0)->Style->Color = (Required)? FReqColor:clWindow;
      }
   }
  else
   {
     if (FNode->CmpName("choiceBD,class"))
      {
        cBox(0)->Style->Color = clBtnFace;
        cBox(0)->Enabled = false;
        cLab(0)->Enabled = false;
        cBox(0)->ItemIndex = -1;
        cBox(0)->Text = "";
        if (cBox(0)->ItemIndex > 0)
         cBox(0)->Text = cBox(0)->Properties->Items->Strings[cBox(0)->ItemIndex];
        cBox(0)->Properties->OnChange((TObject*)cBox(0));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetEnableEx(bool AEnable)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (AEnable)
   {
     if (FNode->CmpName("choiceBD,class"))
      {
        cBox(0)->Enabled = true;  cLab(0)->Enabled = true;
        cBox(0)->Style->Color = (Required)? FReqColor:clWindow;
      }
   }
  else
   {
     if (FNode->CmpName("choiceBD,class"))
      {
        cBox(0)->Style->Color = clBtnFace;
        cBox(0)->Enabled = false;
        cLab(0)->Enabled = false;
        cBox(0)->ItemIndex = -1;
        cBox(0)->Text = "";
        if (cBox(0)->ItemIndex > 0)
         cBox(0)->Text = cBox(0)->Properties->Items->Strings[cBox(0)->ItemIndex];
        cBox(0)->Properties->OnChange((TObject*)cBox(0));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (AVal.Length())
   {
     if (FNode->CmpName("choiceBD"))
      {
        SetChBDValue(AVal,AEnable);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetLabel(UnicodeString xLab)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     if (xLab.Length()) Lab->Caption = xLab+":";
     else            Lab->Caption = "";//FNode->AV["name"]+":";
     Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetOnLab(TTagNode* ANode)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetLabList->Add(ANode);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetOnEnable(TTagNode* ANode)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetEnList->Add(ANode);
   if (FNode->CmpName("choiceBD"))
    {
      if (!TmpCtrl)
       {
         if (!IsAppend && SrcDS()->Active)
          {
            UnicodeString  ClCode;
            if (cLast() > 0) GetClassCode(ClCode,irquAsInt(SrcDS(),"R"+AVUp(FNode,"uid")),FNode);
            else           ClCode = irquAsStr(SrcDS(),"R"+AVUp(FNode,"uid"))+"\n";
            SetChBDValue(ClCode,isEnabled);
          }
         if (IsTemplate(FNode))
          SetChBDValue(_TemplateValue(FNode),false);
       }
    }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::riDataChange(TObject *Sender)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::FDataChange(TObject *Sender)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
     if (FNode->CmpName("choiceBD")) // ����� �� choiceBD
      {
        TcxComboBox *CB = (TcxComboBox*)Sender;
        xUID = CB->Name.SubString(4,4).UpperCase();
        TTagNode *xNode;
        TStringList *CBValues;
        if (CB->ItemIndex >= 1)  // ���� �������� ����, ��������� �����
         {
           CBValues = ((TStringList*)CB->Properties->Items->Objects[CB->ItemIndex]);
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xNode = ((TTagNode*)SetLabList->Items[i]);
              xRef = xNode->AV["depend"].UpperCase();     // ������ xxxx.yyyy
              // xxxx == uid.choiceBD yyyy == uid.��������������* ��� ref.choiceBD == uid.��������������
              if ((xRef.Length() == 9)&&(xRef.SubString(1,4) == FUID)&&(xRef.SubString(6,4) == xUID))
               {                                    // uid.��������������* - uid ����� ��� ������������ ��������������
                 xRef = "R"+AVUp(xNode,"ref");      // ref - uid ���� �������������� �������� �������� �������� ������
                 if (CBValues->IndexOfName(xRef) != -1)
                  {
                    xRef = CBValues->Values[xRef];
                    int iInd = xRef.Pos("_#_");
                    if (iInd) // ���� �� �������� �������� �������� ����� ��� "������"
                     xRef = xRef.SubString(iInd+3,xRef.Length()-iInd-2);
                    FGetExtCtrl(xNode->AV["uid"])->SetLabel(xRef);
                  }
               }
            }
         }
        else                 // ����� �������� �����
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xNode = ((TTagNode*)SetLabList->Items[i]);
              xRef = xNode->AV["depend"].UpperCase();
              if ((xRef.Length() == 9)&&(xRef.SubString(1,4) == FUID)&&(xRef.SubString(6,4) == xUID))
               FGetExtCtrl(xNode->AV["uid"])->SetLabel("");
            }
         }
      }
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceBDTmpl::GetControl(int AIdx)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    return (TControl*)cBox(AIdx);
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceBDTmpl::GetFirstEnabledControl()
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("choiceBD,class"))
    {
      for (int i = 0; i < FCtrlList->Count; i++)
       {
         if (cBox(i)->Enabled&&isEdit)  return (TControl*)cBox(i);
       }
      return NULL;
    }
   else
    return NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceBDTmpl::GetValue(UnicodeString ARef)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("choiceBD"))
    {
      UnicodeString clUID = _GUI(ARef);
      TcxComboBox *CB = NULL;
      if (!_UID(ARef).Length())
       {
         CB = cBox(cLast());
         if (CB->ItemIndex >= 1)
          return IntToStr(_CLASSCODE_);
         else
          return "";
       }
      for (int i = 0; i < FCtrlList->Count; i++)
       {
         CB = cBox(i);
         if (CB->Name == "CB_"+_UID(ARef)+clUID)
          {
            if (CB->ItemIndex >= 1)
             return IntToStr(_CLASSCODE_);
            else
             return "";
          }
         if (CB->Name.SubString(4,4).UpperCase() == clUID) break;
         CB = NULL;
       }
      if (!CB) return "";
      TStringList *CBValues;
      if (CB->ItemIndex >= 1)
       {
         CBValues = ((TStringList*)CB->Properties->Items->Objects[CB->ItemIndex]);
         UnicodeString xRef = "R"+_UID(ARef);
         if (CBValues->IndexOfName(xRef) != -1)
          {
            xRef = CBValues->Values[xRef];
            int iInd = xRef.Pos("_#_");
            if (iInd) // ���� �� �������� �������� �������� ����� ��� "������"
             xRef = xRef.SubString(iInd+3,xRef.Length()-iInd-2);
            return xRef;
          }
       }
    }
   return "";
}
//---------------------------------------------------------------------------
TStringList* __fastcall TriChoiceBDTmpl::GetStrings(UnicodeString AClRef)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("choiceBD"))
    {
      TcxComboBox *CB = NULL;
      for (int i = 0; i < FCtrlList->Count; i++)
       {
         CB = cBox(i);
         if (CB->Name.SubString(4,4).UpperCase() == AClRef) break;
         CB = NULL;
       }
      if (CB) return (TStringList*)CB->Properties->Items;
    }
   return NULL;
}
//---------------------------------------------------------------------------
Variant __fastcall TriChoiceBDTmpl::GetCompValue(UnicodeString ARef)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if ((ARef.Length())&&FNode->CmpName("choiceBD"))
   {
     TStringList *CBValues;
     for (int i = 0; i <= cLast(); i++)
      {
        if (ARef.SubString(1,4) == cBox(i)->Name.SubString(4,4))
         {
           if (cBox(i)->ItemIndex >= 1)
            {
              CBValues = ((TStringList*)cBox(i)->Properties->Items->Objects[cBox(i)->ItemIndex]);
              UnicodeString __flName = "R"+ARef.SubString(6,4);
              if (CBValues->IndexOfName(__flName) != -1)
               {
                 __flName = CBValues->Values[__flName];
                 int iInd = __flName.Pos("_#_");
                 if (iInd)
                   __flName = __flName.SubString(1,iInd-1);
                 return Variant(__flName);
               }
            }
           else
            return Variant::Empty();
         }
      }
     return Variant::Empty();
   }
  else
   {
      ShowMessage(FMT1(icsRegEDErrorRefObject,ARef).c_str());
      return Variant::Empty();
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::SetChBDValue(UnicodeString AVal, bool AEnabled)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TStringList *ValList = NULL;
   try
    {
      ValList = new TStringList;
      ValList->Text = AVal.Trim();
      while (ValList->Count > FCtrlList->Count) // ����������� ���������� �������� � ��������
       ValList->Delete(ValList->Count-1);
      for (int i = 0; i < ValList->Count; i++)
       {
         for (int k = (int)TmpCtrl; k < cBox(i)->Properties->Items->Count; k++)
          {
            __int64 ItemCode = (__int64)((TStringList*)cBox(i)->Properties->Items->Objects[k])->Objects[0];
            if (ItemCode == (__int64)ValList->Strings[i].ToIntDef(-2))
             {
               cBox(i)->ItemIndex = k; cBox(i)->Properties->OnChange((TObject*)cBox(i));
               if (!AEnabled)
                {
                  cLab(i)->Enabled = false;
                  cBox(i)->Enabled = false;
                  cBox(i)->Style->Color = clBtnFace;
                }
               break;
             }
          }
       }
    }
   __finally
    {
      if (ValList) delete ValList;
    }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceBDTmpl::CheckReqValue(TcxPageControl *APC)
{
   if (!this) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     TWinControl *tmpCTRL = NULL;
     try
      {
        if (FNode->CmpName("choiceBD,class"))
         {
           for (int i = 0; i <= cLast(); i++)
            {
              if (cBox(i)->Enabled && (cBox(i)->ItemIndex < 1))
               {
                 tmpCTRL = cBox(i);
                 UnicodeString ErrMessage = FNode->AV["name"]+": ";
                 ErrMessage += FCtrlOwner->GetNode(cBox(i)->Name.SubString(4,4).c_str())->AV["name"];
                 ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
                 MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONINFORMATION);
                 RC = false;
               }
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
           tmpCTRL->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceBDTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  for (int i = 0; i <= cLast(); i++)
   cBox(i)->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

