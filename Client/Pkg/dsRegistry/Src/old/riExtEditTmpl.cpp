//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riExtEditTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "cxButtonEdit"

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_BE       dynamic_cast<TcxButtonEdit*>(ED)
#define cExtLab      static_cast<TLabel*>(CBList->Items[0])
//---------------------------------------------------------------------------
__fastcall TriExtEditTmpl::TriExtEditTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  //������ ����� ��������
  Lab = new TLabel(this);   Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Caption = FNode->AV["name"]+":";
  // ������ ����� ��� ���������� ��������
  CBList = new TList;
  CBList->Add(new TLabel(this));
  cExtLab->Parent = this;
  cExtLab->Transparent = true;
  cExtLab->AutoSize = false;
  cExtLab->WordWrap = true;
  cExtLab->Color = LtColor(clBtnFace,16);
  cExtLab->Font->Color = clGray;
  cExtLab->Font->Size = 6;
  // ������ �������
  ED = new TcxButtonEdit((TComponent*)NULL);
  tED_BE->Parent =  this;
  tED_BE->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_BE->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  tED_BE->Properties->ClickKey = ShortCut(VK_DOWN,TShiftState() << ssAlt);
  tED_BE->Properties->ReadOnly = true;
  tED_BE->Properties->ViewStyle = vsHideCursor;
  TcxEditButton *tmpBtn = tED_BE->Properties->Buttons->Add();
  tmpBtn->Kind = bkText;
  tmpBtn->Caption = "X";
//      tmpBtn->Glyph->LoadFromResourceName((int)HInstance,"CLEAR");
  tED_BE->Properties->OnButtonClick = ExtBtnClick;
  // ������ ���������
  if (Required) tED_BE->Style->Color = FReqColor;
  Lab->FocusControl = ED;
  // ������������ � �������

  Lab->Top = 2;  Lab->Left = 2;
  if (Lab->Width > (AParent->Width-2))
   {
     Lab->Font->Size = Lab->Font->Size - 1;
     Lab->Update();
     if (Lab->Width > (AParent->Width - 2))
      Lab->Width = AParent->Width-2;
   }
  ED->Constraints->MinWidth = 60;

  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
     tED_BE->Left = Lab->Width+5;
     tED_BE->Top = 0;
   }
  else
   {
     tED_BE->Left = 2;
     tED_BE->Top = Lab->Height+2;
     TwoLine = true;  
   }
  tED_BE->Width = AParent->Width - 4 - tED_BE->Left;

  if ((tED_BE->Left + tED_BE->Width) > (AParent->Width - 4))
   {
     TwoLine = true;
     tED_BE->Left = 4;
     tED_BE->Top = Lab->Height;
     tED_BE->Width = AParent->Width - 4 - tED_BE->Left;
   }
  cExtLab->Left = Lab->Left;
  cExtLab->Width = Width-4;
  cExtLab->Height = (cExtLab->Height-2)*(FNode->AV["linecount"].ToIntDef(1)-1);
  cExtLab->Top = tED_BE->Top+tED_BE->Height;
  CMaxH = cExtLab->Top + cExtLab->Height+2; CMinH = CMaxH;
  Height = CMaxH;
  if (FNode->GetAVDef("depend").Length() && isRefMCtrl(_GUI(FNode->AV["depend"])))
   {
     UnicodeString DepUID = _GUI(FNode->AV["depend"]);
     TTagNode *DepNode = FCtrlOwner->GetNode(DepUID);
     UnicodeString DepVal = RefMCtrl(_GUI(FNode->AV["depend"]))->GetValue("");
     SetDependVal(DepNode->AV["ref"], DepVal.ToIntDef(-1), DepNode->AV["uid"]);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetOnDependValues()
{
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEditTmpl::FSetEditValue(UnicodeString AVal)
{
  UnicodeString FCode = "";
  UnicodeString FText = "";
  try
   {
     int ind = AVal.Pos("=");
     if (ind)
      {
        FCode = AVal.SubString(1,ind-1);
        FText = AVal.SubString(ind+1,AVal.Length()-ind);
      }
     else
      FCode = AVal;
     if (FNode->AV["showtype"].ToIntDef(0))
      { // 1 - � ��������� ���
        tED_BE->Text = FCode;
        cExtLab->Caption = FText;
      }
     else
      { // 0 - � ��������� �����
        tED_BE->Text = FText;
        cExtLab->Caption = FCode;
      }
     riDataChange(this);
   }
  __finally
   {
   }
  return FCode; 
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::ExtBtnClick(TObject *Sender, int AButtonIndex)
{
  UnicodeString SrcCode = "";
  if (AButtonIndex == 0)
   {
     if (FExtBtnClick)
      {
        if (FNode->AV["showtype"].ToIntDef(0)) SrcCode = tED_BE->Text;
        else                                   SrcCode = cExtLab->Caption;

        if (FExtBtnClick(FNode,SrcCode,NULL,true,(TObject*) FCtrlOwner,0))
         FSetEditValue(SrcCode);
      }
   }
  else
   {
     tED_BE->Text = "";
     cExtLab->Caption = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSetExtBtnClick(TExtBtnClick AExtBtnClick)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FExtBtnClick = AExtBtnClick;
  if (AExtBtnClick)
   {
     UnicodeString SrcCode = "";
     if (FNode->AV["showtype"].ToIntDef(0)) SrcCode = tED_BE->Text;
     else                                   SrcCode = cExtLab->Caption;

     if (FExtBtnClick(FNode,SrcCode,NULL, false,(TObject*) FCtrlOwner,0))
      FSetEditValue(SrcCode);
   }
}
//---------------------------------------------------------------------------
__fastcall TriExtEditTmpl::~TriExtEditTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  delete cExtLab;
  delete tED_BE;
//  delete CBList;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine) ED->Left = ALeft;
  cExtLab->Width = ED->Left+ED->Width;
  Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else               *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else               *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     cExtLab->Left = 2; cExtLab->Width = Width-4;
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriExtEditTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEditTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->AV["showtype"].ToIntDef(0)) xVal = cExtLab->Caption;
  else                                   xVal = tED_BE->Text;
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetEnable(bool AEnable)
{
  isEnabled = AEnable;
  ED->Enabled = isEnabled;
  cExtLab->Enabled = isEnabled;
  if (!ED->Enabled)
   {
     FSetEditValue("");
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  FSetEditValue(AVal);
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
// ������ �������� ��� ��������� �������� �������� 
  if (ED->Enabled)
   {
     if (FNode->AV["showtype"].ToIntDef(0)) _TemplateValue(FNode) = (tED_BE->Text.Trim().Length()) ? Variant(tED_BE->Text) : Variant::Empty();
     else                                   _TemplateValue(FNode) = (cExtLab->Caption.Trim().Length()) ? Variant(cExtLab->Caption) : Variant::Empty();
   }
  else
   _TemplateValue(FNode) = Variant::Empty();
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (FNode->AV["showtype"].ToIntDef(0)) Src = tED_BE->Text;
     else                                   Src = cExtLab->Caption;
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (FNode->AV["showtype"].ToIntDef(0)) Src = tED_BE->Text;
     else                                   Src = cExtLab->Caption;
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriExtEditTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,date,datetime,time,digit,extedit,binary,choice,choiceTree"))
    return (TControl*)ED;
   else
    return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriExtEditTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEditTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FNode->AV["showtype"].ToIntDef(0)) return tED_BE->Text;
  else                                   return cExtLab->Caption;
}
//---------------------------------------------------------------------------
Variant __fastcall TriExtEditTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" 'tagtype not supported'");
}
//---------------------------------------------------------------------------
bool __fastcall TriExtEditTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
         {
          if (!tED_BE->Text.Length())  RC = false;
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  tED_BE->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

