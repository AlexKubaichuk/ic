//---------------------------------------------------------------------------

#ifndef riChoiceTmplH
#define riChoiceTmplH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <DB.hpp>
//#include <DBCtrls.hpp>
//#include <Registry.hpp>
//#include "AxeUtil.h"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
#include <ADODB.hpp>
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3


//#include "cxContainer.hpp"
//#include "cxControls.hpp"
//#include "cxDropDownEdit.hpp"
//#include "cxEdit.hpp"
//#include "cxGraphics.hpp"
//#include "cxMaskEdit.hpp"
//#include "cxTextEdit.hpp"

//---------------------------------------------------------------------------
#include "RegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriChoiceTmpl: public TRegEDItem
{
private:
    void     __fastcall cResize(TObject *Sender);
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    void     __fastcall FDefaultDataChange(TObject *Sender);
    void     __fastcall FDataChange(TObject *Sender);
    void     __fastcall FSetEDRequired(bool AVal);
public:
    void         __fastcall SetOnDependValues();
    void         __fastcall riDataChange(TObject *Sender);
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString xLab);
    void         __fastcall SetOnLab(TTagNode* ANode);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
__published:
    __fastcall TriChoiceTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                      UnicodeString APref,  TColor AReqColor,
                                      TRegEDContainer *ACtrlOwner,
                                      UnicodeString AVal);
    __fastcall ~TriChoiceTmpl();
};
//---------------------------------------------------------------------------
#endif
