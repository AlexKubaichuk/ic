//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riExtEdit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_BE       dynamic_cast<TcxButtonEdit*>(ED)
#define cExtLab      static_cast<TLabel*>(CBList->Items[0])
//---------------------------------------------------------------------------
__fastcall TriExtEdit::TriExtEdit(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
  //������ ����� ��������
  Lab = new TLabel(this);   Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Caption = FNode->AV["name"]+":";
  // ������ ����� ��� ���������� ��������
  CBList = new TList;
  CBList->Add(new TLabel(this));
  cExtLab->Parent = this;
  cExtLab->Transparent = true;
  cExtLab->AutoSize = false;
  cExtLab->ParentFont = true;
  cExtLab->WordWrap = true;
  cExtLab->Color = LtColor(clBtnFace,16);
/*  cExtLab->Font->Color = clGray;
  cExtLab->Font->Size = 6;*/
  // ������ �������
  ED = new TcxButtonEdit(this);
  tED_BE->Parent =  this;
  tED_BE->ShowHint =  true;
  tED_BE->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_BE->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  tED_BE->Properties->ClickKey = ShortCut(VK_DOWN,TShiftState() << ssAlt);
  tED_BE->Properties->ReadOnly = true;
  tED_BE->Properties->ViewStyle = vsHideCursor;
  TcxEditButton *tmpBtn;

  int FBtnCount = FNode->AV["btncount"].ToIntDef(1);
  bool FSetBmp;
  UnicodeString FHint;
/*
//  RegIL->GetBitmap(48,ABitmap);
  Graphics::TBitmap *FFF = new Graphics::TBitmap;

  RegIL->GetBitmap(48,FFF);
  FFF->SaveToStream(DDD);
  ABitmap->LoadFromStream(DDD);
//  RegIL->GetBitmap(47,ABitmap);
  delete DDD;
  delete FFF;
  return true;
*/
  TMemoryStream *BmpStm = new TMemoryStream;
  try
   {
     for (int i = 1; i < FBtnCount; i++)
      {
        tmpBtn = tED_BE->Properties->Buttons->Add();
        FSetBmp = false;
        FHint = "";
        if (ACtrlOwner->OnGetExtBtnGetBitmap)
         {
           tmpBtn->Kind = bkGlyph;
           FSetBmp = ACtrlOwner->OnGetExtBtnGetBitmap(FNode, i, tmpBtn, FHint);
         }
        tmpBtn->Hint = FHint;
        if (!FSetBmp)
         {
           tmpBtn->Kind = bkText;
           tmpBtn->Caption = FHint;
         }
      }
   }
  __finally
   {
     delete BmpStm;
   }
  tmpBtn = tED_BE->Properties->Buttons->Add();
  tmpBtn->Kind = bkText;
  tmpBtn->Caption = "X";
  tED_BE->Properties->OnButtonClick = ExtBtnClick;
  // ������ ���������
  if (Required) tED_BE->Style->Color = FReqColor;
  Lab->FocusControl = ED;
  // ������������ � �������

  Lab->Top = 2;  Lab->Left = 2;
  if (Lab->Width > (AParent->Width-2))
   {
     Lab->Font->Size = Lab->Font->Size - 1;
     Lab->Update();
     if (Lab->Width > (AParent->Width - 2))
      Lab->Width = AParent->Width-2;
   }
  ED->Constraints->MinWidth = 60;

  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
     tED_BE->Left = Lab->Left+Lab->Width+5;
     tED_BE->Top = 0;
   }
  else
   {
     tED_BE->Left = 2;
     tED_BE->Top = Lab->Height+2;
     TwoLine = true;
   }
  tED_BE->Width = AParent->Width - 4 - tED_BE->Left;

  if ((tED_BE->Left + tED_BE->Width) > (AParent->Width - 4))
   {
     TwoLine = true;
     tED_BE->Left = 4;
     tED_BE->Top = Lab->Height;
     tED_BE->Width = AParent->Width - 4 - tED_BE->Left;
   }
  cExtLab->Left = Lab->Left;
  cExtLab->Width = Width-4;
  cExtLab->Height = (cExtLab->Height-2)*(FNode->AV["linecount"].ToIntDef(1)-1);
  cExtLab->Top = tED_BE->Top+tED_BE->Height;
  CMaxH = cExtLab->Top + cExtLab->Height+2; CMinH = CMaxH;
  Height = CMaxH;
  if (FNode->GetAVDef("depend").Length() && isRefMCtrl(_GUI(FNode->AV["depend"])))
   {
     UnicodeString DepUID = _GUI(FNode->AV["depend"]);
     TTagNode *DepNode = FCtrlOwner->GetNode(DepUID);
     UnicodeString DepVal = RefMCtrl(_GUI(FNode->AV["depend"]))->GetValue("");
     SetDependVal(DepNode->AV["ref"], DepVal.ToIntDef(-1), DepNode->AV["uid"]);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetOnDependValues()
{
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEdit::FSetEditValue(UnicodeString AVal)
{
  UnicodeString FCode = "";
  UnicodeString FText = "";
  try
   {
     int ind = AVal.Pos("=");
     if (ind)
      {
        FCode = AVal.SubString(1,ind-1);
        FText = AVal.SubString(ind+1,AVal.Length()-ind);
      }
     else
      FCode = AVal;
     if (FNode->AV["showtype"].ToIntDef(0))
      { // 1 - � ��������� ���
        tED_BE->Text = FCode;
        cExtLab->Caption = FText;
      }
     else
      { // 0 - � ��������� �����
        tED_BE->Text = FText;
        cExtLab->Caption = FCode;
      }
     riDataChange(this);
   }
  __finally
   {
   }
  return FCode; 
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::ExtBtnClick(TObject *Sender, int AButtonIndex)
{
  UnicodeString SrcCode = "";
  if (AButtonIndex < tED_BE->Properties->Buttons->Count-1)
   {
     if (FExtBtnClick&&SrcDSEditable())
      {
        SrcCode = irquAsStr(SrcDS(),FFlName);
        if (FExtBtnClick(FNode,SrcCode,FSrc,true,(TObject*) FCtrlOwner, AButtonIndex))
         {
           SrcCode = FSetEditValue(SrcCode);
           irquSetAsStr(SrcDS(),FFlName,SrcCode);
         }
      }
   }
  else
   {
     if (SrcDSEditable())
      {
        tED_BE->Text = "";
        cExtLab->Caption = "";
        SrcCode = "";
        irquSetAsStr(SrcDS(),FFlName,SrcCode);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::FSetExtBtnClick(TExtBtnClick AExtBtnClick)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FExtBtnClick = AExtBtnClick;
  if (AExtBtnClick)
   {
     if (SrcDS()->Active)
      {
        UnicodeString SrcCode = "";
        if (IsAppend)
         {
           SrcCode = FNode->AV["default"].Trim();
           if (IsTemplate(FNode))
            SrcCode = _TemplateValue(FNode);
         }
        else
         SrcCode = irquAsStr(SrcDS(),FFlName);
        if (FExtBtnClick(FNode,SrcCode,FSrc, false,(TObject*) FCtrlOwner,0))
         {
           SrcCode = FSetEditValue(SrcCode);
           if (SrcDSEditable())
            irquSetAsStr(SrcDS(),FFlName,SrcCode);
         }
      }
   }
}
//---------------------------------------------------------------------------
__fastcall TriExtEdit::~TriExtEdit()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_BE->Properties->OnButtonClick = NULL;
  delete cExtLab;
  delete tED_BE;
//  delete CBList;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine) ED->Left = ALeft;
  cExtLab->Width = ED->Left+ED->Width;
  Width = ED->Left+ED->Width+2;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else               *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else               *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     cExtLab->Left = 2; cExtLab->Width = Width-4;
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4; }
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriExtEdit::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEdit::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->AV["showtype"].ToIntDef(0)) xVal = cExtLab->Caption;
  else                                   xVal = tED_BE->Text;
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate;
  ED->Enabled = Lab->Enabled;
  cExtLab->Enabled = Lab->Enabled;
  if (isEdit&&SrcDSEditable() && !ED->Enabled)
   {
     FSetEditValue("");
     irquSetAsStr(SrcDS(),FFlName,"");
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (SrcDSEditable())
   {
     UnicodeString FVal = FSetEditValue(AVal);
     irquSetAsStr(SrcDS(),FFlName,FVal);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriExtEdit::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriExtEdit::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,date,datetime,time,digit,extedit,binary,choice,choiceTree"))
    return (TControl*)ED;
   else
    return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriExtEdit::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEdit::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FNode->AV["showtype"].ToIntDef(0)) return tED_BE->Text;
  else                                   return cExtLab->Caption;
}
//---------------------------------------------------------------------------
Variant __fastcall TriExtEdit::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" 'tagtype not supported'");
}
//---------------------------------------------------------------------------
bool __fastcall TriExtEdit::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (!((TcxButtonEdit*)ED)->Text.Length())  RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

