//---------------------------------------------------------------------------
#ifndef irTreeClassH
#define irTreeClassH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxProgressBar.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "Registry_ICScomp.h"
/*#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxProgressBar.hpp"
#include "cxTextEdit.hpp"
#include "dxStatusBar.hpp"
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
#include "cxLookAndFeels.hpp"
#include "dxSkinBlack.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinCaramel.hpp"
#include "dxSkinCoffee.hpp"
#include "dxSkinDarkRoom.hpp"
#include "dxSkinDarkSide.hpp"
#include "dxSkinFoggy.hpp"
#include "dxSkinGlassOceans.hpp"
#include "dxSkiniMaginary.hpp"
#include "dxSkinLilian.hpp"
#include "dxSkinLiquidSky.hpp"
#include "dxSkinLondonLiquidSky.hpp"
#include "dxSkinMcSkin.hpp"
#include "dxSkinMoneyTwins.hpp"
#include "dxSkinOffice2007Black.hpp"
#include "dxSkinOffice2007Blue.hpp"
#include "dxSkinOffice2007Green.hpp"
#include "dxSkinOffice2007Pink.hpp"
#include "dxSkinOffice2007Silver.hpp"
#include "dxSkinOffice2010Black.hpp"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinOffice2010Silver.hpp"
#include "dxSkinPumpkin.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsDefaultPainters.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
#include "dxSkinSeven.hpp"
#include "dxSkinSharp.hpp"
#include "dxSkinSilver.hpp"
#include "dxSkinSpringTime.hpp"
#include "dxSkinStardust.hpp"
#include "dxSkinSummer2008.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"    */
//---------------------------------------------------------------------------
class PACKAGE TirTreeClassForm : public TForm
{
__published:	// IDE-managed Components
        TTreeView *ClassTree;
        TPanel *BtnPanel;
        TLabel *FindLab;
        TPanel *Panel1;
        TcxCheckBox *SelGroupChB;
        TcxButtonEdit *QSearchED;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TdxStatusBar *StatusBar;
        TdxStatusBarContainerControl *dxStatusBar1Container0;
        TcxProgressBar *PrBar;
        void __fastcall BtnOkClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ClassTreeEditing(TObject *Sender, TTreeNode *Node,
          bool &AllowEdit);
        void __fastcall QSearchEDPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall ClassTreeChange(TObject *Sender, TTreeNode *Node);
private:	// User declarations
//    TRegistry_ICS* RegComp;
    int FindPos;
    void** NodeMap;
    __int64       FclCode;
    UnicodeString  tmpName,tmpName1;
    bool __fastcall FGetTemplateMode();
    void __fastcall FSetTemplateMode(bool AMode);
    void __fastcall FSetClCode(__int64 AVal);
    void __fastcall FInit(void* ANode,UnicodeString AClName,UnicodeString AWhere,TClassCallBack ACallBack, TpFIBQuery* AQuery);
public:		// User declarations
       UnicodeString clText,clExtCode;
       __property __int64       clCode = {read = FclCode, write=FSetClCode};
       bool       clSelectGroup;
       __int64       clCodeLast;
       __property bool TemplateMode = {read=FGetTemplateMode, write=FSetTemplateMode};
       __fastcall TirTreeClassForm(TComponent* Owner,void* ANode,UnicodeString AClName,UnicodeString AWhere,TClassCallBack ACallBack, TRegistry_ICS* ARegComp);
       __fastcall TirTreeClassForm(TComponent* Owner,void* ANode,UnicodeString AClName,UnicodeString AWhere,TClassCallBack ACallBack, TpFIBQuery* AQuery);
};
//---------------------------------------------------------------------------
extern PACKAGE TirTreeClassForm *irTreeClassForm;
//---------------------------------------------------------------------------
#endif
