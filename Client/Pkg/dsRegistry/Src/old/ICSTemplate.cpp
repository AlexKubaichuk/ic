//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop


//---------------------------------------------------------------------------
#include "ICSTemplate.h"
#include "RegSeparator.h"
#include "Reg_DMF.h"
#include "msgdef.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
/*
<!ELEMENT root (fl|gr|separator)+>            //������
<!ATTLIST root ref CDATA #IMPLIED>  // ref - uid �������������� ��� ��������
                                    // ����������� ������
                                    // ���� ref = NULL - ������ �����������
                                    // ��� ������� ����� �� �������� ������������

<!ELEMENT gr (fl|gr|separator)+>              // ������
<!ATTLIST gr ref CDATA #REQUIRED>   // ������ ��� �������� ����������� ����� ��
                                    // �������������� �� ������� ��������� �������
                                    // � uid-�� = ref, ������ ������� ����� ���� ������
                                    // ���� "choiceBD" - ������ ������ �� �������������

<!ELEMENT fl EMPTY>                 // ����
<!ATTLIST fl ref CDATA #REQUIRED    // �������� ���� �� �������� �������� ����������
             def CDATA #IMPLIED     // ��������� ����� ref - uid ��������
             show (0|1) "1"         // ���������� ��������� �������� � ???
             req (0|1) "0"          // �������������� ������� ��������
                            "0" - �� �����������
                            "1" - �����������
                            "2" - �������������� ������������ ��������� ��������
             en  (0|1) "1">         // def - �������� �� ���������
                                    // en - ����������� ��� ��������������

<!ELEMENT separator EMPTY>          // �����������

 AFields -  XML - �������� ����� �������
 ACompPanel - ������ ��� ���������� ��������� �������
 AquFree -  ��������� �� FIBQuery ��� ������� � ��
 ATreePath - ������ ���� � ����� �������� ���
 ATree - ��������� �� TTagNode - � ����������� ���������
*/
//---------------------------------------------------------------------------
__fastcall EICSTemplateError::EICSTemplateError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall TICSTemplate::TICSTemplate(UnicodeString AFields, TPanel *ACompPanel, TADODataSet *AquFree, UnicodeString APref, UnicodeString ATreePath, TTagNode *ATree,TColor AReqColor, bool ANativeStyle, TcxLookAndFeelKind ALFKind, bool AIsTemplateField)
    : TObject()
{
   FADO = true;
   FIsTemplateField = AIsTemplateField;
   FInit(AFields, ACompPanel, AquFree, APref, ATreePath, ATree, AReqColor, ANativeStyle, ALFKind);
}
//---------------------------------------------------------------------------
__fastcall TICSTemplate::TICSTemplate(UnicodeString AFields, TPanel *ACompPanel, TpFIBQuery *AquFree, UnicodeString APref, UnicodeString ATreePath, TTagNode *ATree, TColor AReqColor, bool ANativeStyle, TcxLookAndFeelKind ALFKind, bool AIsTemplateField)
    : TObject()
{
   FADO = false;
   FIsTemplateField = AIsTemplateField;
   FInit(AFields, ACompPanel, (TDataSet*)AquFree, APref, ATreePath, ATree, AReqColor, ANativeStyle, ALFKind);
}
//---------------------------------------------------------------------------
void __fastcall TICSTemplate::FInit(UnicodeString AFields, TPanel *ACompPanel, TDataSet *AquFree, UnicodeString APref, UnicodeString ATreePath, TTagNode *ATree, TColor AReqColor, bool ANativeStyle, TcxLookAndFeelKind ALFKind)
{
   FHeight = 0;
   FWidth  = 0;
   FLastTop = 0;
   FLastHeight = 0;

   FReqColor = AReqColor;
   xquFree = AquFree;
   CList = NULL; FltFl = NULL; Root = NULL;
   if (ATree)
    {
      Root = ATree;
      isCreate = false;
    }
   else if (ATreePath.Length())
    {
      Root = new TTagNode(NULL);
      Root->LoadFromFile(ATreePath);
      isCreate = true;
    }
   else
    {
      _MSG_ERRA(FMT(icsTemplateNoClassDef), FMT(icsErrorMsgCaption)); return;
    }
   if (!AFields.Length())
    {
      _MSG_ERRA(FMT(icsTemplateNoFltDef), FMT(icsErrorMsgCaption)); return;
    }
   CompPanel = ACompPanel;
   FltFl = new TTagNode(NULL);
   FltFl->AsXML = AFields;
   // ���������� �������
   CList = new TStringList;
   xTop = 2; MaxW = 20;

   FCtrList = new TRegEDContainer(CompPanel->Owner,CompPanel);
   FCtrList->DefXML = Root;
   FCtrList->ReqColor = AReqColor;
   FCtrList->TablePref = APref;
   FCtrList->ADOSrc = FADO; 
   FCtrList->NativeStyle = ANativeStyle;
   FCtrList->TemplateField = FIsTemplateField;
   FCtrList->LFKind = ALFKind;
   if (FADO)
    FCtrList->ADOTemplateQuery = (TADODataSet*)AquFree;
   else
    FCtrList->FIBTemplateQuery = (TpFIBQuery*)AquFree;
   MS = false;
   int FlCount = FltFl->GetCount(true, "fl,separator");
   if ((FlCount+2)*21 > Screen->Height/2)
    MS = true;
   FlCounter = 0;
   FNextCol = false;
   FltFl->Iterate(CreateTmpl,*&UnicodeString(""));

   MaxW = max(MaxW,CompPanel->Width - 5);
   for (int i = 0; i < CList->Count; i++)
    {
     if (CList->Objects[i])
      {
        ((TWinControl*)CList->Objects[i])->Width = MaxW-10;
      }
     else
      {
        FCtrList->TemplateItems[CList->Strings[i]]->Width = MaxW-10;
        FCtrList->TemplateItems[CList->Strings[i]]->UpdateChanges();
      }
    }
   if (CList->Count)
    {
      FHeight = FLastTop + FLastHeight + 3;
      if (MS&&FNextCol)
       FWidth  = MaxW*2+10;
      else
       FWidth  = MaxW+5;

      CompPanel->Height = FHeight;
      CompPanel->Width =  FWidth;
      FltFl->Iterate(FSetValue,*&UnicodeString(""));
    }
   else
    {
      FHeight = 0;
      FWidth  = 0;
    }
   FUpperFunc = "Upper";
}
//---------------------------------------------------------------------------
__fastcall TICSTemplate::~TICSTemplate()
{
  for (int i = 0; i < CList->Count; i++)
   {
     if (CList->Objects[i]) delete ((TRegSeparator*)CList->Objects[i]);
   }
  delete FCtrList;
  if (CList) delete CList;
  if (FltFl) delete FltFl;
  if ((Root != NULL) && isCreate) delete Root;
}
//---------------------------------------------------------------------------
TWinControl* __fastcall TICSTemplate::GetFirstEnabledControl()
{
  TControl *tmpCtrl;
  TRegEDItem *tmpItem;
  TTagNode *itNode = FltFl->GetFirstChild();
  while (itNode)
   {
     if (FCtrList->GetTemplateControl(itNode->AV["ref"]))
      {
        tmpItem = FCtrList->TemplateItems[itNode->AV["ref"]];
        tmpCtrl = tmpItem->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           return (TWinControl*)tmpCtrl;
         }
      }
     itNode = itNode->GetNext();
   }
  return NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::CreateTmpl(TTagNode *itxTag, UnicodeString& tmp)
{
  TRegEDItem *tmpItem;
  TTagNode   *FNode;


  FlCounter++;
  if (itxTag->CmpName("fl"))
   {
     FNode = Root->GetTagByUID(itxTag->AV["ref"]);
     if (FNode)
      {
        if (FNode->CmpAV("isedit","no")&& !FNode->CmpAV("depend",""))
         {
           tmpItem = FCtrList->AddTemplateItem(FNode, itxTag->AV["def"]);
           if (FNode->CmpName("choiceBD,class"))
            tmpItem->SetEnableEx((bool)itxTag->AV["en"].ToIntDef(1));
           else
            tmpItem->SetEnable((bool)itxTag->AV["en"].ToIntDef(1));
           tmpItem->SetIsVal(true);
           CList->AddObject(FNode->AV["uid"],(TObject*)0);
           tmpItem->Visible = false;
         }
        else
         {
           tmpItem = FCtrList->AddTemplateItem(FNode, itxTag->AV["def"]);
           if (FNode->CmpName("choiceBD,class"))
            tmpItem->SetEnableEx((bool)itxTag->AV["en"].ToIntDef(1));
           else
            tmpItem->SetEnable((bool)itxTag->AV["en"].ToIntDef(1));
           tmpItem->SetIsVal(true);
           if (MS&&FNextCol)
            tmpItem->Left = CompPanel->Width+2;
           else
            tmpItem->Left = 2;
           tmpItem->Top = xTop;
           xTop += 2 + tmpItem->Height;
           MaxW = max(tmpItem->Width,MaxW);
           if (FLastTop < xTop) FLastTop = xTop;
           FLastHeight = tmpItem->Height;
           CList->AddObject(FNode->AV["uid"],(TObject*)0);
         }
        if (itxTag->AV["req"].ToIntDef(0) == 1)
         tmpItem->Required = true;
        else if (!itxTag->AV["req"].ToIntDef(0))
         tmpItem->Required = false;
      }
     else
      throw EICSTemplateError((FMT1(icsTemplateRefError,itxTag->AV["ref"])).c_str());
   }
  else if (itxTag->CmpName("separator"))
   {
     CList->AddObject("",(TObject*)new TRegSeparator(CompPanel, CompPanel->Width, itxTag->AV["comment"]));
     xTop += 3;
     ((TRegSeparator*)CList->Objects[CList->Count-1])->Top = xTop;
     xTop += 12;
     if (MS&&FNextCol)
      ((TRegSeparator*)CList->Objects[CList->Count-1])->Left = CompPanel->Width+2;
     else
      ((TRegSeparator*)CList->Objects[CList->Count-1])->Left = 3;

     if (FLastTop < xTop) FLastTop = xTop;
     FLastHeight = 4;
     if (MS && !FNextCol)
      {
        if ((FlCounter)*21 > Screen->Height/2)
         {
           xTop = 0;
           FNextCol = true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FSetValue(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     if (itxTag->GetAttrByName("def"))
      {
        FCtrList->TemplateItems[itxTag->AV["ref"]]->SetValue(itxTag->AV["def"]);
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FPrepare()
{  // ������ ��������� � xml
   bool IsEmpty = true;

   for (int i = 0; i < CList->Count; i++)
    {
      if (CList->Strings[i].Length())
       IsEmpty &= FCtrList->TemplateItems[CList->Strings[i]]->UpdateChanges();
    }
   return IsEmpty;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::CheckInput()
{
  bool RC = false;
  try
   {
     UnicodeString DDD = "1";
     FltFl->Iterate(FCheckInput,DDD);
     if (DDD.Length())
      {
        if (FltFl->CmpAV("blank","1"))
         RC = true;
        else
         {
           if (!IsEmpty())  RC = true;
           else
            {
              _MSG_ERRA(FMT(icsTemplateReqEmpty), FMT(icsErrorMsgCaption));
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FCheckInput(TTagNode *itxTag, UnicodeString& tmp)
{
  bool RC = false;
  try
   {
     if (itxTag->CmpName("fl") && itxTag->AV["req"].ToIntDef(0))
      RC = !FCtrList->TemplateItems[itxTag->AV["ref"]]->CheckReqValue(NULL);
     if (RC) tmp = "";
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FGetCBCount(TTagNode *itxTag, UnicodeString &ACount)
{
  if (itxTag->CmpName("class"))
   {
     ACount = IntToStr(ACount.ToInt()+1);
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TICSTemplate::Clear()
{
   FltFl->Iterate(FClear,*&UnicodeString(""));
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FClear(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     Variant vEmpty; vEmpty.Empty();  
     _TemplateValue(Root->GetTagByUID(itxTag->AV["ref"])) = vEmpty;
   }
  return false;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetWhere(UnicodeString AAlias)
{
  FPrepare();
  return FGetWhere(FltFl->GetFirstChild(),AAlias);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetWhereClRep(UnicodeString AAlias, TStringList *ASelList)
{
  FSelList = NULL;
  UnicodeString RC = "";
  try
   {
     FSelList = new TStringList;
     ASelList->Clear();
     FPrepare();
     RC = FGetWhere(FltFl->GetFirstChild(),AAlias,true);
     for (int i = 0; i < FSelList->Count; i++)
      ASelList->Add(FSelList->Strings[i]);
   }
  __finally
   {
     if (FSelList) {delete FSelList; FSelList = NULL;}
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetGrSelect(TTagNode *AGroup)
{                                 
  UnicodeString xSel = "Select CODE From CLASS_"+FTabPref;
  TTagNode *FRef = Root->GetTagByUID(AGroup->AV["ref"]);
  if (FRef)
   {
     if (FRef->CmpName("class"))
      xSel += FRef->AV["uid"].UpperCase()+" Where ";
     else                                                
      xSel += FRef->AV["ref"].UpperCase()+" Where ";
   }
  else
   {
      xSel += "_ERROR_ Where ";
   }
  UnicodeString xWhere = FGetWhere(AGroup->GetFirstChild(),"");
  if (xWhere.Length()) return xSel+xWhere;
  else              return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::FGetWhere(TTagNode *AitNode, UnicodeString AAlias, bool AClRep)
{
  UnicodeString xWhere = "";
  UnicodeString itVal,iFName,iOper;
  TTagNode *xTag;
  TTagNode *itNode = AitNode;
  while(itNode)
   {
     if (!itNode->CmpName("separator"))
      {                             
        xTag = Root->GetTagByUID(itNode->AV["ref"]);
        iOper = (!xWhere.Length())? " ":" and ";
        iFName = "R"+xTag->AV["uid"]; 
        if (AAlias.Length()) iFName = AAlias+"."+iFName;
        if (itNode->CmpName("fl"))
         {
           itVal = GetValue(itNode->AV["ref"]);
           if (itVal.Length())
            {
              if (xTag->CmpName("binary"))                  xWhere += iOper+iFName+"="+itVal;
              else if (xTag->CmpName("datetime,time"))      xWhere += iOper+iFName+"='"+itVal.UpperCase()+"'";
              else if (xTag->CmpName("text"))
               {
                 if (FUpperFunc.Length())
                  xWhere += iOper+FUpperFunc+"("+iFName+")='"+itVal.UpperCase()+"'";
                 else
                  xWhere += iOper+"U"+iFName+"='"+itVal.UpperCase()+"'";
               }
              else if (xTag->CmpName("digit"))
               {
                 if (wcschr(itVal.c_str(),';'))
                  {
                    if ((GetPart1(itVal,';').Length())&&(GetPart2(itVal,';').Length()))
                     xWhere += iOper+"("+iFName+">="+GetPart1(itVal,';')+" and "+iFName+"<="+GetPart2(itVal,';')+")";
                    else if (GetPart1(itVal,';').Length())
                     xWhere += iOper+iFName+">="+GetPart1(itVal,';');
                    else if (GetPart2(itVal,';').Length())
                     xWhere += iOper+iFName+"<="+GetPart2(itVal,';');
                  }
                 else
                  xWhere += iOper+iFName+"="+itVal;
               }
              else if (xTag->CmpName("date"))
               {
                 if (wcschr(itVal.c_str(),';'))
                  {
                    if ((GetPart1(itVal,';').Length())&&(GetPart2(itVal,';').Length()))
                     xWhere += iOper+"("+iFName+">='"+GetPart1(itVal,';')+"' and "+iFName+"<='"+GetPart2(itVal,';')+"')";
                    else if (GetPart1(itVal,';').Length())
                     xWhere += iOper+iFName+">='"+GetPart1(itVal,';')+"'";
                    else if (GetPart2(itVal,';').Length())
                     xWhere += iOper+iFName+"<='"+GetPart2(itVal,';')+"'";
                  }
                 else
                  xWhere += iOper+iFName+"='"+itVal+"'";
               }
              else if (xTag->CmpName("choice"))             xWhere += iOper+iFName+"="+itVal;
              else if (xTag->CmpName("choiceBD,class"))
               {
                 if (AClRep)
                  {
                    if (IsClassSelect(itVal,xTag))
                     {
                       xWhere += iOper+iFName+" in (_SEL"+IntToStr(FSelList->Count)+")";
                       FSelList->Add(GetClassSelectOnly(itVal,xTag,FTabPref));
                     }
                    else
                     xWhere += iOper+GetClassSelect(itVal,xTag,FTabPref,iFName);
                  }
                 else
                  xWhere += iOper+GetClassSelect(itVal,xTag,FTabPref,iFName);
               }
              else if (xTag->CmpName("choiceTree"))
               {
                 UnicodeString tmpTreeVal = GetPart1(itVal,':');
                 if (tmpTreeVal.ToIntDef(0))
                  xWhere += iOper+"("+iFName+">="+GetPart1(GetPart2(itVal,':'),'#')+" and "+iFName+"<="+GetPart2(GetPart2(itVal,':'),'#')+")";
                 else
                  xWhere += iOper+iFName+"="+GetPart1(GetPart2(itVal,':'),'#');
               }
              else if (xTag->CmpName("extedit"))
               {
                 if (FGetExtWhere)
                  {
                   UnicodeString SrcCode = itVal;
                   if (FGetExtWhere(xTag,SrcCode,NULL,true,FCtrList,0))
                     xWhere += iOper+SrcCode;
                  }
               }
            }
         }
        else
         {
           if (GetGrSelect(itNode).Length())
            xWhere += iOper+iFName+" in ("+GetGrSelect(itNode)+")";
         }
      }
     itNode = itNode->GetNext();
   }
  return xWhere;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetValue(UnicodeString AUID)
{
  return FGetValue(AUID.UpperCase(),false);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetName(UnicodeString AUID)
{
  TTagNode *xTag;
  xTag = Root->GetTagByUID(AUID);
  if (xTag)
   {
     return xTag->AV["name"]+"="+FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr();
   }
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TICSTemplate::FSetWidth(int AWidth)
{

   MaxW = AWidth - 5;
   for (int i = 0; i < CList->Count; i++)
    {
     if (CList->Objects[i])
      {
        ((TWinControl*)CList->Objects[i])->Width = MaxW+10;
      }
     else
      {
        FCtrList->TemplateItems[CList->Strings[i]]->Width = MaxW-10;
        FCtrList->TemplateItems[CList->Strings[i]]->UpdateChanges();
      }
    }
   FWidth = AWidth;
   CompPanel->Width = AWidth;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::IsEmpty()
{
  return FPrepare();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetSelectedUpdateSQL()
{
  UnicodeString RC = "";
  FltFl->Iterate(FGetSelectedTextValues,RC);
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FGetSelectedUpdateSQL(TTagNode *itxTag, UnicodeString& Values)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *xTag;
     xTag = Root->GetTagByUID(itxTag->AV["ref"]);
     if (xTag)
      {
        if (itxTag->AV["show"].ToIntDef(1))
         {
           if (FCtrList->TemplateItems[xTag->AV["uid"]]->Enabled)
            {
              UnicodeString FVal = FGetValue(itxTag->AV["ref"],false);
              if (FVal.Length())
               {
                 if (Values.Length())
                  Values += ","+AsTypeUpdate(xTag, FVal);
                 else
                  Values += AsTypeUpdate(xTag, FVal);
               }
            }
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::AsTypeUpdate(TTagNode *ADefNode, UnicodeString AVal)
{
    // ���������� �������� AVal � ������� SQL
    // � ������������ � ����� ���� Src
    // ������������ ��� ������������ "UPDATE"
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  UnicodeString FName = "R"+ADefNode->AV["UID"].UpperCase();
  if (ADefNode->CmpName("class,choiceBD,choiceTree,binary,choice,digit"))
   return FName+"="+StringReplace(AVal,",",".",rFlag);
  else if (ADefNode->CmpName("date,time,datetime,text,extedit"))
   return FName+"='"+StringReplace(AVal,"'","`",rFlag)+"'";
  else
   return "_ERROR_GET_VALUE_";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::GetSelectedTextValues(bool CanFieldName)
{
  UnicodeString RC = "";
  if (CanFieldName)
   FltFl->Iterate(FGetSelectedTextFieldValues,RC);
  else
   FltFl->Iterate(FGetSelectedTextValues,RC);
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FGetSelectedTextValues(TTagNode *itxTag, UnicodeString& Values)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *xTag;
     xTag = Root->GetTagByUID(itxTag->AV["ref"]);
     if (xTag)
      {
        if (itxTag->AV["show"].ToIntDef(1))
         {
           if (FGetValue(itxTag->AV["ref"],true).Length())
            Values += FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr(true) + "\n";
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FGetSelectedTextFieldValues(TTagNode *itxTag, UnicodeString& Values)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *xTag;
     xTag = Root->GetTagByUID(itxTag->AV["ref"]);
     if (xTag)
      {
        if (itxTag->AV["show"].ToIntDef(1))
         {
           if (FGetValue(itxTag->AV["ref"],true).Length())
            Values += xTag->AV["name"] + "#" + FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr(true) + "\n";
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTemplate::FGetValue(UnicodeString AUID, bool ABinaryNames)
{
  TTagNode *xTag;
  xTag = Root->GetTagByUID(AUID);
  Variant iVal = _TemplateValue(xTag);
  if (!iVal.IsEmpty())
   {
     if (xTag->CmpName("binary"))
      {
        if (ABinaryNames)
         return ((int)iVal)? xTag->AV["name"]:xTag->AV["invname"];
        else
         return UnicodeString(((int)iVal)? "1":"0");
      }
     else if (xTag->CmpName("text,datetime,time,choice,choiceBD,class,extedit,digit,choiceTree"))
      return UnicodeString((wchar_t*)iVal);
     else if (xTag->CmpName("date"))
      return TDateTime((wchar_t*)iVal).FormatString("dd.mm.yyyy");
     else return "";
   }
  else return "";
}
//---------------------------------------------------------------------------
void __fastcall TICSTemplate::FSetTreeBtnClick(TTreeBtnClick AOnTreeBtnClick)
{
   FOnTreeBtnClick = AOnTreeBtnClick;
   FltFl->Iterate(FSetTreeFormList,*&UnicodeString(""));
}
//---------------------------------------------------------------------------
void __fastcall TICSTemplate::FSetExtBtnClick(TExtBtnClick AOnExtBtnClick)
{
   FOnExtBtnClick = AOnExtBtnClick;
   FltFl->Iterate(FSetExtClick,*&UnicodeString(""));
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FSetTreeFormList(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *tmpNode = Root->GetTagByUID(itxTag->AV["ref"]);
     if (tmpNode)
      {
        if (tmpNode->CmpName("choiceTree"))
        FCtrList->TemplateItems[tmpNode->AV["uid"]]->OnTreeBtnClick = FOnTreeBtnClick;
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TICSTemplate::FSetExtClick(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *tmpNode = Root->GetTagByUID(itxTag->AV["ref"]);
     if (tmpNode)
      {
        if (tmpNode->CmpName("extedit"))
        FCtrList->TemplateItems[tmpNode->AV["uid"]]->OnExtBtnClick = FOnExtBtnClick;
      }
   }
  return false;
}
//---------------------------------------------------------------------------

