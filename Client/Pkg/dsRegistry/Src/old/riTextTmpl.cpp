//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riTextTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED         ((TcxMaskEdit*)ED)
#define tED_CB      ((TcxComboBox*)ED)
//---------------------------------------------------------------------------
__fastcall TriTextTmpl::TriTextTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
  Lab = new TLabel(this);  Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Top = 2;            Lab->Left = 4;
  Lab->Caption = FNode->AV["name"]+":";
  FOnGetFormat = ACtrlOwner->OnGetFormat;
  if (isFV)
   {
     ED = new TcxComboBox(this);
     xSize = Rect(0,0,16,21);
     ED->Parent =  this;
     tED_CB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
     tED_CB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
     tED_CB->Properties->Items->Clear();
     xSize.Right = 5*FNode->AV["length"].ToInt()+GetSystemMetrics(SM_CXVSCROLL);
     ED->Enabled = ED->Enabled&&isEnabled;
     tED_CB->Properties->MaxLength = FNode->AV["length"].ToInt();
     tED_CB->CMinW = 60;
   }
  else
   {
     ED = new TcxMaskEdit(this);
     ED->Parent =  this;
     tED->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
     tED->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
     int xW = 5*FNode->AV["length"].ToInt();
     if (xW > 180) xW = 180;
     xSize = Rect(0,0,xW,21);
     tED->Properties->MaxLength = FNode->AV["length"].ToInt();
     if (FNode->AV["format"].Length())
      {
       tED->Properties->MaskKind = emkRegExprEx;
       if (FOnGetFormat)
        tED->Properties->EditMask = FOnGetFormat(FNode->AV["format"]);
       else
        tED->Properties->EditMask = FNode->AV["format"];
      }
   }
  ED->Constraints->MinWidth = 60;
  if ((xSize.Right+Lab->Width+Lab->Left+10) >= 255)
   {
     TwoLine = true;  Lab->Top = 0;
     ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
     CMaxH = 34; CMinH = 34;
     Height = CMaxH;
   }
  else
   {
     ED->Left = Lab->Width+5;
     ED->Top = xSize.Top;
   }
  if (!FCtrlOwner->TemplateField && !_TemplateValue(FNode).IsEmpty())
   {
     if (!isFV)
      tED->Text = UnicodeString((wchar_t*)_TemplateValue(FNode));
   }
  ED->Width = xSize.Right+10;
  ED->Height = xSize.Bottom;
  ED->Parent =  this;
  Width = ED->Left+ED->Width;
  Lab->FocusControl = ED;
  if (Parent->ClassNameIs("TriTextTmpl"))
   {
     Anchors.Clear();
     Anchors << akLeft << akRight << akTop;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
  tED->Properties->OnChange    = riDataChange;
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetFixedVar(UnicodeString AVals)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (isFV && FNode->CmpName("text"))
    {
      UnicodeString tmp = tED_CB->Text;
      tED_CB->Properties->Items->Text = AVals;
      int ind = tED_CB->Properties->Items->IndexOf(tmp);
      if (ind != -1)
       {
         tED_CB->ItemIndex = ind;
         tED_CB->Text = tmp;
       }
      else
       tED_CB->Text = tmp;
    }
}
//---------------------------------------------------------------------------
__fastcall TriTextTmpl::~TriTextTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     if (!isFV) tED->Properties->OnChange    = NULL;
     else       tED_CB->Properties->OnChange = NULL;
     if (!isFV) delete tED;
     else       delete tED_CB;
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED) ED->Left = ALeft;
  if (ED)                                Width = ED->Left+ED->Width+4;
   if (TmpCtrl)
    if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4+ED2->Width;
     else
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else                                            *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FieldCast(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   UnicodeString DDD;
   bool isED = Sender->ClassNameIs("TcxMaskEdit");
   if (isED)
    {
      if (!((TcxMaskEdit*)Sender)->Text.Length()) return;
      DDD = ((TcxMaskEdit*)Sender)->Text.Trim();
    }
   else
    {
      if (!((TcxComboBox*)Sender)->Text.Length()) return;
      DDD = ((TcxComboBox*)Sender)->Text.Trim();
    }
   UnicodeString SSS = "";
   UnicodeString xCast = FNode->AV["cast"].LowerCase();
   if (xCast == "lo")
    {
      DDD = DDD.LowerCase();
    }
   else if (xCast == "up")
    {
      DDD = DDD.UpperCase();
    }
   else if (xCast == "up1st")
    {
      DDD = DDD.SubString(1,1).UpperCase() + DDD.SubString(2,DDD.Length()-1).LowerCase();
    }
   if (isED) ((TcxMaskEdit*)Sender)->Text = DDD;
   else      ((TcxComboBox*)Sender)->Text = DDD;
   if (SrcDSEditable()) irquSetAsStr(SrcDS(),"R"+AVUp(FNode,"uid"),DDD);

}
//---------------------------------------------------------------------------
bool __fastcall TriTextTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!isFV) tED->Properties->OnChange((TObject*)ED);
  else       tED_CB->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriTextTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled)
      {
        if (!isFV)
         {
           if (tED->Text.Length())            xVal += tED->Text;
         }
        else
         {
           if (tED_CB->Text.Length())            xVal += tED_CB->Text;
         }
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Lab->Enabled = AEnable;
  ED->Enabled  = AEnable;
  if (AEnable)
   {
     if (isFV) tED_CB->Style->Color = (Required)? FReqColor:clWindow;
     else      tED->Style->Color = (Required)? FReqColor:clWindow;
   }
  else
   {
     if (isFV)
      {
        tED_CB->Style->Color = clBtnFace;
        tED_CB->Text = "";
      }
     else
      {
        tED->Style->Color = clBtnFace;
        tED->Text = "";
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (!isFV)  tED->Text = AVal;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  if (!isFV)
   tED->Properties->OnChange  = riDataChange;
  else
   tED_CB->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled||isVal)
      {
        if (!isFV)
         {
           if (tED->Text.Length())            xVal = Variant(tED->Text);
         }
        else
         {
           if (tED_CB->Text.Length())         xVal = Variant(tED_CB->Text);
         }
      }
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (tED->Text.Trim().Length())     Src = tED->Text.Trim();
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
/*
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        if (TmpCtrl) ItmIndex  = tED_CB->ItemIndex;
        else         ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= (int)TmpCtrl)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               {
                 if (TmpCtrl) labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
                 else         labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
               }
            }
         }
      }
*/
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (tED->Text.Trim().Length())     Src = tED->Text.Trim();
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriTextTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriTextTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriTextTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!isFV)
   return tED->Text;
  else
   return tED_CB->Text;
}
//---------------------------------------------------------------------------
Variant __fastcall TriTextTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" 'tagtype not supported'");
}
//---------------------------------------------------------------------------
bool __fastcall TriTextTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (!isFV)
               RC = tED->Text.Trim().Length();
              else
               RC = tED_CB->Text.Trim().Length();
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  if (isFV) tED_CB->Style->Color = (Required)? FReqColor:clWindow;
  else      tED->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

