//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "Registry_ICScomp.h"
#include "DxLocale.h"
#include "irClassif.h"
#include "ICSRegList.h"
#include "irTreeClass.h"
#include "ICSRegSelList.h"
#include "Reg_DMF.h"
#include "ICSRegProgress.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TRegistry_ICS *)
{
        new TRegistry_ICS(NULL);
}
//---------------------------------------------------------------------------
__fastcall TExtBarItem::TExtBarItem()
{
  FCapt = ""; FImagIdx = -1; FGetButtonEnabled = NULL; FExtItemClick = NULL;
}
//---------------------------------------------------------------------------
__fastcall TExtBarItemList::TExtBarItemList() : TList()
{
}
//---------------------------------------------------------------------------
TExtBarItem* __fastcall TExtBarItemList::FGetExtItem(int Idx)
{
  return (TExtBarItem*) Items[Idx];
}
//---------------------------------------------------------------------------
TExtBarItem* __fastcall TExtBarItemList::AddItem()
{
  Add(new TExtBarItem());
  return (TExtBarItem*) Items[Count-1];
}
//---------------------------------------------------------------------------
__fastcall TExtBarItemList::~TExtBarItemList()
{
  for (int i = 0; i < Count; i++)
   {
     Item[i]->OnGetItemEnabled = NULL;
     Item[i]->OnItemClick = NULL;
     delete (TExtBarItem*)Items[i];
   }
}
//---------------------------------------------------------------------------
__fastcall TRegistry_ICS::TRegistry_ICS(TComponent* Owner)
        : TComponent(Owner)
{
  FUseClassDisControls = false;
  FUseReportSetting = false;
  FDBUser = "";
  FDBPassword ="";
  FDBCharset = "";
  RegistryKey = "Software\\ICS Ltd.\\ClassifSize";
  FUnitListCreated = NULL;
  FBuyCaption = "";
//  FLargeIcons = false;
//  FFetchAll = true;
  FUnitListDeleted = NULL;
  FOnExtFilterSet = NULL;
  FOnExtFilterClear = NULL;
  FOnExtFilterGetText = NULL;
  FOnCtrlDataChange = NULL;

  FRefreshList = false;
  FExtBarItems = new TExtBarItemList();
  FClassTreeColor = clBtnFace;
  FReqColor       = clInfoBk;
  FUpperFunc = "Upper";
  FDTFormat = "dd.mm.yyyy";
  DM = NULL;
//  FNativeStyle = true;
//  FLFKind = lfStandard;
}
//---------------------------------------------------------------------------
__fastcall TRegistry_ICS::~TRegistry_ICS()
{
  Active = false;
  delete FExtBarItems;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetFullEdit(bool AFullEdit)
{
  FFullEdit = AFullEdit;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetActive(bool AActive)
{
  if (AActive == true)
   {
      if (!FDBName.Length())
      {
        MessageBox(NULL,FMT(icsRegistryErrorDBNameNotDef).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
        return;
      }
      if ((!FXMLName.Length()) && !FLoadExtXML)
      {
        MessageBox(NULL,FMT(icsRegistryErrorNoRegDef).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
        return;
      }
      if (FActive) return;
      FActive = true;
      DxLocale();
      bool RC = Connect(FDBName, FXMLName);
      if (!RC)
       {
         MessageBox(NULL,FMT(icsRegistryErrorDBConnect).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
         if (DM)
          {
            if (DM->REG_DB->Connected) DM->REG_DB->Close();
            if (DM->UnitList)
             {
               if (FUnitListDeleted) FUnitListDeleted((TICSRegListForm*)DM->UnitList);
               delete (TICSRegListForm*)DM->UnitList;
             }
            DM->UnitList = NULL;
            if (DM) delete DM;
            DM = NULL;
          }
         return;
       }
      // ������������ ������ ���������������
      DM->Classes = DM->RegUnit->GetChildByName("classes");
      DM->FClassData = new TICSClassData(DM->RegUnit, DM->quFree);
      // ������������ ������ ���������� ���������������
      TList *TmpList = new TList;
      this->GetTreeChoice(TmpList);
      TTagNode *xN, *itNode;
      if (ComponentState.Contains(csDesigning))
       {
         if (TmpList->Count > 0)
          {
            PrF = new TICSRegProgressForm(NULL);
            ((TForm*)PrF)->Show();
            FDesOnInit = FSetInit;
            for (int i = 0; i < TmpList->Count; i++)
             {
               xN = (TTagNode*)TmpList->Items[i];
               itNode = DM->RegUnit->GetTagByUID(xN->AV["ref"]);
               DM->TreeClasses->AddObject(xN->AV["uid"],(TObject*)new TirTreeClassForm(NULL,itNode,xN->AV["name"],xN->AV["wherestr"],FDesOnInit,this));
             }
            FDesOnInit = NULL;
            ((TForm*)PrF)->Close();
            delete ((TICSRegProgressForm*)PrF); PrF = NULL;
          }
       }
      else
       {
         for (int i = 0; i < TmpList->Count; i++)
          {
            xN = (TTagNode*)TmpList->Items[i];
            itNode = DM->RegUnit->GetTagByUID(xN->AV["ref"]);
            DM->TreeClasses->AddObject(xN->AV["uid"],(TObject*)new TirTreeClassForm(NULL,itNode,xN->AV["name"],xN->AV["wherestr"],FOnInit,this));
          }
       }
      delete TmpList; TmpList = NULL;
   }
  else
   {
     if (!FActive) return;
     FActive = false;
     if (DM->UnitList)
      {
        if (FUnitListDeleted) FUnitListDeleted((TICSRegListForm*)DM->UnitList);
        delete (TICSRegListForm*)DM->UnitList;
      }
//     DM->UnitList = NULL;
     if (DM->REG_DB->Connected) DM->REG_DB->Close();
     if (DM) delete DM;
     DM = NULL;
//     Sleep(200);
   }
 FActive = AActive;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetClassEnabled(bool AClassEnabled)
{
  FClassEnabled = AClassEnabled;
  if (DM)
   if (DM->UnitList) ((TICSRegListForm*)DM->UnitList)->CheckAccess();
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetListEditEnabled(bool AListEditEnabled)
{
  FListEditEnabled = AListEditEnabled;
  if (DM)
   if (DM->UnitList) ((TICSRegListForm*)DM->UnitList)->CheckAccess();
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetOpenCardEnabled(bool AOpenCardEnabled)
{
  FOpenCardEnabled = AOpenCardEnabled;
  if (DM)
   if (DM->UnitList) ((TICSRegListForm*)DM->UnitList)->CheckAccess();
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetUListTop(int ATop)
{
  FUListTop = ATop;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetUListLeft(int ALeft)
{
  FUListLeft = ALeft;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetDBName(UnicodeString  ADBName)
{
  FDBName = ADBName;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetXMLName(UnicodeString  AXMLName)
{
  FXMLName = AXMLName;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::ShowClasses(UnicodeString AClassID, TRegClassType AShowType, UnicodeString AGroups)
{
  if (!FActive) return;
  if (!FClassEnabled) return;
  TirClassifForm *ClassDlg = NULL;
  try
   {
     ClassDlg = new TirClassifForm(NULL,DM->Classes, this, AShowType, AGroups, -1);
     ClassDlg->SetActiveClass(AClassID);
     ClassDlg->ShowModal();
   }
  __finally
   {
     if (ClassDlg) delete ClassDlg;
     ClassDlg = NULL;
   }
}
//---------------------------------------------------------------------------
void  __fastcall TRegistry_ICS::ShowClasses(TRegClassType AShowType, UnicodeString AGroups, int AClassNum)
{
  if (!FActive) return;
  if (!FClassEnabled) return;
  TirClassifForm *ClassDlg = NULL;
  try
   {
     ClassDlg = new TirClassifForm(NULL,DM->Classes, this, AShowType, AGroups, AClassNum);
     ClassDlg->ShowModal();
   }
  __finally
   {
     if (ClassDlg) delete ClassDlg;
     ClassDlg = NULL;
   }
}
//---------------------------------------------------------------------------
void  __fastcall TRegistry_ICS::ShowUnitList( bool isModal )
{
  if (!FActive) return;
  if (!DM) return;
  if (!DM->UnitList)
   {
     if (FUnitListCreated) FUnitListCreated((TICSRegListForm*)DM->UnitList);
     DM->UnitList = new TICSRegListForm(Application->Owner, UnitListTop, UnitListLeft,this);
   }

  // ������� ������ ������
  if (DM->SearchList) delete[] DM->SearchList;
  DM->SearchList = NULL;
  DM->SearchIndex = -1;
  DM->SeachCount = 0;

  ((TICSRegListForm*)DM->UnitList)->Top  = FUListTop;
  ((TICSRegListForm*)DM->UnitList)->Left = FUListLeft;

  if ( isModal )
    ((TICSRegListForm*)DM->UnitList)->ShowModal();
  else
    ((TICSRegListForm*)DM->UnitList)->Show();
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::ShowSelUnitList(UnicodeString ATmpl,TStringList *ACodeList)
{
  if (!FActive) return;
  if (!DM) return;
  TSelListForm *Dlg = new TSelListForm(NULL, UnitListTop, UnitListLeft,this,ATmpl);
  if (Dlg)
   {
     Dlg->ShowModal();
     if (Dlg->ModalResult == mrOk)
      {
        Dlg->GetSelCodes(ACodeList);
      }
     delete Dlg;
   }
}
//---------------------------------------------------------------------------
void  __fastcall TRegistry_ICS::RefreshUnitList()
{
  if (!FActive) return;
  if (!DM->UnitList) return;
  if (!((TForm*)DM->UnitList)->Visible) return;
//  ((TICSRegListForm*)DM->UnitList)->Top  = FUListTop;
//  ((TICSRegListForm*)DM->UnitList)->Left = FUListLeft;
  ((TICSRegListForm*)DM->UnitList)->ListReopen();
}
//---------------------------------------------------------------------------
void  __fastcall TRegistry_ICS::FSetInqComp(TControl *AInqComp)
{
  FInqComp  = AInqComp;
}
//---------------------------------------------------------------------------
bool  __fastcall TRegistry_ICS::IsListShowing()
{
  if (!FActive) return false;
  if (DM->UnitList)
   {
     return ((TForm*)DM->UnitList)->Showing;
   }
  return false;
}
//---------------------------------------------------------------------------
bool  __fastcall TRegistry_ICS::GetSelected(__int64 *ACode, UnicodeString &AUnitStr)
{
  if (!IsListShowing()) return false;
  if (DM->UnitList)
   {
     __int64 XXCode = ((TICSRegListForm*)DM->UnitList)->GetSelCode();
     if (XXCode == -1) return false;
     *ACode   = XXCode;
     AUnitStr = ((TICSRegListForm*)DM->UnitList)->GetSelStr();
     return true;
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::GetSelectedUnits(TStrings *ADest)
{
  if (!IsListShowing()) return;
  if (DM->UnitList)
   ((TICSRegListForm*)DM->UnitList)->GetSelectedUnits(ADest);
}
//---------------------------------------------------------------------------
bool __fastcall TRegistry_ICS::Connect(UnicodeString ADBName, UnicodeString AXMLName)
{
   try
    {
      if (!FActive) return false;
      try
       {
         if (!DM) DM = new TicsREG_DM(this);
       }
      catch (Exception &E)
       {
         MessageBox(NULL,FMT1(icsRegistryErrorLoadDM,E.Message).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
         throw;
       }
      if (!DM) return false;
      DM->RegUnit  = new TTagNode(NULL);
      DM->SelRegUnit  = new TTagNode(NULL);
      UnicodeString fPar;
      DM->REG_DB->DBParams->Clear();
      fPar = (FDBUser.Length())?FDBUser:UnicodeString("SYSDBA");
      DM->REG_DB->DBParams->Add(("user_name="+fPar).LowerCase().c_str());
      fPar = (FDBPassword.Length())?FDBPassword:UnicodeString("MASTERKEY");
      DM->REG_DB->DBParams->Add(("password="+fPar).LowerCase().c_str());
      DM->REG_DB->DBParams->Add("sql_role_name=");
      if (FDBCharset.Length())
       DM->REG_DB->DBParams->Add(("lc_ctype="+FDBCharset).LowerCase().c_str());

      DM->REG_DB->DBName = ADBName;
      DM->REG_DB->Open();

      if (FileExists(AXMLName))
       {
         DM->RegUnit->LoadFromFile(AXMLName);
         DM->SelRegUnit->LoadFromFile(AXMLName);
       }
      else
       {
         if (FLoadExtXML)
          {
            FLoadExtXML(DM->RegUnit, DM->SelRegUnit);
          }
         else
          return false;
       }
      return true;
    }
   catch(EFIBInterBaseError& E)
    {
      if (E.SQLCode == -902)
       {
         MessageBox(NULL,FMT1(icsRegistryErrorDBNotExist,ADBName).c_str(),FMT1(icsRegistryErrorDBErrorMsgCaption,IntToStr(E.SQLCode)).c_str(),MB_ICONSTOP);
         return false;
       }
      else if (E.SQLCode == -904)
       {
         MessageBox(NULL,FMT(icsRegistryErrorNoServer).c_str(),FMT1(icsRegistryErrorDBErrorMsgCaption,IntToStr(E.SQLCode)).c_str(),MB_ICONSTOP);
         return false;
       }
      else
       {
         MessageBox(NULL,E.Message.c_str(),FMT1(icsRegistryErrorDBErrorMsgCaption,IntToStr(E.SQLCode)).c_str(),MB_ICONSTOP);
         return false;
       }
    }
  catch (...)
    {
      MessageBox(NULL,FMT(icsRegistryErrorLoadRegDef).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
      return false;
    }
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::GetTreeChoice(TList *AList)
{
  tmpChTree = new TList;
  DM->RegUnit->Iterate(GetTreeCh,*&UnicodeString(""));
  AList->Assign(tmpChTree,laCopy,NULL);
  if (tmpChTree) delete tmpChTree;
  tmpChTree = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TRegistry_ICS::GetTreeCh(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag->CmpName("choiceTree"))
    tmpChTree->Add(itTag);
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetInit(int APersent, UnicodeString AMess)
{
  if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
  ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegistry_ICS::GetUnitTabName()
{
  if (!FActive) return "";
  if (!DM->RegUnit) return "";
  if (!DM->RegUnit->GetChildByName("unit")) return "";
  return (TablePrefix+DM->RegUnit->GetChildByName("unit")->AV["tblname"]).UpperCase();
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::InvadeClass(TComboBox* AClassCB, __int64 AClassCode, bool AisFirstNULL, UnicodeString AClassUID, UnicodeString APref)
{
  InvadeClassCB(AClassCB, AClassCode, AisFirstNULL, DM->RegUnit->GetTagByUID(AClassUID),DM->xquFree,APref,"",DM->RegUnit);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetTemplateFields(TTagNode *ADescription)
{
  TTagNode *FTmpl = ADescription->GetChildByName("maskfields");
  UnicodeString tmpl = "";
  if (FTmpl)
   {
     FTmpl = FTmpl->GetFirstChild();
     if (FTmpl->CmpName("field"))
      { // old template set
        TTagNode *tmp = new TTagNode(NULL);
        tmp->Name = "root";
        tmp->AV["ref"] = "";
        while (FTmpl)
         {
           if (FTmpl->CmpName("field"))
            tmp->AddChild("fl","ref="+FTmpl->AV["ref"]);
           else
            tmp->AddChild("separator");
           FTmpl = FTmpl->GetNext();
         }
        tmpl = tmp->AsXML;
        delete tmp;
      }
     else
      tmpl = FTmpl->AsXML;
   }
  return tmpl;
}
//---------------------------------------------------------------------------
TStringList* __fastcall TRegistry_ICS::FGetTreeFormList()
{
  if (DM) return DM->TreeClasses;
  else    return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::AddTreeFormList(UnicodeString ANodeRef, UnicodeString ANodeName, UnicodeString AClassRef)
{
  if (FActive == true)
   {
     if (DM&&DM->TreeClasses)
      {
        if (DM->TreeClasses->IndexOf(ANodeRef) == -1)
         {
           TTagNode *itNode;
           itNode = DM->RegUnit->GetTagByUID(AClassRef);
           DM->TreeClasses->AddObject(ANodeRef,(TObject*)new TirTreeClassForm(NULL,itNode,ANodeName,"",FOnInit,this));
         }
      }
   }
}
//---------------------------------------------------------------------------
TdxBar*  __fastcall TRegistry_ICS::GetListExtTB(void)
{
  if (DM->UnitList) return ((TICSRegListForm*)DM->UnitList)->GetExtTB();
  else              return NULL;
}

//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetUpperFunc(UnicodeString  AParam)
{
  FUpperFunc = AParam;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetDTFormat(UnicodeString  AParam)
{
  FDTFormat = AParam;
}
//---------------------------------------------------------------------------
void __fastcall TRegistry_ICS::FSetClassShowExpanded(UnicodeString AClassShowExpanded)
{
  FClassShowExpanded = AClassShowExpanded;
}
//---------------------------------------------------------------------------

/*************************************************************
* �������� ������� ������ � ������� ������ �� ������������   *
* ���������: ��������� �.�. 23.09.08                         *
* ������: --->                                               *
*************************************************************/
__int64 __fastcall TRegistry_ICS::GetMCode(UnicodeString AGenName)
{
    return DM->GetMCode ( AGenName );
}

//---------------------------------------------------------------------------

void __fastcall TRegistry_ICS::qtExec ( UnicodeString ASQL, bool aCommit )
{
    DM->qtExec ( ASQL, aCommit );
}

//---------------------------------------------------------------------------

void __fastcall TRegistry_ICS::qtStart ( UnicodeString ASQL )
{
    DM->qtStart ( ASQL );
}

//---------------------------------------------------------------------------

void __fastcall TRegistry_ICS::qtExecCommit ( )
{
    DM->qtExecCommit ( );
}

//---------------------------------------------------------------------------

void __fastcall TRegistry_ICS::qtRollback ( )
{
    DM->qtRollback ( );
}

//---------------------------------------------------------------------------

TpFIBQuery* TRegistry_ICS::getQueryLink ( )
{
    return DM->quFree;
}
/*************************************************************
* �������� ������� ������ � ������� ������ �� ������������   *
* ���������: ��������� �.�. 23.09.08                         *
* <--- �����                                                 *
*************************************************************/

