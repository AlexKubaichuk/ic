//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riUnitgroupTmpl.h"

//---------------------------------------------------------------------------

//#include "TreeClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TriUnitgroupTmpl::TriUnitgroupTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   Lab = new TLabel(this);  Lab->Parent = this;
   Lab->Transparent = true;
   Lab->Top = 2;            Lab->Left = 4;
   Lab->Caption = FNode->AV["name"]+":";
   Lab->Top = -1; Lab->Left = 5;
   Lab->Color = clBackground;
   Lab->Caption = " "+FNode->AV["name"]+" ";
   Lab->Font->Color = clWhite;
   Lab->Font->Style.Clear();
   Lab->Font->Style = TFontStyles() << fsBold;
   CMaxH = 500;
   CMinH = 22;
   BevelInner = bvRaised; BevelOuter = bvLowered;
   Width = xSize.Right+10;
   if (Parent->ClassNameIs("TriUnitgroupTmpl"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetOnDependValues()
{
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriUnitgroupTmpl::~TriUnitgroupTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  *AEDLeft = -1;
  *ALabR = Lab->Left+Lab->Width;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
bool __fastcall TriUnitgroupTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriUnitgroupTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Lab->Enabled = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroupTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriUnitgroupTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriUnitgroupTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriUnitgroupTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriUnitgroupTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" unitgroup has no value");
}
//---------------------------------------------------------------------------
bool __fastcall TriUnitgroupTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return true;
}
//---------------------------------------------------------------------------

