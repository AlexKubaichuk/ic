//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riDigit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define dbED         ((TcxDBMaskEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TriDigit::TriDigit(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   Lab = new TLabel(this);  Lab->Parent = this;
   Lab->Transparent = true;
   Lab->Top = 2;            Lab->Left = 4;
   Lab->Caption = FNode->AV["name"]+":";
   ED = new TcxDBMaskEdit(this);
   ED->Parent = this;
   dbED->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
   dbED->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
   dbED->Properties->OnChange = FDefaultDataChange;
   FIsFloat = (FNode->AV["decimal"].ToIntDef(-1) != -1);

   dbED->Properties->Alignment->Horz = taRightJustify;
   dbED->Properties->MaskKind = emkRegExpr;
   dbED->Properties->EditMask = icsCalcMask(FIsFloat, FNode->AV["digits"].ToIntDef(0), FNode->AV["decimal"].ToIntDef(0), FNode->AV["min"]);
   dbED->Properties->OnValidate         = FEditValidate;

//   dbED->Ctl3D = is3D;
   dbED->DataBinding->DataField = FFlName; dbED->DataBinding->DataSource = FSrc;
   if (Required) dbED->Style->Color = FReqColor;
   if (IsTemplate(FNode))
    {
      if (SrcDSEditable()) irquSetAsVar(SrcDS(),FFlName,_TemplateValue(FNode));
    }
   ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
   int xW = 5*FNode->AV["digits"].ToInt();
   if (xW > 180) xW = 180;
   xSize = Rect(0,0,xW,21);
//   dbED->Properties->MaxLength = FNode->AV["digits"].ToInt();
   if (ED)
    {
      ED->Constraints->MinWidth = 60;
      ED->Parent = this;
      ED->Width = xSize.Right+10;
      if (ED->Width < 60) ED->Width = 60;
      ED->Top = xSize.Top;
      ED->Height = xSize.Bottom;
      if (Lab->Width > (AParent->Width-2))
       {
         Lab->Font->Size = Lab->Font->Size - 1;
         Lab->Update();
         if (Lab->Width > (AParent->Width - 2))
          Lab->Width = AParent->Width-2;
       }
      ED->Left = Lab->Left + Lab->Width + 5;
      if ((ED->Left + ED->Width) > (AParent->Width - 2))
       {
         TwoLine = true;  Lab->Top = 0;
         ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
         CMaxH = 34; CMinH = 34;
         Height = CMaxH;
         if ((ED->Left + ED->Width) > (AParent->Width - 2))
          ED->Width = AParent->Width - 4 - ED->Left;
         if (FNode->CmpName("text"))
          ED->Width = AParent->Width - 12 - ED->Left;
       }
    }
   else
    Width = xSize.Right+15;
   if (Lab&&ED)
    Lab->FocusControl = ED;
   if (Parent->ClassNameIs("TriDigit"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriDigit::~TriDigit()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   dbED->Properties->OnChange    = NULL;
   delete dbED;
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  if (ED)                                Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else               *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else               *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else         {ED->Left = Width - ED->Width - 2;}
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriDigit::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  dbED->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDigit::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled)
      {
        if (dbED->Text.Length())  xVal += dbED->Text;
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled  = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate))
   {
     dbED->Style->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
     dbED->Style->Color = clBtnFace;
     if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
      dbED->Text = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (!isFV)
   {
     if (SrcDSEditable()) irquSetAsStr(SrcDS(),FFlName,AVal);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  dbED->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled||isVal)
      {
        if (dbED->Text.Length()) xVal = Variant(dbED->Text);
      }
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
/*
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        if (TmpCtrl) ItmIndex  = tED_CB->ItemIndex;
        else         ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= (int)TmpCtrl)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               {
                 if (TmpCtrl) labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
                 else         labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
               }
            }
         }
      }
*/
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDigit::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDigit::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDigit::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return dbED->Text;
}
//---------------------------------------------------------------------------
Variant __fastcall TriDigit::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (dbED->Text.Length()) return Variant(dbED->Text);
  else                     return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriDigit::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (!dbED->Text.Length())  RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           UnicodeString ErrMessage;
           if (Lab) ErrMessage = Lab->Caption;
           else     ErrMessage = FNode->AV["name"];
           ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
           MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
         }
      }
   }
  if (RC)
   {
     if (ED)
      {
        if (ED->Enabled && dbED->Text.Length())
         {
           if (!FIsFloat)
            { // �����
              int FMin, FMax, FVal;
              FMin = FNode->AV["min"].ToIntDef(0);
              FMax = FNode->AV["max"].ToIntDef(0);
              FVal = dbED->Text.ToIntDef(-1);
              RC   = !((FVal < FMin)||(FVal > FMax));
            }
           else
            { // ������������  StrToFloat
              double FMin, FMax, FVal;
              FMin = ICSStrToFloatDef(FNode->AV["min"],0);
              FMax = ICSStrToFloatDef(FNode->AV["max"],0);
              FVal = ICSStrToFloatDef(dbED->Text,-1);
              RC   = !((FVal < FMin)||(FVal > FMax));
            }
           if (!RC)
            {
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];

              ErrMessage = FMT3(icsriDigitValRange,ErrMessage,FNode->AV["min"],FNode->AV["max"]);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
            }
         }
      }
   }
  if (!RC)
   {
     if (APC) APC->ActivePageIndex = this->Tag;
     ED->SetFocus();
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriDigit::FEditValidate(TObject *Sender, Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  if (FIsFloat)
   { //������������
     if (UnicodeString((wchar_t*)DisplayValue).Length())
      DisplayValue = Variant
                     (
                       StringReplace
                       (
                         StringReplace(UnicodeString((wchar_t*)DisplayValue),".",FormatSettings.DecimalSeparator,rFlag),
                         ",",
                         FormatSettings.DecimalSeparator,rFlag
                       )
                     );
   }
  Error = false;
}
//---------------------------------------------------------------------------

