//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riChoiceTreeTmpl.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_BE       dynamic_cast<TcxButtonEdit*>(ED)
#define cExtLab      static_cast<TLabel*>(CBList->Items[0])
//---------------------------------------------------------------------------
__fastcall TriChoiceTreeTmpl::TriChoiceTreeTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  //������ ����� ��������
  Lab = new TLabel(this);   Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Caption = FNode->AV["name"]+":";
  // ������ ����� ��� ���������� ��������
  CBList = new TList;
  CBList->Add(new TLabel(this));
  cExtLab->Parent = this;
  cExtLab->Transparent = true;
  cExtLab->AutoSize = false;
  cExtLab->WordWrap = true;
  cExtLab->Color = LtColor(clBtnFace,16);
  cExtLab->Font->Color = clGray;
  cExtLab->Font->Size = 6;
  // ������ �������
  ED = new TcxButtonEdit(this);
  tED_BE->Parent =  this;
  tED_BE->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_BE->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  tED_BE->Properties->ClickKey = ShortCut(VK_DOWN,TShiftState() << ssAlt);
  tED_BE->Properties->ReadOnly = true;
  tED_BE->Properties->ViewStyle = vsHideCursor;
  TcxEditButton *tmpBtn = tED_BE->Properties->Buttons->Add();
  tmpBtn->Kind = bkText;
  tmpBtn->Caption = "X";
//      tmpBtn->Glyph->LoadFromResourceName((int)HInstance,"CLEAR");
  tED_BE->Properties->OnButtonClick = TmpTreeBtnClick;
  // ������ ���������
  if (Required) tED_BE->Style->Color = FReqColor;
  Lab->FocusControl = ED;
  TreeClasses = NULL;
  FTreeDlg = NULL;
  FchIsGroup = false;
  FchCodeBeg = "";
  FchCodeEnd = "";
  FchExtCode = "";
  FchText = "";
  if (!FCtrlOwner->TemplateField && !_TemplateValue(FNode).IsEmpty())
    FSetEditValue(FGetIsGroup(_TemplateValue(FNode)),FGetBeginCode(_TemplateValue(FNode)),FGetEndCode(_TemplateValue(FNode)));

  // ������������ � �������
  Lab->Top = 2;  Lab->Left = 2;
  if (Lab->Width > (AParent->Width-2))
   {
     Lab->Font->Size = Lab->Font->Size - 1;
     Lab->Update();
     if (Lab->Width > (AParent->Width - 2))
      Lab->Width = AParent->Width-2;
   }
  ED->Constraints->MinWidth = 60;

  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
     tED_BE->Left = Lab->Width+5;
     tED_BE->Top = 0;
   }
  else
   {
     tED_BE->Left = 2;
     tED_BE->Top = Lab->Height+2;
     TwoLine = true;
   }
  tED_BE->Width = AParent->Width - 4 - tED_BE->Left;

  if ((tED_BE->Left + tED_BE->Width) > (AParent->Width - 4))
   {
     TwoLine = true;
     tED_BE->Left = 4;
     tED_BE->Top = Lab->Height;
     tED_BE->Width = AParent->Width - 4 - tED_BE->Left;
   }
  cExtLab->Left = Lab->Left;
  cExtLab->Width = Width-4;
  cExtLab->Height = (cExtLab->Height-2)*(FNode->AV["linecount"].ToIntDef(1)-1);
  cExtLab->Top = tED_BE->Top+tED_BE->Height;
  CMaxH = cExtLab->Top + cExtLab->Height+2; CMinH = CMaxH;
  Height = CMaxH;
//***************************************************************************
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
//  tED_CB->OnChange = riDataChange;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::TmpTreeBtnClick(TObject *Sender, int AButtonIndex)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString SrcCode = "";
  if (AButtonIndex == 0)
   {
     if (FTreeBtnClick)
      {
        bool FIsGroup = false;
        UnicodeString FCodeBeg = "";
        UnicodeString FCodeEnd = "";
        UnicodeString FExtCode = "";
        UnicodeString FText = "";
        if (FTreeBtnClick(FNode, FIsGroup, FCodeBeg, FCodeEnd, FExtCode, FText, true))
         {
           if (FCodeBeg.Length())
            {
              // ������ "group:begin_code#end_code"
              FSetEditValue(FIsGroup, FCodeBeg, FCodeEnd);
/*
               _TemplateValue(FNode) = Variant(FGetTemplateVal(FIsGroup,FCodeBeg,FCodeEnd));
              else
               _TemplateValue(FNode) = Variant::Empty();
*/
            }
         }
      }
     else if (TreeClasses)
      {
        int clIndex = TreeClasses->IndexOf(FNode->AV["uid"]);
        TirTreeClassForm *Dlg = (TirTreeClassForm*)TreeClasses->Objects[clIndex];
        Dlg->Caption = FNode->AV["dlgtitle"];
        if (!_TemplateValue(FNode).IsEmpty())
         Dlg->clCode = FGetBeginCode(_TemplateValue(FNode)).ToIntDef(0);
        //        __int64 FClCode = irquAsStr(SrcDS(),FFlName).ToIntDef(0);
//        Dlg->clCode = FClCode;
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
           FSetEditValue(Dlg->clSelectGroup, IntToStr(Dlg->clCode), IntToStr(Dlg->clCodeLast));
/*
           if (FSetEditValue(Dlg->clSelectGroup, IntToStr(Dlg->clCode), IntToStr(Dlg->clCodeLast)))
            _TemplateValue(FNode) = Variant(FGetTemplateVal(Dlg->clSelectGroup,Dlg->clCode,Dlg->clCodeLast));
           else
            _TemplateValue(FNode) = Variant::Empty();
*/
         }
      }
     else
      {
        if (!FTreeDlg)
         {
           TTagNode *clDefNode = FCtrlOwner->DefXML->GetTagByUID(FNode->AV["ref"]);
           FTreeDlg = new TirTreeClassForm(this, clDefNode, FNode->AV["name"], FNode->AV["wherestr"],TClassCallBack(NULL), FCtrlOwner->FIBTemplateQuery);
         }
        FTreeDlg->Caption = FNode->AV["dlgtitle"];
        FTreeDlg->TemplateMode = !FCtrlOwner->TemplateField;
        if (!_TemplateValue(FNode).IsEmpty())
         FTreeDlg->clCode = FGetBeginCode(_TemplateValue(FNode)).ToIntDef(0);
//           __int64 FClCode = irquAsStr(SrcDS(),FFlName).ToIntDef(0);
//           Dlg->clCode = FClCode;
        FTreeDlg->ShowModal();
        if (FTreeDlg->ModalResult == mrOk)
         {
           FSetEditValue(FTreeDlg->clSelectGroup, IntToStr(FTreeDlg->clCode), IntToStr(FTreeDlg->clCodeLast));
/*
           if (FSetEditValue(FTreeDlg->clSelectGroup, IntToStr(FTreeDlg->clCode), IntToStr(FTreeDlg->clCodeLast)))
            _TemplateValue(FNode) = Variant(FGetTemplateVal(FTreeDlg->clSelectGroup,FTreeDlg->clCode,FTreeDlg->clCodeLast));
           else
            _TemplateValue(FNode) = Variant::Empty();
*/
         }
      }
   }
  else
   {
     FSetEditValue(false,"");
//     _TemplateValue(FNode) = Variant::Empty();
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::FGetIsGroup(UnicodeString ASrc)
{
  return (bool)(GetPart1(ASrc,':').ToIntDef(0));
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetBeginCode(UnicodeString ASrc)
{
  return GetPart1(GetPart2(ASrc,':'),'#');
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetEndCode(UnicodeString ASrc)
{
  return GetPart2(ASrc,'#');
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetTemplateVal(bool AIsGroup, int ABegin, int AEnd)
{
  return FGetTemplateVal(AIsGroup, IntToStr(ABegin), IntToStr(AEnd));
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::FGetTemplateVal(bool AIsGroup, UnicodeString ABegin, UnicodeString AEnd)
{
  return IntToStr((int)AIsGroup)+":"+ABegin+"#"+AEnd;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FSetTreeBtnClick(TTreeBtnClick ATreeBtnClick)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   FTreeBtnClick = ATreeBtnClick;
}
//---------------------------------------------------------------------------
__fastcall TriChoiceTreeTmpl::~TriChoiceTreeTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_BE->Properties->OnButtonClick = NULL;
  delete cExtLab;
  delete tED_BE;
  TreeClasses = NULL;
  if (FTreeDlg) delete FTreeDlg;
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::FSetEditValue(bool AIsGroup, UnicodeString AValue, UnicodeString ALastValue)
{
  bool RC = false;
  try
   {
     if (AValue.ToIntDef(-1) != -1)
      {
        try
         {
           TTagNode *clN = FCtrlOwner->GetNode(AVUp(FNode,"ref"));
           irExecProc("CLASSDATA_"+FTabPref+AVUp(clN,"tblname"), quFree, "CL_CODE", Variant((__int64)AValue.ToIntDef(-1)));
           cExtLab->Caption = UnicodeString((AIsGroup)? UnicodeString(FMT(icsriChoiceTreeSubClass)+"\n").c_str():_T(""))+irquAsStr(quFree,"P_NAME");
           tED_BE->Text     = irquAsStr(quFree,"P_EXTCODE");
           FchIsGroup = AIsGroup;
           FchCodeBeg = AValue;
           FchCodeEnd = ALastValue;
           FchExtCode = irquAsStr(quFree,"P_EXTCODE");
           FchText    = irquAsStr(quFree,"P_NAME");
           _TemplateValue(FNode) = Variant(FGetTemplateVal(FchIsGroup, FchCodeBeg, FchCodeEnd));
           RC = true;
         }
        __finally
         {
           irExecCloseProc(quFree,true);
         }
      }
     else
      {
        cExtLab->Caption = "";
        tED_BE->Text     = "";
        _TemplateValue(FNode) = Variant::Empty();
      }
     riDataChange(this);
   }
  __finally
   {
   }
  return RC; 
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine) ED->Left = ALeft;
  cExtLab->Width = ED->Left+ED->Width;
  Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else               *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else               *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     cExtLab->Left = 2; cExtLab->Width = Width-4;
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4; }
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  riDataChange(this);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return UnicodeString((FchIsGroup)?UnicodeString(FMT(icsriChoiceTreeSubClass)+"\n").c_str():_T(""))+FchText;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate;
  ED->Enabled = Lab->Enabled;
  cExtLab->Enabled = Lab->Enabled;
  if (!ED->Enabled)
   {
     FSetEditValue(false,"");
//     _TemplateValue(FNode) = Variant::Empty();
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  FSetEditValue(FGetIsGroup(AVal),FGetBeginCode(AVal),FGetEndCode(AVal));
/*
  if (FSetEditValue(FGetIsGroup(AVal),FGetBeginCode(AVal),FGetEndCode(AVal)))
   _TemplateValue(FNode) = Variant(AVal);
  else
   _TemplateValue(FNode) = Variant::Empty();
*/
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (FchCodeBeg.ToIntDef(0))
      Src = FGetTemplateVal(FchIsGroup, FchCodeBeg, FchCodeEnd);
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceTreeTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceTreeTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTreeTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  // ������ "group:begin_code#end_code"
  return FGetTemplateVal(FchIsGroup, FchCodeBeg, FchCodeEnd);
}
//---------------------------------------------------------------------------
Variant __fastcall TriChoiceTreeTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (tED_BE->Text.Length()) return Variant(tED_BE->Text);
  else                       return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTreeTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (!((TcxButtonEdit*)ED)->Text.Length())  RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTreeTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  tED_BE->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

