//---------------------------------------------------------------------------

#ifndef RegEDFuncH
#define RegEDFuncH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <DB.hpp>
//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
#include "AxeUtil.h"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
#include <ADODB.hpp>

#include "cxGridDBTableView.hpp"
//---------------------------------------------------------------------------
//namespace IcsRegED
//{
//---------------------------------------------------------------------------
typedef enum {bcInsert,bcEdit,bcDelete} TbtType;
//---------------------------------------------------------------------------
//#include "RegDefs.h"
typedef bool __fastcall (__closure *TExtBtnClick)(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource, bool isClicked, TObject* ACtrlList, int ABtnIdx);
typedef bool __fastcall (__closure *TExtBtnGetBitmapEvent)(TTagNode *ItTag, int ABtnIdx, TObject *AStm, UnicodeString &AHint);
typedef bool __fastcall (__closure *TTreeBtnClick)(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked);
typedef bool __fastcall (__closure *TGetExtData)(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
typedef bool __fastcall (__closure *TGetCompValue)(TTagNode *ItTag, Variant &AVal);
typedef bool __fastcall (__closure *TRegEditButtonClickEvent)(TbtType AType);
typedef UnicodeString __fastcall (__closure *TRegGetOrderEvent)(TTagNode *ADefNode);
typedef UnicodeString __fastcall (__closure *TRegGetFormatEvent)(UnicodeString ADefFormat);
typedef void __fastcall (__closure *TRegListGetColWidthEvent)(TcxGridDBColumn *AColumn, int AMaxWidth);

//---------------------------------------------------------------------------
//class TRegEDContainer;
class PACKAGE T_tmp : public TObject
{
private:
public:
    bool __fastcall getClassCode(TTagNode *itTag, UnicodeString &UID);
};
//---------------------------------------------------------------------------
extern PACKAGE void       __fastcall irExecSelect(UnicodeString ASQL, TDataSet *AQuery);
extern PACKAGE void       __fastcall irExecClose(TDataSet *AQuery,bool ACommit);
extern PACKAGE void       __fastcall irExecProc(UnicodeString AProcName, TDataSet *AQuery, UnicodeString AParamName, Variant AParam);
extern PACKAGE void       __fastcall irExecCloseProc(TDataSet *AQuery,bool ACommit);
extern PACKAGE bool       __fastcall irIsEof(TDataSet *AQuery);
extern PACKAGE UnicodeString __fastcall irquFieldStr(TDataSet *AQuery, int AIdx);
extern PACKAGE UnicodeString __fastcall irquFieldName(TDataSet *AQuery, int AIdx);
extern PACKAGE void       __fastcall irquNext(TDataSet *AQuery);
extern PACKAGE int        __fastcall irquFieldCount(TDataSet *AQuery);
extern PACKAGE UnicodeString __fastcall irquAsStr(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE void       __fastcall irquSetAsStr(TDataSet *AQuery, UnicodeString AName, UnicodeString AVal);
extern PACKAGE __int64    __fastcall irquAsInt(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE void       __fastcall irquSetAsInt(TDataSet *AQuery, UnicodeString AName, int AVal);
extern PACKAGE float      __fastcall irquAsFloat(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE void       __fastcall irquSetAsFloat(TDataSet *AQuery, UnicodeString AName, float AVal);
extern PACKAGE Variant    __fastcall irquAsVar(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE void       __fastcall irquSetAsVar(TDataSet *AQuery, UnicodeString AName, Variant AVal);
extern PACKAGE TDateTime  __fastcall irquAsDate(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE void       __fastcall irquSetAsDate(TDataSet *AQuery, UnicodeString AName, TDateTime AVal);
extern PACKAGE void       __fastcall irquSetAsNull(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE bool       __fastcall irquIsNull(TDataSet *AQuery, UnicodeString AName);
extern PACKAGE Variant&   __fastcall _TemplateValue(TTagNode *ANode);

extern PACKAGE bool __fastcall IsTemplate(TTagNode *itTag);
extern PACKAGE UnicodeString __fastcall FieldCast(TTagNode *ADefNode, UnicodeString AVal);
extern PACKAGE void __fastcall ClearClassCBItems(TStrings* AClassCB);
extern PACKAGE void __fastcall FInvadeClassCB(TStrings* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, bool allFld, TDataSet *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot);
extern PACKAGE void __fastcall InvadeClassCB(TComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode,              TDataSet *quFree, UnicodeString APref, UnicodeString ACodeVals, TTagNode *ADefRoot);
extern PACKAGE void __fastcall InvadeClassCB(TComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, TpFIBQuery *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot);
extern PACKAGE void __fastcall InvadeClassCB(TcxComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode,              TDataSet *quFree, UnicodeString APref, UnicodeString ACodeVals, TTagNode *ADefRoot);
extern PACKAGE void __fastcall InvadeClassCB(TcxComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, TpFIBQuery *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot);
extern PACKAGE UnicodeString __fastcall GetWhere(TTagNode *ADesc, __int64 AUCode, TStringList *AList, UnicodeString APref, UnicodeString AUpperFunc);
extern PACKAGE bool __fastcall IsClassSelect(UnicodeString ACode,TTagNode *ANode);
UnicodeString __fastcall FClassSelect(TTagNode *ANode,UnicodeString ACode,UnicodeString APref, UnicodeString AFlName);
extern PACKAGE UnicodeString __fastcall GetClassSelect(UnicodeString ACode,TTagNode *ANode,UnicodeString APref,UnicodeString AFlName = "");
extern PACKAGE UnicodeString __fastcall GetClassSelectOnly(UnicodeString ACode,TTagNode *ANode,UnicodeString APref);
extern PACKAGE Extended __fastcall ICSStrToFloat(UnicodeString AVal);
extern PACKAGE Extended __fastcall ICSStrToFloatDef(UnicodeString AVal, Extended ADefVal);
extern PACKAGE UnicodeString __fastcall icsCalcMask(bool AIsFolat, int ADigits, int ADecimal, UnicodeString AMin);
#endif
//---------------------------------------------------------------------------

