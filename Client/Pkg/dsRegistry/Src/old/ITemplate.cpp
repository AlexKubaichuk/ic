//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ITemplate.h"
#include "irTreeClass.h"
#include "Reg_DMF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
//#define lComp ((TWinControl*)CList->Items[CList->Count-1])
TITemplateForm *ITemplateForm;
//---------------------------------------------------------------------------
__fastcall TITemplateForm::TITemplateForm(TComponent* Owner,TRegistry_ICS* ARegComp, UnicodeString ATemplXML)
    : TForm(Owner)
{
   Caption            = FMT(icsITemplateCaption);
   OkBtn->Caption     = FMT(icsITemplateApplyBtn);
   CancelBtn->Caption = FMT(icsITemplateCancelBtn);
   FRoot = ARegComp->DM->RegUnit;
   Tmpl = new TICSTemplate(ATemplXML,CompPanel,ARegComp->DM->quFree,ARegComp->TablePrefix,"",FRoot,ARegComp->RequiredColor,ARegComp->NativeStyle,ARegComp->LFKind, true);
   Tmpl->UpperFunc = ARegComp->UpperFunc;
   FTreeFormList = ARegComp->DM->TreeClasses;
   Tmpl->OnTreeBtnClick = FOnTreeBtnClick;
   Tmpl->OnExtBtnClick = ARegComp->OnExtBtnClick;
   Tmpl->OnGetExtWhere = ARegComp->OnGetExtWhere;
   Height = CompPanel->Height+(Height-ClientHeight)+BtnPanel->Height+BtnBevel->Height;
   Width = CompPanel->Width;
   ActiveControl = Tmpl->GetFirstEnabledControl();
}
//---------------------------------------------------------------------------
void __fastcall TITemplateForm::OkBtnClick(TObject *Sender)
{
   if (Tmpl->CheckInput())
    {
      ModalResult = mrOk;
    }
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall TITemplateForm::GetWhere(UnicodeString ATabAlias)
{
   return Tmpl->GetWhere(ATabAlias);
}
//---------------------------------------------------------------------------
bool  __fastcall TITemplateForm::IsEmpty()
{
   return Tmpl->IsEmpty();
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall TITemplateForm::GetSelectedText(bool CanField)
{
   return Tmpl->GetSelectedTextValues(CanField);
}
//---------------------------------------------------------------------------
void  __fastcall TITemplateForm::Clear()
{
   Tmpl->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TITemplateForm::FormDestroy(TObject *Sender)
{
  if (Tmpl) delete Tmpl;
//  if (FRoot) delete FRoot;
}
//---------------------------------------------------------------------------
bool __fastcall TITemplateForm::FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked)
{
  bool RC = false;
  try
   {
     if (isClicked)
      {
        int clIndex = FTreeFormList->IndexOf(ItTag->AV["uid"]);
        if (clIndex != -1)
         {
           TirTreeClassForm *Dlg = (TirTreeClassForm*)FTreeFormList->Objects[clIndex];
           Dlg->Caption = ItTag->AV["dlgtitle"];
           Dlg->TemplateMode = false;
           Dlg->ShowModal();
           if (Dlg->ModalResult == mrOk)
            {
              RC = true;
              AIsGroup = Dlg->clSelectGroup;
              ACodeBeg = IntToStr(Dlg->clCode);
              ACodeEnd = IntToStr(Dlg->clCodeLast);
              AExtCode = Dlg->clExtCode;
              AText =    Dlg->clText;
            }
         }
        else
         {
           MessageBox(NULL,cFMT1(icsIErrTreeFormMsg,ItTag->AV["name"]),cFMT(icsErrorMsgCaption),MB_ICONSTOP);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TITemplateForm::CancelBtnClick(TObject *Sender)
{
   Tmpl->Clear();
}
//---------------------------------------------------------------------------
bool __fastcall TITemplateForm::SetcValue(TTagNode *itTag, UnicodeString &UID)
{
  if (itTag->GetAttrByName("uid"))
   if (FRoot->GetTagByUID(itTag->AV["uid"]))
    _TemplateValue(itTag) = _TemplateValue(FRoot->GetTagByUID(itTag->AV["uid"]));
  return false;
}
//---------------------------------------------------------------------------

