//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "RegEDContainer.h"
#include "riBinary.h"
#include "riBinaryTmpl.h"
#include "riChoice.h"
#include "riChoiceBD.h"
#include "riChoiceBDTmpl.h"
#include "riChoiceTmpl.h"
#include "riChoiceTree.h"
#include "riChoiceTreeTmpl.h"
#include "riDate.h"
#include "riDatetime.h"
#include "riDatetimeTmpl.h"
#include "riDateTmpl.h"
#include "riDigit.h"
#include "riDigitTmpl.h"
#include "riExtEdit.h"
#include "riExtEditTmpl.h"
#include "riText.h"
#include "riTextTmpl.h"
#include "riTime.h"
#include "riTimeTmpl.h"
#include "riUnitgroup.h"
#include "riUnitgroupTmpl.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall ERegEDContainerError::ERegEDContainerError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegEDContainerDSError::ERegEDContainerDSError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegEDError::ERegEDError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegEDTemplateError::ERegEDTemplateError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegLabError::ERegLabError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall TRegEDContainer::TRegEDContainer(TComponent *AOwner, TWinControl *AParent)
{
  if (!AOwner)  throw ERegEDContainerError(FMT(icsRegEDContainerErrorNoOwner));
  FOwner = AOwner;
  if (!AParent) throw ERegEDContainerError(FMT(icsRegEDContainerErrorNoParent));
  FPlaceCtrl = AParent;
  FCtrlDataChange = NULL;
  FADOSrc = false;
  FTmplDS  = NULL;
  FDataSrc = NULL;
  FDefXML  = NULL;
  FXMLList = NULL;
  FReqColor = clInfoBk;
  FTablePref = "";
  FClassData = NULL;
  FNativeStyle = true;
  FLFKind = lfStandard;

}
//---------------------------------------------------------------------------
__fastcall TRegEDContainer::~TRegEDContainer()
{
  ClearEditItems();
  ClearTemplateItems();
  ClearLabItems();
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::ReiterateClear(TTagNode *ANode, TRegEDMap AMap)
{
  TTagNode *itNode = ANode->GetLastChild();
  while (itNode)
   {
     ReiterateClear(itNode, AMap);
     itNode = itNode->GetPrev();
   }
  if (ANode->CmpName("binary,choice,choiceBD,class,choiceTree,date,datetime,time,digit,text,extedit,unitgroup"))
   {
     if (ANode->AV["uid"].Trim().Length())
      {
        TRegEDMap::iterator FRC = AMap.find(ANode->AV["uid"].Trim());
        if (FRC != AMap.end())
         {
           if (FRC->second)
            {
//              TRegEDItem* regItem = FRC->second;
              if (FRC->second->DefNode->CmpName("choiceBD,class"))
               {
                 delete FRC->second;
                 FRC->second = NULL;
               }
              else if (FRC->second->DefNode->CmpName("binary"))
               {
                 delete FRC->second;
                 FRC->second = NULL;
               }
              else
               {
                 FRC->second->Parent = NULL;
                 delete FRC->second;
                 FRC->second = NULL;
               }
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::ClearEditItems()
{
  ReiterateClear(FDefXML, FEditMap);
/*
  for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
   {
     if (i->second->DefNode->CmpName("choiceBD"))
      {
        delete i->second;
        i->second = NULL;
      }
     else
       i->second->Parent = NULL;
   }
  for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
   if (i->second) delete i->second;
*/
  FEditMap.clear();
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::ClearTemplateItems()
{
  ReiterateClear(FDefXML, FTmplEditMap);
/*
  for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
   {
     if (i->second->DefNode->CmpName("choiceBD"))
      {
        delete i->second;
        i->second = NULL;
      }
     else
       i->second->Parent = NULL;
   }
  for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
   if (i->second) delete i->second;
*/
  FTmplEditMap.clear();
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::ClearLabItems()
{
  for (TRegLabMap::iterator i = FLabMap.begin(); i != FLabMap.end(); i++)
   i->second->Parent = NULL;
  for (TRegLabMap::iterator i = FLabMap.begin(); i != FLabMap.end(); i++)
   delete i->second;
  FLabMap.clear();
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::EditDataChange()
{
  for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
   i->second->riDataChange(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::TemplateDataChange()
{
  for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
   i->second->riDataChange(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::LabDataChange()
{
  for (TRegLabMap::iterator i = FLabMap.begin(); i != FLabMap.end(); i++)
   i->second->riDataChange();
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDContainer::FGetEditItems(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FEditMap.find(AUID);
  if (RC == FEditMap.end())
   throw ERegEDContainerError(FMT1(icsRegEDContainerErrorEDNo,AUID).c_str());
  return FEditMap[AUID];
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDContainer::FGetTemplateItems(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FTmplEditMap.find(AUID);
  if (RC == FTmplEditMap.end())
   throw ERegEDContainerError(FMT1(icsRegEDContainerErrorTmplNo,AUID).c_str());
  return FTmplEditMap[AUID];
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegEDContainer::FGetLabItems(UnicodeString AUID)
{
  TRegLabMap::iterator RC = FLabMap.find(AUID);
  if (RC == FLabMap.end())
   throw ERegEDContainerError(FMT1(icsRegEDContainerErrorLabNo,AUID).c_str());
  return FLabMap[AUID];
}
//---------------------------------------------------------------------------
int __fastcall TRegEDContainer::FGetEditCount()
{
  return FEditMap.size();
}
//---------------------------------------------------------------------------
int __fastcall TRegEDContainer::FGetTemplateEditCountCount()
{
  return FTmplEditMap.size();
}
//---------------------------------------------------------------------------
int __fastcall TRegEDContainer::FGetLabCount()
{
  return FLabMap.size();
}
//---------------------------------------------------------------------------
TpFIBQuery* __fastcall TRegEDContainer::FGetFIBTemplateQuery()
{
  if (FADOSrc)
   throw ERegEDContainerDSError(FMT(icsRegEDContainerErrorDSTypeADO));
  else
   return (TpFIBQuery*)FTmplDS;
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::FSetFIBTemplateQuery(TpFIBQuery* ATmplSrc)
{
  if (FADOSrc)
   throw ERegEDContainerDSError(FMT(icsRegEDContainerErrorDSTypeADO));
  else
   FTmplDS = (TDataSet*)ATmplSrc;
}
//---------------------------------------------------------------------------
TADODataSet* __fastcall TRegEDContainer::FGetADOTemplateQuery()
{
  if (FADOSrc)
   return (TADODataSet*)FTmplDS;
  else
   throw ERegEDContainerDSError(FMT(icsRegEDContainerErrorDSTypeFIB));
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::FSetADOTemplateQuery(TADODataSet* ATmplSrc)
{
  if (FADOSrc)
   FTmplDS = (TDataSet*)ATmplSrc;
  else
   throw ERegEDContainerDSError(FMT(icsRegEDContainerErrorDSTypeFIB));
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDContainer::AddEditItem(TTagNode* ANode, bool AIsApp, TWinControl *AParent)
{
  if (!FDataSrc)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataSrc));

  if (!FTmplDS)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoTmpQuery));

  if (FADOSrc)
   {
     if (!FTmplDS->ClassNameIs("TADODataSet"))
      throw ERegEDError(FMT(icsRegEDContainerErrorTmpQueryTypeADO));
   }
  else
   {
     if (!FTmplDS->ClassNameIs("TpFIBQuery"))
      throw ERegEDError(FMT(icsRegEDContainerErrorTmpQueryTypeFIB));
   }

  if (!ANode)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataDef));
  if (!ANode->AV["uid"].Length())
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataDefId));

  UnicodeString FUID = ANode->AV["uid"];
  try
   {
     TRegEDMap::iterator RC = FEditMap.find(FUID);
     if (RC == FEditMap.end())
      {
        try
         {
           if (ANode->CmpName("binary"))
            {
              FEditMap[FUID] = new TriBinary(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("choice"))
            {
              FEditMap[FUID] = new TriChoice(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("choiceBD,class"))
            {
              FEditMap[FUID] = new TriChoiceBD(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("choiceTree"))
            {
              FEditMap[FUID] = new TriChoiceTree(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("date"))
            {
              FEditMap[FUID] = new TriDate(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("datetime"))
            {
              FEditMap[FUID] = new TriDatetime(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("time"))
            {
              FEditMap[FUID] = new TriTime(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("digit"))
            {
              FEditMap[FUID] = new TriDigit(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("text"))
            {
              FEditMap[FUID] = new TriText(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("extedit"))
            {
              FEditMap[FUID] = new TriExtEdit(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           else if (ANode->CmpName("unitgroup"))
            {
              FEditMap[FUID] = new TriUnitgroup(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                              FTablePref, FReqColor,
                                              this, FDataSrc, AIsApp);
            }
           FEditMap[FUID]->SetOnDependValues();
         }
        catch(Exception &E)
         {
           throw ERegEDContainerError(FMT2(icsRegEDContainerSysErrorEDCreate,FUID,E.Message).c_str());
         }
        catch(...)
         {
           throw ERegEDContainerError(FMT1(icsRegEDContainerErrorEDCreate,FUID).c_str());
         }
      }
     else
      throw ERegEDContainerError(FMT1(icsRegEDContainerErrorEDExists,FUID).c_str());
   }
  __finally
   {
   }
  return FEditMap[FUID];
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDContainer::AddTemplateItem(TTagNode* ANode, UnicodeString AVal, TWinControl *AParent)
{
  if (!FTmplDS)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoTmpQuery));

  if (FADOSrc)
   {
     if (!FTmplDS->ClassNameIs("TADODataSet"))
      throw ERegEDError(FMT(icsRegEDContainerErrorTmpQueryTypeADO));
   }
  else
   {
     if (!FTmplDS->ClassNameIs("TpFIBQuery"))
      throw ERegEDError(FMT(icsRegEDContainerErrorTmpQueryTypeFIB));
   }

  if (!ANode)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataDef));
  if (!ANode->AV["uid"].Length())
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataDefId));

  UnicodeString FUID = ANode->AV["uid"];
  try
   {
     TRegEDMap::iterator RC = FTmplEditMap.find(FUID);
     if (RC == FTmplEditMap.end())
      {
        try
         {
           if (ANode->CmpName("binary"))
            {
              FTmplEditMap[FUID] = new TriBinaryTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("choice"))
            {
              FTmplEditMap[FUID] = new TriChoiceTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("choiceBD,class"))
            {
              FTmplEditMap[FUID] = new TriChoiceBDTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("choiceTree"))
            {
              FTmplEditMap[FUID] = new TriChoiceTreeTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("date"))
            {
              FTmplEditMap[FUID] = new TriDateTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("datetime"))
            {
              FTmplEditMap[FUID] = new TriDatetimeTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("time"))
            {
              FTmplEditMap[FUID] = new TriTimeTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("digit"))
            {
              FTmplEditMap[FUID] = new TriDigitTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("text"))
            {
              FTmplEditMap[FUID] = new TriTextTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("extedit"))
            {
              FTmplEditMap[FUID] = new TriExtEditTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           else if (ANode->CmpName("unitgroup"))
            {
              FTmplEditMap[FUID] = new TriUnitgroupTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                                  FTablePref, FReqColor,
                                                  this,  AVal);
            }
           FTmplEditMap[FUID]->SetOnDependValues();
           FTmplEditMap[FUID]->SetValue(AVal);
         }
        catch(Exception &E)
         {
           throw ERegEDContainerError(FMT2(icsRegEDContainerSysErrorTmplCreate,FUID,E.Message).c_str());
         }
        catch(...)
         {
           throw ERegEDContainerError(FMT1(icsRegEDContainerErrorTmplCreate,FUID).c_str());
         }
      }
     else
      throw ERegEDContainerError(FMT1(icsRegEDContainerErrorTmplExists,FUID).c_str());
   }
  __finally
   {
   }
  return FTmplEditMap[FUID];
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegEDContainer::AddLabItem(TTagNode* ANode, int AWidth, TWinControl *AParent)
{
  if (!FDataSrc)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataSrc));

  if (!FTmplDS)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoTmpQuery));

  if (FADOSrc)
   {
     if (!FTmplDS->ClassNameIs("TADODataSet"))
      throw ERegEDError(FMT(icsRegEDContainerErrorTmpQueryTypeADO));
   }
  else
   {
     if (!FTmplDS->ClassNameIs("TpFIBQuery"))
      throw ERegEDError(FMT(icsRegEDContainerErrorTmpQueryTypeFIB));
   }

  if (!ANode)
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataDef));
  if (!ANode->AV["uid"].Length())
   throw ERegEDError(FMT(icsRegEDContainerErrorNoDataDefId));

  UnicodeString FUID = ANode->AV["uid"];
  try
   {
     TRegLabMap::iterator RC = FLabMap.find(FUID);
     if (RC == FLabMap.end())
      {
        try
         {
           FLabMap[FUID] = new TRegLabItem (FOwner, (AParent)? AParent : FPlaceCtrl, ANode, FTmplDS,
                                            FTablePref, this, FDataSrc, AWidth);
           FLabMap[FUID]->SetOnDependValues();
         }
        catch(Exception &E)
         {
           throw ERegEDContainerError(FMT2(icsRegEDContainerSysErrorLabCreate,FUID,E.Message).c_str());
         }
        catch(...)
         {
           throw ERegEDContainerError(FMT1(icsRegEDContainerErrorLabCreate,FUID).c_str());
         }
      }
     else
      throw ERegEDContainerError(FMT1(icsRegEDContainerErrorLabExists,FUID).c_str());
   }
  __finally
   {
   }
  return FLabMap[FUID];
}
//---------------------------------------------------------------------------
TRegEDItem*  __fastcall TRegEDContainer::GetEDControl(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FEditMap.find(AUID);
  if (RC == FEditMap.end())
   return NULL;
  else
   return FEditMap[AUID];
}
//---------------------------------------------------------------------------
TRegEDItem*  __fastcall TRegEDContainer::GetTemplateControl(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FTmplEditMap.find(AUID);
  if (RC == FTmplEditMap.end())
   return NULL;
  else
   return FTmplEditMap[AUID];
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegEDContainer::GetLabControl(UnicodeString AUID)
{
  TRegLabMap::iterator RC = FLabMap.find(AUID);
  if (RC == FLabMap.end())
   return NULL;
  else
   return FLabMap[AUID];
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TRegEDContainer::GetRef(UnicodeString ARef)
{
  if (FXMLList) return FXMLList->GetRefer(ARef);
  else          return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TRegEDContainer::GetNode(UnicodeString ARef)
{
  if (FDefXML) return FDefXML->GetTagByUID(ARef);
  else         return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::SetOnCtrlDataChange(int AType)
{
  // AType:
  //  0 - EditMap
  //  1 - TmplEditMap
  //  3 - All
  if (FCtrlDataChange)
   {
     switch (AType)
      {
        case 0:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = FCtrlDataChange;
          break;
        }
        case 1:
        {
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = FCtrlDataChange;
          break;
        }
        case 3:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = FCtrlDataChange;
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = FCtrlDataChange;
        }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::ClearOnCtrlDataChange(int AType)
{
  // AType:
  //  0 - EditMap
  //  1 - TmplEditMap
  //  2 - LabMap
  //  3 - All
  if (FCtrlDataChange)
   {
     switch (AType)
      {
        case 0:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = NULL;
          break;
        }
        case 1:
        {
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = NULL;
          break;
        }
        case 3:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = NULL;
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = NULL;
        }
      }
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegEDContainer::GetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString ARef)
{
  UnicodeString RC = "";
  try
   {
     if (ATemplate)
      RC = TemplateItems[AUID]->GetValue(ARef);
     else
      RC = EditItems[AUID]->GetValue(ARef);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TRegEDContainer::SetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString AValue)
{
  if (ATemplate)
   TemplateItems[AUID]->SetValue(AValue);
  else
   EditItems[AUID]->SetValue(AValue);
}
//---------------------------------------------------------------------------

