//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riDatetime.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define dbED         ((TDBEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED         ((TEdit*)ED)
//---------------------------------------------------------------------------
__fastcall TriDatetime::TriDatetime(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   if (FNode->CmpName("text,date,datetime,time,digit,choice,unitgroup,choiceTree,extedit"))
    { // init component label
      Lab = new TLabel(this);  Lab->Parent = this;
      Lab->Transparent = true;
      Lab->Top = 2;            Lab->Left = 4;
      Lab->Caption = FNode->AV["name"]+":";
    }
   if (FNode->CmpName("text,datetime,time,digit")&&!isFV)
    {
      ED = new TDBEdit(this);
      ED->Parent = this;
      dbED->OnChange = FDefaultDataChange;
      dbED->Ctl3D = is3D;
      dbED->DataField = FFlName; dbED->DataSource = FSrc;
      if (Required) dbED->Color = FReqColor;
      if (IsTemplate(FNode))
       {
         if (SrcDSEditable()) irquSetAsVar(SrcDS(),FFlName,_TemplateValue(FNode));
       }
    }
   ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
   if (FNode->CmpName("datetime"))
    {
      xSize = Rect(0,0,140,21);
    }
   if (ED)
    {
      ED->Constraints->MinWidth = 60;
      ED->Parent = this;
      ED->Width = xSize.Right+10;
      if (ED->Width < 60) ED->Width = 60;
      ED->Top = xSize.Top;
      ED->Height = xSize.Bottom;
      if (FNode->CmpName("binary"))
       {
         ED->Left = 5;
       }
      else
       {
         if (Lab->Width > (AParent->Width-2))
          {
            Lab->Font->Size = Lab->Font->Size - 1;
            Lab->Update();
            if (Lab->Width > (AParent->Width - 2))
             Lab->Width = AParent->Width-2;
          }
         ED->Left = Lab->Left + Lab->Width + 5;
         if ((ED->Left + ED->Width) > (AParent->Width - 2))
          {
            TwoLine = true;  Lab->Top = 0;
            ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
            CMaxH = 34; CMinH = 34;
            Height = CMaxH;
            if ((ED->Left + ED->Width) > (AParent->Width - 2))
             ED->Width = AParent->Width - 4 - ED->Left;
            if (FNode->CmpName("text"))
             ED->Width = AParent->Width - 12 - ED->Left;
          }
       }
    }
   else
    Width = xSize.Right+15;
   if (Lab&&ED)
    Lab->FocusControl = ED;
   if (Parent->ClassNameIs("TriDatetime"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriDatetime::~TriDatetime()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   dbED->OnChange    = NULL;
   delete dbED;
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  if (FNode->CmpName("digit,date")&&TmpCtrl)     ED2->Left = ALeft;
  if (ED)                                Width = ED->Left+ED->Width+4;
  if (TmpCtrl)
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4+ED2->Width;
  else
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED&&(!FNode->CmpName("binary")))  *AEDLeft = ED->Left;
  else                                            *AEDLeft = -1;
  if (!TwoLine&&Lab&&(!FNode->CmpName("binary"))) *ALabR = Lab->Left+Lab->Width;
  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriDatetime::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDatetime::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
    if (tED->Text.Length())            xVal += tED->Text;
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate))
   {
     dbED->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
     dbED->Color = clBtnFace;
     if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
      dbED->Text = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (!isFV)
   {
     if (SrcDSEditable()) irquSetAsStr(SrcDS(),FFlName,AVal);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  dbED->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDatetime::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDatetime::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,date,datetime,time,digit,extedit,binary,choice,choiceTree"))
    return (TControl*)ED;
   else
    return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDatetime::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDatetime::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return dbED->Text;
}
//---------------------------------------------------------------------------
Variant __fastcall TriDatetime::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  TDateTime FDateTime;
  if (TryStrToDateTime(dbED->Text,FDateTime)) return Variant(FDateTime);
  else                                  return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriDatetime::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
          if (!dbED->Text.Length())  RC = false;
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

