//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riUnitgroup.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TriUnitgroup::TriUnitgroup(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
   Lab = new TLabel(this);  Lab->Parent = this;
   Lab->Top = 2;            Lab->Left = 4;
//   Lab = NULL;
//   Caption = " "+FNode->AV["name"]+" ";
//   Lab->Caption = FNode->AV["name"]+":";
   Lab->Top = -1; Lab->Left = 5;
   Lab->Color = DrColor(clBtnFace,64);
   Lab->Caption = " "+FNode->AV["name"]+" ";
   Lab->Font->Color = clWhite;
   Lab->Font->Style.Clear();
   Lab->Font->Style = TFontStyles() << fsBold;
   CMaxH = 500;
   CMinH = 22;

//   PanelStyle->Active = false;
//   Transparent = true;
//   Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
//   Style->LookAndFeel->Kind = FCtrlOwner->LFKind;


//   Style->Edges  = TcxBorders() << bLeft << bTop << bRight << bBottom;

      BevelInner = bvRaised; BevelOuter = bvLowered;
   if (FNode->GetParent()->CmpName("unitgroup"))
    Width = 130;
   else
    Width = 135;
   if (Parent->ClassNameIs("TriUnitgroup"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetOnDependValues()
{
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriUnitgroup::~TriUnitgroup()
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
//  ED->Left = ALeft;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  *AEDLeft = -1;
  *ALabR = -1;
//  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
bool __fastcall TriUnitgroup::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriUnitgroup::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  Lab->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetValue(UnicodeString AVal,bool AEnable)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetLabel(UnicodeString xLab)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (xLab.Length()) Lab->Caption = xLab+":";
   else            Lab->Caption = "";//FNode->AV["name"]+":";
//   Lab->Update();
//   if (xLab.Length()) Caption = xLab+":";
//   else               Caption = "";//FNode->AV["name"]+":";
   Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::SetOnEnable(TTagNode* ANode)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetEnList->Add(ANode);
   riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::riDataChange(TObject *Sender)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriUnitgroup::FDefaultDataChange(TObject *Sender)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriUnitgroup::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriUnitgroup::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriUnitgroup::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriUnitgroup::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" unitgroup has no value");
}
//---------------------------------------------------------------------------
bool __fastcall TriUnitgroup::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return true;
}
//---------------------------------------------------------------------------

