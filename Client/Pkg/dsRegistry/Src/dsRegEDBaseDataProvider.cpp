//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsRegEDBaseDataProvider.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsRegEDBaseDataProvider::TdsRegEDBaseDataProvider()
{
  FGetCompValue  = NULL;
  FOnGetClassXML = NULL;
}
//---------------------------------------------------------------------------
__fastcall TdsRegEDBaseDataProvider::~TdsRegEDBaseDataProvider()
{
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDBaseDataProvider::FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode)
{
  return false;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDBaseDataProvider::GetChDBData(UnicodeString ARef, UnicodeString RecVal)
{
  return 0;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDBaseDataProvider::GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption)
{
  return 0;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDBaseDataProvider::SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue)
{
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegEDBaseDataProvider::GetTreeValue(UnicodeString ARef, UnicodeString ACode, UnicodeString &AExtCode)
{
  UnicodeString RC = "";
  try
   {
     if (FOnGetClassXML)
      {
        TTagNode *FDef;
        FOnGetClassXML(ARef, FDef);
        FDef = FDef->GetChildByAV("i","c",ACode,true);
        if (FDef)
         {
           RC = FDef->AV["n"];
           AExtCode = FDef->AV["e"];
         }
      }
     else
      throw System::Sysutils::Exception("�� ����� ���������� OnGetClassXML.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

