//---------------------------------------------------------------------------
#include <vcl.h>
//#include <stdio.h>
//#include <clipbrd.hpp>
#pragma hdrstop

#include "dsRegDMUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"

TdsRegDM *dsRegDM;
//---------------------------------------------------------------------------
__fastcall TdsRegDM::TdsRegDM(TComponent* Owner)
    : TDataModule(Owner)
{
  FClassDefNode = NULL;
//  FRegDefNode = NULL;
  FClassEditable = true;
  FFullEdit = true;
  FClassShowExpanded = "";
  FRequiredColor = clInfoBk;
  FULEditWidth = 700;

  FFetchAll = TdsRegFetch::None;
  FClassFetchAll = TdsRegFetch::None;
  FFetchRecCount = 50;
  FClassFetchRecCount = 50;
  FUnitListOpenType = TdsRegListType::ltFull;
  FClassListOpenType = TdsRegListType::ltFull;
  FDataPath = icsPrePath(GetEnvironmentVariable("APPDATA")+"\\ic");
  FXMLPath = icsPrePath(FDataPath+"\\xml");

//  enum class TdsRegListType { ltFull = 0, ltFilter = 1, ltSearchFilter = 2};
}
//---------------------------------------------------------------------------
void __fastcall TdsRegDM::DataModuleCreate(TObject *Sender)
{
  UnitList = NULL;
  SelRegUnit = NULL;
  DTFormats[0] = FMT(icsDTFormatsD);
  DTFormats[1] = FMT(icsDTFormatsDT);
  DTFormats[2] = FMT(icsDTFormatsT);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegDM::DataModuleDestroy(TObject *Sender)
{
/*
  delete FClassData;
  if (SelRegUnit)   delete SelRegUnit;
  SelRegUnit = NULL;
  if (REG_DB->Connected) REG_DB->Close();
*/
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::NewGUID()
{
  return icsNewGUID().UpperCase();
}
//---------------------------------------------------------------------------
//TTagNode* __fastcall TdsRegDM::FGetRegDefNode()
//{
//  TTagNode *RC = FRegDefNode;
//  return RC;
//}
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsRegDM::FGetClassDef()
{
  TTagNode *RC = FClassDefNode;
  try
   {
     if (!RC)
      {
        FClassDefNode = RegDef->GetChildByName("classes");
        RC = FClassDefNode;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
__int64 __fastcall TdsRegDM::kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, System::UnicodeString AFilterParam)
{
  __int64 RC = 0;
  try
   {
     if (FOnGetCount)
      RC = FOnGetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsRegDM::kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, System::UnicodeString AFilterParam)
{
  TJSONObject* RC = NULL;
  try
   {
     if (FOnGetIdList)
      FOnGetIdList(AId, AMax, AFilterParam, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsRegDM::kabCDSGetValById(UnicodeString AId, UnicodeString ARecId)
{
  TJSONObject* RC = NULL;
  try
   {
     if (FOnGetValById)
      FOnGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsRegDM::kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId)
{
  TJSONObject* RC = NULL;
  try
   {
     if (FOnGetValById10)
      FOnGetValById10(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::kabCDSInsert(UnicodeString AId, TJSONObject* AValues, System::UnicodeString &ARecID)
{
  UnicodeString RC = "";
  try
   {
     if (FOnInsertData)
      FOnInsertData(AId, AValues, ARecID, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::kabCDSEdit(UnicodeString AId, TJSONObject* AValues, System::UnicodeString &ARecID)
{
  UnicodeString RC = "";
  try
   {
     if (FOnEditData)
      FOnEditData(AId, AValues, ARecID, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::kabCDSDelete(UnicodeString AId, UnicodeString ARecId)
{
  UnicodeString RC = "";
  try
   {
     if (FOnDeleteData)
      FOnDeleteData(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::kabCDSGetClassXML(UnicodeString AId)
{
  UnicodeString RC = "";
  try
   {
     if (FOnGetClassXML)
       FOnGetClassXML(AId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
{
  try
   {
     if (!FXMLList->XMLExist(AId))
      {
        TTagNode *tmp = new TTagNode;
        try
         {
           UnicodeString FFN = icsPrePath(FXMLPath+"\\"+AId+".xml");
           if (!FileExists(FFN))
            {
              ForceDirectories(FXMLPath);
              tmp->AsXML = kabCDSGetClassXML(AId);
              tmp->SaveToXMLFile(FFN,"");
            }
           FXMLList->Load(FFN,AId);
         }
        __finally
         {
           delete tmp;
         }
      }
     ARC = FXMLList->GetXML(AId);
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegDM::GetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData)
{
  bool RC = false;
  AData = NULL;
  TTagNode *FDefUnitNode = NULL;
  try
   {
     TTagNode *FRootClass = FRegDefNode->GetChildByAV("classform","group","unit", true);
     if (FRootClass)
      {
        FRootClass = FRootClass->GetFirstChild();
        FDefUnitNode = TkabCustomDataSet::CreateDefNode(FRootClass);
        if (FDefUnitNode)
         {
           AData = new TkabCustomDataSetRow(FDefUnitNode);

           TJSONObject *tmpFlData;
           TJSONString *tmpData;
           UnicodeString FFlId;
           UnicodeString FFlVal;

           TJSONObject* FRetData = kabCDSGetValById("reg.1000", IntToStr(ACode));

           TTagNode* itField = FDefUnitNode->GetFirstChild();
           while (itField)
            {
              if (!itField->CmpName("code,data"))
               {
                 FFlId = itField->AV["uid"];
                 tmpFlData = (TJSONObject*)FRetData->Values[FFlId];
                 if (tmpFlData)
                  {
                    if (!tmpFlData->Null)
                     {
                       tmpData = (TJSONString*)tmpFlData;
                       if (itField->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
                        {
                          AData->Value[FFlId] = Variant::Empty();
                          AData->StrValue[FFlId] = "";
                          if (tmpFlData->Count)
                           {
                             FFlVal =((TJSONString*)tmpFlData->Pairs[0]->JsonString)->Value();
                             if(FFlVal.Trim().Length())
                              {
                                if (itField->CmpName("binary,choice,choiceDB"))
                                  AData->Value[FFlId] = FFlVal.ToInt();
                                else
                                  AData->Value[FFlId] = FFlVal;

                              }
                             else
                              AData->Value[FFlId] = Variant::Empty();
                             AData->StrValue[FFlId] = ((TJSONString*)tmpFlData->Pairs[0]->JsonValue)->Value();
                           }
                        }
                       else
                        {
                          if(!tmpData->Value().Trim().Length())
                           AData->Value[FFlId] = Variant::Empty();
                          else
                           {
                             if (itField->CmpName("date"))
                              AData->Value[FFlId] = TDate(tmpData->Value());
                             else
                              AData->Value[FFlId] = tmpData->Value();
                           }
                        }
                     }
                  }
               }
              itField = itField->GetNext();
            }
           RC = true;
         }
      }
   }
  __finally
   {
     if (FDefUnitNode) delete FDefUnitNode;
   }
  return RC;
}
//---------------------------------------------------------------------------

