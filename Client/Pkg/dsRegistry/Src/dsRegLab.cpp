//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "dsRegLab.h"
#include "dsRegEDFunc.h"
#include "cxGroupBox.hpp"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight

//---------------------------------------------------------------------------
__fastcall TdsRegLabItem::TdsRegLabItem (TComponent *AOwner, TWinControl *AParent, TTagNode* ANode,
                                         TdsRegEDContainer *ACtrlOwner, int AWidth)
: TPanel(AOwner)
{
  if (!ACtrlOwner)
   throw ERegLabError(FMT(icsRegEDErrorNoOwner));
  FCtrlOwner = ACtrlOwner;

  SetLabList = new TList; SetEnList = new TList;
  FNode = ANode;
  Width = AWidth-5;
  Left = 1;
  TwoLine = false; Parent = AParent;
  Alignment = taLeftJustify; Caption = "";
  BevelInner = bvNone; BevelOuter = bvNone;

  isTmpVal = false;
  _UID_ = FNode->AV["uid"];
  FLab = new TLabel(this);      FLab->Parent = this;
  FDataLab = new TLabel(this);  FDataLab->Parent = this;
//   FLab->Transparent = true;
  FLab->Top = 1;            FLab->Left = 2;
  FLab->Font->Color = clWindowText;
  FLab->Font->Style.Clear();
//   FLab->Font->Style = TFontStyles() << fsBold;
  FLab->Caption = FNode->AV["name"]+":";
  if (FNode->CmpName("binary"))
   {
     FLab->Caption = "";
     FLab->Width = 0;
     FLab->Transparent = true;
   }

  FDataLab = new TLabel(this);
  FDataLab->Parent = this;
  FDataLab->Transparent = true;
  FDataLab->Font->Style = TFontStyles() << fsBold;

  if (FNode->CmpName("text,date,digit,choice,choiceDB,class,choiceTree,extedit"))
   {
     FGetData();
     FDataLab->Parent = this;
     int lnCount = FNode->AV["linecount"].ToIntDef(0);
     if (lnCount == 0)
      {
        if (FNode->AV["minleft"].ToIntDef(0))
         {
           if ((FLab->Left+FLab->Width+4) > FNode->AV["minleft"].ToIntDef(0))
            FDataLab->Left = FLab->Left+FLab->Width+4;
           else
            FDataLab->Left = FNode->AV["minleft"].ToIntDef(0);
         }
        else
         FDataLab->Left = FLab->Left+FLab->Width+4;
        FDataLab->Top = FLab->Top;
        FDataLab->Width = Width - FDataLab->Left - 20;
        FDataLab->Constraints->MaxHeight = 14;
        FDataLab->Constraints->MinHeight = 14;
        FDataLab->Anchors.Clear();
        FDataLab->Anchors << akLeft << akRight << akTop;
        FDataLab->WordWrap = false;
        FDataLab->AutoSize = false;
        FDataLab->Update();
        Height = 14;
      }
     else
      {
        FDataLab->AutoSize = false;
        FDataLab->Anchors.Clear();
        FDataLab->Anchors << akLeft << akRight << akTop;
        FDataLab->Left = FLab->Left+2;
        FDataLab->Top = FLab->Top+FLab->Height+1;
        FDataLab->Width = Width - FDataLab->Left - 6;
        FDataLab->Constraints->MaxHeight = 14*lnCount;
        FDataLab->Constraints->MinHeight = 14*lnCount;
        Height = 14*lnCount+14;
        FDataLab->WordWrap = true;
        FDataLab->Update();
      }
     FDataLab->Cursor      = crHandPoint;
     FDataLab->OnClick     = DataLabClick;
     FDataLab->OnMouseDown = DataLabMouseDown;
     FDataLab->OnMouseUp   = DataLabMouseUp;

   }
  else if (FNode->CmpName("group"))
   {
//     Color = clInactiveCaption;
     FLab->Top = -1; FLab->Left = 5;
     FLab->Visible = false;
     FDataLab->Top = -1; FDataLab->Left = 5;
     FDataLab->Visible = false;

     FGroupCtrl = new TcxGroupBox(this);
     FGroupCtrl->Parent = this;  FGroupCtrl->Top = 0;  FGroupCtrl->Left = 0;
     FGroupCtrl->Width = Width-17;//20;
     ((TcxGroupBox*)FGroupCtrl)->Caption = " "+FNode->AV["name"]+" ";
     ((TcxGroupBox*)FGroupCtrl)->Style->Font->Color = clNavy;
     Height = 15;
     FGroupCtrl->Anchors.Clear();
     FGroupCtrl->Anchors << akLeft << akRight << akTop;
   }

  ParentBackground = true;
  Anchors.Clear();
  Anchors << akLeft << akRight << akTop;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::FOnLabResize(TObject *Sender)
{
  if (FDataLab)
   {
     if (FNode->CmpName("choiceDB,class,choiceTree,extedit"))
      {
        FDataLab->Update();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::SetOnDependValues()
{
   if (FNode->CmpName("text,date,digit,choice,binary"))
    {// ��������� ����������� ��� �����
      if (FNode->AV["depend"].Length() == 9 && isRefMLabCtrl(FNode))
       RefMLabCtrl(FNode)->SetOnLab(FNode);
      else if (FNode->AV["ref"].Length() == 4 && isRefLabCtrl(FNode))
       RefLabCtrl(FNode)->SetOnLab(FNode);
    }
   if (FNode->GetChildByName("actuals"))
    {// ��������� ����������� "Enabled"
      TTagNode *itxNode = FNode->GetChildByName("actuals")->GetFirstChild();
      while (itxNode)
       {
         TdsRegLabItem* SSS;
         if (itxNode->AV["depend"].Length() == 9 && isRefMLabCtrl(itxNode))
          {
            SSS = RefMLabCtrl(itxNode);
            if (SSS) SSS->SetOnEnable(FNode);
          }
         else if (itxNode->AV["ref"].Length() == 4 && isRefLabCtrl(itxNode))
          {
            SSS = RefLabCtrl(itxNode);
            if (SSS) SSS->SetOnEnable(FNode);
          }
         itxNode = itxNode->GetNext();
       }
    }
   if (FNode->CmpAV("inlist","n"))
    {
      CMinH = 0; CMaxH = 0; Height = 0;
    }
}
//---------------------------------------------------------------------------
__fastcall TdsRegLabItem::~TdsRegLabItem()
{
  if (FNode->CmpName("text,date,digit,choice"))    delete FDataLab;
  else if (FNode->CmpName("choiceDB,class,choiceTree,extedit"))  delete FDataLab;
//  else if (FNode->CmpName("group"))                          delete ((TBevel*)ED);
//  FNode->LabCtrl = NULL;
  if (FLab) delete FLab;
  FLab = NULL;
  if (SetLabList) delete SetLabList;
  SetLabList = NULL;
  if (SetEnList) delete SetEnList;
  SetEnList = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::SetEnable(bool AEnable)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (AEnable)
   {
     FLab->Font->Color = clWindowText;
     FDataLab->Enabled = true;
   }
  else
   {
     FLab->Font->Color = clInactiveCaption;
     FDataLab->Enabled = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::SetLabel(UnicodeString ACaption)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("binary"))
    {
      if (ACaption.Length()) FLab->Caption = ACaption;
      else                   FLab->Caption = "";//FNode->AV["name"]+":";
    }
   else
    {
      if (ACaption.Length()) FLab->Caption = ACaption+":";
      else                   FLab->Caption = "";//FNode->AV["name"]+":";
    }
   FLab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::SetOnLab(TTagNode* ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetLabList->Add(ANode);
   DataChange();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::SetOnEnable(TTagNode* ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetEnList->Add(ANode);
   DataChange();
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegLabItem::FGetData()
{
  bool RC = false;
  try
   {
     UnicodeString FCapt = "";
     FDataValue = FCtrlOwner->GetCtrlData(FNode, FCapt);
     if (!(FDataValue.IsNull()||FDataValue.IsEmpty()))
      {
        if (FNode->CmpName("binary"))
         FCapt = (FCapt.ToIntDef(0))?FNode->AV["name"]:UnicodeString("");
        else if (FNode->CmpName("date"))
         {
           if (!(int)FDataValue)
            FCapt = "";
         }
        else if (FNode->CmpName("choiceDB"))
         {
           if (!(int)FDataValue)
            FCapt = "";
         }
      }
     FDataLab->Caption = FCapt;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::DataChange()
{
  if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FGetData();

  if (!FNode->CmpName("group"))
   {
     if (FCtrlOwner->IsTemplate(_UID_))
      {
        Color = clActiveCaption;
        FLab->Font->Color = clWhite;
        FDataLab->Font->Color = clWhite;
      }
     else
      {
        if (!ParentBackground)
         {
           FLab->Font->Color = clWindowText;
           FDataLab->Font->Color = clWindowText;
           ParentBackground = true;
         }
      }
     Application->ProcessMessages();
   }

  UnicodeString xRef,FUID;
  FUID = _UID_.UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceDB
   {
/*
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        for (int i = 0; i < SetLabList->Count; i++)
         {
           xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
           if (xRef == FUID)
            labLabCtrl(i)->SetLabel(irquAsStr(SrcDS(),FFlName));
         }
      }
     else if (FNode->CmpName("choiceDB")) // ����� �� choiceDB
      {
        TTagNode *xNode;
        for (int i = 0; i < SetLabList->Count; i++)
         {
           xNode = ((TTagNode*)SetLabList->Items[i]);
           xRef = xNode->AV["depend"].UpperCase();     // ������ xxxx.yyyy
           // xxxx == uid.choiceDB yyyy == uid.��������������* ��� ref.choiceDB == uid.��������������
           if ((xRef.Length() == 9)&&(xRef.SubString(1,4) == FUID))
            {                                            // uid.��������������* - uid ����� ��� ������������ ��������������
              xRef = xNode->AV["ref"]; // ref - uid ���� �������������� �������� �������� �������� ������
              FGetExtCtrl(xNode->AV["uid"])->SetLabel(GetClValue(xRef));
            }
         }
      }
      */
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
//     return Variant(cFields[ARef.SubString(6,4)]);
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegLabItem::Condition(TTagNode *CondNode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC;
  UnicodeString xCond  = CondNode->AV["condition"];
  Variant* pmList; pmList = new Variant[CondNode->Count];
  CalcParam(CondNode->GetFirstChild(),pmList);
  wchar_t *spCh;
  wchar_t *Src = new wchar_t[xCond.Length()+1];
  wcscpy(Src,xCond.c_str());
  spCh = wcschr(Src,' ');
  while (spCh) {spCh[0] = '\n'; spCh = wcschr(Src,' '); }
  TStringList  *ExpList = new TStringList;
  ExpList->Text = UnicodeString(Src);
  UnicodeString tmpOper;
  for (int i = 0; i < ExpList->Count; i++)
   {
     tmpOper = ExpList->Strings[i];
     if (tmpOper[1] == '@')
      ExpList->Strings[i] = VarToWideStrDef(pmList[tmpOper.SubString(2,tmpOper.Length()-1).ToInt()],"");
   }
  RC = (bool)CalcCondition(ExpList);
  if (Src) delete[] Src;
  Src = NULL;
  if (pmList)  delete[] pmList;
  pmList = NULL;
  if (ExpList) delete ExpList;
  ExpList = NULL;
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::CalcParam(TTagNode *AFirst, Variant* AParam)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TTagNode *xNode = AFirst;
   TTagNode *refNode;
   Variant RC = NULL;
   int i = 0;
   UnicodeString FCapt, FRC;
   Variant CtrlRC;
   while(xNode)
    {
      if (xNode->AV["depend"].Length() == 9)
       {
         refNode = FCtrlOwner->GetNode( _GUI(xNode->AV["depend"]));
         if (refNode)
          {
          CtrlRC = FCtrlOwner->GetCtrlData(refNode, FCapt);
          FRC = VarToWideStrDef(CtrlRC,"-1");
          if (!FRC.Length())
            RC = 0;
          else
            RC = FCtrlOwner->GetChDBData(_UID(xNode->AV["depend"])+"."+xNode->AV["ref"], FRC );
          }
         else
          ShowMessage(FMT(icsRegEDErrorNoRef)+" : "+xNode->AsXML);
       }
      else if (xNode->AV["ref"].Length() == 4)
       RC = FCtrlOwner->GetCtrlData(FCtrlOwner->GetNode(xNode->AV["ref"]), FCapt);
      else if (xNode->CmpAV("name", "null"))
       RC = "NULL";
      else
       ShowMessage(FMT(icsRegEDErrorNoRef)+" : "+xNode->AsXML);
      AParam[i] = RC;
      i++;
      xNode = xNode->GetNext();
    }
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegLabItem::GetCompValue(UnicodeString ARef)
{
  if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
/*
  if ((!ARef.Length())&&FNode->CmpName("date,digit,choice,binary,choiceTree"))
   {
     if (SrcDS()->Active)
      {
        if (irquIsNull(SrcDS(),FFlName))
         return Variant(-2);
        else
         {
           if (FNode->CmpName("date"))
            return Variant(irquAsDate(SrcDS(),FFlName).FormatString("'dd.mm.yyyy'"));
           else if (FNode->CmpName("digit"))
            return Variant(irquAsStr(SrcDS(),FFlName));
           else if (FNode->CmpName("choice,binary,choiceTree"))
            return Variant((int)irquAsInt(SrcDS(),FFlName));
         }
      }
     else   return Variant(-2);
   }
  else if ((ARef.Length())&&FNode->CmpName("choiceDB"))
   {
     return Variant(GetClValue(ARef.SubString(6,4)));
   }
  else
   {
      ShowMessage(FMT1(icsRegEDErrorRefObject,ARef).c_str());
      return Variant(-2);
   }           */
  return FDataValue;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegLabItem::GetClValue(UnicodeString ARef)
{
  if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString RC = "";
/*  try
   {
     if (FNode->CmpName("choiceDB"))
      irExecProc("CLASSDATA_"+FTabPref+AVUp(FNode,"ref"), quFree, "CL_CODE", Variant((__int64)irquAsInt(SrcDS(),FFlName)));
     else
      irExecProc("CLASSDATA_"+FTabPref+AVUp(FNode,"uid"), quFree, "CL_CODE", Variant((__int64)irquAsInt(SrcDS(),FFlName)));
     RC = irquAsStr(quFree,"P_"+ARef);
   }
  __finally
   {
   }*/
  return RC;
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegLabItem::FGetExtCtrl(UnicodeString AUID)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FCtrlOwner)
    return FCtrlOwner->GetLabControl(AUID);
   else
    throw ERegLabError(FMT(icsRegEDErrorNoOwner));
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegLabItem::isRefLabCtrl(TTagNode *ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TTagNode *tmp = FCtrlOwner->GetNode(ANode->AV["ref"]);
   if (tmp)
    if (FGetExtCtrl(tmp->AV["uid"])) return true;
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegLabItem::isRefMLabCtrl(TTagNode *ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TTagNode *tmp = FCtrlOwner->GetNode(_GUI(ANode->AV["depend"]));
   if (tmp)
    if (FGetExtCtrl(tmp->AV["uid"])) return true;
   return false;
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegLabItem::RefLabCtrl(TTagNode* ARef)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FCtrlOwner->GetNode(ARef->AV["ref"]))
   return FGetExtCtrl(ARef->AV["ref"]);
  else
   return NULL;
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegLabItem::RefMLabCtrl(TTagNode* ARef)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FCtrlOwner->GetNode(_GUI(ARef->AV["depend"])))
   return FGetExtCtrl(_GUI(ARef->AV["depend"]));
  else
   return NULL;
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegLabItem::labLabCtrl(int AIdx)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return FGetExtCtrl(((TTagNode*)SetLabList->Items[AIdx])->AV["uid"]);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::DataLabClick(TObject *Sender)
{
  Clipboard()->AsText = ((TLabel*)Sender)->Caption;
  ShowMessage("\""+((TLabel*)Sender)->Caption+"\"\nC���������� � ����� ������.");
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::DataLabMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
  FSaveTransparent = FDataLab->Transparent;
  FSaveColor       = FDataLab->Color;
  FSaveFontColor   = FDataLab->Font->Color;
  FDataLab->Transparent = false;
  FDataLab->Color = clHighlight;
  FDataLab->Font->Color = clHighlightText;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegLabItem::DataLabMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
  FDataLab->Transparent = FSaveTransparent;
  FDataLab->Color = FSaveColor;
  FDataLab->Font->Color = FSaveFontColor;

  Application->ProcessMessages();
}
//---------------------------------------------------------------------------

