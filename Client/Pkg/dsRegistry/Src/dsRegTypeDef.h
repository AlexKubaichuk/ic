//---------------------------------------------------------------------------
#ifndef dsRegTypeDefH
#define dsRegTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//#include "XMLContainer.h"
#include "AxeUtil.h"
#include "KabCustomDS.h"
#include "dsRegExtFilterSetTypes.h"

//---------------------------------------------------------------------------
enum TdsRegClassType { ctAll = 0, ctList = 1, ctNone = 2};
enum TdsInsEditClassType { ietNone = 0, ietAny = 1, ietPriv = 2, ietTest = 3, ietCheck = 4, ietCancel = 4};
enum class TdsRegListType { ltFull = 0, ltFilter = 1, ltSearchFilter = 2};
enum TRegistryDemoMessage
{
// ������ ������ �����
rdmUnitInsert,
rdmUnitEdit,
rdmUnitDelete,
rdmUnitSearch,
rdmUnitSearchNext,
rdmUnitTemplate,
rdmUnitFilter,
rdmUnitOpen,
rdmUnitExt,
rdmUnitRefresh,
rdmUnitInqPanel,

// ������ ���������������
rdmClassInsert,
rdmClassEdit,
rdmClassDelete,
rdmClassSearch,
rdmClassSearchNext,
rdmClassTemplate,
rdmClassFilter,
rdmClassList,
rdmClassRefresh
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegEDContainer;
typedef void __fastcall (__closure *TRegistryDataEditEvent)(UnicodeString ATableName, UnicodeString ACode, UnicodeString AUID);
typedef void __fastcall (__closure *TRegistryDemoMessageEvent)(TRegistryDemoMessage AMsgType);
typedef void __fastcall (__closure *TUnitEvent)(__int64 Code, UnicodeString UnitStr);
typedef void __fastcall (__closure *TClassEditEvent)(TTagNode *ACls);
typedef void __fastcall (__closure *TInqUpdate)(__int64 Code, TControl* Sender, TDataSource *ASource);
typedef void __fastcall (__closure *TClassCallBack)(int Persent, UnicodeString Message);
typedef void __fastcall (__closure *TModifUnitEvent)(__int64 UnitCode, bool &CanModif);

typedef void __fastcall (__closure *TExtCountEvent)(UnicodeString &AData);
typedef UnicodeString __fastcall (__closure *TExtFilterSetEvent)(TTagNode *ADefNode, bool &CanContinue, UnicodeString AFilterGUI);
typedef UnicodeString __fastcall (__closure *TExtEditDependFieldEvent)(TTagNode *ADefNode);
//typedef void __fastcall (__closure *TExtFilterClearEvent)(TdsRegExtFilterSetType ASetType, TTagNode *ADefNode);
//typedef void __fastcall (__closure *TExtFilterGetTextEvent)(TdsRegExtFilterSetType ASetType, TTagNode *ADefNode, TStrings *AValues, UnicodeString AFilterGUI);

typedef void __fastcall (__closure *TdsRegReportClickEvent)(UnicodeString ARepGUI, __int64 AUCode, UnicodeString AFilter, TkabProgress AProgressProc);
typedef void __fastcall (__closure *TGetReportListEvent)(UnicodeString AId, TStringList *AList);

typedef bool __fastcall (__closure *TBarItemGetEnabled)(__int64 UCode, TkabCustomDataSource* ASource);
typedef void __fastcall (__closure *TBarItemClick)(__int64 UCode, TkabCustomDataSource* ASource);

typedef void __fastcall (__closure *TExtEditData)(TTagNode *AClsNode, TkabCustomDataSource* ASource, __int64 UCode, bool &Continue, bool &ARefresh, __int64 &ACode);

typedef bool __fastcall (__closure *TdsExportData)(TkabProgressType AStage, TTagNode *AClsNode, TkabCustomDataSource* ASource, __int64 ARecCode, TkabProgress AProgress);
typedef bool __fastcall (__closure *TdsInsEditData)(TkabProgressType AStage, TdsInsEditClassType AItemType, TTagNode *AClsNode, TkabCustomDataSource* ASource, __int64 ARecCode, TkabProgress AProgress);
typedef void __fastcall (__closure *TdsImportData)(TTagNode *ADataNode, UnicodeString &ALastRecCode, TkabProgress AProgress);

typedef void __fastcall (__closure *TExtLoadXML)(TTagNode *ARegNode, TTagNode *ASelRegNode);
typedef void __fastcall (__closure *TGetClassXMLEvent)(UnicodeString ARef, TTagNode *& ADef);
//typedef void __fastcall (__closure *TClassEditCreate)( TForm* form, TdsRegEDContainer *FCtrList, TTagNode* AClassRoot, __int64 CODE);

typedef void __fastcall (__closure *TExtValidateData)(__int64 ARecCode, TTagNode *ANode, TdsRegEDContainer *ControlList, bool &Valid);
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TdsGetClassXMLEvent)(System::UnicodeString ARef, System::UnicodeString &RC);
typedef __int64 __fastcall (__closure *TdsGetCountEvent)(System::UnicodeString AId, System::UnicodeString AFilterParam);
typedef bool __fastcall (__closure *TdsGetIdListEvent)(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, TJSONObject *& RC);
typedef bool __fastcall (__closure *TdsGetListByIdEvent)(System::UnicodeString AId, TJSONObject *& RC);
typedef bool __fastcall (__closure *TdsGetValByIdEvent)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
typedef bool __fastcall (__closure *TdsGetValById10Event)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
typedef bool __fastcall (__closure *TdsFindEvent)(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& RC);
typedef bool __fastcall (__closure *TdsInsertDataEvent)(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &RC);
typedef bool __fastcall (__closure *TdsEditDataEvent)(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &RC);
typedef bool __fastcall (__closure *TdsDeleteDataEvent)(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString &RC);
//---------------------------------------------------------------------------
extern PACKAGE UnicodeString __fastcall GetTemplateFields(TTagNode *ADescription);
#endif

