﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsRegEdBinary.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define dbED_ChB     ((TcxDBCheckBox*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
// ---------------------------------------------------------------------------
__fastcall TriBinary::TriBinary(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  ED          = new TcxCheckBox(this);
  ED->Parent  = this;
  ED->Caption = FNode->AV["name"];
  // Transparent = true;
  ED->Transparent            = true;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  CMaxH                      = 21;
  CMinH                      = 21; // Height = 18;
  ED->Constraints->MinWidth  = 60;
  ED->Left                   = 5;
  ED->Top                    = 1;
  ED->Height                 = 21;
  ED->Width                  = ED->Canvas->TextWidth(ED->Caption) + 15;
  Height                     = 21;
  ED->Properties->Alignment  = taLeftJustify;
  // ED->DataBinding->DataField = FFlName;
  // ED->DataBinding->DataSource = FSrc;
  ED->Properties->ValueChecked   = "1";
  ED->Properties->ValueUnchecked = "0";
  ED->OnClick                    = FDataChange;
  ED->Properties->OnChange       = FDataChange;
  if (IsAppend)
   {
    if (FCtrlOwner->IsTemplate(_UID_))
     FCtrlOwner->SetCtrlData(FNode, FCtrlOwner->TemplateData->Value[_UID_], "");
    else
     FCtrlOwner->SetCtrlData(FNode, (FNode->CmpAV("default", "uncheck") ? 0 : 1), "");
   }
  ED->Checked = FCtrlOwner->GetCtrlData(FNode, _ES_);
  ED->Enabled   = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  ED->OnKeyDown = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
TcxCheckBox * __fastcall TriBinary::FGetCastED()
 {
  return (TcxCheckBox *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::FSetCastED(TcxCheckBox * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriBinary::~TriBinary()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->OnClick = NULL;
  ED->Parent  = NULL;
  delete ED;
  ED = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  Width   = ED->Left + ED->Width + 2;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  *AEDLeft = -1;
  *ALabR   = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    ED->Left  = 2;
    ED->Width = Width - 4;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriBinary::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  DataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriBinary::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
    if (ABinaryNames)
     {
      if (ED->State != cbsGrayed)
       xVal += (ED->Checked) ? FNode->AV["name"] : FNode->AV["invname"];
     }
    else
     {
      if (ED->State != cbsGrayed)
       xVal += UnicodeString((ED->Checked) ? FMT(icsriBinaryCheckTxt).c_str() : FMT(icsriBinaryUnCheckTxt).c_str());
     }
   }
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Enabled = isEnabled && tmpClearVal;
  if (!isEnabled && tmpClearVal)
   {
    ED->Checked = false;
    FCtrlOwner->SetCtrlData(FNode, 0, "");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  if (AVal.Length())
   ED->Checked = AVal.ToIntDef(0);
  else
   {
    ED->State                              = cbsGrayed;
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Caption = (ACaption.Length()) ? ACaption : UnicodeString("");
  ED->Update();
  /* SIZE sz;
   TLabel *tmpLab = new TLabel(this);
   tmpLab->Parent = this;
   tmpLab->Transparent = true;
   tmpLab->Caption = ACaption;
   tmpLab->Font->Assign(ED->Style->Font);
   ::GetTextExtentPoint32(tmpLab->Canvas->Handle,ACaption.c_str(),ACaption.Length(),&sz);
   ED->Width = 25+sz.cx;
   this->Width = ED->Width+5;
   delete tmpLab;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  ED->OnClick = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinary::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FCtrlOwner->SetCtrlData(FNode, (ED->Checked == 0) ? 0 : 1, "");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriBinary::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriBinary::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriBinary::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return IntToStr((int)ED->Checked);
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriBinary::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
    if (FCtrlOwner->DataProvider->OnGetCompValue)
     {
      if (ED->State != cbsGrayed)
       RC = Variant((int)ED->Checked);
      FCtrlOwner->DataProvider->OnGetCompValue(FNode, RC);
     }
    else
     {
      if (ED->State != cbsGrayed)
       RC = Variant((int)ED->Checked);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriBinary::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled && (ED->State == cbsGrayed))
       RC = false;
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------

