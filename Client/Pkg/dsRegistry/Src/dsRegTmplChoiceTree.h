﻿//---------------------------------------------------------------------------

#ifndef dsRegTmplChoiceTreeH
#define dsRegTmplChoiceTreeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "cxButtonEdit.hpp"
//#include <DB.hpp>
/*
#include <Classes.hpp>
#include <DB.hpp>
//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3
  */
//---------------------------------------------------------------------------
#include "dsRegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriChoiceTreeTmpl: public TdsRegEDItem
{
private:
  TcxButton  *FClearBtn;
    UnicodeString FTreeClName;
    TLabel     *FExtLab;
    //    TList      *CBList;
    bool       FchIsGroup;
    UnicodeString FchCodeBeg;
    UnicodeString FchCodeEnd;
    UnicodeString FchExtCode;
    UnicodeString FchText;

    void       __fastcall cResize(TObject *Sender);
    void       __fastcall FDataChange(TObject *Sender);
//    void     __fastcall TreeBtnClick(TObject *Sender);
    void       __fastcall TreeBtnClick(TObject *Sender, int AButtonIndex);
//    bool       __fastcall FSetEditValue(UnicodeString AValue);

    bool          __fastcall FSetEditValue(bool AIsGroup, UnicodeString AValue, UnicodeString ALastValue = "");
    bool          __fastcall FGetIsGroup(UnicodeString ASrc);
    UnicodeString __fastcall FGetBeginCode(UnicodeString ASrc);
    UnicodeString __fastcall FGetEndCode(UnicodeString ASrc);
    UnicodeString __fastcall FGetTemplateVal(bool AIsGroup, int ABegin, int AEnd);
    UnicodeString __fastcall FGetTemplateVal(bool AIsGroup, UnicodeString ABegin, UnicodeString AEnd);

    TcxButtonEdit* __fastcall FGetCastED();
    void __fastcall FSetCastED(TcxButtonEdit *AVal);
    __property TcxButtonEdit *ED = {read=FGetCastED,write=FSetCastED};
  void __fastcall FClearClick(TObject *ASender);
  void __fastcall FEDKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
public:
    __fastcall TriChoiceTreeTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
    __fastcall ~TriChoiceTreeTmpl();
    void         __fastcall SetOnDependValues();
    void         __fastcall DataChange(TObject *Sender);
    Variant    __fastcall GetCompValue(UnicodeString ARef);
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetEnableEx(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString ACaption);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL, bool ASilent = false);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
  void __fastcall Clear();
};
//---------------------------------------------------------------------------
#endif
