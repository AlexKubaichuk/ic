﻿// ---------------------------------------------------------------------------
#include <vcl.h>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdChoiceTree.h"
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #pragma link "cxButtonEdit"
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// #define ED       dynamic_cast<TcxButtonEdit*>(ED)
// ---------------------------------------------------------------------------
__fastcall TriChoiceTree::TriChoiceTree(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  FTreeClName = FCtrlOwner->GetNode(FNode->AV["ref"])->AV["tblname"];
  // Создаём метку контрола
  FLab              = new TLabel(this);
  FLab->Parent      = this;
  FLab->Transparent = true;
  FLab->Caption     = FNode->AV["name"] + ":";
  // Создаём метку для текстового значения
  FExtLab              = new TLabel(this);
  FExtLab->Parent      = this;
  FExtLab->Transparent = true;
  FExtLab->AutoSize    = false;
  FExtLab->ParentFont  = true;
  FExtLab->WordWrap    = true;
  FExtLab->Color       = clInactiveBorder;
  // Создаём контрол
  ED                                      = new TcxButtonEdit(this);
  ED->Parent                              = this;
  ED->ShowHint                            = true;
  ED->Style->StyleController              = FCtrlOwner->StyleController;
  ED->Properties->ClickKey                = ShortCut(VK_DOWN, TShiftState() << ssAlt);
  ED->Properties->ReadOnly                = true;
  ED->Properties->ViewStyle               = vsHideCursor;
  ED->Properties->Buttons->Items[0]->Kind = bkGlyph;
  ED->Properties->Buttons->Items[0]->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\tree.bmp"));
  TcxEditButton * tmpBtn = ED->Properties->Buttons->Add();
  tmpBtn->Kind = bkGlyph;
  tmpBtn->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\clear.bmp"));
  /*
   }
   tmpBtn = ED->Properties->Buttons->Add();
   */
  // tmpBtn->Kind = bkText;
  // tmpBtn->Caption = "X";
  // tmpBtn->Glyph->LoadFromResourceName((int)HInstance,"CLEAR");
  ED->Properties->OnButtonClick = TreeBtnClick;
  // прочие настройки
  if (Required)
   ED->Style->Color = FReqColor;
  FLab->FocusControl = ED;
  if (IsAppend)
   {
    if (FCtrlOwner->IsTemplate(_UID_))
     {
      if (FSetEditValue(FGetBeginCode(FCtrlOwner->TemplateData->Value[_UID_]).ToIntDef(0)))
       FCtrlOwner->SetCtrlData(FNode, FGetBeginCode(FCtrlOwner->TemplateData->Value[_UID_]).ToIntDef(0), "");
      else
       FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // NULL val
     }
   }
  else
   FSetEditValue(FCtrlOwner->GetCtrlData(FNode, _ES_));
  FLab->Top  = 2;
  FLab->Left = 2;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Constraints->MinWidth = 60;
  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
    ED->Left = FLab->Width + 5;
    ED->Top  = 0;
   }
  else
   {
    ED->Left = 2;
    ED->Top  = FLab->Height + 2;
    TwoLine  = true;
   }
  ED->Width = AParent->Width - 4 - ED->Left;
  if ((ED->Left + ED->Width) > (AParent->Width - 4))
   {
    TwoLine   = true;
    ED->Left  = 4;
    ED->Top   = FLab->Height;
    ED->Width = AParent->Width - 4 - ED->Left;
   }
  FExtLab->Left = FLab->Left;
  FExtLab->Width  = Width - 4;
  FExtLab->Height = (FExtLab->Height - 2) * (FNode->AV["linecount"].ToIntDef(1) - 1);
  FExtLab->Top    = ED->Top + ED->Height;
  CMaxH           = FExtLab->Top + FExtLab->Height + 2;
  CMinH           = CMaxH;
  Height          = CMaxH;
  ED->OnKeyDown   = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
TcxButtonEdit * __fastcall TriChoiceTree::FGetCastED()
 {
  return (TcxButtonEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FSetCastED(TcxButtonEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetOnDependValues()
 {
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::TreeBtnClick(TObject * Sender, int AButtonIndex)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString SrcCode = "";
  if (AButtonIndex == 0)
   {
    /* if (FCtrlOwner->OnExtBtnClick)
     {
     UnicodeString Src;

     Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
     //        if (FCtrlOwner->OnExtBtnClick(FNode,Src,FSrc,true,(TObject*) FCtrlOwner, 0))
     if (FCtrlOwner->OnExtBtnClick(FNode,Src,NULL,true,(TObject*) FCtrlOwner, 0))
     {
     int ind = Src.Pos("=");
     SrcCode = Src.SubString(ind+1,Src.Length()-ind);
     //           if (SrcDSEditable())
     //            {
     if (FSetEditValue(SrcCode))
     FCtrlOwner->SetCtrlData(FNode, SrcCode, "");
     else
     FCtrlOwner->SetCtrlData(FNode, 0, "");//               irquSetAsNull(SrcDS(),FFlName);
     //            }
     }
     } */
    TdsRegTreeClassForm * Dlg = FCtrlOwner->GetTreeFormDlg(FTreeClName);
    Dlg->TemplateMode = false;
    Dlg->Caption      = FNode->AV["dlgtitle"];
    __int64 FClCode = FCtrlOwner->GetCtrlData(FNode, _ES_);
    if (FClCode)
     Dlg->clCode = FClCode;
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      if (FSetEditValue(IntToStr(Dlg->clCode)))
       FCtrlOwner->SetCtrlData(FNode, Dlg->clCode, "");
      else
       FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // irquSetAsNull(SrcDS(),FFlName);
     }
   }
  else
   {
    // if (SrcDSEditable())
    // {
    FSetEditValue("");
    FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // irquSetAsNull(SrcDS(),FFlName);
    // }
   }
 }
// ---------------------------------------------------------------------------
__fastcall TriChoiceTree::~TriChoiceTree()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnButtonClick = NULL;
  delete FExtLab;
  delete ED;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTree::FSetEditValue(UnicodeString AValue)
 {
  bool RC = false;
  try
   {
    if (AValue.ToIntDef(-1) != -1)
     {
      try
       {
        UnicodeString FNameVal, FExtCodeVal;
        FNameVal         = FCtrlOwner->GetTreeValue(FTreeClName, AValue, FExtCodeVal);
        FExtLab->Caption = FNameVal;
        ED->Hint         = FNameVal;
        ED->Text         = FExtCodeVal;
        RC               = true;
       }
      __finally
       {
       }
     }
    else
     {
      FExtLab->Caption = "";
      ED->Text         = "";
     }
    DataChange(this);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  FExtLab->Width = ED->Left + ED->Width;
  Width          = ED->Left + ED->Width + 4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine && ED)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine && FLab)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    FExtLab->Left  = 2;
    FExtLab->Width = Width - 4;
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4;
     }
    else
     ED->Left = Width - ED->Width - 2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTree::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTree::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  xVal = ED->Text;
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  FLab->Enabled    = isEnabled && tmpClearVal; // isEdit && !isDelegate;
  ED->Enabled      = isEnabled && tmpClearVal;
  FExtLab->Enabled = isEnabled && tmpClearVal;
  if (!isEnabled && tmpClearVal)
   {
    FSetEditValue("");
    FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // irquSetAsNull(SrcDS(),FFlName);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  // if (SrcDSEditable())
  // {
  if (FSetEditValue(AVal))
   FCtrlOwner->SetCtrlData(FNode, AVal, "");
  else
   FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // irquSetAsNull(SrcDS(),FFlName);
  // }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN) // (Key == 'F') && Shift.Contains(ssCtrl)
   {
    // if (FEDModify)
    // {
    // FEDBtnClick(NULL, 0);
    // Key = 0;
    // }
   }
  else if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    // FEDBtnClick(NULL, 1);
    // Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceTree::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceTree::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTree::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return FCtrlOwner->GetCtrlData(FNode, _ES_);
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriChoiceTree::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " ' tagtype not supported'");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTree::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED)
       {
        if (ED->Enabled)
         {
          if (!((TcxButtonEdit *)ED)->Text.Length())
           RC = false;
         }
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTree::FGetBeginCode(UnicodeString ASrc)
 {
  return GetPart1(GetPart2(ASrc, ':'), '#');
 }
// ---------------------------------------------------------------------------
