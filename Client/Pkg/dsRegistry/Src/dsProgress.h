//---------------------------------------------------------------------------

#ifndef dsProgressH
#define dsProgressH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxProgressBar.hpp"
//---------------------------------------------------------------------------
class TdsProgressForm : public TForm
{
__published:	// IDE-managed Components
 TTimer *ShowTimer;
 TcxProgressBar *PrBar;
 void __fastcall ShowTimerTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
 __fastcall TdsProgressForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsProgressForm *dsProgressForm;
extern PACKAGE TdsProgressForm* __fastcall ShowProgress(TForm *AParent, UnicodeString AText = "");
//---------------------------------------------------------------------------
#endif
