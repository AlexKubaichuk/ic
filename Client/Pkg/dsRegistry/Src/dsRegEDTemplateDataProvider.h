//---------------------------------------------------------------------------

#ifndef dsRegEDTemplateDataProviderH
#define dsRegEDTemplateDataProviderH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "dsRegEDBaseDataProvider.h"

//#include "dsRegConstDef.h"
//#include "cxContainer.hpp"
//#include "dsRegTypeDef.h"
//#include "XMLContainer.h"
//#include "dsRegEDFunc.h"
//#include "dsRegTreeClass.h"
//---------------------------------------------------------------------------
class PACKAGE TdsRegEDTemplateDataProvider : public TdsRegEDBaseDataProvider
{
typedef map<UnicodeString,Variant> TdsRegTemplateDataMap;
private:
  TdsGetValByIdEvent FOnGetValById;
protected:
public:
  __fastcall TdsRegEDTemplateDataProvider();
  __fastcall ~TdsRegEDTemplateDataProvider();

  Variant __fastcall GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption);
  void    __fastcall SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue);

  bool    __fastcall FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode = -1);
  Variant __fastcall GetChDBData(UnicodeString ARef, UnicodeString RecVal);

  __property TdsGetValByIdEvent OnGetValById = {read=FOnGetValById, write=FOnGetValById};
};
//---------------------------------------------------------------------------
extern PACKAGE bool __fastcall FillClassCB(TdsGetValByIdEvent AOnGetValById, UnicodeString ARef, TStringList *AValues, __int64 ACode);
//---------------------------------------------------------------------------
#endif
