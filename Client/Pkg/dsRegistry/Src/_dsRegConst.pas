unit dsRegConst;

interface

resourcestring
// Common
  icsErrorMsgCaption = '������';
  icsErrorMsgCaptionSys = '������.'+#13#10+'��������� ���������:'+#13#10+'%s';
  icsMessageMsgCaption = '���������';
  icsInputErrorMsgCaption = '������ �����';
  icsRecCount1       = '������';
  icsRecCount2       = '������';
  icsRecCount3       = '�������';
  icsCommonRecCount  = '�����:';
  icsFieldNotFound   = '���� "%s" � ������� "%s" �����������.';
  icsRegListCreate   = '������������ ������';

  icsRegErrorInsertRec = '������ ���������� ������.';
  icsRegErrorInsertRecSys = '������ ���������� ������, ��������� ���������:'+#13#10+'%s';
  icsRegErrorEditRec   = '������ �������������� ������.';
  icsRegErrorEditRecSys = '������ �������������� ������, ��������� ���������:'+#13#10+'%s';
  icsRegErrorDeleteRec = '������ �������� ������.';
  icsRegErrorDeleteRecSys = '������ �������� ������, ��������� ���������:'+#13#10+'%s';
  icsRegErrorSearch    = '������ ������; ��������:';
  icsRegRecFound       = '��� ����������! ������� ��������� � ��������� ������.';

  icsRegErrorConfirmDeleteRec   = '�������������';
  icsRegErrorConfirmDeleteRecMsgCaption   = '������������� �������� ������';
  icsRegTransportErrorNoRegDefXML = '�� ������� xml-�������� ������������';
  icsRegTransportErrorNoEIDataProvider = '�� ����� ��������� ��������/������� ������';

// DTFormats
  icsDTFormatsD      = 'dd.mm.yyyy �.';
  icsDTFormatsDT     = 'dd.mm.yyyy hh:nn:ss';
  icsDTFormatsT      = 'hh:nn:ss';

// RegEd
  iscRegEDErrorNoOwner   = '�� �������� ��������� ��������.';
  iscRegEDErrorNoRef     = '����������� ������ �� ������.';
  iscRegEDErrorRefObject = '"��������� ������ ������ -> "%s".';

// RegItem common
  icsRegItemErrorReqField = '�� ������� �������� !'+#13#10+'��� ������������� ���� "%s".';
// RegItem binary
  icsriBinaryCheckTxt   = '�������';
  icsriBinaryUnCheckTxt = '�� �������';
// RegItem ChoiceTreeTmpl
  icsriChoiceTreeSubClass = '{��������� ���������} ';
// RegItem Digit
  icsriDigitValRange = '�������� ���� "%s" ������ ���� � ��������� ��: %s ��: %s.';


//  TdsRegEDContainer
  dsRegEDContainerErrorNoOwner         = '�� ���!!!, �������� - Owner ������ ���� ��������.';
  dsRegEDContainerErrorNoParent        = '�� ���!!!, �������� - Parent ������ ���� ��������.';
  dsRegEDContainerErrorNoTmpQuery      = '��������� ��� ���������� ��������� �������� ������ ���� �����. ������ = (FIBTemplateQuery ��� ADOTemplateQuery = NULL)';
  dsRegEDContainerErrorTmpQueryTypeADO = '��������� ��� ���������� ��������� �������� ������ ���� ���� TADODataSet.';
  dsRegEDContainerErrorTmpQueryTypeFIB = '��������� ��� ���������� ��������� �������� ������ ���� ���� TpFIBQuery.';
  dsRegEDContainerErrorNoDataSrc       = '�������� ������ ������ ���� �����. ������ = (DataSource = NULL)';
  dsRegEDContainerErrorNoDataDef       = '�������� �������� �������� ������ ���� �����. ������ = (ANode = NULL)';
  dsRegEDContainerErrorNoDataDefId     = '��� �������� �������� ������ ���� �����. ������ = (ANode->AV["uid"] = "")';

  dsRegEDContainerErrorDSTypeADO       = '��������� ��������� � ������ ������� � ������ ����� ADO - ����������.';
  dsRegEDContainerErrorDSTypeFIB       = '��������� ��������� � ������ ������� � ������ ����� FIB - ����������.';
  dsRegEDContainerErrorEDNo            = '�������� � ���=''%s'' �����������.';
  dsRegEDContainerErrorEDExists        = '�������� � ���=''%s'' ��� ����������.';
  dsRegEDContainerErrorEDCreate        = '������ �������� ��������� � ���=''%s''.';
  dsRegEDContainerSysErrorEDCreate     = '������ �������� ��������� � ���=''%s''.'+#13#10+'��������� ���������:%s';
  dsRegEDContainerErrorTmplNo          = '�������� ������� � ���=''%s'' �����������.';
  dsRegEDContainerErrorTmplExists      = '�������� ������� � ���=''%s'' ��� ����������.';
  dsRegEDContainerErrorTmplCreate      = '������ �������� ��������� ������� � ���=''%s''.';
  dsRegEDContainerSysErrorTmplCreate   = '������ �������� ��������� ������� � ���=''%s''.'+#13#10+'��������� ���������:%s';
  dsRegEDContainerErrorLabNo           = '��������� ����� � ���=''%s'' �����������.';
  dsRegEDContainerErrorLabExists       = '��������� ����� � ���=''%s'' ��� ����������.';
  dsRegEDContainerErrorLabCreate       = '������ �������� ��������� ����� � ���=''%s''.';
  dsRegEDContainerSysErrorLabCreate    = '������ �������� ��������� ����� � ���=''%s''.'+#13#10+'��������� ���������:%s';

// RegEDFunc
  icsRegEDFuncErrorGetValues            = '������ ��������� �������� ��������������.';

// RegEI
  icsRegEIErrorClassesExport            = '������ �������� ���������������.';
  icsRegEIErrorClassExport              = '������ �������� ��������������.';
  icsRegEIErrorUnitsExport              = '������ �������� ������ ������ �����.';
  icsRegEIErrorUnitExport               = '������ �������� ������� �����.';
  icsRegEIErrorClassesImport            = '������ ������� ���������������.';
  icsRegEIErrorClassImport              = '������ ������� �������������� ''%s''.';
  icsRegEIErrorLastInqDate              = '���� ���������� ������������.';
  icsRegEIErrorUnitImport               = '������ ������� ������ ��������.';
  icsRegEIErrorUnitsImport              = '������ ������� ������ ������ �����.';

// TICSRegTransport
  icsRegTransportErrorClassesExport            = '������ �������� ���������������.';
  icsRegTransportErrorClassesExportSys         = '������ �������� ���������������.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorClassExport              = '������ �������� ��������������.';
  icsRegTransportErrorClassExportSys           = '������ �������� ��������������.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorUnitsExport              = '������ �������� ������ ������ �����.';
  icsRegTransportErrorUnitsExportSys           = '������ �������� ������ ������ �����.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorUnitExport               = '������ �������� ������� �����.';
  icsRegTransportErrorUnitExportSys            = '������ �������� ������� �����.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorClassesImport            = '������ ������� ���������������.';
  icsRegTransportErrorClassesImportSys         = '������ ������� ���������������.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorClassImport              = '������ ������� �������������� ''%s''.';
  icsRegTransportErrorClassImportSys           = '������ ������� �������������� ''%s''.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorLastInqDate              = '���� ���������� ������������.';
//  icsRegTransportErrorUnitImport               = '������ ������� ������ ��������.';
  icsRegTransportErrorUnitsImport              = '������ ������� ������ ������ �����.';
  icsRegTransportErrorUnitsImportSys           = '������ ������� ������ ������ �����.'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorUnitsImportData          = '������ ������� ������ ������ �����.'+#13#10+'������: %s';
  icsRegTransportErrorUnitsImportDataSys       = '������ ������� ������ ������ �����.'+#13#10+'������: %s'+#13#10+'��������� ���������:%s';
  icsRegTransportErrorGetClsKey                = '������ ��������� �������� ����� ��� ��������������, ��� = %s';
  icsRegTransportErrorGetClsKeySys             = '������ ��������� �������� ����� ��� ��������������, ��� = %s'+#13#10+'��������� ���������:%s';


// TRegXMLEIDataProvider
  icsRegXMLEIDataProviderRecNotFound           = '������ ��������� ��������, ������ �����������.';
  icsRegXMLEIDataProviderIEDataNotFound        = '�� ������ �������� �������� ������ ������.';
  icsRegXMLEIDataProviderErrorCreateSect       = '������ �������� ������. ������ "%s" �� ��������� � ������� "%s".';
  icsRegXMLEIDataProviderErrorSectNotFound     = '����������� ������ "%s".';

// TRegFIBEIDataProvider
  icsRegFIBEIDataProviderErrorExecProcSys      = '������ ���������� �������� ��������� "%s".'+#13#10+'��������� ���������: %s"';
  icsRegFIBEIDataProviderErrorExecProc         = '������ ���������� �������� ��������� "%s".';
  icsRegFIBEIDataProviderErrorExecSQLSys       = '������ ���������� SQL �������:'+#13#10+'"%s".'+#13#10+'��������� ���������: %s"';
  icsRegFIBEIDataProviderErrorExecSQL          = '������ ���������� SQL �������:'+#13#10+'"%s".';
  icsRegFIBEIDataProviderNoQueryDef            = '����������� ��������� ��� ���������� �������.';
  icsRegFIBEIDataProviderErrorSectNotFound     = '����������� ������ "%s".';

//  icsRegXMLEIDataProviderErrorCreateSect       = '������ �������� ������. ������ "%s" �� ��������� � ������� "%s".';
//  icsRegXMLEIDataProviderErrorSectNotFound     = '����������� ������ "%s".';

// ICSRegistry
  icsRegistryErrorDBNameNotDef          = '�� ������ ��� ��.';
  icsRegistryErrorNoRegDef              = '�� ������ �������� ������������.';
  icsRegistryErrorDBConnect             = '������ ����������� � �� ��� �������� �������� ������������.';
  icsRegistryErrorCommDBErrorMsgCaption = '������ ������ � ��';
  icsRegistryErrorDBErrorMsgCaption     = '������ ������ � ��, ���: %s';
  icsRegistryErrorDBNotExist            = '���� ������ "%s" �����������!';
  icsRegistryErrorNoServer              = '�� ������� "FireBird" server.';
  icsRegistryErrorLoadRegDef            = '������ �������� �������� ������������.';
  icsRegistryErrorLoadDM                = '������������. ������ �������� ������ ������.'+#13#10+'��������� ���������:%s';

// ICSTemplate
  icsTemplateReqField   = '�� ������ �������� ��� ������������� ���� ''%s''.';
  icsTemplateNoClassDef = '�� ������ �������� ���������������.';
  icsTemplateNoFltDef   = '�� ������ �������� �������.';
  icsTemplateRefError   = '��� � ���=''%s'' �����������.';
  icsTemplateReqEmpty   = '���������� ������� �������� ��� ������� ��� ������ ���������.';

// TITemplateForm
  icsITemplateCaption   = '������������ �������';
  icsITemplateApplyBtn  = '���������';
  icsITemplateCancelBtn = '������';
  icsIErrTreeFormMsg    = '������ ������������ ����� ��� "%s".';

// Reg SearchUnit
  icsRegSearchEndMsg           = '����� ��������.'+#13#10+'������ � ������ ������?';

  icsRegSearchCaption           = '�����';
  icsRegSearchSettingBtnCaption = '���������';
  icsRegSearchFindBtnCaption    = '�����';
  icsRegSearchCancelBtnCaption  = '������';
  icsRegSearchNotFound          = '���������� �� �������.';
// Reg SearchUnit Setting
  icsRegSearchSettingCaption             = '��������� ������';
  icsRegSearchSettingFieldListLabCaption = '������ ����� ��� ������:';
  icsRegSearchSettingSaveBtnCaption      = '���������';
  icsRegSearchSettingCancelBtnCaption    = '������';

// RegSelList
  icsRegSelListSelectAllCaption      = '������� ���';
  icsRegSelListSelectAllHint         = '������� ��� ...';
  icsRegSelListSelectAllShortCut     = 'Ins';

  icsRegSelListUnSelectAllCaption    = '����� ���������';
  icsRegSelListUnSelectAllHint       = '����� ��������� ...';
  icsRegSelListUnSelectAllShortCut   = 'Del';

  icsRegSelListApplyCaption          = '���������';
  icsRegSelListApplyHint             = '��������� ...';
  icsRegSelListApplyShortCut         = 'Shift+Enter';

  icsRegSelListSetTemplateCaption    = '���������� ������';
  icsRegSelListSetTemplateHint       = '���������� ������ ...';
  icsRegSelListSetTemplateShortCut   = 'Ctrl+T';

  icsRegSelListReSetTemplateCaption  = '����� ������';
  icsRegSelListReSetTemplateHint     = '����� ������ ...';
  icsRegSelListReSetTemplateShortCut = 'Ctrl+G';

// RegList
  icsRegListInsertCaption      = '��������';
  icsRegListInsertHint         = '�������� ...';
  icsRegListInsertShortCut     = 'Ins';

  icsRegListEditCaption      = '��������';
  icsRegListEditHint         = '�������� ...';
  icsRegListEditShortCut     = 'Shift+Enter';

  icsRegListDeleteCaption      = '�������';
  icsRegListDeleteHint         = '������� ...';
  icsRegListDeleteShortCut     = 'Del';

  icsRegListSetTemplateCaption      = '���������� ������';
  icsRegListSetTemplateHint         = '���������� ������ ...';
  icsRegListSetTemplateShortCut     = 'Ctrl+T';

  icsRegListReSetTemplateCaption      = '����� ������';
  icsRegListReSetTemplateHint         = '����� ������ ...';
  icsRegListReSetTemplateShortCut     = 'Ctrl+G';

  icsRegListOpenCaption      = '������� �����';
  icsRegListOpenHint         = '������� ����� ...';
  icsRegListOpenShortCut     = 'Ctrl+Enter';

  icsRegListExtCaption      = '� �����';
  icsRegListExtHint         = '� ����� ...';
  icsRegListExtShortCut     = '';

  icsRegListRefreshCaption      = '��������';
  icsRegListRefreshHint         = '�������� ...';
  icsRegListRefreshShortCut     = 'F5';

  icsRegListFindCaption      = '�����';
  icsRegListFindHint         = '����� ...';
  icsRegListFindShortCut     = 'Ctrl+F';

  icsRegListFindNextCaption      = '����� �����';
  icsRegListFindNextHint         = '����� ����� ...';
  icsRegListFindNextShortCut     = 'F3';

  icsRegListViewUnitParamCaption      = '��������� ��������';
  icsRegListViewUnitParamHint         = '��������� �������� ...';
  icsRegListViewUnitParamShortCut     = 'Alt+1';

  icsRegListViewTemplateParamCaption      = '��������� �������';
  icsRegListViewTemplateParamHint         = '��������� ������� ...';
  icsRegListViewTemplateParamShortCut     = 'Alt+2';

  icsRegListPrintListBICaption  = '������';
  icsRegListPrintListBIHint     = '������...';
  icsRegListPrintListBIShortCut = 'Ctrl+P';


// RegListEdit
  icsRegListEditApplyBtnCaption = '���������';
  icsRegListEditCancelBtnCaption = '������';

// RegClassif
  icsRegClassifInsertCaption      = '��������';
  icsRegClassifInsertHint         = '�������� ...';
  icsRegClassifInsertShortCut     = 'Ins';

  icsRegClassifEditCaption      = '��������';
  icsRegClassifEditHint         = '�������� ...';
  icsRegClassifEditShortCut     = 'Shift+Enter';

  icsRegClassifDeleteCaption      = '�������';
  icsRegClassifDeleteHint         = '������� ...';
  icsRegClassifDeleteShortCut     = 'Del';

  icsRegClassifSetTemplateCaption      = '���������� ������';
  icsRegClassifSetTemplateHint         = '���������� ������ ...';
  icsRegClassifSetTemplateShortCut     = 'Ctrl+T';

  icsRegClassifReSetTemplateCaption      = '����� ������';
  icsRegClassifReSetTemplateHint         = '����� ������ ...';
  icsRegClassifReSetTemplateShortCut     = 'Ctrl+G';

  icsRegClassifRefreshCaption      = '��������';
  icsRegClassifRefreshHint         = '�������� ...';
  icsRegClassifRefreshShortCut     = 'F5';

  icsRegClassifFindCaption      = '�����';
  icsRegClassifFindHint         = '����� ...';
  icsRegClassifFindShortCut     = 'Ctrl+F';

  icsRegClassifFindNextCaption      = '����� �����';
  icsRegClassifFindNextHint         = '����� ����� ...';
  icsRegClassifFindNextShortCut     = 'F3';

  icsRegClassifViewClassParamCaption      = '��������� �������� ��������������';
  icsRegClassifViewClassParamHint         = '��������� �������� �������������� ...';
  icsRegClassifViewClassParamShortCut     = 'Alt+1';

  icsRegClassifViewTemplateParamCaption      = '��������� �������';
  icsRegClassifViewTemplateParamHint         = '��������� ������� ...';
  icsRegClassifViewTemplateParamShortCut     = 'Alt+2';
// RegListEdit
  icsRegClassifEditApplyBtnCaption = '���������';
  icsRegClassifEditCancelBtnCaption = '������';

// TTreeClassForm
  icsRegTreeClassLoadMsg    = '�������� �������������� "%s" ��� "%s"';
  icsRegTreeClassLoadError  = '������ �������� ��������������';

  icsRegTreeClassSelGroupCaption  = '������� &��������� ��������';
  icsRegTreeClassFindLabCaption   = '�����:';
  icsRegTreeClassApplyBtnCaption  = '���������';
  icsRegTreeClassCancelBtnCaption = '������';
implementation

end.
