//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsRegEDFunc.h"
#include "math.h"
#include "dsRegConstDef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
UnicodeString __fastcall FieldCast(TTagNode *ADefNode, UnicodeString AVal)
{
  UnicodeString FVal = AVal.Trim();
  if (FVal.Length())
   {
     if (ADefNode->CmpAV("cast","lo"))
      FVal = FVal.LowerCase();
     else if (ADefNode->CmpAV("cast","up"))
      FVal = FVal.UpperCase();
     else if (ADefNode->CmpAV("cast","up1st"))
      FVal = FVal.SubString(1,1).UpperCase() + FVal.SubString(2,FVal.Length()-1).LowerCase();
   }
  return FVal;
}
//---------------------------------------------------------------------------
Extended __fastcall ICSStrToFloat(UnicodeString AVal)
{
  UnicodeString FVal = AVal.Trim();
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  FVal = StringReplace(FVal, ",", FormatSettings.DecimalSeparator, rFlag);
  FVal = StringReplace(FVal, ".", FormatSettings.DecimalSeparator, rFlag);
  FVal = StringReplace(FVal, " ", "", rFlag);
  FVal = StringReplace(FVal, FormatSettings.ThousandSeparator, "", rFlag);

  return StrToFloat(FVal);
}
//---------------------------------------------------------------------------
Extended __fastcall ICSStrToFloatDef(UnicodeString AVal, Extended ADefVal)
{
  try
   {
     return ICSStrToFloat(AVal);
   }
  catch (EConvertError& E)
   {
     return ADefVal;
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall icsCalcMask(bool AIsFolat, int ADigits, int ADecimal, UnicodeString AMin)
{
  UnicodeString RC = "";
  try
   {
     UnicodeString tmpMask;
     UnicodeString tmpMaskSub = "";
     if (!AIsFolat)
      {
        int FMin = AMin.ToIntDef(0);
        if (abs(FMin) != FMin)
         {
          if (ADigits > 1)
           tmpMaskSub = "[-]\\d\\d{"+IntToStr(ADigits-2)+"}";
          else
           throw Exception("��� ������������� ����� ���������� �������� ������ ���� ������ 1");
         }
        RC = "\\d{"+IntToStr(ADigits)+"}";
      }
     else
      {
        Extended FMin = ICSStrToFloatDef(AMin, 0.0);
        if (fabs(FMin) != FMin)
         {
           for (int i = 1; i <= ADecimal; i++)
            {
              tmpMask = "[-]";
              for (int j = 1; j <= ADigits - 2 - i; j++)
               tmpMask += "\\d?";
              tmpMask = "("+tmpMask+"[.,]?\\d{"+IntToStr(i)+"})";
              if (tmpMaskSub.Length())
               tmpMaskSub += "|"+tmpMask;
              else
               tmpMaskSub = tmpMask;
            }
         }
        for (int i = 1; i <= ADecimal; i++)
         {
           tmpMask = "";
           for (int j = 1; j <= ADigits - 1 - i; j++)
            tmpMask += "\\d?";
           tmpMask = "("+tmpMask+"[.,]?\\d{"+IntToStr(i)+"})";
           if (RC.Length())
            RC += "|"+tmpMask;
           else
            RC = tmpMask;
         }
      }
     if (tmpMaskSub.Length())
      RC = "("+tmpMaskSub+")|("+RC+")";
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

