//---------------------------------------------------------------------------

#ifndef dsRegExtEditH
#define dsRegExtEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "cxButtonEdit.hpp"
//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3

//---------------------------------------------------------------------------
#include "dsRegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriExtEdit: public TdsRegEDItem
{
private:

  TLabel*      FExtLab;
  TcxListBox*  FLb;
  bool         FEDModify;
  bool         FEDSetValue;
  bool         FSearchMode;
  TAnsiStrMap    FFindedObjects;
    void     __fastcall cResize(TObject *Sender);
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    UnicodeString __fastcall FSetEditValue(UnicodeString AVal);
    void     __fastcall FDataChange(TObject *Sender);
    void     __fastcall ExtBtnClick(TObject *Sender, int AButtonIndex);
    TcxButtonEdit* __fastcall FGetCastED();
    void __fastcall FSetCastED(TcxButtonEdit *AVal);
    __property TcxButtonEdit *ED = {read=FGetCastED,write=FSetCastED};

  void __fastcall FSearchLBDblClick(TObject *Sender);
  void __fastcall FSearchLBKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
  void __fastcall FSearchLBExit(TObject* Sender);
  void __fastcall FEDKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
  void __fastcall FEDBtnClick(TObject *Sender, int AButtonIndex);
  void __fastcall FEDChanged(TObject *Sender);
public:
    void         __fastcall SetOnDependValues();
    void         __fastcall DataChange(TObject *Sender);
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString ACaption);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
__published:
    __fastcall TriExtEdit(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,
                          TdsRegEDContainer *ACtrlOwner, bool IsApp);
    __fastcall ~TriExtEdit();
  
  __property bool EDModify = {read=FEDModify};
  __property bool SearchMode = {read=FSearchMode};
};
//---------------------------------------------------------------------------
#endif
