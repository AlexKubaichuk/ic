// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop
#include "dsRegTreeClass.h"
#include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxProgressBar"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxStatusBarPainter"
#pragma link "dxStatusBar"
#pragma link "cxCustomData"
#pragma link "cxInplaceContainer"
#pragma link "cxStyles"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "cxClasses"
#pragma resource "*.dfm"
TdsRegTreeClassForm * dsRegTreeClassForm;
// ---------------------------------------------------------------------------
__fastcall TdsRegTreeClassForm::TdsRegTreeClassForm(TComponent * Owner, TTagNode * ADef) : TForm(Owner)
 {
  try
   {
    ClassTree->BeginUpdate();
    ClassTree->Clear();
    TagNodeToTreeNode(ClassTree->Root, ADef);
    ClassTree->Root->Expand(false);
   }
  __finally
   {
    ClassTree->EndUpdate();
    // ClassTree->SetFocus();
    // ClassTree->Root->Focused = true;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::TagNodeToTreeNode(TcxTreeListNode * ADest, TTagNode * ASrc)
 {
  bool FCanIterate = true;
  TTagNode * itNode;
  if (ASrc->CmpName("i"))
   {
    ADest->Texts[0] = ASrc->AV["e"] + " : " + ASrc->AV["n"];
    ADest->Texts[1] = ASrc->AV["n"];
    ADest->Texts[2] = ASrc->AV["e"];
    ADest->Texts[3] = ASrc->AV["c"];
    ADest->Texts[4] = ASrc->AV["i"];
   }
  itNode = ASrc->GetFirstChild();
  while (FCanIterate && itNode)
   {
    TagNodeToTreeNode(ADest->AddChild(), itNode);
    itNode = itNode->GetNext();
   }
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsRegTreeClassForm::GetLastGroupNode(TcxTreeListNode * ARoot)
 {
  TcxTreeListNode * RC = ARoot;
  try
   {
    if (RC)
     {
      TcxTreeListNode * LC = GetLastGroupNode(RC->GetLastChild());
      if (LC)
       RC = LC;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::BtnOkClick(TObject * Sender)
 {
  if (ClassTree->SelectionCount)
   {
    FclText        = ClassTree->Selections[0]->Texts[1];
    FclExtCode     = ClassTree->Selections[0]->Texts[2];
    FclCode        = ClassTree->Selections[0]->Texts[3].ToIntDef(0);
    FclSelectGroup = (SelGroupChB->Visible) ? SelGroupChB->Checked : false;
    FclCodeLast    = -1;
    if (FclSelectGroup)
     {
      TcxTreeListNode * LC = GetLastGroupNode(ClassTree->Selections[0]);
      if (LC)
       FclCodeLast = LC->Texts[3].ToIntDef(0);
      else
       FclCodeLast = -1;
     }
    ModalResult = mrOk;
   }
  else
   _MSG_ERR("���������� ������� ��������.", "������");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   BtnOkClick(this);
  if (Shift.Contains(ssCtrl) && Key == 'F')
   QSearchEDPropertiesButtonClick(QSearchED, 0);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::QSearchEDPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSearchText(QSearchED->Text, TextCol);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::FSetClCode(__int64 AVal)
 {
  FclCode = AVal;
  FSearchText(IntToStr(AVal), CodeCol);
  FFirstSN = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTreeClassForm::FGetTemplateMode()
 {
  return !SelGroupChB->Visible;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::FSetTemplateMode(bool AMode)
 {
  SelGroupChB->Visible = AMode;
  TmplPanel->Visible   = AMode;
  TmplPanel->Height    = (AMode) ? 25 : 0;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::ClassTreeStylesGetContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode)
   {
    if (ANode->Texts[4].ToIntDef(0))
     AStyle = bStyle;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::ClassTreeFocusedNodeChanged(TcxCustomTreeList * Sender,
  TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  if (AFocusedNode)
   {
    OkBtn->Enabled                    = AFocusedNode->Texts[4].ToIntDef(0);
    StatusBar->Panels->Items[1]->Text = AFocusedNode->Texts[0];
   }
  else
   StatusBar->Panels->Items[1]->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::QSearchEDPropertiesChange(TObject * Sender)
 {
  FFirstSN             = NULL;
  SearchTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::FSearchText(UnicodeString AText, TcxTreeListColumn * AColumn)
 {
  if (AText.Length())
   {
    FFirstSN = ClassTree->FindNodeByText(AText, AColumn, FFirstSN, false, true, false, tlfmNormal,
    NULL /* SP */ , true);
    if (FFirstSN)
     {
      // ClassTree->SetFocus();
      FFirstSN->MakeVisible();
      FFirstSN->Focused = true;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTreeClassForm::SearchTimerTimer(TObject * Sender)
 {
  SearchTimer->Enabled = false;
  FSearchText(QSearchED->Text, TextCol);
  QSearchED->SetFocus();
  QSearchED->SelStart = QSearchED->Text.Length();
 }
// ---------------------------------------------------------------------------
