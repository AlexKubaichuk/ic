﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsRegTmplBinary.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define dbED_ChB     ((TcxDBCheckBox*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
// ---------------------------------------------------------------------------
__fastcall TriBinaryTmpl::TriBinaryTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  ED                         = new TcxCheckBox(this);
  ED->Parent                 = this;
  ED->State                  = cbsGrayed;
  ED->Caption                = FNode->AV["name"];
  ED->Transparent            = true;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  if (Required)
   ED->Style->Color = FReqColor;
  CMaxH                          = 21;
  CMinH                          = 21; // Height = 18;
  ED->Constraints->MinWidth      = 60;
  ED->Left                       = 5;
  ED->Top                        = 1;
  ED->Height                     = 21;
  ED->Width                      = ED->Canvas->TextWidth(ED->Caption) + 10;
  Height                         = 21;
  ED->Properties->Alignment      = taLeftJustify;
  ED->Properties->ValueChecked   = "1";
  ED->Properties->ValueUnchecked = "0";
  ED->State                      = cbsGrayed;
  ED->OnClick                    = DataChange;
  ED->Properties->OnChange       = DataChange;
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   {
    ED->Checked = FCtrlOwner->TemplateData->Value[_UID_];
   }
  ED->Enabled = isEnabled && isEdit && (!isTemplate || FCtrlOwner->TemplateNoLock());
  FClearBtn->Top = ED->Top + ED->Height / 2 - 10;
  ED->OnKeyDown  = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::FClearClick(TObject * ASender)
 {
  ED->State                              = cbsGrayed;
  FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxCheckBox * __fastcall TriBinaryTmpl::FGetCastED()
 {
  return (TcxCheckBox *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::FSetCastED(TcxCheckBox * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriBinaryTmpl::~TriBinaryTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->OnClick = NULL;
  ED->Parent  = NULL;
  delete ED;
  ED = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  Width   = ED->Left + ED->Width + 2;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  *AEDLeft = -1;
  *ALabR   = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    ED->Left        = 2;
    ED->Width       = Width - 4 - _TMPL_CLEAR_WIDTH_;
    FClearBtn->Left = ED->Left + ED->Width + 2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriBinaryTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriBinaryTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
    if (ABinaryNames)
     {
      if (ED->State != cbsGrayed)
       xVal += (ED->Checked) ? FNode->AV["name"] : FNode->AV["invname"];
     }
    else
     {
      if (ED->State != cbsGrayed)
       xVal += UnicodeString((ED->Checked) ? FMT(icsriBinaryCheckTxt).c_str() : FMT(icsriBinaryUnCheckTxt).c_str());
     }
   }
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FClearBtn->Enabled = ED->Enabled;
  if (!(isEnabled && isEdit && !isTemplate))
   {
    if (isEdit && !isTemplate)
     {
      ED->State                              = cbsGrayed;
      FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  if (AVal.Length())
   {
    ED->Checked                            = AVal.ToIntDef(0);
    FCtrlOwner->TemplateData->Value[_UID_] = AVal.ToIntDef(0);
   }
  else
   {
    ED->State                              = cbsGrayed;
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Caption = (ACaption.Length()) ? ACaption : UnicodeString("");
  ED->Update();
  /* SIZE sz;
   TLabel *tmpLab = new TLabel(this);
   tmpLab->Parent = this;
   tmpLab->Transparent = true;
   tmpLab->Caption = ACaption;
   tmpLab->Font->Assign(ED->Style->Font);
   ::GetTextExtentPoint32(tmpLab->Canvas->Handle,ACaption.c_str(),ACaption.Length(),&sz);
   ED->Width = 25+sz.cx;
   this->Width = ED->Width+5;
   delete tmpLab;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  ED->OnClick = FDataChange;
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->State == cbsGrayed)
   FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  else
   FCtrlOwner->TemplateData->Value[_UID_] = (ED->Checked) ? 1 : 0;
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->TemplateData->Value[_UID_];
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriBinaryTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriBinaryTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriBinaryTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return IntToStr((int)ED->Checked);
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriBinaryTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
    if (FCtrlOwner->DataProvider->OnGetCompValue)
     {
      if (ED->State != cbsGrayed)
       RC = Variant((int)ED->Checked);
      FCtrlOwner->DataProvider->OnGetCompValue(FNode, RC);
     }
    else
     {
      if (ED->State != cbsGrayed)
       RC = Variant((int)ED->Checked);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriBinaryTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled && (ED->State == cbsGrayed))
       RC = false;
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
