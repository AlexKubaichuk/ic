//---------------------------------------------------------------------------
//#include <vcl.h>
#pragma hdrstop

#include "dsRegTemplateData.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsRegTemplateData::TdsRegTemplateData()
 {
   FNoLock = false;
 }
//---------------------------------------------------------------------------
__fastcall TdsRegTemplateData::~TdsRegTemplateData()
 {
 }
//---------------------------------------------------------------------------
Variant __fastcall TdsRegTemplateData::FGetDataValue1(UnicodeString AId)
 {
  Variant RC = Variant::Empty();
  try
   {
    TdsRegTemplateDataMap::iterator FRC = FDataMap1.find(AId.UpperCase());
    if (FRC != FDataMap1.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateData::FSetDataValue1(UnicodeString AId, Variant AValue)
 {
  try
   {
    FDataMap1[AId.UpperCase()] = AValue;
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
Variant __fastcall TdsRegTemplateData::FGetDataValue2(UnicodeString AId)
 {
  Variant RC = Variant::Empty();
  try
   {
    TdsRegTemplateDataMap::iterator FRC = FDataMap2.find(AId.UpperCase());
    if (FRC != FDataMap2.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateData::FSetDataValue2(UnicodeString AId, Variant AValue)
 {
  try
   {
    FDataMap2[AId.UpperCase()] = AValue;
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateData::Copy(TdsRegTemplateData * AValue)
 {
  if (AValue)
   {
    for (TdsRegTemplateDataMap::iterator i = AValue->FDataMap1.begin(); i != AValue->FDataMap1.end(); i++)
     {
      FDataMap1[i->first] = i->second;
     }
    for (TdsRegTemplateDataMap::iterator i = AValue->FDataMap2.begin(); i != AValue->FDataMap2.end(); i++)
     {
      FDataMap2[i->first] = i->second;
     }
    FNoLock = AValue->NoLock;
  }
 }
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateData::Clear()
 {
  FDataMap1.clear();
  FDataMap2.clear();
 }
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateData::Erase(UnicodeString  AId)
 {
  if (FDataMap1.find(AId.UpperCase()) != FDataMap1.end())
   FDataMap1.erase(AId.UpperCase());
  if (FDataMap2.find(AId.UpperCase()) != FDataMap2.end())
   FDataMap2.erase(AId.UpperCase());
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplateData::ToString()
 {
  UnicodeString RC = "";
  try
   {
    Variant FRC;
    RC += " Value1: ";
    for (TdsRegTemplateDataMap::iterator i = FDataMap1.begin(); i != FDataMap1.end(); i++)
     {
      FRC = i->second;
      if (!(FRC.IsEmpty() || FRC.IsNull()))
       RC += " [" + i->first + "]=" + VarToStr(i->second) + ";";
      else
       RC += " [" + i->first + "]=null;";
     }
    RC += " Value2: ";
    for (TdsRegTemplateDataMap::iterator i = FDataMap2.begin(); i != FDataMap2.end(); i++)
     {
      FRC = i->second;
      if (!(FRC.IsEmpty() || FRC.IsNull()))
       RC += " [" + i->first + "]=" + VarToStr(i->second) + ";";
      else
       RC += " [" + i->first + "]=null;";
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplateData::ToJSONString(bool AFull)
 {
  UnicodeString RC = "";
  try
   {
    Variant FRC;
    RC += "{\"Value1\":{";
    bool empty = true;
    for (TdsRegTemplateDataMap::iterator i = FDataMap1.begin(); i != FDataMap1.end(); i++)
     {
      FRC = i->second;
      if (!(FRC.IsEmpty() || FRC.IsNull()))
       {
        if (!empty)
         RC += ",";
        RC += "\"" + i->first + "\":\"" + VarToStr(i->second) + "\"";
        empty = false;
       }
      else if (AFull)
       {
        if (!empty)
         RC += ",";
        RC += "\"" + i->first + "\":null";
        empty = false;
       }
     }
    RC += "},\"Value2\":{";
    empty = true;
    for (TdsRegTemplateDataMap::iterator i = FDataMap2.begin(); i != FDataMap2.end(); i++)
     {
      FRC = i->second;
      if (!(FRC.IsEmpty() || FRC.IsNull()))
       {
        if (!empty)
         RC += ",";
        RC += "\"" + i->first + "\":\"" + VarToStr(i->second) + "\"";
        empty = false;
       }
      else if (AFull)
       {
        if (!empty)
         RC += ",";
        RC += "\"" + i->first + "\":null";
        empty = false;
       }
     }
    RC += "}}";
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplateData::IsTemplate(UnicodeString  AId)
 {
  bool RC = false;
  try
   {
    Variant FRC = FGetDataValue1(AId);
    RC = !(FRC.IsEmpty() || FRC.IsNull());
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
