﻿//---------------------------------------------------------------------------

#ifndef dsRegTmplGroupH
#define dsRegTmplGroupH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <DB.hpp>
//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
#include "cxGroupBox.hpp"
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3

//---------------------------------------------------------------------------
#include "dsRegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriGroupTmpl: public TdsRegEDItem
{
private:
    void     __fastcall cResize(TObject *Sender);
    void     __fastcall FDataChange(TObject *Sender);
    TcxGroupBox* __fastcall FGetCastED();
    void __fastcall FSetCastED(TcxGroupBox *AVal);
    __property TcxGroupBox *ED = {read=FGetCastED,write=FSetCastED};
public:
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    void         __fastcall SetOnDependValues();
    void         __fastcall DataChange(TObject *Sender);
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString ACaption);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL, bool ASilent = false);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
__published:
    __fastcall TriGroupTmpl::TriGroupTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
    __fastcall ~TriGroupTmpl();
};
//---------------------------------------------------------------------------
#endif
