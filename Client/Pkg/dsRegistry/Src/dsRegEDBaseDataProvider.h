//---------------------------------------------------------------------------

#ifndef dsRegEDBaseDataProviderH
#define dsRegEDBaseDataProviderH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "dsRegTypeDef.h"
#include "XMLContainer.h"
#include "dsRegEDFunc.h"
//---------------------------------------------------------------------------
class PACKAGE TdsRegEDBaseDataProvider : public TObject
{
private:
protected:
    TGetCompValue      FGetCompValue;
    TGetClassXMLEvent  FOnGetClassXML;
public:
  __fastcall TdsRegEDBaseDataProvider();
  __fastcall ~TdsRegEDBaseDataProvider();

  virtual Variant       __fastcall GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption);
  virtual void          __fastcall SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue);

  virtual bool          __fastcall FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode = -1);
  virtual Variant       __fastcall GetChDBData(UnicodeString ARef, UnicodeString RecVal);
  virtual UnicodeString __fastcall GetTreeValue(UnicodeString ARef, UnicodeString ACode, UnicodeString &AExtCode);

  __property TGetCompValue     OnGetCompValue = {read=FGetCompValue,  write=FGetCompValue};
  __property TGetClassXMLEvent OnGetClassXML  = {read=FOnGetClassXML, write=FOnGetClassXML};
};
//---------------------------------------------------------------------------
#endif
