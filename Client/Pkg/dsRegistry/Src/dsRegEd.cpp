﻿//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "dsRegEd.h"
//#include "dsRegConstDef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TdsRegEDItem::TdsRegEDItem(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,
                                      TdsRegEDContainer *ACtrlOwner, bool IsApp)
: TPanel(AOwner)
{
  if (!ACtrlOwner)
   throw ERegEDError(FMT(icsRegEDErrorNoOwner));
  FRegEDItemInit(AOwner, AParent, ANode, ACtrlOwner);
  TmpCtrl = false;
  IsAppend = IsApp;
  isDelegate = FNode->CmpAV("isdelegate","yes");
  isEdit = FNode->AV["isedit"].ToIntDef(0) && !isDelegate;
  isFV = FNode->CmpAV("fixedvar","1");
  isEnabled = !(!isEdit && FNode->AV["default"].Length());
  isTemplate = FCtrlOwner->IsTemplate(_UID_);
  ParentBackground = true;
}
//---------------------------------------------------------------------------
__fastcall TdsRegEDItem::TdsRegEDItem(TComponent *AOwner, TWinControl *AParent, TTagNode* ANode,
                                  TdsRegEDContainer *ACtrlOwner)
:TPanel(AOwner)   // TcxGroupBox конструктор для Template-controls
{
  if (!ACtrlOwner)
   throw ERegEDError(FMT(icsRegEDErrorNoOwner));
  FRegEDItemInit(AOwner, AParent, ANode, ACtrlOwner);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::FRegEDItemInit(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner)
{
  FCtrlOwner = ACtrlOwner;
  FReqColor  = FCtrlOwner->ReqColor;
  FGetDataChange = NULL;
  TmpCtrl = true;
  IsAlign = false;
  isVal = false;
  IsAppend = false;
  SetEnList = new TList;
  FED2 = NULL;
  FED = NULL; FLab = NULL; FNode = ANode; //FNode->Ctrl = this;
  TRect xSize = Rect(0,0,120,21); //  default позиция и размер для "ED"
  Height = 22;  CMaxH = 22;  CMinH = 22;
  TwoLine = false; Parent = AParent; Alignment = taLeftJustify; Caption = "";
  BevelInner = bvNone; BevelOuter = bvNone;
  FRequired  = FNode->AV["required"].ToIntDef(0);
  isDelegate = false;
  isEdit = true;
  isFV = false;
  _UID_ = FNode->AV["uid"];
  isTemplate = false;
  OnResize = cResize;
  _ES_ = "";
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::FSetOnDependValues()
{
  if (FNode->GetChildByName("actuals"))
   {// прописать зависимости "Enabled"
     TTagNode *itActual = FNode->GetChildByName("actuals")->GetFirstChild();
     UnicodeString FDep,FDep2;
     while (itActual)
      {
        FDep2 = itActual->AV["depend"].UpperCase();
        bool FCanFill = true;
        if (FDep2.Length() >= 9)
         {
           FDep2 += ";";
           while (FDep2.Length())
            {
              FDep  = GetLPartB(FDep2,';').Trim();
              if (!FDep.Length())
               FDep = FDep2;
              if (FDep.Length() == 9 && EDCtrlExists(_GUI(FDep)))
               {
                 GetEDCtrl(_GUI(FDep))->SetOnEnable(FNode);
                 FCanFill = false;
               }
              FDep2 = GetRPartB(FDep2,';').Trim();
            }
         }
        if (FCanFill && (itActual->AV["ref"].Length() == 4) && EDCtrlExists(itActual->AV["ref"]))
         GetEDCtrl(itActual->AV["ref"])->SetOnEnable(FNode);
        itActual = itActual->GetNext();
      }
   }
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDItem::FGetExtCtrl(UnicodeString AUID)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   if (TmpCtrl)
    {
      if (FCtrlOwner)
       return FCtrlOwner->GetTemplateControl(AUID);
      else
       throw ERegEDTemplateError(FMT(icsRegEDErrorNoOwner));
    }
   else
    {
      if (FCtrlOwner)
       return FCtrlOwner->GetEDControl(AUID);
      else
       throw ERegEDError(FMT(icsRegEDErrorNoOwner));
    }
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDItem::labCtrl(UnicodeString AId)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  TdsRegEDItem *RC = NULL;
  try
   {
     TRegEDMap::iterator FRC = FLabList.find(AId);
     if (FRC != FLabList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDItem::depCtrl(UnicodeString AId)
{
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  TdsRegEDItem *RC = NULL;
  try
   {
     TRegEDMap::iterator FRC = FDependList.find(AId);
     if (FRC != FDependList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
__fastcall TdsRegEDItem::~TdsRegEDItem()
{
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  if (FLab) delete FLab;
//  if (SetValList) delete SetValList;
  FLabList.clear();
  FDependList.clear();
//  if (SetLabList) delete SetLabList;
  if (SetEnList) delete SetEnList;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::SetOnValDepend(TdsRegEDItem *AItem)
{
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  FDependList[AItem->DefNode->AV["uid"]] = AItem;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDItem::Condition(TTagNode *CondNode)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  bool RC;
  UnicodeString xCond  = CondNode->AV["condition"];
  Variant* pmList; pmList = new Variant[CondNode->Count];
  CalcParam(CondNode->GetFirstChild(),pmList);
  wchar_t *spCh;
  wchar_t *Src = new wchar_t[xCond.Length()+1];
  wcscpy(Src,xCond.c_str());
  spCh = wcschr(Src,' ');
  while (spCh) {spCh[0] = '\n'; spCh = wcschr(Src,' '); }
  TStringList  *ExpList = new TStringList;
  ExpList->Text = UnicodeString(Src);
  UnicodeString tmpOper;
  for (int i = 0; i < ExpList->Count; i++)
   {
     tmpOper = ExpList->Strings[i];
     if (tmpOper[1] == '@')
      ExpList->Strings[i] = VarToWideStrDef(pmList[tmpOper.SubString(2,tmpOper.Length()-1).ToInt()],"");
   }
  RC = (bool)CalcCondition(ExpList);
  if (Src) delete[] Src;
  Src = NULL;
  if (pmList)  delete[] pmList;
  pmList = NULL;
  if (ExpList) delete ExpList;
  ExpList = NULL;
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::CalcParam(TTagNode *AFirst,Variant* AParam)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   TTagNode *xNode = AFirst;
   Variant RC = NULL;
   Variant RC2 = NULL;
   int i = 0;
   UnicodeString xxRef;
   UnicodeString FDep,FDep2;

   while(xNode)
    {
      FDep2 = xNode->AV["depend"].UpperCase();
      bool FCanFill = true;
      if (FDep2.Length() >= 9)
       {
         FDep2 += ";";
         while (FDep2.Length())
          {
            FDep  = GetPart1(FDep2,';').Trim();
            if (!FDep.Length())
             FDep = FDep2;
            if (FDep.Length() == 9 && EDCtrlExists(_GUI(FDep)))
             {
               xxRef = _UID(FDep)+"."+ xNode->AV["ref"];
               RC = GetEDCtrl(_GUI(FDep))->GetCompValue(xxRef);
               FCanFill = false;

/*               if (!RC2.IsNull() && !RC2.IsEmpty() )
                if (UnicodeString(RC2).ToIntDef(-1) != -1)
                 RC = RC2;*/
             }
            FDep2 = GetRPartB(FDep2,';').Trim();
          }
       }
      if (FCanFill)
       {
         if ((xNode->AV["ref"].Length() == 4) && EDCtrlExists(xNode->AV["ref"]))
          RC = GetEDCtrl(xNode->AV["ref"])->GetCompValue("");
         else if (xNode->CmpAV("name", "null"))
          RC = "NULL";
         else
          ShowMessage(FMT(icsRegEDErrorNoRef));
       }
      AParam[i] = RC;
      i++;
      xNode = xNode->GetNext();
    }
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDItem::GetEDCtrl(UnicodeString AId)
{
//   ARef->AV["ref"]
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  TdsRegEDItem *RC = NULL;
  try
   {
     if (FCtrlOwner->GetNode(AId))
      RC = FGetExtCtrl(AId);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDItem::EDCtrlExists(UnicodeString AId)
{
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  bool RC = false;
  try
   {
     if (FCtrlOwner->GetNode(AId))
      if (FGetExtCtrl(AId))
       RC = true;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
/*
TdsRegEDItem* __fastcall TdsRegEDItem::RefCtrl(TTagNode* ARef)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  if (FCtrlOwner->GetNode(ARef->AV["ref"]))
   return FGetExtCtrl(ARef->AV["ref"]);
  else
   return NULL;
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDItem::RefMCtrl(UnicodeString ADepend)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  if (FCtrlOwner->GetNode(ADepend))
   return FGetExtCtrl(ADepend);
  else
   return NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDItem::isRefCtrl(TTagNode *ANode)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   if (FCtrlOwner->GetNode(AVUp(ANode,"ref")))
    if (FGetExtCtrl(AVUp(ANode,"ref")))
     return true;
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDItem::isRefMCtrl(UnicodeString ADepend)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   if (FCtrlOwner->GetNode(ADepend))
    if (FGetExtCtrl(ADepend))
     return true;
   return false;
}
*/
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::cResize(TObject *Sender)
{
}
//---------------------------------------------------------------------------
Variant      __fastcall TdsRegEDItem::GetCompValue(UnicodeString ARef)
{
  return Variant::Empty();
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::FDataChange(TObject *Sender)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetOnDependValues()
{
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::SetOnLab(TdsRegEDItem *AEDItem)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::DataChange(TObject *Sender)
{
}
//---------------------------------------------------------------------------
UnicodeString   __fastcall TdsRegEDItem::GetValueStr(bool ABinaryNames)
{
  return "";
}
//---------------------------------------------------------------------------
TStringList* __fastcall TdsRegEDItem::GetStrings(UnicodeString AClRef)
{
  return NULL;
}
//---------------------------------------------------------------------------
TControl*    __fastcall TdsRegEDItem::GetControl(int AIdx)
{
  return NULL;
}
//---------------------------------------------------------------------------
TControl*    __fastcall TdsRegEDItem::GetFirstEnabledControl()
{
  return NULL;
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetEnable(bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetEnableEx(bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetIsVal(bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetValue(UnicodeString AVal,bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetLabel(UnicodeString ACaption)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetOnEnable(TTagNode* ANode)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetEDLeft(int ALeft)
{
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::GetEDLeft(int *ALabR, int *AEDLeft)
{
}
//---------------------------------------------------------------------------
UnicodeString   __fastcall TdsRegEDItem::GetValue(UnicodeString ARef)
{
  return "";
}
//---------------------------------------------------------------------------
UnicodeString   __fastcall TdsRegEDItem::GetExtValue(UnicodeString ARef)
{
  return "";
}
//---------------------------------------------------------------------------
void         __fastcall TdsRegEDItem::SetFixedVar(UnicodeString AVals)
{
}
//---------------------------------------------------------------------------
bool         __fastcall TdsRegEDItem::UpdateChanges()
{
  return false;
}
//---------------------------------------------------------------------------
bool         __fastcall TdsRegEDItem::CheckReqValue(TcxPageControl *APC, bool ASilent)
{
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDItem::Clear()
{
}
//---------------------------------------------------------------------------

