//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsRegEDkabDSDataProvider.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsRegEDkabDSDataProvider::TdsRegEDkabDSDataProvider()
{
  FDataSrc = NULL;
}
//---------------------------------------------------------------------------
__fastcall TdsRegEDkabDSDataProvider::~TdsRegEDkabDSDataProvider()
{
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDkabDSDataProvider::FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode)
{
  bool RC = false;
  try
   {
     if (FDataSrc)
      {
        TJSONObject *RetData;
        TJSONObject *tmpPair;
        TJSONString *tmpData;
        int FClCode;
        UnicodeString FClStr;
        RetData = FDataSrc->DataSet->GetChDBData(ARef, ACode);
        TJSONPairEnumerator *itPair = RetData->GetEnumerator();
        AValues->Clear();
        while (itPair->MoveNext())
         {
           FClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(0);
           FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
           AValues->AddObject(FClStr, (TObject*)FClCode);
         }
        RC = true;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDkabDSDataProvider::GetChDBData(UnicodeString ARef, UnicodeString RecVal)
{
  Variant RC = Variant::Empty();
  try
   {
     if (FDataSrc)
      {
        TJSONObject *RetData;
        RetData = FDataSrc->DataSet->GetChDBData("f."+ARef, RecVal);
        RC =((TJSONString*)RetData->Pairs[0]->JsonValue)->Value();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDkabDSDataProvider::GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption)
{
  Variant RC = Variant::Empty();
  ACaption = "";
  try
   {
     if (FDataSrc)
      {
        TkabCustomDataSetRow *FCurRow = FDataSrc->DataSet->EditedRow;
        if (!FCurRow)
         {
           int recIdx = FDataSrc->CurrentRecIdx;
           if (recIdx >= 0)
             FCurRow = FDataSrc->DataSet->Row[recIdx];
         }
        if (FCurRow)
         {
           RC = FCurRow->Value[ADefNode->AV["uid"]];
           if (!(RC.IsEmpty() || RC.IsNull()))
            ACaption = VarToStr(RC);
           if (ADefNode->CmpName("choice,choiceDB,extedit"))
            ACaption = FCurRow->StrValue[ADefNode->AV["uid"]];
           else if (ADefNode->CmpName("choiceTree"))
            {
              ACaption = FCurRow->StrValue[ADefNode->AV["uid"]];
              if (FOnGetClassXML)
               {
                 TTagNode *FDef;
                 FOnGetClassXML(GetNode(ADefNode->AV["ref"])->AV["tblname"], FDef);
                 FDef = FDef->GetChildByAV("i","c",ACaption,true);
                 if (FDef)
                  {
                    ACaption = FDef->AV["e"]+" "+FDef->AV["n"];
                  }
               }
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDkabDSDataProvider::SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue)
{
  try
   {
     if (FDataSrc)
      {
        TkabCustomDataSetRow *FCurRow = FDataSrc->DataSet->EditedRow;
        if (!FCurRow)
         {
           int recIdx = FDataSrc->CurrentRecIdx;
           if (recIdx >= 0)
             FCurRow = FDataSrc->DataSet->Row[recIdx];
         }
        if (FCurRow)
         {
           FCurRow->Value[ADefNode->AV["uid"]] = AValue;
           if (ADefNode->CmpName("choice,choiceDB,choiceTree,extedit"))
            FCurRow->StrValue[ADefNode->AV["uid"]] = AStrValue;
         }
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsRegEDkabDSDataProvider::GetNode(UnicodeString ARef)
{
  if (FDefXML) return FDefXML->GetTagByUID(ARef);
  else         return NULL;
}
//---------------------------------------------------------------------------

