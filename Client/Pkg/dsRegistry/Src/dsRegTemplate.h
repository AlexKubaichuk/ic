//---------------------------------------------------------------------------

#ifndef dsRegTemplateH
#define dsRegTemplateH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Controls.hpp>
//#include <ExtCtrls.hpp>
//#include <Mask.hpp>
//#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsRegEDContainer.h"
#include <System.JSON.hpp>
#include "dsRegEd.h"
#include "dsRegEDTemplateDataProvider.h"
//---------------------------------------------------------------------------
class PACKAGE EdsRegTemplateError : public System::Sysutils::Exception
{
public:
     __fastcall EdsRegTemplateError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegTemplate : public TObject
{
private:
  TColor FReqColor;
  bool FIsTemplateField;
  TWinControl *FCompPanel;
  UnicodeString   FDataPath;
  bool FSilent;

  TdsRegEDContainer *FCtrList;
  TdsRegEDTemplateDataProvider *FDataProvider;
  TcxEditStyleController *FStyleController;

  TTagNode *FRegDef;
  TTagNode *FFltFl;
  TStringList *FTmplFieldList;
  TObjectList *FSepList;
  TKeyEvent FOnKeyDown;
  TdsRegTemplateData  *FCopyTemplateData;
        TTreeBtnClick FOnTreeBtnClick;
        TExtBtnClick  FOnExtBtnClick;
        TExtBtnClick  FGetExtWhere;

  int xTop, MaxW, FMaxHeight;

  void __fastcall FSetOnKeyDown(TKeyEvent AValue);
  Variant __fastcall FGetVarValue(UnicodeString AUID);
        bool __fastcall FPrepare();
        bool __fastcall CreateTmpl(TTagNode *itxTag);
        bool __fastcall FSetDefaultValue(TTagNode *itxTag);
        bool __fastcall FCheckInput(TTagNode *itxTag, UnicodeString& tmp);
        UnicodeString __fastcall FGetValue(UnicodeString AUID, bool ABinaryNames, UnicodeString ADefVal = "");
        bool __fastcall FGetSelectedTextValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FGetSelectedTextFieldValues(TTagNode *itxTag, UnicodeString& Values);
        int FLastTop,FLastHeight;
        int FHeight, FWidth;
        int FlCounter;
        bool MS,FNextCol;
        void __fastcall FSetWidth(int AWidth);
public:   // User declarations
  __fastcall TdsRegTemplate(TTagNode *ARegDef);
  __fastcall ~TdsRegTemplate();

  void __fastcall CreateTemplate(UnicodeString AFields, TdsRegTemplateData  *ATemplateData = NULL, int AFixHeight = 0);

  TdsRegTemplateData* __fastcall GetTemplateData();
    bool __fastcall CheckInput(bool ASilent = false);
    bool __fastcall IsEmpty();
    UnicodeString    __fastcall GetValue(UnicodeString AUID);
    UnicodeString    __fastcall GetName(UnicodeString AUID);
    UnicodeString    __fastcall GetSelectedTextValues(bool CanFieldName = false);
    TWinControl*  __fastcall GetFirstEnabledControl();
    void __fastcall Clear();
    TJSONObject*  __fastcall GetJSONValue();

  __property int Height = {read=FHeight};
  __property int Width = {read=FWidth, write=FSetWidth};

  __property TColor ReqColor           = {read=FReqColor, write=FReqColor};

  __property bool IsTemplateField = {read=FIsTemplateField, write=FIsTemplateField};
  __property TWinControl *TmplPanel = {read=FCompPanel, write=FCompPanel};

  __property TdsRegEDContainer *CtrList = {read=FCtrList};
  __property TdsRegEDTemplateDataProvider *DataProvider = {read=FDataProvider};

  __property TcxEditStyleController *StyleController = {read=FStyleController, write=FStyleController};

  __property TExtBtnClick OnExtBtnClick = {read=FOnExtBtnClick, write=FOnExtBtnClick};
  __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
  __property TKeyEvent OnKeyDown = {read=FOnKeyDown, write=FSetOnKeyDown};
  __property UnicodeString       DataPath = {read=FDataPath, write=FDataPath};
};
//---------------------------------------------------------------------------
extern PACKAGE UnicodeString __fastcall GetTemplateFields(TTagNode *ADescription);
#endif
