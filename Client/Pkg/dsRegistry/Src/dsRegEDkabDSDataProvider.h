//---------------------------------------------------------------------------

#ifndef dsRegEDkabDSDataProviderH
#define dsRegEDkabDSDataProviderH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "dsRegEDBaseDataProvider.h"
#include "KabCustomDS.h"

//#include "dsRegConstDef.h"
//#include "cxContainer.hpp"
//#include "dsRegTypeDef.h"
//#include "XMLContainer.h"
//#include "dsRegEDFunc.h"
//#include "dsRegTreeClass.h"
//---------------------------------------------------------------------------
class PACKAGE TdsRegEDkabDSDataProvider : public TdsRegEDBaseDataProvider
{
private:
  TkabCustomDataSource *FDataSrc;
  TTagNode           *FDefXML;
protected:
  TTagNode*  __fastcall GetNode(UnicodeString ARef);
public:
  __fastcall TdsRegEDkabDSDataProvider();
  __fastcall ~TdsRegEDkabDSDataProvider();

  Variant __fastcall GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption);
  void    __fastcall SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue);

  bool    __fastcall FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode = -1);
  Variant __fastcall GetChDBData(UnicodeString ARef, UnicodeString RecVal);

  __property TkabCustomDataSource* DataSource = {read=FDataSrc, write=FDataSrc};
  __property TTagNode*         DefXML         = {read=FDefXML,        write=FDefXML};
};
//---------------------------------------------------------------------------
#endif
