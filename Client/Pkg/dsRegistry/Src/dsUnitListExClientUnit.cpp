// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsUnitListExClientUnit.h"
#include "dsClassifEditClientUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsRegTemplateUnit.h"
#include "msgdef.h"
// #include "pkgsetting.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxImage"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxNavigator"
#pragma link "cxPC"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxGDIPlusClasses"
#pragma link "dxStatusBar"
#pragma link "dxBarExtItems"
#pragma link "cxProgressBar"
#pragma link "cxMemo"
#pragma link "cxRichEdit"
#pragma resource "*.dfm"
TdsUnitListExClientForm * dsUnitListExClientForm;
// ---------------------------------------------------------------------------
__fastcall TdsUnitListExClientForm::TdsUnitListExClientForm(TComponent * Owner, TdsRegDM * ADM) : TForm(Owner)
 {
  FTBImageList                          = NULL;
  FDM                                   = ADM;
  SearchBE->Style->StyleController      = FDM->StyleController;
  ProgressPanel->Style->StyleController = FDM->StyleController;
  SaveEnInsert                          = false;
  SaveEnEdit                            = false;
  SaveEnDelete                          = false;
  SaveEnSetTemplate                     = false;
  SaveEnReSetTemplate                   = false;
  SaveEnRefresh                         = false;
  SaveEnFind                            = false;
  SaveEnFindNext                        = false;
  SaveEnViewClassParam                  = false;
  SaveEnViewTemplateParam               = false;
  FDataSrc                              = NULL;
  FFilter                               = "";
  FSearchIdx                            = 0;
  FSearchModify                         = false;
  // FSearchAndFilter = false;
  FSearchList = new TStringList;
  FProgress   = NULL;
  FSearchCodes.clear();
  FFilterMode   = true;
  FindFilterSrc = false;
  if (FDM->OnExtCount)
   CountED->Visible = true;
  else
   CountED->Visible = false;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  RootClass = FDM->RegDef->GetChildByAV("classform", "group", "unit", true);
  if (RootClass)
   {
    RootClass = RootClass->GetFirstChild();
    UnitDesc  = RootClass->GetChildByName("description");
   }
  FCtrList = new TdsRegEDContainer(this, ExtListPC);
  FCtrList->DefXML       = RootClass;
  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  FLastTop                              = 0;
  FLastHeight                           = 0;
  Caption                               = SaveCaption;
  curCD                                 = 0;
  int AllItemsCount = 0;
  for (int i = 0; i < ClassTBM->Bars->Count; i++)
   {
    AllItemsCount += ClassTBM->Bars->Items[i]->ItemLinks->Count;
   }
  if (FDM->FullEdit)
   {
    // RootClass->Iterate(FSetEdit);
   }
  CheckCtrlVisible();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::FSetEdit(TTagNode * itTag)
 {
  if (itTag->GetAttrByName("isedit"))
   itTag->AV["isedit"] = "1";
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::SetActiveClass(UnicodeString AClsId)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ChangeSrc()
 {
  CheckCtrlState(false);
  try
   {
    try
     {
      Caption = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      FCtrList->ClearLabItems();
      ClassGridView->DataController->CustomDataSource = NULL;
      if (FDataSrc)
       delete FDataSrc;
      FDataSrc                          = new TkabCustomDataSource(_CLASSNODE_());
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      ClassGridView->BeginUpdate();
      try
       {
        FDataSrc->CreateColumns(ClassGridView);
        ClassGridView->DataController->CustomDataSource = FDataSrc;
        FDataSrc->DataChanged();
       }
      __finally
       {
        ClassGridView->EndUpdate();
       }
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
      xcTop = 1;
      int FPC = ExtListPC->PageCount - 1;
      for (int i = FPC; i >= 0; i--)
       {
        FCurPage              = ExtListPC->Pages[i];
        FCurPage->PageControl = NULL;
        delete FCurPage;
       }
      _CLASSNODE_()->Iterate(CreateListExt);
      StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
      // CorrectCount(FDataSrc->DataSet->RecordCount);
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsErrorMsgCaptionSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegistryErrorCommDBErrorMsgCaption), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    CheckCtrlState(true);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[2]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[2]->Text = AMsg;
  else
   StatusSB->Panels->Items[2]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_INSERT && actInsert->Enabled)
   actInsert->OnExecute(this);
  else if (Key == VK_RETURN)
   {
    if (actEdit->Enabled && Shift.Contains(ssShift))
     actEdit->OnExecute(this);
    else
     {
      if (SearchBE->IsFocused)
       {
        if (!FDataSrc->DataSet->RecordCount) // ���� ��� �������, �� ���������� ���������, ����� ����
         {
          actFindAndFilterExecute(actFindAndFilter);
          FFind(true);
         }
        else
         { // ���� �������� ����, �� ���������� ������ ���� ���� ������ ���������� ����������
          if (FFilterMode)
           {
            actFindAndFilterExecute(actFindAndFilter);
            FFind(true);
           }
          else
           actFindExecute(actFind);
         }
       }
     }
   }
  else if (Key == VK_DELETE && actDelete->Enabled)
   actDelete->OnExecute(this);
  else if (((Key == VK_UP) || (Key == VK_DOWN)) && FDataSrc->DataSet->RecordCount && SearchBE->IsFocused)
   {
    if (Key == VK_UP)
     ClassGridView->DataController->GotoPrev();
    else
     ClassGridView->DataController->GotoNext();
    if (FDataSrc->DataSet->CurRow)
     SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(FDataSrc->DataSet->CurRow->Value["CODE"]));
    else
     SetFocusedRec(0);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::CreateClassTV(TTagNode * itTag)
 {
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FormDestroy(TObject * Sender)
 {
  delete FSearchList;
  if (FProgress)
   delete FProgress;
  // ClearTemplate();
  if (FCtrList)
   {
    if (FCtrList->DataProvider)
     delete FCtrList->DataProvider;
    delete FCtrList;
   }
  FCtrList = NULL;
  // if (quClass()->Active) quClass()->Close();
  // if (quClass()->Transaction->Active) quClass()->Transaction->Rollback();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ListAfterScroll(TDataSet * DataSet)
 {
  FCtrList->LabDataChange();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::CreateListExt(TTagNode * itTag)
 {
  if (itTag->CmpName("group") && itTag->GetParent()->CmpName("class"))
   {
    // TcxTabSheet *Page;
    // ExtPanelList->Add((void*) new TPanel(this));
    // CurExtList = (TPanel*)ExtPanelList->Items[ExtPanelList->Count-1];
    FCurPage              = new TcxTabSheet(this);
    FCurPage->Caption     = itTag->AV["name"];
    FCurPage->PageControl = ExtListPC;
    FCurPage->BorderWidth = 3;
    FCurPage->ImageIndex  = FCurPage->TabIndex;
    xcTop                 = 0;
   }
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->CmpAV("inlist", "e"))
     {
      tmpItem      = FCtrList->AddLabItem(itTag, FCurPage->Width, FCurPage);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      // if (itTag->CmpName("extedit"))
      // tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actInsertExecute(TObject * Sender)
 {
  if (actInsert->Enabled)
   {
    TdsClassEditClientForm * Dlg = NULL;
    TTagNode * EditNode = NULL;
    bool CanRefresh, CanCancel, CanCont;
    try
     {
      CanRefresh = true;
      CanCancel  = true;
      EditNode   = new TTagNode(NULL);
      EditNode->Assign(_CLASSNODE_(), true);
      try
       {
        CanCont = true;
        bool CanReopen = false;
        if (FDM->OnExtInsertData)
         {
          __int64 FCode;
          FDM->OnExtInsertData(EditNode, FDataSrc, 0, CanCont, CanReopen, FCode);
          if (CanReopen)
           curCD = FCode;
         }
        if (CanCont)
         {
          Dlg = new TdsClassEditClientForm(this, true, EditNode, FDM, _CLASSNODE_()->GetRoot(), FDataSrc,
            FCtrList->TemplateData, FDM->SingleEditCol);
          if (Dlg)
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
         }
        else
         {
          CanRefresh = CanReopen;
          CanCancel  = false;
         }
       }
      catch (Exception & E)
       {
        _MSG_ERR(FMT1(icsRegErrorInsertRecSys, E.Message), FMT(icsErrorMsgCaption));
       }
      catch (...)
       {
        _MSG_ERR(FMT(icsRegErrorInsertRec), FMT(icsErrorMsgCaption));
       }
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
      if (EditNode)
       delete EditNode;
      if (CanCancel)
       {
        // if (quClass()->State != dsBrowse)
        FDataSrc->DataSet->Cancel();
       }
      if (CanRefresh)
       {
        if (FDM->OnClassAfterInsert)
         FDM->OnClassAfterInsert(_CLASSNODE_());
        RefreshById(IntToStr(curCD));
       }
     }
    SearchBE->Clear();
    FFilterMode = true;
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actEditExecute(TObject * Sender)
 {
  if (actEdit->Enabled && IsRecSelected())
   {
    TTagNode * EditNode = NULL;
    TdsClassEditClientForm * Dlg = NULL;
    bool CanRefresh, CanCancel, CanCont;
    try
     {
      CanRefresh = true;
      CanCancel  = true;
      EditNode   = new TTagNode(NULL);
      EditNode->Assign(_CLASSNODE_(), true);
      try
       {
        CanCont = true;
        bool CanReopen = false;
        if (FDM->OnExtEditData)
         {
          __int64 FCode;
          FDM->OnExtEditData(EditNode, FDataSrc, FDataSrc->DataSet->CurRow->Value["CODE"], CanCont, CanReopen, FCode);
          if (CanReopen)
           curCD = FCode;
         }
        if (CanCont)
         {
          Dlg = new TdsClassEditClientForm(this, false, EditNode, FDM, _CLASSNODE_()->GetRoot(), FDataSrc,
            FCtrList->TemplateData, FDM->SingleEditCol);
          if (Dlg)
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
         }
        else
         {
          CanRefresh = CanReopen;
          CanCancel  = false;
         }
       }
      catch (Exception & E)
       {
        _MSG_ERR(FMT1(icsRegErrorEditRecSys, E.Message), FMT(icsErrorMsgCaption));
       }
      catch (...)
       {
        _MSG_ERR(FMT(icsRegErrorEditRec), FMT(icsErrorMsgCaption));
       }
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
      if (EditNode)
       delete EditNode;
      if (CanCancel)
       {
        FDataSrc->DataSet->Cancel();
       }
      if (CanRefresh)
       {
        if (FDM->OnClassAfterEdit)
         FDM->OnClassAfterEdit(_CLASSNODE_());
        RefreshById(IntToStr(curCD));
       }
     }
    SearchBE->Clear();
    FFilterMode = true;
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actDeleteExecute(TObject * Sender)
 {
  if (actDelete->Enabled && IsRecSelected())
   {
    bool CanRefresh, CanCancel, CanCont;
    try
     {
      CanRefresh = true;
      CanCancel  = true;
      try
       {
        CanCont = true;
        if (CanCont)
         {
          int FRecIdx = FDataSrc->CurrentRecIdx;
          UnicodeString Str = "";
          UnicodeString FFlId;
          if (FRecIdx >= 0)
           {
            if (UnitDesc->AV["confirmlabel"].Trim().Length())
             { // ���� confirmlabel;
              TStringList * FConfMsgList = new TStringList;
              try
               {
                FConfMsgList->Delimiter     = '#';
                FConfMsgList->QuoteChar     = '~';
                FConfMsgList->DelimitedText = UnitDesc->AV["confirmlabel"].Trim();
                for (int i = 0; i < FConfMsgList->Count; i++)
                 {
                  if (FConfMsgList->Strings[i].Length())
                   {
                    if (FConfMsgList->Strings[i][1] == '@')
                     {
                      FFlId = FConfMsgList->Strings[i].SubString(2, 4);
                      Str += " " + FDataSrc->DataSet->CurRow->StrValue[FFlId];
                     }
                    else
                     {
                      Str += FConfMsgList->Strings[i];
                     }
                   }
                 }
               }
              __finally
               {
                delete FConfMsgList;
               }
             }
            else
             {
              TTagNode * itFl = FDataSrc->DataSet->ListDefNode->GetFirstChild();
              while (itFl)
               {
                if (Str.Length())
                 Str += ", ";
                Str += FDataSrc->DataSet->CurRow->StrValue[itFl->AV["uid"]];
                itFl = itFl->GetNext();
               }
              Str = FMT(icsRegErrorConfirmDeleteRec) + " " + UnitDesc->AV["deletelabel"].LowerCase() + "\n\n\" " +
                Str + "\"\n";
             }
           }
          if (_MSG_QUE(Str, FMT(icsRegErrorConfirmDeleteRecMsgCaption)) == IDYES)
           {
            FDataSrc->DataSet->Delete();
            CanRefresh = true;
            CanCancel  = false;
           }
          else
           {
            CanRefresh = false;
            CanCancel  = false;
           }
         }
        else
         {
          CanRefresh = false;
          CanCancel  = false;
         }
       }
      catch (Exception & E)
       {
        _MSG_ERR(FMT1(icsRegErrorDeleteRecSys, E.Message), FMT(icsErrorMsgCaption));
       }
      catch (...)
       {
        _MSG_ERR(FMT(icsRegErrorDeleteRec), FMT(icsErrorMsgCaption));
       }
     }
    __finally
     {
      if (CanCancel)
       {
        FDataSrc->DataSet->Cancel();
       }
      if (CanRefresh)
       {
        if (FDM->OnClassAfterDelete)
         FDM->OnClassAfterDelete(_CLASSNODE_());
        actRefreshExecute(actRefresh);
       }
     }
    SearchBE->Clear();
    FFilterMode = true;
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::SetFiltered(bool AVal)
 {
  // SetTmpl->Visible = AVal;
  // ResetTmpl->Visible = !AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FSetFilterExecute(UnicodeString AFilterGUI)
 {
  if (!actSetFilter->Checked)
   {
    if (FDM->OnExtFilterSet)
     {
      bool FCanSet = false;
      UnicodeString FTabSel = "";
      if (AFilterGUI.Length())
       FFilter = FDM->OnExtFilterSet(RootClass, FCanSet, AFilterGUI);
      else
       FFilter = FDM->OnExtFilterSet(RootClass, FCanSet, FFilter);
      if (FCanSet)
       {
        actSetFilter->Checked = true;
        SetFiltered(true);
        actRefreshExecute(actRefresh);
       }
     }
   }
  else
   {
    actSetFilter->Checked = false;
    SetFiltered(false);
    actRefreshExecute(actRefresh);
   }
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actSetFilterExecute(TObject * Sender)
 {
  FSetFilterExecute();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actReSetFilterExecute(TObject * Sender)
 {
  SetFiltered(false);
  FFilter               = "";
  actSetFilter->Checked = false;
  if (FDM->OnExtFilterSet)
   {
    bool tmp = false;
    FDM->OnExtFilterSet(RootClass, tmp, "clear");
   }
  actRefreshExecute(actRefresh);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FGetData()
 {
  // TdsRegListType { ltFull = 0, ltFilter = 1, ltSearchFilter = 2};
  if (actSetFilter->Checked)
   {
    if (!FFilter.Trim().Length())
     ClearData();
    else
     FDataSrc->DataSet->GetData(FFilter, FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
   }
  else
   {
    if (FDM->UnitListOpenType == TdsRegListType::ltFull)
     FDataSrc->DataSet->GetData("", FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
    else
     ClearData();
   }
  FindFilterSrc = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ClearData()
 {
  FDataSrc->DataSet->Clear();
  StatusSB->Panels->Items[1]->Text = "";
  StatusSB->Panels->Items[2]->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actRefreshExecute(TObject * Sender)
 {
  CheckCtrlState(false);
  ClassGridView->BeginUpdate();
  try
   {
    // if (!FindFilterSrc)
    FGetData();
    FDataSrc->DataChanged();
    GetCommCount();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    CheckCtrlState(true);
    ClassGridView->EndUpdate();
   }
  SearchBE->Clear();
  FFilterMode = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::RefreshById(UnicodeString ACode)
 {
  CheckCtrlState(false);
  ClassGridView->BeginUpdate();
  try
   {
    FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
    FDataSrc->DataChanged();
    GetCommCount();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    ClassGridView->EndUpdate();
    CheckCtrlState(true);
    SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(ACode));
   }
 }
// ---------------------------------------------------------------------------
TcxGridColumn * __fastcall TdsUnitListExClientForm::ClassCol(int AIdx)
 {
  return ClassGridView->Columns[AIdx];
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsUnitListExClientForm::_CLASSNODE_()
 {
  return RootClass;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::GetBtnEn(UnicodeString AAttrName)
 {
  return ((bool)(_CLASSNODE_()->GetChildByName("description")->AV[AAttrName].Length()));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actTemplateExecute(TObject * Sender)
 {
  if (!actTemplate->Checked)
   {
    UnicodeString tmpl = GetTemplateFields(UnitDesc);
    if (tmpl.Length())
     {
      TdsRegTemplateForm * TmpDlg = NULL;
      try
       {
        TmpDlg = new TdsRegTemplateForm(this, FDM, tmpl, FCtrList->TemplateData);
        if (TmpDlg)
         {
          TmpDlg->ShowModal();
          if (TmpDlg->ModalResult == mrOk)
           {
            if (!TmpDlg->IsEmpty())
             {
              FCtrList->TemplateData = TmpDlg->TemplateData;
              actTemplate->Checked   = true;
              FCtrList->LabDataChange();
             }
           }
         }
       }
      __finally
       {
        if (TmpDlg)
         delete TmpDlg;
       }
     }
   }
  else
   {
    actTemplate->Checked = false;
    try
     {
      FCtrList->TemplateData->Clear();
      FCtrList->LabDataChange();
     }
    __finally
     {
     }
   }
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FormShow(TObject * Sender)
 {
  ShowTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ClassGridViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
  TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value["CODE"]), FGetDataProgress);
  FCtrList->LabDataChange();
  actEdit->Enabled        = IsRecSelected() && FDM->UnitListEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
  actDelete->Enabled      = IsRecSelected() && FDM->UnitListEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
  actOpen->Enabled        = IsRecSelected() && FDM->OpenCardEnabled && actOpen->Visible;
  actShowPrePlan->Enabled = IsRecSelected() && FDM->OpenCardEnabled && actShowPrePlan->Visible;
  actImport->Enabled      = FDM->UnitListEditable;
  actExport->Enabled      = IsRecSelected() && FDM->UnitListEditable;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::IsRecSelected()
 {
  return ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actOpenExecute(TObject * Sender)
 {
  if (actOpen->Enabled && FDM->OnOpenCard && IsRecSelected())
   FDM->OnOpenCard((__int64)FDataSrc->DataSet->CurRow->Value["CODE"], GetUnitStr());
  SearchBE->Clear();
  FFilterMode = true;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsUnitListExClientForm::GetUnitStr()
 {
  TTagNode * itNode = RootClass->GetChildByName("fields", true)->GetFirstChild();
  UnicodeString RC = "";
  if (IsRecSelected())
   {
    while (itNode)
     {
      RC += VarToWideStrDef(FDataSrc->DataSet->CurRow->Value[itNode->AV["ref"]], "") + " ";
      itNode = itNode->GetNext();
     }
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FGetQuickFilterList(bool ASet)
 {
  for (int i = QuickFilterMenu->ItemLinks->Count - 1; i > 0; i--)
   delete(TdxBarButton *)QuickFilterMenu->ItemLinks->Items[i]->Item;
  if (FDM->OnExtFilterSet && FDM->OnGetQuickFilterList)
   {
    TStringList * FQFList = new TStringList;
    try
     {
      if (ASet)
       FQFList->Add("set");
      else
       FQFList->Add("add");
      FDM->OnGetQuickFilterList("filter:������ �������� ��� ������������", FQFList);
      if (!FDM->UseQuickFilterSetting)
       {
        actFilterSetting->Enabled = false;
        actFilterSetting->Visible = false;
       }
      UnicodeString FCaption, FGUI;
      FQuickFilterList.clear();
      TdxBarButton * ExtBtn;
      for (int i = 0; i < FQFList->Count; i++)
       {
        FCaption = GetRPartB(FQFList->Strings[i], '=');
        FGUI     = GetLPartB(FQFList->Strings[i], '=');
        ExtBtn   = new TdxBarButton(NULL);
        QuickFilterMenu->ItemLinks->Add()->Item = ExtBtn;
        ExtBtn->Caption               = FCaption;
        ExtBtn->ImageIndex            = -1;
        ExtBtn->Tag                   = i + 1;
        FQuickFilterList[ExtBtn->Tag] = FGUI;
        ExtBtn->OnClick               = QuickFilterItemClick;
       }
     }
    __finally
     {
      delete FQFList;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::QuickFilterItemClick(TObject * Sender)
 {
  UnicodeString FQFGUI;
  if (FDM->OnExtFilterSet)
   {
    FQFGUI = FQuickFilterList[((TdxBarButton *)Sender)->Tag];
    if (FQFGUI.Trim().Length())
     {
      FSetFilterExecute(FQFGUI);
     }
   }
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actFilterSettingExecute(TObject * Sender)
 {
  if (FDM->UseQuickFilterSetting)
   FGetQuickFilterList(true);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FGetReportList(bool ASet)
 {
  for (int i = ReportMenu->ItemLinks->Count - 1; i >= 0; i--)
   delete(TdxBarButton *)ReportMenu->ItemLinks->Items[i]->Item;
  if (FDM->OnPrintReportClick && FDM->OnGetReportList)
   {
    TStringList * FRepList = new TStringList;
    try
     {
      if (ASet)
       FRepList->Add("set");
      else
       FRepList->Add("add");
      FDM->OnGetReportList("report:������ ���������� ��� ������������", FRepList);
      if (!FDM->UseReportSetting)
       {
        actPrintListSetting->Enabled = false;
        actPrintListSetting->Visible = false;
        ReportMenu->ItemLinks->Clear();
       }
      UnicodeString FCaption, FGUI;
      FReportList.clear();
      TdxBarButton * ExtBtn;
      if ((FRepList->Count == 1) && !FDM->UseReportSetting)
       PrintBI->ButtonStyle = bsDefault;
      for (int i = 0; i < FRepList->Count; i++)
       {
        FCaption = GetRPartB(FRepList->Strings[i], '=');
        FGUI     = GetLPartB(FRepList->Strings[i], '=');
        ExtBtn   = new TdxBarButton(NULL);
        ReportMenu->ItemLinks->Add()->Item = ExtBtn;
        ExtBtn->Caption          = FCaption;
        ExtBtn->ImageIndex       = -1;
        ExtBtn->Tag              = i + 1;
        FReportList[ExtBtn->Tag] = FGUI;
        ExtBtn->OnClick          = PrintReportItemClick;
        ExtBtn->Hint             = FCaption;
       }
     }
    __finally
     {
      delete FRepList;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::PrintReportItemClick(TObject * Sender)
 {
  CheckCtrlState(false);
  try
   {
    UnicodeString FRepGUI;
    if (FDM->OnPrintReportClick)
     {
      FRepGUI = FReportList[((TdxBarButton *)Sender)->Tag];
      if (FRepGUI.Trim().Length())
       {
        if (IsRecSelected())
         FDM->OnPrintReportClick(FRepGUI, (__int64)FDataSrc->DataSet->CurRow->Value["CODE"], FFilter, CheckProgress);
        else
         FDM->OnPrintReportClick(FRepGUI, -1, FFilter, CheckProgress);
       }
     }
   }
  __finally
   {
    CheckCtrlState(true);
   }
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  Application->ProcessMessages();
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      SetFocus();
      PrBar->Properties->Max           = AVal;
      StatusSB->Panels->Items[2]->Text = AMsg;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      StatusSB->Panels->Items[2]->Text = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      Caption                          = SaveCaption + " {" + FloatToStrF(AVal / 10.0, ffFixed, 2, 1) + "%}";
      StatusSB->Panels->Items[2]->Text = AMsg;
      PrBar->Position                  = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      StatusSB->Panels->Items[2]->Text = "";
      Caption                          = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      break;
     }
   }
  Application->ProcessMessages();
  return InProgress;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actPrintListSettingExecute(TObject * Sender)
 {
  if (FDM->UseReportSetting)
   FGetReportList(true);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  if (FTBImageList)
   {
    ClassAL->Images                     = FTBImageList;
    ClassTBM->ImageOptions->LargeImages = FTBImageList;
    ReportMenu->Images                  = FTBImageList;
    QuickFilterMenu->Images             = FTBImageList;
    ServiceMenu->Images                 = FTBImageList;
    PMenu->Images                       = FTBImageList;
    InsertMenu->Images                  = FTBImageList;
    EditMenu->Images                    = FTBImageList;
   }
  Application->ProcessMessages();
  ChangeSrc();
  if (FDM->UnitListOpenType == TdsRegListType::ltFull)
   actRefreshExecute(actRefresh);
  else if (FDM->UnitListOpenType == TdsRegListType::ltFilter)
   FSetFilterExecute();
  else
   GetCommCount();
  Application->ProcessMessages();
  FGetReportList(false);
  FGetQuickFilterList(false);
  Application->ProcessMessages();
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::CheckCtrlState(bool AEnable)
 {
  InProgress = !AEnable;
  if (AEnable)
   {
    // if (FProgress) delete FProgress;
    // FProgress = NULL;
    PrTimer->Enabled       = false;
    PrBar->Visible         = false;
    ProgressPanel->Visible = false;
    ProgressPanel->Width   = 0;
    ProgressPanel->Height  = 0;
   }
  else
   {
    PrTimer->Enabled       = true;
    PrBar->Visible         = true;
    ProgressPanel->Left    = 0;
    ProgressPanel->Top     = 0;
    ProgressPanel->Width   = ClientWidth;
    ProgressPanel->Height  = ClientHeight - StatusSB->Height - 5;
    ProgressPanel->Visible = true;
    ProgressPanel->BringToFront();
   }
  Application->ProcessMessages();
  actInsert->Enabled = AEnable && FDM->UnitListEditable && (FDM->FullEdit | GetBtnEn("insertlabel"));
  actEdit->Enabled   = false;
  actDelete->Enabled = false;
  actImport->Enabled = false;
  actExport->Enabled = false;
  if (AEnable && ClassGridView->DataController->CustomDataSource && FDataSrc)
   {
    if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
     {
      actEdit->Enabled   = AEnable && FDM->UnitListEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
      actDelete->Enabled = AEnable && FDM->UnitListEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
      actImport->Enabled = AEnable && FDM->UnitListEditable;
      actExport->Enabled = AEnable && FDM->UnitListEditable;
     }
   }
  Application->ProcessMessages();
  actRefresh->Enabled          = AEnable;
  actTemplate->Enabled         = AEnable && actTemplate->Visible && actInsert->Enabled;
  actSetFilter->Enabled        = AEnable && actSetFilter->Visible;
  actReSetFilter->Enabled      = AEnable && actReSetFilter->Visible;
  actFilterSetting->Enabled    = AEnable && actFilterSetting->Visible;
  actOpen->Enabled             = AEnable && FDM->OpenCardEnabled && actOpen->Visible;
  actExport->Enabled           = AEnable && actExport->Visible;
  actSelAll->Enabled           = AEnable && actSelAll->Visible;
  actAddImmAct->Enabled        = AEnable && actAddImmAct->Visible;
  actShowPrePlan->Enabled      = AEnable && FDM->OpenCardEnabled && actShowPrePlan->Visible;
  actPrintF63->Enabled         = AEnable && actPrintF63->Visible;
  actInsPriv->Enabled          = AEnable && FDM->OpenCardEnabled && actInsPriv->Visible;
  actInsTest->Enabled          = AEnable && FDM->OpenCardEnabled && actInsTest->Visible;
  actEditLastPriv->Enabled     = AEnable && actEditLastPriv->Visible;
  actEditLastTest->Enabled     = AEnable && actEditLastTest->Visible;
  PrintBI->Enabled             = AEnable;
  actPrintListSetting->Enabled = AEnable && actPrintListSetting->Visible;
  SearchBE->Enabled            = AEnable;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::CheckCtrlVisible()
 {
  actOpen->Visible         = FDM->OnOpenCard;
  actShowPrePlan->Visible  = FDM->OnOpenCard;
  actPrintF63->Visible     = FDM->OnOpenCard;
  actInsPriv->Visible      = FDM->OnInsPTCData;
  actInsTest->Visible      = FDM->OnInsPTCData;
  actEditLastPriv->Visible = FDM->OnEditPTCData;
  actEditLastTest->Visible = FDM->OnEditPTCData;
  if (FDM->OnInsPTCData)
   InsertBI->ButtonStyle = bsDropDown;
  if (FDM->OnEditPTCData)
   EditBI->ButtonStyle = bsDropDown;
  actSelAll->Visible           = false;
  actFilterSetting->Visible    = FDM->OnGetQuickFilterList;
  actPrintListSetting->Visible = FDM->OnGetReportList && FDM->OnPrintReportClick;
  if (!actPrintListSetting->Visible)
   PrintBI->ButtonStyle = bsDefault;
  actShowPrePlan->Visible = FDM->OnShowPrePlan;
  actPrintF63->Visible    = FDM->OnPrintF63;
  if (!_CLASSNODE_()->GetChildByName("description")->GetChildByName("maskfields"))
   {
    actTemplate->Visible    = false;
    actSetFilter->Visible   = false;
    actReSetFilter->Visible = false;
   }
  actImport->Visible = FDM->OnImportData;
  actExport->Visible = FDM->OnExportData;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::GetSelectedUnitData(TkabCustomDataSetRow *& AData)
 {
  AData = NULL;
  if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
   {
    AData = new TkabCustomDataSetRow(FDataSrc->DataSet->DefNode, FDataSrc->DataSet->CurRow);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actFindExecute(TObject * Sender)
 {
  FFind(false);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FFind(bool ASilent)
 {
  if (SearchBE->Text.Trim().Length() && (SearchBE->Text.Trim().UpperCase() != "�����"))
   {
    CheckCtrlState(false);
    ClassGridView->BeginUpdate();
    try
     {
      if (FSearchModify)
       {
        FSearchCodes.clear();
        TJSONObject * FRC;
        UnicodeString tmpValue;
        if (FDM->OnFind("reg.1000", "findbyfield:" + SearchBE->Text.Trim(), FRC))
         {
          for (int i = 0; i < ((TJSONArray *)FRC)->Count; i++)
           {
            tmpValue = ((TJSONString *)((TJSONArray *)FRC)->Items[i])->Value();
            if (FDataSrc->DataSet->GetRowIdxByCode(tmpValue) != -1)
             FSearchCodes.push_back(tmpValue);
           }
         }
        FSearchModify = false;
       }
      if (FSearchCodes.size())
       {
        SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(FSearchCodes.front()));
        FSearchCodes.push_back(FSearchCodes.front());
        FSearchCodes.pop_front();
       }
      else if (!ASilent)
       _MSG_INF("����� ���������, �� �� ������ ����� :(", "���������");
     }
    __finally
     {
      ClassGridView->EndUpdate();
      CheckCtrlState(true);
     }
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actFindAndFilterExecute(TObject * Sender)
 {
  if (FDM->OnFind && SearchBE->Text.Trim().Length() /* && !FSearchAndFilter */ &&
    (SearchBE->Text.Trim().UpperCase() != "�����"))
   {
    CheckCtrlState(false);
    ClassGridView->BeginUpdate();
    try
     {
      actSetFilter->Checked = false;
      SetFiltered(false);
      TJSONObject * FRC;
      if (FDM->OnFind("reg.1000", SearchBE->Text.Trim(), FRC))
       {
        FDataSrc->DataSet->FilterByFind((TJSONArray *)FRC, FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
        FindFilterSrc = true;
        FDataSrc->DataChanged();
        StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
        GetCommCount();
       }
     }
    __finally
     {
      CheckCtrlState(true);
      ClassGridView->EndUpdate();
      if (FDataSrc->DataSet->RecordCount)
       SetFocusedRec(0);
     }
    SetActiveCtrl();
    FFilterMode = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actClearSearchFieldExecute(TObject * Sender)
 {
  if (SearchBE->Text.Trim().Length())
   {
    SearchBE->Clear();
    FFilterMode = true;
    actRefreshExecute(actRefresh);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::SearchBEPropertiesChange(TObject * Sender)
 {
  FSearchIdx    = 0;
  FSearchModify = true;
  // FSearchAndFilter = false;
  if (!SearchBE->Text.Trim().Length())
   FFilterMode = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ClassGridViewColumnHeaderClick(TcxGridTableView * Sender,
  TcxGridColumn * AColumn)
 {
  // FFetchAllRec();//FDataSrc->DataSet->FetchAll(FDM->FetchRecCount, FGetDataProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ClassGridViewDataControllerFilterChanged(TObject * Sender)
 {
  // FFetchAllRec(); //FDataSrc->DataSet->FetchAll(FDM->FetchRecCount, FGetDataProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FFetchAllRec()
 {
  if (!FDataSrc->DataSet->IsAllFetched())
   {
    CheckCtrlState(false);
    try
     {
      FDataSrc->DataSet->FetchAll(FDM->FetchRecCount, FGetDataProgress);
     }
    __finally
     {
      CheckCtrlState(true);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actShowPrePlanExecute(TObject * Sender)
 {
  if (actShowPrePlan->Enabled && FDM->OnShowPrePlan && IsRecSelected())
   FDM->OnShowPrePlan((__int64)FDataSrc->DataSet->CurRow->Value["CODE"], GetUnitStr());
  SearchBE->Clear();
  FFilterMode = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actPrintF63Execute(TObject * Sender)
 {
  if (actPrintF63->Enabled && FDM->OnPrintF63)
   {
    if (IsRecSelected())
     FDM->OnPrintF63((__int64)FDataSrc->DataSet->CurRow->Value["CODE"], "setting");
    else
     _MSG_ERR("���������� ������� ��������.", "������");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::ServiceBIClick(TObject * Sender)
 {
  ServiceBI->DropDown();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::FormActivate(TObject * Sender)
 {
  if (SearchBE->Enabled)
   {
    SearchBE->Clear();
    FFilterMode = true;
    SearchBE->SetFocus();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actImportExecute(TObject * Sender)
 {
  if (actImport->Enabled && FDM->OnImportData)
   {
    UnicodeString FLastRec = "";
    CheckCtrlState(false);
    try
     {
      FDM->OnImportData(_CLASSNODE_(), FLastRec, CheckProgress);
     }
    __finally
     {
      CheckCtrlState(true);
      actRefreshExecute(actRefresh);
      if (FLastRec.Length())
       SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(FLastRec));
     }
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actExportExecute(TObject * Sender)
 {
  int SelCount = ClassGridView->DataController->GetSelectedCount();
  if (actExport->Enabled && SelCount && FDM->OnExportData)
   {
    CheckCtrlState(false);
    if (FDM->OnExportData(TkabProgressType::InitStage, NULL, FDataSrc, SelCount, CheckProgress))
     {
      Application->ProcessMessages();
      try
       {
        int RowIdx;
        for (int i = 0; i < SelCount; i++)
         {
          Application->ProcessMessages();
          RowIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
          Application->ProcessMessages();
          FDM->OnExportData(TkabProgressType::StageInc, _CLASSNODE_(), FDataSrc,
            FDataSrc->DataSet->Row[RowIdx]->Value["CODE"], CheckProgress);
          Application->ProcessMessages();
          // if (ClassGridView->DataController->GetSelectedCount() > 1)
          // ClassGridView->ViewData->Records[RowIdx]->Selected = false;
         }
        Application->ProcessMessages();
        FDM->OnExportData(TkabProgressType::StageComplite, _CLASSNODE_(), FDataSrc, 0, CheckProgress);
        Application->ProcessMessages();
       }
      __finally
       {
        CheckCtrlState(true);
        Application->ProcessMessages();
       }
      SetActiveCtrl();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::PrTimerTimer(TObject * Sender)
 {
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::SetInProgress(bool AVal)
 {
  // ApplyBtn->Caption = (AVal)? "��������":"��������� � ��";
  FInProgress = AVal;
  // PrBar->Visible = AVal;
  // if (AVal)  PrBar->Position = 0;
  // else       StatusBar->Panels->Items[1]->Text = "";
  // SetEnabled(!AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsUnitListExClientForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (_MSG_QUE("��������?", "���������") == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::SetActiveCtrl()
 {
  if (SearchBE->Enabled)
   SearchBE->SetFocus();
  else
   ClassGrid->SetFocus();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::SetFocusedRec(__int64 ARowIdx)
 {
  ClassGridView->Controller->ClearSelection();
  ClassGridView->DataController->FocusedRecordIndex = ARowIdx;
  if (ClassGridView->Controller->FocusedRecord)
   {
    ClassGridView->Controller->FocusedRecord->Selected = true;
    ClassGridView->Controller->FocusedRecord->MakeVisible();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actAddImmActExecute(TObject * Sender)
 {
  int SelCount = ClassGridView->DataController->GetSelectedCount();
  if (actAddImmAct->Enabled && SelCount && FDM->OnInsPriv)
   {
    CheckCtrlState(false);
    if (FDM->OnInsPriv(TkabProgressType::InitStage, NULL, FDataSrc, SelCount, CheckProgress))
     {
      Application->ProcessMessages();
      try
       {
        int RowIdx;
        for (int i = 0; i < SelCount; i++)
         {
          Application->ProcessMessages();
          RowIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
          Application->ProcessMessages();
          FDM->OnInsPriv(TkabProgressType::StageInc, _CLASSNODE_(), FDataSrc,
            FDataSrc->DataSet->Row[RowIdx]->Value["CODE"], CheckProgress);
          Application->ProcessMessages();
         }
        Application->ProcessMessages();
        FDM->OnInsPriv(TkabProgressType::StageComplite, _CLASSNODE_(), FDataSrc, 0, CheckProgress);
        Application->ProcessMessages();
       }
      __finally
       {
        CheckCtrlState(true);
        Application->ProcessMessages();
       }
      ClassGridView->Controller->ClearSelection();
      SetActiveCtrl();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::GetCommCount()
 {
  if (FDM->OnExtCount)
   {
    UnicodeString RC = "";
    FDM->OnExtCount(RC);
    CountED->Lines->Text                  = RC;
    CountED->Properties->VisibleLineCount = CountED->Lines->Count + 1;
   }
  else
   {
    CountED->Lines->Text                  = "";
    CountED->Properties->VisibleLineCount = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actPrintFullF63Execute(TObject * Sender)
 {
  if (actPrintF63->Enabled && FDM->OnPrintF63)
   {
    if (IsRecSelected())
     FDM->OnPrintF63((__int64)FDataSrc->DataSet->CurRow->Value["CODE"], "all");
    else
     _MSG_ERR("���������� ������� ��������.", "������");
   }
 }
// ---------------------------------------------------------------------------
/*
 // ���������� ��������
 ButtonItem  = DM->RegComp->ExtBarItems->AddItem();
 ButtonItem->Caption = "�������� ��������";
 ButtonItem->ImageIndex = 21;
 ButtonItem->ShortCut = ShortCut(VK_INSERT, TShiftState()<<ssShift);
 ButtonItem->OnGetItemEnabled = GetAddPrivEnabled;
 ButtonItem->OnItemClick      = AddPrivClick;

 // �������������� ��������� ��������
 ButtonItem  = DM->RegComp->ExtBarItems->AddItem();
 ButtonItem->Caption = "������������� ��������� ��������";
 ButtonItem->ImageIndex = 22;
 ButtonItem->ShortCut = ShortCut(VK_RETURN, TShiftState()<<ssShift<<ssCtrl);
 ButtonItem->OnGetItemEnabled = GetLastPrivEnabled;
 ButtonItem->OnItemClick      = LastPrivClick;

 // ���������� �����
 ButtonItem  = DM->RegComp->ExtBarItems->AddItem();
 ButtonItem->Caption = "�������� �����";
 ButtonItem->ImageIndex = 23;
 ButtonItem->ShortCut = ShortCut(VK_INSERT, TShiftState()<<ssAlt);
 ButtonItem->OnGetItemEnabled = GetAddProbEnabled;
 ButtonItem->OnItemClick      = AddProbClick;

 // �������������� ��������� �����
 ButtonItem  = DM->RegComp->ExtBarItems->AddItem();
 ButtonItem->Caption = "������������� ��������� �����";
 ButtonItem->ImageIndex = 24;
 ButtonItem->ShortCut = ShortCut(VK_RETURN, TShiftState()<<ssShift<<ssAlt);
 */
void __fastcall TdsUnitListExClientForm::actInsPrivExecute(TObject * Sender)
 {
  int SelCount = ClassGridView->DataController->GetSelectedCount();
  if (actInsPriv->Enabled && SelCount && FDM->OnInsPTCData)
   {
    CheckCtrlState(false);
    if (FDM->OnInsPTCData(TkabProgressType::InitStage, ietPriv, NULL, FDataSrc, SelCount, CheckProgress))
     {
      Application->ProcessMessages();
      try
       {
        int RowIdx;
        for (int i = 0; i < SelCount; i++)
         {
          Application->ProcessMessages();
          RowIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
          Application->ProcessMessages();
          FDM->OnInsPTCData(TkabProgressType::StageInc, ietPriv, _CLASSNODE_(), FDataSrc, RowIdx, CheckProgress);
          Application->ProcessMessages();
         }
        Application->ProcessMessages();
        FDM->OnInsPTCData(TkabProgressType::StageComplite, ietPriv, _CLASSNODE_(), FDataSrc, 0, CheckProgress);
        Application->ProcessMessages();
       }
      __finally
       {
        CheckCtrlState(true);
        Application->ProcessMessages();
       }
      // ClassGridView->Controller->ClearSelection();
      SetActiveCtrl();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actInsTestExecute(TObject * Sender)
 {
  int SelCount = ClassGridView->DataController->GetSelectedCount();
  if (actInsTest->Enabled && SelCount && FDM->OnInsPTCData)
   {
    CheckCtrlState(false);
    if (FDM->OnInsPTCData(TkabProgressType::InitStage, ietTest, NULL, FDataSrc, SelCount, CheckProgress))
     {
      Application->ProcessMessages();
      try
       {
        int RowIdx;
        for (int i = 0; i < SelCount; i++)
         {
          Application->ProcessMessages();
          RowIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
          Application->ProcessMessages();
          FDM->OnInsPTCData(TkabProgressType::StageInc, ietTest, _CLASSNODE_(), FDataSrc, RowIdx, CheckProgress);
          Application->ProcessMessages();
         }
        Application->ProcessMessages();
        FDM->OnInsPTCData(TkabProgressType::StageComplite, ietTest, _CLASSNODE_(), FDataSrc, 0, CheckProgress);
        Application->ProcessMessages();
       }
      __finally
       {
        CheckCtrlState(true);
        Application->ProcessMessages();
       }
      // ClassGridView->Controller->ClearSelection();
      SetActiveCtrl();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actEditLastPrivExecute(TObject * Sender)
 {
  int SelCount = ClassGridView->DataController->GetSelectedCount();
  if (actEditLastPriv->Enabled && SelCount && FDM->OnEditPTCData)
   {
    CheckCtrlState(false);
    if (FDM->OnEditPTCData(TkabProgressType::InitStage, ietPriv, NULL, FDataSrc, SelCount, CheckProgress))
     {
      Application->ProcessMessages();
      try
       {
        int RowIdx;
        for (int i = 0; i < SelCount; i++)
         {
          Application->ProcessMessages();
          RowIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
          Application->ProcessMessages();
          FDM->OnEditPTCData(TkabProgressType::StageInc, ietPriv, _CLASSNODE_(), FDataSrc, RowIdx, CheckProgress);
          Application->ProcessMessages();
         }
        Application->ProcessMessages();
        FDM->OnEditPTCData(TkabProgressType::StageComplite, ietPriv, _CLASSNODE_(), FDataSrc, 0, CheckProgress);
        Application->ProcessMessages();
       }
      __finally
       {
        CheckCtrlState(true);
        Application->ProcessMessages();
       }
      // ClassGridView->Controller->ClearSelection();
      SetActiveCtrl();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsUnitListExClientForm::actEditLastTestExecute(TObject * Sender)
 {
  int SelCount = ClassGridView->DataController->GetSelectedCount();
  if (actEditLastTest->Enabled && SelCount && FDM->OnEditPTCData)
   {
    CheckCtrlState(false);
    if (FDM->OnEditPTCData(TkabProgressType::InitStage, ietTest, NULL, FDataSrc, SelCount, CheckProgress))
     {
      Application->ProcessMessages();
      try
       {
        int RowIdx;
        for (int i = 0; i < SelCount; i++)
         {
          Application->ProcessMessages();
          RowIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
          Application->ProcessMessages();
          FDM->OnEditPTCData(TkabProgressType::StageInc, ietTest, _CLASSNODE_(), FDataSrc, RowIdx, CheckProgress);
          Application->ProcessMessages();
         }
        Application->ProcessMessages();
        FDM->OnEditPTCData(TkabProgressType::StageComplite, ietTest, _CLASSNODE_(), FDataSrc, 0, CheckProgress);
        Application->ProcessMessages();
       }
      __finally
       {
        CheckCtrlState(true);
        Application->ProcessMessages();
       }
      // ClassGridView->Controller->ClearSelection();
      SetActiveCtrl();
     }
   }
 }
// ---------------------------------------------------------------------------
