//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop
USEFORM("..\Src\dsProgress.cpp", dsProgressForm);
USEFORM("..\Src\dsClassifEditClientUnit.cpp", dsClassEditClientForm);
USEFORM("..\Src\dsClassifClientUnit.cpp", dsClassifClientForm);
USEFORM("..\Src\dsRegDMUnit.cpp", dsRegDM); /* TDataModule: File Type */
USEFORM("..\Src\dsUnitListClientUnit.cpp", dsUnitListClientForm);
USEFORM("..\Src\dsUnitListExClientUnit.cpp", dsUnitListExClientForm);
USEFORM("..\Src\dsSelUnitListClientUnit.cpp", dsSelUnitListClientForm);
USEFORM("..\Src\dsRegTreeClass.cpp", dsRegTreeClassForm);
USEFORM("..\Src\dsRegTemplateUnit.cpp", dsRegTemplateForm);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#pragma argsused
extern "C" int _libmain(unsigned long reason)
{
 return 1;
}
//---------------------------------------------------------------------------
