﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("Pkg\dsPlan\Src\vacupdate\AddIncompMIBPUnit.cpp", AddIncompMIBPForm);
USEFORM("Pkg\dsPlan\Src\vacupdate\AddPauseUnit.cpp", AddPauseForm);
USEFORM("main_ips_admin\MainUnit.cpp", MainForm);
USEFORM("main_ips_admin\vacupdate\UpdateMIBPForm.cpp", UpdateMIBPForm);
USEFORM("Pkg\dsPlan\Src\vacupdate\dsICPlanDMUnit.cpp", dsICPlanDM); /* TDataModule: File Type */
USEFORM("Pkg\dsPlan\Src\vacupdate\dsIPSPlanSetting.cpp", IPSPlanSettingForm);
USEFORM("Pkg\dsPlan\Src\vacupdate\AddPlanInfUnit.cpp", AddPlanInfForm);
USEFORM("Pkg\dsPlan\Src\vacupdate\AddPreTestUnit.cpp", AddPreTestForm);
USEFORM("Pkg\dsPlan\Src\vacupdate\dsCommSchListUnit.cpp", CommSchListForm);
USEFORM("Common\Connect\MainLPUCodeEdit.cpp", MainLPUEditForm);
USEFORM("Common\ProgressUnit.cpp", dsProgressForm);
USEFORM("Common\Connect\LoginUnit.cpp", LoginForm);
USEFORM("Common\Connect\DBSelect.cpp", DBSelectUnit);
USEFORM("Common\Connect\dsConnectUnit.cpp", ConnectDM); /* TDataModule: File Type */
USEFORM("main_ips_admin\DMUnit.cpp", DM); /* TDataModule: File Type */
USEFORM("main_ips_admin\CheckCodeUnit.cpp", CheckCode);
USEFORM("Common\Update\ClientUpdateUnit.cpp", ClientUpdateForm);
USEFORM("Common\Update\UpdateUnit.cpp", UpdateDM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
#include "ClientUpdateUnit.h"
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    Application->MainFormOnTaskBar = true;
    bool Continue = false;
    TClientUpdateForm * UpdDlg = new TClientUpdateForm(NULL);
    try
     {
      Continue = UpdDlg->CheckUpdate(false);
     }
    __finally
     {
      delete UpdDlg;
     }
    if (Continue)
     {
      Application->CreateForm(__classid(TDM), &DM);
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->Run();
     }
   }
  catch (Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw Exception("");
     }
    catch (Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
//---------------------------------------------------------------------------
