object SetSkinForm: TSetSkinForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1058#1077#1084#1072
  ClientHeight = 155
  ClientWidth = 377
  Color = clBtnFace
  Constraints.MinHeight = 180
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object SkinNameCB: TcxComboBox
    AlignWithMargins = True
    Left = 5
    Top = 5
    Margins.Left = 5
    Margins.Top = 5
    Margins.Right = 5
    Margins.Bottom = 5
    Align = alTop
    Properties.DropDownListStyle = lsEditFixedList
    Properties.Items.Strings = (
      #1057#1090#1072#1085#1076#1072#1088#1090#1085#1072#1103' (Windows)'
      #1050#1083#1072#1089#1089#1080#1095#1077#1089#1082#1072#1103
      'Flat'
      'UltraFlat'
      'Office11'
      'Black'
      'Blue'
      'Blueprint'
      'Caramel'
      'Coffee'
      'Darkroom'
      'DarkSide'
      'DevExpressDarkStyle'
      'DevExpressStyle'
      'Foggy'
      'GlassOceans'
      'HighContrast'
      'iMaginary'
      'Lilian'
      'LiquidSky'
      'LondonLiquidSky'
      'McSkin'
      'Metropolis'
      'MetropolisDark'
      'MoneyTwins'
      'Office2007Black'
      'Office2007Blue'
      'Office2007Green'
      'Office2007Pink'
      'Office2007Silver'
      'Office2010Black'
      'Office2010Blue'
      'Office2010Silver'
      'Office2013DarkGray'
      'Office2013LightGray'
      'Office2013White'
      'Pumpkin'
      'Seven'
      'SevenClassic'
      'Sharp'
      'SharpPlus'
      'Silver'
      'Springtime'
      'Stardust'
      'Summer2008'
      'TheAsphaltWorld'
      'Valentine'
      'VS2010'
      'Whiteprint'
      'Xmas2008Blue')
    Properties.OnChange = SkinNameCBPropertiesChange
    TabOrder = 0
    Width = 367
  end
  object cxGroupBox1: TcxGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 40
    Align = alBottom
    Caption = #1055#1088#1080#1084#1077#1088#1099' '#1086#1090#1088#1080#1089#1086#1074#1082#1080' '#1090#1077#1084#1099
    TabOrder = 1
    Height = 112
    Width = 371
    object cxCheckBox1: TcxCheckBox
      Left = 19
      Top = 66
      Caption = #1054#1090#1084#1077#1090#1082#1072
      TabOrder = 0
      Transparent = True
      Width = 101
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 14
      Top = 26
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end
        item
        end
        item
          Caption = 'X'
          Kind = bkText
        end>
      TabOrder = 1
      Text = #1056#1077#1076#1072#1082#1090#1086#1088' '#1089' '#1082#1085#1086#1087#1082#1072#1084#1080
      Width = 187
    end
    object cxComboBox2: TcxComboBox
      Left = 234
      Top = 26
      Properties.Items.Strings = (
        #1057#1090#1088#1086#1082#1072' 1'
        #1057#1090#1088#1086#1082#1072' 2'
        #1057#1090#1088#1086#1082#1072' 3')
      TabOrder = 2
      Text = #1057#1090#1088#1086#1082#1072' 2'
      Width = 121
    end
    object cxMaskEdit1: TcxMaskEdit
      Left = 234
      Top = 66
      TabOrder = 3
      Text = #1056#1077#1076#1072#1082#1090#1086#1088
      Width = 121
    end
    object cxButton1: TcxButton
      Left = 126
      Top = 64
      Width = 75
      Height = 25
      Caption = #1050#1085#1086#1087#1082#1072
      TabOrder = 4
    end
  end
end
