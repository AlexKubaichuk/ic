//---------------------------------------------------------------------------
#ifndef PGMSETTINGH
#define PGMSETTINGH
//---------------------------------------------------------------------------
//  #include <classes.hpp>
#define IMM_LPU
#define PRODFOLDER UnicodeString("ic")
#define _PRODID_ 1
#define _PRMASK_ 0xFF00

//  #include "pgm_config.h"
  //#ifdef _ADO_ undef _ADO_
//  #define INTEGRATION_MODE

  #define _CONFSETTTING
  //  #define _TIMEDEBUG

  #ifdef _DEMOMODE
    #undef _DEMOMODE
  #endif

  #ifdef IMM_LPU
    #define K_REG "Software\\ICS Ltd.\\IC\\V6"
    //#define _IMM_DEBUG                // ����� ������� ���������
    // ������������
    #define IMM_REG                  // ������������
//    #define IMM_F63                  // ������ ����������� �����
    #define IMM_REG_CLASS            // ������ � ����������� ������������
    #define IMM_REG_CLASS_EDIT       // �������������� ������������ ������������
    #define IMM_REG_LIST             // ������ � ������ ���������
    #define IMM_REG_LIST_EDIT        // �������������� ������ ���������
    #define IMM_REG_CARD             // ������ �� �������� ����� ��������
    #define IMM_CARD_EDIT            // ������ �� �������������� ����� ��������
//    #define IMM_CARD_RO false        // 
    #define _REG_FULL_FILTER         // ���������� ������ ��������� �� ������������
    #define _REG_DOC_CREATE          // ������������ ��������� ���������� �� ������ ���������
    // �����
//    #define IMM_CARD_UPDATE_GEN      // 

    // ������������
    #define IMM_PLAN                 // ������������
    #define IMM_PLAN_EDIT            // ������ � ���������� ������������
    #define IMM_PLAN_SETTING         // ��������� ������������
    #define IMM_PLAN_VAC_SCH         // ����� ������
    #define IMM_PLAN_VAC_SCH_EDIT    // �������������� ���� ������
    #define IMM_PLAN_PROB_SCH        // ����� ����
    #define IMM_PLAN_PROB_SCH_EDIT   // �������������� ���� ����
    #define IMM_PLAN_DISABLE_FILTER  // ����������� ���������� ������ ������� ����������

    // ���������
    #define _DOC_EX                      // ���������
    #define _DOC_EX_SPEC                 // ������ ��������������
    #define _DOC_EX_SPEC_NEW             // ���������� ������ ��������       
    #define _DOC_EX_SPEC_EDIT            // �������������� ��������          
    #define _DOC_EX_SPEC_DELETE          // �������� ��������                
    #define _DOC_EX_SPEC_CREATE          // ������������ ���������           
    #define _DOC_EX_SPEC_HANDCREATE      // ������ �������������� ���������  
    #define _DOC_EX_BANKDOC              // ������ ����������
    #define _DOC_EX_FILTER_LIST          // ������ ��������
    #define _DOC_EX_FILTER_LIST_EDIT     // �������������� ������ ��������
    #define _DOC_EX_SUBORD_ORG_LIST      // ������ ����������������� �����������
    #define _DOC_EX_SUBORD_ORG_LIST_EDIT // �������������� ������ ����������������� �����������
//    #define _DOC_EX_CREATE_GROUP_REPORT  // ������������ ���������� ��� ������ ���������
    // ������/�������
    #define IMM_EI                   // ����/�������� ������
    #define IMM_EI_SPECDOC           // ����/�������� ����������
    #define IMM_EI_SPECDOC_SI        // ���� �������� ����������
    #define IMM_EI_SPECDOC_DE        // �������� ����������
//    #define _DOC_DOS_EI              
    #define IMM_EI_SPECDOC_DOS_SI    // ���� �������� ���������� �� DOS
    #define IMM_EI_SPECDOC_DOS_DE    // �������� ���������� � DOS
//    #define IMM_EI_CARD              // ����/�������� ����
//    #define IMM_EI_SCH               // ����/�������� ����
    // ������
//    #define IMM_GROUP_MODIF          //
//    #define IMM_SETTING
//    #define IMM_SETTING_COMM
//    #define IMM_SETTING_RIGHT
//    #define IMM_SETTING_REG
    // �����
//    #define IMM_ARC                  // ����� ������������
//    #define IMM_ARC_CLASS            // ������ � ����������� ������ ������������
//    #define IMM_ARC_LIST             // ������ � ������ ������ ���������
//    #define IMM_ARC_CARD             // ������ �� �������� �������� ����� ��������
  #endif

  #ifdef IMM_LPU
    #define _PRODNAME  "���������� ������������"
  #else
    #define _PRODNAME  "���������� ������������ - ???"
  #endif
  // ������������� ������������
  #ifdef _REG_FULL_FILTER
    #define _USE_NOM            // ������������ �����������
  #endif
  #ifdef _DOC_EX_FILTER_LIST         
    #define _USE_NOM            // ������������ �����������
  #endif
  //###########################################################################
  //#                                                                         #
  //#                            ����� ������                                 #
  //#                                                                         #
  //###########################################################################
  //---------------------------------------------------------------------------
  #define _IMM_DEBUG
  #ifndef _IMM_DEBUG
    #ifndef IMM_DOCUMENT 
      #define GUARDANT
      #define _ENCODECHAR_ 0xC5
      #define _DEFAPPVER_  0x40
      #define ipV1 "[����'�hn2_��{3�-<bT_@��'�W�"
      #define ipV2 ")���J_\"�4f��$�7�_Rt�[pg{�_&B1"
      #include "GrdDongle.h"
      #ifdef _USE_TIME
        #define _ENCODEDEFNUM_ 0x0D
        #define _LICDEFNUM_    0x0C
        #define _DOCENCODEDEFNUM_ 0x0E
        #define _DOCDECODEDEFNUM_ 0x0F
        #define ipDocV1 "֪��@y�a�j\"�rN's�ic��>}ˆ�Y"
        //#define ipDocV2 "+8<RL��&�>�Z��u�9�yPx�t������"
      #else
        #define _ENCODEDEFNUM_ 0x04
        #define _DOCENCODEDEFNUM_ 0x05
        #define _DOCDECODEDEFNUM_ 0x06
        #define ipDocV1 "֪��@y�a�j\"�rN's�ic��>}ˆ�Y"
        //#define ipDocV2 "+8<RL��&�>�Z��u�9�yPx�t������"
      #endif
    #endif
  #endif
  //---------------------------------------------------------------------------

#endif



