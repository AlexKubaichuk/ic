object DM: TDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 456
  Width = 441
  object CliOptDlg: TAppOptDialog
    FormHeight = 500
    Options = CliOpt
    OnExternEditClick = CliOptDlgExternEditClick
    AfterComponentCreate = CliOptDlgAfterComponentCreate
    Left = 26
    Top = 96
  end
  object CliOpt: TAppOptions
    OnLoadXML = CliOptLoadXML
    DataProvider = CliOptXMLData
    Left = 100
    Top = 95
  end
  object CliOptXMLData: TAppOptXMLProv
    OnWriteVal = CliOptXMLDataWriteVal
    Left = 188
    Top = 94
  end
  object AppOptDlg: TAppOptDialog
    FormHeight = 500
    Options = AppOpt
    OnExternEditClick = AppOptDlgExternEditClick
    AfterComponentCreate = AppOptDlgAfterComponentCreate
    Left = 23
    Top = 164
  end
  object AppOpt: TAppOptions
    OnLoadXML = AppOptLoadXML
    DataProvider = AppOptXMLData
    Left = 98
    Top = 166
  end
  object AppOptXMLData: TAppOptXMLProv
    OnWriteVal = AppOptXMLDataWriteVal
    Left = 187
    Top = 168
  end
  object EditStyle: TcxEditStyleController
    Left = 85
    Top = 356
    PixelsPerInch = 96
  end
  object DefEditStyle: TcxDefaultEditStyleController
    Left = 150
    Top = 357
    PixelsPerInch = 96
  end
  object Skin: TdxSkinController
    Left = 27
    Top = 356
  end
  object OpenDlg: TOpenDialog
    Filter = #1050#1072#1088#1090#1099' '#1055#1050' '#1059#1048'-'#1051#1055#1059' (*.zxml)|*.zxml'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 172
    Top = 230
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.zxml'
    Filter = #1050#1072#1088#1090#1099' '#1055#1050' '#1059#1048'-'#1051#1055#1059' (*.zxml)|*.zxml'
    Left = 170
    Top = 294
  end
  object ConnectOpt: TAppOptions
    OnLoadXML = ConnectOptLoadXML
    DataProvider = ConnectOptXMLData
    Left = 99
    Top = 28
  end
  object ConnectOptXMLData: TAppOptXMLProv
    Left = 188
    Top = 28
  end
  object ConnectOptDlg: TAppOptDialog
    FormHeight = 500
    Options = ConnectOpt
    OnExternEditClick = ConnectOptDlgExternEditClick
    AfterComponentCreate = ConnectOptDlgAfterComponentCreate
    Left = 27
    Top = 29
  end
end
