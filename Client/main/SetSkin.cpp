﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SetSkin.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlack"
#pragma link "dxSkinBlue"
#pragma link "dxSkinBlueprint"
#pragma link "dxSkinCaramel"
#pragma link "dxSkinCoffee"
#pragma link "dxSkinDarkRoom"
#pragma link "dxSkinDarkSide"
#pragma link "dxSkinDevExpressDarkStyle"
#pragma link "dxSkinDevExpressStyle"
#pragma link "dxSkinFoggy"
#pragma link "dxSkinGlassOceans"
#pragma link "dxSkinHighContrast"
#pragma link "dxSkiniMaginary"
#pragma link "dxSkinLilian"
#pragma link "dxSkinLiquidSky"
#pragma link "dxSkinLondonLiquidSky"
#pragma link "dxSkinMcSkin"
#pragma link "dxSkinMetropolis"
#pragma link "dxSkinMetropolisDark"
#pragma link "dxSkinMoneyTwins"
#pragma link "dxSkinOffice2007Black"
#pragma link "dxSkinOffice2007Blue"
#pragma link "dxSkinOffice2007Green"
#pragma link "dxSkinOffice2007Pink"
#pragma link "dxSkinOffice2007Silver"
#pragma link "dxSkinOffice2010Black"
#pragma link "dxSkinOffice2010Blue"
#pragma link "dxSkinOffice2010Silver"
#pragma link "dxSkinOffice2013DarkGray"
#pragma link "dxSkinOffice2013LightGray"
#pragma link "dxSkinOffice2013White"
#pragma link "dxSkinPumpkin"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsDefaultPainters"
#pragma link "dxSkinSeven"
#pragma link "dxSkinSevenClassic"
#pragma link "dxSkinSharp"
#pragma link "dxSkinSharpPlus"
#pragma link "dxSkinSilver"
#pragma link "dxSkinSpringTime"
#pragma link "dxSkinStardust"
#pragma link "dxSkinSummer2008"
#pragma link "dxSkinTheAsphaltWorld"
#pragma link "dxSkinValentine"
#pragma link "dxSkinVS2010"
#pragma link "dxSkinWhiteprint"
#pragma link "dxSkinXmas2008Blue"
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxGroupBox"
#pragma resource "*.dfm"
TSetSkinForm *SetSkinForm;
//---------------------------------------------------------------------------
__fastcall TSetSkinForm::TSetSkinForm(TComponent* Owner)
 : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSetSkinForm::SkinNameCBPropertiesChange(TObject *Sender)
{
/*
0 Стандартная (Windows)
1 Классическая
2 Flat
3 UltraFlat
4 Office11
*/
  if (SkinNameCB->ItemIndex > 0)
   {
     DM->Skin->NativeStyle = false;
     DM->Skin->SkinName = "";
     switch (SkinNameCB->ItemIndex)
     {
       case 1:  {DM->Skin->Kind = lfStandard;  break;}
       case 2:  {DM->Skin->Kind = lfFlat;      break;}
       case 3:  {DM->Skin->Kind = lfUltraFlat; break;}
       case 4:  {DM->Skin->Kind = lfOffice11;  break;}
       default:
        {
          DM->Skin->SkinName = SkinNameCB->Properties->Items->Strings[SkinNameCB->ItemIndex];
        }
     }
     Application->ProcessMessages();
   }
  else
   {
     DM->Skin->NativeStyle = true;
     DM->Skin->SkinName = "";
     DM->Skin->Kind = lfStandard;
   }

  DM->CliOpt->Vals["skkind"]  = UnicodeString("lfStandard");
  DM->CliOpt->Vals["skinname"]    = DM->Skin->SkinName;
  DM->CliOpt->Vals["sknativestyle"] = DM->Skin->NativeStyle;
  switch (DM->Skin->Kind)
  {
    case lfStandard :  {DM->CliOpt->Vals["skkind"] = UnicodeString("lfStandard");  break;} //skkind
    case lfFlat     :  {DM->CliOpt->Vals["skkind"] = UnicodeString("lfFlat");      break;}
    case lfUltraFlat:  {DM->CliOpt->Vals["skkind"] = UnicodeString("lfUltraFlat"); break;}
    case lfOffice11 :  {DM->CliOpt->Vals["skkind"] = UnicodeString("lfOffice11");  break;}
  }
//  ShowMessage(DM->CliOpt->Vals["skinname"].AsStringDef("qqq"));
// DM->CliOpt->Vals["kind"]        = DM->Skin->Kind;
}
//---------------------------------------------------------------------------
