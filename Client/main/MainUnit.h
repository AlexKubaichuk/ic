﻿//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ImgList.hpp>
#include "cxClasses.hpp"
#include "cxGraphics.hpp"
#include "dxBar.hpp"
#include "ICSAboutDialog.h"
//---------------------------------------------------------------------------
#include "DMUnit.h"
/*
//---------------------------------------------------------------------------
#include "dxSkinBlack.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinBlueprint.hpp"
#include "dxSkinCaramel.hpp"
#include "dxSkinCoffee.hpp"
#include "dxSkinDarkRoom.hpp"
#include "dxSkinDarkSide.hpp"
#include "dxSkinDevExpressDarkStyle.hpp"
#include "dxSkinDevExpressStyle.hpp"
#include "dxSkinFoggy.hpp"
#include "dxSkinGlassOceans.hpp"
#include "dxSkinHighContrast.hpp"
#include "dxSkiniMaginary.hpp"
#include "dxSkinLilian.hpp"
#include "dxSkinLiquidSky.hpp"
#include "dxSkinLondonLiquidSky.hpp"
#include "dxSkinMcSkin.hpp"
#include "dxSkinMetropolis.hpp"
#include "dxSkinMetropolisDark.hpp"
#include "dxSkinMoneyTwins.hpp"
#include "dxSkinOffice2007Black.hpp"
#include "dxSkinOffice2007Blue.hpp"
#include "dxSkinOffice2007Green.hpp"
#include "dxSkinOffice2007Pink.hpp"
#include "dxSkinOffice2007Silver.hpp"
#include "dxSkinOffice2010Black.hpp"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinOffice2010Silver.hpp"
#include "dxSkinOffice2013DarkGray.hpp"
#include "dxSkinOffice2013LightGray.hpp"
#include "dxSkinOffice2013White.hpp"
#include "dxSkinPumpkin.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsDefaultPainters.hpp"
#include "dxSkinsdxBarPainter.hpp"
#include "dxSkinSeven.hpp"
#include "dxSkinSevenClassic.hpp"
#include "dxSkinSharp.hpp"
#include "dxSkinSharpPlus.hpp"
#include "dxSkinSilver.hpp"
#include "dxSkinSpringTime.hpp"
#include "dxSkinStardust.hpp"
#include "dxSkinSummer2008.hpp"
#include "dxSkinTheAsphaltWorld.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinVS2010.hpp"
#include "dxSkinWhiteprint.hpp"
#include "dxSkinXmas2008Blue.hpp"
#include <Vcl.Mask.hpp>
#include <Vcl.StdCtrls.hpp>
*/
class TMainForm : public TForm
{
__published:	// IDE-managed Components
  TActionList *MainAL;
  TAction *actPatList;
  TAction *actGetClasses;
  TAction *actPlanWork;
  TAction *actSpecListOpen;
  TAction *actDocBankOpen;
  TAction *actDocChoice;
  TAction *actPgmSetting;
  TAction *actHelp;
  TAction *actAbout;
  TcxImageList *MainIL;
  TdxBarManager *MainTBM;
  TdxBar *MainTBMBar2;
  TdxBarSubItem *mHelp;
  TdxBarButton *mHelpItem;
  TdxBarButton *mHelpAboutItem;
  TdxBarSubItem *mService;
  TdxBarButton *mSetting;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton5;
 TdxBarLargeButton *dxBarLargeButton6;
 TdxBarLargeButton *dxBarLargeButton7;
 TdxBarLargeButton *dxBarLargeButton8;
 TdxBarLargeButton *SettingBI;
 TICSAboutDialog *AboutDlg;
 TAction *actShowStore;
 TdxBarLargeButton *dxBarLargeButton11;
 TcxImageList *LMainIL48;
 TdxBarDockControl *MainDoc;
 TdxBarPopupMenu *ServicePM;
 TdxBar *MainTBMBar7;
 TdxBarButton *dxBarButton8;
 TAction *actClientSetting;
 TAction *actConnectSetting;
 TdxBarLargeButton *dxBarLargeButton14;
 TdxBarSubItem *biService;
 TdxBarLargeButton *dxBarLargeButton3;
 TAction *actPatRegDataGrChange;
 TAction *actPatPrivTestGrInsert;
 TdxBarLargeButton *dxBarLargeButton4;
 TAction *actPatOtvodGrInsert;
 TdxBarLargeButton *dxBarLargeButton9;
 TAction *actPatSchemeGrChange;
 TdxBarLargeButton *dxBarLargeButton10;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
  void __fastcall actPatListExecute(TObject *Sender);
  void __fastcall actGetClassesExecute(TObject *Sender);
  void __fastcall actPlanWorkExecute(TObject *Sender);
  void __fastcall actSpecListOpenExecute(TObject *Sender);
  void __fastcall actDocBankOpenExecute(TObject *Sender);
  void __fastcall actDocChoiceExecute(TObject *Sender);
  void __fastcall actPgmSettingExecute(TObject *Sender);
  void __fastcall actHelpExecute(TObject *Sender);
  void __fastcall actAboutExecute(TObject *Sender);
 void __fastcall actShowStoreExecute(TObject *Sender);
 void __fastcall actClientSettingExecute(TObject *Sender);
 void __fastcall actConnectSettingExecute(TObject *Sender);
 void __fastcall SettingBIClick(TObject *Sender);
 void __fastcall actPatRegDataGrChangeExecute(TObject *Sender);
 void __fastcall actPatPrivTestGrInsertExecute(TObject *Sender);
 void __fastcall actPatOtvodGrInsertExecute(TObject *Sender);
 void __fastcall actPatSchemeGrChangeExecute(TObject *Sender);

private:	// User declarations
  int FC;
  void __fastcall UpdateMenuItems();
  bool __fastcall CheckConnect();
public:		// User declarations
  __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
