﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "DMUnit.h"
#include "SetDefAddr.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxListBox"
#pragma resource "*.dfm"
TDefAddrForm * DefAddrForm;
// ---------------------------------------------------------------------------
__fastcall TDefAddrForm::TDefAddrForm(TComponent * Owner) : TForm(Owner)
 {
  FFindedObjects.clear();
  AddrLB->Visible = false;
  AddrLB->Items->Clear();
  FAddr        = DM->CliOpt->Vals["defAddr"].AsStringDef("");
  AddrBE->Text = "";
  if (FAddr.Trim().Length())
   {
    UnicodeString AddrRC = DM->AddrCodeToStrF(FAddr, 0xFE);
    if (!SameText(AddrRC, "error"))
     {
      AddrBE->Text = AddrRC;
      FAddr        = DM->CliOpt->Vals["defAddr"].AsStringDef("");
     }
    else
     {
      FAddr        = "";
      AddrBE->Text = "";
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::AddrBEPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  UnicodeString tmpAddr = AddrBE->Text.Trim();
  if (tmpAddr.Length() && !FAddr.Length())
   { // что-то введено и адрес не определялся
    if (SameText(DM->GetAddr(tmpAddr, "", 0x1C), "ok")) // 0x1C - только формализованный адрес, без квартиры
     {
      TStringList * FValues = new TStringList;
      try
       {
        AddrLB->Items->Clear();
        FValues->Text = tmpAddr;
        if (FValues->Count == 1)
         {
          // ShowMessage(FValues->Text);
          AddrBE->Text = GetRPartB(FValues->Strings[0], '='); ;
          FAddr        = GetLPartB(FValues->Strings[0], '=');
         }
        else if (FValues->Count > 1)
         {
          for (int i = 0; i < FValues->Count; i++)
           {
            FFindedObjects[FValues->Values[FValues->Names[i]].UpperCase()] = FValues->Names[i];
            AddrLB->Items->Add(FValues->Values[FValues->Names[i]]);
           }
          // AddrLB->Left = Left + AddrBE->Left;
          // AddrLB->Top     = Top + AddrBE->Top + AddrBE->Height + 1;
          // AddrLB->Width   = AddrBE->Width;
          AddrLB->Visible = true;
          AddrLB->BringToFront();
          AddrLB->SetFocus();
          AddrLB->ItemIndex = 0;
         }
        else
         {
          FAddr = "";
          AddrBE->SetFocus();
         }
       }
      __finally
       {
        delete FValues;
       }
     }
   }
  /*
   UnicodeString FAddr = AddrBE->Text;
   DM->GetAddr(FAddr,"",0x1E);
   AddrBE->Text = FAddr;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::OkBtnClick(TObject * Sender)
 {
  // ShowMessage(FAddr);
  if (AddrBE->Text.Trim().Length())
   DM->CliOpt->Vals["defAddr"] = FAddr;
  else
   DM->CliOpt->Vals["defAddr"] = "";
  ModalResult = mrOk;
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::AddrLBDblClick(TObject * Sender)
 {
  if (AddrLB->ItemIndex != -1)
   {
    AddrBE->Text = AddrLB->Items->Strings[AddrLB->ItemIndex];
    FAddr        = FFindedObjects[AddrLB->Items->Strings[AddrLB->ItemIndex].UpperCase()];
    FFindedObjects.clear();
    AddrLB->Visible = false;
    AddrBE->SetFocus();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::AddrLBKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    AddrLBDblClick((TcxListBox *) Sender);
    Key = 0;
   }
  else if (Key == VK_ESCAPE)
   {
    FFindedObjects.clear();
    ((TcxListBox *)Sender)->Visible = false;
    AddrBE->SetFocus();
    FAddr = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::AddrLBExit(TObject * Sender)
 {
  AddrLB->Visible = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::AddrBEKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN) // (Key == 'F') && Shift.Contains(ssCtrl)
   {
    if (!FAddr.Length())
     {
      AddrBEPropertiesButtonClick(AddrBE, 0);
      Key = 0;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDefAddrForm::AddrBEPropertiesChange(TObject * Sender)
 {
  FAddr = "";
 }
// ---------------------------------------------------------------------------
