﻿// ---------------------------------------------------------------------------
#pragma hdrstop
#include "dsConnectUnit.h"
// #include "ExtUtils.h"
#include "LoginUnit.h"
#include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TConnectDM * ConnectDM;
// ---------------------------------------------------------------------------
__fastcall TConnectDM::TConnectDM(TComponent * Owner, UnicodeString AConnData) : TDataModule(Owner)
 {
  try
   {
    FUserName = "";
    FPassword = "";
    SetHostParam(AConnData);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::DataModuleDestroy(TObject * Sender)
 {
  delete FDocClient;
  delete FAppClient;
  // delete FKLADRClient;
  // delete FOrgClient;
  delete FVSClient;
  delete FEIDataClient;
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::dsICRestConLogin(TObject * Sender, TDSRestLoginProperties * LoginProperties, bool & Cancel)
 {
  if (FUserName.Length() && FPassword.Length())
   {
    LoginProperties->UserName = FUserName;
    LoginProperties->Password = FPassword;
   }
  else
   {
    // LoginProperties->UserName = "1";
    // LoginProperties->Password = "1";
    TLoginForm * LogDlg = new TLoginForm(this);
    try
     {
      try
       {
        LogDlg->ShowModal();
        Cancel = (LogDlg->ModalResult != mrOk);
        if (!Cancel)
         {
          LoginProperties->UserName = LogDlg->User;
          LoginProperties->Password = LogDlg->Pass;
          FUserName                 = LogDlg->User;
          FPassword                 = LogDlg->Pass;
         }
       }
      catch (System::Sysutils::Exception & E)
       {
        _MSG_ERRA("Ошибка аутентификации пользователя.\nСистемное сообщение:\n" + E.Message, "Ошибка входа в систему.");
        Application->Terminate();
       }
     }
    __finally
     {
      delete LogDlg;
     }
   }
  // ShowMessage("dsRestConLogin");
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::SetConnectParam(UnicodeString AUser, UnicodeString APaswd)
 {
  FUserName = AUser;
  FPassword = APaswd;
  // HTTPAuth->Username       = AUser;
  // HTTPAuth->Password       = APaswd;
  // dsICRestCon->UserName    = AUser;
  // dsICRestCon->Password    = APaswd;
  // dsICDocRestCon->UserName = AUser;
  // dsICDocRestCon->Password = APaswd;
  // dsICSVRestCon->UserName  = AUser;
  // dsICSVRestCon->Password  = APaswd;
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::SetHostParam(UnicodeString AConnData)
 {
  TTagNode * FConnectParam = new TTagNode;
  FConnectParam->AsXML = AConnData;
  try
   {
    UnicodeString FSrvHost = FConnectParam->AV["ic_server"];
    int FSrvPort = FConnectParam->AV["ic_port"].ToIntDef(5100);
    UnicodeString FSrvDocHost = FConnectParam->AV["ic_doc_server"];
    int FSrvDocPort = FConnectParam->AV["ic_doc_port"].ToIntDef(5100);
    UnicodeString FSrvSVHost = FConnectParam->AV["ic_sv_server"];
    int FSrvSVPort = FConnectParam->AV["ic_sv_port"].ToIntDef(5100);
    dsICRestCon->Host    = FSrvHost;
    dsICRestCon->Port    = FSrvPort;
    dsICDocRestCon->Host = FSrvDocHost;
    dsICDocRestCon->Port = FSrvDocPort;
    dsICSVRestCon->Host  = FSrvSVHost;
    dsICSVRestCon->Port  = FSrvSVPort;
    FAppClient           = new TdsICClassClient(dsICRestCon);
    FEIDataClient        = new TdsEIDataClassClient(dsICRestCon);
    // FKLADRClient  = new TdsKLADRClassClient (dsICRestCon);
    // FOrgClient    = new TdsOrgClassClient   (dsICRestCon);
    FAdmin = new TdsAdminClassClient(dsICRestCon);
    if ((FSrvHost.UpperCase() == FSrvDocHost.UpperCase()) && (FSrvPort == FSrvDocPort))
     FDocClient = new TdsDocClassClient(dsICRestCon);
    else
     FDocClient = new TdsDocClassClient(dsICDocRestCon);
    if ((FSrvHost.UpperCase() == FSrvSVHost.UpperCase()) && (FSrvPort == FSrvSVPort))
     FVSClient = new TdsVacStoreClassClient(dsICRestCon);
    else
     FVSClient = new TdsVacStoreClassClient(dsICSVRestCon);
   }
  __finally
   {
    delete FConnectParam;
   }
 }
// ---------------------------------------------------------------------------
