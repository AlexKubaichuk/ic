﻿//---------------------------------------------------------------------------
#ifndef MainLPUCodeEditH
#define MainLPUCodeEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dsRegTypeDef.h"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"
//---------------------------------------------------------------------------
class PACKAGE TMainLPUEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TPanel *LPUClassPanel;
 TLabel *LPULab;
 TcxComboBox *LPUCB;
        void __fastcall OkBtnClick(TObject *Sender);
private:
  TdsGetValByIdEvent FOnGetValById;
  __int64 FLPUCode;

  void __fastcall LoadLPUData(TStringList *AItems, UnicodeString ASQL);
  bool __fastcall CheckInput();

  void __fastcall FSetLPUCode(__int64 AValue);

public:
  __fastcall TMainLPUEditForm(TComponent* Owner, TdsGetValByIdEvent AOnGetValById);

  __property __int64 LPUCode = {read=FLPUCode, write=FSetLPUCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TMainLPUEditForm *MainLPUEditForm;
//---------------------------------------------------------------------------
#endif
