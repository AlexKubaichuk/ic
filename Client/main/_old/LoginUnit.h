﻿//---------------------------------------------------------------------------

#ifndef LoginUnitH
#define LoginUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class TLoginForm : public TForm
{
__published:	// IDE-managed Components
 TcxButton *ApplyBtn;
 TcxButton *CancelBtn;
 TcxCheckBox *SavePassChB;
 TLabel *UserPassLab;
 TLabel *UserNameLab;
 TcxMaskEdit *UserED;
 TcxMaskEdit *PassED;
 void __fastcall UserEDPropertiesChange(TObject *Sender);
 void __fastcall SavePassChBClick(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
private:	// User declarations
  UnicodeString __fastcall FGetUser();
  UnicodeString __fastcall FGetPass();
public:		// User declarations
 __fastcall TLoginForm(TComponent* Owner);
  __property UnicodeString User = {read=FGetUser};
  __property UnicodeString Pass = {read=FGetPass};
};
//---------------------------------------------------------------------------
extern PACKAGE TLoginForm *LoginForm;
//---------------------------------------------------------------------------
#endif
