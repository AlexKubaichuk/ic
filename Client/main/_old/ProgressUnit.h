﻿//---------------------------------------------------------------------------

#ifndef ProgressUnitH
#define ProgressUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxProgressBar.hpp"
#include "cxLabel.hpp"
#include "cxImage.hpp"
#include <Vcl.Graphics.hpp>
#include "dxGDIPlusClasses.hpp"
//---------------------------------------------------------------------------
class TdsProgressForm : public TForm
{
__published:	// IDE-managed Components
 TTimer *RefreshTimer;
 TcxProgressBar *PrBar;
 TcxLabel *MsgLab;
 TcxImage *cxImage1;
 TcxLabel *cxLabel1;
 void __fastcall RefreshTimerTimer(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
 __fastcall TdsProgressForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsProgressForm *dsProgressForm;
//---------------------------------------------------------------------------
#endif
