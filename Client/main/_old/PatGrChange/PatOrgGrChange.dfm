inherited PatOrgGrChangeForm: TPatOrgGrChangeForm
  Caption = 'PatOrgGrChangeForm'
  ExplicitWidth = 739
  ExplicitHeight = 628
  PixelsPerInch = 96
  TextHeight = 13
  inherited StepPC: TcxPageControl
    Properties.ActivePage = ReadyTS
    inherited Step2TS: TcxTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 709
      ExplicitHeight = 409
    end
    inherited Step3TS: TcxTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 709
      ExplicitHeight = 409
      object RegDataPanel: TPanel
        Left = 0
        Top = 0
        Width = 709
        Height = 409
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object S1Panel: TPanel
          Left = 0
          Top = -1
          Width = 362
          Height = 410
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    inherited ReadyTS: TcxTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 709
      ExplicitHeight = 409
      inherited ReadyPC: TcxPageControl
        inherited BaseTS: TcxTabSheet
          ExplicitLeft = 4
          ExplicitTop = 4
          ExplicitWidth = 695
          ExplicitHeight = 395
        end
      end
    end
    inherited RunTS: TcxTabSheet
      inherited RunMsgLab: TcxLabel
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Panel2: TPanel
    inherited Panel4: TPanel
      inherited StepNameLab: TcxLabel
        Style.IsFontAssigned = True
        AnchorY = 15
      end
      inherited StepCommentLab: TcxLabel
        Style.IsFontAssigned = True
      end
    end
  end
end
