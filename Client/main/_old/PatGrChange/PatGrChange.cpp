﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "PatGrChange.h"
// #include "ShemaList.h"
// #include "SchList.h"
// #include "ExtFunc.h"
// #include "ChoiceList.h"
// #include "RegEI.h"
// #include "ImmEI.h"
// #include "Dist.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxGDIPlusClasses"
#pragma resource "*.dfm"
TPatGrChangeForm * PatGrChangeForm;
// ---------------------------------------------------------------------------
/*
 UnicodeString StepCaptions[5][6] =
 {
 {
 "Изменение параметров пациентов",
 "Всё готово для изменения параметров пациентов.",
 "Пациенты для которых будут изменены данные:",
 "Параметры, которые будут установлены:",
 "Выбор параметров пациентов, которые необходимо изменить",
 "Укажите параметры пациентов, которые необходимо изменить."
 },
 {
 "Установка временного отвода",
 "Всё готово для установки ВРЕМЕННОГО отвода.",
 "Пациенты для которых будет установлен отвод:",
 "Список отводимых инфекций:",
 "Параметры отвода",
 "Укажите параметры отвода."
 },
 {
 "Установка схем на тур",
 "Всё готово для установки схем на тур вакцинации.",
 "Пациенты для которых будут установлены схемы:",
 "Список схем которые будут установлены:",
 "Выбор схем",
 "Укажите схемы для инфекций от которых проводится туровая вакцинация."
 },
 {
 "Установка схем",
 "Всё готово для установки схем.",
 "Пациенты для которых будут установлены схемы:",
 "Список схем которые будут установлены:",
 "Выбор схем",
 "Укажите схемы для инфекций которые необходимо установить."
 },
 {
 "Перенос в архив",
 "Всё готово для переноса в архив.",
 "Пациенты которые будут перенесены в архив:",
 "",
 "",
 ""
 }
 };
 */
// ---------------------------------------------------------------------------
__fastcall TPatGrChangeForm::TPatGrChangeForm(TComponent * Owner) : TForm(Owner)
 {
  StepPC->HideTabs = true;
  // Step1ContentTS->TabVisible = false;
  // Step2ContentTS->TabVisible = false;
  // Step3ContentTS->TabVisible = false;
  // Step4ContentTS->TabVisible = false;
  // организованность
  FInCheck = false;
  // отвод
  /*
   DM->qtExec(" Select CODE, R003C From CLASS_003A ");
   TcxCheckListBoxItem * tmpItem;
   while (!DM->quFree->Eof)
   {
   tmpItem             = InfChLB->Items->Add();
   tmpItem->Text       = quFNStr(" R003C ");
   tmpItem->ItemObject = (TObject *)quFNInt(" CODE ");
   DM->quFree->Next();
   }
   DM->trFree->Commit();
   FilterCB->Properties->Items->Clear();
   FilterCB->Properties->Items->AddObject(" ", (TObject *)0);
   DM->qtExec(" Select CODE, NAME From FILTERS Where CHTYPE = 0 and Upper(MODULE_TYPE) = '" + DM->SubDir.UpperCase() + "' Order By NAME ", false);
   while (!DM->quFree->Eof)
   {
   FilterCB->Properties->Items->AddObject(quFNStr(" NAME ").c_str(), (TObject *)quFNInt(" CODE "));
   DM->quFree->Next();
   }
   DM->trFree->Commit();
   TcxTreeListNode * tmpNode;
   */
  // туровые схемы
  /*
   DM->qtExec(" Select CODE, R003C From CLASS_003A Order By R003C ");
   while (!DM->quFree->Eof)
   {
   SchInfList->AddObject((quFNStr(" R003C ") + "=-$").c_str(), (TObject *)quFNInt(" CODE "));

   tmpNode           = SchTL->Nodes->Root->AddChild();
   tmpNode->Data     = (void *)quFNInt("CODE");
   tmpNode->Texts[0] = quFNStr("R003C");
   tmpNode->Texts[1] = " ";
   tmpNode->Texts[2] = " ";

   tmpNode           = nSchTL->Nodes->Root->AddChild();
   tmpNode->Data     = (void *)quFNInt("CODE");
   tmpNode->Texts[0] = quFNStr("R003C");
   tmpNode->Texts[1] = " ";
   tmpNode->Texts[2] = " ";

   DM->quFree->Next();
   }
   DM->trFree->Commit();
   */
  // просто схемы
  // усё остальное
  // lvl 0
  Caption = "Мастер";
  /*
   switch (ActionType)
   {
   case 0:
   RegDataPanel->Parent = Step3TS;
   break;
   case 1:
   S2Panel->Parent = Step3TS;
   break;
   case 2:
   S3Panel->Parent = Step3TS;
   for (int i = 0; i < SchInfList->Count; i++)
   SchInfList->Strings[i] = GetPart1(SchInfList->Strings[i], '=') + "=-$";
   break;
   case 3:
   S4Panel->Parent = Step3TS;
   for (int i = 0; i < SchInfList->Count; i++)
   SchInfList->Strings[i] = GetPart1(SchInfList->Strings[i], '=') + "=-$";
   break;
   case 4:
   break;
   } */
  BtnClick           = true;
  StepPC->ActivePage = Step2TS;
  ActiveControl      = PatList;
  StepPCChange(StepPC);
  SetBtnEnabled();
  BtnClick = false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatGrChangeForm::CheckInput()
 {
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::SelectNextPage()
 {
  if (StepPC->ActivePage == Step3TS)
   {
    if (CheckInput())
     StepPC->SelectNextPage(true);
   }
  else
   StepPC->SelectNextPage(true);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::SelectPrevPage()
 {
  StepPC->SelectNextPage(false);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::FillReadyTS()
 {
  StepNameLab->Caption        = "1"; // [1];
  PatReadyMemoLab->Caption    = "2"; // [2];
  DetailReadyMemoLab->Caption = "3"; // [3];
  /*
   switch (ActionType)
   {
   case 0:
   DetailReadyMemo->Lines->Text = StringReplace(FTempl->GetSelectedTextValues(true), "#", "=", TReplaceFlags() << rfReplaceAll);
   break;
   case 1:
   StepCommentLab->Caption = "С " + FrDE->Text + " по " + ToDE->Text + ".";
   StepCommentLab->Caption = StepCommentLab->Caption + " Причина отвода - " + UnicodeString((TypeCB->ItemIndex == 0) ? " Карантин " : " Распоряжение администрации ");
   for (int i = 0; i < InfChLB->Items->Count; i++)
   {
   if (InfChLB->Items->Items[i]->Checked)
   {
   DetailReadyMemo->Lines->Add(InfChLB->Items->Items[i]->Text);
   }
   }
   break;
   case 2:
   for (int i = 0; i < SchInfList->Count; i++)
   {
   if (GetPart2(GetPart2(SchInfList->Strings[i], '='), '$') != " ")
   {
   DetailReadyMemo->Lines->Add(GetPart1(SchInfList->Strings[i], '=') + "->");
   DetailReadyMemo->Lines->Add(GetPart2(GetPart2(SchInfList->Strings[i], '='), '$')); //!!!
   DetailReadyMemo->Lines->Add(" ");
   }
   }
   break;
   case 3:
   for (int i = 0; i < SchInfList->Count; i++)
   {
   if (GetPart2(GetPart2(SchInfList->Strings[i], '='), '$') != " ")
   {
   DetailReadyMemo->Lines->Add(GetPart1(SchInfList->Strings[i], '=') + "->");
   DetailReadyMemo->Lines->Add(GetPart2(GetPart2(SchInfList->Strings[i], '='), '$')); //!!!
   DetailReadyMemo->Lines->Add(" ");
   }
   }
   break;
   case 4:
   break;
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::BeforeExecute()
 {
  /* TdsRegTemplateData * FNewPatData = NULL;
   if (ActionType == 0)
   FNewPatData = FTempl->CtrList->TemplateData;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::Execute(int AIdx, __int64 AUCode)
 {
  /*
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::AfterExecute()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::ExecuteFinally()
 {
  /*
   if (ActionType == 2)
   {
   //Caption = " Установка схем на тур ";
   UnicodeString msg;
   msg = "\t ВНИМАНИЕ ! ! !\n \n "
   "После окончания планирования по туру произойдёт \n"
   "автоматический возврат на планирование по календарным схемам.\n \n"
   "Перейти к установке календарных схем для данного контингента?";
   if (_MSG_QUE(msg, " Сообщение ") == ID_YES)
   {
   ActionType       = 3;
   S3Panel->Parent  = NULL;
   S4Panel->Parent  = Step3TS;
   NextBtn->Caption = "Далее >>";
   Caption          = "Установка схем";
   for (int i = 0; i < SchInfList->Count; i++)
   SchInfList->Strings[i] = GetPart1(SchInfList->Strings[i], '=') + "=-$";
   BtnClick           = true;
   StepPC->ActivePage = Step3TS;
   StepPCChange(StepPC);
   SetBtnEnabled();
   BtnClick = false;
   }
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::NextBtnClick(TObject * Sender)
 {
  if (StepPC->ActivePage == RunTS)
   {
    ModalResult = mrOk;
   }
  else if (StepPC->ActivePage != ReadyTS)
   { // Клик " Далее >> "
    BtnClick = true;
    SelectNextPage();
    PrevBtn->Enabled = true;
    if (StepPC->ActivePage == ReadyTS)
     {
      NextBtn->Caption = " Готово ";
      DetailReadyMemo->Lines->Clear();
      StepCommentLab->Caption = " ";
      FillReadyTS();
     }
   }
  else // Клик " Готово "
   {
    BtnClick = true;
    StepPC->SelectNextPage(true);
    NextBtn->Enabled          = false;
    PrevBtn->Enabled          = false;
    CancelBtn->Enabled        = false;
    RunPrBar->Properties->Max = PatList->Count;
    RunPrBar->Position        = 0;
    try
     {
      BeforeExecute();
      for (int i = 0; i < PatList->Count; i++)
       {
        RunMsgLab->Caption = PatList->Root->Items[i]->Texts[0] + " " + PatList->Root->Items[i]->Texts[1] + " " +
          PatList->Root->Items[i]->Texts[2];
        Application->ProcessMessages();
        try
         {
          Execute(i, (__int64)PatList->Root->Items[i]->Data);
         }
        catch (...)
         {
          // if (DM->trFree->Active)
          // DM->trFree->Rollback();
         }
        RunPrBar->Position++ ;
        Application->ProcessMessages();
       }
      AfterExecute();
     }
    __finally
     {
      NextBtn->Enabled   = true;
      NextBtn->Caption   = "Закрыть";
      RunPrBar->Position = 0;
      RunPrBar->Visible  = false;
      RunMsgLab->Caption = "";
      RunMsgLab->Update();
      StepNameLab->Caption = "Выполнено";
      StepNameLab->Update();
      StepCommentLab->Caption = "Для завершения нажмите кнопку \"Закрыть\"";
      StepCommentLab->Update();
      ExecuteFinally();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::PrevBtnClick(TObject * Sender)
 {
  if (StepPC->ActivePage->PageIndex > 0)
   {
    BtnClick = true;
    SelectPrevPage();
    NextBtn->Caption = " Далее >> ";
    if (StepPC->ActivePage->PageIndex == 0)
     PrevBtn->Enabled = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::StepChange()
 {
  StepNameLab->Caption    = "4"; // [4];
  StepCommentLab->Caption = "5"; // [5];
  /*
   switch (ActionType)
   {
   case 0:
   ActiveControl = FTempl->GetFirstEnabledControl();
   break;
   case 1:
   ActiveControl = FrDE;
   break;
   case 2:
   ActiveControl = SchTL;
   break;
   case 3:
   ActiveControl = nSchTL;
   break;
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::StepPCChange(TObject * Sender)
 {
  if (StepPC->ActivePage == Step2TS)
   PatList->Parent = PatLBPanel;
  else if (StepPC->ActivePage == ReadyTS)
   PatList->Parent = ReadyLabPanel;
  if (StepPC->ActivePage == Step2TS)
   {
    ActiveControl           = PatList;
    StepNameLab->Caption    = "Список пациентов";
    StepCommentLab->Caption =
      "Определите список пациентов для которых необходимо изменить параметры. Для этого воспользуйтесь кнопками \"Добавить ...\" и \"Удалить ...\".";
   }
  else if (StepPC->ActivePage == Step3TS)
   {
    StepChange();
   }
  else if (StepPC->ActivePage == RunTS)
   {
    StepNameLab->Caption    = "Выполнение";
    StepCommentLab->Caption = "";
   }
  SetBtnEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::ButtonEnabled()
 {
  //
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::SetBtnEnabled()
 {
  bool tmpVal;
  if (StepPC->ActivePage == Step2TS)
   {
    NextBtn->Enabled = PatList->Count;
   }
  else if (StepPC->ActivePage == Step3TS)
   {
    ButtonEnabled();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::SetSGSetEditText(TObject * Sender, int ACol, int ARow, const UnicodeString Value)
 {
  SetBtnEnabled();
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatGrChangeForm::CheckKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (NextBtn->Enabled)
   {
    if (Key == VK_RETURN)
     {
      if (CheckKeyDown(Sender, Key, Shift))
       Key = 0;
      NextBtnClick(NextBtn);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::CheckBtnEnabled(TObject * Sender)
 {
  SetBtnEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::FormDestroy(TObject * Sender)
 {
  // for (TAnsiStrMapList::iterator i = NewSchListMap.begin(); i != NewSchListMap.end(); i++)
  // if (i->second)
  // delete i->second;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::StepPCPageChanging(TObject * Sender, TcxTabSheet * NewPage, bool & AllowChange)
 {
  // AllowChange = BtnClick;
  // BtnClick    = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::SelFromTemplateBtnClick(TObject * Sender)
 {
  TStringList * FCodeList = new TStringList;
  TStringList * FRowData = new TStringList;
  try
   {
    DM->RegComp->ShowSelUnitList(" ", FCodeList);
    TcxTreeListNode * tmpNode;
    InProgress = true;
    Application->ProcessMessages();
    PrBar->Visible         = true;
    PrBar->Properties->Max = FCodeList->Count;
    PrBar->Position        = 0;
    PatList->BeginUpdate();
    int PatCode;
    for (int i = 0; i < FCodeList->Count; i++)
     {
      FRowData->Text = StringReplace(FCodeList->Strings[i], "##", "\n\t", TReplaceFlags() << rfReplaceAll);
      /* DM->qtExec(" Select(R001F || ' ' || R0020 || ' ' || R002F) as PAT_FIO, R0031 From " + DM->RegComp->GetUnitTabName() + " Where CODE = " + FCodeList->Strings[i]); */
      if (FRowData->Count == 7)
       {
        PatCode = FRowData->Strings[0].Trim().ToIntDef(-1);
        TListNodeMap::iterator it = CodeList.find(PatCode);
        if (it == CodeList.end())
         {
          tmpNode            = PatList->Root->AddChild();
          CodeList[PatCode]  = tmpNode;
          tmpNode->Texts[0]  = FRowData->Strings[1].Trim();
          tmpNode->Texts[1]  = FRowData->Strings[2].Trim();
          tmpNode->Texts[2]  = FRowData->Strings[3].Trim();
          tmpNode->Values[3] = TDate(FRowData->Strings[4].Trim());
          tmpNode->Values[4] = FRowData->Strings[5].Trim();
          tmpNode->Values[5] = FRowData->Strings[6].Trim();
          tmpNode->Data      = (void *)PatCode;
         }
       }
      else
       ShowMessage(FRowData->Text + "\n\n" + FCodeList->Strings[i]);
      PrBar->Position++ ;
      Application->ProcessMessages();
      if (!InProgress)
       throw EAbort(" ");
     }
    // if (DM->trFree->Active)
    // DM->trFree->Commit();
   }
  __finally
   {
    PatList->EndUpdate();
    InProgress     = false;
    PrBar->Visible = false;
    delete FCodeList;
    delete FRowData;
    ActiveControl = PatList;
    SetBtnEnabled();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::DelBtnClick(TObject * Sender)
 {
  if (PatList->SelectionCount)
   {
    int PatCode = (int)PatList->Selections[0]->Data;
    TListNodeMap::iterator it = CodeList.find(PatCode);
    if (it != CodeList.end())
     {
      PatList->Selections[0]->Delete();
      CodeList.erase(PatCode);
     }
   }
  DelBtn->Enabled = (bool)PatList->Count;
  SetBtnEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::PatListClick(TObject * Sender)
 {
  DelBtn->Enabled = (bool)PatList->Count;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatGrChangeForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (MessageBox(Handle, L"Прервать передачу данных?", L"Сообщение", MB_ICONQUESTION | MB_YESNO) == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::SetEnabled(bool AEnabled)
 {
  //
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::FProgressInit(int AMax, UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort(" ");
  PrBar->Properties->Max = AMax;
  PrBar->Position        = 0;
  PrBar->Update();
  // StatusBar->Panels->Items[1]->Text = AMsg;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::FProgressChange(int APercent, UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort(" ");
  if (APercent == -1)
   PrBar->Position++ ;
  // else if (APercent == -2)
  // StatusBar->Panels->Items[1]->Text = AMsg;
  else
   PrBar->Position = APercent;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::FProgressComplite(UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort(" ");
  PrBar->Position = 0;
  // StatusBar->Panels->Items[1]->Text = AMsg;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatGrChangeForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatGrChangeForm::FOnCtrlDataChange(TTagNode * ItTag, UnicodeString & Src,
  TkabCustomDataSource * ASource)
 {
  if (!FInCheck)
   SetBtnEnabled();
  return true;
 }
// ---------------------------------------------------------------------------
