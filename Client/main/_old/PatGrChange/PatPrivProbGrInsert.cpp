﻿// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PatPrivProbGrInsert.h"
// ---------------------------------------------------------------------------
#define _Fam        0
#define _Name       1
#define _Otch       2
#define _BirthDay   3
#define _VacType    4
#define _ReakType   5
#define _ReakVal    6
#define _ReakTypeId 7
#define _SchVal     8
// ---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxRadioGroup"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxCore"
#pragma link "dxGDIPlusClasses"
#pragma link "PatGrChange"
#pragma resource "*.dfm"
TPatPrivProbGrInsertForm * PatPrivProbGrInsertForm;

// ---------------------------------------------------------------------------
__fastcall TPatPrivProbGrInsertForm::TPatPrivProbGrInsertForm(TComponent * Owner) : TPatGrChangeForm(Owner)
 {
  MIBPCB->Properties->Items->Clear();
  TestCB->Properties->Items->Clear();

  VacVar          = new TStringList;
  ReacVar         = new TStringList;
  ExecuteDE->Date = Now();

  PrivDef = DM->RegDef->GetTagByUID("1003");
  TestDef = DM->RegDef->GetTagByUID("102f");

  TJSONObject * RetData = NULL;
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;
  try
   {
    DM->GetValById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035 order by R0040", "0", RetData);
    itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      MIBPCB->Properties->Items->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
     }

    DM->GetValById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A order by R002C", "0", RetData);
    itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      TestCB->Properties->Items->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
     }
   }
  __finally
   {
   }
  Caption = "Добавление прививок/проб для группы пациентов"; // [0];

  FInsVacDataExecutor               = new TInsertCardItem(PrivDef, DM->XMLList->GetXML("12063611-00008CD7-CD89"));
  FInsVacDataExecutor->OnGetValById = DM->GetValById;
  FInsVacDataExecutor->OnInsertData = DM->InsertData;
  FInsVacDataExecutor->InLPU        = 1;
  FInsVacDataExecutor->LPU          = DM->CardComp->LPUCode;
  FInsVacDataExecutor->BinReak      = true;
  FInsVacDataExecutor->Preload();

  FInsTestDataExecutor               = new TInsertCardItem(TestDef, DM->XMLList->GetXML("001D3500-00005882-DC28"));
  FInsTestDataExecutor->OnGetValById = DM->GetValById;
  FInsTestDataExecutor->OnInsertData = DM->InsertData;
  FInsTestDataExecutor->InLPU        = 1;
  FInsTestDataExecutor->LPU          = DM->CardComp->LPUCode;
  FInsTestDataExecutor->Preload();
  // FInsDataExecutor->Doc = 0;
  // FInsDataExecutor->MedSis = 0;
  // FInsDataExecutor->FinSrc = 0;
  // FInsDataExecutor->ExtInf = 0;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatPrivProbGrInsertForm::CheckInput()
 {
  bool RC = ((int)ExecuteDE->Date) != Cxdateutils::NullDate;
  TInsertCardItem * FInsExecutor;
  int ItemCode = 0;
  try
   {
    if (MIBPRB->Checked)
     {
      RC &= (MIBPCB->ItemIndex != -1);
     }
    else
     {
      RC &= (TestCB->ItemIndex != -1);
     }
    if (RC)
     {
      TcxTreeListNode * ndCh;
      if (MIBPRB->Checked)
       ItemCode = (int)MIBPCB->Properties->Items->Objects[MIBPCB->ItemIndex];
      else
       ItemCode = (int)TestCB->Properties->Items->Objects[TestCB->ItemIndex];
      UnicodeString FValue;

      for (int i = 0; (i < PatPrivTL->Count) && RC; i++)
       {
        ndCh = PatPrivTL->Root->Items[i];

        RC &= GetTLValue(ndCh, _VacType).Length();

        if (RC)
         {
          if (MIBPRB->Checked)
           FInsExecutor = FInsVacDataExecutor;
          else
           FInsExecutor = FInsTestDataExecutor;

          FValue = (GetTLValue(ndCh, _ReakType).Length()) ? ndCh->Texts[_ReakTypeId] : UnicodeString("0");
          RC &= FInsExecutor->CheckInput(ItemCode, FValue, GetTLValue(ndCh, _ReakVal), !MIBPRB->Checked);
         }
        Application->ProcessMessages();
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::FillReadyTS()
 {
  StepNameLab->Caption        = "Всё готово для добавления прививок/проб."; // [1];
  PatReadyMemoLab->Caption    = "Пациенты для которых будут добавлены прививки/пробы:"; // [2];
  DetailReadyMemoLab->Caption = "Прививки/пробы, которые будут добавлены:"; // [3];
  ReadyPC->ActivePage         = CustomTS;
  ReadyTL->Clear();

  PatPrivTL->BeginUpdate();
  ReadyTL->BeginUpdate();
  ReadyTL->Clear();
  try
   {
    TcxTreeListNode * patNode;
    for (int i = 0; i < PatPrivTL->Count; i++)
     {
      patNode = ReadyTL->Root->AddChild();

      patNode->Texts[_Fam]      = PatPrivTL->Root->Items[i]->Texts[_Fam];
      patNode->Texts[_Name]     = PatPrivTL->Root->Items[i]->Texts[_Name];
      patNode->Texts[_Otch]     = PatPrivTL->Root->Items[i]->Texts[_Otch];
      patNode->Texts[_BirthDay] = PatPrivTL->Root->Items[i]->Texts[_BirthDay];
      patNode->Texts[_VacType]  = PatPrivTL->Root->Items[i]->Texts[_VacType];
      patNode->Texts[_ReakType] = PatPrivTL->Root->Items[i]->Texts[_ReakType];
      patNode->Texts[_ReakVal]  = PatPrivTL->Root->Items[i]->Texts[_ReakVal];
      Application->ProcessMessages();
     }
   }
  __finally
   {
    PatPrivTL->EndUpdate();
    ReadyTL->EndUpdate();
   }
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::BeforeExecute()
 {
  // FInsDataExecutor->Doc = 0;
  // FInsDataExecutor->MedSis = 0;
  // FInsDataExecutor->FinSrc = 0;
  // FInsDataExecutor->ExtInf = 0;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::Execute(int AIdx, __int64 AUCode)
 {
  UnicodeString FRecID, FRC;
  TkabCustomDataSetRow * RecData;
  if (PatPrivTL->IsEditing)
   PatPrivTL->HideEdit();
  try
   {
    TcxTreeListNode * nd = PatPrivTL->Root->Items[AIdx];
    TDate ptbd = TDate(nd->Texts[_BirthDay]);
    UnicodeString ReakTypeId = nd->Texts[_ReakTypeId].Trim();
    if (MIBPRB->Checked)
     {
      FInsVacDataExecutor->NewRec(ptbd);
      int MIBPCode = (int)MIBPCB->Properties->Items->Objects[MIBPCB->ItemIndex];
      RecData = FInsVacDataExecutor->Data;

      RecData->Value["1024"] = ExecuteDE->Date; // date name='Дата выполнения'
      RecData->Value["1020"] = MIBPCode;        // choiceDB name='МИБП'
      RecData->Value["1021"] = FInsVacDataExecutor->GetPrivVar(UIDStr(nd->Texts[_SchVal].ToIntDef(0)));
      // text name='Вид'
      RecData->Value["1075"] = Variant::Empty(); // choiceDB name='Инфекция'
      RecData->Value["1022"] = DM->GetVacDefDoze(ExecuteDE->Date, ptbd, MIBPCode); // digit name='Доза'
      RecData->Value["1023"] = SerED->Text; // text name='Серия'
      if (!FInsVacDataExecutor->ReakIsBin(MIBPCode))
       {
        if (ReakTypeId.Length()) // задан формат реакции
         {
          RecData->Value["1025"] = 1;          // binary name='Реакция проверена'
          RecData->Value["10A8"] = ReakTypeId; // choiceDB name='Формат реакции'
          if (FInsVacDataExecutor->ReakCanVal(ReakTypeId.ToIntDef(0))) // есть значение реакции
            RecData->Value["1027"] = nd->Texts[_ReakVal]; // digit name='Значение реакции'
         }
       }
      FInsVacDataExecutor->Execute(AUCode, FRecID, FRC);
     }
    else
     {
      FInsTestDataExecutor->NewRec(ptbd);
      int TestCode = (int)TestCB->Properties->Items->Objects[TestCB->ItemIndex];
      RecData = FInsTestDataExecutor->Data;

      RecData->Value["1031"] = ExecuteDE->Date; // date name='Дата выполнения' uid='1031'
      RecData->Value["1032"] = TestCode;        // choiceDB name='Проба' uid='1032'
      // choiceDB name='Инфекция' uid='1200'
      RecData->Value["1200"] = FInsTestDataExecutor->GetTestInf(IntToStr(TestCode));
      RecData->Value["1033"] = SerED->Text; // text name='Серия препарата' uid='1033'
      RecData->Value["1034"] = 0;           // choice name='Повод' uid='1034' >> План' value='0'
      if (ReakTypeId.Length())              // задан формат реакции
       {
        RecData->Value["103B"] = 1;          // binary name='Реакция проверена' uid='103B'
        RecData->Value["10A7"] = ReakTypeId; // choiceDB name='Формат реакции' uid='10A7'
        if (FInsTestDataExecutor->ReakCanVal(ReakTypeId.ToIntDef(0))) // есть значение реакции
          RecData->Value["103F"] = nd->Texts[_ReakVal]; // digit name='Значение' uid='103F'
        // FInsDataExecutor->Data->Value["103F"] = AUCode; // digit name='Значение' uid='103F'
        // FInsDataExecutor->Data->Value["0184"] = AUCode; // digit name='Значение 2' uid='0184'
        // FInsDataExecutor->Data->Value["0186"] = AUCode; // digit name='Значение 3' uid='0186'
        // FInsDataExecutor->Data->Value["0188"] = AUCode; // digit name='Значение 4' uid='0188'
        // FInsDataExecutor->Data->Value["018A"] = AUCode; // digit name='Значение 5' uid='018A'
       }
      RecData->Value["1093"] = UIDStr(nd->Texts[_SchVal].ToIntDef(0)); // extedit name='Схема' uid='1093'
      FInsTestDataExecutor->Execute(AUCode, FRecID, FRC);
     }
   }
  catch (System::Sysutils::Exception & E)
   {
    MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
   }
  catch (...)
   {
    MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
   }
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::AfterExecute()
 {
  // if (FNewPatData)
  // delete FNewPatData;
  // FNewPatData = NULL;
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::StepChange()
 {
  StepNameLab->Caption    = "Выбор параметров пациентов, которые необходимо изменить"; // [4];
  StepCommentLab->Caption = "Укажите параметры пациентов, которые необходимо изменить."; // [5];
  PatPrivTL->Clear();
  TcxTreeListNode * patNode;
  PatPrivTL->BeginUpdate();
  try
   {
    for (int i = 0; i < PatList->Count; i++)
     {
      patNode                   = PatPrivTL->Root->AddChild();
      patNode->Texts[_Fam]      = PatList->Root->Items[i]->Texts[_Fam];
      patNode->Texts[_Name]     = PatList->Root->Items[i]->Texts[_Name];
      patNode->Texts[_Otch]     = PatList->Root->Items[i]->Texts[_Otch];
      patNode->Texts[_BirthDay] = PatList->Root->Items[i]->Texts[_BirthDay];
      Application->ProcessMessages();
     }
   }
  __finally
   {
    PatPrivTL->EndUpdate();
    if (PatPrivTL->Count)
     {
      PatPrivTL->Root->Items[0]->Focused = true;
      VacTypeCol->Focused                = true;
     }
   }
  ActiveControl = MIBPRB;
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::ButtonEnabled()
 {
  NextBtn->Enabled = CheckInput();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::ClearValues()
 {
  PatPrivTL->BeginUpdate();
  try
   {
    for (int i = 0; i < PatPrivTL->Count; i++)
     {
      PatPrivTL->Root->Items[i]->Texts[_VacType]    = "";
      PatPrivTL->Root->Items[i]->Texts[_ReakType]   = "";
      PatPrivTL->Root->Items[i]->Texts[_ReakVal]    = "";
      PatPrivTL->Root->Items[i]->Texts[_ReakTypeId] = "";
      PatPrivTL->Root->Items[i]->Texts[_SchVal]     = "";
      Application->ProcessMessages();
     }
   }
  __finally
   {
    PatPrivTL->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::MIBPRBClick(TObject * Sender)
 {
  if (MIBPRB->Checked)
   {
    MIBPCB->Enabled   = true;
    TestCB->Enabled   = false;
    TestCB->ItemIndex = -1;
   }
  else
   {
    TestCB->Enabled   = true;
    MIBPCB->Enabled   = false;
    MIBPCB->ItemIndex = -1;
   }
  ClearValues();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::FormDestroy(TObject * Sender)
 {
  delete FInsVacDataExecutor;
  delete FInsTestDataExecutor;
  delete VacVar;
  delete ReacVar;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::FillVacVar(UnicodeString ACode, TStrings * AItems, bool ACanMIBP)
 {
  if (ACanMIBP)
   FInsVacDataExecutor->FillVacVar(ACode, AItems);
  else
   FInsTestDataExecutor->FillVacVar(ACode, AItems);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::FillReacVar(int ACode, TStrings * AItems, bool ACanMIBP)
 {
  if (ACanMIBP)
   FInsVacDataExecutor->FillReacVar(AItems, ACode, 1);
  else
   FInsTestDataExecutor->FillReacVar(AItems, ACode, 2);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::MIBPCBPropertiesChange(TObject * Sender)
 {
  ClearValues();
  if (MIBPCB->ItemIndex != -1)
   {
    FillVacVar(IntToStr((int)MIBPCB->Properties->Items->Objects[MIBPCB->ItemIndex]), VacVar, true);
    FillReacVar((int)MIBPCB->Properties->Items->Objects[MIBPCB->ItemIndex], ReacVar, true);
   }
  ButtonEnabled();
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::TestCBPropertiesChange(TObject * Sender)
 {
  ClearValues();
  if (TestCB->ItemIndex != -1)
   {
    FillVacVar(IntToStr((int)TestCB->Properties->Items->Objects[TestCB->ItemIndex]), VacVar, false);
    FillReacVar((int)TestCB->Properties->Items->Objects[TestCB->ItemIndex], ReacVar, false);
   }
  ButtonEnabled();
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::ExecuteDEPropertiesChange(TObject * Sender)
 {
  ButtonEnabled();
 }

// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::SetNextValues(int AIdx, int AExtIdx)
 {
  TcxTreeListNode * tmp = GetActiveItemNode();
  if (tmp)
   {
    int Idx = tmp->Index;
    UnicodeString FValue = "";
    UnicodeString FExtValue = "";
    if (PatPrivTL->IsEditing)
     {
      FValue = VarToStr(PatPrivTL->InplaceEditor->EditingValue);
      if (AExtIdx)
       {
        TcxComboBox * CB = (TcxComboBox *)PatPrivTL->InplaceEditor;
        int ValIdx = CB->ItemIndex;
        if (ValIdx != -1)
         FExtValue = IntToStr((int)CB->Properties->Items->Objects[ValIdx]);
       }
     }
    else
     {
      FValue = tmp->Texts[AIdx];
     }
    if (AExtIdx)
     tmp->Texts[AExtIdx] = FExtValue;

    PatPrivTL->BeginUpdate();
    try
     {
      TcxTreeListNode * ptmp = tmp->Parent;

      for (int i = Idx + 1; i < ptmp->Count; i++)
       {
        if (ptmp->Items[i]->Visible)
         ptmp->Items[i]->Texts[AIdx] = FValue;
        if (AExtIdx && ptmp->Items[i]->Visible)
         ptmp->Items[i]->Texts[AExtIdx] = FExtValue;
        Application->ProcessMessages();
       }
     }
    __finally
     {
      PatPrivTL->EndUpdate();
     }
   }
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TPatPrivProbGrInsertForm::GetActiveItemNode()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (PatPrivTL->SelectionCount)
     {
      RC = PatPrivTL->Selections[0];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TPatPrivProbGrInsertForm::GetActiveItemIndex()
 {
  int RC = -1;
  TcxTreeListNode * tmpNode = GetActiveItemNode();
  try
   {
    if (tmpNode)
     {
      RC = tmpNode->Index;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::PatPrivTLEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn,
 bool & Allow)
 {
  if ((AColumn->Position->ColIndex == _ReakType) || (AColumn->Position->ColIndex == _ReakVal))
   {
    if (MIBPRB->Checked)
     {
      Allow = MIBPCB->ItemIndex != -1;
      if (Allow)
       Allow = (!FInsVacDataExecutor->ReakIsBin((int)MIBPCB->Properties->Items->Objects[MIBPCB->ItemIndex]));
     }
    else
     {
      Allow = TestCB->ItemIndex != -1;
     }

    TcxTreeListNode * curNode = GetActiveItemNode();
    if (Allow && curNode && (AColumn->Position->ColIndex == _ReakVal))
     {
      if (MIBPRB->Checked)
       Allow = FInsTestDataExecutor->ReakCanVal(curNode->Texts[_ReakTypeId].ToIntDef(0)); // есть значение реакции
      else
       Allow = FInsTestDataExecutor->ReakCanVal(curNode->Texts[_ReakTypeId].ToIntDef(0)); // есть значение реакции
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::cxTreeList1Column1PropertiesValidate(TObject * Sender, Variant & DisplayValue,
 TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::SerEDPropertiesValidate(TObject * Sender, Variant & DisplayValue,
 TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::VacTypeColPropertiesEditValueChanged(TObject * Sender)
 {
  SetNextValues(_VacType, _SchVal);
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::ReacTypeColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * FCurRow = GetActiveItemNode();
  FCurRow->Values[_ReakTypeId] = 0;
  if (FCurRow && Sender)
   {
    TcxComboBox * FCB = (TcxComboBox *)Sender;
    if (FCB->ItemIndex != -1)
     {
      FCurRow->Values[_ReakTypeId] = (int)(FCB->Properties->Items->Objects[FCB->ItemIndex]);
     }
   }
  SetNextValues(_ReakType, _ReakTypeId);
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::VacTypeColGetEditingProperties(TcxTreeListColumn * Sender,
 TcxTreeListNode * ANode, TcxCustomEditProperties *& EditProperties)
 {
  ((TcxComboBoxProperties *)EditProperties)->Items->Assign(VacVar);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::ReacTypeColGetEditingProperties(TcxTreeListColumn * Sender,
 TcxTreeListNode * ANode, TcxCustomEditProperties *& EditProperties)
 {
  ((TcxComboBoxProperties *)EditProperties)->Items->Assign(ReacVar);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatPrivProbGrInsertForm::ReacValColPropertiesChange(TObject * Sender)
 {
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
