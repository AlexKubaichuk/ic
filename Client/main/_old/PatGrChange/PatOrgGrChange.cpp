﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PatOrgGrChange.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxGDIPlusClasses"
#pragma link "PatGrChange"
#pragma resource "*.dfm"
TPatOrgGrChangeForm * PatOrgGrChangeForm;
//---------------------------------------------------------------------------
/*
 UnicodeString StepCaptions[6] =
 {
 {
 0 "Изменение параметров пациентов",
 1 "Всё готово для изменения параметров пациентов.",
 2 "Пациенты для которых будут изменены данные:",
 3 "Параметры, которые будут установлены:",
 4 "Выбор параметров пациентов, которые необходимо изменить",
 5 "Укажите параметры пациентов, которые необходимо изменить."
 },
 };
 */
__fastcall TPatOrgGrChangeForm::TPatOrgGrChangeForm(TComponent * Owner)
    : TPatGrChangeForm(Owner)
 {
  UnicodeString tmp =
      "<root>"
      "<fl ref = '0021'/>"//<choice name='&amp;Пол' uid='0021' required='1' inlist='e' minleft='90' default='2'/>
      "<separator/>"
      "<fl ref = '32B3'/>"//<choice name='&amp;Статус обслуживания' uid='32B3' required='1' inlist='l' default='0'/>
      "<separator/>"
      "<fl ref = '0136'/>"//<extedit name='&amp;Адрес' uid='0136' comment='search' inlist='e' linecount='3' showtype='1' length='40'/>
      "<fl ref = '0022'/>"//<binary name='Адрес обслуживается' uid='0022' invname='Не обслуживается'/>
      "<separator/>"
      "<fl ref = '0025'/>"//<extedit name='&amp;Организация' uid='0025' comment='search' ref='0001' inlist='e' depend='011B.0001' linecount='3' showtype='1' fieldtype='integer'/>
      "<fl ref = '00F0'/>"//<binary name='Обслуживаемая организация (своя)' uid='00F0' required='1'/>
      "<separator/>"
      "<fl ref = '0023'/>"//<choice name='Социальный статус' uid='0023' default='5'/>
      "<separator/>"
      "<fl ref = '00F1'/>"//<choiceDB name='ЛПУ' uid='00F1' ref='00C7' required='1' inlist='e' isedit='0' default='Main'/>
      "<fl ref = '00B3'/>"//<choiceDB name='&amp;Участок' uid='00B3' ref='000E' inlist='e' depend='00F1.000E'/>
      "<separator/>"
      "<fl ref = '0093'/>"//<choiceDB name='Страх. компания' uid='0093' ref='0095' inlist='e'/>
      "<separator/>"
      "<fl ref = '0086'/>"//<binary name='Часто болеющий' uid='0086' inlist='e' invname='Не часто болеющий'/>
      "<fl ref = '0088'/>"//<binary name='Риск по туберкулёзу' uid='0088' inlist='e' invname='Отсутствует риск по туберкулёзу'/>
      "<fl ref = '00A7'/>"//<binary name='Иммунодефицитное состояние' uid='00A7' inlist='e'/>
      "<separator/>"
      "<fl ref = '00C4'/>"//<binary name='Сверка выполнена' uid='00C4' inlist='e'/>
      "<fl ref = '00C5'/>"//<date name='Дата сверки' uid='00C5' required='1' inlist='e'>
      "<separator/>"
      "<fl ref = '00BF'/>"//<choiceDB name='Доп. информация' uid='00BF' ref='00BD' inlist='e' linecount='2'/>
      "<fl ref = '00C1'/>"//<text name='Примечания' uid='00C1' inlist='e' cast='no' linecount='2' length='150'/>
      "</root>";

  FTempl                             = new TdsRegTemplate(DM->RegDef);
  FTempl->TmplPanel                  = S1Panel;
  FTempl->DataProvider->OnGetValById = DM->OnGetValById;
  //FTempl->DataProvider->OnGetCompValue  = FDM->OOnGetValById;
  FTempl->DataProvider->OnGetClassXML = DM->OnGetXML;
  FTempl->OnExtBtnClick               = DM->OnExtBtnClick;
  //FTempl->OnTreeBtnClick = FOnTreeBtnClick;

  //FTempl->StyleController = DM->StyleController;
  FTempl->DataPath = DM->DataPath;
  FTempl->CreateTemplate(tmp, NULL/*ATmplData*/, S1Panel->Height / 2);

  FTempl->CtrList->OnDataChange = FOnCtrlDataChange;

  FTempl->OnKeyDown = FormKeyDown;

  S1Panel->Align = alClient;
  Caption        = "Изменение параметров пациентов"; //[0];
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::FormDestroy(TObject * Sender)
 {
  delete FTempl;
 }
//---------------------------------------------------------------------------
bool __fastcall TPatOrgGrChangeForm::CheckInput()
 {
  return FTempl->CheckInput();
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::FillReadyTS()
 {
  StepNameLab->Caption        = "Всё готово для изменения параметров пациентов."; //[1];
  PatReadyMemoLab->Caption    = "Пациенты для которых будут изменены данные:"; //[2];
  DetailReadyMemoLab->Caption = "Параметры, которые будут установлены:"; //[3];

  DetailReadyMemo->Lines->Text = StringReplace(FTempl->GetSelectedTextValues(true), "#", "=", TReplaceFlags() << rfReplaceAll);
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::BeforeExecute()
 {
  FNewPatData = FTempl->CtrList->TemplateData;
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::Execute(int AIdx, __int64 AUCode)
 {
  DM->PatGrChData(AUCode, FNewPatData);
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::AfterExecute()
 {
  if (FNewPatData)
   delete FNewPatData;
  FNewPatData = NULL;
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::StepChange()
 {
  StepNameLab->Caption    = "Выбор параметров пациентов, которые необходимо изменить"; //[4];
  StepCommentLab->Caption = "Укажите параметры пациентов, которые необходимо изменить."; //[5];
  ActiveControl           = FTempl->GetFirstEnabledControl();
 }
//---------------------------------------------------------------------------
void __fastcall TPatOrgGrChangeForm::ButtonEnabled()
 {
  FInCheck = true;
  try
   {
    NextBtn->Enabled = !FTempl->IsEmpty();
   }
  __finally
   {
    FInCheck = false;
   }
 }
//---------------------------------------------------------------------------
