﻿//---------------------------------------------------------------------------

#ifndef PatOrgGrChangeH
#define PatOrgGrChangeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxGDIPlusClasses.hpp"
#include "PatGrChange.h"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class TPatOrgGrChangeForm : public TPatGrChangeForm
{
__published:	// IDE-managed Components
 TPanel *RegDataPanel;
 TPanel *S1Panel;
 void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
  TdsRegTemplate *FTempl;
  TdsRegTemplateData * FNewPatData;

  bool __fastcall CheckInput();
//  void __fastcall SelectNextPage();
//  void __fastcall SelectPrevPage();
  void __fastcall FillReadyTS();
  void __fastcall BeforeExecute();
  void __fastcall Execute(int AIdx, __int64 AUCode);
  void __fastcall AfterExecute();
//  void __fastcall ExecuteFinally();
  void __fastcall StepChange();
  void __fastcall ButtonEnabled();
public:		// User declarations
 __fastcall TPatOrgGrChangeForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPatOrgGrChangeForm *PatOrgGrChangeForm;
//---------------------------------------------------------------------------
#endif
