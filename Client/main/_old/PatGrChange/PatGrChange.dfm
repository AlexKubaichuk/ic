object PatGrChangeForm: TPatGrChangeForm
  Left = 920
  Top = 202
  HelpContext = 4009
  BorderIcons = [biSystemMenu]
  Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1075#1088#1091#1087#1087#1072#1084#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1074
  ClientHeight = 589
  ClientWidth = 723
  Color = clGradientInactiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001001800680300001600000028000000100000002000
    0000010018000000000040030000000000000000000000000000000000000000
    0000000084848400000000000000000000000000000000000031314218103931
    31426B6B6B8C8C8C0000000000000000000000008484844A7B7B427373426B63
    00000000000029086B4A10D64200D64208D63110847B7B7B8484840000000000
    000000008484844A8C844A7B7B4273730000000000004A10D66329DE00000000
    00006329DE21085A7373730000000000000000003131314A948C4A8C844A7B7B
    0000008484847342DE6329DE4A10D60000000000003908BD7373730000000000
    0084848452525252A59C4A948C4A8C840000000000004A10D60000006329DE4A
    10D60000007B42FF2121210000000000008484845AB5AD52A59C4A948C4A8C84
    4A8C840000004A29B50000000000006329DE4A10D66329DE3131420000000000
    0084848473BDB563ADAD4A948C4A948C4A8C840000005A5A9C6329DE00000000
    00006329DE4208D65252520000000000008484847BC6BD73BDB552A5A54A948C
    42847B000000526BA5634AAD4A18CE4200D64A10D67B63B5CECECE0000000000
    000000000000006363633131311818183939396B84BD7384AD4A63944A638C4A
    5A7B000000DEDECE0000000000000000000000003939399CCECE8CA5A5637B7B
    0000007B94C66B84BD526BA54A6394425A840000000000000000000000000000
    00000000737373D6D6D69CCECE7B94940000006363637384AD526BA54A639408
    0808000000000000000000000000000000000000636363ADDEDECED6D6949C9C
    1010080000006363633131311818180000000000000000000000000000000000
    000000000000005A5A5A7373735A5A5A0000003939399CADCE8C94AD636B8400
    0000000000000000000000000000000000000000000000000000000000000000
    000000737373D6D6D69CADCE6B73940000000000000000000000000000000000
    00000000000000000000000000000000000000636363ADBDDECED6D67B949410
    1008000000000000000000000000000000000000000000000000000000000000
    0000000000005A5A5A7373735A5A5A000000000000000000000000000000C183
    0000C1010000C1310000C019000080490000806100008031000080010000E003
    0000C0070000C00F0000C11F0000E20F0000FE0F0000FE0F0000FF1F0000}
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 545
    Width = 717
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Color = clGradientInactiveCaption
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      717
      41)
    object CancelBtn: TcxButton
      Left = 637
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 0
    end
    object PrevBtn: TcxButton
      Left = 467
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight]
      Caption = '< '#1053#1072#1079#1072#1076
      Enabled = False
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 1
      OnClick = PrevBtnClick
    end
    object NextBtn: TcxButton
      Left = 546
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight]
      Caption = #1044#1072#1083#1077#1077' >'
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 2
      OnClick = NextBtnClick
    end
    object PrBar: TcxProgressBar
      Left = 8
      Top = 10
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 3
      Visible = False
      Width = 312
    end
  end
  object StepPC: TcxPageControl
    AlignWithMargins = True
    Left = 3
    Top = 102
    Width = 717
    Height = 437
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Properties.ActivePage = Step2TS
    Properties.CustomButtons.Buttons = <>
    Properties.TabSlants.Kind = skCutCorner
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    TabSlants.Kind = skCutCorner
    OnChange = StepPCChange
    OnPageChanging = StepPCPageChanging
    ClientRectBottom = 433
    ClientRectLeft = 4
    ClientRectRight = 713
    ClientRectTop = 24
    object Step2TS: TcxTabSheet
      Caption = #1064#1072#1075' 2'
      ImageIndex = 1
      object PatLBPanel: TPanel
        Left = 0
        Top = 0
        Width = 709
        Height = 409
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object PatList: TcxTreeList
          Left = 2
          Top = 42
          Width = 705
          Height = 365
          Align = alClient
          Bands = <
            item
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          Navigator.Buttons.CustomButtons = <>
          OptionsBehavior.CellHints = True
          OptionsBehavior.HeaderHints = True
          OptionsData.Editing = False
          OptionsData.Deleting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRect = False
          OptionsView.CellEndEllipsis = True
          OptionsView.Buttons = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.ExtPaintStyle = True
          OptionsView.GridLineColor = clBtnFace
          OptionsView.GridLines = tlglBoth
          OptionsView.ShowRoot = False
          ParentFont = False
          TabOrder = 0
          OnClick = PatListClick
          object cxTreeList1cxTreeListColumn1: TcxTreeListColumn
            Caption.AlignHorz = taCenter
            Caption.Text = #1060#1072#1084#1080#1083#1080#1103
            DataBinding.ValueType = 'String'
            MinWidth = 90
            Options.Sizing = False
            Options.Editing = False
            Options.Sorting = False
            Width = 90
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1cxTreeListColumn2: TcxTreeListColumn
            Caption.AlignHorz = taCenter
            Caption.Text = #1048#1084#1103
            DataBinding.ValueType = 'String'
            MinWidth = 70
            Options.Sizing = False
            Options.Editing = False
            Options.Sorting = False
            Width = 70
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object PatListColumn1: TcxTreeListColumn
            Caption.Text = #1054#1090#1095#1077#1089#1090#1074#1086
            DataBinding.ValueType = 'String'
            MinWidth = 80
            Options.Sizing = False
            Options.Sorting = False
            Width = 80
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object PatListColumn2: TcxTreeListColumn
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taCenter
            Caption.AlignHorz = taCenter
            Caption.Text = #1044#1072#1090#1072' '#1088#1086#1078#1076'.'
            DataBinding.ValueType = 'DateTime'
            MinWidth = 75
            Options.Sizing = False
            Options.Sorting = False
            Width = 75
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object PatListColumn3: TcxTreeListColumn
            Caption.Text = #1040#1076#1088#1077#1089
            DataBinding.ValueType = 'String'
            MinWidth = 200
            Width = 200
            Position.ColIndex = 4
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object PatListColumn4: TcxTreeListColumn
            Caption.Text = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
            DataBinding.ValueType = 'String'
            MinWidth = 150
            Width = 150
            Position.ColIndex = 5
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 2
          Width = 705
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object SelFromTemplateBtn: TcxButton
            Left = 6
            Top = 5
            Width = 213
            Height = 25
            Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1094#1080#1077#1085#1090#1086#1074' '#1074' '#1089#1087#1080#1089#1086#1082
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007171
              710071717100717171006D6D6D006D6D6D006D6D6D006D6D6D006D6D6D00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006D6D6D00B165
              4D008C5036008C5036008C5036008C5036006C554C006A635F00717171006D6D
              6D006D6D6D006D6D6D006D6D6D0071717100717171006D6D6D00E0664200DC51
              3100F9635000FD725C00F1684E00D7744500F1684E00D24C2700355938003559
              38002F982D00489037002F982D0025662500355938006D6D6D00D5885500F963
              5000FD7C6400FC8B6D00D5885500FFCF9C00F36D5300FD6A5700917930004DB3
              4D006AA9660063CA630052C252003CB73C00239023006D6D6D00FF00FF00E066
              4200FD846900EB805900FFCF9C00FFCF9C00EF765600FD6B580080AB670080B3
              8000FFEFDF004DB34D0075D275004FC14F00489037006D6D6D00FF00FF00FF00
              FF00B1654D004C214F002E3457007F4C6E00B1654D00917930007CD77C008FE0
              8F00FFF7E800FF00FF005BC55B0044A64400FF00FF00FF00FF00FF00FF001717
              170005070E000D296800143AA000102F9500081D6C00545454006AA966004273
              99001579BA00247DB600377D57004D4D4D00FF00FF00FF00FF003F3F3F001A1A
              1A00102C5B001A4DB3001C56BC001B51B700102F950054545400699AAE002C92
              F1003399FF003399FF002C92F1002C586F0084848400FF00FF00121212002828
              28000F2D93002774DA002671D7002671D7001E5AC0005A6064003F95C30040A6
              FF0040A6FF0040A6FF003DA2FF002385C6006D6D6D00FF00FF002C2C2C003636
              36002C586F003191F9003399FF003694F700246AD0005A606400479FD0004BB1
              FF004BB1FF004DB3FF0049AFFF002D92E60066666600FF00FF006D6D6D004A4A
              4A003F3F3F0046464600143FA4002060C600135F88007E7E7E0057A9D7004DB3
              F2004DB3F20055BBFF0051B7FF0043A8ED006D6D6D00FF00FF00FF00FF003838
              3800666666008F8F8F00A4A4A4004D4D4D0046464600FF00FF0063ABD200247D
              B60057A9D70063ABD200479FD0001372A200699AAE00FF00FF00FF00FF00FF00
              FF006D6D6D00605E5E00605E5E0066666600FF00FF00FF00FF00FF00FF003A8B
              B70084C0E400A3D0EA00479FD000699AAE00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF003A8BB7003A8BB7003A8BB700FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            TabOrder = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = SelFromTemplateBtnClick
          end
          object DelBtn: TcxButton
            Left = 231
            Top = 5
            Width = 232
            Height = 25
            Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1072#1094#1080#1077#1085#1090#1072' '#1080#1079' '#1089#1087#1080#1089#1082#1072
            Enabled = False
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            TabOrder = 1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = DelBtnClick
          end
        end
      end
    end
    object Step3TS: TcxTabSheet
      Caption = #1064#1072#1075' 3'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object ReadyTS: TcxTabSheet
      Caption = #1064#1072#1075' 4'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ReadyPC: TcxPageControl
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 703
        Height = 403
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        Properties.ActivePage = BaseTS
        Properties.CustomButtons.Buttons = <>
        Properties.HideTabs = True
        Properties.TabSlants.Kind = skCutCorner
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        TabSlants.Kind = skCutCorner
        OnChange = StepPCChange
        OnPageChanging = StepPCPageChanging
        ClientRectBottom = 399
        ClientRectLeft = 4
        ClientRectRight = 699
        ClientRectTop = 4
        object CustomTS: TcxTabSheet
          Caption = #1064#1072#1075' 3'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 24
          ExplicitWidth = 0
          ExplicitHeight = 375
        end
        object BaseTS: TcxTabSheet
          Caption = #1064#1072#1075' 2'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Step3Panel: TPanel
            Left = 0
            Top = 0
            Width = 695
            Height = 395
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object ReadyListPanel: TPanel
              Left = 0
              Top = 0
              Width = 695
              Height = 395
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object ReadyLabPanel: TPanel
                Left = 0
                Top = 0
                Width = 695
                Height = 233
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object PatReadyMemoLab: TLabel
                  AlignWithMargins = True
                  Left = 3
                  Top = 3
                  Width = 689
                  Height = 32
                  Align = alTop
                  AutoSize = False
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  Transparent = True
                  WordWrap = True
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 346
                end
              end
              object PatListPanel: TPanel
                Left = 0
                Top = 233
                Width = 695
                Height = 162
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object DetailReadyMemoLab: TLabel
                  AlignWithMargins = True
                  Left = 3
                  Top = 3
                  Width = 689
                  Height = 32
                  Align = alTop
                  AutoSize = False
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  Transparent = True
                  WordWrap = True
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 245
                end
                object DetailReadyMemo: TMemo
                  Left = 0
                  Top = 38
                  Width = 695
                  Height = 124
                  Align = alClient
                  BevelKind = bkTile
                  BorderStyle = bsNone
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  ScrollBars = ssVertical
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
    end
    object RunTS: TcxTabSheet
      Caption = 'RunTS'
      ImageIndex = 4
      DesignSize = (
        709
        409)
      object RunPrBar: TcxProgressBar
        Left = 100
        Top = 223
        Anchors = [akLeft, akTop, akRight]
        Properties.PeakValue = 35.000000000000000000
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        Width = 526
      end
      object RunMsgLab: TcxLabel
        Left = 82
        Top = 112
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -19
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        Properties.LabelStyle = cxlsRaised
        Properties.WordWrap = True
        Transparent = True
        Height = 41
        Width = 562
      end
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 717
    Height = 93
    Align = alTop
    BevelOuter = bvNone
    Color = clGradientInactiveCaption
    ParentBackground = False
    TabOrder = 2
    object Image1: TImage
      AlignWithMargins = True
      Left = 650
      Top = 3
      Width = 64
      Height = 64
      Align = alRight
      Constraints.MaxHeight = 64
      Constraints.MaxWidth = 64
      Constraints.MinHeight = 64
      Constraints.MinWidth = 64
      ParentShowHint = False
      Picture.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000040
        000000400806000000AA6971DE0000000467414D410000B18F0BFC6105000000
        206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
        98000017709CBA513C00001AB949444154785EED5B075855D7B23E87628C2D89
        892DC6DE0BF6AEA08228A0A8540101A94A2FD27BEFBD833401E9485504A48914
        05144101D158B0618F255163C9FFD69C1C73DFFBEE7BEF26D1FBEE8BDF1DBFF5
        9DCD76ADBDF7CC9A35B3D6CC3F9C7FD35F98562E5CCA559355E4CE9C39937FE7
        13A74973272C9D3567D6B2592B662CD1DAAB2524B34A4268F982C55C575757EE
        902143F8BD3E41121E2730728AC748A3453ED3334522265E5D1FB5E4817C9264
        516094ADC93CB1699B04070B0EE677FD3469F6ECD902E23E2B83E7A68CBD363B
        73CCBB69874742E4F0F8D74BCB27BF599E35FDE60ADBF90EABB7AED8F6DDA4F1
        13385CF6EF53A3CD9292822BC5967F37F4BBCF978E97FEC673DEC1B16F16954E
        78BDB2721A3634CDC786DAF92FE54AC4AF2E339B9725308AF32D7FD8A7416A1A
        7B38C2C2C29CA1C386F1EF305BB0FE5BCD79B6937397944CC2A6A685D8D6BA12
        AA9724B0FDA418A6388E8D1F3C4F680DBFEB5F9F2424B77054357504C4374B0B
        F06F71B85C2EEF7AA1EBC2739275AB5EEDEC5A03B51E09685D9181EA05F1FBD3
        5C4667718438826C31FC36E62F4D0202FF3D1F5F2C1F2936336F6E82DCA555D0
        B8B8057AD76461F148193B9AD6DC9FAC3BDA85FB15E76B7ED74F87B802829C71
        126AA406BCBF855C04978836CCAFDA7D5902063777C2FC9E32CCEE2ABD93A95F
        F164E8EA41620243B99F9E7F5CB1DB9CBB7AF3569E5A7C317BF8C8AD41125A8A
        E7D6C164400116F777C1E6B13A74AF6DC3F2E8991943960BCDE20DFABFA4670F
        CE4CBCDF5FB1E9F6A57CBB4B6763837ADA23BDFB3A52B42E9E39B0E0CE95E31F
        ECB3C77D3B9E9B9D922CB064D122CE98D1A339D2B232B335B3E5B2F52E6F83F5
        63352680DDB07AA80A95EE8D18AB375C49E81BEE17FCA1FF5C7AFDF35D91FBD7
        F3DB067A0FA2BF331697DAC3D179C21DA7CA2D5193AF836387D4703C7FEFBDE6
        4A87F4EEB6E839FC617F8A664E9BCA91DAB285B340640167EACCA9830DFDF7AE
        DEDD2909EB476AB0FD419D27089DAB32589638BD70F06AC1D9FC61FF3C7AF3F2
        FE9AC7378FE2D1B532DCBB9C875B3DA9B8D21183DE538138DFE885B3B50E3855
        6189BA425D1C49574049CA7694671A56759E2E5FCC7FC49F265F5FDF41817181
        A32DCA75638CEFC9C3EE8906F63F5481F1ED9DD8D2B2E4972F9506EF64067138
        AFB32087347008F30F1FCF43BC7EF964C48DEE832F1E5C2DC3405F1E6E5C4843
        FFF9445C3D1789CBED41E83CE98996725B54E719A338490399110A08B3178589
        C21438196D8EE43F86471919E9930302020C22222252D91EBF5A5F5FFFA4B3B3
        739583834382939393869F9FDFDF59F645CB1672E3726247987A1BCC31B8BBE3
        99CD93DDCC162833A3288B1D6D2BDF4DF51D552CBC96BB56682677BBD06AAEF1
        504921CF411B0534B8DF7046F01FF161D4561FE57CA93D86317D087DEDA18CE9
        30B4D7B8A03C6B1FD2C2E410EE24010FA395B0D75E042BF585D8AF2602ED2DDF
        C1C75AB982FF084E7171F1D6B0B0B026474747D8D8D8C0D4D414868686BCA6A1
        A1014545456CDBB60D626262F0F7F7F7E8ECECFC2F3368E5B97FACAAB3E277D2
        39ABC2AC9FA9C29419449D6B5250ED13C38AD2A9CF26B98EAC9FE4FDE5E939B9
        A39E4C4C1C3E30DC4AD84A60CE47F01077EF9CFBAA285109BD2D316CBD7BA3F9
        9835F262951164BB0A6E860BE16CB018C1D6E248F55142BABF06C26C65B14F61
        2EF6A9AD6B38DFDD31E8D193579F4545C534787878809A9B9B1B1C1D9D784230
        3131818181017475757942505656E60961C68C19888D8DF5A4F70F1F396CC436
        95AD73C39243D74978AC7358913CA3D3FCBED25BC3DB3BA0C17687F25DABB1B9
        65C13BB1FA99AFD7344CC6FCC3A3F06DF4B03383560A2CE331F0A1D4DB91AB53
        95B50765297B901D238FE4605904DA8922CA4D1239917B70FE4810061AD872A8
        88C5992C4F1C0936408ABB4E0F7F38C7C73F2CD7C5CD1321E191080A09454040
        103CBD7CE1E8E40253734BE8EDDB072D6D5DA8ABFF2A00393939AC5DBB161B36
        6CB827AFBD537777B492BB5EC5AE9C4DC5CB6E4995ADE837B9A7F0B3F90345E8
        F76F856AEF7A6C3FBB1C9B9B4520563F1DCB2B266046D6C8FB5FB80EDA2F3091
        FB71D4BFAED4B6B626C70009019288F792446F7534AEB7E7E26E6F39DEDC69C6
        BBAB7578DC9A8BEF9900CEE7FBA026CA046551F6BD973A4F0BF21FC1B1B3347A
        636C6400737373D64C616A6208837D7AD0D5D2C26E754DA8ECD6808AAA1A9494
        94202B2B0B7171719E166819698668152A1C51B820FAC2F6F9EE5F1C7ED284F5
        1335A6FEF2D0BEB2198AE7D7626BDB628837CEC1DADAA9585CF6EDBBB121430B
        8434B88338C2FC977F085DBF5233B2245905C7D27411EEBA1E39119A787BB906
        6FEF9FC1DBA717F0EA411B5E5CADC6F38E42DCAA4D404F71204E1D7440A6A756
        BF938ED420FE63386732ECD09AE585D66C3F346778A221C519D5F1D628093644
        A213F3E9FA0AD829BF1352D2DBB071A338D3803598356B169881CC9ABF7CEEA2
        E9CE638B944F6FBC6F3AA0F8CEEA910A0C6F6E877A9F38E43A5642EAD402AC3F
        3113CB8F4EC0F48C91AF86ED11DECB1DCEF98CFFEA0FA3AE33D9EB0A62155010
        A782449F9D68CC76C3F3AE2236F34DF8F979275E3F398717FDB578D6918FDB35
        F1E82B0D42FB213764B8AAF79B494D17E23F86D39264891B9591787AEA20EE9F
        3C806B9551E82D0DC1996C4F34279AE348803A4CD5A5B06AAD18D6AC1185A8E8
        3A2C59B2049B366D6A9696961E3FE83BC139E315BFB6DA50B6F096E96379ECBB
        B10D2A3D62903DB30C92CDF3205A371D0B0AC7BE181B313457701D7702FFB51F
        4E17BB8E6AE5846D437AC80E6404A9E37A65127E3C5B8057D76AF1EA7E0B6B4D
        78F17D059E9DCDC69DBA04F49585A035DD0979814657421D0D84CB127D785A70
        22D106374FA6E18733F91868CCC0E5CA38741586A2F1A02B8E861BA332581B51
        1672909214C74A26003131518888889061ECB1B6B69ECCFB182E67D0E71B848D
        9645CFA8DED6B4FCB9D2C55FD57FE3C9D958533305D352BEBA3ACC4658863B9A
        D7FBE3505591A3595E94229202B622D2591ABDC51178DA9A8D9FBA4BF0E27239
        63BE9C5D17E3495B266E9300980634255AA13ACDE7128DEF6BAB196FB869A240
        75AC25EE3665E0C1A92CDC3A9186CBCC5E7414F8A33ED116E5E146280FD2469C
        B53C64A524B06495285B02A2983D7B3679876EE6397E150023AE1067C8A06F85
        44C6988CC8DED422F27CF36991B7624CFD17148D7BFB8DFBE71942221F71F689
        4A328D4CB3421598FA4BC3DB622D2AE2F6E35E5D127E683E88E76773F0FC5C1E
        9EB567E1C1C964F41F8FF9D506A4DAE37088E98BC2F470D9538551DF32017C76
        3CCE1AD76B1271A33E0557AB0EA0BB3818AD192EA88BDF8F236186381AA48568
        2B796CDD2C81E56BC4B05A9469C08245D8B2794BA3BCBCFC38FEE7FC46420BB9
        ABC7980F0F9C1D37E6C2CABAC96FA7A58EEC1492E72E6186EF37C3FB51A8E76C
        9966A2870413800CBC2D5723C06A133AD9CCDD6D48C2FDC6143C6282B8CF98BF
        551B8F4B4742D155E08BF64C57C4D9EC8485D6C6BAB39519EB9800461C8BB144
        4F59385BF7A1B870D81FED591E684CB1474DAC0533847B51E8A38E6427B6B951
        9580CDEE8DD057D8809933A743534B3B9169C0DFDB73618E1005433E9311D837
        C673E889AFAC07DB09CE63A6EF635373FD8165514E6248F49641A0B518ECF72D
        C3916873DC64B37DBD2A1A37AA6379BF578E45A0BBC81F1D39CCA8A53923C444
        121AB2F3BF2F4A0BB265021856E8A78D56661CDB98704EB3996F4973422D9BFD
        6311C628F4D741A6BB1A8E841AE080C566A49889C1537D2516CF9F091DFD7DBB
        F89FF2DF127714E76B8149DCB102733843B85FF26F7E4CEAED69100A73DB7ACF
        DF7C0542EDC4E166B40A07DC94D1571CCAEC41202E9606A3B7248837F36773DC
        D176C815C7E3AD60AFB11C6A52B306E2FDCD8399000667382BA0FE8035C81836
        24D9A226CE121591262809D247AEB726D29D9550CA365091A65B10B5772DF66C
        988C8DA26B6F5958D98DE77FCABF8E9AEA72752CD526C3CB78257CCCD6C2C764
        0363C21E5D79DECC8D7930C63D7933DBCCFC3F3198E6AA0ABDAD539906CCBB7D
        30C2D99409809B60298DCA285354459BF17E8FB2755F1CA8877C9F3DCC65AAE0
        A0833C0AFCF5106D2E0577E50590583C91768996FC4FF8D7535A98699AF5EE79
        AF1DB597C2466331A26DB6A129D9012793ED78ED44A235EAD9C6A622D26CC0CF
        60E37355B191B0D2D9D4DEDA50C6734A91861B90EFAB85A2005D9ECA17B0EB5C
        2F0D1C62CCA7D8CB21D94E1E599E5AF0D35C09A36D222F0CF475AD782FFEFF44
        89A1E68EFABBE6C3504504EEFAAB99EA6AF066B22C641F4A1863C7C24CAF371F
        8E963E1866E9A62733ED45417AB4067F28275877F5EB838E8AC8626B3DDB6337
        8FF134272524D9EE44AC8514D29C9571D06917CC364F42B8BD56097FD8FF1F4A
        3DE9332EB6D4D742DD5EFCA5A6C91AA4303B90E9BC0BA9760A48614CA4DAEC40
        BCBBEACD900C57E59058AB7D11C1D6C6FCA13C0AD75B7734C24802C9F6F23CA6
        13A9FFFE6D88666B3EC2681372BCF620406F032CA5A6C06AD7BA964477DD7FFD
        DA7F4F959DA972C9F5F6571CB3E561CA4E835A5E52C80CD54355A0210EB9B199
        745040269B4D5F7B7696375C80AD664B5F1AFBAA27B824ED5BC07F04E7A08DCA
        761FF5C5882223C75AA4C96684196E4280AE281299FAC759ED80F58ED970929B
        0D8DF5931E6E5931FD83C2691F8D2ABA92F6C7D79A21AEDE00B1B516F0C9D785
        69C44E8485ECC6C9707366C87451C00C59A1F71E4431F5DE652F89ED16CB2063
        381FDABEEBE0936DE86BE96ACC0B6CF8ED5EDC1DA22F8AE0BDEB11A827865043
        71C45B6F67CCEF849D9C0898B18493C23CB8AB2C82DC76695DDE07FC8DDEE703
        E9F77DFBE7527577866B4C8D09B25BDC91D2608F845A2B441F378367DE1EB847
        A9A236D21C35E1A6286736E058B03E0E786B41CB590EAAF6EBA1E9BC016691D2
        B03D28832DC60B2A468F1BFAB9E69AA953BC55E63D75919F095FAD9588B7DA8E
        A0BD1B61273F1F468C790B99E970DBB50091FAAB60A8A73E307B85B8E38C456B
        FEF352A05D1E9D2D6863448DFEA603D7C78BFDBDA7FE8717D724D65B23AFD50F
        59A7DC91DC6087843A2B5E0BAF32865BBA06F2A30CD0C2B4A03A541F554C00F1
        1EBAD073518686AB048C8364E090AA0C8F5C55D867EEC49C4D6379364165DDB4
        D53EDA1B7E76525E04E75D8BD8CCCF87E9962930979E0647F905F0559D075FED
        D5B074F581515819769907B6CD5D2AA6C6867EC51A250F29C4F5FE97829F74EC
        25A17C3C215C1A681F92D6E87A3BE7B40F0ADAFC90D6E8840327AC79CC1F6042
        89ABDF8FC0230688C9324143710C8E6507A024DD1F3151AEB00CD0C4FE989D70
        4A538167AE3ADCB39800D2A420653ABD67D2D255DBB5DCF202E30A4EBCF33755
        80ABFC743828883021CC83BDA2083C7689C077D70C3858B265165604C7B43684
        55DC40E891EFA1661D52387AE2CC15ECF328FE4F5BDEF7BF2488CF59234DF838
        CBE2E8B9A43D09D51628ED08E7A97F52BD2DE26BF7FFA601D4222A8D1054C14E
        7197AB707AE012EAAE9C41DEC91204A49B22F8A836FC8B75E053A005D78C1D30
        8BD900D34417C434DC43413F10577D1D7E260AF0DB35876D7957C04D65295CD8
        DAF7DA3901EE86DBE110570CAFE2EF11C2180F28E94344D50DC4373E634229EF
        9F346F9522FBC491ACD1C6F7FDEF50D64813FEB816D0FE99F6D1B49FA67D35DD
        CB3AE97334ABC90B856CF6D34F3A21A1FE6F8C9306902D88A8304660E91EA49F
        0AC5153CC5E5B78F5077B10591B93688A9334050A91E13801A6C92656095EA8B
        A8BA6728EC05F2BB7F815F5211F62B2E838FE254782ACF85A3EC1478294E43B0
        B51AFCD22A105C710791C76F22B4FC7B0497F4C2AFA0135EB99DF03F7A0B16B1
        F5AF44D6EE70639FF90D6BC4FCFB5FD2843F2E003A41D1498A4E5474B2624A24
        1056BAF75E4EB30F329B5C18D3D6BCD927C679CC332144579921E29809028BD9
        4C176AA3F14613AEFFFC12C73B9A1096618DE8BA7D0828D6824B962C9B795378
        165E41E629A0A8EB25723B9EC3F7401EDC2CB510632D073FADB5CC2DCA203BC1
        1F8955BD886C788AB8BA7BEC1DFD88ACB88AE86397117EA417FE873BE196D906
        8F823E5827B662C6920DFBD9E7D3CC53A3A5F05BF8EDF7133B3BD3199ACED274
        A6A6B3359DB1ADE214907E8231CF964164A531A22A4D794C538B396ECE983766
        EBD2800940071E396C43D41C8DEBCFDFA2BCA51101A99608ABD26533A60C8B44
        599824A723E4E88FC86C798882CE9F90D17C1B4171875050548E73BD57515B5D
        858EAE6E1CEB798EA8934F99F60C3021DF61CBEE16D21A6E23EDC40D24D67C8F
        C8A33DF0C93B0BC7942638677543C7AFE8E537E3A7AFE573F2E788A2271445A1
        680A455528BA4251968DBE0BE095AE8780421DA6CAFA082EDDCB63F87D0B29DB
        C798D783EF61E60EB3941071C4065DB7EEA1B8BA01FE29A6082853871D737FFA
        D1AAB04C694158F943A43266B2DA9F23B9BA0F01E1B1A86BECC08F007E78079C
        1900A26BEF20ACB21F89270670B0F91E724EDF4741EB3DE49FBA83AC93FD48A8
        BA84E0E24E38A7B5C02EA99179961EACD8AA13C767E58F13C5CD287E4671348A
        A7515C8DE26B1467DBD1B8123B525643D1632D3C7334E17798545D070145BABC
        46D774CF2B57132E190ACCDA6BA0AAA3112515CDF049DA07CF2205982748403B
        6C2FDB075C44E8D19BCCF0F523B9F909E28E74C03F380C754D9D78C90470FB31
        90D97807BE2597115F771BA9CDF791D3F618F9ED8F9807BA8BC2D37790D77C03
        C94C0BC28ABBD8FB4EC12EB9018E87CE434AC7BD4B5050F8EFA245BF8B28724A
        11548AA452449522AB1461A5482B455C29F2BAA57429E49910CC226419B31AF0
        CED3E435BAF6CC2117B71B0E697270489743564D3A8A8A4FC2ED8026EC32A4A0
        1FBE015A418EECFF99212B651F7FF40A62D9DA8E2C68426858145ADA2FE1C7B7
        4043F70F082EEB4348F95524D60F30A3FA18D96D4F90D3FA08D9A70790DB740B
        598D6C1954B3E7F004D0029BA413EC9D1D50B14F7E23FCD9E02D8C1D32DEBF3F
        0C4E31738A9D530C9D62E91453A7D83AC5D829D64E31778ABD9BDE51846AB338
        D6FBCD8759B82C1C5294E0CAFCBA5B960A3348BB988B53669B1DB68D3DB803D1
        C5FE48CF2E8363A212CC0E88624FF07AEC090A8773FA55B65C2E22A8EC227367
        B7109ADB80A898449CBF7007F79E01197537E059D0CD6CCD756664EFF20490D9
        FA03D29B1FB2FDC71D6434DC405AFD35C455F621B0F01C9C329A619D50CFF608
        AD3008ADC417A3C693319CCFDA581E73FF90D80692B226943DA12C0A655328AB
        42EA4F5916CAB650D685B22F9485B179A606C54E51E867CBBD33F4DFCED44F1E
        B629726C1DCAC1367907EC5377C0294B0E6125F6C8284B8703B3FC2689EBA015
        2A0183A80C38A55F64B6A28BD9930B083E72154199B5883B700817FB1EE2E26D
        20BCB40FDE877B9881BDCE5BFFA94D0FD83278C8B6DE77915C7F1BC975FD48AA
        B9C28C207387879900D25B9800EAE078B00516F14D982A229AC2B85AC71A6169
        FFB127A07C19E5CD287F467934CAA7515E8DF26B9467A37C1BE5DD8CEFCABF96
        CC5FFA7063F6929F5412A41EEFF2DCF24AC5663DF6FA49C23088DA1618046DC2
        DE4009E8F9AD874910134CB036146C17436CF7D4274BA567F46BBA27BC71CB3C
        FFC623E71C5B3A9DF02DEE83DFA13AA4A41FC6A5BE2768EE7D09EFFC2EF8335F
        1F55C966B9E6269218E324880375B79050C38CDFF1AB88ABB8C45C6137CF0B38
        A436829DCEE098DAC4EC4B3B16AD573AC1D892616D216BFF3817481953CA9C52
        069532A99451A5CC2A655829D34A1957CABC52069632B16EC1AE12F396CC95FE
        ECF341B2C3BF1CBCEB8B5143F78D1C3BD4F4EBB1C3CCBF1A3BD49C5D5B7C3566
        A825BBB6FC72F410A3215F0A6B0A0A73D6B357AD5BB251B9DC32BAF2866B66FB
        0BF7EC8E771E794C104C0019D9C7D0D7F71C656D0F994B3B83C0921EE65AAF30
        17CB18668630BEF63662D9754CE555C4565D46D4B18BCC8E74C133BB9D670049
        004EA9CD7038740E4B37AB77B07751E074096BFF3B4C8672E59433A7DC39E5D0
        29974E3975CAAD538E9D72ED9473A7DC3BE5E029174F3979FEF03F4C82829FAD
        53320BEC6542B86E9FDAF8D4F550EB1BD7D41A6415D4A0ABEB0724B3CD8E637A
        2B829806841DBDFCEBC6872D056A741DC1EE8531DB1152DA0DDFFC0E9E07B049
        3CF1AB06A434C329B3134BB76850169A04F08F9700A125083541E8094251109A
        82501584AE209405A12D087541E80B4261101A83501984CEE03FE20FD3678387
        C8882B19375BC7D70ED824353C76CD3A8D9CC22634331F1F5CD4C3ACF9A95FED
        43E9459E10C2D9F6971A5D87B07B0185C47C273CB2DAD9BA6F86155BFFFB1399
        00D24E31EDE97AB7525AE7067B8D246BBF3315FE2B6E66081F47C3215C0DE16B
        086743781BC2DD10FE86703884C7215C0EE173A8EF07D0F4093317796F37F2BF
        669D5887CAE65B68BDF082ED272EFEE2927DF617EFBC73EF028ABADF0595F4BC
        0B29EDFB2594B94512480013906F4117BC72D8EC1F6A65EBBFF917DBE4A65F6C
        539A7E714C3BFDD632E1C4BB497396E7B0E77F187698105684B422C41521AF78
        08AC476A20441621B308A145482D426C1172EB4F928080D0A025A326CEF03236
        77EDCB3D76093EB99D4FAD62AA1E3924D63C77C968FEC92BABED996FFEB96781
        45DD3FF91776BFF43B7CE195576EC7CFEC0CF0C63EA5E99D5542ED1BCBD8EA17
        3609F53F1A0617BD5D20B6A34E4040601EFFF97F9E086347583BC2DC11F68E30
        782408C2E411368F307A84D523CC1E61F708C3C71FFA674850585070FE986F27
        1ACF59B12967BBBE5BB7BE77DA3DB3B0A2EB969165172C234ACFDAC557F63824
        D55E7348AABBE9C0FCA16D42ED0393B092277B5C935FEC30F4B8B750745BC988
        AFC76AB3677DBC3C30A12D097549E84B4261121A93509984CE249426A135A91F
        A13709C5C91BF4E1F485A090F09CCF877D2139E2EB31BBBF193FC562DC94D92E
        13668AF84D99B33468D2ACC501E3A6CEF518F5DD54EB2F478DD3F97CC4573242
        8306CF1F3E62E4C7074512DE9670B784BF251C2EE17109974BF85CC2E9125E97
        D791CBE5E17809CFFB4F241230D9AA8F25E8DF4784BC26043621B109914DC86C
        426813529B10DB84DCE677FD2FF43F21BDFF7A44D87B218E2061F109934FD87C
        C2E813569F30FB84DDE775E363F98908E34F587FC2FC7F3244D518549541D519
        54A541D51A54B541D51B54C541D51CFCAEBC2A0FAAF6A0AA8F4F8AA82E87EA73
        A84E87EA75A86E87EA77A88E87EA79A8AE87EA7BA8CE87EA7DF8C33E21E272B8
        54A145955A54B145955B54C145955C54D145955D54E145955E54F1C51FF56912
        D5EC51ED1ED5F0512D1FD5F4516D1FD5F851AD1FD5FCF1BB7E7A44D59A54B549
        D59B54C549D59C54D549D59D54E5C9EFF66913D5ED52FD2ED5F1F26FFD9BFE4D
        7F19E270FE03421F2EBD4F455EC70000000049454E44AE426082}
      Proportional = True
      ShowHint = False
      Transparent = True
      ExplicitLeft = 633
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 647
      Height = 93
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object StepNameLab: TcxLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Align = alTop
        AutoSize = False
        ParentColor = False
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        Height = 23
        Width = 641
        AnchorY = 15
      end
      object StepCommentLab: TcxLabel
        AlignWithMargins = True
        Left = 3
        Top = 32
        Align = alClient
        AutoSize = False
        ParentColor = False
        ParentFont = False
        Style.Color = clWindow
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.WordWrap = True
        Transparent = True
        Height = 58
        Width = 641
      end
    end
  end
end
