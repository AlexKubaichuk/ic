﻿//---------------------------------------------------------------------------

#ifndef DBSelectH
#define DBSelectH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Vcl.ExtCtrls.hpp>
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class TDBSelectUnit : public TForm
{
__published:	// IDE-managed Components
 TPanel *Panel1;
 TButton *ApplyBtn;
 TButton *CancelBtn;
 TLabel *Label1;
 TcxComboBox *DBNameCB;
 void __fastcall DBNameCBPropertiesChange(TObject *Sender);
 void __fastcall ApplyBtnClick(TObject *Sender);
 void __fastcall DBNameCBKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);

private:	// User declarations
  typedef map<int, UnicodeString> TIntStrMap;
  TIntStrMap FDBCodes;
  UnicodeString FDBId;
public:		// User declarations
  __fastcall TDBSelectUnit(TComponent* Owner, TTagNode * ADBDef);
  __property UnicodeString DBId = {read = FDBId};
};
//---------------------------------------------------------------------------
extern PACKAGE TDBSelectUnit *DBSelectUnit;
//---------------------------------------------------------------------------
#endif
