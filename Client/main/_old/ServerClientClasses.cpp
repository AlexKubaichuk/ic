﻿//
// Created by the DataSnap proxy generator.
// 21.11.2018 23:23:46
//

#include "ServerClientClasses.h"

  struct TDSRestParameterMetaData TdsICClass_AppOptLoadXML[] =
  {
    {"AOptNode", 1, 37, "TTagNode"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetDefXML[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetClassXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetClassZIPXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetClassZIPXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetCount[] =
  {
    {"AId", 1, 26, "String"},
    {"AFilterParam", 1, 26, "String"},
    {"AExtCount", 3, 26, "String"},
    {"", 4, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetIdList[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetIdList_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetValById[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetValById_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetValById10[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetValById10_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_Find[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_Find_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindEx[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindEx_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_InsertData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_EditData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_DeleteData[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetF63[] =
  {
    {"ARef", 1, 26, "String"},
    {"APrintParams", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetVacDefDoze[] =
  {
    {"APrivDate", 1, 11, "TDateTime"},
    {"APatBirthday", 1, 11, "TDateTime"},
    {"AVacCode", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_CreateUnitPlan[] =
  {
    {"APtId", 1, 18, "Int64"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_DeletePlan[] =
  {
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_DeletePatientPlan[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_StartCreatePlan[] =
  {
    {"AVal", 1, 37, "TJSONObject"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_StopCreatePlan[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_CheckCreatePlanProgress[] =
  {
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetPatPlanOptions[] =
  {
    {"APatCode", 1, 18, "Int64"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetPrivVar[] =
  {
    {"APrivCode", 1, 18, "Int64"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetOtvodInf[] =
  {
    {"AOtvodCode", 1, 18, "Int64"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetPatPlanOptions[] =
  {
    {"APatCode", 1, 18, "Int64"},
    {"AData", 1, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_PrintPlan[] =
  {
    {"ACode", 1, 18, "Int64"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetPlanPrintFormats[] =
  {
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetPlanPrintFormats_Cache[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetAppOptData[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetAppOptData[] =
  {
    {"ARef", 1, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetMainLPU[] =
  {
    {"ALPUCode", 1, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindMKB[] =
  {
    {"AStr", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindMKB_Cache[] =
  {
    {"AStr", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_MKBCodeToText[] =
  {
    {"ACode", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetPlanTemplateDef[] =
  {
    {"AType", 1, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetPlanOpt[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetPlanOpt[] =
  {
    {"AOpt", 1, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_UploadData[] =
  {
    {"ADataPart", 1, 26, "String"},
    {"AType", 1, 6, "Integer"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsICClass_GetCliOptData[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetCliOptData[] =
  {
    {"ARef", 1, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_SetUpdateSearch[] =
  {
    {"AVal", 1, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsICClass_PatGrChData[] =
  {
    {"APtId", 1, 18, "Int64"},
    {"AData", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindAddr[] =
  {
    {"AStr", 1, 26, "String"},
    {"ADefAddr", 1, 26, "String"},
    {"AParams", 1, 6, "Integer"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindAddr_Cache[] =
  {
    {"AStr", 1, 26, "String"},
    {"ADefAddr", 1, 26, "String"},
    {"AParams", 1, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_AddrCodeToText[] =
  {
    {"ACode", 1, 26, "String"},
    {"AParams", 1, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_LPUCode[] =
  {
    {"", 4, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindOrg[] =
  {
    {"AStr", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsICClass_FindOrg_Cache[] =
  {
    {"AStr", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsICClass_OrgCodeToText[] =
  {
    {"ACode", 1, 26, "String"},
    {"ASetting", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetDefXML[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetClassXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetClassZIPXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetCount[] =
  {
    {"AId", 1, 26, "String"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetIdList[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetIdList_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetValById[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetValById_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetValById10[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetValById10_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_Find[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsDocClass_Find_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_ImportData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_InsertData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_EditData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_DeleteData[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_SetTextData[] =
  {
    {"AId", 1, 26, "String"},
    {"AFlId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"AVal", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetTextData[] =
  {
    {"AId", 1, 26, "String"},
    {"AFlId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"AMsg", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetSpecById[] =
  {
    {"AId", 1, 26, "String"},
    {"ASpecType", 3, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetDocById[] =
  {
    {"AId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CreateDocPreview[] =
  {
    {"AId", 1, 26, "String"},
    {"Al", 3, 6, "Integer"},
    {"At", 3, 6, "Integer"},
    {"Ar", 3, 6, "Integer"},
    {"Ab", 3, 6, "Integer"},
    {"Aps", 3, 7, "Double"},
    {"Ao", 3, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_SaveDoc[] =
  {
    {"AId", 1, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CheckByPeriod[] =
  {
    {"FCreateByType", 1, 4, "Boolean"},
    {"ACode", 1, 26, "String"},
    {"ASpecGUI", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_DocListClearByDate[] =
  {
    {"ADate", 1, 11, "TDateTime"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetDocCountBySpec[] =
  {
    {"ASpecOWNER", 1, 26, "String"},
    {"ASpecGUI", 1, 26, "String"},
    {"ARCMsg", 3, 26, "String"},
    {"", 4, 6, "Integer"}
  };

  struct TDSRestParameterMetaData TdsDocClass_SetMainLPU[] =
  {
    {"ALPUCode", 1, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetMainLPUParam[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_MainOwnerCode[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_IsMainOwnerCode[] =
  {
    {"AOwnerCode", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CopySpec[] =
  {
    {"AName", 1, 26, "String"},
    {"ANewGUI", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CheckSpecExists[] =
  {
    {"AName", 1, 26, "String"},
    {"AGUI", 3, 26, "String"},
    {"AIsEdit", 1, 4, "Boolean"},
    {"", 4, 6, "Integer"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CheckSpecExistsByCode[] =
  {
    {"AGUI", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CheckDocCreateType[] =
  {
    {"ASpecGUI", 1, 26, "String"},
    {"ACreateType", 3, 6, "Integer"},
    {"ADefCreateType", 1, 6, "Integer"},
    {"AGetCreateType", 3, 6, "Integer"},
    {"AGetPeriod", 3, 6, "Integer"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsDocClass_DocCreateCheckExist[] =
  {
    {"ASpecGUI", 1, 26, "String"},
    {"AAuthor", 1, 26, "String"},
    {"APerType", 1, 6, "Integer"},
    {"APeriodDateFr", 1, 11, "TDateTime"},
    {"APeriodDateTo", 1, 11, "TDateTime"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CheckFilter[] =
  {
    {"AFilterGUI", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_DocCreateCheckFilter[] =
  {
    {"ASpecGUI", 1, 26, "String"},
    {"AFilterCode", 1, 26, "String"},
    {"AFilterName", 3, 26, "String"},
    {"AFilterGUI", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_DocCreate[] =
  {
    {"ASpecGUI", 1, 26, "String"},
    {"AAuthor", 1, 26, "String"},
    {"AFilter", 1, 26, "String"},
    {"AConcr", 1, 4, "Boolean"},
    {"ACreateType", 1, 6, "Integer"},
    {"APerType", 1, 6, "Integer"},
    {"APeriodDateFr", 1, 11, "TDateTime"},
    {"APeriodDateTo", 1, 11, "TDateTime"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_StartDocCreate[] =
  {
    {"ASpecGUI", 1, 26, "String"},
    {"AAuthor", 1, 26, "String"},
    {"AFilter", 1, 26, "String"},
    {"AFilterNameGUI", 1, 26, "String"},
    {"AConcr", 1, 4, "Boolean"},
    {"ACreateType", 1, 6, "Integer"},
    {"APerType", 1, 6, "Integer"},
    {"APeriodDateFr", 1, 11, "TDateTime"},
    {"APeriodDateTo", 1, 11, "TDateTime"},
    {"ANotSave", 1, 4, "Boolean"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_DocCreateProgress[] =
  {
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_LoadDocPreviewProgress[] =
  {
    {"AMsg", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetQList[] =
  {
    {"AId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetQList_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_SetQList[] =
  {
    {"AId", 1, 26, "String"},
    {"AList", 1, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsDocClass_GetFilterByGUI[] =
  {
    {"AGUI", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsDocClass_CanHandEdit[] =
  {
    {"ASpecGUI", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetDefXML[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetClassXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetClassZIPXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetCount[] =
  {
    {"AId", 1, 26, "String"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetIdList[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetIdList_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetValById[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetValById_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetValById10[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetValById10_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_Find[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_Find_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_InsertData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_EditData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_DeleteData[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_SetTextData[] =
  {
    {"AId", 1, 26, "String"},
    {"AFlId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"AVal", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetTextData[] =
  {
    {"AId", 1, 26, "String"},
    {"AFlId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"AMsg", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_SetMainLPU[] =
  {
    {"ALPUCode", 1, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_MainOwnerCode[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_IsMainOwnerCode[] =
  {
    {"AOwnerCode", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetMIBPForm[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetMIBPList[] =
  {
    {"AId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsVacStoreClass_GetMIBPList_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsEIDataClass_ImportData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"AResMessage", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsEIDataClass_UploadData[] =
  {
    {"ADataPart", 1, 26, "String"},
    {"AType", 1, 6, "Integer"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsEIDataClass_GetExportDataHeader[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsEIDataClass_GetPatDataById[] =
  {
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsEIDataClass_GetPatCardDataById[] =
  {
    {"ARecId", 1, 26, "String"},
    {"AClsRC", 1, 37, "TJSONObject"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_MigrateScriptProgress[] =
  {
    {"Sender", 1, 37, "TObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetDefXML[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetClassXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetClassZIPXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_SetClassZIPXML[] =
  {
    {"ARef", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetCount[] =
  {
    {"AId", 1, 26, "String"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 18, "Int64"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetIdList[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetIdList_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AMax", 1, 6, "Integer"},
    {"AFilterParam", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetValById[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetValById_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetValById10[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetValById10_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_Find[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_Find_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_FindEx[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_FindEx_Cache[] =
  {
    {"AId", 1, 26, "String"},
    {"AFindData", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_InsertData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_EditData[] =
  {
    {"AId", 1, 26, "String"},
    {"AValue", 1, 37, "TJSONObject"},
    {"ARecId", 3, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_DeleteData[] =
  {
    {"AId", 1, 26, "String"},
    {"ARecId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_UploadData[] =
  {
    {"ADataPart", 1, 26, "String"},
    {"AType", 1, 6, "Integer"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_CheckUser[] =
  {
    {"AUser", 1, 26, "String"},
    {"APasswd", 1, 26, "String"},
    {"AValid", 3, 4, "Boolean"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_LoginUser[] =
  {
    {"AId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_CheckRT[] =
  {
    {"AURT", 1, 26, "String"},
    {"AFunc", 1, 26, "String"},
    {"", 4, 4, "Boolean"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_GetUpdateCode[] =
  {
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_FindAddr[] =
  {
    {"AStr", 1, 26, "String"},
    {"ADefAddr", 1, 26, "String"},
    {"AParams", 1, 6, "Integer"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_FindAddr_Cache[] =
  {
    {"AStr", 1, 26, "String"},
    {"ADefAddr", 1, 26, "String"},
    {"AParams", 1, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TdsAdminClass_AddrCodeToText[] =
  {
    {"ACode", 1, 26, "String"},
    {"AParams", 1, 6, "Integer"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TMISAPI_PatientF63[] =
  {
    {"APtId", 1, 26, "String"},
    {"", 4, 37, "TJSONObject"}
  };

  struct TDSRestParameterMetaData TMISAPI_PatientF63_Cache[] =
  {
    {"APtId", 1, 26, "String"},
    {"", 4, 26, "String"}
  };

  struct TDSRestParameterMetaData TMISAPI_ImportPatient[] =
  {
    {"AMIS", 1, 26, "String"},
    {"ASrc", 1, 37, "TJSONObject"},
    {"", 4, 26, "String"}
  };

void __fastcall TdsICClassClient::AppOptLoadXML(TJSONObject* AOptNode)
{
  if (FAppOptLoadXMLCommand == NULL)
  {
    FAppOptLoadXMLCommand = FConnection->CreateCommand();
    FAppOptLoadXMLCommand->RequestType = "POST";
    FAppOptLoadXMLCommand->Text = "TdsICClass.\"AppOptLoadXML\"";
    FAppOptLoadXMLCommand->Prepare(TdsICClass_AppOptLoadXML, 0);
  }
  FAppOptLoadXMLCommand->Parameters->Parameter[0]->Value->SetJSONValue(AOptNode, FInstanceOwner);
  FAppOptLoadXMLCommand->Execute();
}

void __fastcall TdsICClassClient::Disconnect()
{
  if (FDisconnectCommand == NULL)
  {
    FDisconnectCommand = FConnection->CreateCommand();
    FDisconnectCommand->RequestType = "GET";
    FDisconnectCommand->Text = "TdsICClass.Disconnect";
  }
  FDisconnectCommand->Execute();
}

System::UnicodeString __fastcall TdsICClassClient::GetDefXML(const String& ARequestFilter)
{
  if (FGetDefXMLCommand == NULL)
  {
    FGetDefXMLCommand = FConnection->CreateCommand();
    FGetDefXMLCommand->RequestType = "GET";
    FGetDefXMLCommand->Text = "TdsICClass.GetDefXML";
    FGetDefXMLCommand->Prepare(TdsICClass_GetDefXML, 0);
  }
  FGetDefXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetDefXMLCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetClassXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassXMLCommand == NULL)
  {
    FGetClassXMLCommand = FConnection->CreateCommand();
    FGetClassXMLCommand->RequestType = "GET";
    FGetClassXMLCommand->Text = "TdsICClass.GetClassXML";
    FGetClassXMLCommand->Prepare(TdsICClass_GetClassXML, 1);
  }
  FGetClassXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassZIPXMLCommand == NULL)
  {
    FGetClassZIPXMLCommand = FConnection->CreateCommand();
    FGetClassZIPXMLCommand->RequestType = "GET";
    FGetClassZIPXMLCommand->Text = "TdsICClass.GetClassZIPXML";
    FGetClassZIPXMLCommand->Prepare(TdsICClass_GetClassZIPXML, 1);
  }
  FGetClassZIPXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassZIPXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassZIPXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

bool __fastcall TdsICClassClient::SetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FSetClassZIPXMLCommand == NULL)
  {
    FSetClassZIPXMLCommand = FConnection->CreateCommand();
    FSetClassZIPXMLCommand->RequestType = "GET";
    FSetClassZIPXMLCommand->Text = "TdsICClass.SetClassZIPXML";
    FSetClassZIPXMLCommand->Prepare(TdsICClass_SetClassZIPXML, 1);
  }
  FSetClassZIPXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FSetClassZIPXMLCommand->Execute(ARequestFilter);
  bool result = FSetClassZIPXMLCommand->Parameters->Parameter[1]->Value->GetBoolean();
  return result;
}

__int64 __fastcall TdsICClassClient::GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, System::UnicodeString  &AExtCount, const String& ARequestFilter)
{
  if (FGetCountCommand == NULL)
  {
    FGetCountCommand = FConnection->CreateCommand();
    FGetCountCommand->RequestType = "GET";
    FGetCountCommand->Text = "TdsICClass.GetCount";
    FGetCountCommand->Prepare(TdsICClass_GetCount, 3);
  }
  FGetCountCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetCountCommand->Parameters->Parameter[1]->Value->SetWideString(AFilterParam);
  FGetCountCommand->Parameters->Parameter[2]->Value->SetWideString(AExtCount);
  FGetCountCommand->Execute(ARequestFilter);
  AExtCount = FGetCountCommand->Parameters->Parameter[2]->Value->GetWideString();
  __int64 result = FGetCountCommand->Parameters->Parameter[3]->Value->GetInt64();
  return result;
}

TJSONObject* __fastcall TdsICClassClient::GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand == NULL)
  {
    FGetIdListCommand = FConnection->CreateCommand();
    FGetIdListCommand->RequestType = "GET";
    FGetIdListCommand->Text = "TdsICClass.GetIdList";
    FGetIdListCommand->Prepare(TdsICClass_GetIdList, 3);
  }
  FGetIdListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetIdListCommand->Parameters->Parameter[3]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand_Cache == NULL)
  {
    FGetIdListCommand_Cache = FConnection->CreateCommand();
    FGetIdListCommand_Cache->RequestType = "GET";
    FGetIdListCommand_Cache->Text = "TdsICClass.GetIdList";
    FGetIdListCommand_Cache->Prepare(TdsICClass_GetIdList_Cache, 3);
  }
  FGetIdListCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand_Cache->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand_Cache->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetIdListCommand_Cache->Parameters->Parameter[3]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsICClassClient::GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand == NULL)
  {
    FGetValByIdCommand = FConnection->CreateCommand();
    FGetValByIdCommand->RequestType = "GET";
    FGetValByIdCommand->Text = "TdsICClass.GetValById";
    FGetValByIdCommand->Prepare(TdsICClass_GetValById, 2);
  }
  FGetValByIdCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValByIdCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand_Cache == NULL)
  {
    FGetValByIdCommand_Cache = FConnection->CreateCommand();
    FGetValByIdCommand_Cache->RequestType = "GET";
    FGetValByIdCommand_Cache->Text = "TdsICClass.GetValById";
    FGetValByIdCommand_Cache->Prepare(TdsICClass_GetValById_Cache, 2);
  }
  FGetValByIdCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValByIdCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsICClassClient::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command == NULL)
  {
    FGetValById10Command = FConnection->CreateCommand();
    FGetValById10Command->RequestType = "GET";
    FGetValById10Command->Text = "TdsICClass.GetValById10";
    FGetValById10Command->Prepare(TdsICClass_GetValById10, 2);
  }
  FGetValById10Command->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValById10Command->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command_Cache == NULL)
  {
    FGetValById10Command_Cache = FConnection->CreateCommand();
    FGetValById10Command_Cache->RequestType = "GET";
    FGetValById10Command_Cache->Text = "TdsICClass.GetValById10";
    FGetValById10Command_Cache->Prepare(TdsICClass_GetValById10_Cache, 2);
  }
  FGetValById10Command_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValById10Command_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsICClassClient::Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand == NULL)
  {
    FFindCommand = FConnection->CreateCommand();
    FFindCommand->RequestType = "GET";
    FFindCommand->Text = "TdsICClass.Find";
    FFindCommand->Prepare(TdsICClass_Find, 2);
  }
  FFindCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand_Cache == NULL)
  {
    FFindCommand_Cache = FConnection->CreateCommand();
    FFindCommand_Cache->RequestType = "GET";
    FFindCommand_Cache->Text = "TdsICClass.Find";
    FFindCommand_Cache->Prepare(TdsICClass_Find_Cache, 2);
  }
  FFindCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsICClassClient::FindEx(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindExCommand == NULL)
  {
    FFindExCommand = FConnection->CreateCommand();
    FFindExCommand->RequestType = "GET";
    FFindExCommand->Text = "TdsICClass.FindEx";
    FFindExCommand->Prepare(TdsICClass_FindEx, 2);
  }
  FFindExCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindExCommand->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindExCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindExCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::FindEx_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindExCommand_Cache == NULL)
  {
    FFindExCommand_Cache = FConnection->CreateCommand();
    FFindExCommand_Cache->RequestType = "GET";
    FFindExCommand_Cache->Text = "TdsICClass.FindEx";
    FFindExCommand_Cache->Prepare(TdsICClass_FindEx_Cache, 2);
  }
  FFindExCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindExCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindExCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindExCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsICClassClient::InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FInsertDataCommand == NULL)
  {
    FInsertDataCommand = FConnection->CreateCommand();
    FInsertDataCommand->RequestType = "POST";
    FInsertDataCommand->Text = "TdsICClass.\"InsertData\"";
    FInsertDataCommand->Prepare(TdsICClass_InsertData, 3);
  }
  FInsertDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FInsertDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FInsertDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FInsertDataCommand->Execute(ARequestFilter);
  ARecId = FInsertDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FInsertDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FEditDataCommand == NULL)
  {
    FEditDataCommand = FConnection->CreateCommand();
    FEditDataCommand->RequestType = "POST";
    FEditDataCommand->Text = "TdsICClass.\"EditData\"";
    FEditDataCommand->Prepare(TdsICClass_EditData, 3);
  }
  FEditDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FEditDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FEditDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FEditDataCommand->Execute(ARequestFilter);
  ARecId = FEditDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FEditDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FDeleteDataCommand == NULL)
  {
    FDeleteDataCommand = FConnection->CreateCommand();
    FDeleteDataCommand->RequestType = "GET";
    FDeleteDataCommand->Text = "TdsICClass.DeleteData";
    FDeleteDataCommand->Prepare(TdsICClass_DeleteData, 2);
  }
  FDeleteDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FDeleteDataCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FDeleteDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDeleteDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetF63(System::UnicodeString ARef, System::UnicodeString APrintParams, const String& ARequestFilter)
{
  if (FGetF63Command == NULL)
  {
    FGetF63Command = FConnection->CreateCommand();
    FGetF63Command->RequestType = "GET";
    FGetF63Command->Text = "TdsICClass.GetF63";
    FGetF63Command->Prepare(TdsICClass_GetF63, 2);
  }
  FGetF63Command->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetF63Command->Parameters->Parameter[1]->Value->SetWideString(APrintParams);
  FGetF63Command->Execute(ARequestFilter);
  System::UnicodeString result = FGetF63Command->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetVacDefDoze(System::TDateTime APrivDate, System::TDateTime APatBirthday, System::UnicodeString AVacCode, const String& ARequestFilter)
{
  if (FGetVacDefDozeCommand == NULL)
  {
    FGetVacDefDozeCommand = FConnection->CreateCommand();
    FGetVacDefDozeCommand->RequestType = "GET";
    FGetVacDefDozeCommand->Text = "TdsICClass.GetVacDefDoze";
    FGetVacDefDozeCommand->Prepare(TdsICClass_GetVacDefDoze, 3);
  }
  FGetVacDefDozeCommand->Parameters->Parameter[0]->Value->AsDateTime = APrivDate;
  FGetVacDefDozeCommand->Parameters->Parameter[1]->Value->AsDateTime = APatBirthday;
  FGetVacDefDozeCommand->Parameters->Parameter[2]->Value->SetWideString(AVacCode);
  FGetVacDefDozeCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetVacDefDozeCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::CreateUnitPlan(__int64 APtId, const String& ARequestFilter)
{
  if (FCreateUnitPlanCommand == NULL)
  {
    FCreateUnitPlanCommand = FConnection->CreateCommand();
    FCreateUnitPlanCommand->RequestType = "GET";
    FCreateUnitPlanCommand->Text = "TdsICClass.CreateUnitPlan";
    FCreateUnitPlanCommand->Prepare(TdsICClass_CreateUnitPlan, 1);
  }
  FCreateUnitPlanCommand->Parameters->Parameter[0]->Value->SetInt64(APtId);
  FCreateUnitPlanCommand->Execute(ARequestFilter);
  System::UnicodeString result = FCreateUnitPlanCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::DeletePlan(System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FDeletePlanCommand == NULL)
  {
    FDeletePlanCommand = FConnection->CreateCommand();
    FDeletePlanCommand->RequestType = "GET";
    FDeletePlanCommand->Text = "TdsICClass.DeletePlan";
    FDeletePlanCommand->Prepare(TdsICClass_DeletePlan, 1);
  }
  FDeletePlanCommand->Parameters->Parameter[0]->Value->SetWideString(ARecId);
  FDeletePlanCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDeletePlanCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::DeletePatientPlan(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FDeletePatientPlanCommand == NULL)
  {
    FDeletePatientPlanCommand = FConnection->CreateCommand();
    FDeletePatientPlanCommand->RequestType = "GET";
    FDeletePatientPlanCommand->Text = "TdsICClass.DeletePatientPlan";
    FDeletePatientPlanCommand->Prepare(TdsICClass_DeletePatientPlan, 2);
  }
  FDeletePatientPlanCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FDeletePatientPlanCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FDeletePatientPlanCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDeletePatientPlanCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::StartCreatePlan(TJSONObject* AVal, const String& ARequestFilter)
{
  if (FStartCreatePlanCommand == NULL)
  {
    FStartCreatePlanCommand = FConnection->CreateCommand();
    FStartCreatePlanCommand->RequestType = "POST";
    FStartCreatePlanCommand->Text = "TdsICClass.\"StartCreatePlan\"";
    FStartCreatePlanCommand->Prepare(TdsICClass_StartCreatePlan, 1);
  }
  FStartCreatePlanCommand->Parameters->Parameter[0]->Value->SetJSONValue(AVal, FInstanceOwner);
  FStartCreatePlanCommand->Execute(ARequestFilter);
  System::UnicodeString result = FStartCreatePlanCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::StopCreatePlan(const String& ARequestFilter)
{
  if (FStopCreatePlanCommand == NULL)
  {
    FStopCreatePlanCommand = FConnection->CreateCommand();
    FStopCreatePlanCommand->RequestType = "GET";
    FStopCreatePlanCommand->Text = "TdsICClass.StopCreatePlan";
    FStopCreatePlanCommand->Prepare(TdsICClass_StopCreatePlan, 0);
  }
  FStopCreatePlanCommand->Execute(ARequestFilter);
  System::UnicodeString result = FStopCreatePlanCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::CheckCreatePlanProgress(System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FCheckCreatePlanProgressCommand == NULL)
  {
    FCheckCreatePlanProgressCommand = FConnection->CreateCommand();
    FCheckCreatePlanProgressCommand->RequestType = "GET";
    FCheckCreatePlanProgressCommand->Text = "TdsICClass.CheckCreatePlanProgress";
    FCheckCreatePlanProgressCommand->Prepare(TdsICClass_CheckCreatePlanProgress, 1);
  }
  FCheckCreatePlanProgressCommand->Parameters->Parameter[0]->Value->SetWideString(ARecId);
  FCheckCreatePlanProgressCommand->Execute(ARequestFilter);
  ARecId = FCheckCreatePlanProgressCommand->Parameters->Parameter[0]->Value->GetWideString();
  System::UnicodeString result = FCheckCreatePlanProgressCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetPatPlanOptions(__int64 APatCode, const String& ARequestFilter)
{
  if (FGetPatPlanOptionsCommand == NULL)
  {
    FGetPatPlanOptionsCommand = FConnection->CreateCommand();
    FGetPatPlanOptionsCommand->RequestType = "GET";
    FGetPatPlanOptionsCommand->Text = "TdsICClass.GetPatPlanOptions";
    FGetPatPlanOptionsCommand->Prepare(TdsICClass_GetPatPlanOptions, 1);
  }
  FGetPatPlanOptionsCommand->Parameters->Parameter[0]->Value->SetInt64(APatCode);
  FGetPatPlanOptionsCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetPatPlanOptionsCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetPrivVar(__int64 APrivCode, const String& ARequestFilter)
{
  if (FGetPrivVarCommand == NULL)
  {
    FGetPrivVarCommand = FConnection->CreateCommand();
    FGetPrivVarCommand->RequestType = "GET";
    FGetPrivVarCommand->Text = "TdsICClass.GetPrivVar";
    FGetPrivVarCommand->Prepare(TdsICClass_GetPrivVar, 1);
  }
  FGetPrivVarCommand->Parameters->Parameter[0]->Value->SetInt64(APrivCode);
  FGetPrivVarCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetPrivVarCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetOtvodInf(__int64 AOtvodCode, const String& ARequestFilter)
{
  if (FGetOtvodInfCommand == NULL)
  {
    FGetOtvodInfCommand = FConnection->CreateCommand();
    FGetOtvodInfCommand->RequestType = "GET";
    FGetOtvodInfCommand->Text = "TdsICClass.GetOtvodInf";
    FGetOtvodInfCommand->Prepare(TdsICClass_GetOtvodInf, 1);
  }
  FGetOtvodInfCommand->Parameters->Parameter[0]->Value->SetInt64(AOtvodCode);
  FGetOtvodInfCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetOtvodInfCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TdsICClassClient::SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData)
{
  if (FSetPatPlanOptionsCommand == NULL)
  {
    FSetPatPlanOptionsCommand = FConnection->CreateCommand();
    FSetPatPlanOptionsCommand->RequestType = "GET";
    FSetPatPlanOptionsCommand->Text = "TdsICClass.SetPatPlanOptions";
    FSetPatPlanOptionsCommand->Prepare(TdsICClass_SetPatPlanOptions, 1);
  }
  FSetPatPlanOptionsCommand->Parameters->Parameter[0]->Value->SetInt64(APatCode);
  FSetPatPlanOptionsCommand->Parameters->Parameter[1]->Value->SetWideString(AData);
  FSetPatPlanOptionsCommand->Execute();
}

System::UnicodeString __fastcall TdsICClassClient::PrintPlan(__int64 ACode, const String& ARequestFilter)
{
  if (FPrintPlanCommand == NULL)
  {
    FPrintPlanCommand = FConnection->CreateCommand();
    FPrintPlanCommand->RequestType = "GET";
    FPrintPlanCommand->Text = "TdsICClass.PrintPlan";
    FPrintPlanCommand->Prepare(TdsICClass_PrintPlan, 1);
  }
  FPrintPlanCommand->Parameters->Parameter[0]->Value->SetInt64(ACode);
  FPrintPlanCommand->Execute(ARequestFilter);
  System::UnicodeString result = FPrintPlanCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

TJSONObject* __fastcall TdsICClassClient::GetPlanPrintFormats(const String& ARequestFilter)
{
  if (FGetPlanPrintFormatsCommand == NULL)
  {
    FGetPlanPrintFormatsCommand = FConnection->CreateCommand();
    FGetPlanPrintFormatsCommand->RequestType = "GET";
    FGetPlanPrintFormatsCommand->Text = "TdsICClass.GetPlanPrintFormats";
    FGetPlanPrintFormatsCommand->Prepare(TdsICClass_GetPlanPrintFormats, 0);
  }
  FGetPlanPrintFormatsCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetPlanPrintFormatsCommand->Parameters->Parameter[0]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::GetPlanPrintFormats_Cache(const String& ARequestFilter)
{
  if (FGetPlanPrintFormatsCommand_Cache == NULL)
  {
    FGetPlanPrintFormatsCommand_Cache = FConnection->CreateCommand();
    FGetPlanPrintFormatsCommand_Cache->RequestType = "GET";
    FGetPlanPrintFormatsCommand_Cache->Text = "TdsICClass.GetPlanPrintFormats";
    FGetPlanPrintFormatsCommand_Cache->Prepare(TdsICClass_GetPlanPrintFormats_Cache, 0);
  }
  FGetPlanPrintFormatsCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetPlanPrintFormatsCommand_Cache->Parameters->Parameter[0]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsICClassClient::GetAppOptData(const String& ARequestFilter)
{
  if (FGetAppOptDataCommand == NULL)
  {
    FGetAppOptDataCommand = FConnection->CreateCommand();
    FGetAppOptDataCommand->RequestType = "GET";
    FGetAppOptDataCommand->Text = "TdsICClass.GetAppOptData";
    FGetAppOptDataCommand->Prepare(TdsICClass_GetAppOptData, 0);
  }
  FGetAppOptDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetAppOptDataCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

void __fastcall TdsICClassClient::SetAppOptData(System::UnicodeString ARef)
{
  if (FSetAppOptDataCommand == NULL)
  {
    FSetAppOptDataCommand = FConnection->CreateCommand();
    FSetAppOptDataCommand->RequestType = "GET";
    FSetAppOptDataCommand->Text = "TdsICClass.SetAppOptData";
    FSetAppOptDataCommand->Prepare(TdsICClass_SetAppOptData, 0);
  }
  FSetAppOptDataCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FSetAppOptDataCommand->Execute();
}

void __fastcall TdsICClassClient::SetMainLPU(__int64 ALPUCode)
{
  if (FSetMainLPUCommand == NULL)
  {
    FSetMainLPUCommand = FConnection->CreateCommand();
    FSetMainLPUCommand->RequestType = "GET";
    FSetMainLPUCommand->Text = "TdsICClass.SetMainLPU";
    FSetMainLPUCommand->Prepare(TdsICClass_SetMainLPU, 0);
  }
  FSetMainLPUCommand->Parameters->Parameter[0]->Value->SetInt64(ALPUCode);
  FSetMainLPUCommand->Execute();
}

TJSONObject* __fastcall TdsICClassClient::FindMKB(System::UnicodeString AStr, const String& ARequestFilter)
{
  if (FFindMKBCommand == NULL)
  {
    FFindMKBCommand = FConnection->CreateCommand();
    FFindMKBCommand->RequestType = "GET";
    FFindMKBCommand->Text = "TdsICClass.FindMKB";
    FFindMKBCommand->Prepare(TdsICClass_FindMKB, 1);
  }
  FFindMKBCommand->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindMKBCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindMKBCommand->Parameters->Parameter[1]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::FindMKB_Cache(System::UnicodeString AStr, const String& ARequestFilter)
{
  if (FFindMKBCommand_Cache == NULL)
  {
    FFindMKBCommand_Cache = FConnection->CreateCommand();
    FFindMKBCommand_Cache->RequestType = "GET";
    FFindMKBCommand_Cache->Text = "TdsICClass.FindMKB";
    FFindMKBCommand_Cache->Prepare(TdsICClass_FindMKB_Cache, 1);
  }
  FFindMKBCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindMKBCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindMKBCommand_Cache->Parameters->Parameter[1]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsICClassClient::MKBCodeToText(System::UnicodeString ACode, const String& ARequestFilter)
{
  if (FMKBCodeToTextCommand == NULL)
  {
    FMKBCodeToTextCommand = FConnection->CreateCommand();
    FMKBCodeToTextCommand->RequestType = "GET";
    FMKBCodeToTextCommand->Text = "TdsICClass.MKBCodeToText";
    FMKBCodeToTextCommand->Prepare(TdsICClass_MKBCodeToText, 1);
  }
  FMKBCodeToTextCommand->Parameters->Parameter[0]->Value->SetWideString(ACode);
  FMKBCodeToTextCommand->Execute(ARequestFilter);
  System::UnicodeString result = FMKBCodeToTextCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetPlanTemplateDef(int AType, const String& ARequestFilter)
{
  if (FGetPlanTemplateDefCommand == NULL)
  {
    FGetPlanTemplateDefCommand = FConnection->CreateCommand();
    FGetPlanTemplateDefCommand->RequestType = "GET";
    FGetPlanTemplateDefCommand->Text = "TdsICClass.GetPlanTemplateDef";
    FGetPlanTemplateDefCommand->Prepare(TdsICClass_GetPlanTemplateDef, 1);
  }
  FGetPlanTemplateDefCommand->Parameters->Parameter[0]->Value->SetInt32(AType);
  FGetPlanTemplateDefCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetPlanTemplateDefCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetPlanOpt(const String& ARequestFilter)
{
  if (FGetPlanOptCommand == NULL)
  {
    FGetPlanOptCommand = FConnection->CreateCommand();
    FGetPlanOptCommand->RequestType = "GET";
    FGetPlanOptCommand->Text = "TdsICClass.GetPlanOpt";
    FGetPlanOptCommand->Prepare(TdsICClass_GetPlanOpt, 0);
  }
  FGetPlanOptCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetPlanOptCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

void __fastcall TdsICClassClient::SetPlanOpt(System::UnicodeString AOpt)
{
  if (FSetPlanOptCommand == NULL)
  {
    FSetPlanOptCommand = FConnection->CreateCommand();
    FSetPlanOptCommand->RequestType = "GET";
    FSetPlanOptCommand->Text = "TdsICClass.SetPlanOpt";
    FSetPlanOptCommand->Prepare(TdsICClass_SetPlanOpt, 0);
  }
  FSetPlanOptCommand->Parameters->Parameter[0]->Value->SetWideString(AOpt);
  FSetPlanOptCommand->Execute();
}

bool __fastcall TdsICClassClient::UploadData(System::UnicodeString ADataPart, int AType, const String& ARequestFilter)
{
  if (FUploadDataCommand == NULL)
  {
    FUploadDataCommand = FConnection->CreateCommand();
    FUploadDataCommand->RequestType = "GET";
    FUploadDataCommand->Text = "TdsICClass.UploadData";
    FUploadDataCommand->Prepare(TdsICClass_UploadData, 2);
  }
  FUploadDataCommand->Parameters->Parameter[0]->Value->SetWideString(ADataPart);
  FUploadDataCommand->Parameters->Parameter[1]->Value->SetInt32(AType);
  FUploadDataCommand->Execute(ARequestFilter);
  bool result = FUploadDataCommand->Parameters->Parameter[2]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsICClassClient::GetCliOptData(const String& ARequestFilter)
{
  if (FGetCliOptDataCommand == NULL)
  {
    FGetCliOptDataCommand = FConnection->CreateCommand();
    FGetCliOptDataCommand->RequestType = "GET";
    FGetCliOptDataCommand->Text = "TdsICClass.GetCliOptData";
    FGetCliOptDataCommand->Prepare(TdsICClass_GetCliOptData, 0);
  }
  FGetCliOptDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetCliOptDataCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

void __fastcall TdsICClassClient::SetCliOptData(System::UnicodeString ARef)
{
  if (FSetCliOptDataCommand == NULL)
  {
    FSetCliOptDataCommand = FConnection->CreateCommand();
    FSetCliOptDataCommand->RequestType = "GET";
    FSetCliOptDataCommand->Text = "TdsICClass.SetCliOptData";
    FSetCliOptDataCommand->Prepare(TdsICClass_SetCliOptData, 0);
  }
  FSetCliOptDataCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FSetCliOptDataCommand->Execute();
}

void __fastcall TdsICClassClient::SetUpdateSearch(bool AVal)
{
  if (FSetUpdateSearchCommand == NULL)
  {
    FSetUpdateSearchCommand = FConnection->CreateCommand();
    FSetUpdateSearchCommand->RequestType = "GET";
    FSetUpdateSearchCommand->Text = "TdsICClass.SetUpdateSearch";
    FSetUpdateSearchCommand->Prepare(TdsICClass_SetUpdateSearch, 0);
  }
  FSetUpdateSearchCommand->Parameters->Parameter[0]->Value->SetBoolean(AVal);
  FSetUpdateSearchCommand->Execute();
}

bool __fastcall TdsICClassClient::PatGrChData(__int64 APtId, System::UnicodeString AData, const String& ARequestFilter)
{
  if (FPatGrChDataCommand == NULL)
  {
    FPatGrChDataCommand = FConnection->CreateCommand();
    FPatGrChDataCommand->RequestType = "GET";
    FPatGrChDataCommand->Text = "TdsICClass.PatGrChData";
    FPatGrChDataCommand->Prepare(TdsICClass_PatGrChData, 2);
  }
  FPatGrChDataCommand->Parameters->Parameter[0]->Value->SetInt64(APtId);
  FPatGrChDataCommand->Parameters->Parameter[1]->Value->SetWideString(AData);
  FPatGrChDataCommand->Execute(ARequestFilter);
  bool result = FPatGrChDataCommand->Parameters->Parameter[2]->Value->GetBoolean();
  return result;
}

TJSONObject* __fastcall TdsICClassClient::FindAddr(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter)
{
  if (FFindAddrCommand == NULL)
  {
    FFindAddrCommand = FConnection->CreateCommand();
    FFindAddrCommand->RequestType = "GET";
    FFindAddrCommand->Text = "TdsICClass.FindAddr";
    FFindAddrCommand->Prepare(TdsICClass_FindAddr, 3);
  }
  FFindAddrCommand->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindAddrCommand->Parameters->Parameter[1]->Value->SetWideString(ADefAddr);
  FFindAddrCommand->Parameters->Parameter[2]->Value->SetInt32(AParams);
  FFindAddrCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindAddrCommand->Parameters->Parameter[3]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::FindAddr_Cache(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter)
{
  if (FFindAddrCommand_Cache == NULL)
  {
    FFindAddrCommand_Cache = FConnection->CreateCommand();
    FFindAddrCommand_Cache->RequestType = "GET";
    FFindAddrCommand_Cache->Text = "TdsICClass.FindAddr";
    FFindAddrCommand_Cache->Prepare(TdsICClass_FindAddr_Cache, 3);
  }
  FFindAddrCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindAddrCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(ADefAddr);
  FFindAddrCommand_Cache->Parameters->Parameter[2]->Value->SetInt32(AParams);
  FFindAddrCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindAddrCommand_Cache->Parameters->Parameter[3]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsICClassClient::AddrCodeToText(System::UnicodeString ACode, int AParams, const String& ARequestFilter)
{
  if (FAddrCodeToTextCommand == NULL)
  {
    FAddrCodeToTextCommand = FConnection->CreateCommand();
    FAddrCodeToTextCommand->RequestType = "GET";
    FAddrCodeToTextCommand->Text = "TdsICClass.AddrCodeToText";
    FAddrCodeToTextCommand->Prepare(TdsICClass_AddrCodeToText, 2);
  }
  FAddrCodeToTextCommand->Parameters->Parameter[0]->Value->SetWideString(ACode);
  FAddrCodeToTextCommand->Parameters->Parameter[1]->Value->SetInt32(AParams);
  FAddrCodeToTextCommand->Execute(ARequestFilter);
  System::UnicodeString result = FAddrCodeToTextCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

__int64 __fastcall TdsICClassClient::LPUCode(const String& ARequestFilter)
{
  if (FLPUCodeCommand == NULL)
  {
    FLPUCodeCommand = FConnection->CreateCommand();
    FLPUCodeCommand->RequestType = "GET";
    FLPUCodeCommand->Text = "TdsICClass.LPUCode";
    FLPUCodeCommand->Prepare(TdsICClass_LPUCode, 0);
  }
  FLPUCodeCommand->Execute(ARequestFilter);
  __int64 result = FLPUCodeCommand->Parameters->Parameter[0]->Value->GetInt64();
  return result;
}

TJSONObject* __fastcall TdsICClassClient::FindOrg(System::UnicodeString AStr, const String& ARequestFilter)
{
  if (FFindOrgCommand == NULL)
  {
    FFindOrgCommand = FConnection->CreateCommand();
    FFindOrgCommand->RequestType = "GET";
    FFindOrgCommand->Text = "TdsICClass.FindOrg";
    FFindOrgCommand->Prepare(TdsICClass_FindOrg, 1);
  }
  FFindOrgCommand->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindOrgCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindOrgCommand->Parameters->Parameter[1]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsICClassClient::FindOrg_Cache(System::UnicodeString AStr, const String& ARequestFilter)
{
  if (FFindOrgCommand_Cache == NULL)
  {
    FFindOrgCommand_Cache = FConnection->CreateCommand();
    FFindOrgCommand_Cache->RequestType = "GET";
    FFindOrgCommand_Cache->Text = "TdsICClass.FindOrg";
    FFindOrgCommand_Cache->Prepare(TdsICClass_FindOrg_Cache, 1);
  }
  FFindOrgCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindOrgCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindOrgCommand_Cache->Parameters->Parameter[1]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsICClassClient::OrgCodeToText(System::UnicodeString ACode, System::UnicodeString ASetting, const String& ARequestFilter)
{
  if (FOrgCodeToTextCommand == NULL)
  {
    FOrgCodeToTextCommand = FConnection->CreateCommand();
    FOrgCodeToTextCommand->RequestType = "GET";
    FOrgCodeToTextCommand->Text = "TdsICClass.OrgCodeToText";
    FOrgCodeToTextCommand->Prepare(TdsICClass_OrgCodeToText, 2);
  }
  FOrgCodeToTextCommand->Parameters->Parameter[0]->Value->SetWideString(ACode);
  FOrgCodeToTextCommand->Parameters->Parameter[1]->Value->SetWideString(ASetting);
  FOrgCodeToTextCommand->Execute(ARequestFilter);
  System::UnicodeString result = FOrgCodeToTextCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}


__fastcall  TdsICClassClient::TdsICClassClient(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TdsICClassClient::TdsICClassClient(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TdsICClassClient::~TdsICClassClient()
{
  delete FAppOptLoadXMLCommand;
  delete FDisconnectCommand;
  delete FGetDefXMLCommand;
  delete FGetClassXMLCommand;
  delete FGetClassZIPXMLCommand;
  delete FSetClassZIPXMLCommand;
  delete FGetCountCommand;
  delete FGetIdListCommand;
  delete FGetIdListCommand_Cache;
  delete FGetValByIdCommand;
  delete FGetValByIdCommand_Cache;
  delete FGetValById10Command;
  delete FGetValById10Command_Cache;
  delete FFindCommand;
  delete FFindCommand_Cache;
  delete FFindExCommand;
  delete FFindExCommand_Cache;
  delete FInsertDataCommand;
  delete FEditDataCommand;
  delete FDeleteDataCommand;
  delete FGetF63Command;
  delete FGetVacDefDozeCommand;
  delete FCreateUnitPlanCommand;
  delete FDeletePlanCommand;
  delete FDeletePatientPlanCommand;
  delete FStartCreatePlanCommand;
  delete FStopCreatePlanCommand;
  delete FCheckCreatePlanProgressCommand;
  delete FGetPatPlanOptionsCommand;
  delete FGetPrivVarCommand;
  delete FGetOtvodInfCommand;
  delete FSetPatPlanOptionsCommand;
  delete FPrintPlanCommand;
  delete FGetPlanPrintFormatsCommand;
  delete FGetPlanPrintFormatsCommand_Cache;
  delete FGetAppOptDataCommand;
  delete FSetAppOptDataCommand;
  delete FSetMainLPUCommand;
  delete FFindMKBCommand;
  delete FFindMKBCommand_Cache;
  delete FMKBCodeToTextCommand;
  delete FGetPlanTemplateDefCommand;
  delete FGetPlanOptCommand;
  delete FSetPlanOptCommand;
  delete FUploadDataCommand;
  delete FGetCliOptDataCommand;
  delete FSetCliOptDataCommand;
  delete FSetUpdateSearchCommand;
  delete FPatGrChDataCommand;
  delete FFindAddrCommand;
  delete FFindAddrCommand_Cache;
  delete FAddrCodeToTextCommand;
  delete FLPUCodeCommand;
  delete FFindOrgCommand;
  delete FFindOrgCommand_Cache;
  delete FOrgCodeToTextCommand;
}

void __fastcall TdsDocClassClient::Disconnect()
{
  if (FDisconnectCommand == NULL)
  {
    FDisconnectCommand = FConnection->CreateCommand();
    FDisconnectCommand->RequestType = "GET";
    FDisconnectCommand->Text = "TdsDocClass.Disconnect";
  }
  FDisconnectCommand->Execute();
}

System::UnicodeString __fastcall TdsDocClassClient::GetDefXML(const String& ARequestFilter)
{
  if (FGetDefXMLCommand == NULL)
  {
    FGetDefXMLCommand = FConnection->CreateCommand();
    FGetDefXMLCommand->RequestType = "GET";
    FGetDefXMLCommand->Text = "TdsDocClass.GetDefXML";
    FGetDefXMLCommand->Prepare(TdsDocClass_GetDefXML, 0);
  }
  FGetDefXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetDefXMLCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::GetClassXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassXMLCommand == NULL)
  {
    FGetClassXMLCommand = FConnection->CreateCommand();
    FGetClassXMLCommand->RequestType = "GET";
    FGetClassXMLCommand->Text = "TdsDocClass.GetClassXML";
    FGetClassXMLCommand->Prepare(TdsDocClass_GetClassXML, 1);
  }
  FGetClassXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassZIPXMLCommand == NULL)
  {
    FGetClassZIPXMLCommand = FConnection->CreateCommand();
    FGetClassZIPXMLCommand->RequestType = "GET";
    FGetClassZIPXMLCommand->Text = "TdsDocClass.GetClassZIPXML";
    FGetClassZIPXMLCommand->Prepare(TdsDocClass_GetClassZIPXML, 1);
  }
  FGetClassZIPXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassZIPXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassZIPXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

__int64 __fastcall TdsDocClassClient::GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetCountCommand == NULL)
  {
    FGetCountCommand = FConnection->CreateCommand();
    FGetCountCommand->RequestType = "GET";
    FGetCountCommand->Text = "TdsDocClass.GetCount";
    FGetCountCommand->Prepare(TdsDocClass_GetCount, 2);
  }
  FGetCountCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetCountCommand->Parameters->Parameter[1]->Value->SetWideString(AFilterParam);
  FGetCountCommand->Execute(ARequestFilter);
  __int64 result = FGetCountCommand->Parameters->Parameter[2]->Value->GetInt64();
  return result;
}

TJSONObject* __fastcall TdsDocClassClient::GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand == NULL)
  {
    FGetIdListCommand = FConnection->CreateCommand();
    FGetIdListCommand->RequestType = "GET";
    FGetIdListCommand->Text = "TdsDocClass.GetIdList";
    FGetIdListCommand->Prepare(TdsDocClass_GetIdList, 3);
  }
  FGetIdListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetIdListCommand->Parameters->Parameter[3]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsDocClassClient::GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand_Cache == NULL)
  {
    FGetIdListCommand_Cache = FConnection->CreateCommand();
    FGetIdListCommand_Cache->RequestType = "GET";
    FGetIdListCommand_Cache->Text = "TdsDocClass.GetIdList";
    FGetIdListCommand_Cache->Prepare(TdsDocClass_GetIdList_Cache, 3);
  }
  FGetIdListCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand_Cache->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand_Cache->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetIdListCommand_Cache->Parameters->Parameter[3]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsDocClassClient::GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand == NULL)
  {
    FGetValByIdCommand = FConnection->CreateCommand();
    FGetValByIdCommand->RequestType = "GET";
    FGetValByIdCommand->Text = "TdsDocClass.GetValById";
    FGetValByIdCommand->Prepare(TdsDocClass_GetValById, 2);
  }
  FGetValByIdCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValByIdCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsDocClassClient::GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand_Cache == NULL)
  {
    FGetValByIdCommand_Cache = FConnection->CreateCommand();
    FGetValByIdCommand_Cache->RequestType = "GET";
    FGetValByIdCommand_Cache->Text = "TdsDocClass.GetValById";
    FGetValByIdCommand_Cache->Prepare(TdsDocClass_GetValById_Cache, 2);
  }
  FGetValByIdCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValByIdCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsDocClassClient::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command == NULL)
  {
    FGetValById10Command = FConnection->CreateCommand();
    FGetValById10Command->RequestType = "GET";
    FGetValById10Command->Text = "TdsDocClass.GetValById10";
    FGetValById10Command->Prepare(TdsDocClass_GetValById10, 2);
  }
  FGetValById10Command->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValById10Command->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsDocClassClient::GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command_Cache == NULL)
  {
    FGetValById10Command_Cache = FConnection->CreateCommand();
    FGetValById10Command_Cache->RequestType = "GET";
    FGetValById10Command_Cache->Text = "TdsDocClass.GetValById10";
    FGetValById10Command_Cache->Prepare(TdsDocClass_GetValById10_Cache, 2);
  }
  FGetValById10Command_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValById10Command_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsDocClassClient::Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand == NULL)
  {
    FFindCommand = FConnection->CreateCommand();
    FFindCommand->RequestType = "GET";
    FFindCommand->Text = "TdsDocClass.Find";
    FFindCommand->Prepare(TdsDocClass_Find, 2);
  }
  FFindCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsDocClassClient::Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand_Cache == NULL)
  {
    FFindCommand_Cache = FConnection->CreateCommand();
    FFindCommand_Cache->RequestType = "GET";
    FFindCommand_Cache->Text = "TdsDocClass.Find";
    FFindCommand_Cache->Prepare(TdsDocClass_Find_Cache, 2);
  }
  FFindCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsDocClassClient::ImportData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FImportDataCommand == NULL)
  {
    FImportDataCommand = FConnection->CreateCommand();
    FImportDataCommand->RequestType = "POST";
    FImportDataCommand->Text = "TdsDocClass.\"ImportData\"";
    FImportDataCommand->Prepare(TdsDocClass_ImportData, 3);
  }
  FImportDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FImportDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FImportDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FImportDataCommand->Execute(ARequestFilter);
  ARecId = FImportDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FImportDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FInsertDataCommand == NULL)
  {
    FInsertDataCommand = FConnection->CreateCommand();
    FInsertDataCommand->RequestType = "POST";
    FInsertDataCommand->Text = "TdsDocClass.\"InsertData\"";
    FInsertDataCommand->Prepare(TdsDocClass_InsertData, 3);
  }
  FInsertDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FInsertDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FInsertDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FInsertDataCommand->Execute(ARequestFilter);
  ARecId = FInsertDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FInsertDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FEditDataCommand == NULL)
  {
    FEditDataCommand = FConnection->CreateCommand();
    FEditDataCommand->RequestType = "POST";
    FEditDataCommand->Text = "TdsDocClass.\"EditData\"";
    FEditDataCommand->Prepare(TdsDocClass_EditData, 3);
  }
  FEditDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FEditDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FEditDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FEditDataCommand->Execute(ARequestFilter);
  ARecId = FEditDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FEditDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FDeleteDataCommand == NULL)
  {
    FDeleteDataCommand = FConnection->CreateCommand();
    FDeleteDataCommand->RequestType = "GET";
    FDeleteDataCommand->Text = "TdsDocClass.DeleteData";
    FDeleteDataCommand->Prepare(TdsDocClass_DeleteData, 2);
  }
  FDeleteDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FDeleteDataCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FDeleteDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDeleteDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal, const String& ARequestFilter)
{
  if (FSetTextDataCommand == NULL)
  {
    FSetTextDataCommand = FConnection->CreateCommand();
    FSetTextDataCommand->RequestType = "GET";
    FSetTextDataCommand->Text = "TdsDocClass.SetTextData";
    FSetTextDataCommand->Prepare(TdsDocClass_SetTextData, 4);
  }
  FSetTextDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FSetTextDataCommand->Parameters->Parameter[1]->Value->SetWideString(AFlId);
  FSetTextDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FSetTextDataCommand->Parameters->Parameter[3]->Value->SetWideString(AVal);
  FSetTextDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FSetTextDataCommand->Parameters->Parameter[4]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString  &AMsg, const String& ARequestFilter)
{
  if (FGetTextDataCommand == NULL)
  {
    FGetTextDataCommand = FConnection->CreateCommand();
    FGetTextDataCommand->RequestType = "GET";
    FGetTextDataCommand->Text = "TdsDocClass.GetTextData";
    FGetTextDataCommand->Prepare(TdsDocClass_GetTextData, 4);
  }
  FGetTextDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetTextDataCommand->Parameters->Parameter[1]->Value->SetWideString(AFlId);
  FGetTextDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FGetTextDataCommand->Parameters->Parameter[3]->Value->SetWideString(AMsg);
  FGetTextDataCommand->Execute(ARequestFilter);
  AMsg = FGetTextDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  System::UnicodeString result = FGetTextDataCommand->Parameters->Parameter[4]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::GetSpecById(System::UnicodeString AId, int  &ASpecType, const String& ARequestFilter)
{
  if (FGetSpecByIdCommand == NULL)
  {
    FGetSpecByIdCommand = FConnection->CreateCommand();
    FGetSpecByIdCommand->RequestType = "GET";
    FGetSpecByIdCommand->Text = "TdsDocClass.GetSpecById";
    FGetSpecByIdCommand->Prepare(TdsDocClass_GetSpecById, 2);
  }
  FGetSpecByIdCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetSpecByIdCommand->Parameters->Parameter[1]->Value->SetInt32(ASpecType);
  FGetSpecByIdCommand->Execute(ARequestFilter);
  ASpecType = FGetSpecByIdCommand->Parameters->Parameter[1]->Value->GetInt32();
  System::UnicodeString result = FGetSpecByIdCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::GetDocById(System::UnicodeString AId, const String& ARequestFilter)
{
  if (FGetDocByIdCommand == NULL)
  {
    FGetDocByIdCommand = FConnection->CreateCommand();
    FGetDocByIdCommand->RequestType = "GET";
    FGetDocByIdCommand->Text = "TdsDocClass.GetDocById";
    FGetDocByIdCommand->Prepare(TdsDocClass_GetDocById, 1);
  }
  FGetDocByIdCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetDocByIdCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetDocByIdCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::CreateDocPreview(System::UnicodeString AId, int  &Al, int  &At, int  &Ar, int  &Ab, double  &Aps, int  &Ao, const String& ARequestFilter)
{
  if (FCreateDocPreviewCommand == NULL)
  {
    FCreateDocPreviewCommand = FConnection->CreateCommand();
    FCreateDocPreviewCommand->RequestType = "GET";
    FCreateDocPreviewCommand->Text = "TdsDocClass.CreateDocPreview";
    FCreateDocPreviewCommand->Prepare(TdsDocClass_CreateDocPreview, 7);
  }
  FCreateDocPreviewCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FCreateDocPreviewCommand->Parameters->Parameter[1]->Value->SetInt32(Al);
  FCreateDocPreviewCommand->Parameters->Parameter[2]->Value->SetInt32(At);
  FCreateDocPreviewCommand->Parameters->Parameter[3]->Value->SetInt32(Ar);
  FCreateDocPreviewCommand->Parameters->Parameter[4]->Value->SetInt32(Ab);
  FCreateDocPreviewCommand->Parameters->Parameter[5]->Value->SetDouble(Aps);
  FCreateDocPreviewCommand->Parameters->Parameter[6]->Value->SetInt32(Ao);
  FCreateDocPreviewCommand->Execute(ARequestFilter);
  Al = FCreateDocPreviewCommand->Parameters->Parameter[1]->Value->GetInt32();
  At = FCreateDocPreviewCommand->Parameters->Parameter[2]->Value->GetInt32();
  Ar = FCreateDocPreviewCommand->Parameters->Parameter[3]->Value->GetInt32();
  Ab = FCreateDocPreviewCommand->Parameters->Parameter[4]->Value->GetInt32();
  Aps = FCreateDocPreviewCommand->Parameters->Parameter[5]->Value->GetDouble();
  Ao = FCreateDocPreviewCommand->Parameters->Parameter[6]->Value->GetInt32();
  System::UnicodeString result = FCreateDocPreviewCommand->Parameters->Parameter[7]->Value->GetWideString();
  return result;
}

void __fastcall TdsDocClassClient::SaveDoc(System::UnicodeString AId)
{
  if (FSaveDocCommand == NULL)
  {
    FSaveDocCommand = FConnection->CreateCommand();
    FSaveDocCommand->RequestType = "GET";
    FSaveDocCommand->Text = "TdsDocClass.SaveDoc";
    FSaveDocCommand->Prepare(TdsDocClass_SaveDoc, 0);
  }
  FSaveDocCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FSaveDocCommand->Execute();
}

System::UnicodeString __fastcall TdsDocClassClient::CheckByPeriod(bool FCreateByType, System::UnicodeString ACode, System::UnicodeString ASpecGUI, const String& ARequestFilter)
{
  if (FCheckByPeriodCommand == NULL)
  {
    FCheckByPeriodCommand = FConnection->CreateCommand();
    FCheckByPeriodCommand->RequestType = "GET";
    FCheckByPeriodCommand->Text = "TdsDocClass.CheckByPeriod";
    FCheckByPeriodCommand->Prepare(TdsDocClass_CheckByPeriod, 3);
  }
  FCheckByPeriodCommand->Parameters->Parameter[0]->Value->SetBoolean(FCreateByType);
  FCheckByPeriodCommand->Parameters->Parameter[1]->Value->SetWideString(ACode);
  FCheckByPeriodCommand->Parameters->Parameter[2]->Value->SetWideString(ASpecGUI);
  FCheckByPeriodCommand->Execute(ARequestFilter);
  System::UnicodeString result = FCheckByPeriodCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::DocListClearByDate(System::TDateTime ADate, const String& ARequestFilter)
{
  if (FDocListClearByDateCommand == NULL)
  {
    FDocListClearByDateCommand = FConnection->CreateCommand();
    FDocListClearByDateCommand->RequestType = "GET";
    FDocListClearByDateCommand->Text = "TdsDocClass.DocListClearByDate";
    FDocListClearByDateCommand->Prepare(TdsDocClass_DocListClearByDate, 1);
  }
  FDocListClearByDateCommand->Parameters->Parameter[0]->Value->AsDateTime = ADate;
  FDocListClearByDateCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDocListClearByDateCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

int __fastcall TdsDocClassClient::GetDocCountBySpec(System::UnicodeString ASpecOWNER, System::UnicodeString ASpecGUI, System::UnicodeString  &ARCMsg, const String& ARequestFilter)
{
  if (FGetDocCountBySpecCommand == NULL)
  {
    FGetDocCountBySpecCommand = FConnection->CreateCommand();
    FGetDocCountBySpecCommand->RequestType = "GET";
    FGetDocCountBySpecCommand->Text = "TdsDocClass.GetDocCountBySpec";
    FGetDocCountBySpecCommand->Prepare(TdsDocClass_GetDocCountBySpec, 3);
  }
  FGetDocCountBySpecCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecOWNER);
  FGetDocCountBySpecCommand->Parameters->Parameter[1]->Value->SetWideString(ASpecGUI);
  FGetDocCountBySpecCommand->Parameters->Parameter[2]->Value->SetWideString(ARCMsg);
  FGetDocCountBySpecCommand->Execute(ARequestFilter);
  ARCMsg = FGetDocCountBySpecCommand->Parameters->Parameter[2]->Value->GetWideString();
  int result = FGetDocCountBySpecCommand->Parameters->Parameter[3]->Value->GetInt32();
  return result;
}

void __fastcall TdsDocClassClient::SetMainLPU(__int64 ALPUCode)
{
  if (FSetMainLPUCommand == NULL)
  {
    FSetMainLPUCommand = FConnection->CreateCommand();
    FSetMainLPUCommand->RequestType = "GET";
    FSetMainLPUCommand->Text = "TdsDocClass.SetMainLPU";
    FSetMainLPUCommand->Prepare(TdsDocClass_SetMainLPU, 0);
  }
  FSetMainLPUCommand->Parameters->Parameter[0]->Value->SetInt64(ALPUCode);
  FSetMainLPUCommand->Execute();
}

System::UnicodeString __fastcall TdsDocClassClient::GetMainLPUParam(const String& ARequestFilter)
{
  if (FGetMainLPUParamCommand == NULL)
  {
    FGetMainLPUParamCommand = FConnection->CreateCommand();
    FGetMainLPUParamCommand->RequestType = "GET";
    FGetMainLPUParamCommand->Text = "TdsDocClass.GetMainLPUParam";
    FGetMainLPUParamCommand->Prepare(TdsDocClass_GetMainLPUParam, 0);
  }
  FGetMainLPUParamCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetMainLPUParamCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::MainOwnerCode(const String& ARequestFilter)
{
  if (FMainOwnerCodeCommand == NULL)
  {
    FMainOwnerCodeCommand = FConnection->CreateCommand();
    FMainOwnerCodeCommand->RequestType = "GET";
    FMainOwnerCodeCommand->Text = "TdsDocClass.MainOwnerCode";
    FMainOwnerCodeCommand->Prepare(TdsDocClass_MainOwnerCode, 0);
  }
  FMainOwnerCodeCommand->Execute(ARequestFilter);
  System::UnicodeString result = FMainOwnerCodeCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

bool __fastcall TdsDocClassClient::IsMainOwnerCode(System::UnicodeString AOwnerCode, const String& ARequestFilter)
{
  if (FIsMainOwnerCodeCommand == NULL)
  {
    FIsMainOwnerCodeCommand = FConnection->CreateCommand();
    FIsMainOwnerCodeCommand->RequestType = "GET";
    FIsMainOwnerCodeCommand->Text = "TdsDocClass.IsMainOwnerCode";
    FIsMainOwnerCodeCommand->Prepare(TdsDocClass_IsMainOwnerCode, 1);
  }
  FIsMainOwnerCodeCommand->Parameters->Parameter[0]->Value->SetWideString(AOwnerCode);
  FIsMainOwnerCodeCommand->Execute(ARequestFilter);
  bool result = FIsMainOwnerCodeCommand->Parameters->Parameter[1]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::CopySpec(System::UnicodeString AName, System::UnicodeString  &ANewGUI, const String& ARequestFilter)
{
  if (FCopySpecCommand == NULL)
  {
    FCopySpecCommand = FConnection->CreateCommand();
    FCopySpecCommand->RequestType = "GET";
    FCopySpecCommand->Text = "TdsDocClass.CopySpec";
    FCopySpecCommand->Prepare(TdsDocClass_CopySpec, 2);
  }
  FCopySpecCommand->Parameters->Parameter[0]->Value->SetWideString(AName);
  FCopySpecCommand->Parameters->Parameter[1]->Value->SetWideString(ANewGUI);
  FCopySpecCommand->Execute(ARequestFilter);
  ANewGUI = FCopySpecCommand->Parameters->Parameter[1]->Value->GetWideString();
  System::UnicodeString result = FCopySpecCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

int __fastcall TdsDocClassClient::CheckSpecExists(System::UnicodeString AName, System::UnicodeString  &AGUI, bool AIsEdit, const String& ARequestFilter)
{
  if (FCheckSpecExistsCommand == NULL)
  {
    FCheckSpecExistsCommand = FConnection->CreateCommand();
    FCheckSpecExistsCommand->RequestType = "GET";
    FCheckSpecExistsCommand->Text = "TdsDocClass.CheckSpecExists";
    FCheckSpecExistsCommand->Prepare(TdsDocClass_CheckSpecExists, 3);
  }
  FCheckSpecExistsCommand->Parameters->Parameter[0]->Value->SetWideString(AName);
  FCheckSpecExistsCommand->Parameters->Parameter[1]->Value->SetWideString(AGUI);
  FCheckSpecExistsCommand->Parameters->Parameter[2]->Value->SetBoolean(AIsEdit);
  FCheckSpecExistsCommand->Execute(ARequestFilter);
  AGUI = FCheckSpecExistsCommand->Parameters->Parameter[1]->Value->GetWideString();
  int result = FCheckSpecExistsCommand->Parameters->Parameter[3]->Value->GetInt32();
  return result;
}

bool __fastcall TdsDocClassClient::CheckSpecExistsByCode(System::UnicodeString AGUI, const String& ARequestFilter)
{
  if (FCheckSpecExistsByCodeCommand == NULL)
  {
    FCheckSpecExistsByCodeCommand = FConnection->CreateCommand();
    FCheckSpecExistsByCodeCommand->RequestType = "GET";
    FCheckSpecExistsByCodeCommand->Text = "TdsDocClass.CheckSpecExistsByCode";
    FCheckSpecExistsByCodeCommand->Prepare(TdsDocClass_CheckSpecExistsByCode, 1);
  }
  FCheckSpecExistsByCodeCommand->Parameters->Parameter[0]->Value->SetWideString(AGUI);
  FCheckSpecExistsByCodeCommand->Execute(ARequestFilter);
  bool result = FCheckSpecExistsByCodeCommand->Parameters->Parameter[1]->Value->GetBoolean();
  return result;
}

bool __fastcall TdsDocClassClient::CheckDocCreateType(System::UnicodeString ASpecGUI, int  &ACreateType, int ADefCreateType, int  &AGetCreateType, int  &AGetPeriod, const String& ARequestFilter)
{
  if (FCheckDocCreateTypeCommand == NULL)
  {
    FCheckDocCreateTypeCommand = FConnection->CreateCommand();
    FCheckDocCreateTypeCommand->RequestType = "GET";
    FCheckDocCreateTypeCommand->Text = "TdsDocClass.CheckDocCreateType";
    FCheckDocCreateTypeCommand->Prepare(TdsDocClass_CheckDocCreateType, 5);
  }
  FCheckDocCreateTypeCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecGUI);
  FCheckDocCreateTypeCommand->Parameters->Parameter[1]->Value->SetInt32(ACreateType);
  FCheckDocCreateTypeCommand->Parameters->Parameter[2]->Value->SetInt32(ADefCreateType);
  FCheckDocCreateTypeCommand->Parameters->Parameter[3]->Value->SetInt32(AGetCreateType);
  FCheckDocCreateTypeCommand->Parameters->Parameter[4]->Value->SetInt32(AGetPeriod);
  FCheckDocCreateTypeCommand->Execute(ARequestFilter);
  ACreateType = FCheckDocCreateTypeCommand->Parameters->Parameter[1]->Value->GetInt32();
  AGetCreateType = FCheckDocCreateTypeCommand->Parameters->Parameter[3]->Value->GetInt32();
  AGetPeriod = FCheckDocCreateTypeCommand->Parameters->Parameter[4]->Value->GetInt32();
  bool result = FCheckDocCreateTypeCommand->Parameters->Parameter[5]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::DocCreateCheckExist(System::UnicodeString ASpecGUI, System::UnicodeString AAuthor, int APerType, System::TDateTime APeriodDateFr, System::TDateTime APeriodDateTo, const String& ARequestFilter)
{
  if (FDocCreateCheckExistCommand == NULL)
  {
    FDocCreateCheckExistCommand = FConnection->CreateCommand();
    FDocCreateCheckExistCommand->RequestType = "GET";
    FDocCreateCheckExistCommand->Text = "TdsDocClass.DocCreateCheckExist";
    FDocCreateCheckExistCommand->Prepare(TdsDocClass_DocCreateCheckExist, 5);
  }
  FDocCreateCheckExistCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecGUI);
  FDocCreateCheckExistCommand->Parameters->Parameter[1]->Value->SetWideString(AAuthor);
  FDocCreateCheckExistCommand->Parameters->Parameter[2]->Value->SetInt32(APerType);
  FDocCreateCheckExistCommand->Parameters->Parameter[3]->Value->AsDateTime = APeriodDateFr;
  FDocCreateCheckExistCommand->Parameters->Parameter[4]->Value->AsDateTime = APeriodDateTo;
  FDocCreateCheckExistCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDocCreateCheckExistCommand->Parameters->Parameter[5]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::CheckFilter(System::UnicodeString AFilterGUI, const String& ARequestFilter)
{
  if (FCheckFilterCommand == NULL)
  {
    FCheckFilterCommand = FConnection->CreateCommand();
    FCheckFilterCommand->RequestType = "GET";
    FCheckFilterCommand->Text = "TdsDocClass.CheckFilter";
    FCheckFilterCommand->Prepare(TdsDocClass_CheckFilter, 1);
  }
  FCheckFilterCommand->Parameters->Parameter[0]->Value->SetWideString(AFilterGUI);
  FCheckFilterCommand->Execute(ARequestFilter);
  System::UnicodeString result = FCheckFilterCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::DocCreateCheckFilter(System::UnicodeString ASpecGUI, System::UnicodeString AFilterCode, System::UnicodeString  &AFilterName, System::UnicodeString  &AFilterGUI, const String& ARequestFilter)
{
  if (FDocCreateCheckFilterCommand == NULL)
  {
    FDocCreateCheckFilterCommand = FConnection->CreateCommand();
    FDocCreateCheckFilterCommand->RequestType = "GET";
    FDocCreateCheckFilterCommand->Text = "TdsDocClass.DocCreateCheckFilter";
    FDocCreateCheckFilterCommand->Prepare(TdsDocClass_DocCreateCheckFilter, 4);
  }
  FDocCreateCheckFilterCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecGUI);
  FDocCreateCheckFilterCommand->Parameters->Parameter[1]->Value->SetWideString(AFilterCode);
  FDocCreateCheckFilterCommand->Parameters->Parameter[2]->Value->SetWideString(AFilterName);
  FDocCreateCheckFilterCommand->Parameters->Parameter[3]->Value->SetWideString(AFilterGUI);
  FDocCreateCheckFilterCommand->Execute(ARequestFilter);
  AFilterName = FDocCreateCheckFilterCommand->Parameters->Parameter[2]->Value->GetWideString();
  AFilterGUI = FDocCreateCheckFilterCommand->Parameters->Parameter[3]->Value->GetWideString();
  System::UnicodeString result = FDocCreateCheckFilterCommand->Parameters->Parameter[4]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::DocCreate(System::UnicodeString ASpecGUI, System::UnicodeString AAuthor, System::UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, System::TDateTime APeriodDateFr, System::TDateTime APeriodDateTo, const String& ARequestFilter)
{
  if (FDocCreateCommand == NULL)
  {
    FDocCreateCommand = FConnection->CreateCommand();
    FDocCreateCommand->RequestType = "GET";
    FDocCreateCommand->Text = "TdsDocClass.DocCreate";
    FDocCreateCommand->Prepare(TdsDocClass_DocCreate, 8);
  }
  FDocCreateCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecGUI);
  FDocCreateCommand->Parameters->Parameter[1]->Value->SetWideString(AAuthor);
  FDocCreateCommand->Parameters->Parameter[2]->Value->SetWideString(AFilter);
  FDocCreateCommand->Parameters->Parameter[3]->Value->SetBoolean(AConcr);
  FDocCreateCommand->Parameters->Parameter[4]->Value->SetInt32(ACreateType);
  FDocCreateCommand->Parameters->Parameter[5]->Value->SetInt32(APerType);
  FDocCreateCommand->Parameters->Parameter[6]->Value->AsDateTime = APeriodDateFr;
  FDocCreateCommand->Parameters->Parameter[7]->Value->AsDateTime = APeriodDateTo;
  FDocCreateCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDocCreateCommand->Parameters->Parameter[8]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::StartDocCreate(System::UnicodeString ASpecGUI, System::UnicodeString AAuthor, System::UnicodeString AFilter, System::UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, System::TDateTime APeriodDateFr, System::TDateTime APeriodDateTo, bool ANotSave, const String& ARequestFilter)
{
  if (FStartDocCreateCommand == NULL)
  {
    FStartDocCreateCommand = FConnection->CreateCommand();
    FStartDocCreateCommand->RequestType = "GET";
    FStartDocCreateCommand->Text = "TdsDocClass.StartDocCreate";
    FStartDocCreateCommand->Prepare(TdsDocClass_StartDocCreate, 10);
  }
  FStartDocCreateCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecGUI);
  FStartDocCreateCommand->Parameters->Parameter[1]->Value->SetWideString(AAuthor);
  FStartDocCreateCommand->Parameters->Parameter[2]->Value->SetWideString(AFilter);
  FStartDocCreateCommand->Parameters->Parameter[3]->Value->SetWideString(AFilterNameGUI);
  FStartDocCreateCommand->Parameters->Parameter[4]->Value->SetBoolean(AConcr);
  FStartDocCreateCommand->Parameters->Parameter[5]->Value->SetInt32(ACreateType);
  FStartDocCreateCommand->Parameters->Parameter[6]->Value->SetInt32(APerType);
  FStartDocCreateCommand->Parameters->Parameter[7]->Value->AsDateTime = APeriodDateFr;
  FStartDocCreateCommand->Parameters->Parameter[8]->Value->AsDateTime = APeriodDateTo;
  FStartDocCreateCommand->Parameters->Parameter[9]->Value->SetBoolean(ANotSave);
  FStartDocCreateCommand->Execute(ARequestFilter);
  System::UnicodeString result = FStartDocCreateCommand->Parameters->Parameter[10]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::DocCreateProgress(System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FDocCreateProgressCommand == NULL)
  {
    FDocCreateProgressCommand = FConnection->CreateCommand();
    FDocCreateProgressCommand->RequestType = "GET";
    FDocCreateProgressCommand->Text = "TdsDocClass.DocCreateProgress";
    FDocCreateProgressCommand->Prepare(TdsDocClass_DocCreateProgress, 1);
  }
  FDocCreateProgressCommand->Parameters->Parameter[0]->Value->SetWideString(ARecId);
  FDocCreateProgressCommand->Execute(ARequestFilter);
  ARecId = FDocCreateProgressCommand->Parameters->Parameter[0]->Value->GetWideString();
  System::UnicodeString result = FDocCreateProgressCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsDocClassClient::LoadDocPreviewProgress(System::UnicodeString  &AMsg, const String& ARequestFilter)
{
  if (FLoadDocPreviewProgressCommand == NULL)
  {
    FLoadDocPreviewProgressCommand = FConnection->CreateCommand();
    FLoadDocPreviewProgressCommand->RequestType = "GET";
    FLoadDocPreviewProgressCommand->Text = "TdsDocClass.LoadDocPreviewProgress";
    FLoadDocPreviewProgressCommand->Prepare(TdsDocClass_LoadDocPreviewProgress, 1);
  }
  FLoadDocPreviewProgressCommand->Parameters->Parameter[0]->Value->SetWideString(AMsg);
  FLoadDocPreviewProgressCommand->Execute(ARequestFilter);
  AMsg = FLoadDocPreviewProgressCommand->Parameters->Parameter[0]->Value->GetWideString();
  System::UnicodeString result = FLoadDocPreviewProgressCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

TJSONObject* __fastcall TdsDocClassClient::GetQList(System::UnicodeString AId, const String& ARequestFilter)
{
  if (FGetQListCommand == NULL)
  {
    FGetQListCommand = FConnection->CreateCommand();
    FGetQListCommand->RequestType = "GET";
    FGetQListCommand->Text = "TdsDocClass.GetQList";
    FGetQListCommand->Prepare(TdsDocClass_GetQList, 1);
  }
  FGetQListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetQListCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetQListCommand->Parameters->Parameter[1]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsDocClassClient::GetQList_Cache(System::UnicodeString AId, const String& ARequestFilter)
{
  if (FGetQListCommand_Cache == NULL)
  {
    FGetQListCommand_Cache = FConnection->CreateCommand();
    FGetQListCommand_Cache->RequestType = "GET";
    FGetQListCommand_Cache->Text = "TdsDocClass.GetQList";
    FGetQListCommand_Cache->Prepare(TdsDocClass_GetQList_Cache, 1);
  }
  FGetQListCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetQListCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetQListCommand_Cache->Parameters->Parameter[1]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

void __fastcall TdsDocClassClient::SetQList(System::UnicodeString AId, TJSONObject* AList)
{
  if (FSetQListCommand == NULL)
  {
    FSetQListCommand = FConnection->CreateCommand();
    FSetQListCommand->RequestType = "POST";
    FSetQListCommand->Text = "TdsDocClass.\"SetQList\"";
    FSetQListCommand->Prepare(TdsDocClass_SetQList, 1);
  }
  FSetQListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FSetQListCommand->Parameters->Parameter[1]->Value->SetJSONValue(AList, FInstanceOwner);
  FSetQListCommand->Execute();
}

System::UnicodeString __fastcall TdsDocClassClient::GetFilterByGUI(System::UnicodeString AGUI, const String& ARequestFilter)
{
  if (FGetFilterByGUICommand == NULL)
  {
    FGetFilterByGUICommand = FConnection->CreateCommand();
    FGetFilterByGUICommand->RequestType = "GET";
    FGetFilterByGUICommand->Text = "TdsDocClass.GetFilterByGUI";
    FGetFilterByGUICommand->Prepare(TdsDocClass_GetFilterByGUI, 1);
  }
  FGetFilterByGUICommand->Parameters->Parameter[0]->Value->SetWideString(AGUI);
  FGetFilterByGUICommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetFilterByGUICommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

bool __fastcall TdsDocClassClient::CanHandEdit(System::UnicodeString ASpecGUI, const String& ARequestFilter)
{
  if (FCanHandEditCommand == NULL)
  {
    FCanHandEditCommand = FConnection->CreateCommand();
    FCanHandEditCommand->RequestType = "GET";
    FCanHandEditCommand->Text = "TdsDocClass.CanHandEdit";
    FCanHandEditCommand->Prepare(TdsDocClass_CanHandEdit, 1);
  }
  FCanHandEditCommand->Parameters->Parameter[0]->Value->SetWideString(ASpecGUI);
  FCanHandEditCommand->Execute(ARequestFilter);
  bool result = FCanHandEditCommand->Parameters->Parameter[1]->Value->GetBoolean();
  return result;
}


__fastcall  TdsDocClassClient::TdsDocClassClient(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TdsDocClassClient::TdsDocClassClient(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TdsDocClassClient::~TdsDocClassClient()
{
  delete FDisconnectCommand;
  delete FGetDefXMLCommand;
  delete FGetClassXMLCommand;
  delete FGetClassZIPXMLCommand;
  delete FGetCountCommand;
  delete FGetIdListCommand;
  delete FGetIdListCommand_Cache;
  delete FGetValByIdCommand;
  delete FGetValByIdCommand_Cache;
  delete FGetValById10Command;
  delete FGetValById10Command_Cache;
  delete FFindCommand;
  delete FFindCommand_Cache;
  delete FImportDataCommand;
  delete FInsertDataCommand;
  delete FEditDataCommand;
  delete FDeleteDataCommand;
  delete FSetTextDataCommand;
  delete FGetTextDataCommand;
  delete FGetSpecByIdCommand;
  delete FGetDocByIdCommand;
  delete FCreateDocPreviewCommand;
  delete FSaveDocCommand;
  delete FCheckByPeriodCommand;
  delete FDocListClearByDateCommand;
  delete FGetDocCountBySpecCommand;
  delete FSetMainLPUCommand;
  delete FGetMainLPUParamCommand;
  delete FMainOwnerCodeCommand;
  delete FIsMainOwnerCodeCommand;
  delete FCopySpecCommand;
  delete FCheckSpecExistsCommand;
  delete FCheckSpecExistsByCodeCommand;
  delete FCheckDocCreateTypeCommand;
  delete FDocCreateCheckExistCommand;
  delete FCheckFilterCommand;
  delete FDocCreateCheckFilterCommand;
  delete FDocCreateCommand;
  delete FStartDocCreateCommand;
  delete FDocCreateProgressCommand;
  delete FLoadDocPreviewProgressCommand;
  delete FGetQListCommand;
  delete FGetQListCommand_Cache;
  delete FSetQListCommand;
  delete FGetFilterByGUICommand;
  delete FCanHandEditCommand;
}

void __fastcall TdsVacStoreClassClient::Disconnect()
{
  if (FDisconnectCommand == NULL)
  {
    FDisconnectCommand = FConnection->CreateCommand();
    FDisconnectCommand->RequestType = "GET";
    FDisconnectCommand->Text = "TdsVacStoreClass.Disconnect";
  }
  FDisconnectCommand->Execute();
}

System::UnicodeString __fastcall TdsVacStoreClassClient::GetDefXML(const String& ARequestFilter)
{
  if (FGetDefXMLCommand == NULL)
  {
    FGetDefXMLCommand = FConnection->CreateCommand();
    FGetDefXMLCommand->RequestType = "GET";
    FGetDefXMLCommand->Text = "TdsVacStoreClass.GetDefXML";
    FGetDefXMLCommand->Prepare(TdsVacStoreClass_GetDefXML, 0);
  }
  FGetDefXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetDefXMLCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::GetClassXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassXMLCommand == NULL)
  {
    FGetClassXMLCommand = FConnection->CreateCommand();
    FGetClassXMLCommand->RequestType = "GET";
    FGetClassXMLCommand->Text = "TdsVacStoreClass.GetClassXML";
    FGetClassXMLCommand->Prepare(TdsVacStoreClass_GetClassXML, 1);
  }
  FGetClassXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassZIPXMLCommand == NULL)
  {
    FGetClassZIPXMLCommand = FConnection->CreateCommand();
    FGetClassZIPXMLCommand->RequestType = "GET";
    FGetClassZIPXMLCommand->Text = "TdsVacStoreClass.GetClassZIPXML";
    FGetClassZIPXMLCommand->Prepare(TdsVacStoreClass_GetClassZIPXML, 1);
  }
  FGetClassZIPXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassZIPXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassZIPXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

__int64 __fastcall TdsVacStoreClassClient::GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetCountCommand == NULL)
  {
    FGetCountCommand = FConnection->CreateCommand();
    FGetCountCommand->RequestType = "GET";
    FGetCountCommand->Text = "TdsVacStoreClass.GetCount";
    FGetCountCommand->Prepare(TdsVacStoreClass_GetCount, 2);
  }
  FGetCountCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetCountCommand->Parameters->Parameter[1]->Value->SetWideString(AFilterParam);
  FGetCountCommand->Execute(ARequestFilter);
  __int64 result = FGetCountCommand->Parameters->Parameter[2]->Value->GetInt64();
  return result;
}

TJSONObject* __fastcall TdsVacStoreClassClient::GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand == NULL)
  {
    FGetIdListCommand = FConnection->CreateCommand();
    FGetIdListCommand->RequestType = "GET";
    FGetIdListCommand->Text = "TdsVacStoreClass.GetIdList";
    FGetIdListCommand->Prepare(TdsVacStoreClass_GetIdList, 3);
  }
  FGetIdListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetIdListCommand->Parameters->Parameter[3]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsVacStoreClassClient::GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand_Cache == NULL)
  {
    FGetIdListCommand_Cache = FConnection->CreateCommand();
    FGetIdListCommand_Cache->RequestType = "GET";
    FGetIdListCommand_Cache->Text = "TdsVacStoreClass.GetIdList";
    FGetIdListCommand_Cache->Prepare(TdsVacStoreClass_GetIdList_Cache, 3);
  }
  FGetIdListCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand_Cache->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand_Cache->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetIdListCommand_Cache->Parameters->Parameter[3]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsVacStoreClassClient::GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand == NULL)
  {
    FGetValByIdCommand = FConnection->CreateCommand();
    FGetValByIdCommand->RequestType = "GET";
    FGetValByIdCommand->Text = "TdsVacStoreClass.GetValById";
    FGetValByIdCommand->Prepare(TdsVacStoreClass_GetValById, 2);
  }
  FGetValByIdCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValByIdCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsVacStoreClassClient::GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand_Cache == NULL)
  {
    FGetValByIdCommand_Cache = FConnection->CreateCommand();
    FGetValByIdCommand_Cache->RequestType = "GET";
    FGetValByIdCommand_Cache->Text = "TdsVacStoreClass.GetValById";
    FGetValByIdCommand_Cache->Prepare(TdsVacStoreClass_GetValById_Cache, 2);
  }
  FGetValByIdCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValByIdCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsVacStoreClassClient::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command == NULL)
  {
    FGetValById10Command = FConnection->CreateCommand();
    FGetValById10Command->RequestType = "GET";
    FGetValById10Command->Text = "TdsVacStoreClass.GetValById10";
    FGetValById10Command->Prepare(TdsVacStoreClass_GetValById10, 2);
  }
  FGetValById10Command->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValById10Command->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsVacStoreClassClient::GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command_Cache == NULL)
  {
    FGetValById10Command_Cache = FConnection->CreateCommand();
    FGetValById10Command_Cache->RequestType = "GET";
    FGetValById10Command_Cache->Text = "TdsVacStoreClass.GetValById10";
    FGetValById10Command_Cache->Prepare(TdsVacStoreClass_GetValById10_Cache, 2);
  }
  FGetValById10Command_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValById10Command_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsVacStoreClassClient::Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand == NULL)
  {
    FFindCommand = FConnection->CreateCommand();
    FFindCommand->RequestType = "GET";
    FFindCommand->Text = "TdsVacStoreClass.Find";
    FFindCommand->Prepare(TdsVacStoreClass_Find, 2);
  }
  FFindCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsVacStoreClassClient::Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand_Cache == NULL)
  {
    FFindCommand_Cache = FConnection->CreateCommand();
    FFindCommand_Cache->RequestType = "GET";
    FFindCommand_Cache->Text = "TdsVacStoreClass.Find";
    FFindCommand_Cache->Prepare(TdsVacStoreClass_Find_Cache, 2);
  }
  FFindCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FInsertDataCommand == NULL)
  {
    FInsertDataCommand = FConnection->CreateCommand();
    FInsertDataCommand->RequestType = "POST";
    FInsertDataCommand->Text = "TdsVacStoreClass.\"InsertData\"";
    FInsertDataCommand->Prepare(TdsVacStoreClass_InsertData, 3);
  }
  FInsertDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FInsertDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FInsertDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FInsertDataCommand->Execute(ARequestFilter);
  ARecId = FInsertDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FInsertDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FEditDataCommand == NULL)
  {
    FEditDataCommand = FConnection->CreateCommand();
    FEditDataCommand->RequestType = "POST";
    FEditDataCommand->Text = "TdsVacStoreClass.\"EditData\"";
    FEditDataCommand->Prepare(TdsVacStoreClass_EditData, 3);
  }
  FEditDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FEditDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FEditDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FEditDataCommand->Execute(ARequestFilter);
  ARecId = FEditDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FEditDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FDeleteDataCommand == NULL)
  {
    FDeleteDataCommand = FConnection->CreateCommand();
    FDeleteDataCommand->RequestType = "GET";
    FDeleteDataCommand->Text = "TdsVacStoreClass.DeleteData";
    FDeleteDataCommand->Prepare(TdsVacStoreClass_DeleteData, 2);
  }
  FDeleteDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FDeleteDataCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FDeleteDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDeleteDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal, const String& ARequestFilter)
{
  if (FSetTextDataCommand == NULL)
  {
    FSetTextDataCommand = FConnection->CreateCommand();
    FSetTextDataCommand->RequestType = "GET";
    FSetTextDataCommand->Text = "TdsVacStoreClass.SetTextData";
    FSetTextDataCommand->Prepare(TdsVacStoreClass_SetTextData, 4);
  }
  FSetTextDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FSetTextDataCommand->Parameters->Parameter[1]->Value->SetWideString(AFlId);
  FSetTextDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FSetTextDataCommand->Parameters->Parameter[3]->Value->SetWideString(AVal);
  FSetTextDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FSetTextDataCommand->Parameters->Parameter[4]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString  &AMsg, const String& ARequestFilter)
{
  if (FGetTextDataCommand == NULL)
  {
    FGetTextDataCommand = FConnection->CreateCommand();
    FGetTextDataCommand->RequestType = "GET";
    FGetTextDataCommand->Text = "TdsVacStoreClass.GetTextData";
    FGetTextDataCommand->Prepare(TdsVacStoreClass_GetTextData, 4);
  }
  FGetTextDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetTextDataCommand->Parameters->Parameter[1]->Value->SetWideString(AFlId);
  FGetTextDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FGetTextDataCommand->Parameters->Parameter[3]->Value->SetWideString(AMsg);
  FGetTextDataCommand->Execute(ARequestFilter);
  AMsg = FGetTextDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  System::UnicodeString result = FGetTextDataCommand->Parameters->Parameter[4]->Value->GetWideString();
  return result;
}

void __fastcall TdsVacStoreClassClient::SetMainLPU(__int64 ALPUCode)
{
  if (FSetMainLPUCommand == NULL)
  {
    FSetMainLPUCommand = FConnection->CreateCommand();
    FSetMainLPUCommand->RequestType = "GET";
    FSetMainLPUCommand->Text = "TdsVacStoreClass.SetMainLPU";
    FSetMainLPUCommand->Prepare(TdsVacStoreClass_SetMainLPU, 0);
  }
  FSetMainLPUCommand->Parameters->Parameter[0]->Value->SetInt64(ALPUCode);
  FSetMainLPUCommand->Execute();
}

System::UnicodeString __fastcall TdsVacStoreClassClient::MainOwnerCode(const String& ARequestFilter)
{
  if (FMainOwnerCodeCommand == NULL)
  {
    FMainOwnerCodeCommand = FConnection->CreateCommand();
    FMainOwnerCodeCommand->RequestType = "GET";
    FMainOwnerCodeCommand->Text = "TdsVacStoreClass.MainOwnerCode";
    FMainOwnerCodeCommand->Prepare(TdsVacStoreClass_MainOwnerCode, 0);
  }
  FMainOwnerCodeCommand->Execute(ARequestFilter);
  System::UnicodeString result = FMainOwnerCodeCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

bool __fastcall TdsVacStoreClassClient::IsMainOwnerCode(System::UnicodeString AOwnerCode, const String& ARequestFilter)
{
  if (FIsMainOwnerCodeCommand == NULL)
  {
    FIsMainOwnerCodeCommand = FConnection->CreateCommand();
    FIsMainOwnerCodeCommand->RequestType = "GET";
    FIsMainOwnerCodeCommand->Text = "TdsVacStoreClass.IsMainOwnerCode";
    FIsMainOwnerCodeCommand->Prepare(TdsVacStoreClass_IsMainOwnerCode, 1);
  }
  FIsMainOwnerCodeCommand->Parameters->Parameter[0]->Value->SetWideString(AOwnerCode);
  FIsMainOwnerCodeCommand->Execute(ARequestFilter);
  bool result = FIsMainOwnerCodeCommand->Parameters->Parameter[1]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsVacStoreClassClient::GetMIBPForm(const String& ARequestFilter)
{
  if (FGetMIBPFormCommand == NULL)
  {
    FGetMIBPFormCommand = FConnection->CreateCommand();
    FGetMIBPFormCommand->RequestType = "GET";
    FGetMIBPFormCommand->Text = "TdsVacStoreClass.GetMIBPForm";
    FGetMIBPFormCommand->Prepare(TdsVacStoreClass_GetMIBPForm, 0);
  }
  FGetMIBPFormCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetMIBPFormCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

TJSONObject* __fastcall TdsVacStoreClassClient::GetMIBPList(System::UnicodeString AId, const String& ARequestFilter)
{
  if (FGetMIBPListCommand == NULL)
  {
    FGetMIBPListCommand = FConnection->CreateCommand();
    FGetMIBPListCommand->RequestType = "GET";
    FGetMIBPListCommand->Text = "TdsVacStoreClass.GetMIBPList";
    FGetMIBPListCommand->Prepare(TdsVacStoreClass_GetMIBPList, 1);
  }
  FGetMIBPListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetMIBPListCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetMIBPListCommand->Parameters->Parameter[1]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsVacStoreClassClient::GetMIBPList_Cache(System::UnicodeString AId, const String& ARequestFilter)
{
  if (FGetMIBPListCommand_Cache == NULL)
  {
    FGetMIBPListCommand_Cache = FConnection->CreateCommand();
    FGetMIBPListCommand_Cache->RequestType = "GET";
    FGetMIBPListCommand_Cache->Text = "TdsVacStoreClass.GetMIBPList";
    FGetMIBPListCommand_Cache->Prepare(TdsVacStoreClass_GetMIBPList_Cache, 1);
  }
  FGetMIBPListCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetMIBPListCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetMIBPListCommand_Cache->Parameters->Parameter[1]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}


__fastcall  TdsVacStoreClassClient::TdsVacStoreClassClient(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TdsVacStoreClassClient::TdsVacStoreClassClient(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TdsVacStoreClassClient::~TdsVacStoreClassClient()
{
  delete FDisconnectCommand;
  delete FGetDefXMLCommand;
  delete FGetClassXMLCommand;
  delete FGetClassZIPXMLCommand;
  delete FGetCountCommand;
  delete FGetIdListCommand;
  delete FGetIdListCommand_Cache;
  delete FGetValByIdCommand;
  delete FGetValByIdCommand_Cache;
  delete FGetValById10Command;
  delete FGetValById10Command_Cache;
  delete FFindCommand;
  delete FFindCommand_Cache;
  delete FInsertDataCommand;
  delete FEditDataCommand;
  delete FDeleteDataCommand;
  delete FSetTextDataCommand;
  delete FGetTextDataCommand;
  delete FSetMainLPUCommand;
  delete FMainOwnerCodeCommand;
  delete FIsMainOwnerCodeCommand;
  delete FGetMIBPFormCommand;
  delete FGetMIBPListCommand;
  delete FGetMIBPListCommand_Cache;
}

System::UnicodeString __fastcall TdsEIDataClassClient::ImportData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, System::UnicodeString  &AResMessage, const String& ARequestFilter)
{
  if (FImportDataCommand == NULL)
  {
    FImportDataCommand = FConnection->CreateCommand();
    FImportDataCommand->RequestType = "POST";
    FImportDataCommand->Text = "TdsEIDataClass.\"ImportData\"";
    FImportDataCommand->Prepare(TdsEIDataClass_ImportData, 4);
  }
  FImportDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FImportDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FImportDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FImportDataCommand->Parameters->Parameter[3]->Value->SetWideString(AResMessage);
  FImportDataCommand->Execute(ARequestFilter);
  ARecId = FImportDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  AResMessage = FImportDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  System::UnicodeString result = FImportDataCommand->Parameters->Parameter[4]->Value->GetWideString();
  return result;
}

bool __fastcall TdsEIDataClassClient::UploadData(System::UnicodeString ADataPart, int AType, const String& ARequestFilter)
{
  if (FUploadDataCommand == NULL)
  {
    FUploadDataCommand = FConnection->CreateCommand();
    FUploadDataCommand->RequestType = "GET";
    FUploadDataCommand->Text = "TdsEIDataClass.UploadData";
    FUploadDataCommand->Prepare(TdsEIDataClass_UploadData, 2);
  }
  FUploadDataCommand->Parameters->Parameter[0]->Value->SetWideString(ADataPart);
  FUploadDataCommand->Parameters->Parameter[1]->Value->SetInt32(AType);
  FUploadDataCommand->Execute(ARequestFilter);
  bool result = FUploadDataCommand->Parameters->Parameter[2]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsEIDataClassClient::GetExportDataHeader(const String& ARequestFilter)
{
  if (FGetExportDataHeaderCommand == NULL)
  {
    FGetExportDataHeaderCommand = FConnection->CreateCommand();
    FGetExportDataHeaderCommand->RequestType = "GET";
    FGetExportDataHeaderCommand->Text = "TdsEIDataClass.GetExportDataHeader";
    FGetExportDataHeaderCommand->Prepare(TdsEIDataClass_GetExportDataHeader, 0);
  }
  FGetExportDataHeaderCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetExportDataHeaderCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsEIDataClassClient::GetPatDataById(System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetPatDataByIdCommand == NULL)
  {
    FGetPatDataByIdCommand = FConnection->CreateCommand();
    FGetPatDataByIdCommand->RequestType = "GET";
    FGetPatDataByIdCommand->Text = "TdsEIDataClass.GetPatDataById";
    FGetPatDataByIdCommand->Prepare(TdsEIDataClass_GetPatDataById, 1);
  }
  FGetPatDataByIdCommand->Parameters->Parameter[0]->Value->SetWideString(ARecId);
  FGetPatDataByIdCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetPatDataByIdCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsEIDataClassClient::GetPatCardDataById(System::UnicodeString ARecId, TJSONObject* AClsRC, const String& ARequestFilter)
{
  if (FGetPatCardDataByIdCommand == NULL)
  {
    FGetPatCardDataByIdCommand = FConnection->CreateCommand();
    FGetPatCardDataByIdCommand->RequestType = "POST";
    FGetPatCardDataByIdCommand->Text = "TdsEIDataClass.\"GetPatCardDataById\"";
    FGetPatCardDataByIdCommand->Prepare(TdsEIDataClass_GetPatCardDataById, 2);
  }
  FGetPatCardDataByIdCommand->Parameters->Parameter[0]->Value->SetWideString(ARecId);
  FGetPatCardDataByIdCommand->Parameters->Parameter[1]->Value->SetJSONValue(AClsRC, FInstanceOwner);
  FGetPatCardDataByIdCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetPatCardDataByIdCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}


__fastcall  TdsEIDataClassClient::TdsEIDataClassClient(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TdsEIDataClassClient::TdsEIDataClassClient(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TdsEIDataClassClient::~TdsEIDataClassClient()
{
  delete FImportDataCommand;
  delete FUploadDataCommand;
  delete FGetExportDataHeaderCommand;
  delete FGetPatDataByIdCommand;
  delete FGetPatCardDataByIdCommand;
}

void __fastcall TdsAdminClassClient::MigrateScriptProgress(TJSONObject* Sender)
{
  if (FMigrateScriptProgressCommand == NULL)
  {
    FMigrateScriptProgressCommand = FConnection->CreateCommand();
    FMigrateScriptProgressCommand->RequestType = "POST";
    FMigrateScriptProgressCommand->Text = "TdsAdminClass.\"MigrateScriptProgress\"";
    FMigrateScriptProgressCommand->Prepare(TdsAdminClass_MigrateScriptProgress, 0);
  }
  FMigrateScriptProgressCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FMigrateScriptProgressCommand->Execute();
}

void __fastcall TdsAdminClassClient::Disconnect()
{
  if (FDisconnectCommand == NULL)
  {
    FDisconnectCommand = FConnection->CreateCommand();
    FDisconnectCommand->RequestType = "GET";
    FDisconnectCommand->Text = "TdsAdminClass.Disconnect";
  }
  FDisconnectCommand->Execute();
}

System::UnicodeString __fastcall TdsAdminClassClient::GetDefXML(const String& ARequestFilter)
{
  if (FGetDefXMLCommand == NULL)
  {
    FGetDefXMLCommand = FConnection->CreateCommand();
    FGetDefXMLCommand->RequestType = "GET";
    FGetDefXMLCommand->Text = "TdsAdminClass.GetDefXML";
    FGetDefXMLCommand->Prepare(TdsAdminClass_GetDefXML, 0);
  }
  FGetDefXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetDefXMLCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsAdminClassClient::GetClassXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassXMLCommand == NULL)
  {
    FGetClassXMLCommand = FConnection->CreateCommand();
    FGetClassXMLCommand->RequestType = "GET";
    FGetClassXMLCommand->Text = "TdsAdminClass.GetClassXML";
    FGetClassXMLCommand->Prepare(TdsAdminClass_GetClassXML, 1);
  }
  FGetClassXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsAdminClassClient::GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FGetClassZIPXMLCommand == NULL)
  {
    FGetClassZIPXMLCommand = FConnection->CreateCommand();
    FGetClassZIPXMLCommand->RequestType = "GET";
    FGetClassZIPXMLCommand->Text = "TdsAdminClass.GetClassZIPXML";
    FGetClassZIPXMLCommand->Prepare(TdsAdminClass_GetClassZIPXML, 1);
  }
  FGetClassZIPXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FGetClassZIPXMLCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetClassZIPXMLCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

bool __fastcall TdsAdminClassClient::SetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter)
{
  if (FSetClassZIPXMLCommand == NULL)
  {
    FSetClassZIPXMLCommand = FConnection->CreateCommand();
    FSetClassZIPXMLCommand->RequestType = "GET";
    FSetClassZIPXMLCommand->Text = "TdsAdminClass.SetClassZIPXML";
    FSetClassZIPXMLCommand->Prepare(TdsAdminClass_SetClassZIPXML, 1);
  }
  FSetClassZIPXMLCommand->Parameters->Parameter[0]->Value->SetWideString(ARef);
  FSetClassZIPXMLCommand->Execute(ARequestFilter);
  bool result = FSetClassZIPXMLCommand->Parameters->Parameter[1]->Value->GetBoolean();
  return result;
}

__int64 __fastcall TdsAdminClassClient::GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetCountCommand == NULL)
  {
    FGetCountCommand = FConnection->CreateCommand();
    FGetCountCommand->RequestType = "GET";
    FGetCountCommand->Text = "TdsAdminClass.GetCount";
    FGetCountCommand->Prepare(TdsAdminClass_GetCount, 2);
  }
  FGetCountCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetCountCommand->Parameters->Parameter[1]->Value->SetWideString(AFilterParam);
  FGetCountCommand->Execute(ARequestFilter);
  __int64 result = FGetCountCommand->Parameters->Parameter[2]->Value->GetInt64();
  return result;
}

TJSONObject* __fastcall TdsAdminClassClient::GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand == NULL)
  {
    FGetIdListCommand = FConnection->CreateCommand();
    FGetIdListCommand->RequestType = "GET";
    FGetIdListCommand->Text = "TdsAdminClass.GetIdList";
    FGetIdListCommand->Prepare(TdsAdminClass_GetIdList, 3);
  }
  FGetIdListCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetIdListCommand->Parameters->Parameter[3]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsAdminClassClient::GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter)
{
  if (FGetIdListCommand_Cache == NULL)
  {
    FGetIdListCommand_Cache = FConnection->CreateCommand();
    FGetIdListCommand_Cache->RequestType = "GET";
    FGetIdListCommand_Cache->Text = "TdsAdminClass.GetIdList";
    FGetIdListCommand_Cache->Prepare(TdsAdminClass_GetIdList_Cache, 3);
  }
  FGetIdListCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetIdListCommand_Cache->Parameters->Parameter[1]->Value->SetInt32(AMax);
  FGetIdListCommand_Cache->Parameters->Parameter[2]->Value->SetWideString(AFilterParam);
  FGetIdListCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetIdListCommand_Cache->Parameters->Parameter[3]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsAdminClassClient::GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand == NULL)
  {
    FGetValByIdCommand = FConnection->CreateCommand();
    FGetValByIdCommand->RequestType = "GET";
    FGetValByIdCommand->Text = "TdsAdminClass.GetValById";
    FGetValByIdCommand->Prepare(TdsAdminClass_GetValById, 2);
  }
  FGetValByIdCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValByIdCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsAdminClassClient::GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValByIdCommand_Cache == NULL)
  {
    FGetValByIdCommand_Cache = FConnection->CreateCommand();
    FGetValByIdCommand_Cache->RequestType = "GET";
    FGetValByIdCommand_Cache->Text = "TdsAdminClass.GetValById";
    FGetValByIdCommand_Cache->Prepare(TdsAdminClass_GetValById_Cache, 2);
  }
  FGetValByIdCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValByIdCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValByIdCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValByIdCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsAdminClassClient::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command == NULL)
  {
    FGetValById10Command = FConnection->CreateCommand();
    FGetValById10Command->RequestType = "GET";
    FGetValById10Command->Text = "TdsAdminClass.GetValById10";
    FGetValById10Command->Prepare(TdsAdminClass_GetValById10, 2);
  }
  FGetValById10Command->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FGetValById10Command->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsAdminClassClient::GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FGetValById10Command_Cache == NULL)
  {
    FGetValById10Command_Cache = FConnection->CreateCommand();
    FGetValById10Command_Cache->RequestType = "GET";
    FGetValById10Command_Cache->Text = "TdsAdminClass.GetValById10";
    FGetValById10Command_Cache->Prepare(TdsAdminClass_GetValById10_Cache, 2);
  }
  FGetValById10Command_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FGetValById10Command_Cache->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FGetValById10Command_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FGetValById10Command_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsAdminClassClient::Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand == NULL)
  {
    FFindCommand = FConnection->CreateCommand();
    FFindCommand->RequestType = "GET";
    FFindCommand->Text = "TdsAdminClass.Find";
    FFindCommand->Prepare(TdsAdminClass_Find, 2);
  }
  FFindCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsAdminClassClient::Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindCommand_Cache == NULL)
  {
    FFindCommand_Cache = FConnection->CreateCommand();
    FFindCommand_Cache->RequestType = "GET";
    FFindCommand_Cache->Text = "TdsAdminClass.Find";
    FFindCommand_Cache->Prepare(TdsAdminClass_Find_Cache, 2);
  }
  FFindCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

TJSONObject* __fastcall TdsAdminClassClient::FindEx(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindExCommand == NULL)
  {
    FFindExCommand = FConnection->CreateCommand();
    FFindExCommand->RequestType = "GET";
    FFindExCommand->Text = "TdsAdminClass.FindEx";
    FFindExCommand->Prepare(TdsAdminClass_FindEx, 2);
  }
  FFindExCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindExCommand->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindExCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindExCommand->Parameters->Parameter[2]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsAdminClassClient::FindEx_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter)
{
  if (FFindExCommand_Cache == NULL)
  {
    FFindExCommand_Cache = FConnection->CreateCommand();
    FFindExCommand_Cache->RequestType = "GET";
    FFindExCommand_Cache->Text = "TdsAdminClass.FindEx";
    FFindExCommand_Cache->Prepare(TdsAdminClass_FindEx_Cache, 2);
  }
  FFindExCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AId);
  FFindExCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(AFindData);
  FFindExCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindExCommand_Cache->Parameters->Parameter[2]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsAdminClassClient::InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FInsertDataCommand == NULL)
  {
    FInsertDataCommand = FConnection->CreateCommand();
    FInsertDataCommand->RequestType = "POST";
    FInsertDataCommand->Text = "TdsAdminClass.\"InsertData\"";
    FInsertDataCommand->Prepare(TdsAdminClass_InsertData, 3);
  }
  FInsertDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FInsertDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FInsertDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FInsertDataCommand->Execute(ARequestFilter);
  ARecId = FInsertDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FInsertDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsAdminClassClient::EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter)
{
  if (FEditDataCommand == NULL)
  {
    FEditDataCommand = FConnection->CreateCommand();
    FEditDataCommand->RequestType = "POST";
    FEditDataCommand->Text = "TdsAdminClass.\"EditData\"";
    FEditDataCommand->Prepare(TdsAdminClass_EditData, 3);
  }
  FEditDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FEditDataCommand->Parameters->Parameter[1]->Value->SetJSONValue(AValue, FInstanceOwner);
  FEditDataCommand->Parameters->Parameter[2]->Value->SetWideString(ARecId);
  FEditDataCommand->Execute(ARequestFilter);
  ARecId = FEditDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  System::UnicodeString result = FEditDataCommand->Parameters->Parameter[3]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdsAdminClassClient::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter)
{
  if (FDeleteDataCommand == NULL)
  {
    FDeleteDataCommand = FConnection->CreateCommand();
    FDeleteDataCommand->RequestType = "GET";
    FDeleteDataCommand->Text = "TdsAdminClass.DeleteData";
    FDeleteDataCommand->Prepare(TdsAdminClass_DeleteData, 2);
  }
  FDeleteDataCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FDeleteDataCommand->Parameters->Parameter[1]->Value->SetWideString(ARecId);
  FDeleteDataCommand->Execute(ARequestFilter);
  System::UnicodeString result = FDeleteDataCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}

bool __fastcall TdsAdminClassClient::UploadData(System::UnicodeString ADataPart, int AType, const String& ARequestFilter)
{
  if (FUploadDataCommand == NULL)
  {
    FUploadDataCommand = FConnection->CreateCommand();
    FUploadDataCommand->RequestType = "GET";
    FUploadDataCommand->Text = "TdsAdminClass.UploadData";
    FUploadDataCommand->Prepare(TdsAdminClass_UploadData, 2);
  }
  FUploadDataCommand->Parameters->Parameter[0]->Value->SetWideString(ADataPart);
  FUploadDataCommand->Parameters->Parameter[1]->Value->SetInt32(AType);
  FUploadDataCommand->Execute(ARequestFilter);
  bool result = FUploadDataCommand->Parameters->Parameter[2]->Value->GetBoolean();
  return result;
}

bool __fastcall TdsAdminClassClient::CheckUser(System::UnicodeString AUser, System::UnicodeString APasswd, bool  &AValid, const String& ARequestFilter)
{
  if (FCheckUserCommand == NULL)
  {
    FCheckUserCommand = FConnection->CreateCommand();
    FCheckUserCommand->RequestType = "GET";
    FCheckUserCommand->Text = "TdsAdminClass.CheckUser";
    FCheckUserCommand->Prepare(TdsAdminClass_CheckUser, 3);
  }
  FCheckUserCommand->Parameters->Parameter[0]->Value->SetWideString(AUser);
  FCheckUserCommand->Parameters->Parameter[1]->Value->SetWideString(APasswd);
  FCheckUserCommand->Parameters->Parameter[2]->Value->SetBoolean(AValid);
  FCheckUserCommand->Execute(ARequestFilter);
  AValid = FCheckUserCommand->Parameters->Parameter[2]->Value->GetBoolean();
  bool result = FCheckUserCommand->Parameters->Parameter[3]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsAdminClassClient::LoginUser(System::UnicodeString AId, const String& ARequestFilter)
{
  if (FLoginUserCommand == NULL)
  {
    FLoginUserCommand = FConnection->CreateCommand();
    FLoginUserCommand->RequestType = "GET";
    FLoginUserCommand->Text = "TdsAdminClass.LoginUser";
    FLoginUserCommand->Prepare(TdsAdminClass_LoginUser, 1);
  }
  FLoginUserCommand->Parameters->Parameter[0]->Value->SetWideString(AId);
  FLoginUserCommand->Execute(ARequestFilter);
  System::UnicodeString result = FLoginUserCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TdsAdminClassClient::Logout()
{
  if (FLogoutCommand == NULL)
  {
    FLogoutCommand = FConnection->CreateCommand();
    FLogoutCommand->RequestType = "GET";
    FLogoutCommand->Text = "TdsAdminClass.Logout";
  }
  FLogoutCommand->Execute();
}

bool __fastcall TdsAdminClassClient::CheckRT(System::UnicodeString AURT, System::UnicodeString AFunc, const String& ARequestFilter)
{
  if (FCheckRTCommand == NULL)
  {
    FCheckRTCommand = FConnection->CreateCommand();
    FCheckRTCommand->RequestType = "GET";
    FCheckRTCommand->Text = "TdsAdminClass.CheckRT";
    FCheckRTCommand->Prepare(TdsAdminClass_CheckRT, 2);
  }
  FCheckRTCommand->Parameters->Parameter[0]->Value->SetWideString(AURT);
  FCheckRTCommand->Parameters->Parameter[1]->Value->SetWideString(AFunc);
  FCheckRTCommand->Execute(ARequestFilter);
  bool result = FCheckRTCommand->Parameters->Parameter[2]->Value->GetBoolean();
  return result;
}

System::UnicodeString __fastcall TdsAdminClassClient::GetUpdateCode(const String& ARequestFilter)
{
  if (FGetUpdateCodeCommand == NULL)
  {
    FGetUpdateCodeCommand = FConnection->CreateCommand();
    FGetUpdateCodeCommand->RequestType = "GET";
    FGetUpdateCodeCommand->Text = "TdsAdminClass.GetUpdateCode";
    FGetUpdateCodeCommand->Prepare(TdsAdminClass_GetUpdateCode, 0);
  }
  FGetUpdateCodeCommand->Execute(ARequestFilter);
  System::UnicodeString result = FGetUpdateCodeCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

TJSONObject* __fastcall TdsAdminClassClient::FindAddr(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter)
{
  if (FFindAddrCommand == NULL)
  {
    FFindAddrCommand = FConnection->CreateCommand();
    FFindAddrCommand->RequestType = "GET";
    FFindAddrCommand->Text = "TdsAdminClass.FindAddr";
    FFindAddrCommand->Prepare(TdsAdminClass_FindAddr, 3);
  }
  FFindAddrCommand->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindAddrCommand->Parameters->Parameter[1]->Value->SetWideString(ADefAddr);
  FFindAddrCommand->Parameters->Parameter[2]->Value->SetInt32(AParams);
  FFindAddrCommand->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FFindAddrCommand->Parameters->Parameter[3]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TdsAdminClassClient::FindAddr_Cache(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter)
{
  if (FFindAddrCommand_Cache == NULL)
  {
    FFindAddrCommand_Cache = FConnection->CreateCommand();
    FFindAddrCommand_Cache->RequestType = "GET";
    FFindAddrCommand_Cache->Text = "TdsAdminClass.FindAddr";
    FFindAddrCommand_Cache->Prepare(TdsAdminClass_FindAddr_Cache, 3);
  }
  FFindAddrCommand_Cache->Parameters->Parameter[0]->Value->SetWideString(AStr);
  FFindAddrCommand_Cache->Parameters->Parameter[1]->Value->SetWideString(ADefAddr);
  FFindAddrCommand_Cache->Parameters->Parameter[2]->Value->SetInt32(AParams);
  FFindAddrCommand_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FFindAddrCommand_Cache->Parameters->Parameter[3]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TdsAdminClassClient::AddrCodeToText(System::UnicodeString ACode, int AParams, const String& ARequestFilter)
{
  if (FAddrCodeToTextCommand == NULL)
  {
    FAddrCodeToTextCommand = FConnection->CreateCommand();
    FAddrCodeToTextCommand->RequestType = "GET";
    FAddrCodeToTextCommand->Text = "TdsAdminClass.AddrCodeToText";
    FAddrCodeToTextCommand->Prepare(TdsAdminClass_AddrCodeToText, 2);
  }
  FAddrCodeToTextCommand->Parameters->Parameter[0]->Value->SetWideString(ACode);
  FAddrCodeToTextCommand->Parameters->Parameter[1]->Value->SetInt32(AParams);
  FAddrCodeToTextCommand->Execute(ARequestFilter);
  System::UnicodeString result = FAddrCodeToTextCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}


__fastcall  TdsAdminClassClient::TdsAdminClassClient(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TdsAdminClassClient::TdsAdminClassClient(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TdsAdminClassClient::~TdsAdminClassClient()
{
  delete FMigrateScriptProgressCommand;
  delete FDisconnectCommand;
  delete FGetDefXMLCommand;
  delete FGetClassXMLCommand;
  delete FGetClassZIPXMLCommand;
  delete FSetClassZIPXMLCommand;
  delete FGetCountCommand;
  delete FGetIdListCommand;
  delete FGetIdListCommand_Cache;
  delete FGetValByIdCommand;
  delete FGetValByIdCommand_Cache;
  delete FGetValById10Command;
  delete FGetValById10Command_Cache;
  delete FFindCommand;
  delete FFindCommand_Cache;
  delete FFindExCommand;
  delete FFindExCommand_Cache;
  delete FInsertDataCommand;
  delete FEditDataCommand;
  delete FDeleteDataCommand;
  delete FUploadDataCommand;
  delete FCheckUserCommand;
  delete FLoginUserCommand;
  delete FLogoutCommand;
  delete FCheckRTCommand;
  delete FGetUpdateCodeCommand;
  delete FFindAddrCommand;
  delete FFindAddrCommand_Cache;
  delete FAddrCodeToTextCommand;
}

TJSONObject* __fastcall TMISAPIClient::PatientF63(System::UnicodeString APtId, const String& ARequestFilter)
{
  if (FPatientF63Command == NULL)
  {
    FPatientF63Command = FConnection->CreateCommand();
    FPatientF63Command->RequestType = "GET";
    FPatientF63Command->Text = "TMISAPI.PatientF63";
    FPatientF63Command->Prepare(TMISAPI_PatientF63, 1);
  }
  FPatientF63Command->Parameters->Parameter[0]->Value->SetWideString(APtId);
  FPatientF63Command->Execute(ARequestFilter);
  TJSONObject* result = (TJSONObject*)FPatientF63Command->Parameters->Parameter[1]->Value->GetJSONValue(FInstanceOwner);
  return result;
}

_di_IDSRestCachedJSONObject __fastcall TMISAPIClient::PatientF63_Cache(System::UnicodeString APtId, const String& ARequestFilter)
{
  if (FPatientF63Command_Cache == NULL)
  {
    FPatientF63Command_Cache = FConnection->CreateCommand();
    FPatientF63Command_Cache->RequestType = "GET";
    FPatientF63Command_Cache->Text = "TMISAPI.PatientF63";
    FPatientF63Command_Cache->Prepare(TMISAPI_PatientF63_Cache, 1);
  }
  FPatientF63Command_Cache->Parameters->Parameter[0]->Value->SetWideString(APtId);
  FPatientF63Command_Cache->ExecuteCache(ARequestFilter);
  _di_IDSRestCachedJSONObject _resultIntf;
  {
  TDSRestCachedJSONObject* obj(new TDSRestCachedJSONObject(FPatientF63Command_Cache->Parameters->Parameter[1]->Value->GetString()));
  obj->GetInterface(_resultIntf);
  }
  return _resultIntf;
}

System::UnicodeString __fastcall TMISAPIClient::ImportPatient(System::UnicodeString AMIS, TJSONObject* ASrc, const String& ARequestFilter)
{
  if (FImportPatientCommand == NULL)
  {
    FImportPatientCommand = FConnection->CreateCommand();
    FImportPatientCommand->RequestType = "POST";
    FImportPatientCommand->Text = "TMISAPI.\"ImportPatient\"";
    FImportPatientCommand->Prepare(TMISAPI_ImportPatient, 2);
  }
  FImportPatientCommand->Parameters->Parameter[0]->Value->SetWideString(AMIS);
  FImportPatientCommand->Parameters->Parameter[1]->Value->SetJSONValue(ASrc, FInstanceOwner);
  FImportPatientCommand->Execute(ARequestFilter);
  System::UnicodeString result = FImportPatientCommand->Parameters->Parameter[2]->Value->GetWideString();
  return result;
}


__fastcall  TMISAPIClient::TMISAPIClient(TDSRestConnection *ARestConnection): TDSAdminRestClient(ARestConnection)
{
}

__fastcall  TMISAPIClient::TMISAPIClient(TDSRestConnection *ARestConnection, bool AInstanceOwner): TDSAdminRestClient(ARestConnection, AInstanceOwner)
{
}

__fastcall  TMISAPIClient::~TMISAPIClient()
{
  delete FPatientF63Command;
  delete FPatientF63Command_Cache;
  delete FImportPatientCommand;
}


