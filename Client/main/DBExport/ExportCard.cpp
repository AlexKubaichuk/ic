//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ExportCard.h"
#include "ExtUtils.h"
#include "ChoiceList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "TB2Dock"
//#pragma link "TB2Item"
//#pragma link "TB2Toolbar"
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxControls"
#pragma link "cxGraphics"
#pragma link "dxStatusBar"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxCustomData"
#pragma link "cxInplaceContainer"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxDropDownEdit"
#pragma link "cxMaskEdit"
#pragma link "cxLabel"
#pragma link "cxProgressBar"
#pragma link "cxClasses"
#pragma link "cxLookAndFeels"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxStatusBarPainter"
#pragma resource "*.dfm"
TExportCardForm *ExportCardForm;
//---------------------------------------------------------------------------
__fastcall TExportCardForm::TExportCardForm(TComponent* Owner)
        : TForm(Owner)
{
  cmdClient->Host = "localhost";
  if (DM->AppOpt->Vals["eihost"].AsString.Length())
   cmdClient->Host = DM->AppOpt->Vals["eihost"].AsString;
  cmdClient->Port = DM->AppOpt->Vals["eiport"].AsInt;

  InitProgressCtrls();
  DWORD FStatus;
  int RC = ServiceStatus(ConvSrvcName, FStatus);
  if (!RC)
   {
     if (FStatus == SERVICE_RUNNING)
      {
        cmdServerConnect();
        bool FPr = IsConvProgress();
        SetCtrlEnabled(!FPr);
        PrTimer->Enabled = FPr;
        if (!FPr)
         {
           UnicodeString Ret = SendConvCommand("get last export state");
           if (Ret.Length() && Ret.LowerCase() != "ok")
            HintLb->Caption = "��������� ��������� �������� ������:\""+Ret+"\"";
           ServiceStop(ConvSrvcName);
           ServiceStart(ConvSrvcName);
           cmdServerConnect();
         }
      }
     else if (FStatus == SERVICE_STOPPED)
      {
        RC = ServiceStart(ConvSrvcName);
        if (RC)
         {
           _MSG_ERR("������ ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
           ExportBtn->Enabled   = false;
           PrTimer->Enabled    = false;
         }
        else
         {
           cmdServerConnect();
           bool FPr = IsConvProgress();
           SetCtrlEnabled(!FPr);
           PrTimer->Enabled = FPr;
         }
      }
     FilterCB->Properties->Items->Clear();
     FilterCB->Properties->Items->AddObject("",(TObject*)0);
     DM->qtExec("Select CODE, NAME From FILTERS Where CHTYPE = 0 and Upper(MODULE_TYPE) = '"+DM->SubDir.UpperCase()+"' Order By NAME", false);
     while (!DM->quFree->Eof)
      {
        FilterCB->Properties->Items->AddObject(quFNStr("NAME").c_str(),(TObject*)quFNInt("CODE"));
        DM->quFree->Next();
      }
     DM->trFree->Commit();
   }
  else
   {
     _MSG_ERR("������ ����������� ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
     ExportBtn->Enabled = false;
     PrTimer->Enabled     = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::SelFromTemplateBtnClick(TObject *Sender)
{
  TStringList *fCodeList = NULL;
  try
   {
     fCodeList = new TStringList;
     UnicodeString tmp;
     tmp =  "<root>"
            "<fl ref='0021'/>"
            "<separator/>"
            "<fl ref='0036'/>"
            "<separator/>"
            "<fl ref='001F'/>"
            "<separator/>"
            "<fl ref='0022'/>"
            "<fl ref='0032'/>"
            "<fl ref='00FF'/>"
            "<separator/>"
            "<fl ref='00EF'/>"
            "<fl ref='00F1'/>"
            "<fl ref='00F9'/>"
            "<fl ref='00B3'/>"
            "<separator/>"
            "<fl ref='0023'/>"
            "<fl ref='0025'/>"
            "<fl ref='0026'/>"
            "<fl ref='0027'/>"
            "<fl ref='0028'/>"
            "</root>";
     DM->RegComp->ShowSelUnitList(tmp,fCodeList);
     TcxTreeListNode *tmpNode;
     InProgress = true;
     Repaint();
//     CurPB->Visible = true;
     CurPB->Properties->Max = fCodeList->Count;
     CurPB->Position = 0;
     PatList->BeginUpdate();
     for (int i = 0; i < fCodeList->Count; i++)
      {
        DM->qtExec("Select (R001F||' '||R0020||' '||R002F) as PAT_FIO, R0031 From "+DM->RegComp->GetUnitTabName()+" Where CODE="+fCodeList->Strings[i]);
        TListNodeMap::iterator it = CodeList.find(fCodeList->Strings[i].ToInt());
        if (it == CodeList.end())
         {
		   tmpNode = PatList->Root->AddChild();
           CodeList[fCodeList->Strings[i].ToInt()] = tmpNode;
           tmpNode->Values[0] = quFNStr("PAT_FIO");
           tmpNode->Values[1] = quFNm("R0031")->AsTextDate;
           tmpNode->Data = (void*)fCodeList->Strings[i].ToInt();
         }
        CurPB->Position++; Application->ProcessMessages(); if (!InProgress) throw EAbort("");
      }
     if (DM->trFree->Active) DM->trFree->Commit();
   }
  __finally
   {
     PatList->EndUpdate();
     InProgress = false;
     CurPB->Position = 0;
     SetEnabled(true);
     if (fCodeList)  delete fCodeList;
     ActiveControl = PatList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::SelFromFilterBtnClick(TObject *Sender)
{
   try
    {
      UnicodeString FFrom;
      UnicodeString FWhere = GetWhereFromFilter (((int)FilterCB->Properties->Items->Objects[FilterCB->ItemIndex]), FFrom, NULL,"0E291426-00005882-2493.0000","");
      TcxTreeListNode *tmpNode;
      if (FWhere != "")
       {
         DM->qtExec("Select Count(CB.CODE) as MCOUNT From "+FFrom+" Where "+FWhere);
         CurPB->Properties->Max = quFNInt("MCOUNT");
         CurPB->Position = 0;
         DM->qtExec("Select CB.CODE, (CB.R001F||' '||CB.R0020||' '||CB.R002F) as PAT_FIO, CB.R0031 From "+FFrom+" Where "+FWhere);
         InProgress = true;
//         PrBar->Visible = true;
         PatList->BeginUpdate();
         while (!DM->quFree->Eof)
          {
            TListNodeMap::iterator it = CodeList.find(quFNInt("CODE"));
            if (it == CodeList.end())
             {
			   tmpNode = PatList->Root->AddChild();
               CodeList[quFNInt("CODE")] = tmpNode;
               tmpNode->Values[0] = quFNStr("PAT_FIO");
               tmpNode->Values[1] = quFNm("R0031")->AsTextDate;
               tmpNode->Data = (void*)quFNInt("CODE");
             }
            DM->quFree->Next();
            CurPB->Position++; Application->ProcessMessages(); if (!InProgress) throw EAbort("");
          }
         if (DM->trFree->Active) DM->trFree->Commit();
       }
    }
   __finally
    {
      PatList->EndUpdate();
      InProgress = false;
      CurPB->Position = 0;
      SetEnabled(true);
      ActiveControl = PatList;
    }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::CardExpChBClick(TObject *Sender)
{
  SetEnabled(true);
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::UnitExpChBClick(TObject *Sender)
{
  SetEnabled(true);
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::FilterCBPropertiesChange(TObject *Sender)
{
  SetEnabled(true);
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::ClassExpChBClick(TObject *Sender)
{
  SetEnabled(true);
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::ExportBtnClick(TObject *Sender)
{
  TStringList *FParamList = new TStringList;
  try
   {
     SaveDlg->InitialDir = "";
     SaveDlg->FileName = "CardExport_"+TDateTime::CurrentDateTime().FormatString("yyyymmdd_hhnnss")+".zxml";
     if (SaveDlg->Execute())
      {
        int RC = ServiceStart(ConvSrvcName);
        if (!RC || (RC == 1))
         {
           cmdServerConnect();
           FParamList->Add("/e");
           if (ClassExpChB->Checked)
            FParamList->Add("/c");
           if (UnitExpChB->Checked)
            FParamList->Add("/u");
           if (CardExpChB->Checked)
            FParamList->Add("/k");

           FParamList->Add("/fn");
           FParamList->Add("\""+ExpandUNCFileName(icsPrePath(SaveDlg->FileName))+"\"");
           SendStartCommand("start", FParamList);
           InitProgressCtrls();
           SetCtrlEnabled(false);
           PrTimer->Enabled = true;
         }
      }
   }
  __finally
   {
     delete FParamList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::FilterListBtnClick(TObject *Sender)
{
  TChListForm *Dlg = NULL;
  try
   {
     Dlg = new TChListForm(this,"������ �������",0,DM->ICSDocEx->DM);
     Dlg->ShowModal();
     FilterCB->Properties->Items->Clear();
     FilterCB->Properties->Items->AddObject("",(TObject*)0);
     DM->qtExec("Select CODE, NAME From FILTERS Where CHTYPE = 0 and Upper(MODULE_TYPE) = '"+DM->SubDir.UpperCase()+"' Order By NAME", false);
     while (!DM->quFree->Eof)
      {
        FilterCB->Properties->Items->AddObject(quFNStr("NAME").c_str(),(TObject*)quFNInt("CODE"));
        DM->quFree->Next();
      }
     DM->trFree->Commit();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::PatListClick(TObject *Sender)
{
  DelBtn->Enabled = (bool)PatList->Root->Count;
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::DelBtnClick(TObject *Sender)
{
  if (PatList->SelectionCount)
   {
     int PatCode = (int)PatList->Selections[0]->Data;
     TListNodeMap::iterator it = CodeList.find(PatCode);
     if (it != CodeList.end())
      {
        PatList->Selections[0]->Delete();
        CodeList.erase(PatCode);
      }
   }
  DelBtn->Enabled = (bool)PatList->Root->Count;
  SetEnabled(true);
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::FProgressInit(int AMax, UnicodeString AMsg)
{
  if (!InProgress) throw EAbort("");
  CurPB->Properties->Max = AMax;
  CurPB->Position = 0;
  CurPB->Update();
//  StatusBar->Panels->Items[1]->Text = AMsg;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::FProgressChange(int APercent, UnicodeString AMsg)
{
  if (!InProgress) throw EAbort("");
  if (APercent == -1)
   CurPB->Position++;
  else if (APercent == -2)
   StatusBar->Panels->Items[1]->Text = AMsg;
  else
   CurPB->Position = APercent;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::FProgressComplite(UnicodeString AMsg)
{
  if (!InProgress) throw EAbort("");
  CurPB->Position = 0;
  StatusBar->Panels->Items[1]->Text = AMsg;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::SetEnabled(bool AEnabled)
{
  CardExpChB->Enabled  = AEnabled;
  UnitExpChB->Enabled  = AEnabled;
  ClassExpChB->Enabled = AEnabled;
  if (AEnabled)
   {
     PatList->Enabled = true;
     PatList->Styles->Content = EnabledStyle;

     if (CardExpChB->Checked)
      {
        UnitExpChB->Checked = true;
        UnitExpChB->Enabled = false;
      }
     else
      {
        UnitExpChB->Enabled = true;
      }
     if (UnitExpChB->Checked)
      {
        ClassExpChB->Checked = true;
        ClassExpChB->Enabled = false;
        SelFromTemplateBtn->Enabled = true;
        SelFromFilterBtn->Enabled = (FilterCB->ItemIndex > 0) && UnitExpChB->Checked;
        FilterCB->Enabled = true;
        FilterListBtn->Enabled = true;
      }
     else
      {
        ClassExpChB->Enabled = true;
        SelFromTemplateBtn->Enabled = false;
        SelFromFilterBtn->Enabled = false;
        FilterCB->Enabled = false;
        FilterListBtn->Enabled = false;
      }
     ExportBtn->Enabled = (ClassExpChB->Checked && (UnitExpChB->Checked&&CodeList.size() || !UnitExpChB->Checked)) ||
	 (!ClassExpChB->Checked && (UnitExpChB->Checked&&CodeList.size() || !UnitExpChB->Checked)&& PatList->Root->Count);
   }
  else
   {
     PatList->Enabled = false;
     PatList->Styles->Content = DisabledStyle;
     SelFromTemplateBtn->Enabled = false;

     SelFromFilterBtn->Enabled = false;
     FilterLab->Enabled = false;
     FilterCB->Enabled = false;
     FilterListBtn->Enabled = false;

     DelBtn->Enabled = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::CloseBtnClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
  if (IsConvProgress())
   _MSG_INF("��������.\n�������� ������ �����������.\n��� ��������� �������� ������ ������� ������ \"��������\".","���������");
  CanClose = true;
}
//---------------------------------------------------------------------------
bool __fastcall TExportCardForm::IsConvProgress()
{
  bool RC = false;
  try
   {
     DWORD FStatus;
     int SRC = ServiceStatus(ConvSrvcName, FStatus);
     if (!SRC)
      {
        if (FStatus == SERVICE_RUNNING)
         RC = (SendConvCommand("get state").LowerCase() == "progress");
      }
   }
  __finally
  {
  }
 return RC;
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::InitProgressCtrls()
{
  HintLb->Caption = "";
  CurProgressLb->Caption = "";
  CurPB->Position = 0;
  TotalProgressLb->Caption = "";
  TotalPB->Position = 0;
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::SetCtrlEnabled(bool AVal)
{
  if (AVal) HintLb->Caption = "";
  ExportBtn->Enabled = AVal && PatList->Root->Count;
  StopBtn->Enabled     = !AVal;
  CardExpChB->Enabled  = AVal;
  UnitExpChB->Enabled  = AVal;
  ClassExpChB->Enabled = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::PrTimerTimer(TObject *Sender)
{
  if (!HintLb->Caption.Length()) HintLb->Caption = SendConvCommand("get state str");
  UnicodeString Ret = SendConvCommand("get progress");
  if (Ret.LowerCase() == "ok")
   {
     PrTimer->Enabled = false;
     InitProgressCtrls();
     SetCtrlEnabled(true);
     PrTimer->Enabled = false;
     Ret = SendConvCommand("get last export state");
     if (Ret.Length() && Ret.LowerCase() != "ok")
      _MSG_INF(Ret,"���������");
   }
  else if (Ret.Length())
   {
     int TP, CP, TM, CM;
     TTagNode *PrData = new TTagNode(NULL);
     TTagNode *tmp;
     try
      {
        try
         {
           PrData->AsXML = Ret;
         }
        catch(...)
         {
         }
        tmp = PrData->GetFirstChild();
        if (tmp)
         {
           CP = tmp->AV["p"].ToIntDef(0);
           CM = tmp->AV["m"].ToIntDef(0);
           if (CurProgressLb->Caption != tmp->AV["PCDATA"])
            CurProgressLb->Caption = tmp->AV["PCDATA"];
           tmp = tmp->GetNext();
           TP = tmp->AV["p"].ToIntDef(0);
           TM = tmp->AV["m"].ToIntDef(0);
           if (TotalProgressLb->Caption != tmp->AV["PCDATA"])
            TotalProgressLb->Caption = tmp->AV["PCDATA"];

           if (CurPB->Properties->Max != CM)
             CurPB->Properties->Max = CM;
           if (TotalPB->Properties->Max != TM)
             TotalPB->Properties->Max = TM;

           if (CurPB->Position != CP)
             CurPB->Position = CP;
           if (TotalPB->Position != TP)
             TotalPB->Position = TP;
         }
      }
     __finally
      {
        delete PrData;
      }
   }
  else if (!Ret.Length())
   {
     PrTimer->Enabled = false;
     InitProgressCtrls();
     SetCtrlEnabled(true);
   }
}
//---------------------------------------------------------------------------
bool __fastcall TExportCardForm::cmdServerConnect()
{
  bool RC = cmdClient->Active;
  try
   {
     if (!RC)
      {
        cmdClient->Active = true;
        for (int i = 0; (i < 1000) && !cmdClient->Active; i++)
         {
           Sleep(10); Application->ProcessMessages();
         }
        RC = cmdClient->Active;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TExportCardForm::SendConvCommand(UnicodeString ACmd)
{
  UnicodeString RC = "";
  try
   {
     cmdServerConnect();
     if (cmdClient->Active)
      {
        FCmdRet = "";
        cmdClient->Socket->SendText(ACmd);
        CmdRetTimer->Enabled = true;
        while (!FCmdRet.Length())
         {
           Sleep(10);
           Application->ProcessMessages();
         }
        RC = FCmdRet;
        CmdRetTimer->Enabled = false;
      }
     else
      {
        PrTimer->Enabled = false;
        throw Exception("�� ������� ���������� ���������� � ��������.");
      }
   }
  __finally
   {
     CmdRetTimer->Enabled = false;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::SendStartCommand(UnicodeString ACmd, TStringList *AParams)
{
  try
   {
     SendConvCommand(ACmd);
     for (int i = 0; i < AParams->Count; i++)
      SendConvCommand(AParams->Strings[i]);
     SendConvCommand("/ulbegin");
     for (TListNodeMap::iterator i = CodeList.begin(); i != CodeList.end(); i++)
      SendConvCommand(IntToStr(i->first));
     SendConvCommand("/ulend");
     SendConvCommand("params_end");
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::StopBtnClick(TObject *Sender)
{
  InProgress = false;
  if (IsConvProgress())
   {
     if (_MSG_QUE("�� ������� � ���, ��� ������ �������� ���������� �������� ������?",
                  Application->Title.c_str()
                 ) == ID_YES)
      SendConvCommand("stop");
   }
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::cmdClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
  FCmdRet = Socket->ReceiveText();
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::cmdClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
  FCmdRet = "Error";
  CmdRetTimer->Enabled = false;
  PrTimer->Enabled = false;
  _MSG_ERR("������ ��������� ������ �������, �������� �������� ��������. ���="+IntToStr(ErrorCode),"������");
  ErrorCode = 0;
}
//---------------------------------------------------------------------------
void __fastcall TExportCardForm::CmdRetTimerTimer(TObject *Sender)
{
  FCmdRet = "Error";
  CmdRetTimer->Enabled = false;
  PrTimer->Enabled = false;
  _MSG_ERR("������ ��������� ������ �������, �������� �������� ��������.","������");
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------

