﻿//---------------------------------------------------------------------------

#ifndef ConvertFrom67H
#define ConvertFrom67H

#include <System.Classes.hpp>
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include <map>
//---------------------------------------------------------------------------
typedef map<UnicodeString, TJSONObject*> TJSONMap;
typedef map<UnicodeString, TJSONMap*> TJSONIdMap;
typedef map<UnicodeString, TJSONIdMap*> TJSONUnitIdMap;
typedef void __fastcall (__closure *TCard67ConvertorOnImportDataEvent)(TJSONObject *AData, UnicodeString &ALastCode);
class TCard67Convertor : public TObject
{
private:
  TTagNode *FDataDef;
//  TTagNode *IEContent;
  TJSONIdMap *OldDataMap;
  TJSONUnitIdMap *FUnitCard;
  TJSONMap       FPrivMap;
  TJSONMap       FOtvMap;
  TAnsiStrMap    FUchOtdMap;
  TAnsiStrMap    InfMap;
//  TStringList *DDD;

  void __fastcall PrintMemoryInfo();
  UnicodeString __fastcall PrintProcessMemoryInfo( DWORD processID );
  void __fastcall ReplaceValue(TJSONObject *AData, UnicodeString AId1, UnicodeString AId2);
  void __fastcall PrepareValue(TJSONObject *AData, UnicodeString AId);
  void __fastcall ReplAndPrepValue(TJSONObject *AData, UnicodeString AId1, UnicodeString AId2);
  bool __fastcall Prepare(TTagNode *AData, TkabProgress AProgress);

  bool __fastcall ConvertUnit(TJSONObject *ACardRoot, TJSONObject *ACls, UnicodeString AUCode, TDate &ABirthday, TDate &AOtvToDate, __int64 ALPUCode, TkabProgress AProgress);
  bool __fastcall Convert1003(TJSONObject *ACardRoot, UnicodeString AUCode, TDate ABirthday, __int64 ALPUCode, TkabProgress AProgress, int &ACount);
  bool __fastcall Convert102f(TJSONObject *ACardRoot, UnicodeString AUCode, TDate ABirthday, __int64 ALPUCode, TkabProgress AProgress, int &ACount);
  bool __fastcall Convert1030(TJSONObject *ACardRoot, UnicodeString AUCode, TDate ABirthday, TDate ADayTo, TkabProgress AProgress, int &ACount);

  TJSONObject* __fastcall PrepareRecData(TTagNode *ARec);
  TJSONObject* __fastcall GetOldRec(UnicodeString AId, UnicodeString ACode);
  void         __fastcall EraseOldRec(UnicodeString AId, UnicodeString ACode);
  void __fastcall SetClData(TJSONObject* ACls, UnicodeString AId, UnicodeString ACode, __int64  ALPU, UnicodeString AOtdel);
  TJSONMap* __fastcall GetOldUnitCard(UnicodeString AId, UnicodeString AUnitCode);
//  TJSONObject* __fastcall GetOldCardRec(UnicodeString AId, UnicodeString AUnitCode, UnicodeString ACode);
  void __fastcall EraseOldUnitCard(UnicodeString AId, UnicodeString AUnitCode);

  void __fastcall PrepareClsData(UnicodeString AClId, TJSONObject* AClRec, UnicodeString ACode, __int64  ALPU, UnicodeString AOtdel);
//  void __fastcall Prepare000E(UnicodeString ACode,  __int64  ALPU, UnicodeString AOtdel);
//  void __fastcall Prepare000C(UnicodeString ACode,  __int64  ALPU);
//  void __fastcall Prepare0001(UnicodeString ACode, __int64  ALPU);



public:
  __fastcall TCard67Convertor(TTagNode *ADataDef);
  __fastcall ~TCard67Convertor();
  bool __fastcall Convert(TTagNode *AData, __int64 ALPUCode, TCard67ConvertorOnImportDataEvent AOnImport, TkabProgress AProgress, UnicodeString &ALastCode);
};
//---------------------------------------------------------------------------
#endif
