object ImportCardForm: TImportCardForm
  Left = 568
  Top = 183
  BorderStyle = bsSizeToolWin
  Caption = #1055#1088#1080#1077#1084' '#1080#1084#1084#1091#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1093' '#1082#1072#1088#1090
  ClientHeight = 444
  ClientWidth = 522
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 308
    Width = 522
    Height = 116
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      522
      116)
    object HintLb: TLabel
      Left = 8
      Top = 7
      Width = 31
      Height = 13
      Caption = 'HintLb'
    end
    object CurProgressLb: TLabel
      Left = 8
      Top = 27
      Width = 69
      Height = 13
      Caption = 'CurProgressLb'
    end
    object TotalProgressLb: TLabel
      Left = 8
      Top = 71
      Width = 77
      Height = 13
      Caption = 'TotalProgressLb'
    end
    object CurPB: TcxProgressBar
      Left = 10
      Top = 44
      Anchors = [akLeft, akTop, akRight]
      ParentColor = False
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = 16425091
      Properties.EndColor = 9841671
      Properties.OverloadValue = 50.000000000000000000
      Style.BorderStyle = ebsUltraFlat
      Style.Color = clWhite
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.BorderStyle = ebsFlat
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.BorderStyle = ebsFlat
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 0
      Width = 511
    end
    object TotalPB: TcxProgressBar
      Left = 10
      Top = 87
      Anchors = [akLeft, akTop, akRight]
      ParentColor = False
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = 16425091
      Properties.EndColor = 9841671
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebsUltraFlat
      Style.Color = clWhite
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.BorderStyle = ebsFlat
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.BorderStyle = ebsFlat
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 1
      Width = 511
    end
  end
  object MsgPanel: TPanel
    Left = 0
    Top = 0
    Width = 522
    Height = 34
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 3
    Color = clWhite
    TabOrder = 1
    object MsgLab: TLabel
      Left = 3
      Top = 3
      Width = 516
      Height = 28
      Align = alClient
      Caption = 'msgLab'
      Enabled = False
      Transparent = True
      WordWrap = True
      ExplicitWidth = 37
      ExplicitHeight = 13
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 34
    Width = 522
    Height = 274
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object OrgParamGB: TcxGroupBox
      Left = 0
      Top = 0
      Align = alClient
      Caption = ' '#1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103', '#1080#1089#1090#1086#1095#1085#1080#1082' '#1076#1072#1085#1085#1099#1093': '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Height = 169
      Width = 522
      object OrgLabPanel: TPanel
        Left = 2
        Top = 42
        Width = 518
        Height = 125
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 3
        TabOrder = 0
        object ExpOrgDataLab: TLabel
          Left = 3
          Top = 3
          Width = 512
          Height = 16
          Align = alTop
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          WordWrap = True
          ExplicitWidth = 8
        end
      end
      object Panel2: TPanel
        Left = 2
        Top = 18
        Width = 518
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 3
        TabOrder = 1
        DesignSize = (
          518
          24)
        object OpenBtn: TcxButton
          Left = 432
          Top = -1
          Width = 75
          Height = 25
          Anchors = [akRight]
          Caption = #1054#1090#1082#1088#1099#1090#1100
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          OnClick = OpenBtnClick
        end
      end
    end
    object ImportDataGB: TcxGroupBox
      Left = 0
      Top = 169
      Align = alBottom
      Caption = ' '#1055#1088#1080#1085#1080#1084#1072#1090#1100': '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      DesignSize = (
        522
        105)
      Height = 105
      Width = 522
      object CardChB: TcxCheckBox
        Left = 7
        Top = 21
        Caption = #1048#1084#1084#1091#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1077' '#1082#1072#1088#1090#1099
        Enabled = False
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        OnClick = CardChBClick
        Width = 165
      end
      object ClassImpChB: TcxCheckBox
        Left = 7
        Top = 73
        Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080' '#1088#1077#1075#1080#1089#1090#1088#1072#1090#1091#1088#1099
        Enabled = False
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 2
        OnClick = ClassImpChBClick
        Width = 174
      end
      object UnitImpChB: TcxCheckBox
        Left = 7
        Top = 47
        Caption = #1057#1087#1080#1089#1086#1082' '#1087#1072#1094#1080#1077#1085#1090#1086#1074
        Enabled = False
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 1
        OnClick = UnitImpChBClick
        Width = 133
      end
      object ClassImpReplaceCB: TcxComboBox
        Left = 185
        Top = 73
        Enabled = False
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          #1085#1077' '#1079#1072#1084#1077#1085#1103#1090#1100
          #1079#1072#1084#1077#1085#1103#1090#1100
          #1079#1072#1084#1077#1085#1103#1090#1100' '#1089' '#1091#1095#1105#1090#1086#1084' '#1076#1072#1090#1099' '#1080#1079#1084#1077#1085#1077#1085#1080#1103)
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 3
        Text = #1079#1072#1084#1077#1085#1103#1090#1100' '#1089' '#1091#1095#1105#1090#1086#1084' '#1076#1072#1090#1099' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
        Width = 245
      end
      object UnitImpReplaceCB: TcxComboBox
        Left = 185
        Top = 47
        Enabled = False
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          #1085#1077' '#1079#1072#1084#1077#1085#1103#1090#1100
          #1079#1072#1084#1077#1085#1103#1090#1100
          #1079#1072#1084#1077#1085#1103#1090#1100' '#1089' '#1091#1095#1105#1090#1086#1084' '#1076#1072#1090#1099' '#1080#1079#1084#1077#1085#1077#1085#1080#1103)
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 4
        Text = #1079#1072#1084#1077#1085#1103#1090#1100' '#1089' '#1091#1095#1105#1090#1086#1084' '#1076#1072#1090#1099' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
        Width = 245
      end
      object ImportBtn: TcxButton
        Left = 437
        Top = 11
        Width = 75
        Height = 25
        Anchors = [akRight]
        Caption = #1055#1088#1080#1085#1103#1090#1100
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        TabOrder = 5
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = ImportBtnClick
      end
      object StopBtn: TcxButton
        Left = 437
        Top = 40
        Width = 75
        Height = 25
        Anchors = [akRight]
        Cancel = True
        Caption = #1055#1088#1077#1088#1074#1072#1090#1100
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        TabOrder = 6
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = StopBtnClick
      end
      object CloseBtn: TcxButton
        Left = 438
        Top = 69
        Width = 75
        Height = 25
        Anchors = [akRight]
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        ModalResult = 2
        TabOrder = 7
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = CloseBtnClick
      end
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 424
    Width = 522
    Height = 20
    Panels = <>
    PaintStyle = stpsXP
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object OpenDlg: TOpenDialog
    DefaultExt = '.zxl'
    Filter = #1060#1072#1081#1083#1099' '#1076#1072#1085#1085#1099#1093'|*.zxml;*.zfdb'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 39
    Top = 52
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 7
    Top = 52
    PixelsPerInch = 96
    object DisabledStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clBtnFace
      TextColor = clMedGray
    end
    object EnabledStyle: TcxStyle
      AssignedValues = [svColor]
      Color = clWindow
    end
  end
  object PrTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = PrTimerTimer
    Left = 102
    Top = 54
  end
  object CmdRetTimer: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = CmdRetTimerTimer
    Left = 148
    Top = 89
  end
end
