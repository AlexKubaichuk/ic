//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ImportCard.h"
#include "ExtUtils.h"
#include "icsRegFIBEIDataProvider.h"
#include "icsRegXMLEIDataProvider.h"
#include "icsZIPFIBProvider.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#pragma link "TB2Dock"
//#pragma link "TB2Item"
//#pragma link "TB2Toolbar"
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxControls"
#pragma link "cxGraphics"
#pragma link "dxStatusBar"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxProgressBar"
#pragma link "cxCheckBox"
#pragma link "cxStyles"
#pragma link "cxGroupBox"
#pragma link "cxDropDownEdit"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxClasses"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxStatusBarPainter"
#pragma resource "*.dfm"
TImportCardForm *ImportCardForm;
//---------------------------------------------------------------------------
__fastcall TImportCardForm::TImportCardForm(TComponent* Owner)
        : TForm(Owner)
{
  FInProgress = false;
  InitProgressCtrls();
  MsgLab->Caption = "";
  FFileLoaded = false;
  SetCtrlEnabled(true);
  FImpFileName = "";
  cmdClient->Host = "localhost";
  if (DM->AppOpt->Vals["eihost"].AsString.Length())
   cmdClient->Host = DM->AppOpt->Vals["eihost"].AsString;
  cmdClient->Port = DM->AppOpt->Vals["eiport"].AsInt;
  InitProgressCtrls();
  DWORD FStatus;
  int RC = ServiceStatus(ConvSrvcName, FStatus);
  if (!RC)
   {
     if (FStatus == SERVICE_RUNNING)
      {
        cmdServerConnect();
        bool FPr = IsConvProgress();
        SetCtrlEnabled(!FPr);
        PrTimer->Enabled = FPr;
        if (!FPr)
         {
            UnicodeString Ret = SendConvCommand("get last import state");
            if (Ret.Length() && Ret.LowerCase() != "ok")
             HintLb->Caption = "��������� ���������� ����� ������:\""+Ret+"\"";
            ServiceStop(ConvSrvcName);
            ServiceStart(ConvSrvcName);
            cmdServerConnect();
         }
      }
     else if (FStatus == SERVICE_STOPPED)
      {
        RC = ServiceStart(ConvSrvcName);
        if (RC)
         {
           _MSG_ERR("������ ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
           ImportBtn->Enabled   = false;
           PrTimer->Enabled    = false;
         }
        else
         {
           cmdServerConnect();
           bool FPr = IsConvProgress();
           SetCtrlEnabled(!FPr);
           PrTimer->Enabled = FPr;
         }
      }
   }
  else
   {
     _MSG_ERR("������ ����������� ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
     ImportBtn->Enabled = false;
     PrTimer->Enabled     = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::OpenBtnClick(TObject *Sender)
{
  TRegBaseEIDataProvider *EIDataProvider = NULL;
  TicsZIPFIBProvider *ZIPDBProvider = NULL;
  bool FXMLSrc = false;
  FFileLoaded = false;
  try
   {
     if (OpenDlg->Execute())
      {
        SetCtrlEnabled(false);
        FInProgress = true;
        if (ExtractFileExt(OpenDlg->FileName).LowerCase() == ".zxml")
         FXMLSrc = true;
        if (FXMLSrc)
         {
           TRegXMLEIDataProvider* tmpEIProv = new TRegXMLEIDataProvider;
           tmpEIProv->IEXML = new TTagNode(NULL);
           EIDataProvider = tmpEIProv;

           tmpEIProv->IEXML->OnProgressInit     = FProgressInit;
           tmpEIProv->IEXML->OnProgress         = FProgressChange;
           tmpEIProv->IEXML->OnProgressComplite = FProgressComplite;
           try
            {
              tmpEIProv->IEXML->LoadFromZIPXMLFile(icsPrePath(OpenDlg->FileName));
              FFileLoaded = true;
              FImpFileName = icsPrePath(OpenDlg->FileName);
            }
           __finally
            {
              tmpEIProv->IEXML->OnProgressInit     = NULL;
              tmpEIProv->IEXML->OnProgress         = NULL;
              tmpEIProv->IEXML->OnProgressComplite = NULL;
            }
         }
        else
         {
           TRegFIBEIDataProvider* tmpEIProv = new TRegFIBEIDataProvider;
           ZIPDBProvider = new TicsZIPFIBProvider;
           tmpEIProv->Query = ZIPDBProvider->Query1;
           ZIPDBProvider->FilePath    = icsPrePath(OpenDlg->FileName);
           ZIPDBProvider->ExtrBaseDir = icsPrePath(ExtractFilePath(ParamStr(0)));

           EIDataProvider = tmpEIProv;

           ZIPDBProvider->OnProgressInit     = FProgressInit;
           ZIPDBProvider->OnProgressChange   = FProgressChange;
           ZIPDBProvider->OnProgressComplite = FProgressComplite;
           try
            {
              ZIPDBProvider->Open();
              FFileLoaded = true;
              FImpFileName = icsPrePath(OpenDlg->FileName);
            }
           __finally
            {
              ZIPDBProvider->OnProgressInit     = NULL;
              ZIPDBProvider->OnProgressChange   = NULL;
              ZIPDBProvider->OnProgressComplite = NULL;
            }
         }
        FImpVersion = EIDataProvider->Version.ToIntDef(0);
        ExpOrgDataLab->Caption = "��� ������";
        if (EIDataProvider->LPUCode.Trim().Length())
         {
           UnicodeString FOrgData = EIDataProvider->LPUFullName;
                      FOrgData += "\n\n ���:       "+EIDataProvider->LPUCode;
                      FOrgData += "\n �����:     "+  EIDataProvider->LPUAddr;
                      FOrgData += "\n �������:   "+  EIDataProvider->LPUPhone;
                      FOrgData += "\n ��. �����: "+  EIDataProvider->LPUMail;
           ExpOrgDataLab->Caption = FOrgData;
           ExpOrgDataLab->Repaint();
         }
        HintLb->Caption = "";
      }
   }
  __finally
   {
     FInProgress = false;
     // ��������� ������� ���������������� ����
     if (EIDataProvider)
      {
        if (EIDataProvider->SectionExist("cls")) //EITag->GetChildByName("cls")
         {
           ClassImpChB->Checked = true;//(bool)EITag->GetChildByName("cls")->Count;
           ClassImpChBClick(ClassImpChB);
         }
        if (EIDataProvider->SectionExist("unit") && ClassImpChB->Checked) //EITag->GetChildByName("unit") && ClassImpChB->Checked
         {
           UnitImpChB->Checked = true;//(bool)EITag->GetChildByName("unit")->Count;
           UnitImpChBClick(UnitImpChB);
         }
        if (EIDataProvider->SectionExist("cards") && UnitImpChB->Checked) //EITag->GetChildByName("cards") && UnitImpChB->Checked
         {
           CardChB->Checked = true;//(bool)EITag->GetChildByName("cards")->Count;
           CardChBClick(CardChB);
         }
        if (FXMLSrc)
         {
            delete ((TRegXMLEIDataProvider*)EIDataProvider)->IEXML;
            ((TRegXMLEIDataProvider*)EIDataProvider)->IEXML = NULL;
            delete EIDataProvider;
            EIDataProvider = NULL;
         }
        else
         {
            ((TRegFIBEIDataProvider*)EIDataProvider)->Query = NULL;
            delete EIDataProvider;
            EIDataProvider = NULL;
            if (ZIPDBProvider)
             {
               delete ZIPDBProvider;
               ZIPDBProvider = NULL;
             }
         }
      }
     SetCtrlEnabled(true);
     InitProgressCtrls();
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::ImportBtnClick(TObject *Sender)
{
  TStringList *FParamList = new TStringList;
  try
   {
     if (!FFileLoaded)
      {
         _MSG_ERR("�� �������� ���� �������", "������");
         return ;
      }
     int RC = ServiceStart(ConvSrvcName);
     if (!RC || (RC == 1))
      {
        cmdServerConnect();
        FParamList->Add("/i");
        if (ClassImpChB->Checked)
         FParamList->Add("/c");
        if (UnitImpChB->Checked)
         FParamList->Add("/u");
        if (CardChB->Checked)
         FParamList->Add("/k");
        if (UnitImpReplaceCB->ItemIndex == 1)
         FParamList->Add("/ur");
        if (UnitImpReplaceCB->ItemIndex == 2)
         FParamList->Add("/uuld");
        if (ClassImpReplaceCB->ItemIndex == 1)
         FParamList->Add("/cr");
        if (ClassImpReplaceCB->ItemIndex == 2)
         FParamList->Add("/culd");
#ifdef IMM_CENTER
        FParamList->Add("/center");
#endif
        FParamList->Add("/fn");
        FParamList->Add("\""+ExpandUNCFileName(FImpFileName)+"\"");

        SendStartCommand("start", FParamList);
        InitProgressCtrls();
        SetCtrlEnabled(false);
        PrTimer->Enabled = true;
      }
   }
  __finally
   {
     delete FParamList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::ClassImpChBClick(TObject *Sender)
{
  ClassImpReplaceCB->Enabled = ClassImpChB->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::UnitImpChBClick(TObject *Sender)
{
  if (UnitImpChB->Checked)
   {
     ClassImpChB->Checked = true;
     ClassImpChB->Enabled = false;
     UnitImpReplaceCB->Enabled = true;
   }
  else
   {
     ClassImpChB->Checked = false;
     ClassImpChB->Enabled = true;
     UnitImpReplaceCB->Enabled = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::CardChBClick(TObject *Sender)
{
  if (CardChB->Checked)
   {
     UnitImpChB->Checked = true;
     UnitImpChB->Enabled = false;
   }
  else
   {
     UnitImpChB->Checked = false;
     UnitImpChB->Enabled = true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::FProgressInit(int AMax, UnicodeString AMsg)
{
  if (!FInProgress) throw EAbort("");
  if (AMsg == "1")
   CurPB->Properties->Max;// = AMax/10000ui64;
  else if (AMsg == "2")
   CurPB->Properties->Max = AMax/10000ui64;
  else
   CurPB->Properties->Max = 2*AMax;

  CurPB->Position = 0;
  CurPB->Update();
//  HintLb->Caption = AMsg;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::FProgressChange(int APercent, UnicodeString AMsg)
{
  if (!FInProgress) throw EAbort("");
  if (AMsg == "1")
   CurPB->Position;
  else if (AMsg == "2")
   CurPB->Position++;
  else
   {
     if (APercent == -1)
      CurPB->Position++;
     else if (APercent == -2)
      HintLb->Caption = AMsg;
     else
      CurPB->Position = APercent;
   }
  CurPB->Update();
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::FProgressComplite(UnicodeString AMsg)
{
  if (!FInProgress) throw EAbort("");
  CurPB->Position = 0;
  HintLb->Caption = AMsg;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::CloseBtnClick(TObject *Sender)
{
  if (FCanClose())
   {
     Close();
   }
}
//---------------------------------------------------------------------------
bool __fastcall TImportCardForm::FCanClose()
{
  bool RC = true;
  try
   {
     if (FInProgress)
      {
        if (_MSG_QUE("�� ������� � ���, ��� ������ �������� ���������� ����� ������?",
                     Application->Title.c_str()
                    ) == ID_YES)
         {
           FInProgress = false;
         }
        else
         RC = false;
      }
     else if (IsConvProgress())
      {
        _MSG_INF("��������.\n���� ������ �����������.\n��� ��������� ����� ������ ������� ������ \"��������\".","���������");
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TImportCardForm::IsConvProgress()
{
  bool RC = false;
  try
   {
     DWORD FStatus;
     int SRC = ServiceStatus(ConvSrvcName, FStatus);
     if (!SRC)
      {
        if (FStatus == SERVICE_RUNNING)
         RC = (SendConvCommand("get state").LowerCase() == "progress");
      }
   }
  __finally
  {
  }
 return RC;
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::InitProgressCtrls()
{
  HintLb->Caption = "";
  CurProgressLb->Caption = "";
  CurPB->Position = 0;
  TotalProgressLb->Caption = "";
  TotalPB->Position = 0;
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::SetCtrlEnabled(bool AVal)
{
  if (!FFileLoaded)
   {
     CardChB->Checked     = false;
     ClassImpChB->Checked = false;
     UnitImpChB->Checked  = false;
   }
  ClassImpChB->Enabled            = AVal && FFileLoaded;
  UnitImpChB->Enabled             = AVal && FFileLoaded;
  CardChB->Enabled                = AVal && FFileLoaded;

  UnitImpReplaceCB->Enabled       = AVal && FFileLoaded;
  ClassImpReplaceCB->Enabled      = AVal && FFileLoaded;

  OpenBtn->Enabled  = AVal;

  if (AVal) HintLb->Caption = "";
  ImportBtn->Enabled = (CardChB->Checked || ClassImpChB->Checked || UnitImpChB->Checked) && FFileLoaded && AVal;
  StopBtn->Enabled     = !AVal;
}
//---------------------------------------------------------------------------
bool __fastcall TImportCardForm::cmdServerConnect()
{
  bool RC = cmdClient->Active;
  try
   {
     if (!RC)
      {
        cmdClient->Active = true;
        for (int i = 0; (i < 1000) && !cmdClient->Active; i++)
         {
           Sleep(10); Application->ProcessMessages();
         }
        RC = cmdClient->Active;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TImportCardForm::SendConvCommand(UnicodeString ACmd)
{
  UnicodeString RC = "";
  try
   {
     cmdServerConnect();
     if (cmdClient->Active)
      {
        FCmdRet = "";
        cmdClient->Socket->SendText(ACmd);
        CmdRetTimer->Enabled = true;
        while (!FCmdRet.Length())
         {
           Sleep(10);
           Application->ProcessMessages();
         }
        RC = FCmdRet;
        CmdRetTimer->Enabled = false;
      }
     else
      {
        PrTimer->Enabled = false;
        throw Exception("�� ������� ���������� ���������� � ��������.");
      }
   }
  __finally
   {
     CmdRetTimer->Enabled = false;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::SendStartCommand(UnicodeString ACmd, TStringList *AParams)
{
  try
   {
     SendConvCommand(ACmd);
     for (int i = 0; i < AParams->Count; i++)
      SendConvCommand(AParams->Strings[i]);
     SendConvCommand("params_end");
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::PrTimerTimer(TObject *Sender)
{
  if (!HintLb->Caption.Length()) HintLb->Caption = SendConvCommand("get state str");
  UnicodeString Ret = SendConvCommand("get progress");
  if (Ret.LowerCase() == "ok")
   {
     PrTimer->Enabled = false;
     InitProgressCtrls();
     SetCtrlEnabled(true);
     PrTimer->Enabled = false;
     Ret = SendConvCommand("get last import state");
     if (Ret.Length() && Ret.LowerCase() != "ok")
      _MSG_INF(Ret,"���������");
   }
  else if (Ret.Length())
   {
     int TP, CP, TM, CM;
     TTagNode *PrData = new TTagNode(NULL);
     TTagNode *tmp;
     try
      {
        try
         {
           PrData->AsXML = Ret;
         }
        catch(...)
         {
         }
        tmp = PrData->GetFirstChild();
        if (tmp)
         {
           CP = tmp->AV["p"].ToIntDef(0);
           CM = tmp->AV["m"].ToIntDef(0);
           if (CurProgressLb->Caption != tmp->AV["PCDATA"])
            CurProgressLb->Caption = tmp->AV["PCDATA"];
           tmp = tmp->GetNext();
           TP = tmp->AV["p"].ToIntDef(0);
           TM = tmp->AV["m"].ToIntDef(0);
           if (TotalProgressLb->Caption != tmp->AV["PCDATA"])
            TotalProgressLb->Caption = tmp->AV["PCDATA"];

           if (CurPB->Properties->Max != CM)
             CurPB->Properties->Max = CM;
           if (TotalPB->Properties->Max != TM)
             TotalPB->Properties->Max = TM;

           if (CurPB->Position != CP)
             CurPB->Position = CP;
           if (TotalPB->Position != TP)
             TotalPB->Position = TP;
         }
      }
     __finally
      {
        delete PrData;
      }
   }
  else if (!Ret.Length())
   {
     PrTimer->Enabled = false;
     InitProgressCtrls();
     SetCtrlEnabled(true);
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::StopBtnClick(TObject *Sender)
{
  if (FInProgress)
   {
     if (_MSG_QUE("�� ������� � ���, ��� ������ �������� ���������� ����� ������?",
                  Application->Title.c_str()
                 ) == ID_YES)
      FInProgress = false;
   }
  else if (IsConvProgress())
   {
     if (_MSG_QUE("�� ������� � ���, ��� ������ �������� ���������� ����� ������?",
                  Application->Title.c_str()
                 ) == ID_YES)
      SendConvCommand("stop");
   }
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::WndProc(Messages::TMessage &Message)
{
  switch (Message.Msg)
  {
    case WM_CLOSE:
     {
       if (!FCanClose())
        {
          Message.Result = 0;
          return;
        }
       else
        inherited::WndProc( Message );
       break;
     }
  }
  inherited::WndProc( Message );
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::cmdClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
  FCmdRet = Socket->ReceiveText();
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::cmdClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
  FCmdRet = "Error";
  CmdRetTimer->Enabled = false;
  PrTimer->Enabled = false;
  _MSG_ERR("������ ��������� ������ �������. ���="+IntToStr(ErrorCode),"������");
  ErrorCode = 0;
}
//---------------------------------------------------------------------------
void __fastcall TImportCardForm::CmdRetTimerTimer(TObject *Sender)
{
  FCmdRet = "Error";
  CmdRetTimer->Enabled = false;
  PrTimer->Enabled = false;
  _MSG_ERR("������ ��������� ������ �������, �������� �������� ��������.","������");
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------

