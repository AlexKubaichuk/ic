//---------------------------------------------------------------------------

#ifndef ImportCardH
#define ImportCardH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//#include "TB2Dock.hpp"
//#include "TB2Item.hpp"
//#include "TB2Toolbar.hpp"
#include <ActnList.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxControls.hpp"
#include "cxGraphics.hpp"
#include "dxStatusBar.hpp"
#include "DMF.h"
//#include "RegEI.h"
//#include "ImmEI.h"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxProgressBar.hpp"
#include "cxCheckBox.hpp"
#include "cxStyles.hpp"
#include "cxGroupBox.hpp"
#include "cxDropDownEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Web.Win.Sockets.hpp>
#include <ScktComp.hpp>
#include "cxClasses.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
//---------------------------------------------------------------------------
class TImportCardForm : public TForm
{
__published:	// IDE-managed Components
        TOpenDialog *OpenDlg;
        TPanel *BtnPanel;
        TPanel *MsgPanel;
        TPanel *Panel1;
        TdxStatusBar *StatusBar;
        TLabel *MsgLab;
        TcxStyleRepository *cxStyleRepository1;
        TcxStyle *DisabledStyle;
        TcxStyle *EnabledStyle;
        TcxGroupBox *OrgParamGB;
        TcxGroupBox *ImportDataGB;
        TcxCheckBox *CardChB;
        TcxCheckBox *UnitImpChB;
        TcxCheckBox *ClassImpChB;
        TPanel *OrgLabPanel;
        TLabel *ExpOrgDataLab;
        TLabel *HintLb;
        TLabel *CurProgressLb;
        TcxProgressBar *CurPB;
        TLabel *TotalProgressLb;
        TcxProgressBar *TotalPB;
        TcxComboBox *ClassImpReplaceCB;
        TcxComboBox *UnitImpReplaceCB;
        TTimer *PrTimer;
        TcxButton *ImportBtn;
        TcxButton *StopBtn;
        TcxButton *CloseBtn;
        TPanel *Panel2;
        TcxButton *OpenBtn;
        TTimer *CmdRetTimer;
        void __fastcall OpenBtnClick(TObject *Sender);
        void __fastcall ImportBtnClick(TObject *Sender);
        void __fastcall UnitImpChBClick(TObject *Sender);
        void __fastcall CardChBClick(TObject *Sender);
        void __fastcall CloseBtnClick(TObject *Sender);
        void __fastcall ClassImpChBClick(TObject *Sender);
        void __fastcall PrTimerTimer(TObject *Sender);
        void __fastcall StopBtnClick(TObject *Sender);
        void __fastcall cmdClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall cmdClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall CmdRetTimerTimer(TObject *Sender);
private:	// User declarations
        TAnsiStrMap infSch;
        TAnsiStrMap infPSch;
        TStringList *FRepList;
        TList       *FUnitList;
        bool        FFileLoaded;
//        TTagNode     *EITag;
        UnicodeString   FCmdRet;
        int FImpVersion;
        UnicodeString   FImpFileName;
        bool FCanClsImport, FCanUnitImport, FCanCardImport;
        bool FInProgress;

        bool       __fastcall cmdServerConnect();
        bool       __fastcall IsConvProgress();
        void       __fastcall InitProgressCtrls();
        void       __fastcall SetCtrlEnabled(bool AVal);
        UnicodeString __fastcall SendConvCommand(UnicodeString ACmd);
        void       __fastcall SendStartCommand(UnicodeString ACmd, TStringList *AParams);

        void __fastcall FProgressInit(int AMax, UnicodeString AMsg);
        void __fastcall FProgressChange(int APercent, UnicodeString AMsg);
        void __fastcall FProgressComplite(UnicodeString AMsg);
        bool __fastcall FCanClose();
protected:
        virtual void __fastcall WndProc(Messages::TMessage &Message);
public:		// User declarations
        __fastcall TImportCardForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TImportCardForm *ImportCardForm;
//---------------------------------------------------------------------------
#endif

