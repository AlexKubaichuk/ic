﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "ConvertFrom67.h"
#include "JSONUtils.h"
#include "ExtUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TCard67Convertor::TCard67Convertor(TTagNode * ADataDef)
 {
  FDataDef     = ADataDef;
  InfMap["1"]  = "Туб.";
  InfMap["2"]  = "Пол.";
  InfMap["3"]  = "Диф.";
  InfMap["4"]  = "Кокл.";
  InfMap["5"]  = "Столб.";
  InfMap["6"]  = "Корь";
  InfMap["7"]  = "Парот.";
  InfMap["8"]  = "Бешен.";
  InfMap["9"]  = "Грипп";
  InfMap["10"] = "Клещ.";
  InfMap["11"] = "Ку-лих.";
  InfMap["12"] = "Менинг.";
  InfMap["13"] = "Красн.";
  InfMap["14"] = "Геп.В";
  InfMap["15"] = "ХИБ.";
  InfMap["16"] = "Пневм.";
  InfMap["17"] = "Триан.";
  InfMap["18"] = "Туляр.";
  InfMap["19"] = "Лепт.";
  InfMap["20"] = "Сиб.яз.";
  InfMap["21"] = "Бруц.";
  InfMap["22"] = "Бр.тиф";
  InfMap["23"] = "Чума";
  InfMap["24"] = "Глобул.";
  InfMap["25"] = "В.оспа";
  InfMap["26"] = "Геп.А";
  InfMap["30"] = "Хол.";
  InfMap["31"] = "Диз.Зонне";
  InfMap["32"] = "Ротав.";
  InfMap["34"] = "Желт.лих.";
  InfMap["35"] = "ВПЧ";
  OldDataMap   = new TJSONIdMap;
  FUnitCard    = new TJSONUnitIdMap;
 }
// ---------------------------------------------------------------------------
__fastcall TCard67Convertor::~TCard67Convertor()
 {
  // TJSONIdMap *OldDataMap;  TJSONIdMap TJSONMap TJSONObject
  for (TJSONIdMap::iterator i = OldDataMap->begin(); i != OldDataMap->end(); i++)
   {
    for (TJSONMap::iterator j = i->second->begin(); j != i->second->end(); j++)
     delete j->second;
    i->second->clear();
    delete i->second;
   }
  OldDataMap->clear();
  delete OldDataMap;
  // TJSONUnitIdMap *FUnitCard; TJSONUnitIdMap TJSONIdMap TJSONMap TJSONObject
  for (TJSONUnitIdMap::iterator i = FUnitCard->begin(); i != FUnitCard->end(); i++)
   {
    for (TJSONIdMap::iterator j = i->second->begin(); j != i->second->end(); j++)
     {
      for (TJSONMap::iterator k = j->second->begin(); k != j->second->end(); k++)
       delete k->second;
      j->second->clear();
      delete j->second;
     }
    i->second->clear();
    delete i->second;
   }
  FUnitCard->clear();
  delete FUnitCard;
  FUchOtdMap.clear();
  FPrivMap.clear();
  FOtvMap.clear();
  InfMap.clear();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
bool __fastcall TCard67Convertor::Prepare(TTagNode * AData, TkabProgress AProgress)
 {
  bool RC = false;
  bool FInProgress = true;
  try
   {
    int FCurCount, FCommCount;
    TJSONObject * FPreRecData;
    // ## подготовка классификаторов [begin] ##################################################
    TTagNode * Cls = AData->GetChildByName("cls");
    if (Cls && FInProgress)
     {
      FCommCount = Cls->Count;
      if (AProgress)
       AProgress(TkabProgressType::InitCommon, FCommCount * 1000, "Подготовка справочников ...");
      TTagNode * itCls = Cls->GetFirstChild();
      TTagNode * itClsRec;
      TJSONIdMap::iterator ClFRC;
      TJSONMap::iterator RecFRC;
      TJSONMap * FClMap;
      FCurCount = 0;
      while (itCls && FInProgress)
       { // цикл по классификаторам
        ClFRC = OldDataMap->find(itCls->AV["uid"].UpperCase());
        if (ClFRC == OldDataMap->end())
         {
          FClMap = new TJSONMap;
          (*OldDataMap)[itCls->AV["uid"].UpperCase()] = FClMap;
         }
        else
         FClMap = ClFRC->second;
        itClsRec = itCls->GetFirstChild();
        while (itClsRec && FInProgress)
         { // цикл по записям классификатора
          RecFRC      = FClMap->find(itClsRec->AV["cd"].UpperCase());
          FPreRecData = PrepareRecData(itClsRec);
          if (RecFRC == FClMap->end())
           (*FClMap)[itClsRec->AV["cd"].UpperCase()] = FPreRecData;
          if (itCls->CmpAV("uid", "0010"))
           {
            FUchOtdMap[JSONToString(FPreRecData, "0012")] = JSONToString(FPreRecData, "0011");
           }
          itClsRec = itClsRec->GetNext();
         }
        FCurCount++ ;
        if (AProgress)
         FInProgress = AProgress(TkabProgressType::CommInc, (FCurCount * 1000) / FCommCount,
          "Подготовка справочников: " + IntToStr(FCurCount) + "/" + IntToStr(FCommCount));
        itCls = itCls->GetNext();
       }
      if (AProgress)
       AProgress(TkabProgressType::CommComplite, 0, "");
     }
    // ## подготовка классификаторов [end ] ##################################################
    // ## подготовка списка пациентов [begin] ##################################################
    TTagNode * Unit = AData->GetChildByName("unit");
    if (Unit && FInProgress)
     {
      FCurCount  = 0;
      FCommCount = Unit->Count;
      if (AProgress)
       AProgress(TkabProgressType::InitCommon, FCommCount * 1000, "Подготовка списка пациентов ...");
      TTagNode * itUnitRec;
      TJSONMap::iterator RecFRC;
      TJSONMap * FUnitMap = new TJSONMap;
      (*OldDataMap)["UNIT"] = FUnitMap;
      itUnitRec = Unit->GetFirstChild();
      while (itUnitRec && FInProgress)
       { // цикл по записям списка пациентов
        RecFRC = FUnitMap->find(itUnitRec->AV["cd"].UpperCase());
        if (RecFRC == FUnitMap->end())
         {
          FPreRecData = PrepareRecData(itUnitRec);
          (*FUnitMap)[itUnitRec->AV["cd"].UpperCase()] = FPreRecData; // PrepareRecData(itUnitRec);
         }
        itUnitRec = itUnitRec->GetNext();
        FCurCount++ ;
        if (AProgress)
         FInProgress = AProgress(TkabProgressType::CommInc, (FCurCount * 1000) / FCommCount,
          "Подготовка списка пациентов: " + IntToStr(FCurCount) + "/" + IntToStr(FCommCount));
       }
      if (AProgress)
       AProgress(TkabProgressType::CommComplite, 0, "");
     }
    // ## подготовка списка пациентов [end] ##################################################
    // ## подготовка прививочных карт [begin] ##################################################
    TTagNode * Card = AData->GetChildByName("cards");
    if (Card && FInProgress)
     {
      TTagNode * itCard = Card->GetFirstChild();
      TTagNode * itCardRec;
      TJSONUnitIdMap::iterator CardFRC;
      TJSONIdMap::iterator FUnitCardFRC;
      TJSONMap::iterator RecFRC;
      TJSONIdMap * FCardMap;
      TJSONMap * FUnitRecCardMap;
      TJSONObject * FCardRecData;
      while (itCard && FInProgress)
       { // цикл по разделам прививочной карты
        if (itCard->CmpAV("uid", "1003,102f,1030"))
         {
          FCurCount  = 0;
          FCommCount = itCard->Count;
          if (AProgress)
           AProgress(TkabProgressType::InitCommon, FCommCount * 1000, "Подготовка карт, раздел [" + itCard->AV["uid"] +
            "] ...");
          CardFRC = FUnitCard->find(itCard->AV["uid"].UpperCase());
          if (CardFRC == FUnitCard->end())
           {
            FCardMap = new TJSONIdMap;
            (*FUnitCard)[itCard->AV["uid"].UpperCase()] = FCardMap;
           }
          else
           FCardMap = CardFRC->second;
          itCardRec = itCard->GetFirstChild();
          while (itCardRec && FInProgress)
           { // цикл по записям раздела прививочной карты
            FCardRecData = PrepareRecData(itCardRec);
            if (FCardRecData)
             {
              FUnitCardFRC = FCardMap->find(JSONToString(FCardRecData, "UCODE").UpperCase());
              if (FUnitCardFRC == FCardMap->end())
               {
                FUnitRecCardMap = new TJSONMap;
                (*FCardMap)[JSONToString(FCardRecData, "UCODE").UpperCase()] = FUnitRecCardMap;
               }
              else
               FUnitRecCardMap = FUnitCardFRC->second;
              RecFRC = FUnitRecCardMap->find(itCardRec->AV["cd"].UpperCase());
              if (RecFRC == FUnitRecCardMap->end())
               (*FUnitRecCardMap)[itCardRec->AV["cd"].UpperCase()] = FCardRecData;
             }
            itCardRec = itCardRec->GetNext();
            FCurCount++ ;
            if (AProgress)
             FInProgress = AProgress(TkabProgressType::CommInc, (FCurCount * 1000) / FCommCount,
              "Подготовка карт, раздел [" + itCard->AV["uid"] + "]: " + IntToStr(FCurCount) + "/" +
              IntToStr(FCommCount));
           }
          if (AProgress)
           AProgress(TkabProgressType::CommComplite, 0, "");
         }
        itCard = itCard->GetNext();
       }
     }
    // ## подготовка прививочных карт [end] ##################################################
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TCard67Convertor::PrepareRecData(TTagNode * ARec)
 {
  TJSONObject * RC = new TJSONObject;
  UnicodeString FVal = ARec->AV["val"];
  UnicodeString FPair, FField, FFieldVal;
  try
   {
    TReplaceFlags Flags;
    Flags << rfReplaceAll << rfIgnoreCase;
    FVal.Delete(1, 1);
    FVal = StringReplace(FVal, "$R", "$", Flags);
    RC->AddPair("CODE", ARec->AV["cd"]);
    while (FVal.Length())
     {
      FPair     = GetLPartB(FVal, '$');
      FField    = GetLPartB(FPair, '=').Trim();
      FFieldVal = GetRPartB(FPair, '=').Trim();
      if (FField.Length())
       {
        if (FField.UpperCase() != "LAST_INQDATE")
         {
          if (FFieldVal.UpperCase() == "NULL__")
           RC->AddPair(FField.UpperCase(), new TJSONNull);
          else
           RC->AddPair(FField.UpperCase(), FFieldVal);
         }
       }
      FVal = GetRPartB(FVal, '$').Trim();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCard67Convertor::Convert(TTagNode * AData, __int64 ALPUCode,
  TCard67ConvertorOnImportDataEvent AOnImport, TkabProgress AProgress, UnicodeString & ALastCode)
 {
  bool RC = false;
  TJSONObject * cdData;
  TJSONObject * URData = NULL;
  TJSONObject * FCls = NULL;
  bool FInProgress = true;
  // DDD = new TStringList;
  int FCardCount;
  try
   {
    UnicodeString MI = "";
    // DDD->Add("start,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
    if (Prepare(AData, AProgress))
     {
      // DDD->Add("prepare,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
      TTagNode * Units = AData->GetChildByName("unit");
      TTagNode * Priv = AData->GetTagByUID("1003");
      TTagNode * Test = AData->GetTagByUID("102f");
      TTagNode * Otv = AData->GetTagByUID("1030");
      TTagNode * ClsNode = AData->GetChildByName("cls");
      if (ClsNode)
       ClsNode->DeleteChild();
      Sleep(50);
      Application->ProcessMessages();
      if (Priv)
       Priv->DeleteChild();
      Sleep(50);
      Application->ProcessMessages();
      if (Test)
       Test->DeleteChild();
      Sleep(50);
      Application->ProcessMessages();
      if (Otv)
       Otv->DeleteChild();
      Sleep(50);
      Application->ProcessMessages();
      // DDD->Add("clear,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
      if (Units)
       {
        TTagNode * itUnit = Units->GetFirstChild();
        TDate OtvDate = TDate(0);
        TDate FBirthday = TDate(0);
        int FCommCount = Units->Count;
        int FCurCount = 0;
        if (AProgress)
         AProgress(TkabProgressType::InitCommon, FCommCount * 1000, "Прием карт ...");
        while (itUnit && FInProgress)
         {
          // конвертируем пациента
          FCls = new TJSONObject;
          if (URData)
           delete URData;
          URData = new TJSONObject;
          // ShowMessage("1");
          // DDD->Add("1,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
          ConvertUnit(URData, FCls, itUnit->AV["cd"], FBirthday, OtvDate, ALPUCode, AProgress);
          // ShowMessage("2");
          // DDD->Add("2,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
          if (Priv || Test || Otv || (int)OtvDate)
           { // конвертируем прививки, пробы, отводы
            cdData = new TJSONObject;
            URData->AddPair("cd", cdData);
            // ShowMessage("3");
            // DDD->Add("3,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
            Convert1003(cdData, itUnit->AV["cd"], FBirthday, ALPUCode, AProgress, FCardCount);
            // DDD->Add("4,"+PrintProcessMemoryInfo(GetCurrentProcessId())+","+IntToStr(FCardCount));
            // ShowMessage("4");
            Convert102f(cdData, itUnit->AV["cd"], FBirthday, ALPUCode, AProgress, FCardCount);
            // DDD->Add("5,"+PrintProcessMemoryInfo(GetCurrentProcessId())+","+IntToStr(FCardCount));
            // ShowMessage("5");
            Convert1030(cdData, itUnit->AV["cd"], FBirthday, OtvDate, AProgress, FCardCount);
            // DDD->Add("6,"+PrintProcessMemoryInfo(GetCurrentProcessId())+","+IntToStr(FCardCount));
            // ShowMessage("6");
           }
          URData->AddPair("cl", FCls);
          // DDD->Add("7,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
          // URData->Owned = false;
          AOnImport((TJSONObject *)TJSONObject::ParseJSONValue(URData->ToString()), ALastCode);
          if (URData)
           delete URData;
          URData            = NULL;
          itUnit->AV["val"] = "";
          itUnit            = itUnit->GetNext();
          FCurCount++ ;
          Application->ProcessMessages();
          // DDD->Add("fin,"+PrintProcessMemoryInfo(GetCurrentProcessId()));
          if (AProgress)
           FInProgress = AProgress(TkabProgressType::CommInc, (FCurCount * 1000) / FCommCount,
            "Прием карт: " + IntToStr(FCurCount) + "/" + IntToStr(FCommCount));
         }
       }
     }
   }
  __finally
   {
    // DDD->SaveToFile("d:\\work\\aaaaaaaaa.txt");
    // delete DDD;
    if (AProgress)
     AProgress(TkabProgressType::CommComplite, 0, "");
    if (URData)
     delete URData;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCard67Convertor::ConvertUnit(TJSONObject * ACardRoot, TJSONObject * ACls, UnicodeString AUCode,
  TDate & ABirthday, TDate & AOtvToDate, __int64 ALPUCode, TkabProgress AProgress)
 {
  bool RC = false;
  ABirthday  = 0;
  AOtvToDate = 0;
  try
   {
    UnicodeString FVal, FVal2;
    TJSONObject * FUnitData;
    FUnitData = GetOldRec("UNIT", AUCode);
    int SupportType;
    if (FUnitData)
     {
      SupportType = 6; // <choice name='Статус обслуживания' uid='32B3'   >
      // Обслуживается по участку - 1
      // Обслуживается по организации - 2
      // Обслуживается по полису - 3
      // Контролируется - 4
      // Архив - 5
      // Не обслуживается - 6
      // -> <text name='Фамилия' uid='001F' length='30'/>
      // -> <text name='Имя' uid='0020' length='20'/>
      // -> <text name='Отчество' uid='002F' length='30'/>
      // <text name='№ карты' uid='0122'    minleft='90' length='30'/>
      // -> <date name='Дата регистрации' uid='0030'  inlist='no'  default='CurrentDate'/>
      // -> <date name='Дата рождения' uid='0031' />
      // -> <extedit name='Возраст' uid='00B2'   depend='0031' minleft='90' length='10'/><extedit name='Возраст' uid='00B2' depend='0031'  />
      ABirthday = JSONToDate(FUnitData, "0031");
      PrepareValue(FUnitData, "0021"); // -> <choice name='Пол' uid='0021'  default='2'>
      // <extedit name='Населённый пункт' uid='0032' depend='0136.0136' length='11'/>
      // <extedit name='Улица' uid='00FF' depend='0136.0136' length='4'/>
      // <extedit name='Дом' uid='0100' depend='0136.0136' length='50'/>
      // PrepareValue(FUnitData, "0033"); //-> <extedit name='Квартира' len='15'/> -> <text len='5'>
      // -> <extedit name='Адрес' uid='0136' comment='search'   linecount='3' showtype='1' length='40'/><choiceBD name='Адрес' uid='0032' ref='0017'   >
      // <choiceDB name='ЛПУ' uid='00F1' ref='00C7'   default='Main' />
      DeleteJSONPair(FUnitData, "00F1");
      FUnitData->AddPair("00F1", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      DeleteJSONPair(FUnitData, "00B3");
      // -> 00B3 <- 00B3 // <choiceDB name='Участок' uid='00B3'/><extedit name='Участок' uid='00B3'/>
      FVal = JSONToString(FUnitData, "0032");
      UnicodeString AddrStr = "";
      if (JSONToString(FUnitData, "0022").ToIntDef(0))
       { // есть адрес и он формализован
        TJSONObject * recData = GetOldRec("0017", FVal);
        if (recData)
         { // адрес найден в справочнике
          FVal = JSONToString(recData, "001E"); // код участка
          if (FVal.ToIntDef(0))
           {
            FUnitData->AddPair("00B3", FVal);
            PrepareValue(FUnitData, "00B3");
            // ->  <choiceDB name='Участок' uid='00B3'/><extedit name='Участок' uid='00B3'/>
            SupportType = 1; // Обслуживается по участку - 1
            FVal2       = "";
            TAnsiStrMap::iterator FRC = FUchOtdMap.find(FVal);
            if (FRC != FUchOtdMap.end())
             FVal2 = FRC->second;
            if (FVal2.ToIntDef(0))
             { // определено отделение
              DeleteJSONPair(FUnitData, "00F9");
              // <choiceDB name='отделение' uid='00F9' comment='empty' ref='000C'  depend='00F1.000C'  />
              FUnitData->AddPair("00F9", FVal2);
              PrepareValue(FUnitData, "00F9");
              SetClData(ACls, "000C", FVal2, ALPUCode, ""); // отделение
             }
            SetClData(ACls, "000E", FVal, ALPUCode, FVal2); // участок
           }
          AddrStr = JSONToString(recData, "001C");
          // <text name='№ дома' uid='001C' ref='no' required='yes' inlist='list' depend='no' cast='lo' length='5'/>
          FVal = JSONToString(recData, "001D");
          // <text name='Корпус' uid='001D' ref='no' inlist='list' depend='no' cast='lo' length='5'/>
          FVal2 = JSONToString(recData, "0018");
          // <class name='Улицы' uid='0018' ref='no' required='yes' inlist='list'>
          if (FVal.Length())
           {
            if (AddrStr.Length())
             AddrStr += ", корп. " + FVal;
            else
             AddrStr = FVal;
           }
          if (JSONToString(FUnitData, "0033").Trim().Length())
           AddrStr += ", кв. " + JSONToString(FUnitData, "0033").Trim();
          DeleteJSONPair(FUnitData, "0033"); // -> <extedit name='Квартира' len='15'/> -> <text len='5'>
          recData = GetOldRec("0018", FVal2);
          if (recData)
           { // улица найдена в справочнике
            FVal = JSONToString(recData, "001B");
            // <text name='Наименование' uid='001B' ref='no' required='yes' inlist='list' depend='no' cast='no' length='30'/>
            FVal2 = JSONToString(recData, "0019");
            // <class name='Населённые пункты' uid='0019' ref='no' required='yes' inlist='list'>
            if (FVal.Length())
             {
              if (AddrStr.Length())
               AddrStr = FVal + ", " + AddrStr;
              else
               AddrStr = FVal;
             }
            recData = GetOldRec("0019", FVal2);
            if (recData)
             { // населенный пункт найден в справочнике
              FVal = JSONToString(recData, "001A");
              // <<text name='Наименование' uid='001A' ref='no' required='yes' inlist='list' depend='no' cast='no' length='30'/>
              if (FVal.Length())
               {
                if (AddrStr.Length())
                 AddrStr = FVal + ", " + AddrStr;
                else
                 AddrStr = FVal;
               }
             }
           }
         }
       }
      else
       AddrStr = JSONToString(FUnitData, "0024"); // <text name='Неформализованный адрес' uid='0024' length='255' >
      DeleteJSONPair(FUnitData, "0024"); // []<text name='Неформализованный адрес' uid='0024' length='255' >
      DeleteJSONPair(FUnitData, "0032");
      DeleteJSONPair(FUnitData, "0136");
      if (AddrStr.Length())
       FUnitData->AddPair("0136", (new TJSONObject)->AddPair("-1", AddrStr));
      PrepareValue(FUnitData, "0022");
      // -> <binary name='Адрес обслуживается поликлиникой' uid='0022'  invname='Не обслуживается' default='check'/>
      ReplaceValue(FUnitData, "0148", "0034");
      // -> 0148 <- 0034 // <text name='Мобильный' format='8(\d\d\d)\d\d\d-\d\d-\d\d' length='20'/><text name='Телефон' uid='0034'  length='10'/>
      // <text name='Домашний' uid='014A' length='20'/>
      // <text name='Рабочий' uid='014C' length='20'/>
      DeleteJSONPair(FUnitData, "0023");
      // old <choice name='Организация' uid='0023'  >  new <choice name='Социальный статус' uid='0023' default='5'>
      PrepareValue(FUnitData, "0026");
      // -> <extedit name='Ур1' uid='0026' depend='0025.0025' length='20'/><text name='Уровень 1' uid='0026' ref='0005'  depend='0025.0002'  length='10' >
      PrepareValue(FUnitData, "0027");
      // -> <extedit name='Ур2' uid='0027' depend='0025.0025' length='20'/><text name='Уровень 2' uid='0027' ref='0006'  depend='0025.0002'  length='10' >
      PrepareValue(FUnitData, "0028");
      // -> <extedit name='Ур3' uid='0028' depend='0025.0025' length='20'/><text name='Уровень 3' uid='0028' ref='0007'  depend='0025.0002'  length='10' >
      // <extedit name='Должность/профессия' uid='0111' depend='0025.0025' length='50'/>
      // <extedit name='Период обучения' uid='0112' ref='0108' depend='0025.0025'  />
      // <extedit name='Группа организаций' uid='011B' ref='0117' depend='0025.0025' showtype='1'   >
      __int64 FOrgCode = JSONToInt(FUnitData, "0025");
      if (FOrgCode)
       {
        TJSONObject * clData = GetOldRec("0001", JSONToString(FUnitData, "0025"));
        if (clData)
         {
          if (JSONToInt(clData, "00AF"))
           {
            SupportType = 2; // Обслуживается по организации - 2
            DeleteJSONPair(FUnitData, "00F0"); // binary name='Обслуживаемая организация (своя)' uid='00F0'/>
            FUnitData->AddPair("00F0", 1);
            PrepareValue(FUnitData, "00F0");
           }
          else if (JSONToInt(clData, "00B1"))
           SupportType = 4; // Контролируется - 4
         }
        SetClData(ACls, "0001", FOrgCode, ALPUCode, ""); // отделение
       }
      PrepareValue(FUnitData, "0025");
      // -> <extedit name='Организация' uid='0025' comment='search' ref='0001'   depend='011B.0001' linecount='3' showtype='1'   ><choiceBD name='Организация' uid='0025' ref='0001'   linecount='0'>
      // RemoveJSONPair(FUnitData, "0116"); ///      <choice name='Неорганизован' uid='0116' linecount='1' default='3'>
      DeleteJSONPair(FUnitData, "32B3"); // <choice name='Статус обслуживания' uid='32B3'   >
      FUnitData->AddPair("32B3", SupportType);
      PrepareValue(FUnitData, "32B3");
      // <extedit name='Настройки' uid='111D' depend='111D.111D' length='42'/>
      // <text name='СНИЛС' uid='00A5' length='14'/>
      bool FClear = true;
      if (JSONToString(FUnitData, "0092").Trim().ToIntDef(0))
       { // Есть страховой полис
        FVal = JSONToString(FUnitData, "0093");
        if (FVal.ToIntDef(0) && /* JSONToString(FUnitData, "0099").Trim().Length() && */ JSONToString(FUnitData,
          "009A").Trim().Length())
         {
          FClear = false;
          SetClData(ACls, "0095", FVal, 0, ""); // Страх. компания
          PrepareValue(FUnitData, "0093"); // -> <choiceBD name='Страх. компания' uid='0093' ref='0095'   >
         }
        // -> <text name='Серия' uid='0099'   length='10' >
        // -> <text name='Номер' uid='009A'   length='15' >
       }
      if (FClear)
       {
        DeleteJSONPair(FUnitData, "0093"); // -> <choiceBD name='Страх. компания' uid='0093' ref='0095'   >
        DeleteJSONPair(FUnitData, "0099"); // -> <text name='Серия' uid='0099'   length='10' >
        DeleteJSONPair(FUnitData, "009A"); // -> <text name='Номер' uid='009A'   length='15' >
        FUnitData->AddPair("0093", new TJSONNull); // -> <choiceBD name='Страх. компания' uid='0093' ref='0095'   >
        FUnitData->AddPair("0099", new TJSONNull); // -> <text name='Серия' uid='0099'   length='10' >
        FUnitData->AddPair("009A", new TJSONNull); // -> <text name='Номер' uid='009A'   length='15' >
       }
      DeleteJSONPair(FUnitData, "0092"); // []<binary name='Есть страховой полис' uid='0092' />
      UnicodeString ULSer = JSONToString(FUnitData, "009D").Trim();
      UnicodeString ULNum = JSONToString(FUnitData, "009E").Trim();
      if (JSONToString(FUnitData, "009B").Trim().ToIntDef(0) && ULSer.Length() && ULNum.Length())
       { // Есть удостоверение личности
        FVal = JSONToString(FUnitData, "009C"); // -> <choice name='Уд. личности' uid='009C'  default='-1'>
        DeleteJSONPair(FUnitData, "009C");
        bool AddSerNum = true;
        // {0 -> 2}{1 -> 1}{2 -> 3}
        if (FVal.ToIntDef(-1) == 0)
         FUnitData->AddPair("009C", (new TJSONObject)->AddPair(2, 2));
        else if (FVal.ToIntDef(-1) == 1)
         FUnitData->AddPair("009C", (new TJSONObject)->AddPair(1, 1));
        else if (FVal.ToIntDef(-1) == 2)
         FUnitData->AddPair("009C", (new TJSONObject)->AddPair(3, 3));
        else
         {
          AddSerNum = false;
          FUnitData->AddPair("009C", new TJSONNull);
         }
        // -> <text name='Серия' uid='009D'   length='5' >
        // -> <text name='Номер' uid='009E'   length='15' >
        DeleteJSONPair(FUnitData, "009D"); // -> <text name='Серия' uid='009D'   length='5' >
        DeleteJSONPair(FUnitData, "009E"); // -> <text name='Номер' uid='009E'   length='15' >
        if (AddSerNum)
         {
          FUnitData->AddPair("009D", ULSer);
          FUnitData->AddPair("009E", ULNum);
         }
       }
      else
       {
        DeleteJSONPair(FUnitData, "009C"); // -> <choice name='Уд. личности' uid='009C'  default='-1'>
        DeleteJSONPair(FUnitData, "009D"); // -> <text name='Серия' uid='009D'   length='5' >
        DeleteJSONPair(FUnitData, "009E"); // -> <text name='Номер' uid='009E'   length='15' >
        FUnitData->AddPair("009C", new TJSONNull); // -> <choice name='Уд. личности' uid='009C'  default='-1'>
        FUnitData->AddPair("009D", new TJSONNull); // -> <text name='Серия' uid='009D'   length='5' >
        FUnitData->AddPair("009E", new TJSONNull); // -> <text name='Номер' uid='009E'   length='15' >
       }
      DeleteJSONPair(FUnitData, "009B"); // []<binary name='Есть удостоверение личности' uid='009B' />
      // <choice name='Группа крови' uid='0138' default='-1'>
      FVal = JSONToString(FUnitData, "0085");
      // -> new <text name='Аллергические реакции' uid='0085' length='255'/> old <binary name='(2) Аллергические проявления' uid='0085' invname='(2) Отсутствуют аллергические проявления'/>
      DeleteJSONPair(FUnitData, "0085");
      if (FVal.ToIntDef(0))
       FUnitData->AddPair("0085", "Есть");
      // <text name='Лекарственная непереносимость' uid='0139' length='255'/>
      FVal = JSONToString(FUnitData, "0087");
      // -> new <text name='Необычные реакции на прививки' uid='0087' length='255'/> old <binary name='(4) Необычные реакции на прививки' uid='0087' invname='(4) Отсутствуют необычные реакции на прививки'/>
      DeleteJSONPair(FUnitData, "0087");
      if (FVal.ToIntDef(0))
       FUnitData->AddPair("0087", "Есть");
      PrepareValue(FUnitData, "0084");
      // -> <binary name='(1) Возможное поражение ЦНС' uid='0084' invname='(1) Отсутствует возможное поражение ЦНС'/>
      PrepareValue(FUnitData, "0086");
      // -> <binary name='(3) Часто болеющий' uid='0086' invname='(3) Не часто болеющий'/>
      PrepareValue(FUnitData, "0088");
      // -> <binary name='(Т) Риск по туберкулёзу' uid='0088' invname='(Т) Отсутствует риск по туберкулёзу'/>
      PrepareValue(FUnitData, "005A"); // -> <binary name='(В) Риск ВИЧ' uid='005A'/>
      PrepareValue(FUnitData, "00A7"); // -> <binary name='(И) Иммунодефицитное состояние' uid='00A7'/>
      PrepareValue(FUnitData, "005C"); // -> <binary name='(Н) Наличие зависимостей' uid='005C'/>
      ReplAndPrepValue(FUnitData, "0131", "00A8");
      // -> 0131 <- 00A8 // <binary name='(О) Оформлена инвалидность' uid='00A8'/>
      // <date name='Дата оформления' uid='0132'  >
      // <text name='Номер свидетельства' uid='0133' length='30' >
      FVal = JSONToString(FUnitData, "00C4");
      PrepareValue(FUnitData, "00C4"); // -> <binary name='Сверка выполнена' uid='00C4'/>
      if (!FVal.ToIntDef(0))
       {
        DeleteJSONPair(FUnitData, "00C5");
        FUnitData->AddPair("00C5", new TJSONNull);
       }
      else
       {
        UnicodeString FDateVal = JSONToString(FUnitData, "00C5");
        DeleteJSONPair(FUnitData, "00C5");
        FUnitData->AddPair("00C5", FDateVal);
       }
      // -> <date name='На' uid='00C5'  >
      // -> <date name='На' uid='00C5'  >
      // <choiceDB name='ЛПУ' uid='00F3' ref='00C7'/>
      FVal = JSONToString(FUnitData, "00BF");
      if (FVal.ToIntDef(0))
       SetClData(ACls, "00BD", FVal, 0, ""); // Доп. информация ()
      PrepareValue(FUnitData, "00BF"); // -> 00BF <- 00BF // <choiceBD name='Доп. информация 1' uid='00BF' ref='00BD'/>
      FUnitData->AddPair("330E", (new TJSONObject)->AddPair(3, "Импорт карт (синхронизация)"));
      // -> 00C1 <- 00C1 // <text name='Примечания' uid='00C1' length='150'/>
      // <date name='Мин. дата плана' uid='3195' depend='3195.3195'/>
      FUnitData->AddPair("3195", new TJSONNull); // []<choice name='Тип сети' uid='0036'  inlist='no' >
      DeleteJSONPair(FUnitData, "0036"); // []<choice name='Тип сети' uid='0036'  inlist='no' >
      FVal = JSONToString(FUnitData, "0083"); // []<binary name='Временно выбыл' uid='0083'/> //отвод
      DeleteJSONPair(FUnitData, "0083");
      if (FVal.ToIntDef(0))
       {
        FVal = JSONToString(FUnitData, "00BC"); // []<date name='Временно выбыл До' uid='00BC'  >
        DeleteJSONPair(FUnitData, "00BC");
        if (TryStrToDate(FVal, AOtvToDate))
         if (AOtvToDate <= Date())
          AOtvToDate = 0;
       }
      ACardRoot->AddPair("pt", TJSONObject::ParseJSONValue(FUnitData->ToString()));
     }
   }
  __finally
   {
    EraseOldRec("UNIT", AUCode);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCard67Convertor::Convert1003(TJSONObject * ACardRoot, UnicodeString AUCode, TDate ABirthday,
  __int64 ALPUCode, TkabProgress AProgress, int & ACount)
 {
  bool RC = false;
  ACount = 0;
  try
   {
    TJSONMap * CardMap;
    TJSONMap::iterator itCardMap;
    TJSONObject * FCardData;
    CardMap = GetOldUnitCard("1003", AUCode);
    if (CardMap && CardMap->size())
     {
      ACount = CardMap->size();
      TJSONObject * privData = new TJSONObject;
      ACardRoot->AddPair("3003", privData);
      FPrivMap.clear();
      UnicodeString Priv_Code, Priv_Val;
      TJSONMap::iterator FPrivFRC;
      for (itCardMap = CardMap->begin(); itCardMap != CardMap->end(); itCardMap++)
       {
        FCardData = itCardMap->second;
        Priv_Code = (JSONToString(FCardData, "1024") + "_" + JSONToString(FCardData, "1020")).UpperCase();
        Priv_Val  = JSONToString(FCardData, "1021") + ":" + JSONToString(FCardData, "1075") + "^" +
          JSONToString(FCardData, "1092");
        FPrivFRC = FPrivMap.find(Priv_Code);
        if (FPrivFRC == FPrivMap.end())
         {
          FCardData->AddPair("CODE", itCardMap->first);
          FCardData->AddPair("317A", AUCode);
          // <extedit  name='Код пациента'            uid='317A' ref='1000' depend='317A.317A' showtype='0' btncount='' fieldtype='integer' length='' default=''/>
          PrepareValue(FCardData, "317A");
          // <extedit     name='Код прививки'            uid='3183' ref='3003' depend='3183.3183' showtype='0' btncount='' fieldtype='integer' length='' default=''/>
          // <extedit     name='Код плана'               uid='32AD' ref='3084' depend='32AD.32AD' showtype='0' btncount='' fieldtype='integer' length='' default=''/>
          // <extedit     name='Возраст'                 uid='30B4' comment='calc' inlist='l' depend='3024' showtype='0' btncount='' fieldtype='varchar' length='9' default=''/>
          ReplaceValue(FCardData, "3024", "1024"); // <date     name='Дата выполнения'         uid='3024'
          /*
           void __fastcall TCard67Convertor::ReplaceValue(TJSONObject * AData, UnicodeString AId1, UnicodeString AId2)
           {
           AData->AddPair(AId1, JSONCopy(AData, AId2));
           }
           // ---------------------------------------------------------------------------
           void __fastcall TCard67Convertor::PrepareValue(TJSONObject * AData, UnicodeString AId)
           {
           TJSONValue * FVal = JSONCopy(AData, AId);
           if (FVal)
           {
           if (!FVal->Null)
           {
           FVal = (new TJSONObject)->AddPair(JSONToString(AData, AId), FVal);
           DeleteJSONPair(AData, AId);
           AData->AddPair(AId.UpperCase(), FVal);
           }
           }
           }
           // ---------------------------------------------------------------------------
           void __fastcall TCard67Convertor::ReplAndPrepValue(TJSONObject * AData, UnicodeString AId1, UnicodeString AId2)
           {
           ReplaceValue(AData, AId1, AId2);
           PrepareValue(AData, AId1);
           }
           // ---------------------------------------------------------------------------
           */
          int VacCode = JSONToString(FCardData, "1020").ToIntDef(0);
          if ((VacCode == 159) && (VacCode == 160))
           {
            DeleteJSONPair(FCardData, "1020");
            TJSONValue * FVal = (new TJSONObject)->AddPair(IntToStr(VacCode + 1), IntToStr(VacCode + 1));
            FCardData->AddPair("3020", FVal);
           }
          else
           ReplAndPrepValue(FCardData, "3020", "1020");
          // <choiceDB name='МИБП'                    uid='3020' ref='0035' required='1' inlist='l' depend='3075.002D' linecount=''/>
          ReplaceValue(FCardData, "3021", "1021"); // <text     name='Вид'                     uid='3021'
          FCardData->AddPair("3185", Priv_Val);
          // <text     name='Вид по инфекциям'        uid='3185' cast='no' fixedvar='0' format='' length='255' default=''/>
          DeleteJSONPair(FCardData, "1075");
          // <choiceDB name='Инфекция'                uid='1075' ref='003A' required='1' depend='1020.002D' linecount=''/>                       //->          <choiceDB name='Инфекция' uid='1075' ref='003A'
          ReplaceValue(FCardData, "3022", "1022");
          // <digit    name='Доза'                    uid='3022' />  //1022//->          <digit name='Доза' uid='1022'
          ReplaceValue(FCardData, "3023", "1023");
          // <text     name='Серия'                   uid='3023' >  //1023//->          <text name='Серия' uid='1023'
          ReplAndPrepValue(FCardData, "3025", "1025");
          // <binary   name='Реакция проверена'       uid='3025' required='1' default='uncheck'/>
          ReplAndPrepValue(FCardData, "3026", "1026");
          // <binary   name='Есть общая реакция'      uid='3026' required='1' default='uncheck'>
          ReplAndPrepValue(FCardData, "3028", "1028");
          // <binary   name='Есть местная реакция'    uid='3028' required='1' default='uncheck'>
          ReplAndPrepValue(FCardData, "30A8", "10A8");
          // <choiceDB name='Формат реакции'          uid='30A8' comment='empty' ref='0038' required='1' depend='3020.0039' linecount=''>
          ReplaceValue(FCardData, "3027", "1027");
          // <digit    name='Значение реакции'        uid='3027' > //1027//-> <digit name='Значение реакции' uid='1027'
          ReplAndPrepValue(FCardData, "30AE", "10AE");
          // <binary   name='Туровая прививка'        uid='30AE' required='1' default='uncheck'/>
          DeleteJSONPair(FCardData, "1092");
          // <extedit  name='Схема'                   uid='1092'    //->          <extedit name='Схема' uid='1092'
          ReplAndPrepValue(FCardData, "30AF", "10AF");
          // <choice   name='Тур закрыт'              uid='30AF' default='0'>
          if (JSONToString(FCardData, "101F").ToIntDef(0))
           {
            FCardData->AddPair("317F", ALPUCode);
            // <choiceDB name='ЛПУ' uid='317F' ref='00C7' inlist='e' linecount='2'/>
            PrepareValue(FCardData, "317F");
           }
          ReplAndPrepValue(FCardData, "301F", "101F");
          // <binary   name='Выполнена в данном ЛПУ'  uid='301F' required='1' invname='Выполнена в другой организации' default='check'/>
          ReplAndPrepValue(FCardData, "30B0", "10B0");
          // <choiceDB name='Врач'                    uid='30B0' ref='00AA' depend='317F.00AA;3180.00AA' />
          // <choiceDB    name='Мед. сестра'             uid='3181' ref='00AA' depend='317F.00AA;3180.00AA' />
          ReplAndPrepValue(FCardData, "30B1", "10B1");
          // <choiceDB name='Источник финансирования' uid='30B1' ref='00AB' />
          ReplAndPrepValue(FCardData, "3182", "2002");
          // <choiceDB name='Доп. информация'         uid='3182' ref='00BE' />
          FCardData->AddPair("3155", ABirthday);
          // <date     name='ДР пациента'             uid='3155' /> <date name='ДР пациента' uid='1155' isedit='0' default='patbd'/>
          FPrivMap[Priv_Code] = FCardData;
         }
        else
         {
          Priv_Val = JSONToString(FPrivFRC->second, "3185") + ";" + Priv_Val;
          DeleteJSONPair(FPrivFRC->second, "3185");
          FPrivFRC->second->AddPair("3185", Priv_Val);
         }
       }
      for (itCardMap = FPrivMap.begin(); itCardMap != FPrivMap.end(); itCardMap++)
       privData->AddPair(JSONToString(itCardMap->second, "CODE"),
        TJSONObject::ParseJSONValue(itCardMap->second->ToString()));
     }
   }
  __finally
   {
    EraseOldUnitCard("1003", AUCode);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCard67Convertor::Convert102f(TJSONObject * ACardRoot, UnicodeString AUCode, TDate ABirthday,
  __int64 ALPUCode, TkabProgress AProgress, int & ACount)
 {
  bool RC = false;
  ACount = 0;
  try
   {
    TJSONMap * CardMap;
    TJSONMap::iterator itCardMap;
    TJSONObject * FCardData;
    CardMap = GetOldUnitCard("102F", AUCode);
    if (CardMap && CardMap->size())
     {
      ACount = CardMap->size();
      TJSONObject * testData = new TJSONObject;
      ACardRoot->AddPair("102F", testData);
      for (itCardMap = CardMap->begin(); itCardMap != CardMap->end(); itCardMap++)
       {
        FCardData = itCardMap->second;
        FCardData->AddPair("017B", AUCode);
        // <extedit name='Код пациента' uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
        PrepareValue(FCardData, "017B");
        // <extedit name='Код плана' uid='32AF' ref='3084' depend='32AF.32AF' isedit='0' fieldtype='integer'/>
        // -> <date name='Дата выполнения' uid='1031'
        // <extedit name='Возраст' uid='10B5'
        // <choiceDB name='Инфекция' uid='1200' ref='003A'/>
        if (JSONToString(FCardData, "1032").ToIntDef(0) == 1)
         {
          DeleteJSONPair(FCardData, "1200");
          FCardData->AddPair("1200", "1");
         }
        PrepareValue(FCardData, "1032"); // -> <choiceDB name='Проба' uid='1032' ref='002A' required='1' inlist='l'/>
        PrepareValue(FCardData, "1200"); // -> <choiceDB name='Проба' uid='1032' ref='002A' required='1' inlist='l'/>
        // -> <text name='Серия препарата' uid='1033' inlist='l' cast='no' length='20'/>
        PrepareValue(FCardData, "1034"); // -> <choice name='Повод' uid='1034' required='1' inlist='e' default='0'>
        PrepareValue(FCardData, "103B");
        // -> <binary name='Реакция проверена' uid='103B' required='1' inlist='e' default='check'/>
        // []<binary name='Есть реакция' uid='1041' ref='' required='no' inlist='list_ext' depend='' invname='Реакция отрицательная' isedit='yes' is3D='yes' default='uncheck'>
        if (!JSONToString(FCardData, "1041").ToIntDef(0))
         {
          DeleteJSONPair(FCardData, "10A7");
          FCardData->AddPair("10A7", "27");
         }
        PrepareValue(FCardData, "10A7");
        // -> <choiceDB name='Формат реакции' uid='10A7' comment='empty' ref='0038' required='1' inlist='l' depend='1032.002B'>
        // -> <digit name='Значение' uid='103F' required='1' inlist='l' digits='13' decimal='6' min='0' max='999999'>
        // <digit name='Значение 2' uid='0184' required='1' inlist='e' digits='13' decimal='6' min='0' max='999999'>
        // <digit name='Значение 3' uid='0186' required='1' inlist='e' digits='13' decimal='6' min='0' max='999999'>
        // <digit name='Значение 4' uid='0188' required='1' inlist='e' digits='13' decimal='6' min='0' max='999999'>
        // <digit name='Значение 5' uid='018A' required='1' inlist='e' digits='13' decimal='6' min='0' max='999999'>
        PrepareValue(FCardData, "1093");
        // -> <extedit name='Схема' uid='1093' required='1' inlist='e' isedit='0' length='9'/>
        if (JSONToString(FCardData, "10A5").ToIntDef(0))
         {
          FCardData->AddPair("018B", ALPUCode);
          // <choiceDB name='ЛПУ' uid='018B' ref='00C7' inlist='e' linecount='2'/>
          PrepareValue(FCardData, "018B");
         }
        PrepareValue(FCardData, "10A5");
        // -> <binary name='Выполнена в данном ЛПУ' uid='10A5' required='1' inlist='e' invname='Выполнена в другой организации' isedit='0' default='check'/>
        PrepareValue(FCardData, "10B2");
        // -> <choiceDB name='Врач' uid='10B2' comment='empty' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
        // <choiceDB name='Мед. сестра' uid='018D' comment='empty' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
        PrepareValue(FCardData, "10B3");
        // -> <choiceDB name='Источник финансирования' uid='10B3' ref='00AB' inlist='e' linecount='2'/>
        ReplAndPrepValue(FCardData, "018E", "2003");
        // <choiceDB name='Доп. информация' uid='018E' ref='00BE' inlist='e' linecount='3'/><choiceBD name='Доп. информация' uid='2003' ref='00BE' required='no' inlist='list_ext' depend='' isedit='yes' is3D='yes' default='' linecount=''/>
        // -> <digit name='Предыдущее значение реакции' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
        // -> <digit name='Предыдущий формат реакции' uid='10C2' digits='2' min='1' max='99' isedit='0'/>
        FCardData->AddPair("1156", ABirthday); // <date name='ДР пациента' uid='1156' isedit='0' default='patbd'/>
        testData->AddPair(itCardMap->first, TJSONObject::ParseJSONValue(FCardData->ToString()));
       }
     }
   }
  __finally
   {
    EraseOldUnitCard("102F", AUCode);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCard67Convertor::Convert1030(TJSONObject * ACardRoot, UnicodeString AUCode, TDate ABirthday,
  TDate ADayTo, TkabProgress AProgress, int & ACount)
 {
  bool RC = false;
  ACount = 0;
  TStringList * FInfList = new TStringList;
  try
   {
    TJSONMap * CardMap;
    TJSONMap::iterator itCardMap;
    TJSONObject * FCardData;
    UnicodeString FVal;
    CardMap = GetOldUnitCard("1030", AUCode);
    if (CardMap && CardMap->size() || (int)ADayTo)
     {
      TJSONObject * otvData = new TJSONObject;
      ACardRoot->AddPair("3030", otvData);
      if ((int)ADayTo)
       { // добавляем отвод на основании "Временно выбыл"
        FCardData = new TJSONObject;
        try
         {
          FCardData->AddPair("code", AUCode);
          FCardData->AddPair("317C", AUCode);
          // <extedit name='Код пациента'       uid='317C' ref='1000' depend='317C.317C' isedit='0' fieldtype='integer'/>
          PrepareValue(FCardData, "317C");
          FCardData->AddPair("305F", Date());
          // <date    name='Начало действия'    uid='305F' required='1' inlist='l'/><date name='Начало действия' uid='105F'
          FCardData->AddPair("3065", ADayTo);
          // <date    name='Окончание действия' uid='3065' inlist='l'/><date name='Окончание действия' uid='1065'
          FCardData->AddPair("3043", (new TJSONObject)->AddPair("0", "Временный"));
          // <choice  name='Отвод'              uid='3043' required='1' inlist='l' default='0'><choice name='Отвод' uid='1043'
          FCardData->AddPair("3083", "all:Все инфекции");
          // <text    name='Список инфекций'    uid='3083' inlist='l' cast='no' length='255'/><choiceBD name='Вакцинация от' uid='1083' ref='003A'
          FCardData->AddPair("3066", (new TJSONObject)->AddPair("5", "Временно выбыл"));
          // <choice  name='Причина'            uid='3066' required='1' inlist='l' default='0'><choice name='Причина' uid='1066' default='0'>    'Заболевание' ='0'/>    'Карантин' ='2'/> 'Отказ' value='3'/>  'Распоряжение администрации'  '4'/>
          FCardData->AddPair("3082", new TJSONNull);
          // <extedit name='Заболевание'        uid='3082' comment='search' required='1' inlist='l' linecount='3' showtype='1' length='255'>
          FCardData->AddPair("3110", (new TJSONObject)->AddPair("1", "+"));
          // <binary  name='Отвод от прививки'  uid='3110' inlist='e' default='check'/>
          FCardData->AddPair("3111", (new TJSONObject)->AddPair("0", "-"));
          // <binary  name='Отвод от пробы'     uid='3111' inlist='e'/>
          FVal = FCardData->ToString();
          otvData->AddPair(AUCode, TJSONObject::ParseJSONValue(FVal));
          // DDD->Add("6_1,"+PrintProcessMemoryInfo(GetCurrentProcessId())+","+AUCode+","+AUCode+","+FVal);
         }
        __finally
         {
          delete FCardData;
         }
       }
      if (CardMap && CardMap->size())
       {
        ACount = CardMap->size();
        // ----------------------
        FOtvMap.clear();
        UnicodeString Otv_Code, Otv_Val;
        TJSONMap::iterator FOtvFRC;
        for (itCardMap = CardMap->begin(); itCardMap != CardMap->end(); itCardMap++)
         {
          FCardData = itCardMap->second;
          /*
           <choice name='Отвод' uid='1043' ref='' required='yes' inlist='list' depend='' isedit='yes' is3D='yes' linecount='' default='0'>
           <date name='Начало действия' uid='105F' ref='' required='yes' inlist='list' depend='' isedit='yes' linecount='' is3D='yes' default=''/>
           <date name='Окончание действия' uid='1065' ref='' required='yes' inlist='list' depend='' isedit='yes' linecount='' is3D='yes' default=''>
           <choice name='Причина' uid='1066' ref='' required='yes' inlist='list_ext' depend='' isedit='yes' is3D='yes' linecount='' default='0'>
           */
          Otv_Code /* Priv_Code */ = (JSONToString(FCardData, "1043") + "_" + JSONToString(FCardData, "105F") + "_" +
            JSONToString(FCardData, "1065") + "_" + JSONToString(FCardData, "1066")).UpperCase();
          Otv_Val = "";
          FVal    = JSONToString(FCardData, "1083");
          if (FVal.ToIntDef(0) > 0)
           {
            TAnsiStrMap::iterator FRC = InfMap.find(FVal);
            if (FRC != InfMap.end())
             Otv_Val = FVal + ":" + FRC->second;
           }
          if (Otv_Val.Length())
           {
            FOtvFRC = FOtvMap.find(Otv_Code /* Priv_Code */);
            if (FOtvFRC == FOtvMap.end())
             {
              FCardData->AddPair("CODE", itCardMap->first);
              FCardData->AddPair("317C", AUCode);
              // <extedit name='Код пациента' uid='317C' ref='1000' depend='317C.317C' isedit='0' fieldtype='integer'/>
              PrepareValue(FCardData, "317C");
              ReplaceValue(FCardData, "305F", "105F");
              // <date name='Начало действия' uid='305F' required='1' inlist='l'/><date name='Начало действия' uid='105F'
              ReplaceValue(FCardData, "3065", "1065");
              // <date name='Окончание действия' uid='3065' inlist='l'/><date name='Окончание действия' uid='1065'
              ReplAndPrepValue(FCardData, "3043", "1043");
              // <choice name='Отвод' uid='3043' required='1' inlist='l' default='0'><choice name='Отвод' uid='1043'
              FVal = JSONToString(FCardData, "1083");
              // <text name='Список инфекций' uid='3083' inlist='l' cast='no' length='255'/><choiceBD name='Вакцинация от' uid='1083' ref='003A'
              DeleteJSONPair(FCardData, "1083");
              FCardData->AddPair("3083", Otv_Val);
              ReplAndPrepValue(FCardData, "3066", "1066");
              // <choice name='Причина' uid='3066' required='1' inlist='l' default='0'><choice name='Причина' uid='1066' default='0'>    'Заболевание' ='0'/>    'Карантин' ='2'/> 'Отказ' value='3'/>  'Распоряжение администрации'  '4'/>
              // []<binary name='Классифицированное по МКБ-10' uid='10A0'
              // <choiceTree name='Заболевание' uid='1082' ref='2000'
              // []<text name='Описание заболевания' uid='10A1'
              FVal = JSONToString(FCardData, "1082");
              // <extedit name='Заболевание' uid='3082' comment='search' required='1' inlist='l' linecount='3' showtype='1' length='255'>
              DeleteJSONPair(FCardData, "1082");
              if (FVal.ToIntDef(0) > 0)
               {
                FCardData->AddPair("3082", "##" + FVal);
                PrepareValue(FCardData, "3082");
               }
              FCardData->AddPair("3110", (new TJSONObject)->AddPair("1", "+"));
              // binary name='Отвод от прививки' uid='3110' inlist='e' default='check'/>
              FCardData->AddPair("3111", (new TJSONObject)->AddPair("0", "-"));
              // binary name='Отвод от пробы' uid='3111' inlist='e'/>
              FOtvMap[Otv_Code /* Priv_Code */] = FCardData;
             }
            else
             {
              Otv_Val /* Priv_Val */ = JSONToString(FOtvFRC->second, "3083") + "," + Otv_Val /* Priv_Val */ ;
              DeleteJSONPair(FOtvFRC->second, "3083");
              FOtvFRC->second->AddPair("3083", Otv_Val /* Priv_Val */);
             }
           }
         }
        for (itCardMap = FOtvMap.begin(); itCardMap != FOtvMap.end(); itCardMap++)
         {
          FInfList->Clear();
          FInfList->Delimiter     = ',';
          FInfList->DelimitedText = JSONToString(itCardMap->second, "3083");
          for (int i = 0; i < FInfList->Count; i++)
           FInfList->Strings[i] = GetLPartB(FInfList->Strings[i], ':');
          bool All = true;
          for (TAnsiStrMap::iterator i = InfMap.begin(); (i != InfMap.end()) && All; i++)
           All &= (FInfList->IndexOf(i->first) != -1);
          if (All)
           {
            DeleteJSONPair(itCardMap->second, "3083");
            itCardMap->second->AddPair("3083", "all:Все инфекции");
           }
          FVal = itCardMap->second->ToString();
          otvData->AddPair(JSONToString(itCardMap->second, "CODE"), TJSONObject::ParseJSONValue(FVal));
         }
        // ----------------------
        EraseOldUnitCard("1030", AUCode);
       }
     }
   }
  __finally
   {
    delete FInfList;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::ReplaceValue(TJSONObject * AData, UnicodeString AId1, UnicodeString AId2)
 {
  AData->AddPair(AId1, JSONCopy(AData, AId2));
  DeleteJSONPair(AData, AId2);
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::PrepareValue(TJSONObject * AData, UnicodeString AId)
 {
  TJSONValue * FVal = JSONCopy(AData, AId);
  if (FVal)
   {
    if (!FVal->Null)
     {
      FVal = (new TJSONObject)->AddPair(JSONToString(AData, AId), FVal);
      DeleteJSONPair(AData, AId);
      AData->AddPair(AId.UpperCase(), FVal);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::ReplAndPrepValue(TJSONObject * AData, UnicodeString AId1, UnicodeString AId2)
 {
  ReplaceValue(AData, AId1, AId2);
  PrepareValue(AData, AId1);
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::SetClData(TJSONObject * ACls, UnicodeString AId, UnicodeString ACode, __int64 ALPU,
  UnicodeString AOtdel)
 {
  if (AId.Trim().Length() && ACode.Trim().Length() && (ACode != "0"))
   {
    TJSONValue * FOldClData = GetOldRec(AId, ACode);
    if (FOldClData)
     {
      TJSONValue * FCls = GetJSONValue(ACls, AId);
      if (!FCls) // если нет справочника AId, то создаём его
       {
        FCls = new TJSONObject;
        ACls->AddPair(AId.UpperCase(), FCls);
       }
      if (FCls)
       { // ошибок нет, справочник существует
        if (!GetJSONValue((TJSONObject *)FCls, ACode))
         {
          FOldClData = JSONCopy((TJSONObject *)FOldClData); // TJSONObject::ParseJSONValue(FOldClData->ToString());
          PrepareClsData(AId, (TJSONObject *)FOldClData, ACode, ALPU, AOtdel);
          ((TJSONObject *)FCls)->AddPair(ACode.UpperCase(), FOldClData);
         }
       }
     }
   }
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TCard67Convertor::GetOldRec(UnicodeString AId, UnicodeString ACode)
 {
  TJSONObject * RC = NULL;
  try
   {
    TJSONMap * FFields;
    TJSONIdMap::iterator FRC = OldDataMap->find(AId.UpperCase());
    if (FRC != OldDataMap->end())
     {
      FFields = FRC->second;
      TJSONMap::iterator FFlRC = FFields->find(ACode.UpperCase());
      if (FFlRC != FFields->end())
       RC = FFlRC->second;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::EraseOldRec(UnicodeString AId, UnicodeString ACode)
 {
  try
   {
    TJSONMap * FFields;
    TJSONIdMap::iterator FRC = OldDataMap->find(AId.UpperCase());
    if (FRC != OldDataMap->end())
     {
      FFields = FRC->second;
      TJSONMap::iterator FFlRC = FFields->find(ACode.UpperCase());
      if (FFlRC != FFields->end())
       {
        delete FFlRC->second;
        FFields->erase(ACode.UpperCase());
       }
     }
   }
  __finally
   {
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
TJSONMap * __fastcall TCard67Convertor::GetOldUnitCard(UnicodeString AId, UnicodeString AUnitCode)
 {
  TJSONMap * RC = NULL;
  try
   {
    TJSONUnitIdMap::iterator CardFRC = FUnitCard->find(AId.UpperCase());
    if (CardFRC != FUnitCard->end())
     {
      TJSONIdMap::iterator FUnitCardFRC = CardFRC->second->find(AUnitCode.UpperCase());
      if (FUnitCardFRC != CardFRC->second->end())
       RC = FUnitCardFRC->second;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::EraseOldUnitCard(UnicodeString AId, UnicodeString AUnitCode)
 {
  TJSONMap * RC = NULL;
  try
   {
    TJSONUnitIdMap::iterator CardFRC = FUnitCard->find(AId.UpperCase());
    if (CardFRC != FUnitCard->end())
     {
      TJSONIdMap::iterator FUnitCardFRC = CardFRC->second->find(AUnitCode.UpperCase());
      if (FUnitCardFRC != CardFRC->second->end())
       {
        for (TJSONMap::iterator i = FUnitCardFRC->second->begin(); i != FUnitCardFRC->second->end(); i++)
         delete i->second;
        FUnitCardFRC->second->clear();
        delete FUnitCardFRC->second;
        CardFRC->second->erase(AUnitCode.UpperCase());
       }
     }
   }
  __finally
   {
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::PrepareClsData(UnicodeString AClId, TJSONObject * AClRec, UnicodeString ACode,
  __int64 ALPU, UnicodeString AOtdel)
 {
  UnicodeString FClId = AClId.UpperCase();
  TJSONObject * FCl = AClRec;
  if (!FCl)
   FCl = GetOldRec(AClId.UpperCase(), ACode);
  if (FCl)
   {
    if (FClId == "000E")
     { // Участки
      IntToJSON(FCl, "00F6", ALPU); // <choiceDB name="ЛПУ" uid="00F6" ref="00C7" required="1" isedit="1" />
      PrepareValue(FCl, "00F6");
      if (AOtdel.ToIntDef(0))
       {
        IntToJSON(FCl, "00F7", AOtdel.ToIntDef(0));
        // <choiceDB name="Отделение" uid="00F7" comment="empty" ref="000C" required="0" isedit="1" />
        PrepareValue(FCl, "00F7");
       }
      DeleteJSONPair(FCl, "00F8");
      // <choiceDB name="Участковый врач" uid="00F8" ref="00AA" required="0" isedit="1" linecount="2" />
      FCl->AddPair("00F8", new TJSONNull);
     }
    else if (FClId == "000C")
     { // Отделения
      IntToJSON(FCl, "00DB", ALPU); // <choiceDB name="ЛПУ" uid="00DB" ref="00C7" required="1" isedit="1" />
      PrepareValue(FCl, "00DB");
     }
    else if (FClId == "0001")
     { // Организации
      if (JSONToInt(FCl, "00AF"))
       { // установлен признак "Обслуживаемая"
        IntToJSON(FCl, "3196", ALPU); // <choiceDB name="ЛПУ" uid="3196" ref="00C7" required="0" isedit="1" />
        PrepareValue(FCl, "3196");
       }
      // <text name="Наименование" uid="0008" ref="no" required="1" isedit="1" length="100" />
      if (JSONToInt(FCl, "0002"))
       {
        TJSONObject * FCl0002 = GetOldRec("0002", JSONToString(FCl, "0002"));
        // <class name='Типы организаций' uid='0002' ref='no' required='yes' inlist='list'>
        if (FCl0002)
         {
          UnicodeString FOrgName = JSONToString(FCl0002, "0003") + " " + JSONToString(FCl, "0008");
          // <text name='Наименование' uid='0003' ref='no' required='yes' inlist='list' depend='no' cast='no' length='25'/>
          DeleteJSONPair(FCl, "0008");
          FCl->AddPair("0008", FOrgName.Trim());
         }
       }
      PrepareValue(FCl, "00A3");
      // <choiceTree name="Вид деятельности по ОКВЭД" uid="00A3" ref="00A1" required="1" dlgtitle="Вид деятельности" dformat="extcode_text" linecount="3" wherestr="" isedit="1" type="" />
      PrepareValue(FCl, "00AF");
      // <binary name="Обслуживаемая" uid="00AF" required="0" invname="Необслуживаемая" isedit="1" />
      PrepareValue(FCl, "00B1");
      // <binary name="Контролируемая" uid="00B1" required="0" invname="Неконтролируемая" isedit="1"></binary>
      // -<text name="Адрес" uid="0107" required="0" isedit="1" linecount="3" length="255" />
      // <text name="Телефон" uid="0009" ref="no" required="0" isedit="1" length="20" />
      // -<choiceDB name="Группа" uid="011A" ref="0117" required="0" isedit="1" />
      // -<choiceDB name="Подчинённость" uid="010A" ref="0109" required="0" isedit="1" />
      // RemoveJSONPair(FCl, "00F8");  // <choiceDB name="Участковый врач" uid="00F8" ref="00AA" required="0" isedit="1" linecount="2" />
      // FCl->AddPair("00F8", new TJSONNull);
     }
   }
 }
// ---------------------------------------------------------------------------
/*
 TJSONObject* __fastcall TCard67Convertor::GetOldCardRec(UnicodeString AId, UnicodeString AUnitCode, UnicodeString ACode)
 {
 TJSONObject* RC = NULL;
 try
 {
 TJSONMap *FUnitCard = GetOldUnitCard(AId, AUnitCode);
 if (FUnitCard)
 {
 TJSONMap::iterator FUnitCardRecFRC = FUnitCard->find(ACode.UpperCase());
 if (FUnitCardRecFRC != FUnitCard->end())
 RC = FUnitCardRecFRC->second;
 }
 }
 __finally
 {
 }
 return RC;
 }
 //---------------------------------------------------------------------------
 */
#include <windows.h>
#include <stdio.h>
#include <psapi.h>
// ---------------------------------------------------------------------------
void __fastcall TCard67Convertor::PrintMemoryInfo()
 {
  /* DWORD aProcesses[1024], cbNeeded, cProcesses;
   unsigned int i;

   if ( EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
   {
   // Calculate how many process identifiers were returned.

   cProcesses = cbNeeded / sizeof(DWORD);

   // Print the memory usage for each process

   for ( i = 0; i < cProcesses; i++ )
   {
   */ PrintProcessMemoryInfo(GetCurrentProcessId() /* aProcesses[i] */);
  /* }
   }
   */
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TCard67Convertor::PrintProcessMemoryInfo(DWORD processID)
 {
  UnicodeString RC = "";
  UnicodeString tmp = "";
  HANDLE hProcess;
  PROCESS_MEMORY_COUNTERS pmc;
  // Print the process identifier.
  // RC += tmp.sprintf( L"\nProcess ID: %u\n", processID );
  // Print information about the memory usage of the process.
  hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);
  if (hProcess)
   {
    if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
     {
      // RC += tmp.sprintf( L"\tPageFaultCount: %d\n",             pmc.PageFaultCount );
      // RC += tmp.sprintf( L"\tPeakWorkingSetSize: %d\n",         pmc.PeakWorkingSetSize );
      // RC += tmp.sprintf( L"\tWorkingSetSize: %d\n",             pmc.WorkingSetSize );
      RC += tmp.sprintf(L"%d", pmc.WorkingSetSize);
      // RC += tmp.sprintf( L"\tQuotaPeakPagedPoolUsage: %d\n",    pmc.QuotaPeakPagedPoolUsage );
      // RC += tmp.sprintf( L"\tQuotaPagedPoolUsage: %d\n",        pmc.QuotaPagedPoolUsage );
      // RC += tmp.sprintf( L"\tQuotaPeakNonPagedPoolUsage: %d\n", pmc.QuotaPeakNonPagedPoolUsage );
      // RC += tmp.sprintf( L"\tQuotaNonPagedPoolUsage: %d\n",     pmc.QuotaNonPagedPoolUsage );
      // RC += tmp.sprintf( L"\tPagefileUsage: %d\n",              pmc.PagefileUsage );
      // RC += tmp.sprintf( L"\tPeakPagefileUsage: %d\n",          pmc.PeakPagefileUsage );
      // ShowMessage(RC);
     }
    CloseHandle(hProcess);
   }
  else
   {
    /*
     LPWSTR message = NULL;
     DWORD  error = GetLastError();
     FormatMessageW(
     FORMAT_MESSAGE_ALLOCATE_BUFFER |
     FORMAT_MESSAGE_FROM_SYSTEM, // use windows internal message table
     0,       // 0 since source is internal message table
     error,    // this is the error code
     // Could just as well have been an error code from generic
     // Windows errors from GetLastError()
     0,       // auto-determine language to use
     (LPWSTR)&message,
     0,       // min size for buffer
     0 );
     if( message )
     {
     ShowMessage(UnicodeString(message));
     LocalFree(message);
     }
     else
     ShowMessage(IntToStr((int)error));
     */
   }
  return RC;
 }
// ---------------------------------------------------------------------------
