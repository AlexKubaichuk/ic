﻿// ---------------------------------------------------------------------------
#pragma hdrstop
#include "dsEIDataUnit.h"
#include "ExtUtils.h"
#include "msgdef.h"
#include "ConvertFrom67.h"
#include "JSONUtils.h"
#include "ImportCardOptUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsEIData * dsEIData;
// ---------------------------------------------------------------------------
__fastcall TdsEIData::TdsEIData(TComponent * Owner, TConnectDM * AClients) : TDataModule(Owner)
 {
  FClients = AClients;
  FLPUCode = 0;
  chLPU    = clUch = clOrg = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsEIData::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      // PrBar->Max = AVal;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      // Caption = FSaveCaption+"  ["+AMsg+"]";
      // PrBar->Position = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      // Caption = FSaveCaption;
      break;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsEIData::Import67Data(TJSONObject * AData, UnicodeString & ALastCode)
 {
  UnicodeString FImpMsg = "";
  if (AData)
   {
    TJSONObject * FPtData = (TJSONObject *)GetJSONValue(AData, "pt");
    if (chLPU)
     {
      DeleteJSONPair(FPtData, "00F1");
      FPtData->AddPair("00F1", NewObjPair(IntToStr(FLPUCode), IntToStr(FLPUCode)));
     }
    if (clUch || chLPU)
     {
      DeleteJSONPair(FPtData, "00B3");
      // FPtData->AddPair("00B3", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("00B3", NewObjPair("3", "Импорт карт (синхронизация)"));
     }
    if (clOrg)
     {
      DeleteJSONPair(FPtData, "0025"); // Организация
      // FPtData->AddPair("0025", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("0025", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "0026"); // Ур1
      // FPtData->AddPair("0026", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("0026", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "0027"); // Ур2
      // FPtData->AddPair("0027", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("0027", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "0028"); // Ур3
      // FPtData->AddPair("0028", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("0028", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "0112"); // Период обучения
      // FPtData->AddPair("0112", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("0112", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "011B"); // Группа организаций
      // FPtData->AddPair("011B", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("011B", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "0111"); // Должность/профессия
      // FPtData->AddPair("0111", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("0111", NewObjPair("3", "Импорт карт (синхронизация)"));
      DeleteJSONPair(FPtData, "00F0"); // Обслуживаемая организация (своя)
      // FPtData->AddPair("00F0", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
      // FPtData->AddPair("00F0", NewObjPair("3", "Импорт карт (синхронизация)"));
     }
   }
  FClients->EIData->ImportData("1000", AData, ALastCode, FImpMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsEIData::ImportData(TTagNode * ADataDefNode, UnicodeString & ALastCode, TkabProgress AProgress)
 {
  bool FInProgress = true;
  try
   {
    // ShowMessage(IntToStr(FLPUCode));
    if (!FLPUCode)
     {
      _MSG_ERRA("В настройках программы не заданы параметры ЛПУ.", "Ошибка");
     }
    else
     {
      TOpenDialog * OpenDlg = new TOpenDialog(this);
      OpenDlg->Filter = "Карты ПК УИ-ЛПУ (*.zxml)|*.zxml";
      OpenDlg->Options << ofHideReadOnly << ofFileMustExist << ofEnableSizing;
      try
       {
        if (OpenDlg->Execute())
         {
          TTagNode * FImpData = new TTagNode;
          TImportCardOptForm * ImpOptDlg = new TImportCardOptForm(this);
          try
           {
            ImpOptDlg->MsgLab->Caption = OpenDlg->FileName;
            if (ImpOptDlg->ShowModal() == mrOk)
             {
              chLPU = ImpOptDlg->ChangeLPUChB->Checked;
              clUch = ImpOptDlg->ClearUchChB->Checked;
              clOrg = ImpOptDlg->ClearOrgChB->Checked;
              if (AProgress)
               AProgress(TkabProgressType::InitCommon, 1000, "Загрузка данных ...");
              FImpData->OnProgressChange = AProgress;
              FImpData->LoadFromZIPXMLFile(OpenDlg->FileName);
              FImpData->OnProgressChange = NULL;
              if (AProgress)
               AProgress(TkabProgressType::InitCommon, 1000, "Подготовка данных ...");
              TTagNode * FContent = FImpData->GetChildByName("content");
              if (!FContent)
               {
                if (FImpData->GetChildByName("unit"))
                 { // карты 6.7
                  TCard67Convertor * F67Convertor = new TCard67Convertor(ADataDefNode);
                  try
                   {
                    if (FImpData->GetTagByUID("1076"))
                     FImpData->GetTagByUID("1076")->DeleteChild();
                    if (FImpData->GetTagByUID("10AA"))
                     FImpData->GetTagByUID("10AA")->DeleteChild();
                    if (FImpData->GetTagByUID("1084"))
                     FImpData->GetTagByUID("1084")->DeleteChild();
                    F67Convertor->Convert(FImpData, FLPUCode, Import67Data, AProgress, ALastCode);
                    // FContent = FImpData->GetChildByName("content");
                   }
                  __finally
                   {
                    delete F67Convertor;
                   }
                 }
               }
              else
               {
                int FCommCount = FContent->Count;
                if (AProgress)
                 AProgress(TkabProgressType::InitCommon, FCommCount * 1000, "Приём данных ...");
                int FCurCount = 0;
                TTagNode * itNode = FContent->GetFirstChild();
                // FContent->SaveToXMLFile("c:\\ffff.xml");
                // UnicodeString FRecId = "";
                UnicodeString FImpMsg;
                while (itNode && FInProgress)
                 {
                  Application->ProcessMessages();
                  FImpMsg = "";
                  // ShowMessage(itNode->AV["PCDATA"].SubString(30,500));
                  TJSONObject * FImportData = (TJSONObject *)TJSONObject::ParseJSONValue(itNode->AV["PCDATA"]);
                  if (FImportData)
                   {
                    TJSONObject * FPtData = (TJSONObject *)GetJSONValue(FImportData, "pt");
                    TJSONObject * FSyncData = (TJSONObject *)GetJSONValue(FPtData, "330E");
                    if (!FSyncData)
                     FPtData->AddPair("330E", NewObjPair("3", "Импорт карт (синхронизация)"));
                    if (chLPU)
                     {
                      DeleteJSONPair(FPtData, "00F1");
                      FPtData->AddPair("00F1", NewObjPair(IntToStr(FLPUCode), IntToStr(FLPUCode)));
                     }
                    if (clUch || chLPU)
                     {
                      DeleteJSONPair(FPtData, "00B3");
                      // FPtData->AddPair("00B3", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("00B3", NewObjPair("3", "Импорт карт (синхронизация)"));
                     }
                    if (clOrg)
                     {
                      DeleteJSONPair(FPtData, "0025"); // Организация
                      // FPtData->AddPair("0025", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("0025", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "0026"); // Ур1
                      // FPtData->AddPair("0026", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("0026", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "0027"); // Ур2
                      // FPtData->AddPair("0027", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("0027", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "0028"); // Ур3
                      // FPtData->AddPair("0028", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("0028", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "0112"); // Период обучения
                      // FPtData->AddPair("0112", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("0112", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "011B"); // Группа организаций
                      // FPtData->AddPair("011B", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("011B", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "0111"); // Должность/профессия
                      // FPtData->AddPair("0111", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("0111", NewObjPair("3", "Импорт карт (синхронизация)"));
                      DeleteJSONPair(FPtData, "00F0"); // Обслуживаемая организация (своя)
                      // FPtData->AddPair("00F0", NewObjPair(IntToStr(ALPUCode), IntToStr(ALPUCode)));
                      // FPtData->AddPair("00F0", NewObjPair("3", "Импорт карт (синхронизация)"));
                     }
                   }
                  FClients->EIData->ImportData("1000", FImportData, ALastCode, FImpMsg);
                  FCurCount++ ;
                  if (AProgress)
                   FInProgress = AProgress(TkabProgressType::CommInc, (FCurCount * 1000) / FCommCount,
                    "Приём карт: " + IntToStr(FCurCount) + "/" + IntToStr(FCommCount));
                  Application->ProcessMessages();
                  itNode = itNode->GetNext();
                 }
               }
             }
           }
          __finally
           {
            delete ImpOptDlg;
            delete FImpData;
           }
         }
       }
      __finally
       {
        delete OpenDlg;
       }
     }
   }
  __finally
   {
    if (FLPUCode && AProgress)
     AProgress(TkabProgressType::CommComplite, 0, "");
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsEIData::ExportData(TkabProgressType AStage, TTagNode * ADataDefNode, TkabCustomDataSource * ASource,
  __int64 ARecCode, TkabProgress AProgress)
 {
  bool RC = false;
  try
   {
    if (AStage == TkabProgressType::InitStage)
     {
      SaveDlg->FileName = Date().FormatString("Карты_УИ_yyyy.mm.dd");
      if (SaveDlg->Execute())
       {
        Application->ProcessMessages();
        if (IEData)
         delete IEData;
        IEData = new TTagNode;
        Application->ProcessMessages();
        IEData->AsXML = FClients->EIData->GetExportDataHeader();
        Application->ProcessMessages();
        IEContent = IEData->GetChildByName("content");
        Application->ProcessMessages();
        FCount = ARecCode;
        FCur   = 0;
        Application->ProcessMessages();
        if (IEContent)
         {
          AProgress(TkabProgressType::InitCommon, ARecCode, "Передача данных ...");
          Application->ProcessMessages();
          RC = true;
         }
        Application->ProcessMessages();
       }
     }
    else if (AStage == TkabProgressType::StageInc)
     {
      FCur++ ;
      AProgress(TkabProgressType::StageInc, 100, IntToStr(FCur) + "/" + IntToStr(FCount));
      IEContent->AddChild("ur")->AV["PCDATA"] = FClients->EIData->GetPatDataById(IntToStr(ARecCode));
      Sleep(10);
      Application->ProcessMessages();
     }
    else if (AStage == TkabProgressType::StageComplite)
     {
      AProgress(TkabProgressType::CommComplite, 0, "");
      IEData->SaveToZIPXMLFile(SaveDlg->FileName);
      delete IEData;
      IEData = NULL;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
