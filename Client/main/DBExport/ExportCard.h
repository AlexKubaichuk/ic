//---------------------------------------------------------------------------

#ifndef ExportCardH
#define ExportCardH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//#include "TB2Dock.hpp"
//#include "TB2Item.hpp"
//#include "TB2Toolbar.hpp"
#include <ActnList.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxControls.hpp"
#include "cxGraphics.hpp"
#include "dxStatusBar.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "DMF.h"
#include "cxCustomData.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxDropDownEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxLabel.hpp"
#include "cxProgressBar.hpp"
#include <Web.Win.Sockets.hpp>
#include <ScktComp.hpp>
#include "cxClasses.hpp"
#include "cxLookAndFeels.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
//---------------------------------------------------------------------------
typedef map<int,TcxTreeListNode*> TListNodeMap;
class TExportCardForm : public TForm
{
__published:	// IDE-managed Components
        TSaveDialog *SaveDlg;
        TPanel *BtnPanel;
        TdxStatusBar *StatusBar;
        TcxTreeList *PatList;
        TcxTreeListColumn *cxTreeList1cxTreeListColumn1;
        TcxTreeListColumn *cxTreeList1cxTreeListColumn2;
        TPanel *Panel2;
        TPanel *Panel3;
        TPanel *Panel1;
        TcxButton *SelFromTemplateBtn;
        TcxButton *SelFromFilterBtn;
        TcxComboBox *FilterCB;
        TcxButton *FilterListBtn;
        TcxButton *DelBtn;
        TcxCheckBox *CardExpChB;
        TcxCheckBox *UnitExpChB;
        TcxCheckBox *ClassExpChB;
        TcxLabel *FilterLab;
        TcxStyleRepository *cxStyleRepository1;
        TcxStyle *DisabledStyle;
        TcxStyle *EnabledStyle;
        TTimer *PrTimer;
        TcxButton *ExportBtn;
        TcxButton *StopBtn;
        TcxButton *CloseBtn;
        TLabel *HintLb;
        TLabel *CurProgressLb;
        TcxProgressBar *CurPB;
        TLabel *TotalProgressLb;
        TcxProgressBar *TotalPB;
        TTimer *CmdRetTimer;
        void __fastcall SelFromTemplateBtnClick(TObject *Sender);
        void __fastcall CardExpChBClick(TObject *Sender);
        void __fastcall UnitExpChBClick(TObject *Sender);
        void __fastcall ExportBtnClick(TObject *Sender);
        void __fastcall SelFromFilterBtnClick(TObject *Sender);
        void __fastcall FilterCBPropertiesChange(TObject *Sender);
        void __fastcall FilterListBtnClick(TObject *Sender);
        void __fastcall PatListClick(TObject *Sender);
        void __fastcall DelBtnClick(TObject *Sender);
        void __fastcall ClassExpChBClick(TObject *Sender);
        void __fastcall CloseBtnClick(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall PrTimerTimer(TObject *Sender);
        void __fastcall StopBtnClick(TObject *Sender);
        void __fastcall cmdClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall cmdClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall CmdRetTimerTimer(TObject *Sender);
private:	// User declarations
        TListNodeMap CodeList;
        bool         InProgress;
        UnicodeString   FCmdRet;

        bool       __fastcall cmdServerConnect();
        void       __fastcall InitProgressCtrls();
        void       __fastcall SetCtrlEnabled(bool AVal);
        UnicodeString __fastcall SendConvCommand(UnicodeString ACmd);
        void       __fastcall SendStartCommand(UnicodeString ACmd, TStringList *AParams);

        bool __fastcall IsConvProgress();

        void __fastcall SetEnabled(bool AEnabled);
        void __fastcall FProgressInit(int AMax, UnicodeString AMsg);
        void __fastcall FProgressChange(int APercent, UnicodeString AMsg);
        void __fastcall FProgressComplite(UnicodeString AMsg);

public:		// User declarations
        __fastcall TExportCardForm(TComponent* Owner);
//        __property bool InProgress = {read=FInProgress, write=SetInProgress};
};
//---------------------------------------------------------------------------
extern PACKAGE TExportCardForm *ExportCardForm;
//---------------------------------------------------------------------------
#endif

