﻿//---------------------------------------------------------------------------

#ifndef dsEIDataUnitH
#define dsEIDataUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "XMLContainer.h"
#include "dsConnectUnit.h"
//---------------------------------------------------------------------------
class TdsEIData : public TDataModule
{
__published:	// IDE-managed Components
 TSaveDialog *SaveDlg;

private:
  TTagNode *IEData;
  TTagNode *IEContent;
  TConnectDM *FClients;
  int FCount, FCur;
  __int64 FLPUCode;
  bool chLPU, clUch, clOrg;

//  void __fastcall PreloadData();
  void __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  void __fastcall Import67Data(TJSONObject *AData, UnicodeString &ALastCode);
public:
  __fastcall TdsEIData(TComponent* Owner, TConnectDM *AClients);

  void __fastcall ImportData(TTagNode *ADataDefNode, UnicodeString &ALastCode, TkabProgress AProgress);
  bool __fastcall ExportData(TkabProgressType AStage, TTagNode *ADataDefNode, TkabCustomDataSource* ASource, __int64 ARecCode, TkabProgress AProgress);
  __property __int64 LPUCode = {read=FLPUCode, write=FLPUCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsEIData *dsEIData;
//---------------------------------------------------------------------------
#endif
