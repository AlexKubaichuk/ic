object ImportCardOptForm: TImportCardOptForm
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = #1048#1084#1087#1086#1088#1090' '#1082#1072#1088#1090
  ClientHeight = 164
  ClientWidth = 366
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MsgLab: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 5
    Width = 360
    Height = 13
    Margins.Top = 5
    Align = alTop
    AutoSize = False
    EllipsisPosition = epPathEllipsis
    ParentShowHint = False
    ShowHint = True
    WordWrap = True
    ExplicitWidth = 304
  end
  object ChangeLPUChB: TcxCheckBox
    AlignWithMargins = True
    Left = 3
    Top = 26
    Margins.Top = 5
    Margins.Bottom = 5
    Align = alTop
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1082#1086#1076' '#1051#1055#1059' '#1085#1072' "'#1089#1074#1086#1081'"'
    Properties.DisplayGrayed = 'NULL'
    Properties.ImmediatePost = True
    Properties.OnChange = ChangeLPUChBPropertiesChange
    State = cbsGrayed
    TabOrder = 0
    Width = 360
  end
  object ClearUchChB: TcxCheckBox
    AlignWithMargins = True
    Left = 3
    Top = 57
    Margins.Top = 5
    Margins.Bottom = 5
    Align = alTop
    Caption = #1057#1073#1088#1086#1089#1080#1090#1100' '#1091#1095#1072#1089#1090#1086#1082
    Properties.DisplayGrayed = 'NULL'
    Properties.ImmediatePost = True
    State = cbsGrayed
    TabOrder = 1
    Width = 360
  end
  object ClearOrgChB: TcxCheckBox
    AlignWithMargins = True
    Left = 3
    Top = 88
    Margins.Top = 5
    Margins.Bottom = 5
    Align = alTop
    Caption = #1057#1073#1088#1086#1089#1080#1090#1100' '#1086#1088#1075#1072#1085#1080#1079#1086#1074#1072#1085#1085#1086#1089#1090#1100
    Properties.DisplayGrayed = 'NULL'
    Properties.ImmediatePost = True
    State = cbsGrayed
    TabOrder = 2
    Width = 360
  end
  object ApplyBtn: TButton
    Left = 202
    Top = 128
    Width = 75
    Height = 25
    Caption = #1048#1084#1087#1086#1088#1090
    TabOrder = 3
    OnClick = ApplyBtnClick
  end
  object CancelBtn: TButton
    Left = 283
    Top = 128
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 4
  end
end
