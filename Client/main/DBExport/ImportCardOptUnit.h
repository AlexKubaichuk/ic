//---------------------------------------------------------------------------

#ifndef ImportCardOptUnitH
#define ImportCardOptUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
class TImportCardOptForm : public TForm
{
__published:	// IDE-managed Components
 TcxCheckBox *ChangeLPUChB;
 TcxCheckBox *ClearUchChB;
 TcxCheckBox *ClearOrgChB;
 TLabel *MsgLab;
 TButton *ApplyBtn;
 TButton *CancelBtn;
 void __fastcall ApplyBtnClick(TObject *Sender);
 void __fastcall ChangeLPUChBPropertiesChange(TObject *Sender);
private:	// User declarations
 TcxCheckBoxState FSaveState;
public:		// User declarations
 __fastcall TImportCardOptForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TImportCardOptForm *ImportCardOptForm;
//---------------------------------------------------------------------------
#endif

