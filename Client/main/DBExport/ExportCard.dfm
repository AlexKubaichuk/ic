object ExportCardForm: TExportCardForm
  Left = 868
  Top = 206
  BorderStyle = bsSizeToolWin
  Caption = #1055#1077#1088#1077#1076#1072#1095#1072' '#1080#1084#1084#1091#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1093' '#1082#1072#1088#1090
  ClientHeight = 408
  ClientWidth = 647
  Color = clBtnFace
  Constraints.MinHeight = 430
  Constraints.MinWidth = 646
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 268
    Width = 647
    Height = 120
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    ExplicitTop = 251
    DesignSize = (
      647
      120)
    object HintLb: TLabel
      Left = 8
      Top = 7
      Width = 31
      Height = 13
      Caption = 'HintLb'
    end
    object CurProgressLb: TLabel
      Left = 8
      Top = 27
      Width = 69
      Height = 13
      Caption = 'CurProgressLb'
    end
    object TotalProgressLb: TLabel
      Left = 8
      Top = 71
      Width = 77
      Height = 13
      Caption = 'TotalProgressLb'
    end
    object CurPB: TcxProgressBar
      Left = 10
      Top = 44
      Anchors = [akLeft, akTop, akRight]
      ParentColor = False
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = 16425091
      Properties.EndColor = 9841671
      Properties.OverloadValue = 50.000000000000000000
      Style.BorderStyle = ebsUltraFlat
      Style.Color = clWhite
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.BorderStyle = ebsFlat
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.BorderStyle = ebsFlat
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 0
      Width = 508
    end
    object TotalPB: TcxProgressBar
      Left = 10
      Top = 87
      Anchors = [akLeft, akTop, akRight]
      ParentColor = False
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = 16425091
      Properties.EndColor = 9841671
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebsUltraFlat
      Style.Color = clWhite
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.BorderStyle = ebsFlat
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.BorderStyle = ebsFlat
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 1
      Width = 508
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 388
    Width = 647
    Height = 20
    Panels = <>
    PaintStyle = stpsXP
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ExplicitTop = 371
  end
  object PatList: TcxTreeList
    Left = 0
    Top = 0
    Width = 405
    Height = 268
    Align = alClient
    Bands = <
      item
      end>
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    Navigator.Buttons.CustomButtons = <>
    OptionsData.Editing = False
    OptionsData.Deleting = False
    OptionsSelection.CellSelect = False
    OptionsSelection.HideFocusRect = False
    OptionsView.Buttons = False
    OptionsView.ExtPaintStyle = True
    OptionsView.GridLineColor = clBtnFace
    OptionsView.GridLines = tlglBoth
    OptionsView.ShowRoot = False
    Styles.Content = DisabledStyle
    TabOrder = 2
    OnClick = PatListClick
    ExplicitHeight = 251
    object cxTreeList1cxTreeListColumn1: TcxTreeListColumn
      Caption.AlignHorz = taCenter
      Caption.Text = #1060'.'#1048'.'#1054'.'
      DataBinding.ValueType = 'String'
      Options.Editing = False
      Width = 274
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxTreeList1cxTreeListColumn2: TcxTreeListColumn
      Caption.AlignHorz = taCenter
      Caption.Text = #1044#1072#1090#1072' '#1088#1086#1078#1076'.'
      DataBinding.ValueType = 'String'
      Options.Editing = False
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object Panel2: TPanel
    Left = 405
    Top = 0
    Width = 242
    Height = 268
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitHeight = 251
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 242
      Height = 89
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object CardExpChB: TcxCheckBox
        Left = 6
        Top = 11
        Caption = #1048#1084#1084#1091#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1077' '#1082#1072#1088#1090#1099
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        OnClick = CardExpChBClick
        Width = 181
      end
      object UnitExpChB: TcxCheckBox
        Left = 6
        Top = 35
        Caption = #1057#1087#1080#1089#1086#1082' '#1087#1072#1094#1080#1077#1085#1090#1086#1074
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 1
        OnClick = UnitExpChBClick
        Width = 181
      end
      object ClassExpChB: TcxCheckBox
        Left = 6
        Top = 60
        Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080' '#1088#1077#1075#1080#1089#1090#1088#1072#1090#1091#1088#1099
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 2
        OnClick = ClassExpChBClick
        Width = 181
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 89
      Width = 242
      Height = 179
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitHeight = 162
      DesignSize = (
        242
        179)
      object SelFromTemplateBtn: TcxButton
        Left = 6
        Top = 77
        Width = 232
        Height = 25
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1094#1080#1077#1085#1090#1086#1074' '#1087#1086' '#1096#1072#1073#1083#1086#1085#1091
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007171
          710071717100717171006D6D6D006D6D6D006D6D6D006D6D6D006D6D6D00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF006D6D6D00B165
          4D008C5036008C5036008C5036008C5036006C554C006A635F00717171006D6D
          6D006D6D6D006D6D6D006D6D6D0071717100717171006D6D6D00E0664200DC51
          3100F9635000FD725C00F1684E00D7744500F1684E00D24C2700355938003559
          38002F982D00489037002F982D0025662500355938006D6D6D00D5885500F963
          5000FD7C6400FC8B6D00D5885500FFCF9C00F36D5300FD6A5700917930004DB3
          4D006AA9660063CA630052C252003CB73C00239023006D6D6D00FF00FF00E066
          4200FD846900EB805900FFCF9C00FFCF9C00EF765600FD6B580080AB670080B3
          8000FFEFDF004DB34D0075D275004FC14F00489037006D6D6D00FF00FF00FF00
          FF00B1654D004C214F002E3457007F4C6E00B1654D00917930007CD77C008FE0
          8F00FFF7E800FF00FF005BC55B0044A64400FF00FF00FF00FF00FF00FF001717
          170005070E000D296800143AA000102F9500081D6C00545454006AA966004273
          99001579BA00247DB600377D57004D4D4D00FF00FF00FF00FF003F3F3F001A1A
          1A00102C5B001A4DB3001C56BC001B51B700102F950054545400699AAE002C92
          F1003399FF003399FF002C92F1002C586F0084848400FF00FF00121212002828
          28000F2D93002774DA002671D7002671D7001E5AC0005A6064003F95C30040A6
          FF0040A6FF0040A6FF003DA2FF002385C6006D6D6D00FF00FF002C2C2C003636
          36002C586F003191F9003399FF003694F700246AD0005A606400479FD0004BB1
          FF004BB1FF004DB3FF0049AFFF002D92E60066666600FF00FF006D6D6D004A4A
          4A003F3F3F0046464600143FA4002060C600135F88007E7E7E0057A9D7004DB3
          F2004DB3F20055BBFF0051B7FF0043A8ED006D6D6D00FF00FF00FF00FF003838
          3800666666008F8F8F00A4A4A4004D4D4D0046464600FF00FF0063ABD200247D
          B60057A9D70063ABD200479FD0001372A200699AAE00FF00FF00FF00FF00FF00
          FF006D6D6D00605E5E00605E5E0066666600FF00FF00FF00FF00FF00FF003A8B
          B70084C0E400A3D0EA00479FD000699AAE00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF003A8BB7003A8BB7003A8BB700FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 0
        OnClick = SelFromTemplateBtnClick
      end
      object SelFromFilterBtn: TcxButton
        Left = 6
        Top = 50
        Width = 232
        Height = 25
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1094#1080#1077#1085#1090#1086#1074' '#1087#1086' '#1092#1080#1083#1100#1090#1088#1091
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0084000000FFFFFF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00840000008400000084000000FFFFFF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0084000000840000008400000084000000FFFFFF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00840000008400000084000000FFFFFF008400000084000000FFFFFF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
          00008400000084000000FFFFFF00FF00FF00FF00FF008400000084000000FFFF
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0084000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF0084000000FFFF
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
          0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0084000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0084000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000FF00FF0000000000FF00FF000000000000000000FF00FF00FF00FF00FF00
          FF00000000000000000084000000FFFFFF00FF00FF00FF00FF00FF00FF000000
          0000FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
          0000FF00FF00FF00FF00FF00FF0084000000FFFFFF00FF00FF00FF00FF000000
          00000000000000000000FF00FF000000000000000000FF00FF00FF00FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF0084000000FF00FF00FF00FF000000
          0000FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00
          FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 1
        OnClick = SelFromFilterBtnClick
      end
      object FilterCB: TcxComboBox
        Left = 6
        Top = 23
        Enabled = False
        Properties.DropDownListStyle = lsFixedList
        Properties.OnChange = FilterCBPropertiesChange
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 2
        Width = 208
      end
      object FilterListBtn: TcxButton
        Left = 214
        Top = 22
        Width = 23
        Height = 23
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0084000000FFFFFF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00840000008400000084000000FFFFFF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0084000000840000008400000084000000FFFFFF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00840000008400000084000000FFFFFF008400000084000000FFFFFF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
          00008400000084000000FFFFFF00FF00FF00FF00FF008400000084000000FFFF
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0084000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF0084000000FFFF
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008400
          0000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0084000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0084000000FFFFFF00FF00FF00FF00FF00FF00FF00FF00FF000000
          0000FF00FF0000000000FF00FF000000000000000000FF00FF00FF00FF00FF00
          FF00000000000000000084000000FFFFFF00FF00FF00FF00FF00FF00FF000000
          0000FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
          0000FF00FF00FF00FF00FF00FF0084000000FFFFFF00FF00FF00FF00FF000000
          00000000000000000000FF00FF000000000000000000FF00FF00FF00FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF0084000000FF00FF00FF00FF000000
          0000FF00FF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF0000000000FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00
          FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        OptionsImage.Layout = blGlyphTop
        TabOrder = 3
        OnClick = FilterListBtnClick
      end
      object DelBtn: TcxButton
        Left = 6
        Top = 104
        Width = 232
        Height = 25
        Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1072#1094#1080#1077#1085#1090#1072' '#1080#1079' '#1089#1087#1080#1089#1082#1072
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        TabOrder = 4
        OnClick = DelBtnClick
      end
      object FilterLab: TcxLabel
        Left = 6
        Top = 2
        Caption = #1060#1080#1083#1100#1090#1088':'
        Enabled = False
        FocusControl = FilterCB
        Style.BorderStyle = ebsNone
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        Style.Shadow = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
      end
      object ExportBtn: TcxButton
        Left = 2
        Top = 148
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = #1055#1077#1088#1077#1076#1072#1090#1100
        Enabled = False
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        TabOrder = 6
        OnClick = ExportBtnClick
      end
      object StopBtn: TcxButton
        Left = 83
        Top = 148
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = #1055#1088#1077#1088#1074#1072#1090#1100
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        TabOrder = 7
        OnClick = StopBtnClick
      end
      object CloseBtn: TcxButton
        Left = 164
        Top = 148
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        ModalResult = 2
        TabOrder = 8
        OnClick = CloseBtnClick
      end
    end
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.zxml'
    Filter = #1060#1072#1081#1083#1099' '#1076#1072#1085#1085#1099#1093'|*.zxml'
    Left = 47
    Top = 36
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 29
    Top = 98
    PixelsPerInch = 96
    object DisabledStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clBtnFace
      TextColor = clMedGray
    end
    object EnabledStyle: TcxStyle
      AssignedValues = [svColor]
      Color = clWindow
    end
  end
  object PrTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = PrTimerTimer
    Left = 155
    Top = 38
  end
  object CmdRetTimer: TTimer
    Interval = 60000
    OnTimer = CmdRetTimerTimer
    Left = 232
    Top = 71
  end
end
