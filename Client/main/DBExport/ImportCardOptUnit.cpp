// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "ImportCardOptUnit.h"
#include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
TImportCardOptForm * ImportCardOptForm;
// ---------------------------------------------------------------------------
__fastcall TImportCardOptForm::TImportCardOptForm(TComponent * Owner) : TForm(Owner)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TImportCardOptForm::ApplyBtnClick(TObject * Sender)
 {
  if (ChangeLPUChB->State != cbsGrayed && ClearUchChB->State != cbsGrayed && ClearOrgChB->State != cbsGrayed)
   ModalResult = mrOk;
  else
   _MSG_ERR("������� �������� ��� ���� ����������.", "������ �����");
 }
// ---------------------------------------------------------------------------
void __fastcall TImportCardOptForm::ChangeLPUChBPropertiesChange(TObject * Sender)
 {
  if (ChangeLPUChB->Checked)
   {
    ClearUchChB->Enabled = false;
    ClearUchChB->Checked = true;
    FSaveState = ClearUchChB->State;
   }
  else
   {
    ClearUchChB->Enabled = true;
    ClearUchChB->State   = FSaveState;
   }
 }
// ---------------------------------------------------------------------------
