﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "MainUnit.h"
#include "ExtUtils.h"
// #include "LoginUnit.h"
#include "LauncherUpdater.h"
// #include "PatGrChange.h"
#include "PatOrgGrChange.h"
#include "PatPrivProbGrInsert.h"
#include "PatOtvodGrInsert.h"
#include "PatSchemeGrInsert.h"
#include "msgdef.h"
#include "ProgressUnit.h"
// #include "SetDBPath.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxGraphics"
#pragma link "dxBar"
#pragma link "ICSAboutDialog"
#pragma resource "*.dfm"
TMainForm * MainForm;
// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent * Owner) : TForm(Owner)
 {
  FC = 0;
#ifdef GUARDANT
  randomize();
  xTime           = new TTimer(this);
  xTime->OnTimer  = xxTimer;
  xTime->Enabled  = false;
  xTime->Interval = 10000;
  trBuf1          = new char[128];
  trBuf2          = new char[128];
  trBuf3          = new char[128];
  memset(trBuf1, 0, 128);
  memset(trBuf2, 0, 128);
  memset(trBuf3, 0, 128);
#endif
  // icDxLocale();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::UpdateMenuItems()
 {
#ifdef GUARDANT
  int nRet;
  unsigned char xVer;
  long XNN = 61212541813;
  XNN = XNN + 0;
  XNN -= 62598345691;
  nRet  = DataM->GKey.Read(1, 1, &xVer);
  xmVer = icsGetFileVersion(Application->ExeName);
#endif
  actGetClasses->Visible = true;
  // RegTB->Visible = (actPatList->Visible || actGetClasses->Visible);
  if (DM->RT["IC"])
   {
    if (DM->RT["ICReg"])
     {
      if (DM->RT["ICRegUList"])
       {
        // DM->RT["ICRegUListEdit"];   = 0; //<binary name='Редактирование' uid='0036'>
        // DM->RT["ICCard"];          =  0; //<binary name='Доступ к имм. карте' uid='002B'>
        // DM->RT["ICCardEdit"];      = 0; //<binary name='Редактирование' uid='0039'>
        actPatList->Visible = true;
        actPatList->Enabled = true;
       }
      if (DM->RT["ICRegCList"])
       {
        // DM->CliOpt->Vals["ICRegCListEdit"].AsBoolDef(false);   = 0; //<binary name='Редактирование' uid='0038'>
        actGetClasses->Visible = true;
        actGetClasses->Enabled = true;
       }
     }
    if (DM->RT["ICPlan"])
     {
      // DM->CliOpt->Vals["ICPlanPlaning"].AsBoolDef(false);   = 0; //<binary name='Выполнение планирования' uid='003A'>
      actPlanWork->Visible = true;
      actPlanWork->Enabled = true;
     }
   }
  if (DM->RT["Doc"])
   {
    if (DM->RT["DocSpec"])
     {
      // DM->CliOpt->Vals["DocSpecEdit"].AsBoolDef(false);      = 0; //<binary name='Редактирование' uid='003D'>
      actSpecListOpen->Visible = true;
      actSpecListOpen->Enabled = true;
     }
    if (DM->RT["DocDoc"])
     {
      // DM->CliOpt->Vals["DocDocEdit"].AsBoolDef(false);       = 0; //<binary name='Редактирование' uid='003E'>
      actDocBankOpen->Visible = true;
      actDocBankOpen->Enabled = true;
     }
    if (DM->RT["DocFilter"])
     {
      // DM->CliOpt->Vals["DocFilterEdit"].AsBoolDef(false);     = 0; //<binary name='Редактирование' uid='003F'>
      actDocChoice->Visible = true;
      actDocChoice->Enabled = true;
     }
    // if (DM->CliOpt->Vals[""].AsBoolDef(false))
    // {
    // actGetSubOrgList->Visible = true;
    // actGetSubOrgList->Enabled = true;
    // }
   }
  if (DM->RT["VS"])
   {
    actShowStore->Visible = true;
    actShowStore->Enabled = true;
   }
  UnicodeString FPreviewPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\DocPreview.exe");
  if (FileExists(FPreviewPath) && !DM->CliOpt->Vals["extview"].AsStringDef("").Length())
   DM->CliOpt->Vals["extview"] = FPreviewPath;
  DM->DocComp->ExternalView = DM->CliOpt->Vals["extview"].AsStringDef("");
 }
// ---------------------------------------------------------------------
bool __fastcall TMainForm::CheckConnect()
 {
  bool RC = false;
  TdsProgressForm * PrForm = new TdsProgressForm(this);
  try
   {
    try
     {
      // Caption = Application->Title;
      // DM->MainWnd = (TObject*)MainForm;
      if (DM->Login())
       {
        PrForm->Show();
        Application->ProcessMessages();
        DM->LoadRegDef();
        Application->ProcessMessages();
#ifdef GUARDANT
        xTime->Enabled = true;
#endif
        DM->PreloadData();
        UpdateMenuItems();
        RC = true;
       }
     }
    catch (EAccessViolation & E)
     {
      Application->MessageBox(E.Message.c_str(), L"Ошибка", MB_OK);
      Caption = Caption + "  (Выборка \"Все дети\")";
     }
   }
  __finally
   {
    PrForm->Close();
    delete PrForm;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject * Sender)
 {
  Application->ProcessMessages();
  TUpdateForm * FLUpd = new TUpdateForm(this);
  try
   {
    try
     {
      // FLUpd->CheckUpdate(DM->ConnectOpt->Vals["ic_server"].AsStringDef("localhost")+":"+DM->ConnectOpt->Vals["ic_port"].AsIntDef(5100));
      CheckConnect();
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERR("Ошибка аутентификации пользователя.\nСистемное сообщение:\n" + E.Message, "Ошибка входа в систему.");
      Application->Terminate();
     }
   }
  __finally
   {
    delete FLUpd;
   }
  // DM->MainWnd = (TObject*)MainForm;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  Action = caFree;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPatListExecute(TObject * Sender)
 {
  DM->RegComp->UnitListOpenType = TdsRegListType::ltSearchFilter;
  if (DM->CliOpt->Vals["RegUListType"].AsString == "filter")
   DM->RegComp->UnitListOpenType = TdsRegListType::ltFilter;
  else if (DM->CliOpt->Vals["RegUListType"].AsString == "list")
   DM->RegComp->UnitListOpenType = TdsRegListType::ltFull;
  DM->RegComp->ShowUnitList();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actGetClassesExecute(TObject * Sender)
 {
  /* DM->RegComp->UnitListOpenType = TdsRegListType::ltSearchFilter;
   if (DM->CliOpt->Vals["RegCListType"].AsString == "filter")
   DM->RegComp->UnitListOpenType = TdsRegListType::ltFilter;
   else if (DM->CliOpt->Vals["RegCListType"].AsString == "list")
   DM->RegComp->UnitListOpenType = TdsRegListType::ltFull;
   */
  // DM->RegComp->ShowUnitList();
  DM->RegComp->ClassShowExpanded = "0000,000B,0016";
  DM->RegComp->ShowClasses(ctList, "0", -1);
  DM->RegComp->ClassShowExpanded = "";
  // DM->FClassData->ClearHash("");
#ifdef IMM_SETTING
#ifdef _USE_NOM
  Function FDefQuest("ClearHash");
  DM->ICSDocEx->DM->ICSNom.Exec(FDefQuest);
#endif
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPlanWorkExecute(TObject * Sender)
 {
#ifdef IMM_PLAN
  DM->PlanComp->ShowPlan();
  /* bool dbgPlan = false;
   dbgPlan = DM->AppOpt->Vals["DebugPlan"].AsBoolDef(false);
   TPlanDialog *Dlg = NULL;
   try
   {
   Dlg = new TPlanDialog();
   Dlg->DB = DM->IMM_DB;

   Dlg->VakSchXML  = DM->xmlVacDef;
   Dlg->ProbSchXML = DM->xmlTestDef;
   Dlg->RegXML     = DM->xmlRegDef;
   Dlg->CardXML    = DM->xmlImmCardDef;
   Dlg->DebugPrintFileName = "";//(dbgPlan)?"c:\\trace.txt":"";
   Dlg->CustomPlanDefFormat = DM->AppOpt->Vals["DefaultPlanFormat"].AsString.Trim();
   if (!Dlg->CustomPlanDefFormat.Length())
   Dlg->CustomPlanDefFormat = "standard";
   Dlg->OnGetCustomPlanFormats   = FGetCustomPlanFormats;
   Dlg->OnEditCustomPlanFormats  = FEditCustomPlanFormats;
   Dlg->OnPrintCustomPlan        = FPrintCustomPlan;
   Dlg->OnEditPlanSettings       = FEditPlanSettings;
   Dlg->OnShowUnitCard           = FShowUnitCard;
   Dlg->OnCheckPlanItem          = FCheckPlanItem;
   Dlg->OnAddPlanUnit            = FAddPlanUnit;
   Dlg->OnAddPlanItem            = FAddPlanItem;
   Dlg->Execute();
   }
   __finally
   {
   //     Dlg->CustomPlanDefFormat = DM->AppOpt->Vals["DefaultPlanFormat"].AsString.Trim();
   DM->AppOpt->Vals["DefaultPlanFormat"] = Dlg->CustomPlanDefFormat;
   if (Dlg) delete Dlg;
   } */
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actSpecListOpenExecute(TObject * Sender)
 {
#ifdef _DOC_EX_SPEC
  DM->DocComp->SpecList();
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actDocBankOpenExecute(TObject * Sender)
 {
#ifdef _DOC_EX_BANKDOC
#ifndef IMM_DOCUMENT
  DM->DocComp->DocList();
#else
  DM->DocComp->DocList(true);
#endif
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actDocChoiceExecute(TObject * Sender)
 {
#ifdef _DOC_EX_FILTER_LIST
  DM->DocComp->FilterList();
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actConnectSettingExecute(TObject * Sender)
 {
  DM->ConnectOptDlg->Execute();
  UpdateMenuItems();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actClientSettingExecute(TObject * Sender)
 {
  DM->CliOptDlg->Execute();
  UpdateMenuItems();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPgmSettingExecute(TObject * Sender)
 {
  DM->AppOptDlg->Execute();
  UpdateMenuItems();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actHelpExecute(TObject * Sender)
 {
  Application->HelpCommand(HELP_FINDER, 4001);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actAboutExecute(TObject * Sender)
 {
  AboutDlg->FileName = icsPrePath(ParamStr(0));
  AboutDlg->VIXML->Text = DM->Clients->App->GetProductVersion();
  AboutDlg->Execute();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actShowStoreExecute(TObject * Sender)
 {
  DM->StoreComp->ShowStore();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::SettingBIClick(TObject * Sender)
 {
  SettingBI->DropDown();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPatRegDataGrChangeExecute(TObject * Sender)
 {
  TPatOrgGrChangeForm * Dlg = new TPatOrgGrChangeForm(this);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPatPrivTestGrInsertExecute(TObject * Sender)
 {
  TPatPrivProbGrInsertForm * Dlg = new TPatPrivProbGrInsertForm(this);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPatOtvodGrInsertExecute(TObject * Sender)
 {
  TPatOtvodGrInsertForm * Dlg = new TPatOtvodGrInsertForm(this);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPatSchemeGrChangeExecute(TObject * Sender)
 {
  TPatSchemeGrInsertForm * Dlg = new TPatSchemeGrInsertForm(this);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
