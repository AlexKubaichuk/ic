object DBSelectUnit: TDBSelectUnit
  Left = 0
  Top = 0
  ActiveControl = DBNameCB
  Caption = #1042#1099#1073#1086#1088' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
  ClientHeight = 120
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    AlignWithMargins = True
    Left = 5
    Top = 5
    Width = 357
    Height = 26
    Margins.Left = 5
    Margins.Top = 5
    Margins.Right = 5
    Margins.Bottom = 5
    Align = alTop
    Caption = 
      #1042#1099#1073#1077#1088#1080#1090#1077' '#1041#1044', '#1089' '#1082#1086#1090#1086#1088#1086#1081' '#1078#1077#1083#1072#1077#1090#1077' '#1088#1072#1073#1086#1090#1072#1090#1100' '#1080' '#1085#1072#1078#1084#1080#1090#1077' '#1082#1085#1086#1087#1082#1091' "'#1055#1088#1080#1084#1077#1085 +
      #1080#1090#1100'"'
    WordWrap = True
    ExplicitWidth = 320
  end
  object Panel1: TPanel
    Left = 0
    Top = 79
    Width = 367
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      367
      41)
    object ApplyBtn: TButton
      Left = 206
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ModalResult = 1
      TabOrder = 0
      OnClick = ApplyBtnClick
    end
    object CancelBtn: TButton
      Left = 287
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object DBNameCB: TcxComboBox
    AlignWithMargins = True
    Left = 5
    Top = 41
    Margins.Left = 5
    Margins.Top = 5
    Margins.Right = 5
    Margins.Bottom = 5
    Align = alTop
    Properties.DropDownListStyle = lsFixedList
    Properties.OnChange = DBNameCBPropertiesChange
    TabOrder = 1
    OnKeyDown = DBNameCBKeyDown
    Width = 357
  end
end
