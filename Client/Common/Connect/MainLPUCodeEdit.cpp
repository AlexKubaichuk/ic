﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainLPUCodeEdit.h"
#include "msgdef.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinOffice2010Blue"
#pragma link "dxSkinValentine"
#pragma link "dxSkinXmas2008Blue"
#pragma resource "*.dfm"
TMainLPUEditForm *MainLPUEditForm;
//---------------------------------------------------------------------------
__fastcall TMainLPUEditForm::TMainLPUEditForm(TComponent* Owner, TdsGetValByIdEvent AOnGetValById)
        : TForm(Owner)
{
  FOnGetValById = AOnGetValById;
  FLPUCode = -1;
  LoadLPUData((TStringList*)LPUCB->Properties->Items, "Select code as ps1, R00CA as ps2 From class_00C7 Order by R00CA");
//  Caption = FMT(icsDocExDocOrgEditSetCaption);
//     Caption = UnicodeString((AIsEdit)?cFMT(icsDocExDocOrgEditEditCaption) : cFMT(icsDocExDocOrgEditInsertCaption));
}
//---------------------------------------------------------------------------
bool __fastcall TMainLPUEditForm::CheckInput()
{
  bool RC = false;
   try
    {
      if (!(LPUCB->ItemIndex >= 0))
       {
         _MSG_ERR("Необходимо выбрать ЛПУ.","Ошибка");
         ActiveControl = LPUCB;
       }
      RC = true;
    }
   __finally
    {
    }
   return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainLPUEditForm::OkBtnClick(TObject *Sender)
{
  if (!CheckInput()) return;

  if (LPUCB->ItemIndex >= 0)
    FLPUCode = (__int64)LPUCB->Properties->Items->Objects[LPUCB->ItemIndex];
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TMainLPUEditForm::LoadLPUData(TStringList *AItems, UnicodeString ASQL)
{
  if (FOnGetValById)
   {
     AItems->Clear();
     TJSONObject *RetData = NULL;
     TJSONPairEnumerator *itPair;
     try
      {
        FOnGetValById("reg.s2."+ASQL, "0", RetData);
        itPair = RetData->GetEnumerator();
        while (itPair->MoveNext())
          AItems->AddObject(((TJSONString*)itPair->Current->JsonValue)->Value(),(TObject*)((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(0));
      }
     __finally
      {
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainLPUEditForm::FSetLPUCode(__int64 AValue)
{
  if (AValue >= 0)
   {
     int idx = LPUCB->Properties->Items->IndexOfObject((TObject*)AValue);
     if (idx != -1)
      {
        FLPUCode = AValue;
        LPUCB->ItemIndex = idx;
      }
   }
}
//---------------------------------------------------------------------------

