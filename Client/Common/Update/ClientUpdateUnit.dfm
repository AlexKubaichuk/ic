object ClientUpdateForm: TClientUpdateForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  BorderWidth = 5
  ClientHeight = 122
  ClientWidth = 495
  Color = clActiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MsgLab: TLabel
    AlignWithMargins = True
    Left = 86
    Top = 38
    Width = 406
    Height = 69
    Margins.Bottom = 15
    Align = alClient
    Alignment = taCenter
    AutoSize = False
    Caption = #1086#1073#1085#1086#1074#1083#1077#1085#1080#1077
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold, fsItalic]
    GlowSize = 1
    ParentColor = False
    ParentFont = False
    ShowAccelChar = False
    Layout = tlCenter
    WordWrap = True
    ExplicitLeft = -2
    ExplicitTop = -2
    ExplicitWidth = 489
    ExplicitHeight = 104
  end
  object Image1: TImage
    AlignWithMargins = True
    Left = 3
    Top = 38
    Width = 77
    Height = 81
    Align = alLeft
    ParentShowHint = False
    Picture.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000040
      000000400806000000AA6971DE0000000467414D410000B18F0BFC6105000006
      C149444154785EE59ADB6F93651CC7175140894A0C17468D375EFA07E8852626
      92A05E6062349EE2891941228A9288075060BA033004B6B195015D771E6DD7B5
      DDBA13DB3A36D6AD87B53B74ED7A5AB7B503B60101651BDBC5D7DFF3120CC24F
      61876E6D77F1C9DE7EF6BE17DFDFFBBCCFF3FCDE36C168342E6B58B99C60653C
      D2D5D50D8BD5863BFDBF3EC42B4EA70B3A7DD55DE105778978C3E7F341A952B3
      E105AC8C1742A1104A4BCBFE33BC8095F1C0E8E8188A8A4BFE37BC8095F1805C
      9E77CFF00256C63A798A7C0C0D0F2FCF0228287C6020785FE105AC8C55F20B0A
      E1A0F59EFBDF7FC1CA58A4A4F434CC66EBACC20B58196BA8D5E514BC79D6E105
      AC8C256A6AEB5059553DA7F00256C60A268B199AF28A398717B032DAE9080E20
      B3A11676977B5EE105AC8C66B4BD76ACCF48C38BA9FB70CEE75D5E05385CAFC7
      BAC4F7F1C03B1BB057AF9A7778012BA311459B11CF27FF881752F6E085DDDF61
      ECCFABCBA700DB328FE2B93D3BF0EAA92CACDBBE091556F3828417B0325AD054
      1A9051AEC5295307120B1558F3D547D8A52E5CB0F00256460375F50DC82F5123
      55A7C7FE9A5AE476B4E3C39424A8F486F82F804C7602DFEFFC098D1D16A42A4A
      B057598E348D069ACA9A050D2F60E552524A7BFAC4CFB7202D2D1D5BB67F8B62
      DAE5155554A154C7BFD39B2FAC5C2A4437B775EBD7282E53E1E7DD7B91992583
      B1A51D7A7D0D946562BFCF5F371F58B9141414156153E216E41514E3977DC978
      E7FD8FA9BBB343A5AA40CE0939F2E405F15B005D652556AD7A042FBDFC0AF626
      A560CFBE14B4B659507FC688DC5C39E4110A2F60E562A2C82FC0E38FAFC5AAD5
      6B90909080A79E7E16C7B28F236DFF21E4C84EE15404C30B58B95888B7371BDF
      7C0B2B563E8C87A8008F3EB6562AC286D7DEC0EFC9A938715211D1F002562E06
      DD3D3D7497D391F8C5977865FDEB7862DD9358B1E2413CFDCCB3F8E4D3441414
      96463CBC80959126181CC4E12399387C341BD9B293D8FD6B1236BEF53656AE5E
      8D77DFFB807A7CDDA28417B0329278BD5E64E71CC7FE838771F050061A9B5A71
      808E0F1C3A827D49BF2323337BD1C20B581929C2E1F3D29D4F4E39800A9D015A
      5D3576FEB08B26BB93D25A5FB448C3FE76581909C2E1B034A36765E5A25C5B8D
      A6B32628955A1C3A92454B5F322D77F7F74DCE42C3CA8566746C94EEB20C2728
      A42C371FCD2D1D282955A3A85885DAFA26C8724E2E4978012B17922B57AEE0B4
      5205457E210C8606385D03F82DE52076EF49424B6B3BE479915DE7EF052B178A
      BFAE4FA0AEAE01E97F6440A9D6A1DF3B44935E1B0A8B4FE36CAB09F905F7FEF6
      36D2B072BE4C4F4F636262024DC6B3D27A5EA6D2422326390AAEA6637B670F4A
      E898BB76B161E57C9998984443439314B8B6AE096E4F10E515D5F86CD3661C3B
      968BB2D391E9ECE6022BE7C3F5EBD7E172F5D3E4D648139E02EEFE006C74C775
      3402747A032D835951135EC0CAB932393985DEDE3E98DA2D683D67967AF9A262
      258E66E4C0EE70A242AB8FAAF00256CE85A9A929F4F5B9D1DDE3449BC9220576
      F67991929A8E6DDFEC805C511475E105AC9C2D53376ED050F7A0D7D92715C0E5
      F2C1E30948C3DE6CB1D3A3208FCAF00256CE86E9E91BF0787D70B9BD540037DD
      F57E0C0D8DA0A9B10516AB9D26C1C6A80D2F60E5FD3243CB5D203040E13DE8F7
      F8A5BF21DAEF9B4C56381CBD3036B7447578012BEF07B1D6878643F0F983F007
      066914F83172FE22AC3607BABAFBD04ABB3CEEBA688395B733353571571031EC
      C3E161040606313814A202047171741CDDBDBDE873B9D1D131FB9FAA2C15ACBC
      9DE9E9191ACE5D18BF7419625F3F3E3E4EC33E200DFD5078442AC2D8F8656904
      F8FD7E38BA7A6226BC809577A2D168693D2F95028B094FDC71272D79FE8100C6
      2E5DA215C04B7380176E777F4C8517B092A3B6AE1E86EA7A787D0184472E5263
      E3C5C885F3521144F83EDAFD71D7453BACE498999941676797B4AEDB1D3DB872
      F52A060683F4DCF74845E1AE890558C9313139494D8C5A6A64C42360A779C1E7
      F7D1CC7F2166C30B58C961EB74405F59832F366F854C968B769AE93B1DB3FB55
      6634C24A0E8BD521BDC0B8F9AD4D3A94AA0AEAF88CD0D276973B3F5660E59DD8
      E8D92FD7E8A99B334813A170F50DCD686D6BA7474245AE2E7EE7008FC727EDE7
      AB0C75C4CDF0B718A725D06AB35101627714B0F216C1C121EAEDA9AF6F3E27BD
      D9E1CE8975FE39106F722E5DBE8C1BD4DA8E8C5CA09D9E98E9BBA9B737A326CA
      3BBAF9F0AF0F6DED26EAE77BA99921BA9C68A7BB7F869EF5DBCF8937EE12566B
      27B5B366349F3D87A6E656D4D4362CAF02087CBE01DAF1D960B6D9A5D1C09D13
      2FB052D04F0D8E8B9A9BE1506879164070EDDA355CA53D3FF7BF788195CB0763
      C2DFE5714B825175E3310000000049454E44AE426082}
    Proportional = True
    ShowHint = False
    Stretch = True
    Transparent = True
  end
  object Label1: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 489
    Height = 17
    Margins.Bottom = 15
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = '"'#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1080#1084#1084#1091#1085#1080#1079#1072#1094#1080#1077#1081' - '#1051#1055#1059'"'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold, fsItalic]
    GlowSize = 1
    ParentColor = False
    ParentFont = False
    ShowAccelChar = False
    Transparent = True
    Layout = tlCenter
    WordWrap = True
  end
  object ConnectOptDlg: TAppOptDialog
    FormHeight = 500
    Options = ConnectOpt
    Left = 17
    Top = 9
  end
  object ConnectOpt: TAppOptions
    OnLoadXML = ConnectOptLoadXML
    DataProvider = ConnectOptXMLData
    Left = 89
    Top = 9
  end
  object ConnectOptXMLData: TAppOptXMLProv
    Left = 178
    Top = 9
  end
end
