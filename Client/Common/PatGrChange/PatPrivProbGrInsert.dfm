inherited PatPrivProbGrInsertForm: TPatPrivProbGrInsertForm
  Caption = 'PatPrivProbGrInsertForm'
  ExplicitWidth = 739
  ExplicitHeight = 628
  PixelsPerInch = 96
  TextHeight = 13
  inherited BtnPanel: TPanel
    TabOrder = 1
    inherited CancelBtn: TcxButton
      TabOrder = 2
    end
    inherited PrevBtn: TcxButton
      TabOrder = 0
    end
    inherited NextBtn: TcxButton
      TabOrder = 1
    end
    inherited PrBar: TcxProgressBar
      TabStop = False
    end
  end
  inherited StepPC: TcxPageControl
    TabOrder = 0
    Properties.ActivePage = Step3TS
    inherited Step3TS: TcxTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 709
      ExplicitHeight = 409
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 709
        Height = 409
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 2
          Width = 705
          Height = 71
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 388
            Top = 11
            Width = 94
            Height = 13
            Caption = #1044#1072#1090#1072' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 390
            Top = 39
            Width = 34
            Height = 13
            Caption = #1057#1077#1088#1080#1103':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object TestRB: TcxRadioButton
            Left = 15
            Top = 44
            Width = 80
            Height = 17
            Caption = #1055#1088#1086#1073#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = MIBPRBClick
          end
          object MIBPRB: TcxRadioButton
            Left = 15
            Top = 9
            Width = 64
            Height = 17
            Caption = #1052#1048#1041#1055':'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TabStop = True
            OnClick = MIBPRBClick
          end
          object TestCB: TcxComboBox
            Left = 74
            Top = 41
            Enabled = False
            ParentFont = False
            Properties.DropDownListStyle = lsEditFixedList
            Properties.OnChange = TestCBPropertiesChange
            Style.Color = clInfoBk
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'MS Sans Serif'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 3
            Width = 250
          end
          object MIBPCB: TcxComboBox
            Left = 74
            Top = 7
            ParentFont = False
            Properties.DropDownListStyle = lsEditFixedList
            Properties.OnChange = MIBPCBPropertiesChange
            Style.Color = clInfoBk
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'MS Sans Serif'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 1
            Width = 250
          end
          object ExecuteDE: TcxDateEdit
            Left = 494
            Top = 5
            ParentFont = False
            Properties.ButtonGlyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              20000000000000040000000000000000000000000000000000000000000A0000
              0010000000110000001100000011000000120000001200000012000000120000
              0012000000120000001300000013000000120000000C0000000381594CC2B47C
              69FFB37B69FFB37B68FFB37A68FFB37A68FFB27A68FFB27A68FFB37968FFB279
              68FFB27967FFB27867FFB17867FFB17866FF7F5649C30000000BB77F6EFFFBF8
              F5FFF8EEE9FFF8EEE9FFF7EFE8FFF7EEE8FFF7EEE8FFF7EEE8FFF7EDE7FFF7ED
              E6FFF6EDE6FFF6ECE6FFF6ECE6FFF6ECE5FFB47B69FF00000011B98472FFFBF8
              F6FFBF998AFFEBDAD3FFBE9788FFEBDAD3FFBD9586FFEBDAD3FFBC9484FFEBDA
              D3FF5D6DDDFFE4DDE1FF5A69DCFFF7EDE7FFB77F6EFF00000011BC8978FFFCFA
              F8FFEBDDD5FFECDCD5FFEBDDD5FFECDCD5FFEBDDD5FFECDCD5FFEBDDD5FFECDC
              D5FFE5DFE3FFE5DFE2FFE5DEE2FFF8EEE9FFB98472FF00000010C08E7DFFFCFA
              F9FFC6A294FFEDDED6FFC4A092FFEDDED6FFC29E8EFFEDDED6FFC19B8CFFEDDE
              D6FF6577E1FFE5E0E4FF6272E0FFF8F1EBFFBC8977FF00000010C39482FFFCFA
              FAFFEDDFD9FFEDDFD8FFEDDFD9FFEDDFD8FFEDDFD9FFEDDFD8FFEDDFD9FFEDDF
              D8FFE6E2E6FFE6E2E6FFE6E2E5FFF9F2EEFFC08E7CFF0000000FC79887FFFDFB
              FAFFCCAB9DFFEEE0DBFFCAA99BFFEEE0DBFFC9A799FFEEE0DBFFC8A496FFEEE0
              DBFF6D81E5FFE8E3E8FF6A7DE4FFFAF4F0FFC49381FF0000000EC99D8CFFFDFC
              FCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2DCFFEEE2
              DCFFE8E6EAFFE8E5EAFFE8E4E9FFFAF6F2FFC69886FF0000000DCDA190FFFEFC
              FCFFD0B1A3FFEFE3DFFFCFB0A2FFEFE3DFFFCFAFA0FFEFE3DFFFCDAD9FFFEFE3
              DFFF7388E8FFE9E6EBFF7186E7FFFBF7F5FFC99D8BFF0000000DCFA594FFFEFC
              FCFFFDF9F9FFFDF9F9FFFDF9F9FFFDFAF8FFFDF9F8FFFDFAF8FFFCF9F7FFFCF9
              F7FFFCF9F7FFFDF8F7FFFCF9F7FFFCF9F7FFCCA290FF0000000C4B53C3FF8D9E
              ECFF687CE3FF6678E2FF6476E1FF6172E0FF5F70DFFF5F70DFFF5D6CDEFF5B69
              DCFF5966DBFF5664DAFF5462D9FF616DDCFF3337AAFF0000000B4C55C4FF93A4
              EEFF6C80E6FF6A7EE4FF687BE4FF6678E2FF6375E1FF6375E1FF6172E0FF5E6F
              DEFF5C6CDDFF5A69DCFF5766DAFF6472DDFF3538ABFF0000000A4D56C6FF96A7
              EFFF95A6EFFF93A4EDFF90A2EDFF8F9FEDFF8B9BEBFF8B9BEBFF8898EAFF8595
              EAFF8291E7FF7F8DE7FF7D89E5FF7987E5FF3539ACFF000000093A4093C14D55
              C5FF4B53C3FF4A51C1FF484FBFFF464DBEFF444BBBFF444BBBFF4249B9FF4046
              B7FF3E44B4FF3C41B3FF3A3EB0FF393CAEFF282B80C200000006000000040000
              0006000000060000000600000007000000070000000700000007000000070000
              0007000000070000000800000008000000070000000500000001}
            Properties.SaveTime = False
            Properties.ShowOnlyValidDates = True
            Properties.ShowTime = False
            Properties.OnChange = ExecuteDEPropertiesChange
            Style.Color = clInfoBk
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'MS Sans Serif'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 4
            Width = 95
          end
          object SerED: TcxMaskEdit
            Left = 436
            Top = 36
            Properties.MaskKind = emkRegExpr
            Properties.EditMask = '.{20}'
            Properties.OnValidate = SerEDPropertiesValidate
            Style.TextStyle = []
            TabOrder = 5
            Width = 154
          end
        end
        object PatPrivTL: TcxTreeList
          Left = 2
          Top = 73
          Width = 705
          Height = 334
          Align = alClient
          Bands = <
            item
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          Navigator.Buttons.CustomButtons = <>
          OptionsBehavior.CellHints = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.HeaderHints = True
          OptionsData.Deleting = False
          OptionsView.CellEndEllipsis = True
          OptionsView.Buttons = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.ExtPaintStyle = True
          OptionsView.GridLineColor = clBtnFace
          OptionsView.GridLines = tlglBoth
          OptionsView.ShowRoot = False
          ParentFont = False
          TabOrder = 1
          OnClick = PatListClick
          OnEditing = PatPrivTLEditing
          object FamCol: TcxTreeListColumn
            Caption.AlignHorz = taCenter
            Caption.Text = #1060#1072#1084#1080#1083#1080#1103
            DataBinding.ValueType = 'String'
            MinWidth = 90
            Options.Sizing = False
            Options.Editing = False
            Options.Sorting = False
            Width = 90
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object NameCol: TcxTreeListColumn
            Caption.AlignHorz = taCenter
            Caption.Text = #1048#1084#1103
            DataBinding.ValueType = 'String'
            MinWidth = 70
            Options.Sizing = False
            Options.Editing = False
            Options.Sorting = False
            Width = 70
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object OtchCol: TcxTreeListColumn
            Caption.Text = #1054#1090#1095#1077#1089#1090#1074#1086
            DataBinding.ValueType = 'String'
            MinWidth = 80
            Options.Sizing = False
            Options.Editing = False
            Options.Sorting = False
            Width = 80
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object BirthDayCol: TcxTreeListColumn
            PropertiesClassName = 'TcxLabelProperties'
            Properties.Alignment.Horz = taCenter
            Caption.AlignHorz = taCenter
            Caption.Text = #1044#1072#1090#1072' '#1088#1086#1078#1076'.'
            DataBinding.ValueType = 'DateTime'
            MinWidth = 75
            Options.Sizing = False
            Options.Editing = False
            Options.Sorting = False
            Width = 75
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object VacTypeCol: TcxTreeListColumn
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsEditFixedList
            Properties.OnChange = ReacValColPropertiesChange
            Properties.OnEditValueChanged = VacTypeColPropertiesEditValueChanged
            Caption.Text = #1042#1080#1076
            DataBinding.ValueType = 'String'
            MinWidth = 30
            Options.Sizing = False
            Options.Sorting = False
            Width = 122
            Position.ColIndex = 4
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
            OnGetEditingProperties = VacTypeColGetEditingProperties
          end
          object ReacTypeCol: TcxTreeListColumn
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsEditFixedList
            Properties.OnChange = ReacValColPropertiesChange
            Properties.OnEditValueChanged = ReacTypeColPropertiesEditValueChanged
            Caption.Text = #1056#1077#1072#1082#1094#1080#1103
            DataBinding.ValueType = 'String'
            MinWidth = 150
            Options.Sizing = False
            Options.Sorting = False
            Width = 150
            Position.ColIndex = 5
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
            OnGetEditingProperties = ReacTypeColGetEditingProperties
          end
          object ReacValCol: TcxTreeListColumn
            PropertiesClassName = 'TcxMaskEditProperties'
            Properties.MaskKind = emkRegExprEx
            Properties.EditMask = '\d{1,4}([,]\d)?\d{2}'
            Properties.ValidationOptions = [evoShowErrorIcon]
            Properties.OnChange = ReacValColPropertiesChange
            Properties.OnValidate = cxTreeList1Column1PropertiesValidate
            Caption.Text = #1047#1085#1072#1095#1077#1085#1080#1077' '#1088#1077#1072#1082#1094#1080#1080
            DataBinding.ValueType = 'String'
            Options.Sizing = False
            Options.Sorting = False
            Width = 116
            Position.ColIndex = 6
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object ReacTypeValCol: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 7
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object SchValCol: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 8
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
      end
    end
    inherited ReadyTS: TcxTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 709
      ExplicitHeight = 409
      inherited ReadyPC: TcxPageControl
        Properties.ActivePage = CustomTS
        inherited CustomTS: TcxTabSheet
          ExplicitLeft = 4
          ExplicitTop = 4
          ExplicitWidth = 695
          ExplicitHeight = 395
          object Label2: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 689
            Height = 13
            Align = alTop
            Caption = #1055#1088#1080#1074#1080#1074#1082#1080'/'#1087#1088#1086#1073#1099', '#1082#1086#1090#1086#1088#1099#1077' '#1073#1091#1076#1091#1090' '#1076#1086#1073#1072#1074#1083#1077#1085#1099':'
            ExplicitWidth = 269
          end
          object ReadyTL: TcxTreeList
            Left = 0
            Top = 19
            Width = 695
            Height = 376
            Align = alClient
            Bands = <
              item
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            LookAndFeel.Kind = lfStandard
            LookAndFeel.NativeStyle = True
            Navigator.Buttons.CustomButtons = <>
            OptionsBehavior.CellHints = True
            OptionsBehavior.HeaderHints = True
            OptionsData.Editing = False
            OptionsData.Deleting = False
            OptionsView.CellEndEllipsis = True
            OptionsView.Buttons = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.ExtPaintStyle = True
            OptionsView.GridLineColor = clBtnFace
            OptionsView.GridLines = tlglBoth
            OptionsView.ShowRoot = False
            ParentFont = False
            TabOrder = 0
            OnClick = PatListClick
            OnEditing = PatPrivTLEditing
            object cxTreeListColumn7: TcxTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = #1060#1072#1084#1080#1083#1080#1103
              DataBinding.ValueType = 'String'
              MinWidth = 90
              Options.Sizing = False
              Options.Editing = False
              Options.Sorting = False
              Width = 90
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn8: TcxTreeListColumn
              Caption.AlignHorz = taCenter
              Caption.Text = #1048#1084#1103
              DataBinding.ValueType = 'String'
              MinWidth = 70
              Options.Sizing = False
              Options.Editing = False
              Options.Sorting = False
              Width = 70
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn9: TcxTreeListColumn
              Caption.Text = #1054#1090#1095#1077#1089#1090#1074#1086
              DataBinding.ValueType = 'String'
              MinWidth = 80
              Options.Sizing = False
              Options.Editing = False
              Options.Sorting = False
              Width = 80
              Position.ColIndex = 2
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn10: TcxTreeListColumn
              PropertiesClassName = 'TcxLabelProperties'
              Properties.Alignment.Horz = taCenter
              Caption.AlignHorz = taCenter
              Caption.Text = #1044#1072#1090#1072' '#1088#1086#1078#1076'.'
              DataBinding.ValueType = 'DateTime'
              MinWidth = 75
              Options.Sizing = False
              Options.Editing = False
              Options.Sorting = False
              Width = 75
              Position.ColIndex = 3
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn11: TcxTreeListColumn
              Caption.Text = #1042#1080#1076
              DataBinding.ValueType = 'String'
              MinWidth = 30
              Width = 122
              Position.ColIndex = 4
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn12: TcxTreeListColumn
              Caption.Text = #1056#1077#1072#1082#1094#1080#1103
              DataBinding.ValueType = 'String'
              MinWidth = 150
              Width = 150
              Position.ColIndex = 5
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn13: TcxTreeListColumn
              PropertiesClassName = 'TcxMaskEditProperties'
              Properties.MaskKind = emkRegExpr
              Properties.EditMask = '\d{1,5}[\.,]\d{1,5}'
              Properties.ValidationOptions = [evoShowErrorIcon]
              Properties.OnValidate = cxTreeList1Column1PropertiesValidate
              Caption.Text = #1047#1085#1072#1095#1077#1085#1080#1077' '#1088#1077#1072#1082#1094#1080#1080
              DataBinding.ValueType = 'String'
              Width = 116
              Position.ColIndex = 6
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxTreeListColumn14: TcxTreeListColumn
              Visible = False
              DataBinding.ValueType = 'String'
              Position.ColIndex = 7
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
        end
        inherited BaseTS: TcxTabSheet
          ExplicitLeft = 4
          ExplicitTop = 4
          ExplicitWidth = 695
          ExplicitHeight = 395
        end
      end
    end
    inherited RunTS: TcxTabSheet
      inherited RunMsgLab: TcxLabel
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Panel2: TPanel
    inherited Panel4: TPanel
      inherited StepNameLab: TcxLabel
        Style.IsFontAssigned = True
        AnchorY = 15
      end
      inherited StepCommentLab: TcxLabel
        Style.IsFontAssigned = True
      end
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 8
    PixelsPerInch = 96
    object dStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clBtnFace
      TextColor = clBtnShadow
    end
  end
end
