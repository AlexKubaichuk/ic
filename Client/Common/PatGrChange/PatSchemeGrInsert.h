﻿//---------------------------------------------------------------------------

#ifndef PatSchemeGrInsertH
#define PatSchemeGrInsertH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxRadioGroup.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxCore.hpp"
#include "dxGDIPlusClasses.hpp"
#include "PatGrChange.h"
#include "InsertCardItem.h"
#include "cxCheckListBox.hpp"
#include "cxButtonEdit.hpp"
//---------------------------------------------------------------------------
class TPatSchemeGrInsertForm : public TPatGrChangeForm
{
__published:	// IDE-managed Components
 TcxStyleRepository *cxStyleRepository1;
 TcxStyle *dStyle;
 TPanel *RegDataPanel;
 TcxTreeList *SchSettingTV;
 TcxTreeListColumn *NKSettingTVCol;
 TcxTreeListColumn *InfNameSchCol;
 TcxTreeListColumn *VacSchCol;
 TcxTreeListColumn *TestSchCol;
 TcxTreeListColumn *SchSettingTVColumn1;
 TcxTreeListColumn *SchSettingTVColumn2;
 void __fastcall VacSchColPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall TestSchColPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 bool __fastcall GetSchData(int ACol, UnicodeString AAttr, UnicodeString & AVal, UnicodeString & AStrVal);
 TTagNode * __fastcall GetSettingSelNode(TcxTreeList * ASrc);




private:	// User declarations

  TAnsiStrMap InfList;
  TTagNode *FSettingNode;
  UnicodeString FSetSchData;

  void __fastcall Preload();
  bool __fastcall CheckKeyDown(TObject * Sender, WORD & Key, TShiftState Shift);

  void __fastcall FSchButtonClick(int AButtonIndex, int ACol, UnicodeString AAttr);
  void __fastcall LoadSchSettingExecute();
  void __fastcall SetSchData(int ACol, UnicodeString AAttr, UnicodeString AVal, UnicodeString AStrVal);

  TInsertCardItem *FInsOtvodDataExecutor;

  bool __fastcall CheckInput();
  void __fastcall FillReadyTS();
  void __fastcall BeforeExecute();
  void __fastcall Execute(int AIdx, __int64 AUCode);
  void __fastcall AfterExecute();
  void __fastcall StepChange();
  void __fastcall ButtonEnabled();

public:		// User declarations
 __fastcall TPatSchemeGrInsertForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPatSchemeGrInsertForm *PatSchemeGrInsertForm;
//---------------------------------------------------------------------------
#endif
