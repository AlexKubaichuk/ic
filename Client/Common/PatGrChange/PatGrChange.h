﻿//---------------------------------------------------------------------------

#ifndef PatGrChangeH
#define PatGrChangeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxGDIPlusClasses.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsRegTemplate.h"
#include "dsRegTemplateData.h"
#include "msgdef.h"
#include "DMUnit.h"
//---------------------------------------------------------------------------
typedef map<int,TcxTreeListNode*> TListNodeMap;
//---------------------------------------------------------------------------
class TPatGrChangeForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxPageControl *StepPC;
        TcxTabSheet *Step2TS;
        TcxTabSheet *Step3TS;
        TcxTabSheet *ReadyTS;
        TPanel *PatLBPanel;
        TPanel *Step3Panel;
        TPanel *ReadyListPanel;
        TPanel *ReadyLabPanel;
        TLabel *PatReadyMemoLab;
        TcxButton *CancelBtn;
        TcxButton *PrevBtn;
        TcxButton *NextBtn;
        TcxTreeList *PatList;
        TcxTreeListColumn *cxTreeList1cxTreeListColumn1;
        TcxTreeListColumn *cxTreeList1cxTreeListColumn2;
        TPanel *Panel6;
        TcxButton *SelFromTemplateBtn;
        TcxButton *DelBtn;
        TcxTabSheet *RunTS;
        TcxProgressBar *RunPrBar;
        TPanel *Panel2;
        TcxLabel *RunMsgLab;
 TPanel *PatListPanel;
        TMemo *DetailReadyMemo;
        TLabel *DetailReadyMemoLab;
        TImage *Image1;
        TcxProgressBar *PrBar;
 TPanel *Panel4;
 TcxLabel *StepNameLab;
 TcxLabel *StepCommentLab;
 TcxTreeListColumn *PatListColumn1;
 TcxTreeListColumn *PatListColumn2;
 TcxTreeListColumn *PatListColumn3;
 TcxTreeListColumn *PatListColumn4;
 TcxPageControl *ReadyPC;
 TcxTabSheet *CustomTS;
 TcxTabSheet *BaseTS;
        void __fastcall NextBtnClick(TObject *Sender);
        void __fastcall PrevBtnClick(TObject *Sender);
        void __fastcall StepPCChange(TObject *Sender);
        void __fastcall SetSGSetEditText(TObject *Sender, int ACol,
          int ARow, const UnicodeString Value);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall CheckBtnEnabled(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall StepPCPageChanging(TObject *Sender,
          TcxTabSheet *NewPage, bool &AllowChange);
        void __fastcall SelFromTemplateBtnClick(TObject *Sender);
        void __fastcall DelBtnClick(TObject *Sender);
        void __fastcall PatListClick(TObject *Sender);
private:	// User declarations
        TListNodeMap CodeList;
//        TAnsiStrMapList NewSchListMap;
//        TICSTemplate *FTempl;
        bool BtnClick;
        TTagNode *FTmplNode;
        bool FInProgress;

        void __fastcall SetBtnEnabled();
        bool __fastcall CanProgress();
        void __fastcall SetInProgress(bool AVal)
        {
//          SaveBtn->Caption = (AVal)? "Прервать":"Передать";
          FInProgress = AVal;
          PrBar->Visible = AVal;
          if (AVal)  PrBar->Position = 0;
//          else       StatusBar->Panels->Items[1]->Text = "";
          SetEnabled(!AVal);
        };
        void __fastcall SetEnabled(bool AEnabled);
        void __fastcall FProgressInit(int AMax, UnicodeString AMsg);
        void __fastcall FProgressChange(int APercent, UnicodeString AMsg);
        void __fastcall FProgressComplite(UnicodeString AMsg);
protected:
  bool FInCheck;

  bool __fastcall FOnCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
  virtual void __fastcall SelectNextPage();
  virtual bool __fastcall CheckInput();
  virtual void __fastcall SelectPrevPage();
  virtual void __fastcall FillReadyTS();
  virtual void __fastcall BeforeExecute();
  virtual void __fastcall Execute(int AIdx, __int64 AUCode);
  virtual void __fastcall AfterExecute();
  virtual void __fastcall ExecuteFinally();
  virtual void __fastcall StepChange();
  virtual void __fastcall ButtonEnabled();
  virtual void __fastcall WndProc(Messages::TMessage &Message);
  bool __fastcall CheckKeyDown(TObject * Sender, WORD & Key, TShiftState Shift);
public:		// User declarations
  __fastcall TPatGrChangeForm(TComponent* Owner);
  __property bool InProgress = {read=FInProgress, write=SetInProgress};
};
//---------------------------------------------------------------------------
extern PACKAGE TPatGrChangeForm *PatGrChangeForm;
//---------------------------------------------------------------------------
#endif

