﻿//---------------------------------------------------------------------------

#ifndef PatPrivProbGrInsertH
#define PatPrivProbGrInsertH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxRadioGroup.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxCore.hpp"
#include "dxGDIPlusClasses.hpp"
#include "PatGrChange.h"
#include "InsertCardItem.h"
//---------------------------------------------------------------------------
class TPatPrivProbGrInsertForm : public TPatGrChangeForm
{
__published:	// IDE-managed Components
 TPanel *Panel3;
 TcxTreeList *PatPrivTL;
 TcxTreeListColumn *FamCol;
 TcxTreeListColumn *NameCol;
 TcxTreeListColumn *OtchCol;
 TcxTreeListColumn *BirthDayCol;
 TcxTreeListColumn *VacTypeCol;
 TcxTreeListColumn *ReacTypeCol;
 TPanel *Panel5;
 TcxRadioButton *TestRB;
 TcxRadioButton *MIBPRB;
 TcxComboBox *TestCB;
 TcxComboBox *MIBPCB;
 TcxDateEdit *ExecuteDE;
 TLabel *Label1;
 TcxTreeListColumn *ReacValCol;
 TcxStyleRepository *cxStyleRepository1;
 TcxStyle *dStyle;
 TcxTreeList *ReadyTL;
 TcxTreeListColumn *cxTreeListColumn7;
 TcxTreeListColumn *cxTreeListColumn8;
 TcxTreeListColumn *cxTreeListColumn9;
 TcxTreeListColumn *cxTreeListColumn10;
 TcxTreeListColumn *cxTreeListColumn11;
 TcxTreeListColumn *cxTreeListColumn12;
 TcxTreeListColumn *cxTreeListColumn13;
 TcxTreeListColumn *cxTreeListColumn14;
 TLabel *Label2;
 TLabel *Label3;
 TcxMaskEdit *SerED;
 TcxTreeListColumn *ReacTypeValCol;
 TcxTreeListColumn *SchValCol;
 void __fastcall MIBPRBClick(TObject *Sender);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall MIBPCBPropertiesChange(TObject *Sender);
 void __fastcall TestCBPropertiesChange(TObject *Sender);
 void __fastcall ExecuteDEPropertiesChange(TObject *Sender);
 void __fastcall PatPrivTLEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
 void __fastcall cxTreeList1Column1PropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall SerEDPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall VacTypeColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall ReacTypeColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall VacTypeColGetEditingProperties(TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
 void __fastcall ReacTypeColGetEditingProperties(TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
 void __fastcall ReacValColPropertiesChange(TObject *Sender);


private:	// User declarations
  TStringList *VacVar;
  TStringList *ReacVar;
  TTagNode *PrivDef, *TestDef;

  TInsertCardItem *FInsVacDataExecutor;
  TInsertCardItem *FInsTestDataExecutor;

  bool __fastcall CheckInput();
//  void __fastcall SelectNextPage();
//  void __fastcall SelectPrevPage();
  void __fastcall FillReadyTS();
  void __fastcall BeforeExecute();
  void __fastcall Execute(int AIdx, __int64 AUCode);
  void __fastcall AfterExecute();
//  void __fastcall ExecuteFinally();
  void __fastcall StepChange();
  void __fastcall ButtonEnabled();
  void __fastcall ClearValues();

  void __fastcall CheckNextBtnEnabled();
  void __fastcall FillVacVar(UnicodeString ACode, TStrings * AItems, bool ACanMIBP);
  void __fastcall FillReacVar(int ACode, TStrings * AItems, bool ACanMIBP);
  TcxTreeListNode * __fastcall GetActiveItemNode();
  int __fastcall GetActiveItemIndex();
  void __fastcall SetNextValues(int AIdx, int AExtIdx = 0);

public:		// User declarations
 __fastcall TPatPrivProbGrInsertForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPatPrivProbGrInsertForm *PatPrivProbGrInsertForm;
//---------------------------------------------------------------------------
#endif
