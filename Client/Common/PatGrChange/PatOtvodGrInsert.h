﻿//---------------------------------------------------------------------------

#ifndef PatOtvodGrInsertH
#define PatOtvodGrInsertH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxRadioGroup.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxCore.hpp"
#include "dxGDIPlusClasses.hpp"
#include "PatGrChange.h"
#include "InsertCardItem.h"
#include "cxCheckListBox.hpp"
//---------------------------------------------------------------------------
class TPatOtvodGrInsertForm : public TPatGrChangeForm
{
__published:	// IDE-managed Components
 TcxStyleRepository *cxStyleRepository1;
 TcxStyle *dStyle;
 TPanel *RegDataPanel;
 TPanel *S1Panel;
 TcxComboBox *InfTypeCB;
 TcxCheckListBox *InfChLB;
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall ExecuteDEPropertiesChange(TObject *Sender);
 void __fastcall InfTypeCBPropertiesChange(TObject *Sender);
 void __fastcall InfChLBClickCheck(TObject *Sender, int AIndex, TcxCheckBoxState APrevState,
          TcxCheckBoxState ANewState);


private:	// User declarations

  TdsRegTemplate *FTempl;
  TdsRegTemplateData * FNewPatData;
  TAnsiStrMap InfList;

  TdsRegEDItem * __fastcall TmplED(UnicodeString  AUID);
  void __fastcall LoadInfList(__int64 AUCode);
  void __fastcall Preload();
  bool __fastcall GetInput(TTagNode * itTag, UnicodeString & AValid);
  void __fastcall SetInfList();
  bool __fastcall CheckKeyDown(TObject * Sender, WORD & Key, TShiftState Shift);




//  TStringList *VacVar;
//  TStringList *ReacVar;
//  TTagNode *PrivDef, *TestDef;
//
  TInsertCardItem *FInsOtvodDataExecutor;
//  TInsertCardItem *FInsTestDataExecutor;

  bool __fastcall CheckInput();
//  void __fastcall SelectNextPage();
//  void __fastcall SelectPrevPage();
  void __fastcall FillReadyTS();
  void __fastcall BeforeExecute();
  void __fastcall Execute(int AIdx, __int64 AUCode);
  void __fastcall AfterExecute();
//  void __fastcall ExecuteFinally();
  void __fastcall StepChange();
  void __fastcall ButtonEnabled();

public:		// User declarations
 __fastcall TPatOtvodGrInsertForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPatOtvodGrInsertForm *PatOtvodGrInsertForm;
//---------------------------------------------------------------------------
#endif
