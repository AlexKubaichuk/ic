﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "PatOtvodGrInsert.h"
// ---------------------------------------------------------------------------
#define _Fam        0
#define _Name       1
#define _Otch       2
#define _BirthDay   3
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxRadioGroup"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxCore"
#pragma link "dxGDIPlusClasses"
#pragma link "PatGrChange"
#pragma link "cxCheckListBox"
#pragma resource "*.dfm"
TPatOtvodGrInsertForm * PatOtvodGrInsertForm;
// ---------------------------------------------------------------------------
__fastcall TPatOtvodGrInsertForm::TPatOtvodGrInsertForm(TComponent * Owner) : TPatGrChangeForm(Owner)
 {
  InfChLB->Items->Clear();
  /*
   MIBPCB->Properties->Items->Clear();
   TestCB->Properties->Items->Clear();

   VacVar          = new TStringList;
   ReacVar         = new TStringList;
   ExecuteDE->Date = Now();

   PrivDef = DM->RegDef->GetTagByUID("1003");
   TestDef = DM->RegDef->GetTagByUID("102f");

   TJSONObject * RetData = NULL;
   TJSONPairEnumerator * itPair;
   UnicodeString FClCode, FClStr;
   try
   {
   DM->GetValById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035 order by R0040", "0", RetData);
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
   FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
   MIBPCB->Properties->Items->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
   }

   DM->GetValById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A order by R002C", "0", RetData);
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
   FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
   TestCB->Properties->Items->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
   }
   }
   __finally
   {
   }
   */
  UnicodeString tmp =
    "<root> <fl ref = '305F' req='1'/>" // <date name='Начало действия' uid='305F' required='1' inlist='l'/>
    "<fl ref = '3065'/>" // <date name='Окончание действия' uid='3065'
    "<separator/>" // разделитель
    "<fl ref = '3066' req='1'/>" // <choice name='Причина' uid='3066' required
    "<fl ref = '3082' req='1'/>" // <extedit name='Заболевание' uid='3082
    "<fl ref = '3083' req='1'/>" // <text name='Список инфекций' uid='3083' inlist
    "<fl ref = '3043' req='1'/>" // <choice name='Отвод' uid='3043' required
    "<separator/>" // разделитель
    "<fl ref = '3110' req='1'/>" // <binary name='Отвод от прививки' uid='3110'
    "<fl ref = '3111' req='1'/>" // <binary name='Отвод от пробы' uid='3111'
    "</root>";
  FTempl                             = new TdsRegTemplate(DM->RegDef);
  FTempl->TmplPanel                  = S1Panel;
  FTempl->DataProvider->OnGetValById = DM->OnGetValById;
  // FTempl->DataProvider->OnGetCompValue  = FDM->OOnGetValById;
  FTempl->DataProvider->OnGetClassXML = DM->OnGetXML;
  FTempl->OnExtBtnClick               = DM->OnExtBtnClick;
  // FTempl->OnTreeBtnClick = FOnTreeBtnClick;
  // FTempl->StyleController = DM->StyleController;
  FTempl->DataPath = DM->DataPath;
  FTempl->CreateTemplate(tmp, NULL /* ATmplData */ , S1Panel->Height / 2);
  FTempl->CtrList->OnDataChange = FOnCtrlDataChange;
  TmplED("305F")->SetValue(Date().FormatString("dd.mm.yyyy"));
  TmplED("3065")->SetValue(Icsdateutil::IncMonth(Date(), 1).FormatString("dd.mm.yyyy"));
  TmplED("3110")->SetValue("1");
  TmplED("3111")->SetValue("1");
  TmplED("3083")->Visible = false;
  TmplED("3043")->Visible = false;
  FTempl->OnKeyDown             = FormKeyDown;
  S1Panel->Align                = alClient;
  Caption                       = "Добавление отвода для группы пациентов"; // [0];
  FTempl->CtrList->OnDataChange = FOnCtrlDataChange;
  Preload();
  FInsOtvodDataExecutor = new TInsertCardItem(DM->RegDef->GetTagByUID("3030"),
    DM->XMLList->GetXML("12063611-00008CD7-CD89"));
  FInsOtvodDataExecutor->OnGetValById = DM->GetValById;
  FInsOtvodDataExecutor->OnInsertData = DM->InsertData;
  FInsOtvodDataExecutor->InLPU        = 1;
  FInsOtvodDataExecutor->LPU          = DM->CardComp->LPUCode;
  FInsOtvodDataExecutor->BinReak      = true;
  FInsOtvodDataExecutor->Preload();
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatOtvodGrInsertForm::CheckInput()
 {
  FTempl->CtrList->OnDataChange = NULL;
  bool RC = false;
  bool FAllCheck = true;
  try
   {
    for (int i = 0; (i < InfChLB->Items->Count) && FAllCheck; i++)
     {
      FAllCheck &= InfChLB->Items->Items[i]->Checked;
     }
    if (FAllCheck)
     InfTypeCB->ItemIndex = 0;
    SetInfList();
    UnicodeString FValid = "";
    // int OtvodType;
    TDateTime begDate, endDate;
    RC = FTempl->CheckInput(true);
    // DM->RegDef->GetTagByUID("3030")->Iterate(GetInput, FValid);
    bool FCont = TmplED("3043")->GetValue("").Trim().Length();
    if (RC || (!RC && !FCont) /* !FValid.Length() */)
     {
      RC = TryStrToDate(TmplED("305F")->GetValue("").c_str(), begDate);
      if (!RC)
       _MSG_ERR("Введите корректную дату начала действия отвода", "Ошибка");
      else
       {
        if (TmplED("3065")->GetValue("").Length())
         {
          RC = TryStrToDate(TmplED("3065")->GetValue("").c_str(), endDate);
          if (!RC)
           _MSG_ERR("Введите корректную дату окончания действия отвода", "Ошибка");
          else
           {
            TmplED("3043")->SetValue("0");
            RC = (begDate <= endDate);
            if (!RC)
             _MSG_ERR("Дата окончания действия отвода не может быть меньше даты начала действия отвода.", "Ошибка");
           }
         }
        else
         {
          TmplED("3043")->SetValue("1");
          RC = true;
         }
        if (RC)
         {
          if (!TmplED("3110")->GetValue("").ToIntDef(0) && !TmplED("3111")->GetValue("").ToIntDef(0))
           _MSG_ERR("Необходимо указать значение для одного из элементов 'Отвод прививки' или 'Отвод пробы'.",
          "Ошибка");
          else
           RC = true;
         }
       }
     }
   }
  __finally
   {
    FTempl->CtrList->OnDataChange = FOnCtrlDataChange;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::SetInfList()
 {
  UnicodeString FInfList = "";
  if (InfTypeCB->ItemIndex)
   {
    for (int i = 0; i < InfChLB->Items->Count; i++)
     {
      if (InfChLB->Items->Items[i]->Checked)
       {
        if (FInfList.Length())
         FInfList += ",";
        FInfList += IntToStr((int)InfChLB->Items->Items[i]->ItemObject) + ":" + InfChLB->Items->Items[i]->Text;
       }
     }
   }
  else
   FInfList = "all:Все инфекции";
  TmplED("3083")->SetValue(FInfList);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::FillReadyTS()
 {
  StepNameLab->Caption         = "Всё готово для добавления отвода."; // [1];
  PatReadyMemoLab->Caption     = "Пациенты для которых будет добавлен отвод:"; // [2];
  DetailReadyMemoLab->Caption  = "Параметры отвода, который будет добавлен:"; // [3];
  DetailReadyMemo->Lines->Text = StringReplace(FTempl->GetSelectedTextValues(true), "#", "=",
    TReplaceFlags() << rfReplaceAll);
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::BeforeExecute()
 {
  // FInsDataExecutor->Doc = 0;
  // FInsDataExecutor->MedSis = 0;
  // FInsDataExecutor->FinSrc = 0;
  // FInsDataExecutor->ExtInf = 0;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::Execute(int AIdx, __int64 AUCode)
 {
  UnicodeString FRecID, FRC;
  TkabCustomDataSetRow * RecData;
  try
   {
    TDate ptbd = TDate(PatList->Root->Items[AIdx]->Texts[_BirthDay]);
    // ShowMessage(ptbd.FormatString("dd.mm.yyyy"));
    FInsOtvodDataExecutor->NewRec(ptbd);
    RecData = FInsOtvodDataExecutor->Data;
    // RecData->Value[""] = TmplED("")->GetCompValue(""); // <extedit name='cguid' uid='32DA' required='1'
    // RecData->Value["317C"] = AUCode; // <extedit name='Код пациента' uid='317C'
    RecData->Value["305F"] = TDate(TmplED("305F")->GetValue(""));
    // <date name='Начало действия' uid='305F' required='1'
    if (TmplED("3065")->GetValue("").Length())
     RecData->Value["3065"] = TDate(TmplED("3065")->GetValue("")); // <date name='Окончание действия' uid='3065'
    RecData->Value["3043"] = TmplED("3043")->GetCompValue(""); // <choice name='Отвод' uid='3043' required='1'
    RecData->Value["3083"] = TmplED("3083")->GetCompValue(""); // <text name='Список инфекций' uid='3083'
    RecData->Value["3066"] = TmplED("3066")->GetCompValue(""); // <choice name='Причина' uid='3066' required='1'
    if (TmplED("3082")->GetValue("").Length())
     RecData->Value["3082"] = TmplED("3082")->GetCompValue(""); // <extedit name='Заболевание' uid='3082' required='1'
    else
     RecData->Value["3082"] = Variant::Empty();
    // TmplED("3082")->GetCompValue(""); // <extedit name='Заболевание' uid='3082' required='1'
    RecData->Value["3110"] = TmplED("3110")->GetCompValue(""); // <binary name='Отвод от прививки' uid='3110'
    RecData->Value["3111"] = TmplED("3111")->GetCompValue(""); // <binary name='Отвод от пробы' uid='3111'
    // ShowMessage("11");
    FInsOtvodDataExecutor->Execute(AUCode, FRecID, FRC);
    // ShowMessage("12");
   }
  catch (System::Sysutils::Exception & E)
   {
    MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
   }
  catch (...)
   {
    MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::AfterExecute()
 {
  // if (FNewPatData)
  // delete FNewPatData;
  // FNewPatData = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::StepChange()
 {
  StepNameLab->Caption    = "Выбор параметров пациентов, которые необходимо изменить"; // [4];
  StepCommentLab->Caption = "Укажите параметры пациентов, которые необходимо изменить."; // [5];
  if (!InfChLB->Items->Count)
   {
    LoadInfList((__int64)PatList->Root->Items[0]->Data);
    InfTypeCBPropertiesChange(InfTypeCB);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::ButtonEnabled()
 {
  NextBtn->Enabled = CheckInput();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::FormDestroy(TObject * Sender)
 {
  delete FTempl;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::ExecuteDEPropertiesChange(TObject * Sender)
 {
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TPatOtvodGrInsertForm::TmplED(UnicodeString AUID)
 {
  return FTempl->CtrList->TemplateItems[AUID];
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::InfTypeCBPropertiesChange(TObject * Sender)
 {
  for (int i = 0; i < InfChLB->Items->Count; i++)
   {
    InfChLB->Items->Items[i]->Checked = (InfTypeCB->ItemIndex != 1);
   }
  InfChLB->Enabled = (InfTypeCB->ItemIndex == 1);
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::LoadInfList(__int64 AUCode)
 {
  TTagNode * FSettingNode = new TTagNode;
  TStringList * CalendInfList = new TStringList;
  TStringList * CommInfList = new TStringList;
  InfChLB->Items->BeginUpdate();
  try
   {
    FSettingNode->AsXML = DM->GetUnitPlanOptions(AUCode);
    TTagNode * FPersPlanList = FSettingNode->GetChildByName("vacplan", true);
    TTagNode * FComm = NULL;
    if (FPersPlanList)
     {
      FComm = FPersPlanList->GetChildByName("comm");
     }
    for (TAnsiStrMap::iterator i = InfList.begin(); i != InfList.end(); i++)
     {
      if (DM->InfInPlan(FComm, NULL, i->first))
       CalendInfList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
      else
       CommInfList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
     }
    CalendInfList->Sort();
    CommInfList->Sort();
    TcxCheckListBoxItem * tmpChLBItem;
    InfChLB->Items->Clear();
    for (int i = 0; i < CalendInfList->Count; i++)
     {
      tmpChLBItem             = InfChLB->Items->Add();
      tmpChLBItem->Text       = CalendInfList->Strings[i];
      tmpChLBItem->ItemObject = CalendInfList->Objects[i];
     }
    for (int i = 0; i < CommInfList->Count; i++)
     {
      tmpChLBItem             = InfChLB->Items->Add();
      tmpChLBItem->Text       = CommInfList->Strings[i];
      tmpChLBItem->ItemObject = CommInfList->Objects[i];
     }
   }
  __finally
   {
    delete CalendInfList;
    delete CommInfList;
    delete FSettingNode;
    InfChLB->Items->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::Preload()
 {
  TJSONObject * RetData = NULL;
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;
  try
   {
    DM->GetValById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A order by R003C", "0", RetData);
    itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode          = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr           = ((TJSONString *)itPair->Current->JsonValue)->Value();
      InfList[FClCode] = FClStr;
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatOtvodGrInsertForm::GetInput(TTagNode * itTag, UnicodeString & AValid)
 {
  if (itTag->CmpAV("uid", "3083"))
   {
    bool IsSel = false;
    for (int i = 0; i < InfChLB->Items->Count; i++)
     IsSel |= InfChLB->Items->Items[i]->Checked;
    if (!IsSel)
     {
      _MSG_INF("Необходимо выбрать минимум одну инфекцию", "Сообщение");
      AValid = "no";
      return true;
     }
   }
  else
   {
    if (FTempl->CtrList->GetEDControl(itTag->AV["uid"]))
     if (!TmplED(itTag->AV["uid"])->CheckReqValue())
      {
       AValid = "no";
       return true;
      }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatOtvodGrInsertForm::InfChLBClickCheck(TObject * Sender, int AIndex, TcxCheckBoxState APrevState,
  TcxCheckBoxState ANewState)
 {
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatOtvodGrInsertForm::CheckKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  bool RC = false;
  try
   {
    if (TmplED("3082")->Enabled)
     {
      RC = TmplED("3082")->CheckReqValue(NULL, true);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
