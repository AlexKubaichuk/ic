﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "PatSchemeGrInsert.h"
#include "dsCardSchListUnit.h"
#include "DMUnit.h"
// ---------------------------------------------------------------------------
#define _Fam        0
#define _Name       1
#define _Otch       2
#define _BirthDay   3
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxRadioGroup"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxCore"
#pragma link "dxGDIPlusClasses"
#pragma link "PatGrChange"
#pragma link "cxCheckListBox"
#pragma link "cxButtonEdit"
#pragma resource "*.dfm"
TPatSchemeGrInsertForm * PatSchemeGrInsertForm;
// ---------------------------------------------------------------------------
__fastcall TPatSchemeGrInsertForm::TPatSchemeGrInsertForm(TComponent * Owner) : TPatGrChangeForm(Owner)
 {
  Preload();
  LoadSchSettingExecute();
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatSchemeGrInsertForm::CheckInput()
 {
  bool RC = false;
  try
   {
    for (int i = 0; (i < SchSettingTV->Root->Count) && !RC; i++)
     RC = SchSettingTV->Root->Items[i]->Texts[2].Length() + SchSettingTV->Root->Items[i]->Texts[3].Length();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::FillReadyTS()
 {
  StepNameLab->Caption        = "Всё готово для установки схем."; // [1];
  PatReadyMemoLab->Caption    = "Пациенты для которых будут установлены схемы:"; // [2];
  DetailReadyMemoLab->Caption = "Схемы, которые будут установены:"; // [3];
  UnicodeString FSch = "";
  for (int i = 0; i < SchSettingTV->Root->Count; i++)
   {
    if (SchSettingTV->Root->Items[i]->Texts[2].Length() + SchSettingTV->Root->Items[i]->Texts[3].Length())
     {
      FSch += SchSettingTV->Root->Items[i]->Texts[1] + " >>> ";
      if (SchSettingTV->Root->Items[i]->Texts[2].Length())
       FSch += " МИБП: " + SchSettingTV->Root->Items[i]->Texts[2];
      if (SchSettingTV->Root->Items[i]->Texts[3].Length())
       FSch += " Проба: " + SchSettingTV->Root->Items[i]->Texts[3];
      FSch += "\n";
     }
   }
  DetailReadyMemo->Lines->Text = FSch;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::BeforeExecute()
 {
  FSetSchData = "";
  TTagNode * FSchNode;
  for (int i = 0; i < SchSettingTV->Root->Count; i++)
   {
    if (SchSettingTV->Root->Items[i]->Texts[2].Length() + SchSettingTV->Root->Items[i]->Texts[3].Length())
     {
      if (SchSettingTV->Root->Items[i]->Data)
       {
        FSchNode = (TTagNode *)SchSettingTV->Root->Items[i]->Data;
        FSetSchData += "<i infref='"+FSchNode->AV["infref"]+"'";
        if (SchSettingTV->Root->Items[i]->Texts[2].Length())
         FSetSchData += " v='" + FSchNode->AV["vschdef"]+"'";
        if (SchSettingTV->Root->Items[i]->Texts[3].Length())
         FSetSchData += " t='" + FSchNode->AV["tschdef"]+"'";
        FSetSchData += "/>";
       }
     }
   }
  FSetSchData = "<s>"+FSetSchData+"</s>";

 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::Execute(int AIdx, __int64 AUCode)
 {
  // UnicodeString FRecID, FRC;
  // TkabCustomDataSetRow * RecData;
  try
   {
    DM->Clients->App->SetPatDefSch(AUCode, FSetSchData);
   }
  catch (System::Sysutils::Exception & E)
   {
    MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
   }
  catch (...)
   {
    MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::AfterExecute()
 {
  // if (FNewPatData)
  // delete FNewPatData;
  // FNewPatData = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::StepChange()
 {
  StepNameLab->Caption    = "Выбор параметров пациентов, которые необходимо изменить"; // [4];
  StepCommentLab->Caption = "Укажите параметры пациентов, которые необходимо изменить."; // [5];
  // if (!InfChLB->Items->Count)
  // {
  // LoadInfList((__int64)PatList->Root->Items[0]->Data);
  // InfTypeCBPropertiesChange(InfTypeCB);
  // }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::ButtonEnabled()
 {
  NextBtn->Enabled = CheckInput();
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::Preload()
 {
  TJSONObject * RetData = NULL;
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;
  try
   {
    DM->GetValById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A order by R003C", "0", RetData);
    itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode          = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr           = ((TJSONString *)itPair->Current->JsonValue)->Value();
      InfList[FClCode] = FClStr;
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatSchemeGrInsertForm::CheckKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  bool RC = false;
  try
   {
    // if (TmplED("3082")->Enabled)
    // {
    // RC = TmplED("3082")->CheckReqValue(NULL, true);
    // }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::FSchButtonClick(int AButtonIndex, int ACol, UnicodeString AAttr)
 {
  bool FModify = false;
  if (AButtonIndex)
   {
    SetSchData(ACol, AAttr, "", "");
    FModify = true;
   }
  else
   {
    UnicodeString FSch, FStrSch;
    if (GetSchData(ACol, AAttr, FSch, FStrSch))
     {
      SetSchData(ACol, AAttr, FSch, FStrSch);
      FModify = true;
     }
   }
  ButtonEnabled();
  if (FModify)
   {
    // SaveSettingBtnPanel->Visible = true;
    // actSaveSchSetting->Enabled   = true;
    // actLoadSchSetting->Enabled   = true;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::VacSchColPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSchButtonClick(AButtonIndex, 2, "vschdef");
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::TestSchColPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSchButtonClick(AButtonIndex, 3, "tschdef");
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::LoadSchSettingExecute()
 {
  SchSettingTV->BeginUpdate();
  try
   {
    FSettingNode = DM->XMLList->GetXML("planSetting");
    TTagNode * FSchList = FSettingNode->GetChildByName("infdef", true);
    TTagNode * itNode;
    TcxTreeListNode * tmpNode;
    if (FSchList)
     {
      SchSettingTV->Clear();
      TTagNode * itInf = FSchList->GetFirstChild();
      while (itInf)
       {
        tmpNode = SchSettingTV->Root->AddChild();
        // tmpNode->Values[0] = InfInPlan(FComm, FPers, itInf->AV["infref"]);
        tmpNode->Texts[1] = InfList[itInf->AV["infref"]];
        tmpNode->Data     = itInf;
        itInf             = itInf->GetNext();
       }
      NKSettingTVCol->SortOrder = soDescending;
      InfNameSchCol->SortOrder = soAscending;
     }
   }
  __finally
   {
    SchSettingTV->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TPatSchemeGrInsertForm::SetSchData(int ACol, UnicodeString AAttr, UnicodeString AVal,
  UnicodeString AStrVal)
 {
  TTagNode * FData = GetSettingSelNode(SchSettingTV);
  if (FData)
   {
    SchSettingTV->Selections[0]->Texts[ACol]     = AStrVal;
    SchSettingTV->Selections[0]->Texts[ACol + 2] = AVal;
    if (SchSettingTV->InplaceEditor)
     {
      SchSettingTV->InplaceEditor->EditValue = AStrVal;
      SchSettingTV->Root->EndEdit(false);
     }
    FData->AV[AAttr] = AVal;
    FData->AV["ch" + AAttr] = "1";
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TPatSchemeGrInsertForm::GetSchData(int ACol, UnicodeString AAttr, UnicodeString & AVal,
  UnicodeString & AStrVal)
 {
  bool RC = false;
  TSchemaListForm * Dlg = NULL;
  try
   {
    TTagNode * FData = GetSettingSelNode(SchSettingTV);
    if (FData)
     {
      Dlg = new TSchemaListForm(this, DM->CardComp->DM, FData->AV["infref"].ToIntDef(0), !(ACol % 2),
        FData->AV[AAttr], false);
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       {
        AVal    = GetLPartB(Dlg->Value, '=');
        AStrVal = GetRPartE(Dlg->Value, '=');
        RC      = true;
       }
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TPatSchemeGrInsertForm::GetSettingSelNode(TcxTreeList * ASrc)
 {
  TTagNode * RC = NULL;
  try
   {
    if (ASrc->SelectionCount)
     {
      if (ASrc->Selections[0]->Data)
       RC = (TTagNode *)ASrc->Selections[0]->Data;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
