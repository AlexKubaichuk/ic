﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ProgressUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxProgressBar"
#pragma link "cxLabel"
#pragma link "cxImage"
#pragma link "dxGDIPlusClasses"
#pragma resource "*.dfm"
TdsProgressForm *dsProgressForm;
//---------------------------------------------------------------------------
__fastcall TdsProgressForm::TdsProgressForm(TComponent* Owner)
 : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TdsProgressForm::RefreshTimerTimer(TObject *Sender)
{
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TdsProgressForm::FormShow(TObject *Sender)
{
  RefreshTimer->Enabled = true;
}
//---------------------------------------------------------------------------
