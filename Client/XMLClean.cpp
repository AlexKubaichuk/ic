//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include "ExtUtils.h"
#include "XMLContainer.h"
#include "System.zip.hpp"
#include "msgdef.h"
//#include "main.h"
//---------------------------------------------------------------------------
bool __fastcall GetCGUID(TTagNode *ANode, UnicodeString &UID)
{
  if (ANode->CmpAV("fl", "cguid"))
   ANode->AV["depend"] = ANode->AV["uid"]+"."+ANode->AV["uid"];
  return false;
}
//---------------------------------------------------------------------------
void __fastcall Clean(TTagNode *ANode)
{
}
//---------------------------------------------------------------------------
void __fastcall SetCGUID(TTagNode *ANode)
{
  UnicodeString ES = "";
//  ANode->LoadFromXMLFile(FFilePath);
  ANode->Iterate(GetCGUID, ES);
}
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
 try
 {
   Application->Initialize();
   Application->MainFormOnTaskBar = true;
   UnicodeString FFilePath = "";
   if (ParamCount() > 0)
     FFilePath = icsPrePath(ParamStr(1));
   else
    {
      TOpenDialog *Dlg = new TOpenDialog(NULL);
      try
       {
         if (Dlg->Execute())
          FFilePath = Dlg->FileName;
       }
      __finally
       {
         delete Dlg;
       }
    }
   if (FFilePath.Length())
    {
      if (FileExists(FFilePath))
       {
         TTagNode *ExpData = new TTagNode;
         try
          {
            ExpData->LoadFromZIPXMLFile(FFilePath);
            TTagNode *FCls = ExpData->GetChildByName("cls", true);
            TTagNode *itRec;
            TTagNode *SaveDelNode;
            TAnsiStrMap FCodes = TAnsiStrMap();
            TAnsiStrMap::iterator FRC;
            if (FCls)
             {
               FCls = FCls->GetFirstChild();
               while (FCls)
                {
                  FCodes.clear();
                  itRec = FCls->GetFirstChild();
                  while (itRec)
                   {
                     FRC = FCodes.find(itRec->AV["cd"]);
                     SaveDelNode = NULL;
                     if (FRC != FCodes.end())
                      SaveDelNode = itRec;
                     else
                      FCodes[itRec->AV["cd"]] = "1";
                     itRec = itRec->GetNext();
                     if (SaveDelNode)
                      delete SaveDelNode;
                   }
                  FCls = FCls->GetNext();
                }
             }
          }
         __finally
          {
            ExpData->GetTagByUID("1076")->DeleteChild();
            ExpData->GetTagByUID("10AA")->DeleteChild();
            ExpData->GetTagByUID("1084")->DeleteChild();

            ExpData->SaveToZIPXMLFile(FFilePath, "");
            delete ExpData;
            ShowMessage("��������.");
          }
       }
      else
       _MSG_ERRA("���� ��� ������� �� ������." , "������");
    }
//   Application->Run();
 }
 catch (System::Sysutils::Exception &exception)
 {
   Application->ShowException(&exception);
 }
 catch (...)
 {
   try
   {
    throw System::Sysutils::Exception("");
   }
   catch (System::Sysutils::Exception &exception)
   {
    Application->ShowException(&exception);
   }
 }
 return 0;
}
//---------------------------------------------------------------------------

