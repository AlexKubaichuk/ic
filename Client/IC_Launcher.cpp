//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("Common\Update\ClientUpdateUnit.cpp", ClientUpdateForm);
USEFORM("Common\Update\UpdateUnit.cpp", UpdateDM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
#include "ClientUpdateUnit.h"
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    Application->MainFormOnTaskBar = true;
    TClientUpdateForm * UpdDlg = new TClientUpdateForm(NULL);
    try
     {
       UpdDlg->CheckUpdate(true);
     }
    __finally
     {
      delete UpdDlg;
     }
   }
  catch (System::Sysutils::Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw System::Sysutils::Exception("");
     }
    catch (System::Sysutils::Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
//---------------------------------------------------------------------------
