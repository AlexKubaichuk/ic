object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = #1056#1091#1082#1086#1080#1089#1087#1088#1072#1074#1083#1103#1083#1082#1072' '#1086#1087#1080#1089#1072#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 471
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 443
    Width = 635
    Height = 13
    Align = alBottom
    AutoSize = False
    ExplicitTop = 343
    ExplicitWidth = 631
  end
  object Bevel1: TBevel
    Left = 210
    Top = 44
    Width = 310
    Height = 104
    Shape = bsFrame
  end
  object Label3: TLabel
    Left = 218
    Top = 190
    Width = 22
    Height = 13
    Caption = 'GUI:'
  end
  object Bevel2: TBevel
    Left = 369
    Top = 45
    Width = 3
    Height = 100
    Shape = bsRightLine
  end
  object Bevel3: TBevel
    Left = 218
    Top = 52
    Width = 413
    Height = 26
    Shape = bsFrame
  end
  object Label4: TLabel
    Left = 215
    Top = 37
    Width = 82
    Height = 13
    Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1090#1091#1088#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 375
    Top = 37
    Width = 87
    Height = 13
    Caption = #1054#1073#1089#1083#1077#1076#1086#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PrBar: TProgressBar
    Left = 0
    Top = 456
    Width = 635
    Height = 15
    Align = alBottom
    Smooth = True
    Step = 1
    TabOrder = 0
    ExplicitTop = 356
    ExplicitWidth = 631
  end
  object spGenChB: TCheckBox
    Left = 218
    Top = 50
    Width = 140
    Height = 17
    Caption = #1061#1088#1072#1085#1080#1084#1099#1077' '#1087#1088#1086#1094#1077#1076#1091#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object FieldNameChB: TCheckBox
    Left = 521
    Top = 49
    Width = 95
    Height = 17
    Caption = #1048#1084#1077#1085#1072' '#1087#1086#1083#1077#1081
    TabOrder = 2
  end
  object ImmNomGenBtn: TButton
    Left = 9
    Top = 239
    Width = 179
    Height = 25
    Caption = #1053#1086#1084#1077#1085#1082#1083#1072#1090#1086#1088' '#1080#1084#1084#1091#1085#1080#1079#1072#1094#1080#1080
    TabOrder = 3
  end
  object NomGUIED: TEdit
    Left = 252
    Top = 187
    Width = 221
    Height = 21
    TabOrder = 4
    Text = '0E291426-00005882-2493'
  end
  object TrigChB: TCheckBox
    Left = 218
    Top = 70
    Width = 140
    Height = 17
    Caption = #1058#1088#1080#1075#1075#1077#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 5
  end
  object GenChB: TCheckBox
    Left = 218
    Top = 90
    Width = 93
    Height = 17
    Caption = #1043#1077#1085#1077#1088#1072#1090#1086#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 6
  end
  object ExceptChB: TCheckBox
    Left = 218
    Top = 110
    Width = 93
    Height = 17
    Caption = #1048#1089#1082#1083#1102#1095#1077#1085#1080#1103
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object inqspGenChB: TCheckBox
    Left = 377
    Top = 50
    Width = 140
    Height = 17
    Caption = #1061#1088#1072#1085#1080#1084#1099#1077' '#1087#1088#1086#1094#1077#1076#1091#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 8
  end
  object inqTrigChB: TCheckBox
    Left = 377
    Top = 70
    Width = 140
    Height = 17
    Caption = #1058#1088#1080#1075#1075#1077#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 9
  end
  object inqGenChB: TCheckBox
    Left = 377
    Top = 90
    Width = 93
    Height = 17
    Caption = #1043#1077#1085#1077#1088#1072#1090#1086#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
  object inqExceptChB: TCheckBox
    Left = 377
    Top = 110
    Width = 93
    Height = 17
    Caption = #1048#1089#1082#1083#1102#1095#1077#1085#1080#1103
    Checked = True
    State = cbChecked
    TabOrder = 11
  end
  object IdxChB: TCheckBox
    Left = 218
    Top = 128
    Width = 93
    Height = 17
    Caption = #1048#1085#1076#1077#1082#1089#1099
    Checked = True
    State = cbChecked
    TabOrder = 12
  end
  object inqIdxChB: TCheckBox
    Left = 377
    Top = 128
    Width = 93
    Height = 17
    Caption = #1048#1085#1076#1077#1082#1089#1099
    Checked = True
    State = cbChecked
    TabOrder = 13
  end
  object FuncCheckBtn: TButton
    Left = 9
    Top = 287
    Width = 179
    Height = 25
    Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1092#1091#1085#1082#1094#1080#1081' '#1085#1086#1084#1077#1085#1082#1083#1072#1090#1086#1088#1072
    TabOrder = 14
  end
  object FuncList: TMemo
    Left = 208
    Top = 220
    Width = 394
    Height = 88
    ScrollBars = ssBoth
    TabOrder = 15
  end
  object ImmNomEditBtn: TButton
    Left = 10
    Top = 265
    Width = 179
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1085#1086#1084#1077#1085#1082#1083#1072#1090#1086#1088' '#1080#1084#1084'.'
    TabOrder = 16
  end
  object ProjectBE: TcxButtonEdit
    Left = 8
    Top = 8
    Properties.Buttons = <
      item
        Default = True
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000CE0E0000C40E00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          77777777777777777777000000000007777700333333333077770B0333333333
          07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
          77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
          7007777777770777070777777777700077777777777777777777}
        Kind = bkGlyph
      end>
    Properties.ReadOnly = True
    Properties.ViewStyle = vsHideCursor
    Properties.OnButtonClick = ProjectBEPropertiesButtonClick
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.Kind = lfStandard
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.Kind = lfStandard
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.Kind = lfStandard
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 17
    Width = 614
  end
  object SQLScriptBtn: TButton
    Left = 16
    Top = 43
    Width = 175
    Height = 25
    Caption = 'SQL-Script'
    TabOrder = 18
    OnClick = SQLScriptBtnClick
  end
  object Button7: TButton
    Left = 11
    Top = 312
    Width = 179
    Height = 25
    Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1092#1091#1085#1082#1094#1080#1081' '#1085#1086#1084#1077#1085#1082#1083#1072#1090#1086#1088#1072
    TabOrder = 19
  end
  object OpenDlg: TOpenDialog
    DefaultExt = '.xml'
    Filter = 'xml - '#1092#1072#1081#1083#1099'|*.xml|tert|ertert'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = #1054#1090#1082#1088#1099#1090#1100' '#1086#1087#1080#1089#1072#1085#1080#1077' '#1087#1088#1086#1077#1082#1090#1072' SSDL'
    Left = 131
    Top = 154
  end
end
