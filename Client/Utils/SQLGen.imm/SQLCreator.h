//---------------------------------------------------------------------------

#ifndef SQLCreatorH
#define SQLCreatorH

//---------------------------------------------------------------------------
/*
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include "DMF.h"
*/
//---------------------------------------------------------------------------
#include "DMF.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TSLIterate)(TTagNode *ANode, TList *Dest);
class TSQLCreator : public TObject
{
private:
     bool __fastcall getFieldDef(TTagNode *itTag, TList *ADest);
     bool __fastcall getClFieldDef(TTagNode *itTag, TList *ADest);
     void __fastcall CreateFieldDefs(TTagNode *ACLDef,TTagNode *ADestDef);
     bool __fastcall GetChBD(TTagNode *itTag, UnicodeString &UID);
     void __fastcall CreateRegDepend(TTagNode *AClDef,TTagNode *AClRoot/*, TTagNode *AUnitRoot*/);
     void __fastcall GetReference(UnicodeString UID, TTagNode *AClRoot, /*TTagNode *AURoot,*/TStringList *AbfUIDList,TStringList *AafUIDList);
     UnicodeString UnitTabName;
//     TStringList *tblList;
//     TTagNode    *xmlCopyList;
//     TTagNode    *xmlTblList;
//     TTagNode    *xmlTable;
//     TTagNode    *xmlGenList;
     TStringList *FrRef;
//     TTagNode  *iTag;
     bool __fastcall Iterate(TTagNode *ASrcNode, TSLIterate ItItem, TList *ADest);
     UnicodeString __fastcall CreateRegStruct(TTagNode *ASrc, bool AExtInq = false);
     bool __fastcall getClassList(TTagNode *itTag, TList *ADest);
     UnicodeString __fastcall GetFlType(TTagNode *Src, bool AFull = true);
     void __fastcall CreateClassDataProc(TTagNode *AClDef,TTagNode *ASPDef, UnicodeString ATabPref);
     void __fastcall Create_SYSDATA(TTagNode *AflNode, UnicodeString ATblName);
     void __fastcall CreateMCODEProc(TTagNode *ASPDef, UnicodeString ATabPref);
     TTagNode *CurrTab;
     TTagNode *CurrFl;
     void __fastcall CreateStruct();
     void __fastcall CreateTablesScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateTableScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateIndicesScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateGeneratorsScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateExceptionsScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateTriggersScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateProceduresScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateExecutesScript(TTagNode *ADefNode,   TStringList *AScript);
     void __fastcall FCreate_Script(TStringList *AScript);
     void __fastcall CreateVarsList(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateInputList(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateOutputList(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateFieldList(TTagNode *ADefNode, TStringList *AScript);
     TTagNode *TabRoot;
     TTagNode *ProcRoot;
     TTagNode *TrigRoot;
     TTagNode *GenRoot;
     TTagNode *ExceptRoot;
     TTagNode *IndRoot;
     TTagNode *ExRoot;

     bool fscSysData,fscFieldName,fscFunction;
     bool fscTables,fscSP,fscExcept,fscGen,fscTrig,fscIdx;
     bool fsinqTables,fscinqSP,fscinqExcept,fscinqGen,fscinqTrig,fscinqIdx;

    UnicodeString FFlPr;
    TTagNode *FDBStruct;
    TTagNode *RegDef;
//    UnicodeString FPrefED;
public:
     TProgressBar *PrBar;
     TLabel *Label1;
     UnicodeString AllXMLPath,OFileName;
     void __fastcall CreateDbStruct();
     void __fastcall Create_Script();

     __property TTagNode *DBStruct        = {read=FDBStruct,write=FDBStruct};

     __property bool CanSystemScript = {read=fscSysData,write=fscSysData};
     __property bool CanFieldName    = {read=fscFieldName,write=fscFieldName};
     __property bool CanFunction     = {read=fscFunction,write=fscFunction};


     __property bool CanTables     = {read=fscTables,write=fscTables};
     __property bool CanStoredProc = {read=fscSP,write=fscSP};
     __property bool CanExceptions = {read=fscExcept,write=fscExcept};
     __property bool CanGenerators = {read=fscGen,write=fscGen};
     __property bool CanTriggers   = {read=fscTrig,write=fscTrig};
     __property bool CanIndices    = {read=fscIdx,write=fscIdx};

     __property bool CanInqTables     = {read=fsinqTables,write=fsinqTables};
     __property bool CanInqStoredProc = {read=fscinqSP,write=fscinqSP};
     __property bool CanInqExceptions = {read=fscinqExcept,write=fscinqExcept};
     __property bool CanInqGenerators = {read=fscinqGen,write=fscinqGen};
     __property bool CanInqTriggers   = {read=fscinqTrig,write=fscinqTrig};
     __property bool CanInqIndices    = {read=fscinqIdx,write=fscinqIdx};
};
#endif
