// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "SQLCreator.h"
// #define _CREATE_TABLE UnicodeString("EXECUTE PROCEDURE CREATETABLE_IF_NOT_EXISTS")
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateDbStruct()
 {
  UnicodeString PrName;
  try
   {
    Label1->Caption = "��������� ��������� �� ... ";
    Label1->Update();
    // ------ ��������� �������  ----------------------------------------------
    UnitTabName = "";
    UnicodeString tmp, _tmp, PrName;
    tmp = "";
    TTagNode * itTMP;
    // ��������� �������� ��������� ��� ������������
    try
     {
      // �������� ���������
      CreateStruct();
      for (int i = 0; i < DM->SQLProj->Count; i++)
       {
        itTMP = (TTagNode *)DM->SQLProj->Items[i];
        if (itTMP->CmpAV("xmltype", "registry"))
         {
          // ��������� ������ ��� ������������
          // --------------------------------------------->>>>
          UnitTabName = CreateRegStruct(DM->XMLList->GetXML(itTMP->AV["xmlref"]));
          // --------------------------------------------->>>>
         }
       }
     }
    __finally
     {
      Label1->Caption = "";
      Label1->Update();
     }
   }
  catch (Exception & E)
   {
    _MSG_ERRA(E.Message, "������ �������� ���������");
    return;
   }
  catch (...)
   {
    _MSG_ERRA("������ �������� ���������", "������");
    return;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateStruct()
 {
  // FDBStruct = new TTagNode(NULL);
  FDBStruct->Name = "FDBStruct";
  TabRoot         = FDBStruct->AddChild("tables");
  ProcRoot        = FDBStruct->AddChild("procedures");
  TrigRoot        = FDBStruct->AddChild("triggers");
  GenRoot         = FDBStruct->AddChild("generators");
  ExceptRoot      = FDBStruct->AddChild("exceptions");
  IndRoot         = FDBStruct->AddChild("indices");
  ExRoot          = FDBStruct->AddChild("Execute");
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TSQLCreator::CreateRegStruct(TTagNode * ASrc, bool AExtInq)
 { // ������� - ��� ������� ������ �����
  RegDef = ASrc;
  UnicodeString UID, RCUnitTabName;
  RCUnitTabName = "";
  TTagNode * cl, *clFieldDefs, *tabDefNode, *keyDefNode;
  TList * tclList = new TList;
  TList * clList = new TList;
  TList * flList = new TList;
  try
   {
    // �������� ������ �������� ������� �����
    // TTagNode *UnitRoot = ASrc->GetChildByName("unit",true);
    if (!AExtInq)
     {
      // �������� ������ �������� ���������������
      TTagNode * ClRoot = ASrc->GetChildByName("classes", true);
      // �������� ������ �������� ���������� ���������������
      // TTagNode *tClRoot = ASrc->GetChildByName("treeclasses",true);
      // ��������� ������ ���������������
      UnicodeString tmp = "";
      UnicodeString tabName;
      if (ClRoot)
       {
        Iterate(ClRoot, getClassList, clList);
       }
      if ((ClRoot == NULL) /* &&(tClRoot == NULL) */)
       return "";
      // ������������ ������ ���������������
      for (int k = 0; k < clList->Count; k++)
       {
        // ������������ ������� ��������������
        cl                        = (TTagNode *)clList->Items[k];
        UID                       = cl->AV["uid"];
        tabName                   = "CLASS_" + UID;
        tabDefNode                = TabRoot->AddChild("table");
        tabDefNode->AV["name"]    = tabName;
        tabDefNode->AV["ID"]      = cl->AV["uid"];
        tabDefNode->AV["comment"] = cl->AV["name"];
        // ��������� �����
        GenRoot->AddChild("generator", "name=GEN_" + UID + "_ID");
        // ��������� ��������� ������ ���� �����
        CreateMCODEProc(ProcRoot->AddChild("PROCEDURE"), UID);
        // ��������� ��������� ������ ��������������
        if (!cl->CmpAV("ref", "unit,card"))
         CreateClassDataProc(cl, ProcRoot->AddChild("PROCEDURE"), UID);
        // �������� �������
        tabDefNode->AddChild("keys")->AddChild("key", "name=CODE,type=INTEGER, req=NOT NULL");
        CreateFieldDefs(cl, tabDefNode->AddChild("fields"));
        Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabName);
        if (!cl->CmpAV("ref", "unit,card"))
         CreateRegDepend(cl, ClRoot /* , UnitRoot */);
       }
     }
   }
  __finally
   {
    if (tclList)
     {
      delete tclList;
      tclList = NULL;
     }
    if (clList)
     {
      delete clList;
      clList = NULL;
     }
    if (flList)
     {
      delete flList;
      flList = NULL;
     }
   }
  return RCUnitTabName;
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateRegDepend(TTagNode * AClDef, TTagNode * AClRoot /* , TTagNode *AUnitRoot */)
 {
  TStringList * bfUIDList = new TStringList;
  // ������ UID-�� ����������� �� �������� ������ "UID�����."="UID����. obj = 0 - class; 1 - unit"
  TStringList * afUIDList = new TStringList;
  // ������ UID-�� ������� ���������� ������� ����� �������� "UID�����."="UID����."
  TTagNode * trNode;
  try
   {
    // �������� ������� ������������
    UnicodeString UID, tabName, exName, exText, tmp, bodyText;
    UID     = AClDef->AV["uid"];
    tabName = "CLASS_" + UID;
    afUIDList->Clear();
    GetReference(UID, AClRoot, bfUIDList, afUIDList);
    if (bfUIDList && (bfUIDList->Count > 0))
     { // ���� �������������� � ������� ���������� ��������� ������� ������
      // �� ������������� � "uid" = UID
      for (int i = 0; i < bfUIDList->Count; i++)
       {
        // ������� ����������
        exName = "DEL_" + tabName + "_" + IntToStr(i);
        if (((int)bfUIDList->Objects[i]) == 0)
         exText = AClRoot->GetTagByUID(bfUIDList->Names[i])->AV["name"];
        exText = "�������� �� ��������. <" + exText + ">";
        if (exText.Length() > 80)
         exText = exText.SubString(1, 80);
        ExceptRoot->AddChild("EXCEPTION", "name=" + exName + ",text=" + exText);
        // ������� �������
        trNode                 = TrigRoot->AddChild("TRIGGER", "name=" + tabName + "_BD" + IntToStr(i));
        trNode->AV["tabname"]  = tabName;
        trNode->AV["position"] = "0";
        trNode->AV["type"]     = "BEFORE DELETE";
        trNode->AddChild("vars")->AddChild("var", "name=MCOUNT,type=INTEGER");
        if (((int)bfUIDList->Objects[i]) == 0)
         tmp = "CLASS_" + bfUIDList->Names[i];
        if (RegDef->GetTagByUID(bfUIDList->Values[bfUIDList->Names[i]])->AV["fl"].Length())
         exText = RegDef->GetTagByUID(bfUIDList->Values[bfUIDList->Names[i]])->AV["fl"];
        else
         exText = "R" + bfUIDList->Values[bfUIDList->Names[i]];
        bodyText = "SELECT COUNT(" + tmp + ".CODE) FROM " + tmp + " WHERE " + tmp + "." + exText +
          "=old.CODE INTO :MCOUNT;";
        bodyText += "  IF (MCOUNT > 0) THEN\n   BEGIN";
        bodyText += "    EXCEPTION " + exName + ";";
        bodyText += "   END";
        trNode->AddChild("body")->AV["pcdata"] = bodyText;
       }
     }
    if (afUIDList->Count > 0)
     { // ���� �������������� �� ������� ���������� ������� �������
      // �� �������� �� ������������� � "uid" = UID
      // ������� �������
      trNode                 = TrigRoot->AddChild("TRIGGER", "name=" + tabName + "_AD0");
      trNode->AV["tabname"]  = tabName;
      trNode->AV["position"] = "0";
      trNode->AV["type"]     = "AFTER DELETE";
      bodyText               = "";
      for (int i = 0; i < afUIDList->Count; i++)
       {
        tmp = "CLASS_" + afUIDList->Names[i];
        if (RegDef->GetTagByUID(afUIDList->Values[afUIDList->Names[i]])->AV["fl"].Length())
         exText = RegDef->GetTagByUID(afUIDList->Values[afUIDList->Names[i]])->AV["fl"];
        else
         exText = "R" + afUIDList->Values[afUIDList->Names[i]];
        bodyText += "DELETE FROM " + tmp + " WHERE " + tmp + "." + exText + "=old.CODE;";
       }
      trNode->AddChild("body")->AV["pcdata"] = bodyText;
     }
   }
  __finally
   {
    if (bfUIDList)
     delete bfUIDList;
    if (afUIDList)
     delete afUIDList;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateFieldDefs(TTagNode * ACLDef, TTagNode * ADestDef)
 {
  TList * flList = new TList;
  try
   {
    TTagNode * flDef, *flDestDef, *idxDef;
    flList->Clear();
    Iterate(ACLDef, getFieldDef, flList);
    for (int i = 0; i < flList->Count; i++)
     {
      flDef                 = (TTagNode *)flList->Items[i];
      flDestDef             = ADestDef->AddChild("field");
      flDestDef->AV["name"] = "R" + flDef->AV["uid"];
      if (flDef->AV["fl"].Length())
       {
        flDestDef->AV["name"] = flDef->AV["fl"];
        if (flDef->CmpAV("fl", "CGUID"))
         {
          idxDef = IndRoot->AddChild("index", "name=CLASS_" + ACLDef->AV["uid"] + "CGUID, tabname=CLASS_" +
            ACLDef->AV["uid"]);
          idxDef->AddChild("field", "name=CGUID");
         }
       }
      flDestDef->AV["type"] = GetFlType(flDef);
      if ((flDef->CmpAV("required", "yes")) && (!flDef->GetChildByName("actuals")))
       flDestDef->AV["req"] = "NOT NULL";
      flDestDef->AV["comment"] = flDef->AV["name"];
      if (flDef->CmpName("choiceTree,choiceDB") && ACLDef)
       {
        if (ACLDef->CmpName("unit"))
         if (ACLDef->AV["tblname"].Length())
          {
           idxDef = IndRoot->AddChild("index", "name=" + ACLDef->AV["tblname"] + flDef->AV["uid"] + ",tabname=" +
             ACLDef->AV["tblname"]);
           idxDef->AddChild("field", "name=CODE");
           if (flDef->AV["fl"].Length())
            idxDef->AddChild("field", "name=" + flDef->AV["fl"]);
           else
            idxDef->AddChild("field", "name=R" + flDef->AV["uid"]);
          }
       }
     }
   }
  __finally
   {
    if (flList)
     delete flList;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateMCODEProc(TTagNode * ASPDef, UnicodeString ATabPref)
 {
  ASPDef->AV["name"] = "MCODE_" + ATabPref;
  ASPDef->AddChild("RETURNS")->AddChild("out", "name=MCODE,type=INTEGER");
  ASPDef->AddChild("body")->AV["pcdata"] = "MCODE=gen_id(GEN_" + ATabPref + "_ID,1);\nsuspend;";
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateClassDataProc(TTagNode * AClDef, TTagNode * ASPDef, UnicodeString ATabPref)
 {
  TList * flList = new TList;
  TList * clFrom = new TList;
  try
   {
    TTagNode * spIn, *spOut, *spVar, *spBody;
    UnicodeString ex = "";
    UnicodeString chvar = "";
    UnicodeString sel = "SELECT ";
    UnicodeString into = "INTO ";
    ASPDef->AV["name"] = "CLASSDATA_" + ATabPref;
    spIn               = ASPDef->AddChild("input");
    spIn->AddChild("in", "name=CL_CODE,type=INTEGER");
    spOut = ASPDef->AddChild("RETURNS");
    spOut->AddChild("out", "name=STR_CLASS,type=VARCHAR(255)");
    spVar  = ASPDef->AddChild("vars");
    spBody = ASPDef->AddChild("body");
    flList->Clear();
    Iterate(AClDef, getClFieldDef, flList);
    // ������������ ����� ��� RETURNS, SELECT, INTO
    clFrom->Add(flList->Items[0]);
    UnicodeString _uid = ((TTagNode *)flList->Items[0])->AV["uid"];
    TTagNode * _i, *n;
    UnicodeString _UID_, _FlType_, _pUID_;
    int xlen;
    for (int s = 1; s < flList->Count; s++)
     {
      _i       = (TTagNode *)flList->Items[s]; // 0
      _UID_    = _i->AV["uid"]; // 1
      _FlType_ = GetFlType(_i); // 2
      _pUID_   = _i->GetParent("class")->AV["uid"]; // 3
      // ������������ �������� ��� choice
      xlen = 0; // max ����� �������� "choice"-a
      bool isChoice = _i->CmpName("choice"); // ��� ���� choice ?
      bool lastF = ((s + 1) == flList->Count); // ���� ��������� ?
      if (isChoice)
       {
        spVar->AddChild("VARIABLE", "name=PV_" + _UID_ + ",type=INTEGER");
        chvar += "IF (PV_" + _UID_ + " IS NULL) THEN\n BEGIN P_" + _UID_ + " = ''; END\nELSE\n BEGIN\n";
        n = _i->GetFirstChild();
        UnicodeString _Name, _Val;
        while (n)
         {
          if (n->CmpName("choicevalue"))
           {
            _Name = n->AV["name"];
            _Val  = n->AV["value"];
            if (xlen < _Name.Length())
             xlen = _Name.Length();
            chvar += "  IF (PV_" + _UID_ + "=" + _Val + ") THEN\n    BEGIN P_" + _UID_ + " = '" + _Name + "'; END\n";
           }
          n = n->GetNext();
         }
        chvar += " END\n";
       }
      // ���� � SELECT
      if (RegDef->GetTagByUID(_UID_)->AV["fl"].Length())
       sel += "CLASS_" + _pUID_ + "." + RegDef->GetTagByUID(_UID_)->AV["fl"] + ((lastF) ? " \n" : " , ");
      else
       sel += "CLASS_" + _pUID_ + ".R" + _UID_ + ((lastF) ? " \n" : " , ");
      // ���� � INTO
      into += " :P" + UnicodeString((isChoice) ? "V" : "") + "_" + _UID_ + ((lastF) ? " \n" : " , ");
      // ���� � RETURNS
      spOut->AddChild("out", "name=P_" + _UID_)->AV["type"] =
        UnicodeString((isChoice) ? UnicodeString("VARCHAR(" + IntToStr(xlen) + ")") : _FlType_);
      if (_i->CmpName("class"))
       clFrom->Add(_i); // ������ ���������������
     }
    ex += "STR_CLASS='';\n";
    // ������������ FROM
    _i    = (TTagNode *)clFrom->Items[0]; // 0
    _UID_ = _i->AV["uid"]; // 1
    UnicodeString xfrom = "FROM CLASS_" + _UID_ + " ";
    if (clFrom->Count == 1)
     xfrom += "WHERE CLASS_" + _UID_ + ".CODE=:CL_CODE\n";
    else
     {
      for (int j = 1; j < clFrom->Count; j++)
       {
        _i       = (TTagNode *)clFrom->Items[j]; // 0
        _UID_    = _i->AV["uid"]; // 1
        _FlType_ = GetFlType(_i); // 2
        _pUID_   = _i->GetParent()->AV["uid"]; // 3
        if (RegDef->GetTagByUID(_UID_)->AV["fl"].Length())
         {
          if (j == 1)
           xfrom += "JOIN CLASS_" + _UID_ + " on (CLASS_" + _UID_ + ".CODE=CLASS_" + _pUID_ + "." +
             RegDef->GetTagByUID(_UID_)->AV["fl"] + " AND CLASS_" + _pUID_ + ".CODE=:CL_CODE)\n";
          else
           xfrom += "   LEFT JOIN CLASS_" + _UID_ + " on (CLASS_" + _UID_ + ".CODE=CLASS_" + _pUID_ + "." +
             RegDef->GetTagByUID(_UID_)->AV["fl"] + ")\n";
         }
        else
         {
          if (j == 1)
           xfrom += "JOIN CLASS_" + _UID_ + " on (CLASS_" + _UID_ + ".CODE=CLASS_" + _pUID_ + ".R" + _UID_ +
             " AND CLASS_" + _pUID_ + ".CODE=:CL_CODE)\n";
          else
           xfrom += "   LEFT JOIN CLASS_" + _UID_ + " on (CLASS_" + _UID_ + ".CODE=CLASS_" + _pUID_ + ".R" +
             _UID_ + ")\n";
         }
       }
     }
    ex += "\n" + sel + "\n" + xfrom + "\n" + into + ";\n" + chvar;
    // ������������ STR_CLASS - ��������� ������������� ��������������
    TTagNode * x;
    for (int j = clFrom->Count - 1; j >= 0; j--)
     {
      _i = ((TTagNode *)clFrom->Items[j])->GetChildByName("description");
      _i = _i->GetFirstChild()->GetFirstChild();
      while (_i)
       {
        n = _i->GetTagByUID(_i->AV["ref"]);
        UnicodeString xname = "'";
        if (n)
         {
          if (fscFieldName)
           xname = n->AV["name"] + " '";
          else
           xname = "'";
          if (n->CmpName("text"))
           xname += "||P_" + n->AV["uid"] + " ";
          else if (n->CmpName("datetime"))
           xname += "||CAST(P_" + n->AV["uid"] + " AS VARCHAR(19)) ";
          else if (n->CmpName("time"))
           xname += "||CAST(P_" + n->AV["uid"] + " AS VARCHAR(8)) ";
          else
           xname += "||CAST(P_" + n->AV["uid"] + " AS VARCHAR(10)) ";
         }
        ex += "    IF (P_" + _i->AV["ref"] + " IS NOT NULL) THEN \n     BEGIN STR_CLASS = STR_CLASS||' " + xname +
          "; END\n";
        _i = _i->GetNext();
       }
     }
    ex += "suspend;";
    // �������� �������� ��������� � PCDATA
    spBody->AV["pcdata"] = ex;
   }
  __finally
   {
    if (flList)
     delete flList;
    if (clFrom)
     delete clFrom;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getFieldDef(TTagNode * itTag, TList * ADest)
 { // --- ������������ ����� ������� ---------------------------------------------------------
  UnicodeString UID, REQ, __tmp;
  if (itTag->CmpName("text,binary,date,datetime,time,digit,choice,extedit,choiceTree,choiceDB"))
   {
    ADest->Add((void *)itTag);
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getClFieldDef(TTagNode * itTag, TList * ADest)
 { // --- ������������ ����� ������� ---------------------------------------------------------
  UnicodeString UID, REQ, __tmp;
  if (itTag->CmpName("class,text,binary,date,datetime,time,digit,choice,extedit,choiceTree,choiceDB"))
   {
    ADest->Add((void *)itTag);
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TSQLCreator::Iterate(TTagNode * ASrcNode, TSLIterate ItItem, TList * ADest)
 {
  if (!ASrcNode)
   throw ETagNodeError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  TTagNode * SaveThis = ASrcNode;
  if (ItItem(ASrcNode, ADest))
   return true;
  if (SaveThis != ASrcNode)
   return false;
  TTagNode * ItNode = ASrcNode->GetFirstChild();
  while (ItNode)
   {
    SaveThis = ItNode->GetNext();
    if (Iterate(ItNode, ItItem, ADest))
     return true;
    ItNode = SaveThis;
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getClassList(TTagNode * itTag, TList * ADest)
 {
  if (itTag->CmpName("class"))
   ADest->Add((void *)itTag);
  return false;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TSQLCreator::GetFlType(TTagNode * Src, bool AFull)
 {
  if (Src->CmpName("class,choiceDB,choiceTree"))
   return "INTEGER";
  else if (Src->CmpName("binary,choice"))
   return "SMALLINT";
  else if (Src->CmpName("date,time"))
   return Src->Name.UpperCase();
  else if (Src->CmpName("datetime"))
   return "TIMESTAMP";
  else if (Src->CmpName("text"))
   return (AFull) ? UnicodeString("VARCHAR(" + Src->AV["length"] + ")") : UnicodeString("VARCHAR");
  else if (Src->CmpName("extedit"))
   {
    if (Src->CmpAV("fieldtype", "char,varchar") || (!Src->AV["fieldtype"].Trim().Length()))
     {
      if (Src->AV["length"].ToIntDef(0))
       {
        if (Src->AV["fieldtype"].Trim().Length())
         return (AFull) ? UnicodeString(Src->AV["fieldtype"].UpperCase() + "(" + Src->AV["length"] + ")") :
           Src->AV["fieldtype"].UpperCase();
        else
         return (AFull) ? UnicodeString("VARCHAR(" + Src->AV["length"] + ")") : Src->AV["fieldtype"].UpperCase();
       }
      else
       {
        ShowMessage("��� ���� '" + Src->AV["name"] + "' �� ������ ������ ����, ��� ������ '" +
          Src->AV["fieldtype"] + "'.");
        return "INTEGER";
       }
     }
    else
     return Src->AV["fieldtype"].UpperCase();
   }
  else if (Src->CmpName("digit"))
   {
    int dec = 0;
    int dig = StrToInt(Src->AV["digits"]);
    if (Src->GetAttrByName("decimal"))
     if (Src->AV["decimal"].Length())
      dec = StrToInt(Src->AV["decimal"]);
    if (dec == 0)
     return (dig < 5) ? "SMALLINT" : "INTEGER";
    else
     return (AFull) ? UnicodeString("DECIMAL(" + Src->AV["digits"] + "," + Src->AV["decimal"] + ")") :
       UnicodeString("DECIMAL");
   }
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::GetReference(UnicodeString UID, TTagNode * AClRoot,
  /* TTagNode *AURoot, */ TStringList * AbfUIDList, TStringList * AafUIDList)
 {
  TTagNode * _cl = AClRoot->GetTagByUID(UID);
  TTagNode * _tmp, *_tmpCl;
  UnicodeString clRef;
  FrRef = NULL;
  try
   {
    if (_cl)
     {
      if (AbfUIDList && _cl->GetParent()->CmpName("class"))
       AbfUIDList->AddObject((_cl->GetParent()->AV["uid"] + "=" + UID).c_str(), (TObject *)0);
      FrRef = new TStringList;
      // ��������� ������� ������ �� ������ ���������������
      clRef = UID;
      FrRef->Clear();
      AClRoot->Iterate(GetChBD, clRef);
      if (FrRef->Count > 0)
       { // ������ ����
        for (int i = 0; i < FrRef->Count; i++)
         {
          _tmpCl = AClRoot->GetTagByUID(FrRef->Strings[i])->GetParent("class");
          _tmp   = _tmpCl->GetChildByName("description")->GetNext();
          bool AllChBD = true;
          while (_tmp && AllChBD)
           {
            AllChBD &= _tmp->CmpName("choiceDB");
            _tmp = _tmp->GetNext();
           }
          if (AllChBD)
           { // ��� ���� "choiceDB" - ����� ������� + ������� �� �����. �� �������� ���� ������
            // �.�. ���� �������� �������������� ���������������
            AafUIDList->AddObject((_tmpCl->AV["uid"] + "=" + FrRef->Strings[i]).c_str(), (TObject *)0);
           }
          else
           {
            AbfUIDList->AddObject((_tmpCl->AV["uid"] + "=" + FrRef->Strings[i]).c_str(), (TObject *)0);
           }
         }
       }
      // ��������� ������� ������ �� ������� ������ �����
      FrRef->Clear();
     }
    else
     {
      ShowMessage("������ �������� ������� ������ �� �������������");
      AbfUIDList->Clear();
      AafUIDList->Clear();
     }
   }
  __finally
   {
    if (FrRef)
     {
      delete FrRef;
      FrRef = NULL;
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TSQLCreator::GetChBD(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag->CmpName("choiceDB"))
   {
    if (itTag->AV["ref"] == UID)
     FrRef->Add(itTag->AV["uid"]);
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::Create_SYSDATA(TTagNode * AflNode, UnicodeString ATblName)
 {
  TTagNode * trNode;
  UnicodeString bodyText;
  AflNode->AddChild("field", "name=INSERT_DT,type=TIMESTAMP,req=NOT NULL");
  AflNode->AddChild("field", "name=UPDATE_DT,type=TIMESTAMP,req=NOT NULL");
  trNode                 = TrigRoot->AddChild("TRIGGER", "name=" + ATblName + "_BI5");
  trNode->AV["tabname"]  = ATblName;
  trNode->AV["position"] = "0";
  trNode->AV["type"]     = "BEFORE INSERT";
  bodyText               = "new.INSERT_DT=current_timestamp;\n";
  bodyText += "new.UPDATE_DT=current_timestamp;";
  trNode->AddChild("body")->AV["pcdata"] = bodyText;
  trNode                 = TrigRoot->AddChild("TRIGGER", "name=" + ATblName + "_BU5");
  trNode->AV["tabname"]  = ATblName;
  trNode->AV["position"] = "0";
  trNode->AV["type"]     = "BEFORE UPDATE";
  trNode->AddChild("body")->AV["pcdata"] = "new.UPDATE_DT=current_timestamp;";
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::Create_Script()
 {
  TStringList * FScr = new TStringList;
  try
   {
    if (FDBStruct)
     {
      FCreate_Script(FScr);
      UnicodeString SQLScriptName = AllXMLPath + "\\SQLScript.sql";
      FScr->SaveToFile(SQLScriptName);
      ShowMessage(("������ �������� � \"" + SQLScriptName + "\"").c_str());
 //     WinExec((L"\"C:\\Program Files\\HK-Software\\IB Expert\\IBExpert.exe\"  \"" + SQLScriptName + "\"").c_str(),        SW_SHOW);
     }
   }
  __finally
   {
    if (FScr)
     delete FScr;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::FCreate_Script(TStringList * AScript)
 {
  if (FDBStruct->CmpName("FDBStruct"))
   {
    CreateTablesScript(FDBStruct->GetChildByName("tables"), AScript);
    CreateIndicesScript(FDBStruct->GetChildByName("indices"), AScript);
    CreateGeneratorsScript(FDBStruct->GetChildByName("generators"), AScript);
    CreateExceptionsScript(FDBStruct->GetChildByName("exceptions"), AScript);
    CreateTriggersScript(FDBStruct->GetChildByName("triggers"), AScript);
    CreateProceduresScript(FDBStruct->GetChildByName("procedures"), AScript);
    CreateExecutesScript(FDBStruct->GetChildByName("Execute"), AScript);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTablesScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      if (itNode->CmpName("table"))
       CreateTableScript(itNode, AScript);
      itNode = itNode->GetNext();
     }
    AScript->Add("commit;");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTableScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode)
   {
    UnicodeString FPKey;
    TTagNode * FKeys, *FFields, *itNode, *FLastChild;
    FKeys      = ADefNode->GetChildByName("keys");
    FFields    = ADefNode->GetChildByName("fields");
    FLastChild = NULL;
    if (FKeys)
     FLastChild = FKeys->GetLastChild();
    else if (FFields && !FLastChild)
     FLastChild = FFields->GetLastChild();
    if (FLastChild)
     {
      AScript->Add(("/*" + ADefNode->AV["comment"] + "*/").c_str());
      AScript->Add(("Create table " + ADefNode->AV["name"]).c_str());
      AScript->Add("(");
      // ������������ ������ �����
      itNode = NULL;
      if (FKeys)
       itNode = FKeys->GetFirstChild();
      while (itNode)
       {
        AScript->Add("/* key field */");
        AScript->Add((itNode->AV["name"] + " " + itNode->AV["type"] + " " + itNode->AV["req"] + ",").c_str());
        itNode = itNode->GetNext();
       }
      itNode = NULL;
      if (FFields)
       itNode = FFields->GetFirstChild();
      while (itNode)
       {
        AScript->Add(("/* " + itNode->AV["comment"] + " */").c_str());
        if (itNode == FLastChild)
         AScript->Add((itNode->AV["name"] + " " + itNode->AV["type"] + " " + itNode->AV["req"]).c_str());
        else
         AScript->Add((itNode->AV["name"] + " " + itNode->AV["type"] + " " + itNode->AV["req"] + ",").c_str());
        itNode = itNode->GetNext();
       }
      if (FKeys)
       {
        FPKey  = "PRIMARY KEY (";
        itNode = FKeys->GetFirstChild();
        while (itNode)
         {
          if (itNode == FLastChild)
           FPKey += itNode->AV["name"];
          else
           FPKey += itNode->AV["name"] + ", ";
          itNode = itNode->GetNext();
         }
        FPKey += ")";
        AScript->Add(FPKey.c_str());
       }
      AScript->Add(");");
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateIndicesScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    UnicodeString FIdxBody;
    TTagNode * itNode, *FLastChild;
    TTagNode * itIdxNode = ADefNode->GetFirstChild();
    while (itIdxNode)
     {
      FIdxBody   = "CREATE INDEX " + itIdxNode->AV["name"] + " ON " + itIdxNode->AV["tabname"] + " (";
      FLastChild = itIdxNode->GetLastChild();
      itNode     = itIdxNode->GetFirstChild();
      while (itNode)
       {
        if (itNode != FLastChild)
         FIdxBody += itNode->AV["name"] + ",";
        else
         FIdxBody += itNode->AV["name"];
        itNode = itNode->GetNext();
       }
      FIdxBody += ");";
      AScript->Add(FIdxBody.c_str());
      itIdxNode = itIdxNode->GetNext();
     }
    AScript->Add("commit;");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateGeneratorsScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      AScript->Add(("CREATE GENERATOR " + itNode->AV["name"] + ";").c_str());
      itNode = itNode->GetNext();
     }
    AScript->Add("commit;");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateExceptionsScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      AScript->Add(("CREATE EXCEPTION " + itNode->AV["name"] + " '" + itNode->AV["text"] + "';").c_str());
      itNode = itNode->GetNext();
     }
    AScript->Add("commit;");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTriggersScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    AScript->Add("SET AUTODDL OFF;");
    AScript->Add("SET TERM ^ ;");
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      AScript->Add(("CREATE TRIGGER " + itNode->AV["name"] + " FOR " + itNode->AV["tabname"]).c_str());
      AScript->Add(("ACTIVE " + itNode->AV["type"] + " POSITION " + itNode->AV["position"]).c_str());
      AScript->Add("AS");
      CreateVarsList(itNode->GetChildByName("vars"), AScript);
      AScript->Add("BEGIN");
      AScript->Add(itNode->GetChildByName("body")->AV["pcdata"].c_str());
      AScript->Add("END^");
      AScript->Add("");
      itNode = itNode->GetNext();
     }
    AScript->Add("SET TERM ; ^");
    AScript->Add("commit;");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateVarsList(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      AScript->Add(("DECLARE VARIABLE " + itNode->AV["name"] + " " + itNode->AV["type"] + ";").c_str());
      itNode = itNode->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateFieldList(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    TTagNode * FLastChild = ADefNode->GetLastChild();
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      if (itNode != FLastChild)
       AScript->Add((itNode->AV["name"] + " " + itNode->AV["type"] + ",").c_str());
      else
       AScript->Add((itNode->AV["name"] + " " + itNode->AV["type"]).c_str());
      itNode = itNode->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateProceduresScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    AScript->Add("SET AUTODDL OFF;");
    AScript->Add("SET TERM ^ ;");
    TTagNode * itNode = ADefNode->GetFirstChild();
    while (itNode)
     {
      AScript->Add(("CREATE OR ALTER PROCEDURE  " + itNode->AV["name"]).c_str());
      CreateInputList(itNode->GetChildByName("input"), AScript);
      CreateOutputList(itNode->GetChildByName("RETURNS"), AScript);
      AScript->Add("AS");
      CreateVarsList(itNode->GetChildByName("vars"), AScript);
      AScript->Add("BEGIN");
      AScript->Add(itNode->GetChildByName("body")->AV["pcdata"].c_str());
      AScript->Add("END^");
      AScript->Add("");
      itNode = itNode->GetNext();
     }
    AScript->Add("SET TERM ; ^");
    AScript->Add("commit;");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateInputList(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    AScript->Add("(");
    CreateFieldList(ADefNode, AScript);
    AScript->Add(")");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateOutputList(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    AScript->Add("RETURNS");
    AScript->Add("(");
    CreateFieldList(ADefNode, AScript);
    AScript->Add(")");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateExecutesScript(TTagNode * ADefNode, TStringList * AScript)
 {
  if (ADefNode && ADefNode->Count)
   {
    TTagNode * itNode = ADefNode->GetFirstChild();
    AScript->Add("Set AutoDDL ON;");
    while (itNode)
     {
      if (itNode->CmpName("script"))
       AScript->Add(itNode->AV["pcdata"]);
      itNode = itNode->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
