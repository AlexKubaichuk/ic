//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("MainUnit.cpp", MainForm);
USEFORM("DMF.cpp", DM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
 try
 {
   Application->Initialize();
   Application->MainFormOnTaskBar = true;
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->CreateForm(__classid(TDM), &DM);
   Application->Run();
 }
 catch (System::Sysutils::Exception &exception)
 {
   Application->ShowException(&exception);
 }
 catch (...)
 {
   try
   {
    throw System::Sysutils::Exception("");
   }
   catch (System::Sysutils::Exception &exception)
   {
    Application->ShowException(&exception);
   }
 }
 return 0;
}
//---------------------------------------------------------------------------

