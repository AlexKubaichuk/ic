//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "DMF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDM *DM;
//---------------------------------------------------------------------------
__fastcall TDM::TDM(TComponent* Owner)
    : TDataModule(Owner)
{
  DataDef = NULL;
  XMLList = NULL;
  SQLProj = new TList;
}
//---------------------------------------------------------------------------
void __fastcall TDM::DMCreate(TObject *Sender)
{
//  InqXMLList = new TStringList;
  CurrentPath = GetCurrentDir();
}
//---------------------------------------------------------------------------
void __fastcall TDM::DMDestroy(TObject *Sender)
{
  if (SQLProj) delete SQLProj;
  if (XMLList) delete XMLList;
  if (DataDef) delete DataDef;
}
//---------------------------------------------------------------------------

