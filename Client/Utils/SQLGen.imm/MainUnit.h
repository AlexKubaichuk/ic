//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
/*
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include <Vcl.ComCtrls.hpp>
*/
class TMainForm : public TForm
{
__published:	// IDE-managed Components
 TLabel *Label1;
 TBevel *Bevel1;
 TLabel *Label3;
 TBevel *Bevel2;
 TBevel *Bevel3;
 TLabel *Label4;
 TLabel *Label5;
 TProgressBar *PrBar;
 TCheckBox *spGenChB;
 TCheckBox *FieldNameChB;
 TButton *ImmNomGenBtn;
 TEdit *NomGUIED;
 TCheckBox *TrigChB;
 TCheckBox *GenChB;
 TCheckBox *ExceptChB;
 TCheckBox *inqspGenChB;
 TCheckBox *inqTrigChB;
 TCheckBox *inqGenChB;
 TCheckBox *inqExceptChB;
 TCheckBox *IdxChB;
 TCheckBox *inqIdxChB;
 TButton *FuncCheckBtn;
 TMemo *FuncList;
 TButton *ImmNomEditBtn;
 TcxButtonEdit *ProjectBE;
 TButton *SQLScriptBtn;
 TButton *Button7;
 TOpenDialog *OpenDlg;

 void __fastcall ProjectBEPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall SQLScriptBtnClick(TObject *Sender);


private:	// User declarations
  TTagNode *FDBStruct;
  UnicodeString AllXMLPath;//,FGroupName,tmpGUI,xTabName;

//     TTagNode *idReg;
//     int    ssdl_oCount;
//     TStringList *tmpUID;
//     TTagNode  *iTag;
//  TTagNode  *NomTag;
//  TTagNode  *DomTag;
//     TTagNode  *ParentTag;
//  bool __fastcall SetOldNames(TTagNode *itTag, UnicodeString &AFake);
//  void __fastcall AddATTR(TTagNode *ANode, UnicodeString AName, UnicodeString AOtherDef);
//     bool __fastcall SetRegField(TTagNode *itTag, UnicodeString &TblName);
//  void __fastcall NomISGen(TTagNode *ARoot, UnicodeString TabName);
//     void __fastcall NomISUpdate(TTagNode *ARoot, UnicodeString TabName);
//     void __fastcall GetFuncList(TTagNode *ASrc, UnicodeString &AFuncList);
//     TAnsiStrMap FFuncList;

     //****************************
//     UnicodeString __fastcall GetFlType(TTagNode *Src);//---
//     UnicodeString __fastcall GetColType(TTagNode *Src, UnicodeString ATabName);
//     UnicodeString __fastcall GetRelType(TTagNode *Src, UnicodeString ATabName);
//     UnicodeString __fastcall AddChBD_ATTR(TTagNode *ADomTag,TTagNode *ASrc, UnicodeString ARefParent, UnicodeString AParentField, UnicodeString AParentTab);
//     UnicodeString __fastcall GetAttrType(TTagNode *Src);


public:		// User declarations
 __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
/*
//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include "DMF.h"
#include <Dialogs.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxMemo.hpp"
#include "cxRichEdit.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
        TProgressBar *PrBar;
        TLabel *Label1;
        TOpenDialog *OpenDlg;
        TCheckBox *spGenChB;
        TCheckBox *FieldNameChB;
        TButton *ImmNomGenBtn;
        TBevel *Bevel1;
        TEdit *NomGUIED;
        TLabel *Label3;
        TCheckBox *TrigChB;
        TCheckBox *GenChB;
        TCheckBox *ExceptChB;
        TCheckBox *inqspGenChB;
        TCheckBox *inqTrigChB;
        TCheckBox *inqGenChB;
        TCheckBox *inqExceptChB;
        TBevel *Bevel2;
        TBevel *Bevel3;
        TLabel *Label4;
        TLabel *Label5;
        TCheckBox *IdxChB;
        TCheckBox *inqIdxChB;
        TButton *FuncCheckBtn;
        TMemo *FuncList;
        TButton *ImmNomEditBtn;
        TcxButtonEdit *ProjectBE;
        TButton *SQLScriptBtn;
        TButton *Button7;
        void __fastcall ImmNomGenBtnClick(TObject *Sender);
        void __fastcall FuncCheckBtnClick(TObject *Sender);
        void __fastcall ImmNomEditBtnClick(TObject *Sender);
        void __fastcall ProjectBEPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
        void __fastcall SQLScriptBtnClick(TObject *Sender);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:
public:
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
*/
