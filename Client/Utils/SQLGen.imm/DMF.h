//---------------------------------------------------------------------------
#ifndef DMFH
#define DMFH
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/*
#include <Registry.hpp>
#include <Classes.hpp>
#include <DB.hpp>
#include <DBTables.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <msxmldom.hpp>
#include <XMLDoc.hpp>
#include <xmldom.hpp>
#include <XMLIntf.hpp>
*/
//#define quFN(a) DM->quFree->FN(a)
//#define K_REG "Software\\ICS Ltd.\\AKDO_V2"
#include "XMLContainer.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
class TDM : public TDataModule
{
__published:	// IDE-managed Components
    void __fastcall DMCreate(TObject *Sender);
    void __fastcall DMDestroy(TObject *Sender);
private:	// User declarations
    AnsiString    SaveOperAND,SQLSel,FamLike,FamOper,BDate;
    AnsiString    SaveOperOR,fltPath;
    bool          ofLast,ofAspon,ofCasebook,ofAddress,ofOrgType,ofOrg,ofRegions,ofStreets;
public:		// User declarations
    TAxeXMLContainer *XMLList;
    TTagNode *DataDef;
//    TStringList  *InqXMLList;
    TTagNode     *XMLProj;
    TList        *SQLProj;
//    TTagNode     *InquiryProj;
//    TTagNode     *RegPrj;
    AnsiString   Message,CurrentPath,ChoiceName,ProdName,ProdVersion,ProdAutor,LPUFullName,_EKGFL_,_EKGCHSS_,_EKGCHSSTAB_;
    unsigned char VerSion;
    int          ErrCode;
    bool         isFirst;
    __fastcall TDM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDM *DM;
//---------------------------------------------------------------------------
#endif

