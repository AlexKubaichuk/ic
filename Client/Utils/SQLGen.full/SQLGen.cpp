//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------
USEFORM("main.cpp", MainForm);
USEFORM("dmf.cpp", DM);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
 try
  {
        Application->Initialize();
        Application->Title = "��������� SQL-��������";
                 Application->HelpFile = "";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->CreateForm(__classid(TDM), &DM);
                 Application->Run();
  }
 catch (EAccessViolation &E)
  {
    Application->MessageBox(E.Message.c_str(),"������",MB_OK );
  }
 catch (EExternalException &E)
  {
    Application->MessageBox(E.Message.c_str(),"������",MB_OK );
  }
 catch (...)
  {
    Application->MessageBox("������ �������� ���������","������",MB_OK );
    Application->Terminate();
  }
      return 0;
}
//---------------------------------------------------------------------
