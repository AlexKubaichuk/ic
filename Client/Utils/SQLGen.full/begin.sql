SET AUTODDL ON;

SET TERM ^ ; 

create or alter PROCEDURE XDELALLPROC
AS
DECLARE VARIABLE PROC_NAME VARCHAR(31);
begin
  FOR Select RDB$PROCEDURE_NAME
      From RDB$PROCEDURES
      Where (RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null)
            and Upper(RDB$PROCEDURE_NAME) <> Upper('XDELALLPROC')
      INTO :PROC_NAME
  DO
    begin
      EXECUTE STATEMENT 'Drop procedure '||:PROC_NAME;
    end
end
^
SET TERM ; ^
commit;

GRANT EXECUTE ON PROCEDURE XDELALLPROC  TO SYSDBA;
commit;

EXECUTE PROCEDURE XDELALLPROC;
commit;

SET TERM ^ ; 

create or alter  PROCEDURE DELALLEXTFUNC
AS
DECLARE VARIABLE FUNCNAME VARCHAR(31);
begin
  FOR Select rdb$function_name
      From rdb$functions
      Where (RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null)
      INTO :FUNCNAME
  DO
    begin
      EXECUTE STATEMENT 'DROP EXTERNAL FUNCTION '||:FUNCNAME;
    end
end
^
SET TERM ; ^
commit;

GRANT EXECUTE ON PROCEDURE DELALLEXTFUNC TO SYSDBA;
commit;

EXECUTE PROCEDURE DELALLEXTFUNC;
commit;

DECLARE EXTERNAL FUNCTION UPCASE
    CSTRING(255)
    RETURNS CSTRING(255)
ENTRY_POINT 'UpCase' MODULE_NAME 'CASEUDF.DLL';

DECLARE EXTERNAL FUNCTION INC_PERIOD
    DATE,
    CSTRING(255),
    CSTRING(2),
    SMALLINT
    RETURNS DATE
    ENTRY_POINT 'IncPeriod' MODULE_NAME 'DTUDF.DLL';


DECLARE EXTERNAL FUNCTION ISDIGIT
    CSTRING(255)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'IsDigit' MODULE_NAME 'CASEUDF.DLL';


DECLARE EXTERNAL FUNCTION LOCASE
    CSTRING(255)
    RETURNS CSTRING(255)
    ENTRY_POINT 'LoCase' MODULE_NAME 'CASEUDF.DLL';


DECLARE EXTERNAL FUNCTION SUBSTR
    CSTRING(255),
    INTEGER,
    INTEGER
    RETURNS CSTRING(255)
    ENTRY_POINT 'SubStr' MODULE_NAME 'CASEUDF.DLL';
commit;

SET TERM ^ ;

create or alter  PROCEDURE CREATE_update_query_TABLE
AS
DECLARE VARIABLE TableExists integer;
begin
  Select Count (rdb$relation_name)
  From rdb$relations
  Where upcase(rdb$relation_name) = upcase('update_query')
  INTO :TableExists;
  if (TableExists = 0) then
  begin
   EXECUTE STATEMENT 'Create table update_query (F_TEXT varchar(255))';
  end
end
^
SET TERM ; ^
commit;

GRANT EXECUTE ON PROCEDURE CREATE_update_query_TABLE TO SYSDBA;
EXECUTE PROCEDURE CREATE_update_query_TABLE;

SET TERM ^ ;

create or alter  PROCEDURE CREATETABLE_IF_NOT_EXISTS (F_TABNAME varchar(31),
                                            F_CREATE_STATMENT varchar(255),
                                            F_UPDATE_STATMENT varchar(255))
AS
DECLARE VARIABLE TableExists integer;
begin
  Select Count (rdb$relation_name)
  From rdb$relations
  Where upcase(rdb$relation_name) = upcase(:F_TABNAME)
  INTO :TableExists;
  if (TableExists = 0) then
  begin
   EXECUTE STATEMENT 'Create table '||:F_TABNAME||' ('||:F_CREATE_STATMENT||')';
   if (F_UPDATE_STATMENT <> '') then
    insert into update_query values(' '||:F_UPDATE_STATMENT||'');
  end
end
^
SET TERM ; ^
commit;

GRANT EXECUTE ON PROCEDURE CREATETABLE_IF_NOT_EXISTS TO SYSDBA;

EXECUTE PROCEDURE CREATETABLE_IF_NOT_EXISTS('update_logs','F_TEXT varchar(255)','');
commit;
EXECUTE PROCEDURE CREATETABLE_IF_NOT_EXISTS('update_query','F_TEXT varchar(255)','');
commit;

delete from update_logs;
commit;

SET TERM ^ ;

create or alter  PROCEDURE DELALLINDEX
AS
DECLARE VARIABLE IDXNAME VARCHAR(31);
begin
  FOR Select RDB$INDEX_NAME
      From RDB$INDICES
      Where (RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null)
            and RDB$INDEX_NAME not like '%RDB%'
            and RDB$INDEX_NAME not like '%PK%'
      INTO :IDXNAME
  DO
    begin
      EXECUTE STATEMENT 'Drop Index '||:IDXNAME;
    end
end
^

create or alter PROCEDURE DELALLPROC
AS
DECLARE VARIABLE PROC_NAME VARCHAR(31);
begin
  FOR Select RDB$PROCEDURE_NAME
      From RDB$PROCEDURES
      Where (RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null)
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DELALLPROC')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DELALLINDEX')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DELALLTRIG')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DELALLEXCEPTION')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DELALLGEN')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('CREATETABLE_IF_NOT_EXISTS')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('CREATEFIELD_IF_NOT_EXISTS')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('CREATE_update_query_TABLE')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('EXEC_UPDATEQUERY')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DKUPDATEGENERATOR')
            and upcase(RDB$PROCEDURE_NAME) <> upcase('DROP_TABLE')
      INTO :PROC_NAME
  DO
    begin
      EXECUTE STATEMENT 'Drop procedure '||:PROC_NAME;
    end
end
^
create or alter  PROCEDURE DKUPDATEGENERATOR (
    PGENERATORNAME VARCHAR(31),
    PTABLENAME VARCHAR(31),
    PFIELDNAME VARCHAR(31))
AS
declare variable nFieldValue integer;
begin
execute statement 'select max(' || pFieldName || ') from ' || pTableName into :nFieldValue;
if (nFieldValue is null) then
begin
execute statement 'set generator ' || pGeneratorName || ' to 1';
end
else
begin
execute statement 'set generator ' || pGeneratorName || ' to ' || cast(nFieldValue as varchar(20));
end
end
^

create or alter PROCEDURE DELALLTRIG
AS
DECLARE VARIABLE TRIG_NAME VARCHAR(31);
begin
  FOR Select RDB$TRIGGER_NAME
      From RDB$TRIGGERS
      Where RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null
      INTO :TRIG_NAME
  DO
    begin
      EXECUTE STATEMENT 'Drop trigger '||:TRIG_NAME;
    end
end
^

create or alter PROCEDURE DELALLEXCEPTION
AS
DECLARE VARIABLE EXCEPT_NAME VARCHAR(31);
begin
  FOR Select rdb$exception_name
      From rdb$exceptions
      Where RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null
      INTO :EXCEPT_NAME
  DO
    begin
      EXECUTE STATEMENT 'Drop exception '||:EXCEPT_NAME;
    end
end
^

create or alter PROCEDURE DELALLGEN
AS
DECLARE VARIABLE GEN_NAME VARCHAR(31);
begin
  FOR Select rdb$generator_name
      From rdb$generators
      Where RDB$SYSTEM_FLAG <> 1 or RDB$SYSTEM_FLAG is null
      INTO :GEN_NAME
  DO
    begin
      EXECUTE STATEMENT 'Drop generator '||:GEN_NAME;
    end
end
^
create or alter PROCEDURE CREATEFIELD_IF_NOT_EXISTS (F_TABLENAME varchar(31),
                                            F_FIELDNAME varchar(31),
                                            F_FIELDTYPE varchar(31),
                                            F_FIELDLEGTH smallint,
                                            F_FIELDSCALE smallint,
                                            F_CREATE_STATMENT varchar(255),
                                            F_UPDATE_STATMENT varchar(255))
AS
DECLARE VARIABLE FieldExists    integer;
DECLARE VARIABLE f_field_length smallint;
DECLARE VARIABLE f_field_scale  smallint;
DECLARE VARIABLE f_field_type   smallint;
DECLARE VARIABLE f_field_prec   smallint;
begin
  Select Count (rdb$field_name)
  From rdb$relation_fields
  Where upcase(rdb$relation_name) = upcase(:F_TABLENAME) and upcase(rdb$field_name) = upcase(:F_FIELDNAME)
  INTO :FieldExists;
  if (FieldExists = 0) then
   begin
    EXECUTE STATEMENT 'Alter table '||:F_TABLENAME||' add '||:F_CREATE_STATMENT;
    if ((F_UPDATE_STATMENT <> '') and (F_UPDATE_STATMENT is not null)) then
     begin
      insert into update_query values(' '||:F_UPDATE_STATMENT||'');
     end
   end
  else
   begin
    Select SysFl.rdb$field_length, SysFl.rdb$field_scale, SysFl.rdb$field_type, SysFl.rdb$field_precision
    From rdb$fields SysFl join rdb$relation_fields SysRelFl
                          on (
                               SysFl.rdb$field_name = SysRelFl.rdb$field_source
                               and upcase(SysRelFl.rdb$relation_name) = upcase(:F_TABLENAME)
                               and upcase(SysRelFl.rdb$field_name) = upcase(:F_FIELDNAME)
                             )
    INTO :f_field_length, :f_field_scale, :f_field_type,:f_field_prec;
    if (upcase(F_FIELDTYPE) = 'CHAR') then
     begin
      if (not ((f_field_type = 14) and (f_field_length = F_FIELDLEGTH))) then
       insert into update_logs values('Error create field '||:F_TABLENAME||'.'||:F_FIELDNAME||' as '||:F_FIELDTYPE);
     end
    else if (upcase(F_FIELDTYPE) = 'VARCHAR') then
     begin
      if (not ((f_field_type = 37) and (f_field_length = F_FIELDLEGTH))) then
       insert into update_logs values('Error create field '||:F_TABLENAME||'.'||:F_FIELDNAME||' as '||:F_FIELDTYPE);
     end
    else if (upcase(F_FIELDTYPE) = 'INTEGER') then
     begin
      if (not ((f_field_type = 8) and (f_field_prec=0))) then
       insert into update_logs values('Error create field '||:F_TABLENAME||'.'||:F_FIELDNAME||' as '||:F_FIELDTYPE);
     end
    else if (upcase(F_FIELDTYPE) = 'SMALLINT') then
     begin
      if (f_field_type <> 7) then
       insert into update_logs values('Error create field '||:F_TABLENAME||'.'||:F_FIELDNAME||' as '||:F_FIELDTYPE);
     end
    else if (upcase(F_FIELDTYPE) = 'DATE') then
     begin
      if (f_field_type <> 12) then
       insert into update_logs values('Error create field '||:F_TABLENAME||'.'||:F_FIELDNAME||' as '||:F_FIELDTYPE);
     end
    else if (upcase(F_FIELDTYPE) = 'NUMERIC') then
     begin                
      if ( not ((f_field_scale=(-1*F_FIELDSCALE)) and (f_field_prec=F_FIELDLEGTH))) then
       insert into update_logs values('Error create field '||:F_TABLENAME||'.'||:F_FIELDNAME||' as '||:F_FIELDTYPE);
     end
   end
end
^
create or alter PROCEDURE EXEC_UPDATEQUERY
AS
DECLARE VARIABLE QUERYSTATEMENT VARCHAR(255);
begin
  FOR Select F_TEXT
      From update_query
      INTO :QUERYSTATEMENT
  DO
    begin
      EXECUTE STATEMENT :QUERYSTATEMENT;
    end
end
^


SET TERM ; ^
commit;

GRANT EXECUTE ON PROCEDURE EXEC_UPDATEQUERY TO SYSDBA;
GRANT EXECUTE ON PROCEDURE DELALLINDEX TO SYSDBA;
GRANT EXECUTE ON PROCEDURE DELALLPROC TO SYSDBA;
GRANT EXECUTE ON PROCEDURE DELALLTRIG TO SYSDBA;
GRANT EXECUTE ON PROCEDURE DELALLEXCEPTION TO SYSDBA;
GRANT EXECUTE ON PROCEDURE DELALLGEN TO SYSDBA;
GRANT EXECUTE ON PROCEDURE CREATEFIELD_IF_NOT_EXISTS TO SYSDBA;
GRANT EXECUTE ON PROCEDURE DKUPDATEGENERATOR TO SYSDBA;


EXECUTE PROCEDURE DELALLPROC;
commit;
EXECUTE PROCEDURE DELALLTRIG;
commit;
EXECUTE PROCEDURE DELALLINDEX;
commit;
EXECUTE PROCEDURE DELALLEXCEPTION;
commit;
EXECUTE PROCEDURE DELALLGEN;
commit;

DROP PROCEDURE DELALLINDEX;
DROP PROCEDURE DELALLPROC;
DROP PROCEDURE DELALLTRIG;
DROP PROCEDURE DELALLEXCEPTION;
DROP PROCEDURE DELALLGEN;

commit;
