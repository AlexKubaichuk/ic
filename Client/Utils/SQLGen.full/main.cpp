//---------------------------------------------------------------------------
#include <vcl.h>
//include <IniFiles.hpp>
#pragma hdrstop

#include "main.h"
#include "DMF.h"
#include "SQLCreator.h"
#include "SysUtils.hpp"
//#define itTag ((TTagNode*)uiTag)
//#define AV(N,attr) N->GetAV(attr)
//#define AVUp(N,attr) N->GetAV(attr).UpperCase()
#define itAV(a) ((TTagNode*)uiTag)->AV[a]
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxMemo"
#pragma link "cxRichEdit"
#pragma resource "*.dfm"
TMainForm *MainForm;
__fastcall TMainForm::TMainForm(TComponent* Owner)
    : TForm(Owner)
{
  FDBStruct = new TTagNode(NULL);
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::GetXMLCount(TTagNode *itTag, AnsiString &UID)
{
   if (itTag->CmpName("project_main"))
    {
      if (itTag->AV["objref"].Length() != 22)
       {
         ShowMessage("����������� ���� �������� ��������� �������");
         UID = "-1";
         return true;
       }
      else
       {
         ssdl_oCount++;
         UID = IntToStr(UID.ToInt()+1);
       }
      if (itTag->AV["rulesref"].Length() != 22)
       {
         ShowMessage("����������� ���� �������� �������� ������");
         UID = "-1";
         return true;
       }
      else
       UID = IntToStr(UID.ToInt()+1);
      if (itTag->AV["funcref"].Length() == 22)
       UID = IntToStr(UID.ToInt()+1);
    }
   else if (itTag->CmpName("sub_project"))
    {
      if (itTag->AV["objref"].Length() != 22)
       {
         ShowMessage(("����������� ���� �������� �������� ���\n'"+itTag->AV["name"]+"'").c_str());
         UID = "-1";
         return true;
       }
      else
       {
         ssdl_oCount++;
         UID = IntToStr(UID.ToInt()+1);
       }
      if (itTag->AV["module_type"].UpperCase()=="SSDL")
       {
         if (itTag->AV["rulesref"].Length() != 22)
          {
            ShowMessage(("����������� ���� �������� ������ ���\n'"+itTag->AV["name"]+"'").c_str());
            UID = "-1";
            return true;
          }
         else
          UID = IntToStr(UID.ToInt()+1);
       }
      else
       {
         if (itTag->AV["rulesref"].Length() == 22)
          UID = IntToStr(UID.ToInt()+1);
       }
    }
   else if (itTag->CmpName("extdef"))
    {
      UID = IntToStr(UID.ToInt()+1);
    }
   else if (itTag->CmpName("report,sub_report,ssdl_project,passport"))
    {
       ;
    }
   else
    {
      ShowMessage(("�� ���������� ��� �������� ������� \'"+itTag->Name+"\'").c_str());
      UID = "-1";
      return true;
    }
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::LoadXMLs(TTagNode *itTag, AnsiString &UID)
{
   if (itTag->CmpName("project_main"))
    {
      // �������� �������� �������� ��������� ������������
      LoadXML(itTag->AV["objref"]);
      LoadXML(itTag->AV["registryref"]);
    }
   else if (itTag->CmpName("sub_project"))
    {
      // �������� �������� �������� ��������������� ������������
      LoadXML(itTag->AV["objref"]);
    }
   else if (itTag->CmpName("extdef"))
    {
      LoadXML(itTag->AV["gui"]);
    }
   else if (itTag->CmpName("report,sub_report,ssdl_project,passport"))
    {
       ;
    }
   else
    {
      ShowMessage(("�� ���������� ��� �������� ������� \'"+itTag->Name+"\'").c_str());
      UID = "-1";
      return true;
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::LoadXML(AnsiString AName)
{
   PrBar->Position +=1;
   DM->XMLList->LoadXML(AllXMLPath+AName+".xml", AName);
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetScaleUID(TTagNode *quTag)
{
   if (quTag->GetAttrByName("scaleuid"))
    {
      if (quTag->AV["scaleuid"] == "") return GetScaleUID(quTag->GetParent());
      else                            return quTag->AV["scaleuid"];
    }
   return GetScaleUID(quTag->GetParent());
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NomGenBtnClick(TObject *Sender)
{
   try
    {
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
//      if ((RegFN == "")||(CardFN == "")) return;
// ------ �������� ������  -----------------------------------------------------
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ��������� ������������  ----------------------------------------------
      Label1->Caption = "��������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);  NomTag->Name = "mais_ontology";
      // passport
      DomTag = NomTag->AddChild("passport");
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      else                      Nom_GUIN = NomGUIED->Text;
      DomTag->AddAttr("GUI",Nom_GUIN);
      DomTag->AddAttr("mainname","����������� ����");
      DomTag->AddAttr("autor","Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19");
      DomTag->AddAttr("version","1");
      DomTag->AddAttr("timestamp",TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss"));
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->AddChild("ATTRS");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=������,ref=period,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=���� ������������,ref=createdate,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=�����,ref=autor,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ������������,ref=createtime,fields=no,procname=DocAttrVal");
      // IS  - TechIS
      _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->AddChild("ATTRS");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=������������ ���,fields=no,procname=SetLPUName");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ��������� ���������,fields=no,procname=SetAddr");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ����������� ����� ���,fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      PrName = itTMP->GetAV("name");
      DomTag = NomTag->AddChild("domain");
      DomTag->AddAttr("name","���������������");
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = GetXML(DM->XMLProj->GetChildByName("project_main")->AV["registryref"])->GetTagByUID("1000");
      NomISGen(_tmpTag, AVUp(_tmpTag,"tblname"));
      DomTag = _tmpDom;
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� �������� "�������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpTag = GetInquirers(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      FGroupName = DM->XMLProj->GetChildByName("project_main")->AV["name"];
      tmpGUI = DM->XMLProj->GetChildByName("project_main")->AV["objref"];
      SetInqField(_tmpTag);
      _tmpTag = GetOutData(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      SetOutField(_tmpTag);
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� ��������� "����������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpTag = DM->XMLProj->GetChildByName("project_main")->GetFirstChild();
      if (tmpTag)
       {// ���� ������ ��������������
         while (tmpTag)
          {
            if (tmpTag->CmpName("sub_project"))
             {
               if (tmpTag->GetAV("module_type")=="ssdl")
                {
                  FGroupName = tmpTag->AV["name"];
                  _tmpTag = GetInquirers(tmpTag->AV["objref"]);
                  tmpGUI = tmpTag->AV["objref"];
                  SetInqField(_tmpTag);
                  _tmpTag = GetOutData(tmpTag->AV["objref"]);
                  SetOutField(_tmpTag);
                }
             }
            tmpTag = tmpTag->GetNext();
          }
       }

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[i]);
         tmpRefTag->AddAttr("typerelation","11"); //1M
         tmpRefTag->AddAttr("keylink","and");
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CODE");
         tmpRefTag->AddAttr("fieldout","CD");
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[0]);
         tmpRefTag->AddAttr("typerelation","11"); // M1
         tmpRefTag->AddAttr("keylink","and");
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CD");
         tmpRefTag->AddAttr("fieldout","CODE");
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + "�����������.xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SQLGenBtnClick(TObject *Sender)
{
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
     Dlg->SQL_AKDO();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::SetRegField(TTagNode *itTag, AnsiString &TblName)
{
   TTagNode *__tmp;
   if (itTag->CmpName("unit,class"))
    {
      // ����� ��������
      __tmp = DomTag->GetChildByAV("IS","ref",tmpGUI+"."+AVUp(itTag,"uid"),true);
      if (!__tmp)
       {
         DomTag = DomTag->AddChild("IS");
         DomTag->AV["uid"] = DomTag->NewUID();
         DomTag->AV["ref"] = tmpGUI+"."+AVUp(itTag,"uid");
       }
      else DomTag = __tmp;
      DomTag->AV["name"] = itTag->GetFirstChild()->AV["name"];
      DomTag->AV["maintable"] = TblName;
      DomTag->AV["keyfield"] = "CODE";
      // ��������� ������ ���������
      __tmp = DomTag->GetChildByName("ATTRS");
      if (!__tmp) DomTag = DomTag->AddChild("ATTRS");
      else        DomTag = __tmp;
      // ��������� ������� ��� ���� ��������
      TTagNode *tmp;
      tmp = itTag->GetChildByName("description")->GetChildByName("fields")->GetFirstChild();

      __tmp = DomTag->GetChildByAV("ATTR","ref",tmpGUI+"."+tmp->AV["ref"].UpperCase());
      if (!__tmp)
       {
         __tmp = DomTag->AddChild("ATTR");
         __tmp->AV["uid"] = DomTag->NewUID();
         __tmp->AV["ref"] = tmpGUI+"."+tmp->AV["ref"].UpperCase();
       }
      else
       {
         if (!__tmp->CmpAV("fields","CODE"))
          {
            __tmp = DomTag->AddChild("ATTR");
            __tmp->AV["uid"] = DomTag->NewUID();
            __tmp->AV["ref"] = tmpGUI+"."+tmp->AV["ref"].UpperCase();
          }
       }
      __tmp->AV["name"] = itTag->GetChildByName("description")->AV["name"];
      __tmp->AV["fields"] = "CODE";
      __tmp->AV["column"] = GetColType(tmp->GetTagByUID(tmp->AV["ref"]),TblName);
      __tmp->AV["type"] = GetFlType(tmp->GetTagByUID(tmp->AV["ref"]));
      __tmp->AV["ref_parent"] = "";
      __tmp->AV["attrmode"] = "attr_idf";
    }
   else if (itTag->CmpName("text,binary,date,datetime,time,digit,choice,extedit,choiceTree"))
    {
      // ��������� �������� ��������� ��������
      // ����� �������� �� uid
      __tmp = DomTag->GetChildByAV("ATTR","ref",tmpGUI+"."+AVUp(itTag,"uid"));
      AnsiString FProcName = "";
      AnsiString FLName = itTag->AV["name"];
      if (itTag->CmpName("binary,choice,extedit,choiceTree"))
       FProcName = "ChoiceValue";

      if (!__tmp)
       { // ������� �� ������, ������ �����
         __tmp = DomTag->AddChild("ATTR");
         __tmp->AV["uid"] = DomTag->NewUID();
         __tmp->AV["ref"] = tmpGUI+"."+AVUp(itTag,"uid");
       }
      else
       { // ������� ������, ��������� ������������ � ��� ���������
         if (__tmp->CmpAV("fields","R"+itTag->AV["uid"]) && __tmp->CmpAV("procname",FProcName))
          {
            // ������ ����� � ������������ ��������� ��������� ���������
            if (!__tmp->CmpAV("name","**"+FLName))
             { // ������������ �� ���������
               FLName += " (# "+__tmp->AV["name"].SubString(3,__tmp->AV["name"].Length()-2)+" #)";
             }
          }
         else
          {
            __tmp = DomTag->AddChild("ATTR");
            __tmp->AV["uid"] = DomTag->NewUID();
            __tmp->AV["ref"] = tmpGUI+"."+AVUp(itTag,"uid");
            FLName = "��������� ������ "+FLName;
          }
       }
      __tmp->AV["name"] = FLName;
      __tmp->AV["fields"] = "R"+AVUp(itTag,"uid");
      __tmp->AV["procname"] = FProcName;
      __tmp->AV["column"] = GetColType(itTag,TblName);
      __tmp->AV["type"] = GetFlType(itTag);
      __tmp->AV["relation"] = GetRelType(itTag,TblName);
      __tmp->AV["ref_parent"] = "";
      __tmp->AV["attrmode"] = GetAttrType(itTag);
      tmpUID->Add((AVUp(itTag,"uid")+"="+AVUp(__tmp,"uid")).c_str());
    }
   else if (itTag->CmpName("choiceDB"))
    {
      tmpUID->Add((AVUp(itTag,"uid")+"="+AddChBD_ATTR(DomTag,itTag->GetTagByUID(itTag->AV["ref"]),"","R"+AVUp(itTag,"uid"),TblName)).c_str());
    }
   return false;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::AddChBD_ATTR(TTagNode *ADomTag,TTagNode *ASrc, AnsiString ARefParent, AnsiString AParentField, AnsiString AParentTab)
{
   TTagNode *__tmp;
   __tmp = ADomTag->GetChildByAV("ATTR","ref",tmpGUI+"."+AVUp(ASrc,"uid"));
   AnsiString _UID = ADomTag->NewUID();
   if (__tmp) _UID = __tmp->AV["uid"];
   if (ASrc->GetChildByName("class"))
    AddChBD_ATTR(ADomTag,ASrc->GetChildByName("class"), _UID, AParentField,"");
   TTagNode *tmp;
   tmp = ASrc->GetChildByName("description")->GetChildByName("fields")->GetFirstChild();
   tmp = tmp->GetTagByUID(tmp->AV["ref"]);
   AnsiString FProcName = "ChoiceValue";
   AnsiString FLName = ASrc->GetChildByName("description")->AV["name"];
   if (!__tmp)
    {
      __tmp = ADomTag->AddChild("ATTR");
      __tmp->AV["uid"] = _UID;
      __tmp->AV["ref"] = tmpGUI+"."+AVUp(ASrc,"uid");
    }
   else
    { // ������� ������, ��������� ������������ � ��� ���������
      if (__tmp->CmpAV("fields",AParentField) && __tmp->CmpAV("procname",FProcName))
       {
         // ������ ����� � ������������ ��������� ��������� ���������
         if (!__tmp->CmpAV("name","**"+FLName))
          { // ������������ �� ���������
            FLName += " (# "+__tmp->AV["name"].SubString(3,__tmp->AV["name"].Length()-2)+" #)";
          }
       }
      else
       {
         __tmp = ADomTag->AddChild("ATTR");
         __tmp->AV["uid"] = ADomTag->NewUID();
         __tmp->AV["ref"] = tmpGUI+"."+AVUp(ASrc,"uid");
         FLName = "��������� ������ "+FLName;
       }
    }
   __tmp->AV["name"] = FLName;
   __tmp->AV["fields"] = AParentField;
   __tmp->AV["procname"] = FProcName;
   __tmp->AV["column"] = "CLASS_"+ASrc->AV["uid"]+".R"+tmp->AV["uid"];
   __tmp->AV["type"] = GetFlType(tmp);
   if (ARefParent == "")
    { // ���������� ��� choiceDB
      __tmp->AV["relation"] = AParentTab+"."+AParentField+"=CLASS_"+ASrc->AV["uid"]+".CODE";
    }
   else
    { // ���������� ��� class
      __tmp->AV["relation"] = "CLASS_"+ASrc->GetParent("class")->AV["uid"]+".R"+ASrc->AV["uid"]+"=CLASS_"+ASrc->AV["uid"]+".CODE";
    }
   __tmp->AV["ref_parent"] = ARefParent;
   __tmp->AV["attrmode"] = "attr_descr";
   return _UID;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SetInqField(TTagNode *AInqNode)
{
  AnsiString atUID;
  TTagNode *tmpAttrs,*tmpRules;
  TTagNode *itNode = AInqNode->GetFirstChild();
  while (itNode)
   {
     xTabName = "INQ_"+GetXMLProjectCode(itNode)+itNode->AV["uid"].UpperCase();
     ParentTag = DomTag->GetChildByAV("IS","ref",tmpGUI+"."+itNode->AV["uid"].UpperCase());
     if (!ParentTag)
      {
        ParentTag = DomTag->AddChild("IS");
        ParentTag->AV["uid"] = DomTag->NewUID();
        ParentTag->AV["ref"] = tmpGUI+"."+itNode->AV["uid"].UpperCase();
      }
     ParentTag->AV["name"] = FGroupName+": "+itNode->AV["name"];
     ParentTag->AV["maintable"] = xTabName;
     ParentTag->AV["keyfield"] = "CD";
     atUID = DomTag->NewUID();
     tmpAttrs = ParentTag->GetChildByName("ATTRS");
     if (!tmpAttrs) tmpAttrs = ParentTag->AddChild("ATTRS");
     if (!tmpAttrs->GetChildByAV("ATTR","ref",tmpGUI))
      tmpAttrs->AddChild("ATTR","uid="+atUID+",name=���� ������������,ref="+tmpGUI+",fields=INQDATE");
     else
      atUID = tmpAttrs->GetChildByAV("ATTR","ref",tmpGUI)->AV["uid"];
     tmpRules = ParentTag->GetChildByName("rulesunit");
     if (!tmpRules) tmpRules = ParentTag->AddChild("rulesunit");
     if (!tmpRules->GetChildByAV("IE","name","���� ������������"))
      tmpRules->AddChild("IE","uid="+DomTag->NewUID()+",name=���� ������������,procname=DatePeriodLast,param="+atUID);
     FSetInqField(itNode, tmpAttrs, tmpRules);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FSetInqField(TTagNode *AInqNode,TTagNode *AAttrsNode, TTagNode *ARulesNode)
{
   AnsiString atFldName,atName,atUID;
   TTagNode *tmpGroup,*Rules;
   TTagNode *Attr,*itNode;
   itNode = AInqNode->GetFirstChild();
   while (itNode)
    {
      if (itNode->CmpName("group"))
       {
         tmpGroup = ARulesNode->GetChildByAV("IMOR","name",itNode->AV["name"]);
         if (!tmpGroup) tmpGroup = ARulesNode->AddChild("IMOR","uid="+DomTag->NewUID());
         tmpGroup->AV["name"] = itNode->AV["name"];
         FSetInqField(itNode, AAttrsNode, tmpGroup);
         if (tmpGroup->Count < 1) delete tmpGroup;
       }
      else if (itNode->CmpName("question"))
       {
          atUID = DomTag->NewUID();
          atName = itNode->AV["name"];
          atFldName = AnsiString("P"+GetXMLProjectCode(itNode)+itNode->AV["uid"]).UpperCase();
          Attr  = AAttrsNode->GetChildByAV("ATTR","ref",tmpGUI+"."+itNode->AV["uid"].UpperCase());
          if (!Attr)
           {
             Attr  = AAttrsNode->AddChild("ATTR");
             Attr->AV["uid"] = atUID;
             Attr->AV["ref"] = tmpGUI+"."+itNode->AV["uid"].UpperCase();
           }
          atUID = Attr->AV["uid"];
          if (itNode->GetParent()->CmpName("group"))
           Attr->AV["name"] = itNode->GetParent()->AV["name"]+": "+atName;
          else
           Attr->AV["name"] = atName;
          Attr->AV["fields"] = atFldName;
          Attr->AV["column"] = GetInqColType(itNode);
          Attr->AV["type"] = GetInqFlType(itNode);
          Attr->AV["ref_parent"] = "";
          Attr->AV["relation"] = GetInqRelType(itNode);
          Attr->AV["attrmode"] = GetInqAttrType(itNode);
          TTagNode *xSc = itNode->GetTagByUID(GetScaleUID(itNode));
          if (!xSc->CmpName("scale_text"))
           {
             if (xSc->CmpName("scale_binary,scale_select,scale_diapazon"))
              Attr->AV["procname"] = "InqChVal";
             Rules = ARulesNode->GetChildByAV("IE","name",atName);
             if (!Rules) Rules = ARulesNode->AddChild("IE","uid="+DomTag->NewUID());
             Rules->AV["name"] = atName;
             Rules->AV["procname"] = "InqValue";
             Rules->AV["param"] = atUID;
           }
       }
      itNode = itNode->GetNext();
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SetOutField(TTagNode *AOutNode)
{
  AnsiString atUID;
  TTagNode *tmpAttrs,*tmpRules;
  TTagNode *itNode = AOutNode->GetFirstChild();
  while (itNode)
   {
     xTabName = "OUT_"+GetXMLProjectCode(itNode)+itNode->AV["uid"].UpperCase();
     ParentTag = DomTag->GetChildByAV("IS","ref",tmpGUI+"."+itNode->AV["uid"].UpperCase());
     if (!ParentTag)
      {
        ParentTag = DomTag->AddChild("IS");
        ParentTag->AV["uid"] = DomTag->NewUID();
        ParentTag->AV["ref"] = tmpGUI+"."+itNode->AV["uid"].UpperCase();
      }
     ParentTag->AV["name"] = FGroupName+": "+itNode->AV["name"];
     ParentTag->AV["maintable"] = xTabName;
     ParentTag->AV["keyfield"] = "CD";
     atUID = DomTag->NewUID();
     tmpAttrs = ParentTag->GetChildByName("ATTRS");
     if (!tmpAttrs) tmpAttrs = ParentTag->AddChild("ATTRS");
     tmpRules = ParentTag->GetChildByName("rulesunit");
     if (!tmpRules) tmpRules = ParentTag->AddChild("rulesunit");
     if (!tmpAttrs->GetChildByAV("ATTR","ref",tmpGUI))
      tmpAttrs->AddChild("ATTR","uid="+atUID+",name=���� ������������,ref="+tmpGUI+",fields=INQDATE");
     else
      atUID = tmpAttrs->GetChildByAV("ATTR","ref",tmpGUI)->AV["uid"];
     if (!tmpRules->GetChildByAV("IE","name","���� ������������"))
      tmpRules->AddChild("IE","uid="+DomTag->NewUID()+",name=���� ������������,procname=DatePeriodLast,param="+atUID);
     FSetOutField(itNode, tmpAttrs, tmpRules);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FSetOutField(TTagNode *AOutNode,TTagNode *AAttrsNode, TTagNode *ARulesNode)
{
   AnsiString atFldName,atName,atUID;
   TTagNode *tmpGroup,*Rules;
   TTagNode *Attr,*itNode;
   itNode = AOutNode->GetFirstChild();
   while (itNode)
    {
      if (itNode->CmpName("outlist"))
       {
         tmpGroup = ARulesNode->GetChildByAV("IMOR","name",itNode->AV["name"]);
         if (!tmpGroup) tmpGroup = ARulesNode->AddChild("IMOR","uid="+DomTag->NewUID());
         tmpGroup->AV["name"] = itNode->AV["name"];
         FSetOutField(itNode, AAttrsNode, tmpGroup);
         if (tmpGroup->Count < 1) delete tmpGroup;
       }
      else if (itNode->CmpName("out"))
       {
          atUID = DomTag->NewUID();
          atName = itNode->AV["name"];
          atFldName = "P"+GetXMLProjectCode(itNode)+itNode->AV["uid"];
          Attr = AAttrsNode->GetChildByAV("ATTR","ref",tmpGUI+"."+itNode->AV["uid"].UpperCase());
          if (!Attr)
           {
             Attr = AAttrsNode->AddChild("ATTR","uid="+atUID+",ref="+tmpGUI+"."+itNode->AV["uid"].UpperCase());
           }
          atUID = Attr->AV["uid"];
          Attr->AV["fields"] = atFldName;
          if (itNode->GetParent()->GetParent()->CmpName("outlist"))
           Attr->AV["name"] = itNode->GetParent()->AV["name"]+": "+atName;
          else
           Attr->AV["name"] = atName;
          Attr->AV["column"] = GetInqColType(itNode);
          Attr->AV["procname"] = "InqChVal";
          Attr->AV["attrmode"] = "attr_valdescr";
          Attr->AV["type"] = "SMALLINT";
          Rules = ARulesNode->GetChildByAV("IE","name",atName);
          if (!Rules) Rules = ARulesNode->AddChild("IE","uid="+DomTag->NewUID());
          Rules->AV["name"] = atName;
          Rules->AV["param"] = atUID;
          Rules->AV["procname"] = "InqValue";
       }
      itNode = itNode->GetNext();
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::RegGenBtnClick(TObject *Sender)
{
  if (!OpenDlg->Execute()) return;
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = OpenDlg->FileName;
     Dlg->SQL_REG();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ImmGenBtnClick(TObject *Sender)
{
  if (!OpenDlg->Execute()) return;
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = OpenDlg->FileName;
     Dlg->SQL_IMM();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ImmNomGenBtnClick(TObject *Sender)
{
   try
    {
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
      OpenDlg->Title = "������� �������� ������������ �����������";
      if (OpenDlg->Execute())
       RegFN = OpenDlg->FileName;
      OpenDlg->Title = "������� �������� ����� �����������";
      if (OpenDlg->Execute())
       CardFN = OpenDlg->FileName;
      if ((RegFN == "")||(CardFN == "")) return;
// ------ �������� ������  -----------------------------------------------------
      ImmReg = new TTagNode(NULL);
      ImmCard = new TTagNode(NULL);
      ImmReg->LoadFromXMLFile(RegFN);
      ImmCard->LoadFromXMLFile(CardFN);
      AllXMLPath =  ExtractFilePath(CardFN);
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ��������� ������������  ----------------------------------------------
      Label1->Caption = "��������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);  NomTag->Name = "mais_ontology";
      // passport
      DomTag = NomTag->AddChild("passport");
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      else                      Nom_GUIN = NomGUIED->Text;
      DomTag->AddAttr("GUI",Nom_GUIN);
      DomTag->AddAttr("mainname","����������� �����������");
      DomTag->AddAttr("autor","Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 251-80-59");
      DomTag->AddAttr("version","1");
      DomTag->AddAttr("timestamp",TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss"));
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->AddChild("domain");
      DomTag->AddAttr("name","���������������");
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = ImmReg->GetTagByUID("1000");
      NomISGen(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
      // ��������� �������� ��������� �� ������� ���������������� �����
      _tmpTag = ImmCard->GetTagByUID("3003");
      while (_tmpTag)
       {
         DomTag = _tmpDom;
         NomISGen(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->AddChild("domain");
      DomTag->AddAttr("name","������������");
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = ImmReg->GetTagByUID("1000");
      NomISGen(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
      // ��������� �������� ��������� �� ������� ���������������� �����
      _tmpTag = ImmCard->GetChildByName("unit");
      while (_tmpTag)
       {
         DomTag = _tmpDom;
         NomISGen(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[i]);
         tmpRefTag->AddAttr("typerelation","11"); //1M
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CODE");
         tmpRefTag->AddAttr("fieldout","UCODE");
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[0]);
         tmpRefTag->AddAttr("typerelation","11"); //M1
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","UCODE");
         tmpRefTag->AddAttr("fieldout","CODE");
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      isUIDs->Clear();
      _tmpDom = _tmpDom->GetNext();
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         for (int i = 0; i < isUIDs->Count; i++)
          {
            if (isUIDs->Strings[i] != AVUp(_tmpTag,"uid"))
             {
               tmpRefTag = tmpTag->AddChild("refer");
               tmpRefTag->AddAttr("ref",isUIDs->Strings[i]);
               tmpRefTag->AddAttr("typerelation","11");
               tmpRefTag = tmpRefTag->AddChild("field_refer");
               tmpRefTag->AddAttr("fieldin","UCODE");
               tmpRefTag->AddAttr("fieldout","UCODE");
             }
          }
         _tmpTag = _tmpTag->GetNext();
       }
      delete isUIDs;
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + "�����������.xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NomISUpdate(TTagNode *ARoot, AnsiString TabName)
{
//   ShowMessage(("NomISGen Start "+TabName).c_str());
   try
    {
      AnsiString _ProcName,_Param;
      TTagNode *itFld;
      TTagNode *_tmpFl;
      TTagNode *_tmpIE;
      tmpUID = new TStringList;
      //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      // ��������� �������� �������� "unit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpGUI = ARoot->GetRoot()->GetChildByName("passport")->AV["gui"];
      idReg = ARoot->GetFirstChild()->GetChildByName("checkfields");
      ARoot->Iterate(SetRegField,TabName);
      // ��������� ������� ������ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
      itFld = ARoot->GetFirstChild()->GetChildByName("maskfields");
      if (itFld)
       {
         TTagNode *iDomTag = DomTag->GetParent()->GetChildByName("rulesunit");
         if (!iDomTag) DomTag = DomTag->GetParent()->AddChild("rulesunit");
         else          DomTag = DomTag->GetParent()->GetChildByName("rulesunit");
         itFld = itFld->GetFirstChild();
         if (itFld->CmpName("root")) itFld = itFld->GetFirstChild();
         while (itFld)
          {
            if (itFld->CmpName("field,fl"))
             {
               _tmpFl = itFld->GetTagByUID(AVUp(itFld,"ref"));
               _tmpIE = DomTag->GetChildByAV("IE","name",_tmpFl->AV["name"]);
               if (!_tmpIE)
                {
                  _tmpIE = DomTag->AddChild("IE");
                  _tmpIE->AV["uid"] = DomTag->NewUID();
                  _tmpIE->AV["name"] = _tmpFl->AV["name"];
                }
               // ���������� ��� ���������
               _Param = "";
               if (_tmpFl->CmpName("text"))
                  _ProcName = "TextValue";
               else if (_tmpFl->CmpName("binary,digit,choice,choiceTree,choiceDB"))
                  _ProcName = "RegVal";
               else if (_tmpFl->CmpName("date,datetime,time"))
                  _ProcName = "DatePeriodLast";
               else if (_tmpFl->CmpName("extedit"))
                  _ProcName = "ExtEditValue";
               else
                ShowMessage(("���� ������ ����"+_tmpFl->Name).c_str());
               _Param += tmpUID->Values[AVUp(itFld,"ref")];
               _tmpIE->AV["procname"] = _ProcName;
               _tmpIE->AV["param"] = _Param;
               if (_tmpFl->CmpName("binary,digit,choice,choiceTree,choiceDB"))
                {
                  _tmpIE->DeleteChild();
                  _tmpIE = _tmpIE->AddChild("root");
                  _tmpIE = _tmpIE->AddChild("fl");
                  _tmpIE->AV["ref"] = AVUp(itFld,"ref");
                  _tmpIE->AV["req"] = "1";
                }
             }
            itFld = itFld->GetNext();
          }
       }
      delete tmpUID;
      //  ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
//  ShowMessage("NomISGen End");
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NomISGen(TTagNode *ARoot, AnsiString TabName)
{
//   ShowMessage(("NomISGen Start "+TabName).c_str());
   try
    {
      AnsiString _ProcName,_Param;
      TTagNode *itFld;
      TTagNode *_tmpFl;
      TTagNode *_tmpIE;
      tmpUID = new TStringList;
      //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      // ��������� �������� �������� "unit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpGUI = ARoot->GetRoot()->GetChildByName("passport")->AV["gui"];
      idReg = ARoot->GetFirstChild()->GetChildByName("checkfields");
      ARoot->Iterate(SetRegField,TabName);
      // ��������� ������� ������ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
      itFld = ARoot->GetFirstChild()->GetChildByName("maskfields");
      if (itFld)
       {
         DomTag = DomTag->GetParent()->AddChild("rulesunit");
         itFld = itFld->GetFirstChild();
         if (itFld->CmpName("root")) itFld = itFld->GetFirstChild();
         while (itFld)
          {
            if (itFld->CmpName("field,fl"))
             {
               _tmpFl = itFld->GetTagByUID(AVUp(itFld,"ref"));
               _tmpIE = DomTag->AddChild("IE");
               _tmpIE->AddAttr("uid",DomTag->NewUID());
               _tmpIE->AddAttr("name",_tmpFl->AV["name"]);
               // ���������� ��� ���������
               _Param = "";
               if (_tmpFl->CmpName("text"))
                {
                  _ProcName = "TextValue";
                }
               else if (_tmpFl->CmpName("binary,digit,choice,choiceTree,choiceDB"))
                {
                  _ProcName = "RegVal";
                }
               else if (_tmpFl->CmpName("date,datetime,time"))
                {
                  _ProcName = "DatePeriodLast";
                }
               else
                ShowMessage(("���� ������ ����"+_tmpFl->Name).c_str());
               _Param += tmpUID->Values[AVUp(itFld,"ref")];
               _tmpIE->AddAttr("procname",_ProcName);
               _tmpIE->AddAttr("param",_Param);
               if (_tmpFl->CmpName("binary,digit,choice,choiceTree,choiceDB"))
                {
                  _tmpIE = _tmpIE->AddChild("root");
                  _tmpIE = _tmpIE->AddChild("fl");
                  _tmpIE->AddAttr("ref",AVUp(itFld,"ref"));
                  _tmpIE->AddAttr("req","1");
                }
             }
            itFld = itFld->GetNext();
          }
       }
      delete tmpUID;
      //  ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
//  ShowMessage("NomISGen End");
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::SetOldNames(TTagNode *itTag, AnsiString &AFake)
{
   TTagNode *__tmp;
   if (itTag->CmpName("IS,ATTR,IE,IMOR,IOR,IAND"))
    {
      itTag->AV["name"] = "**"+itTag->AV["name"];
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::AddATTR(TTagNode *ANode, AnsiString AName, AnsiString AOtherDef)
{
  if (!ANode->GetChildByAV("ATTR","name",AName)) ANode->AddChild("ATTR","uid="+DomTag->NewUID()+",name="+AName+","+AOtherDef);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NomEditBtnClick(TObject *Sender)
{
   try
    {
      if (NomGUIED->Text == "")
       {
         ShowMessage("�� ������ GUI ������������.");
         return;
       }
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
//      if ((RegFN == "")||(CardFN == "")) return;
// ------ �������� ������  -----------------------------------------------------
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ��������� ������������  ----------------------------------------------
      Label1->Caption = "���������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);
      NomTag->LoadFromFile(AllXMLPath+NomGUIED->Text);
      // passport
      DomTag = NomTag->GetChildByName("passport");
      if (!DomTag) DomTag = NomTag->AddChild("passport");
      Nom_GUIN = NomGUIED->Text;
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      DomTag->AV["GUI"] = Nom_GUIN;
      DomTag->AV["mainname"] = "����������� ����";
      DomTag->AV["autor"] = "Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19";
      DomTag->AV["version"] = IntToStr(NomTag->AV["version"].ToIntDef(0)+1);
      DomTag->AV["timestamp"] = TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->GetChildByName("technounit",true);
      if (!DomTag) DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->GetChildByName("DocIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"������","ref=period,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ������)","ref=periodfr,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ���������)","ref=periodto,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���� ������������","ref=createdate,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"�����","ref=autor,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"����� ������������","ref=createtime,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������������ �������� �������","ref=extfiltername,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"��������� ����������� �������� ��� �������� �������","ref=extfiltervalues,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� ���","ref=oldyear,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� �����","ref=oldmonth,fields=no,procname=DocAttrVal");

      // IS  - TechIS
      _tmpDom = DomTag->GetChildByName("TechIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"��� �����������","fields=no,procname=SetOrgCode");
      AddATTR(tmpTag,"����������� ������������ ���","fields=no,procname=SetLPUName");
      AddATTR(tmpTag,"������ ������������ ���","fields=no,procname=SetLPUFullName");
      AddATTR(tmpTag,"�����","fields=no,procname=SetAddr");
      AddATTR(tmpTag,"����","fields=no,procname=SetOKPO");
      AddATTR(tmpTag,"�����","fields=no,procname=SetSOATO");
      AddATTR(tmpTag,"�����","fields=no,procname=SetOKVED");
      AddATTR(tmpTag,"�����","fields=no,procname=SetOKATO");
      AddATTR(tmpTag,"�������","fields=no,procname=SetPhone");
      AddATTR(tmpTag,"����� ����������� ����� ���","fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->GetChildByAV("domain","name","���������������");
      if (!DomTag) DomTag = NomTag->AddChild("domain");
      DomTag->Iterate(SetOldNames,"");
      DomTag->AV["name"] = "���������������";
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = GetXML(DM->XMLProj->GetChildByName("project_main")->AV["registryref"])->GetChildByName("unit");
      NomISUpdate(_tmpTag, AVUp(_tmpTag,"tblname"));
      DomTag = _tmpDom;
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� �������� "�������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpTag = GetInquirers(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      FGroupName = DM->XMLProj->GetChildByName("project_main")->AV["name"];
      tmpGUI = DM->XMLProj->GetChildByName("project_main")->AV["objref"];
      SetInqField(_tmpTag);
      _tmpTag = GetOutData(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      SetOutField(_tmpTag);
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� ��������� "����������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpTag = DM->XMLProj->GetChildByName("project_main")->GetFirstChild();
      if (tmpTag)
       {// ���� ������ ��������������
         while (tmpTag)
          {
            if (tmpTag->CmpName("sub_project"))
             {
               if (tmpTag->GetAV("module_type")=="ssdl")
                {
                  FGroupName = tmpTag->AV["name"];
                  _tmpTag = GetInquirers(tmpTag->AV["objref"]);
                  tmpGUI = tmpTag->AV["objref"];
                  SetInqField(_tmpTag);
                  _tmpTag = GetOutData(tmpTag->AV["objref"]);
                  SetOutField(_tmpTag);
                }
             }
            tmpTag = tmpTag->GetNext();
          }
       }

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetChildByName("refers");
      if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      tmpTag->DeleteChild();
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[i];
         tmpRefTag->AV["typerelation"] = "11"; //1M
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CODE";
         tmpRefTag->AV["fieldout"] = "CD";
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetChildByName("refers");
         if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpTag->DeleteChild();
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[0];
         tmpRefTag->AV["typerelation"] = "11"; // M1
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CD";
         tmpRefTag->AV["fieldout"] = "CODE";
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + NomGUIED->Text+".xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetFlType(TTagNode *Src)
{ // ��� MS-SQL
    if (Src->CmpName("choiceTree"))              return "VARCHAR(150)";
    else if (Src->CmpName("choice"))
     {
       TTagNode *tmp = Src->GetFirstChild();
       int FLen = 0;
       while (tmp)
        {
          if (tmp->AV["name"].Length() > FLen) FLen = tmp->AV["name"].Length();
          tmp = tmp->GetNext();
        }
       return ("VARCHAR("+IntToStr(FLen)+")");
     }
    else if (Src->CmpName("binary"))             return "VARCHAR(3)";
    else if (Src->CmpName("date,time,datetime")) return "DATETIME";
    else if (Src->CmpName("text"))               return "VARCHAR("+Src->AV["length"]+")";
    else if (Src->CmpName("extedit"))            return "VARCHAR("+Src->AV["length"]+")";
    else if (Src->CmpName("digit"))
     {
        int dec = 0;
        int dig=StrToInt(Src->AV["digits"]);
        if (Src->GetAttrByName("decimal"))
         if (Src->AV["decimal"] != "")
           dec=StrToInt(Src->AV["decimal"]);
        if (dec == 0) return ((dig<5)? "SMALLINT" : (MSSQLChB)?"INT":"INTEGER");
        else          return "NUMERIC("+Src->AV["digits"]+","+Src->AV["decimal"]+")";
     }
    else                                         return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetColType(TTagNode *Src, AnsiString ATabName)
{ // ��� MS-SQL
    if (Src->CmpName("choice"))
     return "DIM"+Src->AV["uid"].UpperCase()+".DVAL";
    else if (Src->CmpName("binary"))
     return "(CASE When "+ATabName+".R"+Src->AV["uid"].UpperCase()+"=0 Then '���' When "+ATabName+".R"+Src->AV["uid"].UpperCase()+"=1 Then '��' End)";
    else if (Src->CmpName("choiceTree"))
     {
       TTagNode *tmp = Src->GetTagByUID(Src->AV["ref"]);
       return tmp->AV["tblname"]+"."+tmp->AV["namefield"];
     }
    else if (Src->CmpName("date,time,datetime,text,extedit,digit"))
     return ATabName+".R"+Src->AV["uid"].UpperCase();
    else
     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetRelType(TTagNode *Src, AnsiString ATabName)
{ // ��� MS-SQL
    if (Src->CmpName("choice"))
     return ATabName+".R"+Src->AV["uid"].UpperCase()+"=DIM"+Src->AV["uid"].UpperCase()+".CODE";
//    else if (Src->CmpName("binary"))
//     return ATabName+".R"+Src->AV["uid"].UpperCase()+"=DIMYESNO.CODE";
    else if (Src->CmpName("choiceTree"))
     {
       TTagNode *tmp = Src->GetTagByUID(Src->AV["ref"]);
       return ATabName+".R"+Src->AV["uid"].UpperCase()+"="+tmp->AV["tblname"]+"."+tmp->AV["codefield"];
     }
    else
     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetAttrType(TTagNode *Src)
{ // ��� MS-SQL
    if (Src->CmpName("choice,choiceTree"))
     return "attr_descr";
    else if (Src->CmpName("binary"))
     return "attr_valdescr";
    else if (Src->CmpName("date,time,datetime"))
     return "attr_datetime";
    else
     return "attr_value";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetInqFlType(TTagNode *Src)
{ // ��� MS-SQL
    TTagNode *xSc = Src->GetTagByUID(GetScaleUID(Src));
    if (xSc->CmpName("scale_binary"))           return "VARCHAR(3)";
    else if (xSc->CmpName("scale_select"))
     {
       TTagNode *tmp = xSc->GetFirstChild();
       int FLen = 0;
       while (tmp)
        {
          if (tmp->AV["name"].Length() > FLen) FLen = tmp->AV["name"].Length();
          tmp = tmp->GetNext();
        }
                                                 return ("VARCHAR("+IntToStr(FLen)+")");
     }
    else if (xSc->CmpName("scale_tree"))
     {
       TTagNode *tmp = xSc->GetFirstChild();
       if (tmp->CmpName("tree_ext"))
        return "VARCHAR(150)";
       else
        {
          AnsiString FLen = "1";
          tmp->Iterate(GetTreeScaleItemLen,FLen);
          return "VARCHAR("+FLen+")";
        }
     }
    else if (xSc->CmpName("scale_text"))         return "VARCHAR("+xSc->AV["length"]+")";
    else if (xSc->CmpName("scale_digit"))
     {
        int dec = 0;
        int dig=StrToInt(xSc->AV["digits"]);
        if (xSc->GetAttrByName("decimal"))
         if (xSc->AV["decimal"] != "")
           dec=StrToInt(xSc->AV["decimal"]);
        if (dec == 0) return ((dig<5)? "SMALLINT" : (MSSQLChB)?"INT":"INTEGER");
        else          return "NUMERIC("+xSc->AV["digits"]+","+xSc->AV["decimal"]+")";
     }
    else                                         return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetInqColType(TTagNode *Src)
{ // ��� MS-SQL
/*
    TTagNode *xSc = Src->GetTagByUID(GetScaleUID(Src));
    AnsiString PrCode = Src->GetRoot()->GetChildByName("passport")->AV["projectcode"];
    TTagNode *FInq = Src->GetParent("inquirer");
    if (xSc->CmpName("scale_select"))
     return "DIM"+PrCode+Src->AV["uid"].UpperCase()+".DVAL";
    else if (xSc->CmpName("scale_binary"))
     return "DIMYESNO.DVAL";
    else if (xSc->CmpName("scale_tree"))
     {
       TTagNode *tmp = xSc->GetFirstChild();
       if (tmp->CmpName("tree_ext"))
        return tmp->AV["tblname"]+"."+tmp->AV["namefield"];
       else
        return "DIM"+PrCode+Src->AV["uid"].UpperCase()+".DVAL";
     }
    else if (xSc->CmpName("scale_text,scale_digit"))
     return AnsiString("INQ_"+PrCode+FInq->AV["uid"]+".P"+PrCode+Src->AV["uid"]).UpperCase();
    else
     return "";
*/
    TTagNode *xSc = Src->GetTagByUID(GetScaleUID(Src));
    AnsiString PrCode = Src->GetRoot()->GetChildByName("passport")->AV["projectcode"];
    TTagNode *FInq = Src->GetParent("inquirer");
    AnsiString FTabName = "";
    if (FInq) FTabName = "INQ_"+PrCode+FInq->AV["uid"].UpperCase();
    if (xSc->CmpName("scale_select"))
     return "DIM"+PrCode+Src->AV["uid"].UpperCase()+".DVAL";
    else if (xSc->CmpName("scale_binary"))
     return "(CASE When "+FTabName+".P"+PrCode+Src->AV["uid"].UpperCase()+"=0 Then '���' When "+FTabName+".P"+PrCode+Src->AV["uid"].UpperCase()+"=1 Then '��' End)";
    else if (xSc->CmpName("scale_diapazon"))
     {
       FInq = Src->GetParent("outlist");
       while (!FInq->GetParent()->CmpName("outdata")) FInq = FInq->GetParent("outlist");
       FTabName  = "OUT_"+PrCode+FInq->AV["uid"].UpperCase();
       AnsiString ColValue = "(CASE ";
       AnsiString FCol = FTabName+".P"+PrCode+Src->AV["uid"].UpperCase();
       AnsiString FMin = xSc->AV["min"];
       AnsiString FMax = xSc->AV["max"];
       AnsiString LowVal = "";
       xSc = xSc->GetFirstChild();
       while (xSc)
        {
          if (xSc->GetAV("value") == "<<")
           {
             ColValue += " WHEN "+FCol+"<"+FMin+" THEN '"+xSc->AV["name"]+"'";
             LowVal = FMin;
           }
          else if (xSc->GetAV("value") == ">>")
           ColValue += " WHEN "+FCol+">="+FMax+" THEN '"+xSc->AV["name"]+"'";
          else
           {
             if (LowVal != "")
              ColValue += " WHEN "+FCol+">="+LowVal+" and "+FCol+"<"+xSc->AV["value"]+" THEN '"+xSc->AV["name"]+"'";
             else
              ColValue += " WHEN "+FCol+"<"+xSc->AV["value"]+" THEN '"+xSc->AV["name"]+"'";
             LowVal = xSc->AV["value"];
           }
          xSc = xSc->GetNext();
        }
       ColValue += " END)";
       return ColValue;
     }
    else if (xSc->CmpName("scale_tree"))
     {
       TTagNode *tmp = xSc->GetFirstChild();
       if (tmp->CmpName("tree_ext"))
        return tmp->AV["tblname"]+"."+tmp->AV["namefield"];
       else
        return "DIM"+PrCode+Src->AV["uid"].UpperCase()+".DVAL";
     }
    else if (xSc->CmpName("scale_text,scale_digit"))
     return FTabName+".P"+PrCode+Src->AV["uid"].UpperCase();
    else
     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetInqRelType(TTagNode *Src)
{ // ��� MS-SQL
    TTagNode *xSc = Src->GetTagByUID(GetScaleUID(Src));
    AnsiString PrCode = Src->GetRoot()->GetChildByName("passport")->AV["projectcode"];
    TTagNode *FInq = Src->GetParent("inquirer");
    if (xSc->CmpName("scale_select"))
     return AnsiString("INQ_"+PrCode+FInq->AV["uid"]+".P"+PrCode+Src->AV["uid"]).UpperCase()+"=DIM"+PrCode+Src->AV["uid"].UpperCase()+".CODE";
//    else if (xSc->CmpName("scale_binary"))
//     return AnsiString("INQ_"+PrCode+FInq->AV["uid"]+".P"+PrCode+Src->AV["uid"]).UpperCase()+"=DIMYESNO.CODE";
    else if (xSc->CmpName("scale_tree"))
     {
       TTagNode *tmp = xSc->GetFirstChild();
       if (tmp->CmpName("tree_ext"))
         return AnsiString("INQ_"+PrCode+FInq->AV["uid"]+".P"+PrCode+Src->AV["uid"]).UpperCase()+"="+tmp->AV["tblname"]+"."+tmp->AV["codefield"];
       else
        return AnsiString("INQ_"+PrCode+FInq->AV["uid"]+".P"+PrCode+Src->AV["uid"]).UpperCase()+"=DIM"+PrCode+Src->AV["uid"].UpperCase()+".CODE";
     }
    else
     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetInqAttrType(TTagNode *Src)
{ // ��� MS-SQL
    TTagNode *xSc = Src->GetTagByUID(GetScaleUID(Src));
    if (xSc->CmpName("scale_select,scale_tree"))
     return "attr_descr";
    else if (xSc->CmpName("scale_binary"))
     return "attr_valdescr";
    else
     return "attr_value";
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::GetTreeScaleItemLen(TTagNode *itTag, AnsiString &ALen)
{
   if (itTag->CmpName("treeitem,treegroup"))
    {
      if (ALen.ToInt() < itTag->AV["name"].Length())
       ALen = IntToStr(itTag->AV["name"].Length());
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::SQLDocBtnClick(TObject *Sender)
{
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
     Dlg->SQL_DOC();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FuncCheckBtnClick(TObject *Sender)
{
  AnsiString saveTitle = OpenDlg->Title;
  AnsiString saveFilter = OpenDlg->Filter;
  TTagNode *Nom = NULL;
  TStringList *NomDll = NULL;
  try
   {
     OpenDlg->InitialDir = "c:\\Program Files\\Axe\\samples";
     OpenDlg->Title = "������� �����������";
     if (OpenDlg->Execute())
      {
        Nom = new TTagNode(NULL);
        Nom->LoadFromXMLFile(OpenDlg->FileName);
        if (Nom->CmpName("mais_ontology"))
         {
           OpenDlg->Title = "������� �������� nom_dll";
           OpenDlg->Filter = "cpp -�����|*.cpp";
           if (OpenDlg->Execute())
            {
              NomDll = new TStringList;
              NomDll->LoadFromFile(OpenDlg->FileName);
              FuncList->Lines->Clear();
              PrBar->Max = NomDll->Count;
              PrBar->Position = 0;
              for (int i = 0; i < NomDll->Count; i++)
               {
                 int StdPos = NomDll->Strings[i].AnsiPos("__stdcall");
                 if (!(NomDll->Strings[i].AnsiPos("_DLLEXP_")&&(bool)StdPos))
                  {
                    NomDll->Delete(i); i--;
                  }
                 else
                  {
                    NomDll->Strings[i] = NomDll->Strings[i].SubString(StdPos+9,NomDll->Strings[i].Length()-StdPos-8).Trim();
                    NomDll->Strings[i] = NomDll->Strings[i].SubString(1,NomDll->Strings[i].AnsiPos("(")-1).Trim();
                  }
                 PrBar->Position++;
               }
              PrBar->Position = 0;
              PrBar->Max = Nom->GetCount(true,"");
              GetFuncList(Nom, NomDll->Text);
            }
         }
        else
         {
           ShowMessage(("���� '"+OpenDlg->FileName+"'�� �������� �������������").c_str());
         }
      }
   }
  __finally
   {
     if (Nom) delete Nom;
     if (NomDll) delete NomDll;
     OpenDlg->Title = saveTitle;
     OpenDlg->Filter = saveFilter;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::GetFuncList(TTagNode *ASrc, AnsiString &AFuncList)
{
  if (ASrc->CmpName("ATTR"))
   {
     if (ASrc->AV["procname"] != "")
      {
        FFuncList[ASrc->AV["procname"]] = "A"+ASrc->AV["procname"];
        if (!AFuncList.AnsiPos(("A"+ASrc->AV["procname"]).c_str()))
         {
           if (FuncList->Lines->IndexOf(("A"+ASrc->AV["procname"]).c_str()) == -1)
            FuncList->Lines->Add("A"+ASrc->AV["procname"]);
         }
      }
   }
  else if (ASrc->CmpName("IE"))
   {
     if (ASrc->AV["procname"] != "")
      {
        FFuncList["D"+ASrc->AV["procname"]] = "Def"+ASrc->AV["procname"];
        FFuncList["S"+ASrc->AV["procname"]] = "SQL"+ASrc->AV["procname"];
        if (!AFuncList.AnsiPos(("SQL"+ASrc->AV["procname"]).c_str()))
         {
           if (FuncList->Lines->IndexOf(("Def"+ASrc->AV["procname"]).c_str()) == -1)
            FuncList->Lines->Add("Def"+ASrc->AV["procname"]);
         }
      }
   }
  TTagNode *itNode = ASrc->GetFirstChild();
  while (itNode)
   {
     GetFuncList(itNode,AFuncList);
     itNode = itNode->GetNext();
   }
  PrBar->Position++;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ImmNomEditBtnClick(TObject *Sender)
{
   try
    {
      if (NomGUIED->Text == "")
       {
         ShowMessage("�� ������ GUI ������������.");
         return;
       }
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
/*      OpenDlg->Title = "������� �������� ������������ �����������";
      if (OpenDlg->Execute())
       RegFN = OpenDlg->FileName;
      OpenDlg->Title = "������� �������� ����� �����������";
      if (OpenDlg->Execute())
       CardFN = OpenDlg->FileName;
      if ((RegFN == "")||(CardFN == "")) return;
*/
// ------ �������� ������  -----------------------------------------------------
      ImmReg = new TTagNode(NULL);
      ImmCard = new TTagNode(NULL);

      _tmpTag =

//      ImmReg->LoadFromXMLFile(RegFN);
      ImmReg = GetXML("40381E23-92155860-4448");
//      ImmCard->LoadFromXMLFile(CardFN);
      ImmCard = GetXML("40381E23-92155860-4448");
//      AllXMLPath =  ExtractFilePath(CardFN);
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ���������� ������������  ----------------------------------------------
      Label1->Caption = "���������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);
//      NomTag->LoadFromFile(AllXMLPath+NomGUIED->Text);
      NomTag = GetXML("0E291426-00005882-2493");
      // passport
      DomTag = NomTag->GetChildByName("passport");
      if (!DomTag) DomTag = NomTag->AddChild("passport");
      Nom_GUIN = NomGUIED->Text;
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      DomTag->AV["GUI"] = Nom_GUIN;
      DomTag->AV["mainname"] = "����������� �����������";
      DomTag->AV["autor"] = "Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19";
      DomTag->AV["version"] = IntToStr(NomTag->AV["version"].ToIntDef(0)+1);
      DomTag->AV["timestamp"] = TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss");
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->GetChildByName("technounit",true);
      if (!DomTag) DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->GetChildByName("DocIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"������","ref=period,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ������)","ref=periodfr,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ���������)","ref=periodto,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���� ������������","ref=createdate,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"�����","ref=autor,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"����� ������������","ref=createtime,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������������ �������� �������","ref=extfiltername,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"��������� ����������� �������� ��� �������� �������","ref=extfiltervalues,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� ���","ref=oldyear,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� �����","ref=oldmonth,fields=no,procname=DocAttrVal");

      // IS  - TechIS
      _tmpDom = DomTag->GetChildByName("TechIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"��� �����������","fields=no,procname=SetOrgCode");
      AddATTR(tmpTag,"����������� ������������ ���","fields=no,procname=SetLPUName");
      AddATTR(tmpTag,"������ ������������ ���","fields=no,procname=SetLPUFullName");
      AddATTR(tmpTag,"�����","fields=no,procname=SetAddr");
      AddATTR(tmpTag,"�������","fields=no,procname=SetPhone");
      AddATTR(tmpTag,"����� ����������� ����� ���","fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      PrName = itTMP->GetAV("name");
      DomTag = NomTag->GetChildByAV("domain","name","���������������");
      if (!DomTag) DomTag = NomTag->AddChild("domain");
      DomTag->Iterate(SetOldNames,"");
      DomTag->AV["name"] = "���������������";
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = ImmReg->GetTagByUID("1000");
      NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
      DomTag = _tmpDom;
      // ��������� �������� ��������� �� ������� ���������������� �����
      _tmpTag = ImmReg->GetTagByUID("3003");
      while (_tmpTag)
       {
         DomTag = _tmpDom;
         NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->GetChildByAV("domain","name","������������");
      if (!DomTag) DomTag = NomTag->AddChild("domain");
      DomTag->AV["name"] = "������������";
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = ImmReg->GetTagByUID("1000");
      NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
      DomTag = _tmpDom;
      // ��������� �������� ��������� �� ������� ���������������� �����
      _tmpTag = ImmCard->GetTagByUID("3003");
      while (_tmpTag)
       {
         DomTag = _tmpDom;
         NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetChildByName("refers");
      if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      tmpTag->DeleteChild();
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[i];
         tmpRefTag->AV["typerelation"] = "1M"; //1M
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CODE";
         tmpRefTag->AV["fieldout"] = "UCODE";
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetChildByName("refers");
         if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpTag->DeleteChild();
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[0];
         tmpRefTag->AV["typerelation"] = "M1"; // M1
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "UCODE";
         tmpRefTag->AV["fieldout"] = "CODE";
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain")->GetNext();
      isUIDs->Clear();
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetChildByName("refers");
      if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      tmpTag->DeleteChild();
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[i];
         tmpRefTag->AV["typerelation"] = "1M"; //1M
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CODE";
         tmpRefTag->AV["fieldout"] = "UCODE";
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetChildByName("refers");
         if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpTag->DeleteChild();
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[0];
         tmpRefTag->AV["typerelation"] = "M1"; // M1
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "UCODE";
         tmpRefTag->AV["fieldout"] = "CODE";
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + NomGUIED->Text+"_new.xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button1Click(TObject *Sender)
{
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
//     Dlg->SQL_AKDO_COPY();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button2Click(TObject *Sender)
{
  TStringList *FList = NULL;
  try
   {
     FList = new TStringList;
     if (!OpenDlg->Execute()) return;
     TSQLCreator *Dlg = NULL;
     try
      {
        Dlg = new TSQLCreator;
        Dlg->DBStruct = FDBStruct;
        Dlg->PrBar = PrBar;
        Dlg->Label1 = Label1;
        Dlg->CanStoredProc = spGenChB->Checked;
        Dlg->CanUpdate = UpdateChB->Checked;
        Dlg->CanArc = ArcChB->Checked;
        Dlg->CanFieldName = FieldNameChB->Checked;
        Dlg->CanExceptions = ExceptChB->Checked;
        Dlg->CanGenerators = GenChB->Checked;
        Dlg->CanTriggers = TrigChB->Checked;
        Dlg->CanIndices = IdxChB->Checked;
        Dlg->CanInqIndices = inqIdxChB->Checked;
        Dlg->CanInqStoredProc = inqspGenChB->Checked;
        Dlg->CanInqTriggers = inqTrigChB->Checked;
        Dlg->CanInqGenerators = inqGenChB->Checked;
        Dlg->CanInqExceptions = inqExceptChB->Checked;
        Dlg->PrefED = PrefED->Text;
        Dlg->AllXMLPath = AllXMLPath;
        Dlg->OFileName = OpenDlg->FileName;
        Dlg->SQL_REG_SYS(FList);
      }
     __finally
      {
        if (Dlg) delete Dlg;
      }
     if (!OpenDlg->Execute()) return;
     Dlg = NULL;
     try
      {
        Dlg = new TSQLCreator;
        Dlg->DBStruct = FDBStruct;
        Dlg->PrBar = PrBar;
        Dlg->Label1 = Label1;
        Dlg->CanStoredProc = spGenChB->Checked;
        Dlg->CanUpdate = UpdateChB->Checked;
        Dlg->CanArc = ArcChB->Checked;
        Dlg->CanFieldName = FieldNameChB->Checked;
        Dlg->CanExceptions = ExceptChB->Checked;
        Dlg->CanGenerators = GenChB->Checked;
        Dlg->CanTriggers = TrigChB->Checked;
        Dlg->CanIndices = IdxChB->Checked;
        Dlg->CanInqIndices = inqIdxChB->Checked;
        Dlg->CanInqStoredProc = inqspGenChB->Checked;
        Dlg->CanInqTriggers = inqTrigChB->Checked;
        Dlg->CanInqGenerators = inqGenChB->Checked;
        Dlg->CanInqExceptions = inqExceptChB->Checked;
        Dlg->PrefED = PrefED->Text;
        Dlg->AllXMLPath = AllXMLPath;
        Dlg->OFileName = OpenDlg->FileName;
        Dlg->SQL_IMM_SYS(FList);
      }
     __finally
      {
        if (Dlg) delete Dlg;
      }
     Dlg = NULL;
     try
      {
        Dlg = new TSQLCreator;
        Dlg->DBStruct = FDBStruct;
        Dlg->PrBar = PrBar;
        Dlg->Label1 = Label1;
        Dlg->CanStoredProc = spGenChB->Checked;
        Dlg->CanUpdate = UpdateChB->Checked;
        Dlg->CanArc = ArcChB->Checked;
        Dlg->CanFieldName = FieldNameChB->Checked;
        Dlg->CanExceptions = ExceptChB->Checked;
        Dlg->CanGenerators = GenChB->Checked;
        Dlg->CanTriggers = TrigChB->Checked;
        Dlg->CanIndices = IdxChB->Checked;
        Dlg->CanInqIndices = inqIdxChB->Checked;
        Dlg->CanInqStoredProc = inqspGenChB->Checked;
        Dlg->CanInqTriggers = inqTrigChB->Checked;
        Dlg->CanInqGenerators = inqGenChB->Checked;
        Dlg->CanInqExceptions = inqExceptChB->Checked;
        Dlg->PrefED = PrefED->Text;
        Dlg->CanMSSQL = MSSQLChB->Checked;
        Dlg->AllXMLPath = AllXMLPath;
        Dlg->OFileName = "";
        Dlg->SQL_DOC_SYS(FList);
      }
     __finally
      {
        if (Dlg) delete Dlg;
      }
     FList->SaveToFile("c:\\SYSDATA.sql"); 
   }
  __finally
   {
     if (FList) delete FList;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button3Click(TObject *Sender)
{
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
     Dlg->DB_AKDO();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button4Click(TObject *Sender)
{
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
     Dlg->CreateDbStruct();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ProjectBEPropertiesButtonClick(TObject *Sender,
      int AButtonIndex)
{
   AnsiString PrName;
   try
    {
      AnsiString ProjXMLPath;
      OpenDlg->InitialDir = "c:\\Program Files\\Axe\\samples";
      OpenDlg->Filter = "����� �������,����� �������(zip)|*ICS_DATA_DEF*.xml;*ICS_DATA_DEF*.zxml";
      if (!OpenDlg->Execute())
       return;
      DM->SQLProj->Clear(); 
      if (DM->DataDef)
       delete DM->DataDef;
      DM->DataDef  = new TTagNode(NULL);
      ProjXMLPath = OpenDlg->FileName;
      if (FileExists(ProjXMLPath))
       {
         if (ExtractFileExt(ProjXMLPath) == ".xml")
          DM->DataDef->LoadFromXMLFile(ProjXMLPath);
         else
          DM->DataDef->LoadFromZIPXMLFile(ProjXMLPath);
       }
      else
       {
         ShowMessage("���� �����������");
         return;
       }
      if (!DM->DataDef->CmpName("xml_list_project"))
       {
         ShowMessage(("���� \""+ProjXMLPath+"\" �� �������� ��������� �������").c_str());
         return;
       }
      if (DM->XMLList)
       delete DM->XMLList;
      DM->XMLList = new TAxeXMLContainer();
      Label1->Caption = "�������� ������ ������� ...";
      Label1->Update();
      AnsiString xxCount = "0";
      AllXMLPath =  ExtractFilePath(ProjXMLPath);
      if (AllXMLPath[AllXMLPath.Length()] == '\\') AllXMLPath = AllXMLPath.SubString(1,AllXMLPath.Length()-1);
// ------ ������ ���������� XML-�� ������� -------------------------------------
      TTagNode *itNode = DM->DataDef->GetChildByName("content");
      if (itNode)
       {
         itNode = itNode->GetFirstChild();
         while (itNode)
          {
            if (!itNode->CmpAV("xmltype","xml_list_project"))
             {
               try
                {
                  if (!itNode->CmpAV("xmltype","vac,probe,ICS_APP_OPT"))
                   DM->XMLList->LoadAV1(AllXMLPath, itNode->AV["xmlref"]);
                }
               catch(...)
                {
                }
               if (itNode->CmpAV("xmltype","project"))
                DM->XMLProj = DM->XMLList->GetXML(itNode->AV["xmlref"]);
               if (itNode->CmpAV("sqltype","1"))
                DM->SQLProj->Add((void*)itNode);

             }
            itNode = itNode->GetNext();
          }
       }
// ------ �������� XML-�� ������� ----------------------------------------------
//      DM->XMLProj->GetChildByName("project_main")->Iterate(LoadXMLs,AnsiString(""));
      DM->_EKGFL_      = "_NULL_";
      DM->_EKGCHSS_    = "_NULL_";
      DM->_EKGCHSSTAB_ = "_NULL_";
      if (DM->XMLProj)
       {
         TTagNode *FSetting = DM->XMLProj->GetChildByName("setting");
         AnsiString INQGUI = DM->XMLProj->GetChildByName("project_main")->AV["objref"];
         FSetting = FSetting->GetChildByAV("ext_module","name","ekg."+INQGUI,true);
         if (FSetting)
          {
            TTagNode *tmpSet;
            tmpSet = FSetting->GetChildByAV("ext_module_set","name","ekgfl");
            if (tmpSet) DM->_EKGFL_ = tmpSet->AV["value"];

            tmpSet = FSetting->GetChildByAV("ext_module_set","name","ekgchss");
            if (tmpSet) DM->_EKGCHSS_ = tmpSet->AV["value"];

            tmpSet = FSetting->GetChildByAV("ext_module_set","name","ekgchsstab");
            if (tmpSet) DM->_EKGCHSSTAB_ = tmpSet->AV["value"];
          }
       }
      PrName = DM->DataDef->GetChildByName("passport")->AV["title"];
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      ProjectBE->Text = PrName+" ("+ProjXMLPath+")";
      NomGenBtn->Enabled = true;
      SQLGenBtn->Enabled = true;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ �������� �������� �������",MB_ICONSTOP);
      NomGenBtn->Enabled = false;
      SQLGenBtn->Enabled = false;
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ �������� �������� �������","������",MB_ICONSTOP);
      NomGenBtn->Enabled = false;
      SQLGenBtn->Enabled = false;
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button5Click(TObject *Sender)
{
/*
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
*/
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
     Dlg->CreateDbStruct();
     Dlg->Create_Script();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button6Click(TObject *Sender)
{
   try
    {
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
//      if ((RegFN == "")||(CardFN == "")) return;
// ------ �������� ������  -----------------------------------------------------
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ��������� ������������  ----------------------------------------------
      Label1->Caption = "��������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);  NomTag->Name = "mais_ontology";
      // passport
      DomTag = NomTag->AddChild("passport");
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      else                      Nom_GUIN = NomGUIED->Text;
      DomTag->AddAttr("GUI",Nom_GUIN);
      DomTag->AddAttr("mainname","����������� ����");
      DomTag->AddAttr("autor","Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19");
      DomTag->AddAttr("version","1");
      DomTag->AddAttr("timestamp",TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss"));
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->AddChild("ATTRS");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=������,ref=period,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=���� ������������,ref=createdate,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=�����,ref=autor,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ������������,ref=createtime,fields=no,procname=DocAttrVal");
      // IS  - TechIS
      _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->AddChild("ATTRS");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=������������ ���,fields=no,procname=SetLPUName");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ��������� ���������,fields=no,procname=SetAddr");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ����������� ����� ���,fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      PrName = itTMP->GetAV("name");
      DomTag = NomTag->AddChild("domain");
      DomTag->AddAttr("name","���������������");
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      TTagNode *FRegistry = GetXML(DM->DataDef->GetTagByUID("0000")->AV["xmlref"]);
      _tmpTag = FRegistry->GetChildByName("unit");
      _tmpDom = DomTag;
      NomISGen(_tmpTag, AVUp(_tmpTag,"tblname"));
      DomTag = _tmpDom;
      TTagNode *itN1,*itN2;
      itN1 = FRegistry->GetChildByName("classform",true);
      while (itN1)
       {
         itN2 = itN1->GetChildByName("class",true);
         while (itN2)
          {
            _tmpDom = DomTag;
            NomISGen(itN2, "CLASS_"+itN2->AV["uid"].UpperCase());
            DomTag = _tmpDom;
            itN2 = itN2->GetNext();
          }
         itN1 = itN1->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      NomTag->SaveToXMLFile("c:\\__sssssssssss.xml","");
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[i]);
         tmpRefTag->AddAttr("typerelation","11"); //1M
         tmpRefTag->AddAttr("keylink","and");
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CODE");
         tmpRefTag->AddAttr("fieldout","CD");
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[0]);
         tmpRefTag->AddAttr("typerelation","11"); // M1
         tmpRefTag->AddAttr("keylink","and");
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CD");
         tmpRefTag->AddAttr("fieldout","CODE");
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + "�����������.xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button7Click(TObject *Sender)
{
  TTagNode *Nom = NULL;
  AnsiString NomDll = "";
  try
   {
     OpenDlg->InitialDir = "c:\\Program Files\\Axe\\samples";
     OpenDlg->Title = "������� �����������";
     if (OpenDlg->Execute())
      {
        Nom = new TTagNode(NULL);
        Nom->LoadFromXMLFile(OpenDlg->FileName);
        if (Nom->CmpName("mais_ontology"))
         {
           FFuncList.clear();
           FuncList->Lines->Clear();
           GetFuncList(Nom, NomDll);
           for (TAnsiStrMap::iterator i = FFuncList.begin(); i != FFuncList.end(); i++)
            FuncList->Lines->Add(i->second);
         }
        else
         {
           ShowMessage(("���� '"+OpenDlg->FileName+"'�� �������� �������������").c_str());
         }
      }
   }
  __finally
   {
     if (Nom) delete Nom;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button8Click(TObject *Sender)
{
  TSQLCreator *Dlg = NULL;
  try
   {
     Dlg = new TSQLCreator;
     Dlg->DBStruct = FDBStruct;
     Dlg->PrBar = PrBar;
     Dlg->Label1 = Label1;
     Dlg->CanStoredProc = spGenChB->Checked;
     Dlg->CanFieldName = FieldNameChB->Checked;
     Dlg->CanExceptions = ExceptChB->Checked;
     Dlg->CanGenerators = GenChB->Checked;
     Dlg->CanTriggers = TrigChB->Checked;
     Dlg->CanUpdate = UpdateChB->Checked;
     Dlg->CanArc = ArcChB->Checked;
     Dlg->CanIndices = IdxChB->Checked;
     Dlg->CanInqIndices = inqIdxChB->Checked;
     Dlg->CanInqStoredProc = inqspGenChB->Checked;
     Dlg->CanInqTriggers = inqTrigChB->Checked;
     Dlg->CanInqGenerators = inqGenChB->Checked;
     Dlg->CanInqExceptions = inqExceptChB->Checked;
     Dlg->PrefED = PrefED->Text;
     Dlg->CanMSSQL = MSSQLChB->Checked;
     Dlg->AllXMLPath = AllXMLPath;
     Dlg->OFileName = "";
     Dlg->CreateInqStruct();
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
  delete FDBStruct;        
}
//---------------------------------------------------------------------------

