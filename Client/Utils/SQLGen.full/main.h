//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include "DMF.h"
#include <Dialogs.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxMemo.hpp"
#include "cxRichEdit.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
        TProgressBar *PrBar;
        TLabel *Label1;
        TOpenDialog *OpenDlg;
        TCheckBox *spGenChB;
        TButton *SQLGenBtn;
        TButton *NomGenBtn;
        TButton *RegGenBtn;
        TCheckBox *FieldNameChB;
        TButton *ImmGenBtn;
        TButton *ImmNomGenBtn;
        TBevel *Bevel1;
        TEdit *NomGUIED;
        TButton *NomEditBtn;
        TEdit *PrefED;
        TCheckBox *MSSQLChB;
        TLabel *Label2;
        TLabel *Label3;
        TButton *SQLDocBtn;
        TCheckBox *UpdateChB;
        TCheckBox *TrigChB;
        TCheckBox *GenChB;
        TCheckBox *ExceptChB;
        TCheckBox *inqspGenChB;
        TCheckBox *inqTrigChB;
        TCheckBox *inqGenChB;
        TCheckBox *inqExceptChB;
        TBevel *Bevel2;
        TBevel *Bevel3;
        TLabel *Label4;
        TLabel *Label5;
        TCheckBox *IdxChB;
        TCheckBox *inqIdxChB;
        TButton *FuncCheckBtn;
        TMemo *FuncList;
        TButton *ImmNomEditBtn;
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TButton *Button4;
        TcxButtonEdit *ProjectBE;
        TButton *Button5;
        TButton *Button6;
        TButton *Button7;
        TButton *Button8;
        TCheckBox *ArcChB;
        void __fastcall NomGenBtnClick(TObject *Sender);
        void __fastcall SQLGenBtnClick(TObject *Sender);
        void __fastcall RegGenBtnClick(TObject *Sender);
        void __fastcall ImmGenBtnClick(TObject *Sender);
        void __fastcall ImmNomGenBtnClick(TObject *Sender);
        void __fastcall NomEditBtnClick(TObject *Sender);
        void __fastcall SQLDocBtnClick(TObject *Sender);
        void __fastcall FuncCheckBtnClick(TObject *Sender);
        void __fastcall ImmNomEditBtnClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall ProjectBEPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Button6Click(TObject *Sender);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall Button8Click(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:
     TTagNode *idReg;
     AnsiString AllXMLPath,FGroupName,tmpGUI,xTabName;
     int    ssdl_oCount;
     TStringList *tmpUID;
     TTagNode  *iTag;
     TTagNode  *NomTag;
     TTagNode  *DomTag;
     TTagNode  *ParentTag;
     bool __fastcall SetOldNames(TTagNode *itTag, AnsiString &AFake);
     void __fastcall AddATTR(TTagNode *ANode, AnsiString AName, AnsiString AOtherDef);
     bool __fastcall GetXMLCount(TTagNode *itTag, AnsiString &UID);
     bool __fastcall LoadXMLs(TTagNode *itTag, AnsiString &UID);
     void __fastcall LoadXML(AnsiString AName);
     bool __fastcall SetRegField(TTagNode *itTag, AnsiString &TblName);
     void __fastcall SetInqField(TTagNode *AInqNode);
     void __fastcall FSetInqField(TTagNode *AInqNode,TTagNode *AAttrsNode, TTagNode *ARulesNode);
     void __fastcall SetOutField(TTagNode *AOutNode);
     void __fastcall FSetOutField(TTagNode *AOutNode,TTagNode *AAttrsNode, TTagNode *ARulesNode);
     void __fastcall NomISGen(TTagNode *ARoot, AnsiString TabName);
     void __fastcall NomISUpdate(TTagNode *ARoot, AnsiString TabName);
     void __fastcall GetFuncList(TTagNode *ASrc, AnsiString &AFuncList);
     TAnsiStrMap FFuncList;
     TTagNode *FDBStruct;

     //****************************
     AnsiString __fastcall GetFlType(TTagNode *Src);//---
     AnsiString __fastcall GetColType(TTagNode *Src, AnsiString ATabName);
     AnsiString __fastcall GetRelType(TTagNode *Src, AnsiString ATabName);
     AnsiString __fastcall AddChBD_ATTR(TTagNode *ADomTag,TTagNode *ASrc, AnsiString ARefParent, AnsiString AParentField, AnsiString AParentTab);
     AnsiString __fastcall GetScaleUID(TTagNode *quTag);//---
     AnsiString __fastcall GetAttrType(TTagNode *Src);
     AnsiString __fastcall GetInqAttrType(TTagNode *Src);
     AnsiString __fastcall GetInqFlType(TTagNode *Src);
     AnsiString __fastcall GetInqColType(TTagNode *Src);
     AnsiString __fastcall GetInqRelType(TTagNode *Src);
     bool       __fastcall GetTreeScaleItemLen(TTagNode *itTag, AnsiString &ALen);
public:
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif

