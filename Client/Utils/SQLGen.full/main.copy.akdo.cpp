void __fastcall TMainForm::NomGenBtnClick(TObject *Sender)
{
   try
    {
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
//      if ((RegFN == "")||(CardFN == "")) return;
// ------ �������� ������  -----------------------------------------------------
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ��������� ������������  ----------------------------------------------
      Label1->Caption = "��������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);  NomTag->Name = "mais_ontology";
      // passport
      DomTag = NomTag->AddChild("passport");
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      else                      Nom_GUIN = NomGUIED->Text;
      DomTag->AddAttr("GUI",Nom_GUIN);
      DomTag->AddAttr("mainname","����������� ����");
      DomTag->AddAttr("autor","Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19");
      DomTag->AddAttr("version","1");
      DomTag->AddAttr("timestamp",TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss"));
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->AddChild("ATTRS");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=������,ref=period,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=���� ������������,ref=createdate,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=�����,ref=autor,fields=no,procname=DocAttrVal");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ������������,ref=createtime,fields=no,procname=DocAttrVal");
      // IS  - TechIS
      _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->AddChild("ATTRS");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=������������ ���,fields=no,procname=SetLPUName");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ��������� ���������,fields=no,procname=SetAddr");
      tmpTag->AddChild("ATTR","uid="+DomTag->NewUID()+",name=����� ����������� ����� ���,fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//      PrName = itTMP->GetAV("name");
      DomTag = NomTag->AddChild("domain");
      DomTag->AddAttr("name","���������������");
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = GetXML(DM->XMLProj->GetChildByName("project_main")->AV["registryref"])->GetChildByName("unit");
      NomISGen(_tmpTag, AVUp(_tmpTag,"tblname"));
      DomTag = _tmpDom;
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� �������� "�������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpTag = GetInquirers(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      FGroupName = DM->XMLProj->GetChildByName("project_main")->AV["name"];
      tmpGUI = DM->XMLProj->GetChildByName("project_main")->AV["objref"];
      SetInqField(_tmpTag);
      _tmpTag = GetOutData(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      SetOutField(_tmpTag);
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� ��������� "����������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpTag = DM->XMLProj->GetChildByName("project_main")->GetFirstChild();
      if (tmpTag)
       {// ���� ������ ��������������
         while (tmpTag)
          {
            if (tmpTag->CmpName("sub_project"))
             {
               if (tmpTag->GetAV("module_type")=="ssdl")
                {
                  FGroupName = tmpTag->AV["name"];
                  _tmpTag = GetInquirers(tmpTag->AV["objref"]);
                  tmpGUI = tmpTag->AV["objref"];
                  SetInqField(_tmpTag);
                  _tmpTag = GetOutData(tmpTag->AV["objref"]);
                  SetOutField(_tmpTag);
                }
             }
            tmpTag = tmpTag->GetNext();
          }
       }

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[i]);
         tmpRefTag->AddAttr("typerelation","11"); //1M
         tmpRefTag->AddAttr("keylink","and");
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CODE");
         tmpRefTag->AddAttr("fieldout","CD");
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AddAttr("ref",isUIDs->Strings[0]);
         tmpRefTag->AddAttr("typerelation","11"); // M1
         tmpRefTag->AddAttr("keylink","and");
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AddAttr("fieldin","CD");
         tmpRefTag->AddAttr("fieldout","CODE");
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + "�����������.xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NomEditBtnClick(TObject *Sender)
{
   try
    {
      if (NomGUIED->Text == "")
       {
         ShowMessage("�� ������ GUI ������������.");
         return;
       }
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmReg, *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
//      if ((RegFN == "")||(CardFN == "")) return;
// ------ �������� ������  -----------------------------------------------------
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ��������� ������������  ----------------------------------------------
      Label1->Caption = "���������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);
      NomTag->LoadFromFile(AllXMLPath+NomGUIED->Text);
      // passport
      DomTag = NomTag->GetChildByName("passport");
      if (!DomTag) DomTag = NomTag->AddChild("passport");
      Nom_GUIN = NomGUIED->Text;
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      DomTag->AV["GUI"] = Nom_GUIN;
      DomTag->AV["mainname"] = "����������� ����";
      DomTag->AV["autor"] = "Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19";
      DomTag->AV["version"] = IntToStr(NomTag->AV["version"].ToIntDef(0)+1);
      DomTag->AV["timestamp"] = TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->GetChildByName("technounit",true);
      if (!DomTag) DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->GetChildByName("DocIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"������","ref=period,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ������)","ref=periodfr,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ���������)","ref=periodto,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���� ������������","ref=createdate,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"�����","ref=autor,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"����� ������������","ref=createtime,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������������ �������� �������","ref=extfiltername,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"��������� ����������� �������� ��� �������� �������","ref=extfiltervalues,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� ���","ref=oldyear,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� �����","ref=oldmonth,fields=no,procname=DocAttrVal");

      // IS  - TechIS
      _tmpDom = DomTag->GetChildByName("TechIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"��� �����������","fields=no,procname=SetOrgCode");
      AddATTR(tmpTag,"����������� ������������ ���","fields=no,procname=SetLPUName");
      AddATTR(tmpTag,"������ ������������ ���","fields=no,procname=SetLPUFullName");
      AddATTR(tmpTag,"�����","fields=no,procname=SetAddr");
      AddATTR(tmpTag,"����","fields=no,procname=SetOKPO");
      AddATTR(tmpTag,"�����","fields=no,procname=SetSOATO");
      AddATTR(tmpTag,"�����","fields=no,procname=SetOKVED");
      AddATTR(tmpTag,"�����","fields=no,procname=SetOKATO");
      AddATTR(tmpTag,"�������","fields=no,procname=SetPhone");
      AddATTR(tmpTag,"����� ����������� ����� ���","fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->GetChildByAV("domain","name","���������������");
      if (!DomTag) DomTag = NomTag->AddChild("domain");
      DomTag->Iterate(SetOldNames,"");
      DomTag->AV["name"] = "���������������";
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = GetXML(DM->XMLProj->GetChildByName("project_main")->AV["registryref"])->GetChildByName("unit");
      NomISUpdate(_tmpTag, AVUp(_tmpTag,"tblname"));
      DomTag = _tmpDom;
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� �������� "�������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpTag = GetInquirers(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      FGroupName = DM->XMLProj->GetChildByName("project_main")->AV["name"];
      tmpGUI = DM->XMLProj->GetChildByName("project_main")->AV["objref"];
      SetInqField(_tmpTag);
      _tmpTag = GetOutData(DM->XMLProj->GetChildByName("project_main")->AV["objref"]);
      SetOutField(_tmpTag);
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �������� ��������� "����������� ������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpTag = DM->XMLProj->GetChildByName("project_main")->GetFirstChild();
      if (tmpTag)
       {// ���� ������ ��������������
         while (tmpTag)
          {
            if (tmpTag->CmpName("sub_project"))
             {
               if (tmpTag->GetAV("module_type")=="ssdl")
                {
                  FGroupName = tmpTag->AV["name"];
                  _tmpTag = GetInquirers(tmpTag->AV["objref"]);
                  tmpGUI = tmpTag->AV["objref"];
                  SetInqField(_tmpTag);
                  _tmpTag = GetOutData(tmpTag->AV["objref"]);
                  SetOutField(_tmpTag);
                }
             }
            tmpTag = tmpTag->GetNext();
          }
       }

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetChildByName("refers");
      if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      tmpTag->DeleteChild();
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[i];
         tmpRefTag->AV["typerelation"] = "11"; //1M
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CODE";
         tmpRefTag->AV["fieldout"] = "CD";
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetChildByName("refers");
         if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpTag->DeleteChild();
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[0];
         tmpRefTag->AV["typerelation"] = "11"; // M1
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CD";
         tmpRefTag->AV["fieldout"] = "CODE";
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + NomGUIED->Text+".xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";
      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------

