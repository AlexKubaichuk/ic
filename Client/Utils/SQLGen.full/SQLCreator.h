//---------------------------------------------------------------------------

#ifndef SQLCreatorH
#define SQLCreatorH
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include "DMF.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TSLIterate)(TTagNode *ANode, TList *Dest);
class TSQLCreator : public TObject
{
private:
     AnsiString __fastcall GetScaleUID(TTagNode *quTag);
     AnsiString __fastcall GetInqVal(TTagNode *inqTag);
     void __fastcall CreateAddInq(TTagNode *inqTag, TStringList *xAdd, TStringList *xDel, TStringList *xInqFl, TStringList *xInqDataFl);
     bool __fastcall GetFields(TTagNode *itTag, TList *ADest);
     void __fastcall CreateExtInqStruct(TTagNode *AInqPrDef);
     bool __fastcall getFieldDef(TTagNode *itTag, TList *ADest);
     bool __fastcall getClFieldDef(TTagNode *itTag, TList *ADest);
     void __fastcall CreateFieldDefs(TTagNode *ACLDef,TTagNode *ADestDef);
     bool __fastcall GetOutFields(TTagNode *itTag, TList *ADest);
     bool __fastcall GetChBD(TTagNode *itTag, AnsiString &UID);
     bool __fastcall GetDistOutFields(TTagNode *itTag, TList *ADest);
     void __fastcall CreateRegDepend(TTagNode *AClDef,TTagNode *AClRoot/*, TTagNode *AUnitRoot*/);
     void __fastcall GetReference(AnsiString UID, TTagNode *AClRoot, /*TTagNode *AURoot,*/TStringList *AbfUIDList,TStringList *AafUIDList);
     bool __fastcall GetTreeScaleItemLen(TTagNode *itTag, AnsiString &ALen);
     AnsiString __fastcall GetInqFlType(TTagNode *uiTag, bool AFull = true);
     AnsiString UnitTabName;
//     TStringList *tblList;
//     TTagNode    *xmlCopyList;
//     TTagNode    *xmlTblList;
//     TTagNode    *xmlTable;
//     TTagNode    *xmlGenList;
     TStringList *FrRef;
//     TTagNode  *iTag;
     bool __fastcall Iterate(TTagNode *ASrcNode, TSLIterate ItItem, TList *ADest);
     bool __fastcall Iterate1st(TTagNode *ASrcNode, TSLIterate ItItem, TList *ADest);
     AnsiString __fastcall CreateRegStruct(TTagNode *ASrc, bool AExtInq = false);
     bool __fastcall getClassList(TTagNode *itTag, TList *ADest);
     bool __fastcall getTreeClassList(TTagNode *itTag, TList *ADest);
     AnsiString __fastcall GetFlType(TTagNode *Src, bool AFull = true);
     void __fastcall CreateClassDataProc(TTagNode *AClDef,TTagNode *ASPDef, AnsiString ATabPref);
     void __fastcall CreateTreeClassDataProc(TTagNode *AClDef,TTagNode *ASPDef, AnsiString ATabPref);
     AnsiString __fastcall GetFlLen(TTagNode *Src);
     AnsiString __fastcall GetFlScale(TTagNode *Src);
     AnsiString __fastcall GetFlDef(TTagNode *Src);
     AnsiString __fastcall GetInqFlLen(TTagNode *Src);
     AnsiString __fastcall GetInqFlScale(TTagNode *Src);
     AnsiString __fastcall GetInqFlDef(TTagNode *Src);
     void __fastcall Create_SYSDATA(TTagNode *AflNode, AnsiString ATblName);
     void __fastcall CreateMCODEProc(TTagNode *ASPDef, AnsiString ATabPref);
     void __fastcall FCreateInqStruct(TTagNode *AInqDef);
     TTagNode *CurrTab;
     TTagNode *CurrFl;
     void __fastcall CreateStruct();
     void __fastcall CreateTablesScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateTableScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateIndicesScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateGeneratorsScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateExceptionsScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateTriggersScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateProceduresScript(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateExecutesScript(TTagNode *ADefNode,   TStringList *AScript);
     void __fastcall FCreate_Script(TStringList *AScript);
     void __fastcall CreateVarsList(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateInputList(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateOutputList(TTagNode *ADefNode, TStringList *AScript);
     void __fastcall CreateFieldList(TTagNode *ADefNode, TStringList *AScript);
     TTagNode *TabRoot;
     TTagNode *ProcRoot;
     TTagNode *TrigRoot;
     TTagNode *GenRoot;
     TTagNode *ExceptRoot;
     TTagNode *IndRoot;
     TTagNode *ExRoot;

     bool fscMSSQL,fscSysData,fscFieldName,fscFunction,fscUpdate,fscArc;
     bool fscTables,fscSP,fscExcept,fscGen,fscTrig,fscIdx;
     bool fsinqTables,fscinqSP,fscinqExcept,fscinqGen,fscinqTrig,fscinqIdx;

    void __fastcall CreateInqDefStruct(TTagNode *AInqDef, TTagNode *ADestNode);
    AnsiString FFlPr;
    void __fastcall AddTypedNode(TTagNode *ADest,TTagNode *AFlDef);
    TTagNode *FDBStruct;
    AnsiString FPrefED;
public:
     TProgressBar *PrBar;
     TLabel *Label1;
     AnsiString PrefED,AllXMLPath,OFileName;
     void __fastcall CreateDbStruct();
     void __fastcall Create_Script();
     void __fastcall CreateInqStruct();
     void __fastcall SQL_AKDO();//!!!
     void __fastcall SQL_IMM();//!!!
     void __fastcall SQL_REG();
     void __fastcall SQL_DOC();//!!!
     void __fastcall DB_AKDO();//!!!
     void __fastcall SQL_REG_SYS(TStringList *AData);//!!!
     void __fastcall SQL_IMM_SYS(TStringList *AData);//!!!
     void __fastcall SQL_DOC_SYS(TStringList *AData);//!!!

     __property TTagNode *DBStruct        = {read=FDBStruct,write=FDBStruct};

     __property bool CanMSSQL        = {read=fscMSSQL,write=fscMSSQL};
     __property bool CanSystemScript = {read=fscSysData,write=fscSysData};
     __property bool CanFieldName    = {read=fscFieldName,write=fscFieldName};
     __property bool CanFunction     = {read=fscFunction,write=fscFunction};
     __property bool CanUpdate       = {read=fscUpdate,write=fscUpdate};
     __property bool CanArc          = {read=fscArc,write=fscArc};


     __property bool CanTables     = {read=fscTables,write=fscTables};
     __property bool CanStoredProc = {read=fscSP,write=fscSP};
     __property bool CanExceptions = {read=fscExcept,write=fscExcept};
     __property bool CanGenerators = {read=fscGen,write=fscGen};
     __property bool CanTriggers   = {read=fscTrig,write=fscTrig};
     __property bool CanIndices    = {read=fscIdx,write=fscIdx};

     __property bool CanInqTables     = {read=fsinqTables,write=fsinqTables};
     __property bool CanInqStoredProc = {read=fscinqSP,write=fscinqSP};
     __property bool CanInqExceptions = {read=fscinqExcept,write=fscinqExcept};
     __property bool CanInqGenerators = {read=fscinqGen,write=fscinqGen};
     __property bool CanInqTriggers   = {read=fscinqTrig,write=fscinqTrig};
     __property bool CanInqIndices    = {read=fscinqIdx,write=fscinqIdx};
};
#endif
