//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "SQLCreator.h"
//#define _CREATE_TABLE AnsiString("EXECUTE PROCEDURE CREATETABLE_IF_NOT_EXISTS")
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateDbStruct()
{
   AnsiString PrName;
   try
    {
      Label1->Caption = "��������� ��������� �� ... ";
      Label1->Update();
// ------ ��������� �������  ----------------------------------------------
      UnitTabName = "";
      AnsiString tmp,_tmp, PrName;
      tmp = "";
      TTagNode *itTMP;
      // ��������� �������� ��������� ��� ������������
      try
       {
         // �������� ���������
         CreateStruct();
         for (int i = 0; i < DM->SQLProj->Count; i++)
          {
            itTMP = (TTagNode*)DM->SQLProj->Items[i];
            if (itTMP->CmpAV("xmltype","registry"))
             {
               // ��������� ������ ��� ������������
               //--------------------------------------------->>>>
               FPrefED = PrefED;
               if (fscArc)
                {
                  FPrefED = "";
                  UnitTabName = CreateRegStruct(GetXML(itTMP->AV["xmlref"]));
                  if (FPrefED != PrefED)
                   {
                     FPrefED = PrefED;
                     CreateRegStruct(GetXML(itTMP->AV["xmlref"]));
                   }
                }
               else
                UnitTabName = CreateRegStruct(GetXML(itTMP->AV["xmlref"]));
               //--------------------------------------------->>>>
             }
            else if (itTMP->CmpAV("xmltype","project"))
             {
               // ��������� ������ ��� ������������ ����
               //--------------------------------------------->>>>
               if (CanInqIndices||CanInqStoredProc||CanInqTriggers||CanInqGenerators||CanInqExceptions)
                {
                  itTMP = GetXML(itTMP->AV["xmlref"])->GetChildByName("project_main");

                  FPrefED = PrefED;
                  if (fscArc)
                   {
                     FPrefED = "";
                     FCreateInqStruct(GetXML(itTMP->AV["objref"]));
                     if (FPrefED != PrefED)
                      {
                        FPrefED = PrefED;
                        FCreateInqStruct(GetXML(itTMP->AV["objref"]));
                      }
                   }
                  else
                   FCreateInqStruct(GetXML(itTMP->AV["objref"]));

                  itTMP = itTMP->GetFirstChild();
                  while (itTMP)
                   {
                     // ��������� ������ ��� ��������������� ������������
                     if (itTMP->CmpName("sub_project"))
                      {
                        if (itTMP->CmpAV("module_type","ssdl"))
                         {
                           FPrefED = PrefED;
                           if (fscArc)
                            {
                              FPrefED = "";
                              FCreateInqStruct(GetXML(itTMP->AV["objref"]));
                              if (FPrefED != PrefED)
                               {
                                 FPrefED = PrefED;
                                 FCreateInqStruct(GetXML(itTMP->AV["objref"]));
                               }
                            }
                           else
                            FCreateInqStruct(GetXML(itTMP->AV["objref"]));

                           TTagNode *itNode = itTMP->GetFirstChild();
                           while (itNode)
                            {
                              if (itNode->CmpName("extdef"))
                               {
                                 FPrefED = PrefED;
                                 if (fscArc)
                                  {
                                    FPrefED = "";
                                    CreateRegStruct(GetXML(itNode->AV["gui"]),true);
                                    if (FPrefED != PrefED)
                                     {
                                       FPrefED = PrefED;
                                       CreateRegStruct(GetXML(itNode->AV["gui"]),true);
                                     }
                                  }
                                 else
                                  CreateRegStruct(GetXML(itNode->AV["gui"]),true);
                               }
                              itNode = itNode->GetNext();
                            }
                         }
                      }
                     else if (itTMP->CmpName("plan"))
                      {
                        if (itTMP->CmpAV("module_type","ssdl"))
                         {
                           FPrefED = PrefED;
                           if (fscArc)
                            {
                              FPrefED = "";
                              FCreateInqStruct(GetXML(itTMP->AV["objref"]));
                              if (FPrefED != PrefED)
                               {
                                 FPrefED = PrefED;
                                 FCreateInqStruct(GetXML(itTMP->AV["objref"]));
                               }
                            }
                           else
                            FCreateInqStruct(GetXML(itTMP->AV["objref"]));

                         }
                      }
                     else if (itTMP->CmpName("extdef"))
                      {
                        FPrefED = PrefED;
                        if (fscArc)
                         {
                           FPrefED = "";
                           CreateRegStruct(GetXML(itTMP->AV["gui"]),true);
                           if (FPrefED != PrefED)
                            {
                              FPrefED = PrefED;
                              CreateRegStruct(GetXML(itTMP->AV["gui"]),true);
                            }
                         }
                        else
                         CreateRegStruct(GetXML(itTMP->AV["gui"]),true);
                      }
                     itTMP = itTMP->GetNext();
                   }
                  FPrefED = PrefED;
                  if (fscArc)
                   {
                     FPrefED = "";
                     CreateExtInqStruct(DM->XMLProj->GetChildByName("project_main"));
                     if (FPrefED != PrefED)
                      {
                        FPrefED = PrefED;
                        CreateExtInqStruct(DM->XMLProj->GetChildByName("project_main"));
                      }
                   }
                  else
                   CreateExtInqStruct(DM->XMLProj->GetChildByName("project_main"));
                }
               //--------------------------------------------->>>>
             }
          }
       }
      __finally
       {
         Label1->Caption = "";
         Label1->Update();
       }
    }
   catch(Exception& E)
    {
      MessageBox(Application->Handle,E.Message.c_str(),"������ �������� ���������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Application->Handle,"������ �������� ���������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_AKDO()
{
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateStruct()
{
//  FDBStruct = new TTagNode(NULL);
  FDBStruct->Name = "FDBStruct";
  TabRoot    = FDBStruct->AddChild("tables");
  ProcRoot   = FDBStruct->AddChild("procedures");
  TrigRoot   = FDBStruct->AddChild("triggers");
  GenRoot    = FDBStruct->AddChild("generators");
  ExceptRoot = FDBStruct->AddChild("exceptions");
  IndRoot    = FDBStruct->AddChild("indices");
  ExRoot     = FDBStruct->AddChild("Execute");
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::DB_AKDO()
{
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::CreateRegStruct(TTagNode *ASrc, bool AExtInq)
{ // ������� - ��� ������� ������ �����
  AnsiString UID, tabPref,RCUnitTabName;
  RCUnitTabName = "";
  TTagNode *cl,*clFieldDefs,*tabDefNode,*keyDefNode;
  TList *tclList = new TList;
  TList *clList  = new TList;
  TList *flList  = new TList;
  try
   {
      // �������� ������ �������� ������� �����
//      TTagNode *UnitRoot = ASrc->GetChildByName("unit",true);
      if (!AExtInq)
       {
         // �������� ������ �������� ���������������
         TTagNode *ClRoot  = ASrc->GetChildByName("classes",true);
         // �������� ������ �������� ���������� ���������������
//         TTagNode *tClRoot = ASrc->GetChildByName("treeclasses",true);
         // ��������� ������ ���������������
         AnsiString tmp = "";
         AnsiString tabName;
         if (ClRoot)  {Iterate(ClRoot,getClassList,clList); }
//         if (tClRoot) {Iterate(tClRoot,getTreeClassList,tclList);}
         if ((ClRoot == NULL)/*&&(tClRoot == NULL)*/) return "";
         //������������ ������ ���������������
         for (int k = 0; k < clList->Count; k++)
          {
            //������������ ������� ��������������
            cl = (TTagNode*)clList->Items[k];
            UID = cl->AV["uid"];
            tabPref = FPrefED+UID;
            tabName="CLASS_"+tabPref;
            tabDefNode = TabRoot->AddChild("table");
            tabDefNode->AV["name"] = tabName;
            tabDefNode->AV["ID"] = cl->AV["uid"];
            tabDefNode->AV["comment"] = cl->AV["name"];
            // ��������� �����
            GenRoot->AddChild("generator","name=GEN_"+tabPref+"_ID");
            // ��������� ��������� ������ ���� �����
            CreateMCODEProc(ProcRoot->AddChild("PROCEDURE"),tabPref);
            // ��������� ��������� ������ ��������������
            if (!cl->CmpAV("ref","unit,card"))
             CreateClassDataProc(cl,ProcRoot->AddChild("PROCEDURE"), tabPref);
            // �������� �������
            tabDefNode->AddChild("keys")->AddChild("key","name=CODE,type=INTEGER, req=NOT NULL");
            CreateFieldDefs(cl, tabDefNode->AddChild("fields"));
            Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabName);
            if (!cl->CmpAV("ref","unit,card"))
             CreateRegDepend(cl, ClRoot/*, UnitRoot*/);
          }
       }
   }
  __finally
   {
      if (tclList) {delete tclList; tclList = NULL;}
      if (clList) {delete clList; clList = NULL;}
      if (flList) {delete flList; flList = NULL;}
   }
  return RCUnitTabName;
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateRegDepend(TTagNode *AClDef,TTagNode *AClRoot/*, TTagNode *AUnitRoot*/)
{
  TStringList *bfUIDList = new TStringList; // ������ UID-�� ����������� �� �������� ������ "UID�����."="UID����. obj = 0 - class; 1 - unit"
  TStringList *afUIDList = new TStringList; // ������ UID-�� ������� ���������� ������� ����� �������� "UID�����."="UID����."
  TTagNode *trNode;
  try
   {
     // �������� ������� ������������
     AnsiString UID,tabName,exName,exText,tmp,bodyText;
     UID = AClDef->AV["uid"];
     tabName="CLASS_"+FPrefED+UID;
     afUIDList->Clear();
     GetReference(UID,AClRoot,/*AUnitRoot,*/bfUIDList,afUIDList);
     if (bfUIDList && (bfUIDList->Count > 0))
      { // ���� �������������� � ������� ���������� ��������� ������� ������
        // �� ������������� � "uid" = UID
        for (int i = 0; i < bfUIDList->Count; i++)
         {
           // ������� ����������
           exName = "DEL_"+tabName+"_"+IntToStr(i);
           if (((int)bfUIDList->Objects[i]) == 0)
            exText = AClRoot->GetTagByUID(bfUIDList->Names[i])->AV["name"];
/*           else
            exText = AUnitRoot->AV["name"];*/
           exText = "�������� �� ��������. <"+exText+">";
           if (exText.Length() > 80) exText = exText.SubString(1,80);
           ExceptRoot->AddChild("EXCEPTION","name="+exName+",text="+exText);
           // ������� �������
           trNode = TrigRoot->AddChild("TRIGGER","name="+tabName+"_BD"+IntToStr(i));
           trNode->AV["tabname"] = tabName;
           trNode->AV["position"] = "0";
           trNode->AV["type"] = "BEFORE DELETE";
           trNode->AddChild("vars")->AddChild("var","name=MCOUNT,type=INTEGER");
           if (((int)bfUIDList->Objects[i]) == 0)
            tmp = "CLASS_"+FPrefED+bfUIDList->Names[i];
/*           else
            tmp = FPrefED+AUnitRoot->AV["tblname"];*/
           exText = "R"+bfUIDList->Values[bfUIDList->Names[i]];
           bodyText =  "SELECT COUNT("+tmp+".CODE) FROM "+tmp+" WHERE "+tmp+"."+exText+"=old.CODE INTO :MCOUNT;";
           bodyText += "  IF (MCOUNT > 0) THEN\n   BEGIN";
           bodyText += "    EXCEPTION "+exName+";";
           bodyText += "   END";
           trNode->AddChild("body")->AV["pcdata"] = bodyText;
         }
      }
     if (afUIDList->Count > 0)
      { // ���� �������������� �� ������� ���������� ������� �������
        // �� �������� �� ������������� � "uid" = UID

        // ������� �������
        trNode = TrigRoot->AddChild("TRIGGER","name="+tabName+"_AD0");
        trNode->AV["tabname"] = tabName;
        trNode->AV["position"] = "0";
        trNode->AV["type"] = "AFTER DELETE";
        bodyText = "";
        for (int i = 0; i < afUIDList->Count; i++)
         {
           tmp = "CLASS_"+FPrefED+afUIDList->Names[i];
           exText = "R"+afUIDList->Values[afUIDList->Names[i]];
           bodyText += "DELETE FROM "+tmp+" WHERE "+tmp+"."+exText+"=old.CODE;";
         }
        trNode->AddChild("body")->AV["pcdata"] = bodyText;
      }
   }
  __finally
   {
     if (bfUIDList) delete bfUIDList;
     if (afUIDList) delete afUIDList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateFieldDefs(TTagNode *ACLDef,TTagNode *ADestDef)
{
  TList *flList = new TList;
  try
   {
     TTagNode *flDef,*flDestDef,*idxDef;
     flList->Clear();
     Iterate(ACLDef,getFieldDef,flList);
     for (int i = 0; i < flList->Count; i++)
      {
        flDef = (TTagNode*)flList->Items[i];
        flDestDef = ADestDef->AddChild("field");
        flDestDef->AV["name"] = "R"+flDef->AV["uid"];
        if (flDef->AV["fl"].Length())
         {
           flDestDef->AV["name"] = flDef->AV["fl"];
           if (flDef->CmpAV("fl","CGUID"))
            {
              idxDef = IndRoot->AddChild("index","name=CLASS_"+FPrefED+ACLDef->AV["uid"]+"CGUID, tabname=CLASS_"+FPrefED+ACLDef->AV["uid"]);
              idxDef->AddChild("field","name=CGUID");
            }
         }

        flDestDef->AV["type"] = GetFlType(flDef);
        if ((flDef->CmpAV("required","yes")) && (!flDef->GetChildByName("actuals")))
         flDestDef->AV["req"] = "NOT NULL";
        flDestDef->AV["comment"] = flDef->AV["name"];

        if (flDef->CmpName("choiceTree,choiceDB")&&ACLDef)
         {
           if (ACLDef->CmpName("unit"))
            if (ACLDef->AV["tblname"].Length())
             {
               idxDef = IndRoot->AddChild("index","name="+FPrefED+ACLDef->AV["tblname"]+flDef->AV["uid"]+",tabname="+FPrefED+ACLDef->AV["tblname"]);
               idxDef->AddChild("field","name=CODE");
               if (flDef->AV["fl"].Length())
                idxDef->AddChild("field","name="+flDef->AV["fl"]);
               else
                idxDef->AddChild("field","name=R"+flDef->AV["uid"]);
             }
         }
      }
   }
  __finally
   {
     if (flList) delete flList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateMCODEProc(TTagNode *ASPDef, AnsiString ATabPref)
{
  ASPDef->AV["name"] = "MCODE_"+ATabPref;
  ASPDef->AddChild("RETURNS")->AddChild("out","name=MCODE,type=INTEGER");
  ASPDef->AddChild("body")->AV["pcdata"] = "MCODE=gen_id(GEN_"+ATabPref+"_ID,1);\nsuspend;";
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateClassDataProc(TTagNode *AClDef,TTagNode *ASPDef, AnsiString ATabPref)
{
  TList *flList  = new TList;
  TList *clFrom = new TList;
  try
   {
     TTagNode *spIn, *spOut, *spVar, *spBody;
     AnsiString ex = "";
     AnsiString chvar = "";
     AnsiString sel = "SELECT ";
     AnsiString into = "INTO ";

     ASPDef->AV["name"] = "CLASSDATA_"+ATabPref;
     spIn = ASPDef->AddChild("input");
     spIn->AddChild("in","name=CL_CODE,type=INTEGER");
     spOut = ASPDef->AddChild("RETURNS");
     spOut->AddChild("out","name=STR_CLASS,type=VARCHAR(255)");
     spVar  = ASPDef->AddChild("vars");
     spBody = ASPDef->AddChild("body");

     flList->Clear();
     Iterate(AClDef,getClFieldDef,flList);
     // ������������ ����� ��� RETURNS, SELECT, INTO
     clFrom->Add(flList->Items[0]);
     AnsiString _uid = ((TTagNode*)flList->Items[0])->AV["uid"];
     TTagNode *_i,*n;
     AnsiString _UID_,_FlType_,_pUID_;
     int xlen;
     for (int s = 1; s < flList->Count; s++)
      {
        _i = (TTagNode*)flList->Items[s];    // 0
        _UID_ = _i->AV["uid"];               // 1
        _FlType_ = GetFlType(_i);            // 2
        _pUID_ = _i->GetParent("class")->AV["uid"]; // 3
        // ������������ �������� ��� choice
        xlen = 0;                                 // max ����� �������� "choice"-a
        bool isChoice = _i->CmpName("choice");    // ��� ���� choice ?
        bool lastF=((s+1) == flList->Count);      // ���� ��������� ?
        if (isChoice)
         {
           spVar->AddChild("VARIABLE","name=PV_"+_UID_+",type=INTEGER");
           chvar += "IF (PV_"+_UID_+" IS NULL) THEN\n BEGIN P_"+_UID_+" = ''; END\nELSE\n BEGIN\n";
           n = _i->GetFirstChild();
           AnsiString _Name,_Val;
           while (n)
            {
              if (n->CmpName("choicevalue"))
               {
                 _Name = n->AV["name"];
                 _Val  = n->AV["value"];
                 if (xlen < _Name.Length()) xlen=_Name.Length();
                 chvar += "  IF (PV_"+_UID_+"="+_Val+") THEN\n    BEGIN P_"+_UID_+" = '"+_Name+"'; END\n";
               }
              n = n->GetNext();
            }
           chvar += " END\n";
         }
        //���� � SELECT
        sel += "CLASS_"+FPrefED+_pUID_+".R"+_UID_+((lastF)?" \n":" , ");
        //���� � INTO
        into += " :P"+AnsiString((isChoice)?"V":"")+"_"+_UID_+((lastF)?" \n":" , ");
        //���� � RETURNS
        spOut->AddChild("out","name=P_"+_UID_)->AV["type"] = AnsiString((isChoice)?AnsiString("VARCHAR("+IntToStr(xlen)+")"):_FlType_);
        if (_i->CmpName("class")) clFrom->Add(_i); // ������ ���������������
      }
     ex +="STR_CLASS='';\n";
     // ������������ FROM
     _i = (TTagNode*)clFrom->Items[0];   // 0
     _UID_ = _i->AV["uid"];               // 1
     AnsiString xfrom = "FROM CLASS_"+FPrefED+_UID_+" ";
     if (clFrom->Count == 1)
      xfrom += "WHERE CLASS_"+FPrefED+_UID_+".CODE=:CL_CODE\n";
     else
      {
        for (int j = 1; j < clFrom->Count; j++)
         {
           _i = (TTagNode*)clFrom->Items[j];   // 0
           _UID_ = _i->AV["uid"];               // 1
           _FlType_ = GetFlType(_i);           // 2
           _pUID_ = _i->GetParent()->AV["uid"]; // 3
           if (j == 1)
            xfrom += "JOIN CLASS_"+FPrefED+_UID_+" on (CLASS_"+FPrefED+_UID_+".CODE=CLASS_"+FPrefED+_pUID_+".R"+_UID_+" AND CLASS_"+FPrefED+_pUID_+".CODE=:CL_CODE)\n";
           else
            xfrom += "   LEFT JOIN CLASS_"+FPrefED+_UID_+" on (CLASS_"+FPrefED+_UID_+".CODE=CLASS_"+FPrefED+_pUID_+".R"+_UID_+")\n";
         }
      }
     ex += "\n"+sel + "\n" + xfrom + "\n" + into + ";\n" + chvar;
     // ������������ STR_CLASS - ��������� ������������� ��������������
     TTagNode *x;
     for (int j = clFrom->Count-1; j >= 0; j--)
      {
        _i = ((TTagNode*)clFrom->Items[j])->GetChildByName("description");
        _i = _i->GetFirstChild()->GetFirstChild();
        while (_i)
         {
           n = _i->GetTagByUID(_i->AV["ref"]);
           AnsiString xname="'";
           if (n)
            {
              if (fscFieldName)  xname = n->AV["name"]+" '";
              else                        xname = "'";
              if (n->CmpName("text"))            xname += "||P_"+n->AV["uid"]+" ";
              else if (n->CmpName("datetime"))   xname += "||CAST(P_"+n->AV["uid"]+" AS VARCHAR(19)) ";
              else if (n->CmpName("time"))       xname += "||CAST(P_"+n->AV["uid"]+" AS VARCHAR(8)) ";
              else                               xname += "||CAST(P_"+n->AV["uid"]+" AS VARCHAR(10)) ";
            }
           ex += "    IF (P_"+_i->AV["ref"]+" IS NOT NULL) THEN \n     BEGIN STR_CLASS = STR_CLASS||' "+xname+"; END\n";
           _i = _i->GetNext();
         }
      }
     ex += "suspend;";
     // �������� �������� ��������� � PCDATA
     spBody->AV["pcdata"] = ex;
   }
  __finally
   {
     if (flList) delete flList;
     if (clFrom) delete clFrom;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTreeClassDataProc(TTagNode *AClDef,TTagNode *ASPDef, AnsiString ATabPref)
{
  TTagNode *spIn, *spOut;
  AnsiString BodyCont;
  ASPDef->AV["name"] = "CLASSDATA_"+ATabPref+AClDef->AV["tblname"];
  spIn = ASPDef->AddChild("input");
  spIn->AddChild("in","name=CL_CODE,type=INTEGER");
  spOut = ASPDef->AddChild("RETURNS");
  spOut->AddChild("out","name=STR_CLASS,type=VARCHAR(255)");
  spOut->AddChild("out","name=P_EXTCODE,type=VARCHAR(30)");
  spOut->AddChild("out","name=P_NAME ,type=VARCHAR(255)");
  BodyCont = "STR_CLASS='';\n";
  BodyCont += "SELECT "+AClDef->AV["namefield"].UpperCase()+","+AClDef->AV["extcodefield"].UpperCase()+"\n";
  BodyCont += "FROM "+ATabPref+AClDef->AV["tblname"].UpperCase()+" \nWHERE "+AClDef->AV["codefield"].UpperCase()+"=:CL_CODE\n";
  BodyCont += "INTO :P_NAME,:P_EXTCODE;\n";
  BodyCont += "IF (P_NAME IS NOT NULL) THEN\n";
  BodyCont += "BEGIN STR_CLASS = P_NAME ; END\n suspend;";
  ASPDef->AddChild("body")->AV["pcdata"] = BodyCont;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getFieldDef(TTagNode *itTag, TList *ADest)
{//--- ������������ ����� ������� ---------------------------------------------------------
  AnsiString UID, REQ, __tmp;
  if (itTag->CmpName("text,binary,date,datetime,time,digit,choice,extedit,choiceTree,choiceDB"))
   {
     ADest->Add((void*)itTag);
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getClFieldDef(TTagNode *itTag, TList *ADest)
{//--- ������������ ����� ������� ---------------------------------------------------------
  AnsiString UID, REQ, __tmp;
  if (itTag->CmpName("class,text,binary,date,datetime,time,digit,choice,extedit,choiceTree,choiceDB"))
   {
     ADest->Add((void*)itTag);
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::Iterate(TTagNode *ASrcNode, TSLIterate ItItem, TList *ADest)
{
   if (!ASrcNode) throw ETagNodeError("Error in "+AnsiString(__FUNC__)+" -> object is NULL");
   TTagNode *SaveThis = ASrcNode;
   if (ItItem(ASrcNode,ADest))
    return true;
   if (SaveThis != ASrcNode) return false;
   TTagNode *ItNode = ASrcNode->GetFirstChild();
   while (ItNode)
    {
      SaveThis = ItNode->GetNext();
      if (Iterate(ItNode, ItItem, ADest))
       return true;
      ItNode = SaveThis;
    }
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::Iterate1st(TTagNode *ASrcNode, TSLIterate ItItem, TList *ADest)
{
   if (!ASrcNode) throw ETagNodeError("Error in "+AnsiString(__FUNC__)+" -> object is NULL");
   TTagNode *ItNode = ASrcNode->GetFirstChild();
   while(ItNode)
    {
      if (ItItem(ItNode,ADest)) return true;
      ItNode = ItNode->GetNext();
    }
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getClassList(TTagNode *itTag, TList *ADest)
{
   if (itTag->CmpName("class")) ADest->Add((void*)itTag);
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::getTreeClassList(TTagNode *itTag, TList *ADest)
{
   if (itTag->CmpName("treeclass")) ADest->Add((void*)itTag);
   return false;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetFlType(TTagNode *Src, bool AFull)
{
    if (Src->CmpName("class,choiceDB,choiceTree")) return (fscMSSQL)?"INT":"INTEGER";
    else if (Src->CmpName("binary,choice"))        return "SMALLINT";
    else if (Src->CmpName("date,time"))            return (fscMSSQL)?AnsiString("DATETIME"):Src->Name.UpperCase();
    else if (Src->CmpName("datetime"))             return "TIMESTAMP";
    else if (Src->CmpName("text"))                 return (AFull)?AnsiString("VARCHAR("+Src->AV["length"]+")"):AnsiString("VARCHAR");
    else if (Src->CmpName("extedit"))
     {
       if (Src->CmpAV("fieldtype","char,varchar")||(!Src->AV["fieldtype"].Trim().Length()))
        {
          if (Src->AV["length"].ToIntDef(0))
           {
             if (Src->AV["fieldtype"].Trim().Length())
              return (AFull)?AnsiString(Src->AV["fieldtype"].UpperCase()+"("+Src->AV["length"]+")"):Src->AV["fieldtype"].UpperCase();
             else
              return (AFull)?AnsiString("VARCHAR("+Src->AV["length"]+")"):Src->AV["fieldtype"].UpperCase();
           }
          else
           {
             ShowMessage("��� ���� '"+Src->AV["name"]+"' �� ������ ������ ����, ��� ������ '"+Src->AV["fieldtype"]+"'.");
             return "INTEGER";
           }
        }
       else
        return Src->AV["fieldtype"].UpperCase();
     }
    else if (Src->CmpName("digit"))
     {
        int dec = 0;
        int dig=StrToInt(Src->AV["digits"]);
        if (Src->GetAttrByName("decimal"))
         if (Src->AV["decimal"].Length())
           dec=StrToInt(Src->AV["decimal"]);
        if (dec == 0) return ((dig<5)? "SMALLINT" : (fscMSSQL)?"INT":"INTEGER");
        else          return (AFull)?AnsiString("DECIMAL("+Src->AV["digits"]+","+Src->AV["decimal"]+")"):AnsiString("DECIMAL");
     }
    else                                             return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetFlLen(TTagNode *Src)
{
    if (Src->CmpName("class,choiceDB,choiceTree,date,time")) return "4";
    else if (Src->CmpName("binary,choice"))                  return "2";
    else if (Src->CmpName("datetime"))                       return "8";
    else if (Src->CmpName("text"))                           return Src->AV["length"];
    else if (Src->CmpName("extedit"))
     {
       if (Src->CmpAV("fieldtype","char,varchar")||(!Src->AV["fieldtype"].Trim().Length()))
        {
          if (Src->AV["length"].ToIntDef(0))
           return Src->AV["length"];
          else
           {
             ShowMessage("��� ���� '"+Src->AV["name"]+"' �� ������ ������ ����, ��� ������ '"+Src->AV["fieldtype"]+"'.");
             return "4";
           }
        }
       else
        {
          if (Src->CmpAV("fieldtype","smallint"))
            return "2";
          else
            return "4";
        }
     }
    else if (Src->CmpName("digit"))
     {
        int dec = 0;
        int dig=StrToInt(Src->AV["digits"]);
        if (Src->AV["decimal"].Length())
         dec=StrToInt(Src->AV["decimal"]);
        if (dec == 0)                                        return ((dig<5)? "2" : "4");
        else                                                 return Src->AV["digits"];
     }
    else                                                     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetFlScale(TTagNode *Src)
{
    if (!Src->CmpName("digit"))              return "0";
    else
     {
        int dec = 0;
        if (Src->AV["decimal"].Length())
         dec=StrToInt(Src->AV["decimal"]);
        if (dec == 0)                        return "0";
        else                                 return Src->AV["decimal"];
     }
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetFlDef(TTagNode *Src)
{
  if (Src->CmpName("binary"))    return "0";
  else if (Src->CmpAV("required","yes") && (Src->GetChildByName("actuals") == NULL))
   {
     if (Src->CmpName("choice")) return Src->AV["default"];
     else                        return "0";
   }
  else                           return "NULL";
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::GetReference(AnsiString UID, TTagNode *AClRoot, /*TTagNode *AURoot,*/TStringList *AbfUIDList,TStringList *AafUIDList)
{
  TTagNode *_cl = AClRoot->GetTagByUID(UID);
  TTagNode *_tmp,*_tmpCl;
  AnsiString clRef;
  FrRef = NULL;
  try
   {
    if (_cl)
     {
        if (AbfUIDList&&_cl->GetParent()->CmpName("class"))
         AbfUIDList->AddObject((_cl->GetParent()->AV["uid"]+"="+UID).c_str(),(TObject*)0);
        FrRef = new TStringList;
        // ��������� ������� ������ �� ������ ���������������
        clRef = UID;
        FrRef->Clear();
        AClRoot->Iterate(GetChBD,clRef);
        if (FrRef->Count > 0)
         { // ������ ����
           for (int i = 0; i < FrRef->Count; i++)
            {
              _tmpCl = AClRoot->GetTagByUID(FrRef->Strings[i])->GetParent("class");
              _tmp = _tmpCl->GetChildByName("description")->GetNext();
              bool AllChBD = true;
              while (_tmp && AllChBD)
               {
                  AllChBD &= _tmp->CmpName("choiceDB");
                  _tmp = _tmp->GetNext();
               }
              if (AllChBD)
               { // ��� ���� "choiceDB" - ����� ������� + ������� �� �����. �� �������� ���� ������
                 // �.�. ���� �������� �������������� ���������������
                 AafUIDList->AddObject((_tmpCl->AV["uid"]+"="+FrRef->Strings[i]).c_str(),(TObject*)0);
               }
              else
               {
                 AbfUIDList->AddObject((_tmpCl->AV["uid"]+"="+FrRef->Strings[i]).c_str(),(TObject*)0);
               }
            }
         }
        // ��������� ������� ������ �� ������� ������ �����
        FrRef->Clear();
/*        AURoot->Iterate(GetChBD,clRef);
        if (FrRef->Count > 0)
         {
           for (int i = 0; i < FrRef->Count; i++)
            AbfUIDList->AddObject(("0000="+FrRef->Strings[i]).c_str(),(TObject*)1);
         }  */
     }
    else
     {
       ShowMessage("������ �������� ������� ������ �� �������������");
       AbfUIDList->Clear();
       AafUIDList->Clear();
//         return;
     }
   }
  __finally
   {
     if (FrRef) {delete FrRef; FrRef = NULL;}
   }
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::GetFields(TTagNode *itTag, TList *ADest)
{ // �������� ��� ������ ����� ���������
   if (itTag->CmpName("question")||(itTag->CmpName("group") && itTag->AV["groupscaleuid"].Length()))
    ADest->Add((void*)itTag);
   return false;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetInqFlType(TTagNode *TagWs, bool AFull)
{//--- ����������� ���� ���� ---------------------------------------------------------
    AnsiString SSS = GetScaleUID(TagWs);
    TTagNode *sc = TagWs->GetTagByUID(SSS);
    AnsiString PrCode = TagWs->GetRoot()->GetChildByName("passport")->AV["projectcode"];
//    TTagNode *FInq = TagWs->GetParent("inquirer");
    if (fscMSSQL)
     {
/*
       // ������������ ������ ���������
       AnsiString TabName = "DIM"+PrCode+TagWs->AV["uid"].UpperCase();
       if (sc->CmpName("scale_select"))
        {
          // �������� ������� DIM"+itTag->AV["uid"]
          TTagNode *tmp = sc->GetFirstChild();
          int FLen = 0;
          while (tmp)
           {
             if (tmp->AV["name"].Length() > FLen) FLen = tmp->AV["name"].Length();
             exqList->Add("Insert Into "+TabName+" Values("+tmp->AV["value"]+",'"+tmp->AV["name"]+"')\n");
             tmp = tmp->GetNext();
           }
          dimtblList->Add(("\nCREATE TABLE dbo."+TabName+"\n(").c_str());
          dimtblList->Add("CODE INT NOT NULL,\nDVAL VARCHAR("+IntToStr(FLen)+"),");
          dimtblList->Add("PRIMARY KEY (CODE)\n)");
        }
       else if (sc->CmpName("scale_tree"))
        {
          TTagNode *tmp = sc->GetFirstChild();
          if (tmp->CmpName("tree_int"))
           {
             AnsiString AFLen = TabName+".1";
             tmp->Iterate(GetTreeScaleItemLen,AFLen);
             dimtblList->Add(("\nCREATE TABLE dbo."+TabName+"\n(").c_str());
             dimtblList->Add("CODE INT NOT NULL,\nDVAL VARCHAR("+_UID(AFLen)+"),");
             dimtblList->Add("PRIMARY KEY (CODE)\n)");
           }
        }
*/
     }
    // ������������ ����� �����
    if (sc->CmpName("scale_tree,scale_ext"))           return (fscMSSQL)?"INT":"INTEGER";
    else if (sc->CmpName("scale_binary,scale_select")) return "SMALLINT";
    else if (sc->CmpName("scale_text"))                return (AFull)?AnsiString("VARCHAR("+sc->AV["length"]+")"):AnsiString("VARCHAR");
    else if (sc->CmpName("scale_digit"))
     {
        int dec = 0;
        int dig = StrToInt(sc->AV["digits"]);
        if (sc->GetAttrByName("decimal"))
         if (sc->AV["decimal"].Length())
           dec=StrToInt(sc->AV["decimal"]);
        if (dec == 0)                                  return ((dig<5)? "SMALLINT" : (fscMSSQL)?"INT":"INTEGER");
        else                                           return (AFull)?AnsiString("DECIMAL("+sc->AV["digits"]+","+sc->AV["decimal"]+")"):AnsiString("DECIMAL"); 
     }
    else                                               return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetInqFlLen(TTagNode *Src)
{
    TTagNode *sc = Src->GetTagByUID(GetScaleUID(Src));
    // ������������ ����� ����� �����
    if (sc->CmpName("scale_tree,scale_ext"))                     return "4";
    else if (sc->CmpName("scale_binary,scale_select")) return "2";
    else if (sc->CmpName("scale_text"))                return sc->AV["length"];
    else if (sc->CmpName("scale_digit"))
     {
        int dec = 0;
        int dig = StrToInt(sc->AV["digits"]);
        if (sc->AV["decimal"].Length())
         dec=StrToInt(sc->AV["decimal"]);
        if (dec == 0)                                  return ((dig<5)? "2" : "4");
        else                                           return sc->AV["digits"];
     }
    else                                               return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetInqFlScale(TTagNode *Src)
{
    TTagNode *sc = Src->GetTagByUID(GetScaleUID(Src));
    // ������������ ����� ����� �����
    if (!sc->CmpName("scale_digit"))                    return "0";
    else
     {
        int dec = 0;
        if (sc->AV["decimal"].Length())
         dec=StrToInt(sc->AV["decimal"]);
        if (dec == 0)                                  return "0";
        else                                           return sc->AV["decimal"];
     }
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetInqFlDef(TTagNode *Src)
{
    TTagNode *sc = Src->GetTagByUID(GetScaleUID(Src));
    // ������������ ����� ����� �����
    if (sc->CmpName("scale_binary"))      return "0";
    else if (sc->CmpName("scale_select")) return sc->AV["default"];
    else                                  return "NULL";
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::GetTreeScaleItemLen(TTagNode *itTag, AnsiString &ALen)
{
/*
   if (itTag->CmpName("treeitem,treegroup"))
    {
      if (_UID(ALen).ToInt() < itTag->AV["name"].Length())
       ALen = _GUI(ALen)+"."+IntToStr(itTag->AV["name"].Length());
      exqList->Add("Insert Into "+_GUI(ALen)+" Values("+IntToStr(UIDInt(itTag->AV["UID"]))+",'"+itTag->AV["name"]+"')\n");
    }
*/
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::GetOutFields(TTagNode *itTag, TList *ADest)
{// �������� ��� ������ ����� �������� ������
   if (itTag->CmpName("out")) ADest->Add((void*)itTag);
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::GetDistOutFields(TTagNode *itTag, TList *ADest)
{// �������� ��� ������ ����� �������������� �������� ������
   if (itTag->CmpName("distout")) ADest->Add((void*)itTag);
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TSQLCreator::GetChBD(TTagNode *itTag, AnsiString &UID)
{
   if (itTag->CmpName("choiceDB"))
    {
      if (itTag->AV["ref"] == UID)
        FrRef->Add(itTag->AV["uid"]);
    }
   return false;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetScaleUID(TTagNode *quTag)
{
   if (quTag->GetAttrByName("scaleuid"))
    {
      if (!quTag->AV["scaleuid"].Length()) return GetScaleUID(quTag->GetParent());
      else                                 return quTag->AV["scaleuid"];
    }
   return GetScaleUID(quTag->GetParent());
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSQLCreator::GetInqVal(TTagNode *inqTag)
{
    AnsiString SSS = GetScaleUID(inqTag);
    TTagNode *sc = inqTag->GetTagByUID(SSS);
    if (sc->CmpName("scale_tree,scale_ext"))        return "-1";
    else if (sc->CmpName("scale_binary"))
     {
       if (sc->AV["default"].Length())
        return IntToStr((int)(sc->AV["default"][1] == '�'));
       else
        return "0";
     }
    else if (sc->CmpName("scale_select")) return sc->AV["default"];
    else if (sc->CmpName("scale_text"))   return "''";
    else if (sc->CmpName("scale_digit"))  return "NULL";
    else                                  return "";
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_IMM()
{
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_REG()
{
   AnsiString PrName;
   try
    {
      Label1->Caption = "��������� ��������� �� ... ";
      Label1->Update();
// ------ ��������� �������  ----------------------------------------------
      UnitTabName = "";
      AnsiString tmp,_tmp, PrName;
      tmp = "";
      TTagNode *itTMP = NULL;
      try
       {
         CreateStruct();
         itTMP = new TTagNode(NULL);
         // ��������� ������ ��� ������������
         itTMP->LoadFromXMLFile(OFileName);
         UnitTabName = CreateRegStruct(itTMP);
//         FDBStruct->SaveToXMLFile("c:\\ddd.xml","");
       }
      __finally
       {
         delete itTMP;
         Label1->Caption = "";
         Label1->Update();
       }
    }
   catch(Exception& E)
    {
      MessageBox(Application->Handle,E.Message.c_str(),"������ �������� ���������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Application->Handle,"������ �������� ���������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_DOC()
{
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_REG_SYS(TStringList *AData)
{
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_IMM_SYS(TStringList *AData)
{
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::SQL_DOC_SYS(TStringList *AData)
{
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::FCreateInqStruct(TTagNode *AInqDef)
{
  AnsiString tabName,extn,DFUID,UID,DUID,prcode = AInqDef->GetChildByName("passport")->AV["projectcode"];
  AnsiString ProcSQL,tabPref;
  // ������������ ������ ��� �����������
  TTagNode *xInq = AInqDef->GetChildByName("inquirers")->GetFirstChild();     // �������� ������ ���������
  TTagNode *__tmp,*tabDefNode,*keyDefNode;
  TTagNode *flDef,*flDestDef,*flDefs,*idxDef,*trNode;
  TList *fldList = new TList;
  TList *_f = new TList;
  try
   {
     while (xInq)
      {
        fldList->Clear();                          // ������ ����� �������
        Iterate(xInq,GetFields,fldList);         // �������� ������ �����
        UID = xInq->AV["uid"];
        tabPref = prcode+UID;
        // �������� �������
        tabDefNode = TabRoot->AddChild("table");
        tabDefNode->AV["name"] = "INQ_"+tabPref;
        tabDefNode->AV["ID"] = UID;
        tabDefNode->AV["comment"] = xInq->AV["name"];
        keyDefNode = tabDefNode->AddChild("keys");
        keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
        keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
        flDefs = tabDefNode->AddChild("fields");
        flDefs->AddChild("field","name=STATE,type=SMALLINT");
        for (int i = 0; i < fldList->Count; i++)
         {
           flDef = (TTagNode*)fldList->Items[i];
           flDefs->AddChild("field","name=P"+prcode+flDef->AV["uid"])->AV["type"] = GetInqFlType(flDef);
         }
        Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabDefNode->AV["name"]);
        // �������
        idxDef = IndRoot->AddChild("index","name=INQ_"+prcode+UID+"IDX1,tabname=INQ_"+tabPref);
        idxDef->AddChild("field","name=CD");
        idxDef->AddChild("field","name=INQDATE");
        idxDef->AddChild("field","name=STATE");

        idxDef = IndRoot->AddChild("index","name=INQ_"+prcode+UID+"IDX2,tabname=INQ_"+tabPref);
        idxDef->AddChild("field","name=INQDATE");
        idxDef->AddChild("field","name=STATE");

        idxDef = IndRoot->AddChild("index","name=INQ_"+prcode+UID+"IDX3,tabname=INQ_"+tabPref);
        idxDef->AddChild("field","name=STATE");

        idxDef = IndRoot->AddChild("index","name=INQ_"+prcode+UID+"IDX4,tabname=INQ_"+tabPref);
        idxDef->AddChild("field","name=INQDATE");

        xInq = xInq->GetNext();
      }
     // ������������ ������ ��� �������� ������
     TTagNode *xOut = AInqDef->GetChildByName("outdata")->GetFirstChild();     // �������� ������ ������ �������� ������
     while (xOut)
      {
        fldList->Clear();                           // ������ ����� �������
        Iterate(xOut,GetOutFields,fldList);         // �������� ������ �����
        UID = xOut->AV["uid"];
        tabPref = prcode+UID;
        // �������� �������
        tabDefNode = TabRoot->AddChild("table");
        tabDefNode->AV["name"] = "OUT_"+tabPref;
        tabDefNode->AV["ID"] = UID;
        tabDefNode->AV["comment"] = xOut->AV["name"];
        keyDefNode = tabDefNode->AddChild("keys");
        keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
        keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
        flDefs = tabDefNode->AddChild("fields");
        flDefs->AddChild("field","name=STATE,type=SMALLINT");
        for (int i = 0; i < fldList->Count; i++)
         {
           flDef = (TTagNode*)fldList->Items[i];
           flDefs->AddChild("field","name=P"+prcode+flDef->AV["uid"]+",type=FLOAT");
         }
        // �������
        Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabDefNode->AV["name"]);
        idxDef = IndRoot->AddChild("index","name=OUT_"+tabPref+"IDX1,tabname=OUT_"+tabPref);
        idxDef->AddChild("field","name=CD");
        idxDef->AddChild("field","name=INQDATE");
        idxDef->AddChild("field","name=STATE");

        idxDef = IndRoot->AddChild("index","name=OUT_"+tabPref+"IDX2,tabname=OUT_"+tabPref);
        idxDef->AddChild("field","name=INQDATE");
        idxDef->AddChild("field","name=STATE");

        idxDef = IndRoot->AddChild("index","name=OUT_"+tabPref+"IDX3,tabname=OUT_"+tabPref);
        idxDef->AddChild("field","name=STATE");

        idxDef = IndRoot->AddChild("index","name=OUT_"+tabPref+"IDX4,tabname=OUT_"+tabPref);
        idxDef->AddChild("field","name=INQDATE");

        xOut = xOut->GetNext();
      }
     // ������������ ������ ��� �������������� �������� ������
     xOut = AInqDef->GetChildByName("distoutdata");     // �������� ������ �������������� �������� ������
     if (xOut)
      {
        xOut = xOut->GetFirstChild();
        TTagNode *dotmp;
        while (xOut)
         {
           fldList->Clear();                                 // ������ ����� �������
           Iterate(xOut,GetDistOutFields,fldList);         // �������� ������ �����
           UID = xOut->AV["uid"];
           DUID = xOut->AV["outlistuid"];
           _f->Assign(fldList);
           dotmp = xOut->GetTagByUID(DUID);
           fldList->Clear();
           Iterate(dotmp,GetOutFields,fldList);         // �������� ������ �����
           int oLen = 0;
           for (int i = 0; i < fldList->Count; i++)
            {
              flDef = (TTagNode*)fldList->Items[i];
              if (oLen < flDef->AV["name"].Length()) oLen = flDef->AV["name"].Length();
            }
           tabName = "DISTADD_"+prcode+UID+"_"+DUID;
           // ��������� �����
           GenRoot->AddChild("generator","name=GEN_"+FPrefED+prcode+UID+"_"+DUID+"_ID");
           // ������� ��������� ������ ���� �����
           trNode = TrigRoot->AddChild("TRIGGER","name=TRCODE_"+prcode+UID+"_"+DUID);
           trNode->AV["tabname"] = tabName;
           trNode->AV["position"] = "0";
           trNode->AV["type"] = "BEFORE INSERT";
           trNode->AddChild("body")->AV["pcdata"] = "NEW.CODE = GEN_ID(GEN_"+FPrefED+prcode+UID+"_"+DUID+"_ID, 1);";
           // �������� �������
           tabDefNode = TabRoot->AddChild("table");
           tabDefNode->AV["name"] = tabName;
           tabDefNode->AV["ID"] = UID+"_"+DUID;
           tabDefNode->AV["comment"] = "";
           keyDefNode = tabDefNode->AddChild("keys");
           keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
           keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
           keyDefNode->AddChild("key","name=CODE,type=INTEGER, req=NOT NULL");
           flDefs = tabDefNode->AddChild("fields");
           flDefs->AddChild("field","name=STATE,type=SMALLINT");
           flDefs->AddChild("field","name=DEPCODE,type=CHAR(4)");
           flDefs->AddChild("field","name=OUTCODE,type=CHAR(4)");
           Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabDefNode->AV["name"]);

           tabDefNode = TabRoot->AddChild("table");
           tabDefNode->AV["name"] = "DIST_"+prcode+UID+"_"+DUID;
           tabDefNode->AV["ID"] = UID+"_"+DUID;
           tabDefNode->AV["comment"] = xOut->AV["name"];
           flDefs = tabDefNode->AddChild("fields");
           flDefs->AddChild("field","name=OUTCODE,type=CHAR(4)");
           flDefs->AddChild("field","name=OUTNAME,type=VARCHAR("+IntToStr(oLen)+"), req=NOT NULL");
           flDefs->AddChild("field","name=DISTCODE,type=CHAR(4), req=NOT NULL");
           Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabDefNode->AV["name"]);

           idxDef = IndRoot->AddChild("index","name=DISTADD_"+prcode+UID+"_"+DUID+"IDX1,tabname=DISTADD_"+prcode+UID+"_"+DUID);
           idxDef->AddChild("field","name=INQDATE");
           idxDef->AddChild("field","name=CD");
           idxDef->AddChild("field","name=STATE");

           idxDef = IndRoot->AddChild("index","name=DISTADD_"+prcode+UID+"_"+DUID+"IDX2,tabname=DISTADD_"+prcode+UID+"_"+DUID);
           idxDef->AddChild("field","name=DEPCODE");
           idxDef->AddChild("field","name=OUTCODE");

           idxDef = IndRoot->AddChild("index","name=DISTADD_"+prcode+UID+"_"+DUID+"IDX3,tabname=DISTADD_"+prcode+UID+"_"+DUID);
           idxDef->AddChild("field","name=DEPCODE");

           idxDef = IndRoot->AddChild("index","name=DISTADD_"+prcode+UID+"_"+DUID+"IDX4,tabname=DISTADD_"+prcode+UID+"_"+DUID);
           idxDef->AddChild("field","name=OUTCODE");


           flDef = (TTagNode*)_f->Items[0];
           DFUID = flDef->AV["uid"];
           for (int i = 0; i < fldList->Count; i++)
            {
              flDef = (TTagNode*)fldList->Items[i];
              ExRoot->AddChild("script")->AV["pcdata"] =
                "INSERT INTO DIST_"+prcode+UID+"_"+DUID+
                "\n(OUTCODE,OUTNAME,DISTCODE) Values ('"+
                flDef->AV["uid"]+"','"+flDef->AV["name"]+"','"+DFUID+"');";
            }

           tabDefNode = TabRoot->AddChild("table");
           tabDefNode->AV["name"] = "DISTOUT_"+prcode+UID;
           tabDefNode->AV["ID"] = UID;
           tabDefNode->AV["comment"] = xOut->AV["name"];
           keyDefNode = tabDefNode->AddChild("keys");
           keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
           keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
           flDefs = tabDefNode->AddChild("fields");
           flDefs->AddChild("field","name=STATE,type=SMALLINT");
           for (int i = 0; i < _f->Count; i++)
            {
              flDef = (TTagNode*)_f->Items[i];
              flDefs->AddChild("field","name=PD"+prcode+flDef->AV["uid"]+",type=SMALLINT");
            }
           Create_SYSDATA(tabDefNode->GetChildByName("fields"), tabDefNode->AV["name"]);

           idxDef = IndRoot->AddChild("index","name=DISTOUT_"+prcode+UID+"IDX1,tabname=DISTOUT_"+prcode+UID);
           idxDef->AddChild("field","name=CD");
           idxDef->AddChild("field","name=INQDATE");
           idxDef->AddChild("field","name=STATE");

           idxDef = IndRoot->AddChild("index","name=DISTOUT_"+prcode+UID+"IDX2,tabname=DISTOUT_"+prcode+UID);
           idxDef->AddChild("field","name=INQDATE");
           idxDef->AddChild("field","name=STATE");

           idxDef = IndRoot->AddChild("index","name=DISTOUT_"+prcode+UID+"IDX3,tabname=DISTOUT_"+prcode+UID);
           idxDef->AddChild("field","name=STATE");

           idxDef = IndRoot->AddChild("index","name=DISTOUT_"+prcode+UID+"IDX4,tabname=DISTOUT_"+prcode+UID);
           idxDef->AddChild("field","name=INQDATE");

           // ��������� �����
           GenRoot->AddChild("generator","name=GEN_"+FPrefED+prcode+UID+"_ID");
           // ��������� ��������� ������ ���� �����
           CreateMCODEProc(ProcRoot->AddChild("PROCEDURE"),FPrefED+prcode+UID);

           xOut = xOut->GetNext();
         }
      }
   }
  __finally
   {
     if (fldList) delete fldList;
     if (_f) delete _f;

   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateExtInqStruct(TTagNode *AInqPrDef)
{
  TTagNode *flDefs,*trNode,*idxflDefs,*tabDefNode,*keyDefNode,*inqBlTabDef,*ExtInqBlTabDef,*inqTabDef,*inqAddDef,*inqDelDef;
  TTagNode *itNode = AInqPrDef;
  AnsiString bodyText;
  // ������������ ������� �������� ������ ������������
  inqBlTabDef = TabRoot->AddChild("table");
  inqBlTabDef->AV["name"] = "INQ_BLOB";
  inqBlTabDef->AV["ID"] = "";
  inqBlTabDef->AV["comment"] = "�������� ������ ������������";
  keyDefNode = inqBlTabDef->AddChild("keys");
  keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
  keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
  flDefs = inqBlTabDef->AddChild("fields");
  Create_SYSDATA(flDefs, "INQ_BLOB");
  flDefs->AddChild("field","name=STATE,type=SMALLINT");
  // ������������ ������� �������� ������ ������� �������������
  ExtInqBlTabDef = TabRoot->AddChild("table");
  ExtInqBlTabDef->AV["name"] = "EXT_INQ_DATA";
  ExtInqBlTabDef->AV["ID"] = "";
  ExtInqBlTabDef->AV["comment"] = "�������� ������ ������� �������������";
  keyDefNode = ExtInqBlTabDef->AddChild("keys");
  keyDefNode->AddChild("key","name=CODE,type=VARCHAR(42), req=NOT NULL");
  flDefs = ExtInqBlTabDef->AddChild("fields");
  flDefs->AddChild("field","name=comm_value,type=BLOB SUB_TYPE 1 SEGMENT SIZE 256");
  Create_SYSDATA(flDefs, "EXT_INQ_DATA");
//  flDefs->AddChild("field","name=STATE,type=SMALLINT");

  // ������������ ������� ���������� �� ������� ������������
  inqTabDef = TabRoot->AddChild("table");
  inqTabDef->AV["name"] = "inq_comments";
  inqTabDef->AV["ID"] = "";
  inqTabDef->AV["comment"] = "���������� �� ������� ������������";
  keyDefNode = inqTabDef->AddChild("keys");
  keyDefNode->AddChild("key","name=uid,type=VARCHAR(20), req=NOT NULL");
  keyDefNode->AddChild("key","name=gui,type=VARCHAR(22), req=NOT NULL");
  flDefs = inqTabDef->AddChild("fields");
  flDefs->AddChild("field","name=comm_value,type=BLOB SUB_TYPE 1 SEGMENT SIZE 512");
  Create_SYSDATA(flDefs, "inq_comments");
  // ������������ ������� ������������
  inqTabDef = TabRoot->AddChild("table");
  inqTabDef->AV["name"] = "INQUIRY";
  inqTabDef->AV["ID"] = "";
  inqTabDef->AV["comment"] = "������ ������������";
  keyDefNode = inqTabDef->AddChild("keys");
  keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
  keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
  flDefs = inqTabDef->AddChild("fields");
  flDefs->AddChild("field","name=STATE,type=SMALLINT");
  flDefs->AddChild("field","name=PBIRTHDAY,type=DATE");
  flDefs->AddChild("field","name=LAST_ED_DATE,type=DATE");
  flDefs->AddChild("field","name=INQ_GUID,type=VARCHAR(40)");
  if (DM->_EKGFL_ != "_NULL_")
   {
     inqBlTabDef->GetChildByName("fields")->AddChild("field","name="+DM->_EKGFL_+",type=BLOB SUB_TYPE 0 SEGMENT SIZE 5000");
     if (DM->_EKGCHSS_ != "_NULL_")
      {
        flDefs->AddChild("field","name=STATE"+DM->_EKGCHSS_.SubString(2,DM->_EKGCHSS_.Length()-1)+",type=SMALLINT");
        flDefs->AddChild("field","name=STATE_BL"+DM->_EKGFL_.SubString(2,DM->_EKGFL_.Length()-1)+",type=SMALLINT");
      }
     trNode = TrigRoot->AddChild("TRIGGER","name=INQ_BLOB_AU0");
     trNode->AV["tabname"] = "INQ_BLOB";
     trNode->AV["position"] = "0";
     trNode->AV["type"] = "AFTER UPDATE";
     bodyText =  "if (new."+DM->_EKGFL_+"  is not null) then\nbegin\n";
     bodyText += "update inquiry set  inquiry.STATE_BL"+DM->_EKGFL_.SubString(2,DM->_EKGFL_.Length()-1)+" = 1";
     bodyText += "where inquiry.CD=new.CD and inquiry.inqdate=new.inqdate;\nend\nelse\nbegin\n";
     bodyText += "update inquiry set  inquiry.STATE_BL"+DM->_EKGFL_.SubString(2,DM->_EKGFL_.Length()-1)+" = 0";
     bodyText += "where inquiry.CD=new.CD and inquiry.inqdate=new.inqdate;\nend";
     trNode->AddChild("body")->AV["pcdata"] = bodyText;
   }
   TStringList *delList = new TStringList;
   TStringList *addList = new TStringList;
   TStringList *inqTab = new TStringList;
   TStringList *inqFl = new TStringList;
   TStringList *inqDataFl = new TStringList;
  try
   {
     CreateAddInq(GetXML(itNode->AV["objref"]), addList, delList, inqFl, inqDataFl);
     itNode = itNode->GetFirstChild();
     while (itNode)
      {
        // ��������� ������ ��� ��������������� ������������
        if (itNode->CmpName("sub_project"))
         {
           if (itNode->CmpAV("module_type","ssdl"))
            {
              CreateAddInq(GetXML(itNode->AV["objref"]), addList, delList, inqFl, inqDataFl);
            }
         }
        else if (itNode->CmpName("plan"))
         {
           if (itNode->CmpAV("module_type","ssdl"))
            {
              CreateAddInq(GetXML(itNode->AV["objref"]), addList, delList, inqFl, inqDataFl);
            }
         }
        itNode = itNode->GetNext();
      }
     idxflDefs = IndRoot->AddChild("index","name=INQUIRYIDX5,tabname=INQUIRY");
     idxflDefs->AddChild("field","name=STATE");

     for (int i = 0; i < inqFl->Count; i++)
      {
        flDefs->AddChild("field","name="+inqFl->Strings[i]+",type=SMALLINT");
        if (inqFl->Strings[i].Length() == 7)
         idxflDefs->AddChild("field","name="+inqFl->Strings[i]);
      }
     Create_SYSDATA(flDefs, "INQUIRY");
     TTagNode *spIn, *spOut, *spVar, *spBody;
     // ��������� DEL_INQ
     delList->Add("DELETE FROM INQ_BLOB WHERE CD = :PAT_CODE AND INQDATE = :INQ_DATE;");
     delList->Add("DELETE FROM INQUIRY WHERE CD = :PAT_CODE AND INQDATE = :INQ_DATE;");
     delList->Add("SELECT MAX(INQDATE) FROM INQUIRY WHERE CD = :PAT_CODE INTO :TMPDATE;");
     delList->Add(("UPDATE "+UnitTabName+" Set LAST_INQDATE = :TMPDATE Where CODE = :PAT_CODE;").c_str());

     inqDelDef = ProcRoot->AddChild("PROCEDURE");
     inqDelDef->AV["name"] = "DEL_INQ";
     spIn = inqDelDef->AddChild("input");
     spIn->AddChild("in","name=PAT_CODE,type=INTEGER");
     spIn->AddChild("in","name=INQ_DATE,type=CHAR(12)");
     inqDelDef->AddChild("vars")->AddChild("VARIABLE","name=TMPDATE,type=DATE");
     inqDelDef->AddChild("body")->AV["pcdata"] = delList->Text;

     // ��������� ADD_INQ
     addList->Add("INSERT INTO INQ_BLOB ( CD, INQDATE, STATE)");
     addList->Add("VALUES ( :PAT_CODE, :INQ_DATE, 0);");
     addList->Add(("UPDATE "+UnitTabName+" Set LAST_INQDATE = :INQ_DATE Where CODE = :PAT_CODE;").c_str());
     addList->Add(("select R0031 From "+UnitTabName+" Where CODE = :PAT_CODE into :PatBirthDay;").c_str());
     addList->Add("INSERT INTO INQUIRY ( INQ_GUID, CD, INQDATE, PBIRTHDAY, STATE, ");
     for (int i = 0; i < inqFl->Count; i++)
      addList->Add((inqFl->Strings[i]+",").c_str());
     addList->Add("LAST_ED_DATE)\nVALUES ( :INQ_GUID, :PAT_CODE, :INQ_DATE, :PatBirthDay, 0,");
     for (int i = 0; i < inqFl->Count; i++)
      addList->Add(" 0,");
     addList->Add(":INQ_DATE);");

     inqDelDef = ProcRoot->AddChild("PROCEDURE");
     inqDelDef->AV["name"] = "ADD_INQ";
     spIn = inqDelDef->AddChild("input");
     spIn->AddChild("in","name=PAT_CODE,type=INTEGER");
     spIn->AddChild("in","name=INQ_DATE,type=CHAR(12)");
     spIn->AddChild("in","name=INQ_GUID,type=VARCHAR(40)");

     inqDelDef->AddChild("vars")->AddChild("VARIABLE","name=PatBirthDay,type=DATE");
     inqDelDef->AddChild("body")->AV["pcdata"] = addList->Text;

     idxflDefs = IndRoot->AddChild("index","name=INQUIRYIDX1,tabname=INQUIRY");
     idxflDefs->AddChild("field","name=CD");
     idxflDefs->AddChild("field","name=INQDATE");
     idxflDefs->AddChild("field","name=STATE");

     idxflDefs = IndRoot->AddChild("index","name=INQUIRYIDX2,tabname=INQUIRY");
     idxflDefs->AddChild("field","name=INQDATE");
     idxflDefs->AddChild("field","name=STATE");

     idxflDefs = IndRoot->AddChild("index","name=INQUIRYIDX3,tabname=INQUIRY");
     idxflDefs->AddChild("field","name=STATE");

     idxflDefs = IndRoot->AddChild("index","name=INQUIRYIDX4,tabname=INQUIRY");
     idxflDefs->AddChild("field","name=INQDATE");
   }
  __finally
   {
     if (delList) delete delList;
     if (addList) delete addList;
     if (inqTab) delete inqTab;
     if (inqFl) delete inqFl;
     if (inqDataFl) delete inqDataFl;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateAddInq(TTagNode *inqTag, TStringList *xAdd, TStringList *xDel, TStringList *xInqFl, TStringList *xInqDataFl)
{
    AnsiString inqTabName, DUID, UID, prcode = inqTag->GetChildByName("passport")->AV["projectcode"];
    AnsiString xxFl,xxVal,bodyText;
    xInqFl->Add(("STATE_"+prcode).c_str());
    // ������������ ������ ��� �����������
    TTagNode *xInq = inqTag->GetChildByName("inquirers")->GetFirstChild();     // �������� ������ ���������
    TTagNode *__tmp,*trNode;
    TList *fldList = new TList;
    TStringList *fl = new TStringList;
    TStringList *val = new TStringList;
    TStringList *inqDatafl = new TStringList;
    while (xInq)
     {
       fl->Clear();
       val->Clear();
       fldList->Clear();                          // ������ ����� �������
       Iterate(xInq,GetFields,fldList);         // �������� ������ �����
       UID = xInq->AV["uid"];
       inqTabName = "INQ_"+prcode+UID;
       xDel->Add(("DELETE FROM "+inqTabName+" WHERE CD = :PAT_CODE AND INQDATE = :INQ_DATE;").c_str());
       fl->Add(("INSERT INTO "+inqTabName+" ( CD, INQDATE, STATE").c_str());
       val->Add("VALUES ( :PAT_CODE, :INQ_DATE, 0");
       xxFl = ""; xxVal = "";
       int ss = 0;
       inqDatafl->Add(("STATE"+prcode+UID).c_str());
       xInqFl->Add(("STATE"+prcode+UID).c_str());

       trNode = TrigRoot->AddChild("TRIGGER","name=INQ_"+prcode+UID+"_AU0");
       trNode->AV["tabname"] = "INQ_"+prcode+UID;
       trNode->AV["position"] = "0";
       trNode->AV["type"] = "AFTER UPDATE";
       bodyText =  "update inquiry set  inquiry.STATE"+prcode+UID+" = new.state\n";
       if (strcmpi(DM->_EKGCHSSTAB_.c_str(),("INQ_"+prcode+UID).c_str()) == 0)
        {
          bodyText += ", inquiry.STATE"+DM->_EKGCHSS_.SubString(2,DM->_EKGCHSS_.Length()-1)+" = new."+DM->_EKGCHSS_;
        }
       bodyText += "\nwhere inquiry.CD=new.CD and inquiry.inqdate=new.inqdate;";
       trNode->AddChild("body")->AV["pcdata"] = bodyText;

       for (int i = 0; i < fldList->Count; i++)
        {
          __tmp = (TTagNode*)fldList->Items[i];
          xxFl  += ",P"+prcode+__tmp->AV["uid"];
          xxVal += ","+GetInqVal(__tmp);
          if (ss > 40)
           {
             fl->Add(xxFl.c_str());
             val->Add(xxVal.c_str());
             xxFl = ""; xxVal = "";
             ss = 0;
           }
          ss++;
        }
       fl->Add((xxFl+")").c_str());
       val->Add((xxVal+");").c_str());
       xAdd->Text = xAdd->Text + "\n" + fl->Text + "\n" + val->Text;
       xInq = xInq->GetNext();
     }
    // ������������ ������ ��� �������� ������
    TTagNode *xOut = inqTag->GetChildByName("outdata")->GetFirstChild();     // �������� ������ ������ �������� ������
    while (xOut)
     {
       fl->Clear();
       val->Clear();
       fldList->Clear();                             // ������ ����� �������
       Iterate(xOut,GetOutFields,fldList);         // �������� ������ �����
       UID =  xOut->AV["uid"];
       inqTabName = "OUT_"+prcode+UID;
       xDel->Add(("DELETE FROM "+inqTabName+" WHERE CD = :PAT_CODE AND INQDATE = :INQ_DATE;").c_str());
       fl->Add(("INSERT INTO "+inqTabName+" ( CD, INQDATE, STATE").c_str());
       val->Add("VALUES ( :PAT_CODE, :INQ_DATE, 0");
       xxFl = ""; xxVal = "";
       int ss = 0;
       if (xOut->GetPrev() == NULL)
        xInqDataFl->Text = xInqDataFl->Text+"STATE"+prcode+UID+"\n"+inqDatafl->Text;
       xInqFl->Add(("STATE"+prcode+UID).c_str());

       trNode = TrigRoot->AddChild("TRIGGER","name=OUT_"+prcode+UID+"_AU0");
       trNode->AV["tabname"] = "OUT_"+prcode+UID;
       trNode->AV["position"] = "0";
       trNode->AV["type"] = "AFTER UPDATE";
       bodyText =  "update inquiry set  inquiry.STATE"+prcode+UID+" = new.state\n";
       bodyText += "where inquiry.CD=new.CD and inquiry.inqdate=new.inqdate;";
       trNode->AddChild("body")->AV["pcdata"] = bodyText;
       for (int i = 0; i < fldList->Count; i++)
        {
          __tmp = (TTagNode*)fldList->Items[i];
          xxFl  += ",P"+prcode+__tmp->AV["uid"];
          xxVal += ",0";
          if (ss > 40)
           {
             fl->Add(xxFl.c_str());
             val->Add(xxVal.c_str());
             xxFl = ""; xxVal = "";
             ss = 0;
           }
          ss++;
        }
       fl->Add((xxFl+")").c_str());
       val->Add((xxVal+");").c_str());
       xAdd->Text = xAdd->Text + "\n" + fl->Text + "\n" + val->Text;
       xOut = xOut->GetNext();
     }
    // ������������ ������ ��� �������������� �������� ������
    xOut = inqTag->GetChildByName("distoutdata");     // �������� ������ �������������� �������� ������
    if (xOut)
     {
       xOut = xOut->GetFirstChild();
       while (xOut)
        {
          fl->Clear();
          val->Clear();
          fldList->Clear();                             // ������ ����� �������
          Iterate(xOut,GetDistOutFields,fldList);         // �������� ������ �����
          UID = xOut->AV["uid"];
          DUID = xOut->AV["outlistuid"];
          inqTabName = "DISTOUT_"+prcode+UID;
          xDel->Add(("DELETE FROM "+inqTabName+" WHERE CD = :PAT_CODE AND INQDATE = :INQ_DATE;").c_str());
          xDel->Add(("DELETE FROM DISTADD_"+prcode+UID+"_"+DUID+" WHERE CD = :PAT_CODE AND INQDATE = :INQ_DATE;").c_str());
          fl->Add(("INSERT INTO "+inqTabName+" ( CD, INQDATE, STATE").c_str());
          val->Add("VALUES ( :PAT_CODE, :INQ_DATE, 0");
          xxFl = ""; xxVal = "";
          int ss = 0;
          for (int i = 0; i < fldList->Count; i++)
           {
             __tmp = (TTagNode*)fldList->Items[i];
             xxFl  += ",PD"+prcode+__tmp->AV["uid"];
             xxVal += ",0";
             if (ss > 40)
              {
                fl->Add(xxFl.c_str());
                val->Add(xxVal.c_str());
                xxFl = ""; xxVal = "";
                ss = 0;
              }
             ss++;
           }
          fl->Add((xxFl+")").c_str());
          val->Add((xxVal+");").c_str());
          xAdd->Text = xAdd->Text + "\n" + fl->Text + "\n" + val->Text;
          xOut = xOut->GetNext();
        }
     }
    delete fldList;
    delete fl;
    delete val;
    delete inqDatafl;
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::Create_SYSDATA(TTagNode *AflNode, AnsiString ATblName)
{
  TTagNode *trNode;
  AnsiString bodyText;

  AflNode->AddChild("field","name=INSERT_DT,type=TIMESTAMP,req=NOT NULL");
  AflNode->AddChild("field","name=UPDATE_DT,type=TIMESTAMP,req=NOT NULL");
//  AflNode->AddChild("field","name=USER_INS,type=VARCHAR(10),req=NOT NULL");
//  AflNode->AddChild("field","name=USER_UPD,type=VARCHAR(10),req=NOT NULL");

  trNode = TrigRoot->AddChild("TRIGGER","name="+ATblName+"_BI5");
  trNode->AV["tabname"] = ATblName;
  trNode->AV["position"] = "0";
  trNode->AV["type"] = "BEFORE INSERT";

  bodyText =  "new.INSERT_DT=current_timestamp;\n";
  bodyText += "new.UPDATE_DT=current_timestamp;";

  trNode->AddChild("body")->AV["pcdata"] = bodyText;

  trNode = TrigRoot->AddChild("TRIGGER","name="+ATblName+"_BU5");
  trNode->AV["tabname"] = ATblName;
  trNode->AV["position"] = "0";
  trNode->AV["type"] = "BEFORE UPDATE";

  trNode->AddChild("body")->AV["pcdata"] = "new.UPDATE_DT=current_timestamp;";
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::Create_Script()
{
  TStringList *FScr = new TStringList;
  try
   {
//     if (!FDBStruct) FDBStruct = new TTagNode(NULL);
//     FDBStruct->LoadFromXMLFile("c:\\ddd.xml");

     if (FDBStruct)
      {
        FCreate_Script(FScr);
        AnsiString SQLScriptName = AllXMLPath+"\\"+FPrefED+((fscUpdate)?"Update_":"")+" SQLScript.sql";
        FScr->SaveToFile(SQLScriptName);
        ShowMessage(("������ �������� � \""+SQLScriptName+"\"").c_str());
        WinExec(("\"C:\\Program Files\\HK-Software\\IB Expert\\IBExpert.exe\"  \""+SQLScriptName+"\"").c_str(),SW_SHOW);
      }
   }
  __finally
   {
     if (FScr) delete FScr;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::FCreate_Script(TStringList *AScript)
{
  if (FDBStruct->CmpName("FDBStruct"))
   {
     CreateTablesScript(FDBStruct->GetChildByName("tables"),AScript);
     CreateIndicesScript(FDBStruct->GetChildByName("indices"),AScript);
     CreateGeneratorsScript(FDBStruct->GetChildByName("generators"),AScript);
     CreateExceptionsScript(FDBStruct->GetChildByName("exceptions"),AScript);
     CreateTriggersScript(FDBStruct->GetChildByName("triggers"),AScript);
     CreateProceduresScript(FDBStruct->GetChildByName("procedures"),AScript);
     CreateExecutesScript(FDBStruct->GetChildByName("Execute"),AScript);
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTablesScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        if (itNode->CmpName("table"))
         CreateTableScript(itNode, AScript);
        itNode = itNode->GetNext();
      }
     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTableScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode)
   {
     AnsiString FPKey;
     TTagNode *FKeys,*FFields,*itNode,*FLastChild;
     FKeys   = ADefNode->GetChildByName("keys");
     FFields = ADefNode->GetChildByName("fields");
     FLastChild = NULL;
     if (FKeys)                       FLastChild = FKeys->GetLastChild();
     else if (FFields && !FLastChild) FLastChild = FFields->GetLastChild();
     if (FLastChild)
      {
        AScript->Add(("/*"+ADefNode->AV["comment"]+"*/").c_str());
        AScript->Add(("Create table "+ADefNode->AV["name"]).c_str());
        AScript->Add("(");
        // ������������ ������ �����
        itNode = NULL;
        if (FKeys) itNode = FKeys->GetFirstChild();
        while (itNode)
         {
           AScript->Add("/* key field */");
           AScript->Add((itNode->AV["name"]+" "+itNode->AV["type"]+" "+itNode->AV["req"]+",").c_str());
           itNode = itNode->GetNext();
         }
        itNode = NULL;
        if (FFields) itNode = FFields->GetFirstChild();
        while (itNode)
         {
           AScript->Add(("/* "+itNode->AV["comment"]+" */").c_str());
           if (itNode == FLastChild)
            AScript->Add((itNode->AV["name"]+" "+itNode->AV["type"]+" "+itNode->AV["req"]).c_str());
           else
            AScript->Add((itNode->AV["name"]+" "+itNode->AV["type"]+" "+itNode->AV["req"]+",").c_str());
           itNode = itNode->GetNext();
         }
        if (FKeys)
         {
           FPKey = "PRIMARY KEY (";
           itNode = FKeys->GetFirstChild();
           while (itNode)
            {
              if (itNode == FLastChild)
               FPKey += itNode->AV["name"];
              else
               FPKey += itNode->AV["name"]+", ";
              itNode = itNode->GetNext();
            }
           FPKey += ")";
           AScript->Add(FPKey.c_str());
         }
        AScript->Add(");");
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateIndicesScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     AnsiString FIdxBody;
     TTagNode *itNode,*FLastChild;
     TTagNode *itIdxNode = ADefNode->GetFirstChild();
     while (itIdxNode)
      {
        FIdxBody =  "CREATE INDEX "+itIdxNode->AV["name"]+" ON "+itIdxNode->AV["tabname"]+" (" ;
        FLastChild = itIdxNode->GetLastChild();
        itNode = itIdxNode->GetFirstChild();
        while (itNode)
         {
           if (itNode != FLastChild) FIdxBody +=  itNode->AV["name"]+",";
           else                      FIdxBody +=  itNode->AV["name"];
           itNode = itNode->GetNext();
         }
        FIdxBody += ");" ;
        AScript->Add(FIdxBody.c_str());
        itIdxNode = itIdxNode->GetNext();
      }
     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateGeneratorsScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        AScript->Add(("CREATE GENERATOR "+itNode->AV["name"]+";").c_str());
        itNode = itNode->GetNext();
      }
     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateExceptionsScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        AScript->Add(("CREATE EXCEPTION "+itNode->AV["name"]+" '"+itNode->AV["text"]+"';").c_str());
        itNode = itNode->GetNext();
      }
     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateTriggersScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     AScript->Add("SET AUTODDL OFF;");
     AScript->Add("SET TERM ^ ;");

     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        AScript->Add(("CREATE TRIGGER "+itNode->AV["name"]+" FOR "+itNode->AV["tabname"]).c_str());
        AScript->Add(("ACTIVE "+itNode->AV["type"]+" POSITION "+itNode->AV["position"]).c_str());
        AScript->Add("AS");
        CreateVarsList(itNode->GetChildByName("vars"), AScript);
        AScript->Add("BEGIN");
        AScript->Add(itNode->GetChildByName("body")->AV["pcdata"].c_str());
        AScript->Add("END^");
        AScript->Add("");
        itNode = itNode->GetNext();
      }
     AScript->Add("SET TERM ; ^");
     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateVarsList(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        AScript->Add(("DECLARE VARIABLE "+itNode->AV["name"]+" "+itNode->AV["type"]+";").c_str());
        itNode = itNode->GetNext();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateFieldList(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     TTagNode *FLastChild = ADefNode->GetLastChild();
     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        if (itNode != FLastChild) AScript->Add((itNode->AV["name"]+" "+itNode->AV["type"]+",").c_str());
        else                      AScript->Add((itNode->AV["name"]+" "+itNode->AV["type"]).c_str());
        itNode = itNode->GetNext();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateProceduresScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     AScript->Add("SET AUTODDL OFF;");
     AScript->Add("SET TERM ^ ;");

     TTagNode *itNode = ADefNode->GetFirstChild();
     while (itNode)
      {
        AScript->Add(("CREATE OR ALTER PROCEDURE  "+itNode->AV["name"]).c_str());
        CreateInputList(itNode->GetChildByName("input"), AScript);
        CreateOutputList(itNode->GetChildByName("RETURNS"), AScript);
        AScript->Add("AS");
        CreateVarsList(itNode->GetChildByName("vars"), AScript);
        AScript->Add("BEGIN");
        AScript->Add(itNode->GetChildByName("body")->AV["pcdata"].c_str());
        AScript->Add("END^");
        AScript->Add("");
        itNode = itNode->GetNext();
      }
     AScript->Add("SET TERM ; ^");
     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateInputList(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     AScript->Add("(");
     CreateFieldList(ADefNode,AScript);
     AScript->Add(")");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateOutputList(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     AScript->Add("RETURNS");
     AScript->Add("(");
     CreateFieldList(ADefNode,AScript);
     AScript->Add(")");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateExecutesScript(TTagNode *ADefNode, TStringList *AScript)
{
  if (ADefNode && ADefNode->Count)
   {
     TTagNode *itNode = ADefNode->GetFirstChild();
     AScript->Add("Set AutoDDL ON;");
     while (itNode)
      {
        if (itNode->CmpName("script"))
         AScript->Add(itNode->AV["pcdata"]);
        itNode = itNode->GetNext();
      }
//     AScript->Add("commit;");
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateInqStruct()
{
   AnsiString PrName;
   try
    {
      Label1->Caption = "��������� �������� ������������ ... ";
      Label1->Update();
// ------ ��������� �������  ----------------------------------------------
      UnitTabName = "";
      AnsiString tmp,_tmp, PrName;
      tmp = "";
      TTagNode *itTMP;
      // ��������� �������� ��������� ��� ������������
      try
       {
         CreateStruct();
         // ��������� ������ ��� ������������
         TTagNode *DDD = new TTagNode(NULL);
         DDD->Name = "inq_card";
         itTMP = DDD->AddChild("passport");
         itTMP->AV["title"] = "�������� ����� ������������ ��������";
         itTMP->AV["GUI"] = NewGUI();

         itTMP->AV["autor"] = "����� ���";
         itTMP->AV["version"] = "0";
         itTMP->AV["release"] = "0";
         itTMP->AV["timestamp"] = "20040101111111";
         itTMP->AV["projectcode"] = "0";
         DDD->AddChild("classes");
         for (int i = 0; i < DM->SQLProj->Count; i++)
          {
            itTMP = (TTagNode*)DM->SQLProj->Items[i];
            if (itTMP->CmpAV("xmltype","project"))
             {
               itTMP = GetXML(itTMP->AV["xmlref"])->GetChildByName("project_main");
               CreateInqDefStruct(GetXML(itTMP->AV["objref"]),DDD);
               itTMP = itTMP->GetFirstChild();
               while (itTMP)
                {
                  // ��������� ������ ��� ��������������� ������������
                  if (itTMP->CmpName("sub_project"))
                   {
                     if (itTMP->CmpAV("module_type","ssdl"))
                      {
                        CreateInqDefStruct(GetXML(itTMP->AV["objref"]),DDD);
                      }
                   }
                  else if (itTMP->CmpName("plan"))
                   {
                     if (itTMP->CmpAV("module_type","ssdl"))
                      {
                        CreateInqDefStruct(GetXML(itTMP->AV["objref"]),DDD);
                      }
                   }
//                  else if (itTMP->CmpName("extdef"))
//                   CreateRegStruct(GetXML(itTMP->AV["gui"]),true);
                  itTMP = itTMP->GetNext();
                }
//               CreateExtInqStruct(DM->XMLProj->GetChildByName("project_main"));
             }
          }

         DDD->SaveToXMLFile("c:\\ddd_db.xml","");
       }
      __finally
       {
         Label1->Caption = "";
         Label1->Update();
       }
    }
   catch(Exception& E)
    {
      MessageBox(Application->Handle,E.Message.c_str(),"������ �������� ���������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Application->Handle,"������ �������� ���������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::CreateInqDefStruct(TTagNode *AInqDef, TTagNode *ADestNode)
{
  AnsiString tabName,extn,DFUID,UID,DUID,prcode = AInqDef->GetChildByName("passport")->AV["projectcode"];
  AnsiString ProcSQL,tabPref;
  // ������������ ������ ��� �����������
  TTagNode *xInq = AInqDef->GetChildByName("inquirers")->GetFirstChild();     // �������� ������ ���������
  TTagNode *__tmp,*tabDefNode,*keyDefNode;
  TTagNode *flDef,*flDestDef,*flDefs,*idxDef,*trNode;
  TList *fldList = new TList;
  TList *_f = new TList;
  try
   {
     while (xInq)
      {
        fldList->Clear();                          // ������ ����� �������
        Iterate(xInq,GetFields,fldList);         // �������� ������ �����
        UID = xInq->AV["uid"];
        tabPref = prcode+UID;
        // �������� �������
        tabDefNode = ADestNode->AddChild("unit");
        tabDefNode->AV["name"] = xInq->AV["name"];
        tabDefNode->AV["tblname"] = "INQ_"+tabPref;

        tabDefNode->AddChild("description");

        FFlPr = "P";
        for (int i = 0; i < fldList->Count; i++)
         {
           flDef = (TTagNode*)fldList->Items[i];
           AddTypedNode(tabDefNode,flDef);
         }
        xInq = xInq->GetNext();
      }
     // ������������ ������ ��� �������� ������
     TTagNode *xOut = AInqDef->GetChildByName("outdata")->GetFirstChild();     // �������� ������ ������ �������� ������
     while (xOut)
      {
        fldList->Clear();                           // ������ ����� �������
        Iterate(xOut,GetOutFields,fldList);         // �������� ������ �����
        UID = xOut->AV["uid"];
        tabPref = prcode+UID;

        // �������� �������
        tabDefNode = ADestNode->AddChild("unit");
        tabDefNode->AV["name"] = xOut->AV["name"];
        tabDefNode->AV["tblname"] = "OUT_"+tabPref;

        tabDefNode->AddChild("description");

        FFlPr = "P";
        for (int i = 0; i < fldList->Count; i++)
         {
           flDef = (TTagNode*)fldList->Items[i];
           AddTypedNode(tabDefNode,flDef);
         }
        xOut = xOut->GetNext();
      }
     // ������������ ������ ��� �������������� �������� ������
/*
     xOut = AInqDef->GetChildByName("distoutdata");     // �������� ������ �������������� �������� ������
     if (xOut)
      {
        xOut = xOut->GetFirstChild();
        TTagNode *dotmp;
        while (xOut)
         {
           fldList->Clear();                                 // ������ ����� �������
           Iterate(xOut,GetDistOutFields,fldList);         // �������� ������ �����
           UID = xOut->AV["uid"];
           DUID = xOut->AV["outlistuid"];
           _f->Assign(fldList);
           dotmp = xOut->GetTagByUID(DUID);
           fldList->Clear();
           Iterate(dotmp,GetOutFields,fldList);         // �������� ������ �����
           int oLen = 0;
           for (int i = 0; i < fldList->Count; i++)
            {
              flDef = (TTagNode*)fldList->Items[i];
              if (oLen < flDef->AV["name"].Length()) oLen = flDef->AV["name"].Length();
            }
           tabName = "DISTADD_"+prcode+UID+"_"+DUID;
           // �������� �������
           tabDefNode = TabRoot->AddChild("table");
           tabDefNode->AV["name"] = tabName;
           tabDefNode->AV["ID"] = UID+"_"+DUID;
           tabDefNode->AV["comment"] = "";
           flDefs = tabDefNode->AddChild("fields");
           flDefs->AddChild("field","name=STATE,type=SMALLINT");
           flDefs->AddChild("field","name=DEPCODE,type=CHAR(4)");
           flDefs->AddChild("field","name=OUTCODE,type=CHAR(4)");

           tabDefNode = TabRoot->AddChild("table");
           tabDefNode->AV["name"] = "DIST_"+prcode+UID+"_"+DUID;
           tabDefNode->AV["ID"] = UID+"_"+DUID;
           tabDefNode->AV["comment"] = xOut->AV["name"];
           flDefs = tabDefNode->AddChild("fields");
           flDefs->AddChild("field","name=OUTCODE,type=CHAR(4)");
           flDefs->AddChild("field","name=OUTNAME,type=VARCHAR("+IntToStr(oLen)+"), req=NOT NULL");
           flDefs->AddChild("field","name=DISTCODE,type=CHAR(4), req=NOT NULL");

           flDef = (TTagNode*)_f->Items[0];
           DFUID = flDef->AV["uid"];
           for (int i = 0; i < fldList->Count; i++)
            {
              flDef = (TTagNode*)fldList->Items[i];
              ExRoot->AddChild("script")->AV["pcdata"] =
                "INSERT INTO DIST_"+prcode+UID+"_"+DUID+
                "\n(OUTCODE,OUTNAME,DISTCODE) Values ('"+
                flDef->AV["uid"]+"','"+flDef->AV["name"]+"','"+DFUID+"');";
            }

           tabDefNode = TabRoot->AddChild("table");
           tabDefNode->AV["name"] = "DISTOUT_"+prcode+UID;
           tabDefNode->AV["ID"] = UID;
           tabDefNode->AV["comment"] = xOut->AV["name"];
           keyDefNode = tabDefNode->AddChild("keys");
           keyDefNode->AddChild("key","name=CD,type=INTEGER, req=NOT NULL");
           keyDefNode->AddChild("key","name=INQDATE,type=DATE, req=NOT NULL");
           flDefs = tabDefNode->AddChild("fields");
           flDefs->AddChild("field","name=STATE,type=SMALLINT");
           for (int i = 0; i < _f->Count; i++)
            {
              flDef = (TTagNode*)_f->Items[i];
              flDefs->AddChild("field","name=PD"+prcode+flDef->AV["uid"]+",type=SMALLINT");
            }
           xOut = xOut->GetNext();
         }
      }
*/
   }
  __finally
   {
     if (fldList) delete fldList;
     if (_f) delete _f;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSQLCreator::AddTypedNode(TTagNode *ADest,TTagNode *AFlDef)
{
    AnsiString SSS = GetScaleUID(AFlDef);
    TTagNode *sc = AFlDef->GetTagByUID(SSS);
    AnsiString PrCode = AFlDef->GetRoot()->GetChildByName("passport")->AV["projectcode"];
    TTagNode *nfl;
    // ������������ ����� �����
    if (sc->CmpName("scale_tree"))
     {
       nfl = ADest->AddChild("choiceTree");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"] = AFlDef->AV["name"];
       nfl->AV["req"] = "no";
       nfl->AV["inlist"] = "no";
       nfl->AV["dlgtitle"] = ".";
       nfl->AV["dformat"] = "extcode_text";
       nfl->AV["isedit"] = "yes";
       nfl->AV["is3D"] = "yes";
     }
    else if (sc->CmpName("scale_ext"))
     {
       nfl = ADest->AddChild("extedit");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"] = AFlDef->AV["name"];
       nfl->AV["req"] = "no";
       nfl->AV["inlist"] = "no";
       nfl->AV["fieldtype"] = "varchar";
       nfl->AV["length"] = "9";
       nfl->AV["isedit"] = "yes";
       nfl->AV["is3D"] = "yes";
     }
    else if (sc->CmpName("scale_binary"))
     {
       nfl = ADest->AddChild("binary");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"] = AFlDef->AV["name"];
       nfl->AV["req"] = "no";
       nfl->AV["inlist"] = "no";
       nfl->AV["isedit"] = "yes";
       nfl->AV["is3D"] = "yes";
       nfl->AV["default"] = (sc->CmpAV("default","�������"))?"check":"uncheck";
     }
    else if (sc->CmpName("scale_select"))
     {
       nfl = ADest->AddChild("choice");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"] = AFlDef->AV["name"];
       nfl->AV["req"] = "no";
       nfl->AV["inlist"] = "no";
       nfl->AV["isedit"] = "yes";
       nfl->AV["is3D"] = "yes";
       nfl->AV["default"] = sc->AV["default"];
       TTagNode *chEl,*chElDef;
       chElDef = sc->GetFirstChild();
       while (chElDef)
        {
          chEl = nfl->AddChild("choicevalue");
          chEl->AV["name"] = chElDef->AV["name"];
          chEl->AV["value"] = chElDef->AV["value"];
          chElDef = chElDef->GetNext();
        }
     }
    else if (sc->CmpName("scale_text"))
     {
       nfl = ADest->AddChild("text");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"] = AFlDef->AV["name"];
       nfl->AV["req"] = "no";
       nfl->AV["inlist"] = "no";
       nfl->AV["isedit"] = "yes";
       nfl->AV["is3D"] = "yes";
       nfl->AV["length"] = sc->AV["length"];
     }
    else if (sc->CmpName("scale_digit"))
     {
       nfl = ADest->AddChild("digit");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"]    = AFlDef->AV["name"];
       nfl->AV["req"]     = "no";
       nfl->AV["inlist"]  = "no";
       nfl->AV["isedit"]  = "yes";
       nfl->AV["is3D"]    = "yes";
       nfl->AV["digits"]  = sc->AV["digits"];
       nfl->AV["decimal"] = sc->AV["decimal"];
       nfl->AV["min"]     = sc->AV["min"];
       nfl->AV["max"]     = sc->AV["max"];
       nfl->AV["units"]   = sc->AV["units"];
     }
    else if (sc->CmpName("scale_diapazon"))
     {
       nfl = ADest->AddChild("digit");
       nfl->AV["uid"] = PrCode+AFlDef->AV["uid"];
       nfl->AV["field"] = FFlPr+PrCode+AFlDef->AV["uid"];
       nfl->AV["name"]    = AFlDef->AV["name"];
       nfl->AV["req"]     = "no";
       nfl->AV["inlist"]  = "no";
       nfl->AV["isedit"]  = "yes";
       nfl->AV["is3D"]    = "yes";
       nfl->AV["digits"]  = "8";
       nfl->AV["decimal"] = "3";
       nfl->AV["min"]     = sc->AV["min"];
       nfl->AV["max"]     = sc->AV["max"];
     }
}
//---------------------------------------------------------------------------

