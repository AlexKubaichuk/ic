// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "Preview.h"
#include "ExtUtils.h"
// ---------------------------------------------------------------------------
USEFORM("..\..\..\..\_ECBXE7Comp\ics\ICSDOCPreview\Src\Preload.cpp", PreloadForm);
USEFORM("..\..\..\..\_ECBXE7Comp\ics\ICSDOCPreview\Src\Preview.cpp", PreviewForm);
USEFORM("..\..\..\..\_ECBXE7Comp\ics\ICSDOCPreview\Src\PrnStatus.cpp", PrnStatusForm);
USEFORM("..\..\..\..\_ECBXE7Comp\ics\ICSDOCPreview\Src\PageSetup.cpp", PageSetupForm);
// ---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    Application->MainFormOnTaskBar = false;
    if (ParamStr(1).Length())
     {
      if (FileExists(ParamStr(1)))
       {
        TPreviewForm * Dlg = new TPreviewForm(NULL);
        try
         {
          TPreviewParam param = TPreviewParam();
          if (ParamStr(2).Length())
           {
            param.Top = ParamStr(2).ToIntDef(10);
            if (ParamStr(3).Length())
             {
              param.Left = ParamStr(3).ToIntDef(10);
              if (ParamStr(4).Length())
               {
                param.Right = ParamStr(4).ToIntDef(10);
                if (ParamStr(5).Length())
                 {
                  param.Bottom = ParamStr(5).ToIntDef(10);
                  if (ParamStr(6).Length())
                   {
                    param.PrintScale = ParamStr(6).ToIntDef(10);
                    if (ParamStr(7).Length())
                     param.Orientation = (Printers::TPrinterOrientation)(ParamStr(7).ToIntDef(0));
                    if (ParamStr(8).Length())
                     param.ZoomType = TPreviewZoom(ParamStr(8).ToIntDef(0));
                   }
                 }
               }
             }
           }
          Dlg->Preview(ParamStr(1), param);
         }
        __finally
         {
          delete Dlg;
         }
       }
      else
       ShowMessage("����������� �������� ��� ���������.");
     }
    else
     ShowMessage("�� ����� �������� ��� ���������.");
    // Application->Run();
   }
  catch (System::Sysutils::Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw System::Sysutils::Exception("");
     }
    catch (System::Sysutils::Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
// ---------------------------------------------------------------------------
