//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "dxBar.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *Panel1;
 TButton *OpenBtn;
 TdxBarManager *dxBarManager1;
 TdxBarPopupMenu *dxBarPopupMenu1;
private:	// User declarations
  UnicodeString FOpenFN;
public:		// User declarations
 __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
