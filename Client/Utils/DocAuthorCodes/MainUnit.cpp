//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainUnit.h"
#include "ExtUtils.h"
#include "XMLContainer.h"
#include "System.zip.hpp"
#include "msgdef.h"
//#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma resource "*.dfm"
TMainForm * MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent * Owner)
    : TForm(Owner)
 {
  FOpenFN = "";
 }
//---------------------------------------------------------------------------
void __fastcall TMainForm::OpenBtnClick(TObject * Sender)
 {
  FOpenFN = "";
  TOpenDialog * Dlg = new TOpenDialog(NULL);
  try
   {
    Dlg->Filter = "����� ������ ��|*.zxml";
    if (Dlg->Execute())
     FOpenFN = Dlg->FileName;
   }
  __finally
   {
    delete Dlg;
   }
  if (FOpenFN.Length())
   {
    if (FileExists(FOpenFN))
     {
      TTagNode * SpecData = new TTagNode;
      try
       {
        TTagNode * itSpec, *itPassport;
        TcxTreeListNode * tmpNode;

        SpecTL->Clear();
        SpecData->LoadFromZIPXMLFile(FOpenFN);
        SpecData->SaveToZIPXMLFile(FOpenFN+".tmp");

        itSpec = SpecData->GetChildByName("specificator", true);
        while (itSpec)
         {
          itPassport = itSpec->GetChildByName("passport", true);
          if (itPassport)
           {
            tmpNode           = SpecTL->Root->AddChild();
            tmpNode->Texts[0] = itPassport->AV["GUI"];
            tmpNode->Texts[1] = itPassport->AV["mainname"];
            tmpNode->Texts[2] = itPassport->AV["autor"];
            tmpNode->Texts[3] = itPassport->AV["GUI"];
           }
          itSpec = itSpec->GetNext();
         }
       }
      __finally
       {
        delete SpecData;
       }
     }
    else
     _MSG_ERRA("���� �� ������.", "������");
   }
 }
//---------------------------------------------------------------------------
void __fastcall TMainForm::SaveBtnClick(TObject * Sender)
 {
  UnicodeString FFilePath = "";
  TSaveDialog * Dlg = new TSaveDialog(NULL);
  try
   {
    Dlg->Filter = "����� ������ ��|*.zxml";
    Dlg->Options << ofOverwritePrompt;
    if (FFilePath.Length())
     Dlg->FileName = FFilePath;
    if (Dlg->Execute())
     FFilePath = Dlg->FileName;
   }
  __finally
   {
    delete Dlg;
   }
  if (FFilePath.Length())
   {
    if (FileExists(FOpenFN))
     {
      TTagNode * SpecData = new TTagNode;
      try
       {
        TTagNode * itPassport;

        SpecData->LoadFromZIPXMLFile(FOpenFN);
        for (int i = 0; i < SpecTL->Root->Count; i++)
         {
          itPassport = SpecData->GetChildByAV("passport", "gui", SpecTL->Root->Items[i]->Texts[3], true);
          if (itPassport)
           {
            itPassport->AV["GUI"]      = SpecTL->Root->Items[i]->Texts[0];
            itPassport->AV["mainname"] = SpecTL->Root->Items[i]->Texts[1];
            itPassport->AV["autor"]    = SpecTL->Root->Items[i]->Texts[2];
           }
         }
       }
      __finally
       {
        SpecData->SaveToZIPXMLFile(FFilePath);
        SpecTL->Clear();
        FOpenFN = "";
        delete SpecData;
       }
     }
    else
     _MSG_ERRA("���� �� ������.", "������");
   }
 }
//---------------------------------------------------------------------------
