//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
 TcxTreeList *SpecTL;
 TPanel *Panel1;
 TButton *OpenBtn;
 TButton *SaveBtn;
 TcxTreeListColumn *cxTreeList1Column1;
 TcxTreeListColumn *cxTreeList1Column2;
 TcxTreeListColumn *cxTreeList1Column3;
 TcxTreeListColumn *SpecTLColumn1;
 void __fastcall OpenBtnClick(TObject *Sender);
 void __fastcall SaveBtnClick(TObject *Sender);
private:	// User declarations
  UnicodeString FOpenFN;
public:		// User declarations
 __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
