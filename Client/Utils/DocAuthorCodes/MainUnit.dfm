object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = #1056#1091#1082#1086#1080#1089#1087#1088#1072#1074#1083#1103#1083#1082#1072' '#1086#1087#1080#1089#1072#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 471
  ClientWidth = 801
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object SpecTL: TcxTreeList
    Left = 0
    Top = 41
    Width = 801
    Height = 430
    Align = alClient
    Bands = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Navigator.Buttons.CustomButtons = <>
    OptionsView.Buttons = False
    OptionsView.GridLineColor = clGrayText
    OptionsView.GridLines = tlglBoth
    OptionsView.ShowRoot = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 322
    ExplicitTop = 166
    ExplicitWidth = 250
    ExplicitHeight = 150
    object cxTreeList1Column1: TcxTreeListColumn
      Caption.Text = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      DataBinding.ValueType = 'String'
      Width = 167
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxTreeList1Column2: TcxTreeListColumn
      Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      DataBinding.ValueType = 'String'
      Width = 353
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object cxTreeList1Column3: TcxTreeListColumn
      Caption.Text = #1050#1086#1076' '#1072#1074#1090#1086#1088#1072
      DataBinding.ValueType = 'String'
      Width = 188
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object SpecTLColumn1: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 375
    ExplicitTop = 41
    ExplicitWidth = 185
    object OpenBtn: TButton
      Left = 15
      Top = 8
      Width = 75
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = OpenBtnClick
    end
    object SaveBtn: TButton
      Left = 105
      Top = 9
      Width = 75
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 1
      OnClick = SaveBtnClick
    end
  end
end
