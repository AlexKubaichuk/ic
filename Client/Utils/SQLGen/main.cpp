//---------------------------------------------------------------------------
#include <vcl.h>
//include <IniFiles.hpp>
#pragma hdrstop

#include "main.h"
#include "DMF.h"
#include "SysUtils.hpp"
//#define itTag ((TTagNode*)uiTag)
//#define AV(N,attr) N->GetAV(attr)
//#define AVUp(N,attr) N->GetAV(attr).UpperCase()
#define itAV(a) ((TTagNode*)uiTag)->AV[a]
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxMemo"
#pragma link "cxRichEdit"
#pragma resource "*.dfm"
TMainForm *MainForm;
__fastcall TMainForm::TMainForm(TComponent* Owner)
    : TForm(Owner)
{
  FDBStruct = new TTagNode(NULL);
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::SetRegField(TTagNode *itTag, AnsiString &TblName)
{
   TTagNode *__tmp;
   if (itTag->CmpName("unit,class"))
    {
      // ����� ��������
      __tmp = DomTag->GetChildByAV("IS","ref",tmpGUI+"."+AVUp(itTag,"uid"),true);
      if (!__tmp)
       {
         DomTag = DomTag->AddChild("IS");
         DomTag->AV["uid"] = DomTag->NewUID();
         DomTag->AV["ref"] = tmpGUI+"."+AVUp(itTag,"uid");
       }
      else DomTag = __tmp;
      DomTag->AV["name"] = itTag->GetFirstChild()->AV["name"];
      DomTag->AV["maintable"] = TblName;
      DomTag->AV["keyfield"] = "CODE";
      // ��������� ������ ���������
      __tmp = DomTag->GetChildByName("ATTRS");
      if (!__tmp) DomTag = DomTag->AddChild("ATTRS");
      else        DomTag = __tmp;
      // ��������� ������� ��� ���� ��������
      TTagNode *tmp;
      tmp = itTag->GetChildByName("description")->GetChildByName("fields")->GetFirstChild();

      __tmp = DomTag->GetChildByAV("ATTR","ref",tmpGUI+"."+tmp->AV["ref"].UpperCase());
      if (!__tmp)
       {
         __tmp = DomTag->AddChild("ATTR");
         __tmp->AV["uid"] = DomTag->NewUID();
         __tmp->AV["ref"] = tmpGUI+"."+tmp->AV["ref"].UpperCase();
       }
      else
       {
         if (!__tmp->CmpAV("fields","CODE"))
          {
            __tmp = DomTag->AddChild("ATTR");
            __tmp->AV["uid"] = DomTag->NewUID();
            __tmp->AV["ref"] = tmpGUI+"."+tmp->AV["ref"].UpperCase();
          }
       }
      __tmp->AV["name"] = itTag->GetChildByName("description")->AV["name"];
      __tmp->AV["fields"] = "CODE";
      __tmp->AV["column"] = GetColType(tmp->GetTagByUID(tmp->AV["ref"]),TblName);
      __tmp->AV["type"] = GetFlType(tmp->GetTagByUID(tmp->AV["ref"]));
      __tmp->AV["ref_parent"] = "";
      __tmp->AV["attrmode"] = "attr_idf";
    }
   else if (itTag->CmpName("text,binary,date,datetime,time,digit,choice,extedit,choiceTree"))
    {
      // ��������� �������� ��������� ��������
      // ����� �������� �� uid
      __tmp = DomTag->GetChildByAV("ATTR","ref",tmpGUI+"."+AVUp(itTag,"uid"));
      if (!__tmp)
        __tmp = DomTag->GetChildByAV("ATTR","name","**"+itTag->AV["name"]);

      AnsiString FProcName = "";
      AnsiString FLName = itTag->AV["name"];
      if (itTag->CmpName("binary,choice,extedit,choiceTree"))
       FProcName = "ChoiceValue";

      if (!__tmp)
       { // ������� �� ������, ������ �����
         __tmp = DomTag->AddChild("ATTR");
         __tmp->AV["uid"] = DomTag->NewUID();
         __tmp->AV["ref"] = tmpGUI+"."+AVUp(itTag,"uid");
       }
      else
       { // ������� ������, ��������� ������������ � ��� ���������
         if (__tmp->CmpAV("fields","R"+itTag->AV["uid"]) && __tmp->CmpAV("procname",FProcName))
          {
            // ������ ����� � ������������ ��������� ��������� ���������
            if (!__tmp->CmpAV("name","**"+FLName))
             { // ������������ �� ���������
               FLName += " (# "+__tmp->AV["name"].SubString(3,__tmp->AV["name"].Length()-2)+" #)";
             }
          }
         else
          {
            __tmp = DomTag->AddChild("ATTR");
            __tmp->AV["uid"] = DomTag->NewUID();
            __tmp->AV["ref"] = tmpGUI+"."+AVUp(itTag,"uid");
            FLName = "��������� ������ "+FLName;
          }
       }
      __tmp->AV["name"] = FLName;
      __tmp->AV["fields"] = "R"+AVUp(itTag,"uid");
      __tmp->AV["procname"] = FProcName;
      __tmp->AV["column"] = GetColType(itTag,TblName);
      __tmp->AV["type"] = GetFlType(itTag);
      __tmp->AV["relation"] = GetRelType(itTag,TblName);
      __tmp->AV["ref_parent"] = "";
      __tmp->AV["attrmode"] = GetAttrType(itTag);
      tmpUID->Add((AVUp(itTag,"uid")+"="+AVUp(__tmp,"uid")).c_str());
    }
   else if (itTag->CmpName("choiceDB"))
    {
      tmpUID->Add((AVUp(itTag,"uid")+"="+AddChBD_ATTR(DomTag,itTag->GetTagByUID(itTag->AV["ref"]),"","R"+AVUp(itTag,"uid"),TblName)).c_str());
    }
   return false;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::AddChBD_ATTR(TTagNode *ADomTag,TTagNode *ASrc, AnsiString ARefParent, AnsiString AParentField, AnsiString AParentTab)
{
   TTagNode *__tmp;
   __tmp = ADomTag->GetChildByAV("ATTR","ref",tmpGUI+"."+AVUp(ASrc,"uid"));
   AnsiString _UID = ADomTag->NewUID();
   if (__tmp) _UID = __tmp->AV["uid"];
   if (ASrc->GetChildByName("class"))
    AddChBD_ATTR(ADomTag,ASrc->GetChildByName("class"), _UID, AParentField,"");
   TTagNode *tmp;
   tmp = ASrc->GetChildByName("description")->GetChildByName("fields")->GetFirstChild();
   tmp = tmp->GetTagByUID(tmp->AV["ref"]);
   AnsiString FProcName = "ChoiceValue";
   AnsiString FLName = ASrc->GetChildByName("description")->AV["name"];
   if (!__tmp)
    {
      __tmp = ADomTag->AddChild("ATTR");
      __tmp->AV["uid"] = _UID;
      __tmp->AV["ref"] = tmpGUI+"."+AVUp(ASrc,"uid");
    }
   else
    { // ������� ������, ��������� ������������ � ��� ���������
      if (__tmp->CmpAV("fields",AParentField) && __tmp->CmpAV("procname",FProcName))
       {
         // ������ ����� � ������������ ��������� ��������� ���������
         if (!__tmp->CmpAV("name","**"+FLName))
          { // ������������ �� ���������
            FLName += " (# "+__tmp->AV["name"].SubString(3,__tmp->AV["name"].Length()-2)+" #)";
          }
       }
      else
       {
         __tmp = ADomTag->AddChild("ATTR");
         __tmp->AV["uid"] = ADomTag->NewUID();
         __tmp->AV["ref"] = tmpGUI+"."+AVUp(ASrc,"uid");
         FLName = "��������� ������ "+FLName;
       }
    }
   __tmp->AV["name"] = FLName;
   __tmp->AV["fields"] = AParentField;
   __tmp->AV["procname"] = FProcName;
   __tmp->AV["column"] = "CLASS_"+ASrc->AV["uid"]+".R"+tmp->AV["uid"];
   __tmp->AV["type"] = GetFlType(tmp);
   if (ARefParent == "")
    { // ���������� ��� choiceDB
      __tmp->AV["relation"] = AParentTab+"."+AParentField+"=CLASS_"+ASrc->AV["uid"]+".CODE";
    }
   else
    { // ���������� ��� class
      __tmp->AV["relation"] = "CLASS_"+ASrc->GetParent("class")->AV["uid"]+".R"+ASrc->AV["uid"]+"=CLASS_"+ASrc->AV["uid"]+".CODE";
    }
   __tmp->AV["ref_parent"] = ARefParent;
   __tmp->AV["attrmode"] = "attr_descr";
   return _UID;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::NomISUpdate(TTagNode *ARoot, AnsiString TabName)
{
//   ShowMessage(("NomISGen Start "+TabName).c_str());
   try
    {
      AnsiString _ProcName,_Param;
      TTagNode *itFld;
      TTagNode *_tmpFl;
      TTagNode *_tmpIE;
      tmpUID = new TStringList;
      //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      // ��������� �������� �������� "unit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      tmpGUI = ARoot->GetRoot()->GetChildByName("passport")->AV["gui"];
      idReg = ARoot->GetFirstChild()->GetChildByName("checkfields");
      ARoot->Iterate(SetRegField,TabName);
      // ��������� ������� ������ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
      itFld = ARoot->GetFirstChild()->GetChildByName("maskfields");
      if (itFld)
       {
         TTagNode *iDomTag = DomTag->GetParent()->GetChildByName("rulesunit");
         if (!iDomTag) DomTag = DomTag->GetParent()->AddChild("rulesunit");
         else          DomTag = DomTag->GetParent()->GetChildByName("rulesunit");
         itFld = itFld->GetFirstChild();
         if (itFld->CmpName("root")) itFld = itFld->GetFirstChild();
         while (itFld)
          {
            if (itFld->CmpName("field,fl"))
             {
               _tmpFl = itFld->GetTagByUID(itFld->AV["ref"]);
               if (!_tmpFl)
                {
                  ShowMessage("����������� ������ "+itFld->AV["ref"]);
                  _tmpIE = NULL;
                }
               else
                _tmpIE = DomTag->GetChildByAV("IE","name",_tmpFl->AV["name"]);
               if (!_tmpIE)
                {
                  _tmpIE = DomTag->AddChild("IE");
                  _tmpIE->AV["uid"] = DomTag->NewUID();
                  _tmpIE->AV["name"] = _tmpFl->AV["name"];
                }
               // ���������� ��� ���������
               _Param = "";
               if (_tmpFl->CmpName("text"))
                  _ProcName = "TextValue";
               else if (_tmpFl->CmpName("binary,digit,choice,choiceTree,choiceDB"))
                  _ProcName = "RegVal";
               else if (_tmpFl->CmpName("date,datetime,time"))
                  _ProcName = "DatePeriodLast";
               else if (_tmpFl->CmpName("extedit"))
                  _ProcName = "ExtEditValue";
               else
                ShowMessage(("���� ������ ����"+_tmpFl->Name).c_str());
               _Param += tmpUID->Values[AVUp(itFld,"ref")];
               _tmpIE->AV["procname"] = _ProcName;
               _tmpIE->AV["param"] = _Param;
               if (_tmpFl->CmpName("binary,digit,choice,choiceTree,choiceDB"))
                {
                  _tmpIE->DeleteChild();
                  _tmpIE = _tmpIE->AddChild("root");
                  _tmpIE = _tmpIE->AddChild("fl");
                  _tmpIE->AV["ref"] = AVUp(itFld,"ref");
                  _tmpIE->AV["req"] = "1";
                }
             }
            itFld = itFld->GetNext();
          }
       }
      delete tmpUID;
      //  ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
//  ShowMessage("NomISGen End");
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::SetOldNames(TTagNode *itTag, AnsiString &AFake)
{
   TTagNode *__tmp;
   if (itTag->CmpName("IS,ATTR,IE,IMOR,IOR,IAND"))
    {
      itTag->AV["name"] = "**"+itTag->AV["name"];
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::AddATTR(TTagNode *ANode, AnsiString AName, AnsiString AOtherDef)
{
  if (!ANode->GetChildByAV("ATTR","name",AName)) ANode->AddChild("ATTR","uid="+DomTag->NewUID()+",name="+AName+","+AOtherDef);
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetFlType(TTagNode *Src)
{ // ��� MS-SQL
    if (Src->CmpName("choiceTree"))              return "VARCHAR(150)";
    else if (Src->CmpName("choice"))
     {
       TTagNode *tmp = Src->GetFirstChild();
       int FLen = 0;
       while (tmp)
        {
          if (tmp->AV["name"].Length() > FLen) FLen = tmp->AV["name"].Length();
          tmp = tmp->GetNext();
        }
       return ("VARCHAR("+IntToStr(FLen)+")");
     }
    else if (Src->CmpName("binary"))             return "VARCHAR(3)";
    else if (Src->CmpName("date,time,datetime")) return "DATETIME";
    else if (Src->CmpName("text"))               return "VARCHAR("+Src->AV["length"]+")";
    else if (Src->CmpName("extedit"))            return "VARCHAR("+Src->AV["length"]+")";
    else if (Src->CmpName("digit"))
     {
        int dec = 0;
        int dig=StrToInt(Src->AV["digits"]);
        if (Src->GetAttrByName("decimal"))
         if (Src->AV["decimal"] != "")
           dec=StrToInt(Src->AV["decimal"]);
        if (dec == 0) return ((dig<5)? "SMALLINT" : "INTEGER");
        else          return "NUMERIC("+Src->AV["digits"]+","+Src->AV["decimal"]+")";
     }
    else                                         return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetColType(TTagNode *Src, AnsiString ATabName)
{ // ��� MS-SQL
    if (Src->CmpName("choice"))
     return "DIM"+Src->AV["uid"].UpperCase()+".DVAL";
    else if (Src->CmpName("binary"))
     return "(CASE When "+ATabName+".R"+Src->AV["uid"].UpperCase()+"=0 Then '���' When "+ATabName+".R"+Src->AV["uid"].UpperCase()+"=1 Then '��' End)";
    else if (Src->CmpName("choiceTree"))
     {
       TTagNode *tmp = Src->GetTagByUID(Src->AV["ref"]);
       return tmp->AV["tblname"]+"."+tmp->AV["namefield"];
     }
    else if (Src->CmpName("date,time,datetime,text,extedit,digit"))
     return ATabName+".R"+Src->AV["uid"].UpperCase();
    else
     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetRelType(TTagNode *Src, AnsiString ATabName)
{ // ��� MS-SQL
    if (Src->CmpName("choice"))
     return ATabName+".R"+Src->AV["uid"].UpperCase()+"=DIM"+Src->AV["uid"].UpperCase()+".CODE";
//    else if (Src->CmpName("binary"))
//     return ATabName+".R"+Src->AV["uid"].UpperCase()+"=DIMYESNO.CODE";
    else if (Src->CmpName("choiceTree"))
     {
       TTagNode *tmp = Src->GetTagByUID(Src->AV["ref"]);
       return ATabName+".R"+Src->AV["uid"].UpperCase()+"="+tmp->AV["tblname"]+"."+tmp->AV["codefield"];
     }
    else
     return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::GetAttrType(TTagNode *Src)
{ // ��� MS-SQL
    if (Src->CmpName("choice,choiceTree"))
     return "attr_descr";
    else if (Src->CmpName("binary"))
     return "attr_valdescr";
    else if (Src->CmpName("date,time,datetime"))
     return "attr_datetime";
    else
     return "attr_value";
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FuncCheckBtnClick(TObject *Sender)
{
  AnsiString saveTitle = OpenDlg->Title;
  AnsiString saveFilter = OpenDlg->Filter;
  TTagNode *Nom = NULL;
  TStringList *NomDll = NULL;
  try
   {
     OpenDlg->InitialDir = "c:\\Program Files\\Axe\\samples";
     OpenDlg->Title = "������� �����������";
     if (OpenDlg->Execute())
      {
        Nom = new TTagNode(NULL);
        Nom->LoadFromXMLFile(OpenDlg->FileName);
        if (Nom->CmpName("mais_ontology"))
         {
           OpenDlg->Title = "������� �������� nom_dll";
           OpenDlg->Filter = "cpp -�����|*.cpp";
           if (OpenDlg->Execute())
            {
              NomDll = new TStringList;
              NomDll->LoadFromFile(OpenDlg->FileName);
              FuncList->Lines->Clear();
              PrBar->Max = NomDll->Count;
              PrBar->Position = 0;
              for (int i = 0; i < NomDll->Count; i++)
               {
                 int StdPos = NomDll->Strings[i].AnsiPos("__stdcall");
                 if (!(NomDll->Strings[i].AnsiPos("_DLLEXP_")&&(bool)StdPos))
                  {
                    NomDll->Delete(i); i--;
                  }
                 else
                  {
                    NomDll->Strings[i] = NomDll->Strings[i].SubString(StdPos+9,NomDll->Strings[i].Length()-StdPos-8).Trim();
                    NomDll->Strings[i] = NomDll->Strings[i].SubString(1,NomDll->Strings[i].AnsiPos("(")-1).Trim();
                  }
                 PrBar->Position++;
               }
              PrBar->Position = 0;
              PrBar->Max = Nom->GetCount(true,"");
              GetFuncList(Nom, NomDll->Text);
            }
         }
        else
         {
           ShowMessage(("���� '"+OpenDlg->FileName+"'�� �������� �������������").c_str());
         }
      }
   }
  __finally
   {
     if (Nom) delete Nom;
     if (NomDll) delete NomDll;
     OpenDlg->Title = saveTitle;
     OpenDlg->Filter = saveFilter;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::GetFuncList(TTagNode *ASrc, AnsiString &AFuncList)
{
  if (ASrc->CmpName("ATTR"))
   {
     if (ASrc->AV["procname"] != "")
      {
        FFuncList[ASrc->AV["procname"]] = "A"+ASrc->AV["procname"];
        if (!AFuncList.AnsiPos(("A"+ASrc->AV["procname"]).c_str()))
         {
           if (FuncList->Lines->IndexOf(("A"+ASrc->AV["procname"]).c_str()) == -1)
            FuncList->Lines->Add("A"+ASrc->AV["procname"]);
         }
      }
   }
  else if (ASrc->CmpName("IE"))
   {
     if (ASrc->AV["procname"] != "")
      {
        FFuncList["D"+ASrc->AV["procname"]] = "Def"+ASrc->AV["procname"];
        FFuncList["S"+ASrc->AV["procname"]] = "SQL"+ASrc->AV["procname"];
        if (!AFuncList.AnsiPos(("SQL"+ASrc->AV["procname"]).c_str()))
         {
           if (FuncList->Lines->IndexOf(("Def"+ASrc->AV["procname"]).c_str()) == -1)
            FuncList->Lines->Add("Def"+ASrc->AV["procname"]);
         }
      }
   }
  TTagNode *itNode = ASrc->GetFirstChild();
  while (itNode)
   {
     GetFuncList(itNode,AFuncList);
     itNode = itNode->GetNext();
   }
  PrBar->Position++;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ImmNomEditBtnClick(TObject *Sender)
{
   try
    {
      if (NomGUIED->Text == "")
       {
         ShowMessage("�� ������ GUI ������������.");
         return;
       }
      PrBar->Position = 0; PrBar->Update();
      AnsiString Nom_GUIN,RegFN,CardFN;
      TTagNode *ImmCard, *_tmpTag, *_tmpDom, *tmpTag, *tmpRefTag;
// ------ �������� ������  -----------------------------------------------------
      ImmReg = new TTagNode(NULL);
      ImmCard = new TTagNode(NULL);

      ImmReg = GetXML("40381E23-92155860-4448");
      ImmCard = GetXML("40381E23-92155860-4448");
      if (AllXMLPath[AllXMLPath.Length()] != '\\') AllXMLPath += "\\";
// ------ ���������� ������������  ----------------------------------------------
      Label1->Caption = "���������� ������������ ... ";
      Label1->Update();
      NomTag = new TTagNode(NULL);
      NomTag = GetXML("0E291426-00005882-2493");
      // passport
      DomTag = NomTag->GetChildByName("passport");
      if (!DomTag) DomTag = NomTag->AddChild("passport");
      Nom_GUIN = NomGUIED->Text;
      if (NomGUIED->Text == "") Nom_GUIN = NewGUI();
      DomTag->AV["GUI"] = Nom_GUIN;
      DomTag->AV["mainname"] = "����������� �����������";
      DomTag->AV["autor"] = "Copyright � ��� \"���������������� ����������� �������\" �����-���������,  ���./���� (812) 717-13-19";
      DomTag->AV["version"] = IntToStr(NomTag->AV["version"].ToIntDef(0)+1);
      DomTag->AV["timestamp"] = TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss");
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "technounit"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // IS  - DocIS
      DomTag = NomTag->GetChildByName("technounit",true);
      if (!DomTag) DomTag = NomTag->AddChild("technounit");
      _tmpDom = DomTag->GetChildByName("DocIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("DocIS","uid="+DomTag->NewUID()+",name=��������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"������","ref=period,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ������)","ref=periodfr,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������ (���� ���������)","ref=periodto,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���� ������������","ref=createdate,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"�����","ref=autor,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"����� ������������","ref=createtime,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"������������ �������� �������","ref=extfiltername,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"��������� ����������� �������� ��� �������� �������","ref=extfiltervalues,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� ���","ref=oldyear,fields=no,procname=DocAttrVal");
      AddATTR(tmpTag,"���������� �����","ref=oldmonth,fields=no,procname=DocAttrVal");

      // IS  - TechIS
      _tmpDom = DomTag->GetChildByName("TechIS",true);
      if (!_tmpDom) _tmpDom = DomTag->AddChild("TechIS","uid="+DomTag->NewUID()+",name=��������� ���������");
      tmpTag  = _tmpDom->GetChildByName("ATTRS",true);
      if (!tmpTag) tmpTag  = _tmpDom->AddChild("ATTRS");
      AddATTR(tmpTag,"��� �����������","fields=no,procname=SetOrgCode");
      AddATTR(tmpTag,"����������� ������������ ���","fields=no,procname=SetLPUName");
      AddATTR(tmpTag,"������ ������������ ���","fields=no,procname=SetLPUFullName");
      AddATTR(tmpTag,"�����","fields=no,procname=SetAddr");
      AddATTR(tmpTag,"�������","fields=no,procname=SetPhone");
      AddATTR(tmpTag,"����� ����������� ����� ���","fields=no,procname=SetEmail");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->GetChildByAV("domain","name","���������������");
      if (!DomTag) DomTag = NomTag->AddChild("domain");
      DomTag->Iterate(SetOldNames,"");
      DomTag->AV["name"] = "���������������";
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = ImmReg->GetTagByUID("1000");
      NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
      DomTag = _tmpDom;
      // ��������� �������� ��������� �� ������� ���������������� �����
      _tmpTag = ImmReg->GetTagByUID("3003");
      while (_tmpTag)
       {
         DomTag = _tmpDom;
         NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      DomTag = NomTag->GetChildByAV("domain","name","������������");
      if (!DomTag) DomTag = NomTag->AddChild("domain");
      DomTag->AV["name"] = "������������";
      _tmpDom = DomTag;
      // ��������� �������� �������� "������������"
      _tmpTag = ImmReg->GetTagByUID("1000");
      NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
      DomTag = _tmpDom;
      // ��������� �������� ��������� �� ������� ���������������� �����
      _tmpTag = ImmCard->GetTagByUID("3003");
      while (_tmpTag)
       {
         DomTag = _tmpDom;
         NomISUpdate(_tmpTag, "CLASS_"+_tmpTag->AV["uid"]);
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� ������ ������ ��� ������ ��������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "���������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain");
      TStringList *isUIDs = new TStringList;
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetChildByName("refers");
      if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      tmpTag->DeleteChild();
      TTagNode *tmpRefNode;
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[i];
         tmpRefTag->AV["typerelation"] = "1M"; //1M
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CODE";
         tmpRefTag->AV["fieldout"] = "UCODE";
         tmpRefNode = NomTag->GetTagByUID(isUIDs->Strings[i]);
         if (tmpRefNode)
          {
            tmpRefNode = ImmReg->GetTagByUID(_UID(tmpRefNode->AV["ref"]));
            if (tmpRefNode)
             {
               tmpRefNode = tmpRefNode->GetChildByAV("", "ref", "1000", true);
               if (tmpRefNode)
                tmpRefTag->AV["fieldout"] = "R"+tmpRefNode->AV["uid"];
             }
          }
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetChildByName("refers");
         if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpTag->DeleteChild();
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[0];
         tmpRefTag->AV["typerelation"] = "M1"; // M1
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "UCODE";
         tmpRefTag->AV["fieldout"] = "CODE";

         tmpRefNode = ImmReg->GetTagByUID(_UID(_tmpTag->AV["ref"]));
         if (tmpRefNode)
          {
            tmpRefNode = tmpRefNode->GetChildByAV("", "ref", "1000", true);
            if (tmpRefNode)
             tmpRefTag->AV["fieldin"] = "R"+tmpRefNode->AV["uid"];
          }

         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // domain "������������"
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      _tmpDom = NomTag->GetChildByName("domain")->GetNext();
      isUIDs->Clear();
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ uid-�� ���������
      while (_tmpTag)
       {
         isUIDs->Add(AVUp(_tmpTag,"uid").c_str());
         _tmpTag = _tmpTag->GetNext();
       }
      _tmpTag = _tmpDom->GetFirstChild();
      // ��������� ������ ������
      tmpTag = _tmpTag->GetChildByName("refers");
      if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
      tmpTag->DeleteChild();
      for (int i = 1; i < isUIDs->Count; i++)
       {
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[i];
         tmpRefTag->AV["typerelation"] = "1M"; //1M
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "CODE";
         tmpRefTag->AV["fieldout"] = "UCODE";
         tmpRefNode = NomTag->GetTagByUID(isUIDs->Strings[i]);
         if (tmpRefNode)
          {
            tmpRefNode = ImmReg->GetTagByUID(_UID(tmpRefNode->AV["ref"]));
            if (tmpRefNode)
             {
               tmpRefNode = tmpRefNode->GetChildByAV("", "ref", "1000", true);
               if (tmpRefNode)
                tmpRefTag->AV["fieldout"] = "R"+tmpRefNode->AV["uid"];
             }
          }
       }
      _tmpTag = _tmpTag->GetNext();
      while (_tmpTag)
       {
         tmpTag = _tmpTag->GetChildByName("refers");
         if (!tmpTag) tmpTag = _tmpTag->GetFirstChild()->Insert("refers");
         tmpTag->DeleteChild();
         tmpRefTag = tmpTag->AddChild("refer");
         tmpRefTag->AV["ref"] = isUIDs->Strings[0];
         tmpRefTag->AV["typerelation"] = "M1"; // M1
         tmpRefTag->AV["keylink"] = "and";
         tmpRefTag = tmpRefTag->AddChild("field_refer");
         tmpRefTag->AV["fieldin"] = "UCODE";
         tmpRefTag->AV["fieldout"] = "CODE";
         tmpRefNode = ImmReg->GetTagByUID(_UID(_tmpTag->AV["ref"]));
         if (tmpRefNode)
          {
            tmpRefNode = tmpRefNode->GetChildByAV("", "ref", "1000", true);
            if (tmpRefNode)
             tmpRefTag->AV["fieldin"] = "R"+tmpRefNode->AV["uid"];
          }
         _tmpTag = _tmpTag->GetNext();
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // ��������� �����������
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      AnsiString NomenklName = AllXMLPath + NomGUIED->Text+"_new.xml";
      AnsiString AxePath;
      char *sss;
      sss = new char[255];
      GetEnvironmentVariable("AXE",sss,254);
      AxePath += AnsiString(sss);
      delete[] sss;
      AxePath += "\\";

      NomTag->Iterate(CheckRef, "");

      NomTag->SaveToXMLFile(NomenklName,"<!DOCTYPE mais_ontology SYSTEM '"+AxePath+"dtds\\mais_ontology.dtd'>");
      ShowMessage(("����������� �������� � \""+NomenklName+"\"").c_str());
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      delete NomTag; NomTag = NULL;  DomTag = NULL;
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ ������������ ������������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ ������������ ������������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::CheckRef(TTagNode *ANode, AnsiString &AID)
{
  if (ANode->CmpName("fl"))
   {
     TTagNode *ref = ImmReg->GetTagByUID(ANode->AV["ref"]);
     if (!ref)
      ANode->AV["ref"] = "� � � � � �"+ANode->AV["ref"];
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ProjectBEPropertiesButtonClick(TObject *Sender,
      int AButtonIndex)
{
   AnsiString PrName;
   try
    {
      AnsiString ProjXMLPath;
      OpenDlg->InitialDir = "c:\\Program Files\\Axe\\samples";
      OpenDlg->Filter = "����� �������,����� �������(zip)|*ICS_DATA_DEF*.xml;*ICS_DATA_DEF*.zxml";
      if (!OpenDlg->Execute())
       return;
      DM->SQLProj->Clear(); 
      if (DM->DataDef)
       delete DM->DataDef;
      DM->DataDef  = new TTagNode(NULL);
      ProjXMLPath = OpenDlg->FileName;
      if (FileExists(ProjXMLPath))
       {
         if (ExtractFileExt(ProjXMLPath) == ".xml")
          DM->DataDef->LoadFromXMLFile(ProjXMLPath);
         else
          DM->DataDef->LoadFromZIPXMLFile(ProjXMLPath);
       }
      else
       {
         ShowMessage("���� �����������");
         return;
       }
      if (!DM->DataDef->CmpName("xml_list_project"))
       {
         ShowMessage(("���� \""+ProjXMLPath+"\" �� �������� ��������� �������").c_str());
         return;
       }
      if (DM->XMLList)
       delete DM->XMLList;
      DM->XMLList = new TAxeXMLContainer();
      Label1->Caption = "�������� ������ ������� ...";
      Label1->Update();
      AnsiString xxCount = "0";
      AllXMLPath =  ExtractFilePath(ProjXMLPath);
      if (AllXMLPath[AllXMLPath.Length()] == '\\') AllXMLPath = AllXMLPath.SubString(1,AllXMLPath.Length()-1);
// ------ ������ ���������� XML-�� ������� -------------------------------------
      TTagNode *itNode = DM->DataDef->GetChildByName("content");
      if (itNode)
       {
         itNode = itNode->GetFirstChild();
         while (itNode)
          {
            if (!itNode->CmpAV("xmltype","xml_list_project"))
             {
               try
                {
                  if (!itNode->CmpAV("xmltype","vac,probe,ICS_APP_OPT"))
                   DM->XMLList->LoadAV1(AllXMLPath, itNode->AV["xmlref"]);
                  if (itNode->CmpAV("xmltype","nomenklator"))
                   NomGUIED->Text = itNode->AV["xmlref"];
                }
               catch(...)
                {
                }
               if (itNode->CmpAV("xmltype","project"))
                DM->XMLProj = DM->XMLList->GetXML(itNode->AV["xmlref"]);
               if (itNode->CmpAV("sqltype","1"))
                DM->SQLProj->Add((void*)itNode);

             }
            itNode = itNode->GetNext();
          }
       }
// ------ �������� XML-�� ������� ----------------------------------------------
//      DM->XMLProj->GetChildByName("project_main")->Iterate(LoadXMLs,AnsiString(""));
      DM->_EKGFL_      = "_NULL_";
      DM->_EKGCHSS_    = "_NULL_";
      DM->_EKGCHSSTAB_ = "_NULL_";
      if (DM->XMLProj)
       {
         TTagNode *FSetting = DM->XMLProj->GetChildByName("setting");
         AnsiString INQGUI = DM->XMLProj->GetChildByName("project_main")->AV["objref"];
         FSetting = FSetting->GetChildByAV("ext_module","name","ekg."+INQGUI,true);
         if (FSetting)
          {
            TTagNode *tmpSet;
            tmpSet = FSetting->GetChildByAV("ext_module_set","name","ekgfl");
            if (tmpSet) DM->_EKGFL_ = tmpSet->AV["value"];

            tmpSet = FSetting->GetChildByAV("ext_module_set","name","ekgchss");
            if (tmpSet) DM->_EKGCHSS_ = tmpSet->AV["value"];

            tmpSet = FSetting->GetChildByAV("ext_module_set","name","ekgchsstab");
            if (tmpSet) DM->_EKGCHSSTAB_ = tmpSet->AV["value"];
          }
       }
      PrName = DM->DataDef->GetChildByName("passport")->AV["title"];
      PrBar->Position = 0; PrBar->Update();
      Label1->Caption = "";
      Label1->Update();
      ProjectBE->Text = PrName+" ("+ProjXMLPath+")";
    }
   catch(Exception& E)
    {
      MessageBox(Handle,E.Message.c_str(),"������ �������� �������� �������",MB_ICONSTOP);
      return;
    }
  catch (...)
    {
      MessageBox(Handle,"������ �������� �������� �������","������",MB_ICONSTOP);
      return;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button7Click(TObject *Sender)
{
  TTagNode *Nom = NULL;
  AnsiString NomDll = "";
  try
   {
     OpenDlg->InitialDir = "c:\\Program Files\\Axe\\samples";
     OpenDlg->Title = "������� �����������";
     if (OpenDlg->Execute())
      {
        Nom = new TTagNode(NULL);
        Nom->LoadFromXMLFile(OpenDlg->FileName);
        if (Nom->CmpName("mais_ontology"))
         {
           FFuncList.clear();
           FuncList->Lines->Clear();
           GetFuncList(Nom, NomDll);
           for (TAnsiStrMap::iterator i = FFuncList.begin(); i != FFuncList.end(); i++)
            FuncList->Lines->Add(i->second);
         }
        else
         {
           ShowMessage(("���� '"+OpenDlg->FileName+"'�� �������� �������������").c_str());
         }
      }
   }
  __finally
   {
     if (Nom) delete Nom;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
  delete FDBStruct;        
}
//---------------------------------------------------------------------------



