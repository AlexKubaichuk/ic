//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "DMF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDM *DM;
//---------------------------------------------------------------------------
__fastcall TDM::TDM(TComponent* Owner)
    : TDataModule(Owner)
{
  DataDef = NULL;
  XMLList = NULL;
  SQLProj = new TList;
}
//---------------------------------------------------------------------------
void __fastcall TDM::DMCreate(TObject *Sender)
{
//  InqXMLList = new TStringList;
  CurrentPath = GetCurrentDir();
}
//---------------------------------------------------------------------------
void __fastcall TDM::DMDestroy(TObject *Sender)
{
  if (SQLProj) delete SQLProj;
  if (XMLList) delete XMLList;
  if (DataDef) delete DataDef;
}
//---------------------------------------------------------------------------
bool __fastcall TDM::GetInqState(TStringList *AList,long ACode, TDateTime AInqDate)
{
  return true;
}
//---------------------------------------------------------------------------
TDateTime __fastcall TDM::GetMaxInqDate(long ACode)
{
   return NULL;
}
//---------------------------------------------------------------------------
int __fastcall TDM::GetInqCount(long ACode)
{
  return 0;
}
//---------------------------------------------------------------------------
void __fastcall TDM::GetInqData(long AUnitCode, TControl *Sender)
{
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetScales(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("scales"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetXML(AnsiString XML_GUI)
{
  if (DM->XMLList)
   {
     return DM->XMLList->GetXML(XML_GUI);
   }
  else
   {
     ShowMessage("����������� ����������� xml-��������");
     return NULL;
   }
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetPerson(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("person"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetInquirers(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("inquirers"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetRules(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("rules"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetPassport(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("passport"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetDepend(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("depend"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetOutData(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("outdata"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetDistOutData(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("distoutdata"));
   else
    return NULL;
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetXMLType(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->Name.UpperCase());
   else
    return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetXMLTitle(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("passport")->GetAV("title"));
   else
    return "";
}
//---------------------------------------------------------------------------
long __fastcall GetTagItemsCount(TTagNode *Src)
{
   TTagNode *itNode = Src->GetFirstChild();
   long RC;
   RC = 0;
   while (itNode)
    {
      RC++;
      if (itNode->Count) RC += GetTagItemsCount(itNode);
      itNode = itNode->GetNext();
    }
   return RC; 
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetXMLGUI(TTagNode* ANode)
{
   return (ANode->GetRoot()->GetChildByName("passport")->GetAV("gui"));
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetXMLProjectCode(TTagNode* ANode)
{
   return (ANode->GetRoot()->GetChildByName("passport")->GetAV("projectcode").UpperCase());
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetXMLProjectCode(AnsiString XML_GUI)
{
   if (GetXML(XML_GUI))
    return (GetXML(XML_GUI)->GetChildByName("passport")->GetAV("projectcode").UpperCase());
   else
    return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetFullTabName(TTagNode *AQuest)
{
  if (AQuest->CmpName("inquirer")||(AQuest->CmpName("outlist")&&AQuest->GetParent()->CmpName("outdata"))||(AQuest->CmpName("distoutlist")&&AQuest->GetParent()->CmpName("distoutdata")))
   {
     if (AQuest->CmpName("inquirer"))
      return ("INQ_"+GetXMLProjectCode(AQuest)+AQuest->GetAV("uid"));
     else if (AQuest->CmpName("outlist"))
      return ("OUT_"+GetXMLProjectCode(AQuest)+AQuest->GetAV("uid"));
     else
      return ("DISTOUT_"+GetXMLProjectCode(AQuest)+AQuest->GetAV("uid"));
   }
  else
   return GetFullTabName(AQuest->GetParent());
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetDiapazonStr(TTagNode *AOut, int AVal)
{
   TTagNode *Scale = NULL;//AOut->GetTagByUID(AOut->GetScaleUID());
   int xMin,xMax;
   AnsiString xVal;
   xMin = Scale->GetAV("min").ToInt();
   xMax = Scale->GetAV("max").ToInt();
   Scale = Scale->GetFirstChild();
   while (Scale)
    {
      xVal = Scale->GetAV("value");
      if (xVal == "<<")
       {
         if (AVal < xMin) return Scale->GetAV("name");
       }
      else if (xVal == ">>")
       {
         if (AVal > xMax) return Scale->GetAV("name");
       }
      else
       {
         if (AVal <= xVal.ToInt()) return Scale->GetAV("name");
       }
      Scale = Scale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetSelScaleStr(AnsiString AGUI, AnsiString AUID, int AVal)
{
   TTagNode *itScale = NULL;//GetXML(AGUI)->GetTagByUID(GetXML(AGUI)->GetTagByUID(AUID)->GetScaleUID())->GetFirstChild();
   while (itScale)
    {
      if (itScale->GetAV("value").ToInt() == AVal)
       {
         return itScale->GetAV("name");
       }
      itScale = itScale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetTreeScaleStr(AnsiString AGUI, AnsiString AUID, int AVal)
{
   TTagNode *itScale = NULL;//GetXML(AGUI)->GetTagByUID(GetXML(AGUI)->GetTagByUID(AUID)->GetScaleUID())->GetFirstChild();
   int iC = 0;
   if (itScale->CmpName("tree_int"))
    return GetTreeScaleStrVal(itScale,&iC,AVal);
   while (itScale)
    {
      if (itScale->GetAV("value").ToInt() == AVal)
       {
         return itScale->GetAV("name");
       }
      itScale = itScale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetTreeScaleStrVal(TTagNode *itNode, int* ICount, int AVal)
{
   TTagNode *itScale = itNode->GetFirstChild();
   AnsiString RC = "";
   while (itScale)
    {
      if (*ICount == AVal)
       return itScale->GetAV("name");
      *ICount = *ICount+1;
      if (itScale->Count)
       {
         RC = GetTreeScaleStrVal(itScale,ICount,AVal);
         if (RC != "") return RC;
       }
      itScale = itScale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetFileVersion(AnsiString AName)
{
   AnsiString RC = " ";
   const AnsiString InfoStr[10] = {"CompanyName", "FileDescription", "FileVersion", "InternalName", "LegalCopyright", "LegalTradeMarks", "OriginalFileName", "ProductName", "ProductVersion", "Comments"};
   char *ExeName = new char[AName.Length()+1];
   strcpy(ExeName,AName.c_str());
   unsigned long n;
   n = GetFileVersionInfoSize(ExeName, &n);
   if (n > 0)
    {
      char *pBuf = new char[n+1];
      void *pValue;
      unsigned int Len;
      GetFileVersionInfo(ExeName, 0, n, pBuf);
      AnsiString temp = "StringFileInfo\\041904E3\\FileVersion";
      if (VerQueryValue(pBuf, temp.c_str(), &pValue, &Len))
        RC = AnsiString((char*)pValue);
      delete pBuf;
    }
   else
    RC = "????";
   delete ExeName;
   return RC;
}
//---------------------------------------------------------------------------

