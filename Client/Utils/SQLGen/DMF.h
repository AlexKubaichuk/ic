//---------------------------------------------------------------------------
#ifndef DMFH
#define DMFH
//---------------------------------------------------------------------------
#include <Registry.hpp>
#include <Classes.hpp>
#include <DB.hpp>
#include <DBTables.hpp>
#include "XMLContainer.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <msxmldom.hpp>
#include <XMLDoc.hpp>
#include <xmldom.hpp>
#include <XMLIntf.hpp>
//---------------------------------------------------------------------------
#define quFN(a) DM->quFree->FN(a)
//#define K_REG "Software\\ICS Ltd.\\AKDO_V2"
//---------------------------------------------------------------------------
class TDM : public TDataModule
{
__published:	// IDE-managed Components
    void __fastcall DMCreate(TObject *Sender);
    void __fastcall DMDestroy(TObject *Sender);
private:	// User declarations
    AnsiString    SaveOperAND,SQLSel,FamLike,FamOper,BDate;
    AnsiString    SaveOperOR,fltPath;
    bool          ofLast,ofAspon,ofCasebook,ofAddress,ofOrgType,ofOrg,ofRegions,ofStreets;
public:		// User declarations
    TAxeXMLContainer *XMLList;
    TTagNode *DataDef;
//    TStringList  *InqXMLList;
    TTagNode     *XMLProj;
    TList        *SQLProj;
//    TTagNode     *InquiryProj;
//    TTagNode     *RegPrj;
    AnsiString   Message,CurrentPath,ChoiceName,ProdName,ProdVersion,ProdAutor,LPUFullName,_EKGFL_,_EKGCHSS_,_EKGCHSSTAB_;
    unsigned char VerSion;
    int          ErrCode;
    bool         isFirst;
    TDateTime __fastcall GetMaxInqDate(long ACode);
    bool __fastcall GetInqState(TStringList *AList, long ACode, TDateTime AInqDate);
    int __fastcall GetInqCount(long ACode);
    void __fastcall GetInqData(long AUnitCode, TControl *Sender);
    void __fastcall ProfParse(AnsiString ParseStr,AnsiString &ProfCode,AnsiString &ProfName,AnsiString &ValFrom,AnsiString &ValTo);
    bool __fastcall ChoiceApply(AnsiString FltName, TStringList *DDD);
    __fastcall TDM(TComponent* Owner);
};
//---------------------------------------------------------------------------
//unsigned short rLen(AnsiString Source, int Offs);
AnsiString __fastcall GetTreeScaleStrVal(TTagNode *itNode, int* ICount, int AVal);
extern TTagNode* __fastcall GetDepend(AnsiString XML_GUI);
extern TTagNode* __fastcall GetPerson(AnsiString XML_GUI);
extern TTagNode* __fastcall GetScales(AnsiString XML_GUI);
extern TTagNode* __fastcall GetInquirers(AnsiString XML_GUI);
extern TTagNode* __fastcall GetRules(AnsiString XML_GUI);
extern TTagNode* __fastcall GetPassport(AnsiString XML_GUI);
extern TTagNode* __fastcall GetOutData(AnsiString XML_GUI);
extern TTagNode* __fastcall GetDistOutData(AnsiString XML_GUI);
extern TTagNode* __fastcall GetXML(AnsiString XML_GUI);
extern AnsiString __fastcall GetXMLGUI(TTagNode* ANode);
extern AnsiString __fastcall GetFileVersion(AnsiString AName);
extern AnsiString __fastcall GetXMLProjectCode(TTagNode* ANode);
extern AnsiString __fastcall GetXMLProjectCode(AnsiString XML_GUI);
extern AnsiString __fastcall GetXMLType(AnsiString XML_GUI);
extern AnsiString __fastcall GetXMLTitle(AnsiString XML_GUI);
extern AnsiString __fastcall GetFullTabName(TTagNode *AQuest);
extern AnsiString __fastcall GetDiapazonStr(TTagNode *AOut, int AVal);
extern AnsiString __fastcall GetSelScaleStr(AnsiString AGUI, AnsiString AUID, int AVal);
extern AnsiString __fastcall GetTreeScaleStr(AnsiString AGUI, AnsiString AUID, int AVal);
extern long __fastcall GetTagItemsCount(TTagNode *Src);
extern PACKAGE TDM *DM;
//---------------------------------------------------------------------------
#endif

