//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include "DMF.h"
#include <Dialogs.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxMemo.hpp"
#include "cxRichEdit.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
        TProgressBar *PrBar;
        TLabel *Label1;
        TOpenDialog *OpenDlg;
        TButton *ImmNomGenBtn;
        TEdit *NomGUIED;
        TLabel *Label3;
        TButton *FuncCheckBtn;
        TMemo *FuncList;
        TcxButtonEdit *ProjectBE;
        TButton *Button7;
        void __fastcall FuncCheckBtnClick(TObject *Sender);
        void __fastcall ImmNomEditBtnClick(TObject *Sender);
        void __fastcall ProjectBEPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:
     TTagNode *idReg;
     AnsiString AllXMLPath,FGroupName,tmpGUI,xTabName;
     int    ssdl_oCount;
     TStringList *tmpUID;
     TTagNode  *iTag;
     TTagNode  *NomTag;
     TTagNode  *DomTag;
     TTagNode  *ParentTag;
     TTagNode *ImmReg;
  bool __fastcall CheckRef(TTagNode *ANode, AnsiString &AID);

     bool __fastcall SetOldNames(TTagNode *itTag, AnsiString &AFake);
     void __fastcall AddATTR(TTagNode *ANode, AnsiString AName, AnsiString AOtherDef);
     bool __fastcall SetRegField(TTagNode *itTag, AnsiString &TblName);
     void __fastcall NomISUpdate(TTagNode *ARoot, AnsiString TabName);
     void __fastcall GetFuncList(TTagNode *ASrc, AnsiString &AFuncList);
     TAnsiStrMap FFuncList;
     TTagNode *FDBStruct;

     //****************************
     AnsiString __fastcall GetFlType(TTagNode *Src);//---
     AnsiString __fastcall GetColType(TTagNode *Src, AnsiString ATabName);
     AnsiString __fastcall GetRelType(TTagNode *Src, AnsiString ATabName);
     AnsiString __fastcall AddChBD_ATTR(TTagNode *ADomTag,TTagNode *ASrc, AnsiString ARefParent, AnsiString AParentField, AnsiString AParentTab);
     AnsiString __fastcall GetAttrType(TTagNode *Src);
public:
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif

