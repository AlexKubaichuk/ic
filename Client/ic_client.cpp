﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("Pkg\dsImmCard\Src\dsICCardDMUnit.cpp", dsICCardDM); /* TDataModule: File Type */
USEFORM("Pkg\dsImmCard\Src\dsCardTestEditUnit.cpp", dsCardTestEditForm);
USEFORM("Pkg\dsPlan\Src\AddIncompMIBPUnit.cpp", AddIncompMIBPForm);
USEFORM("Pkg\dsImmCard\Src\dsInsertF63TestUnit.cpp", dsInsertF63TestForm);
USEFORM("Pkg\dsImmCard\Src\dsCardEditClientUnit.cpp", dsCardEditClientForm);
USEFORM("Pkg\dsImmCard\Src\dsCardClientUnit.cpp", dsCardClientForm);
USEFORM("Pkg\dsImmCard\Src\dsCardCheckEditUnit.cpp", dsCardCheckEditForm);
USEFORM("Pkg\dsImmCard\Src\dsCardSchListUnit.cpp", SchemaListForm);
USEFORM("Pkg\dsImmCard\Src\dsCardPrivEditUnit.cpp", dsCardPrivEditForm);
USEFORM("Pkg\dsImmCard\Src\dsCardOtvodEditUnit.cpp", dsCardOtvodEditForm);
USEFORM("Pkg\dsPlan\Src\dsICPlanSetting.cpp", PlanSettingForm);
USEFORM("Pkg\dsPlan\Src\dsICPlanDMUnit.cpp", dsICPlanDM); /* TDataModule: File Type */
USEFORM("Pkg\dsPlan\Src\dsPlanGroupCheckUnit.cpp", dsPlanGroupCheckForm);
USEFORM("Pkg\dsPlan\Src\dsPlanEditClientUnit.cpp", dsPlanEditClientForm);
USEFORM("Pkg\dsPlan\Src\dsPlanClientUnit.cpp", dsPlanClientForm);
USEFORM("Pkg\dsPlan\Src\AddPreTestUnit.cpp", AddPreTestForm);
USEFORM("Pkg\dsPlan\Src\AddPlanInfUnit.cpp", AddPlanInfForm);
USEFORM("Pkg\dsPlan\Src\AddPauseUnit.cpp", AddPauseForm);
USEFORM("Pkg\dsPlan\Src\dsICPlanCreatePlanUnit.cpp", dsCreatePlanForm);
USEFORM("Pkg\dsPlan\Src\dsCommSchListUnit.cpp", CommSchListForm);
USEFORM("Common\PatGrChange\PatOtvodGrInsert.cpp", PatOtvodGrInsertForm);
USEFORM("Common\PatGrChange\PatOrgGrChange.cpp", PatOrgGrChangeForm);
USEFORM("Common\PatGrChange\PatGrChange.cpp", PatGrChangeForm);
USEFORM("Common\ProgressUnit.cpp", dsProgressForm);
USEFORM("Common\PatGrChange\PatSchemeGrInsert.cpp", PatSchemeGrInsertForm);
USEFORM("Common\PatGrChange\PatPrivProbGrInsert.cpp", PatPrivProbGrInsertForm);
USEFORM("Common\Connect\dsConnectUnit.cpp", ConnectDM); /* TDataModule: File Type */
USEFORM("Common\Connect\DBSelect.cpp", DBSelectUnit);
USEFORM("Common\Connect\MainLPUCodeEdit.cpp", MainLPUEditForm);
USEFORM("Common\Connect\LoginUnit.cpp", LoginForm);
USEFORM("main\SetDefAddr.cpp", DefAddrForm);
USEFORM("main\MainUnit.cpp", MainForm);
USEFORM("main\DMUnit.cpp", DM); /* TDataModule: File Type */
USEFORM("main\SetSkin.cpp", SetSkinForm);
USEFORM("Common\Update\UpdateUnit.cpp", UpdateDM); /* TDataModule: File Type */
USEFORM("Common\Update\LauncherUpdater.cpp", UpdateForm);
USEFORM("Common\Update\ClientUpdateUnit.cpp", ClientUpdateForm);
USEFORM("main\DBExport\dsEIDataUnit.cpp", dsEIData); /* TDataModule: File Type */
USEFORM("main\DBExport\ImportCardOptUnit.cpp", ImportCardOptForm);
//---------------------------------------------------------------------------
#include "ClientUpdateUnit.h"
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    Application->MainFormOnTaskBar = true;
    bool Continue = false;
    TClientUpdateForm * UpdDlg = new TClientUpdateForm(NULL);
    try
     {
      Continue = UpdDlg->CheckUpdate(false);
     }
    __finally
     {
      delete UpdDlg;
     }
    if (Continue)
     {
      Application->CreateForm(__classid(TDM), &DM);
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->Run();
     }
   }
  catch (Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw Exception("Ошибка >>");
     }
    catch (Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
//---------------------------------------------------------------------------
