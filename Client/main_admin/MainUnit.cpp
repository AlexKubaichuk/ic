﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "MainUnit.h"
#include "ExtUtils.h"
#include "SrvcUtil.h"
#include "DKUtils.h"
// #include "LoginUnit.h"
#include "LauncherUpdater.h"
#include "JSONUtils.h"
#include "msgdef.h"
#include "UpdateCodeUnit.h"
#include "Winapi.TlHelp32.hpp"
// #include "SetDBPath.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxGraphics"
#pragma link "dxGDIPlusClasses"
#pragma link "ICSAboutDialog"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
TMainForm * MainForm;
const UnicodeString ICSrvcName = "ICBaseServer";
const UnicodeString ICGrdSrvcName = "ICGuardant";
const UnicodeString ICDocSrvcName = "ICDocServer";
const UnicodeString ICSrvcCapt = "УИ - ЛПУ";
const UnicodeString ICGrdSrvcCapt = "УИ - Контроллер";
const UnicodeString ICDocSrvcCapt = "УИ - Документооборот";
// ---------------------------------------------------------------------------
#define RegTB  MainTBM->Bars->Items[0]
// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent * Owner) : TForm(Owner)
 {
  FC = 0;
  // icDxLocale();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::UpdateMenuItems()
 {
  // <binary name='Администрирование' uid='001E' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  // <binary name='Ведение списка ЛПУ' uid='011F' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  bool FAdm = true;
  bool FAdmLPU = true;
  // if (DM->RT->GetTagByUID("001E"))
  // FAdm = DM->RT->GetTagByUID("001E")->AV["v"].ToIntDef(0);
  // if (DM->RT->GetTagByUID("011F"))
  // FAdmLPU = DM->RT->GetTagByUID("011F")->AV["v"].ToIntDef(0);
  if (FAdm)
   {
    actLPUList->Visible     = DM->RegDef->CmpName("registr");
    actLPUList->Enabled     = DM->RegDef->CmpName("registr");
    actUserList->Visible    = DM->RegDef->CmpName("registr");
    actUserList->Enabled    = DM->RegDef->CmpName("registr");
    actDBList->Visible      = DM->RegDef->CmpName("registr");
    actDBList->Enabled      = DM->RegDef->CmpName("registr");
    actUsersDBList->Visible = DM->RegDef->CmpName("registr");
    actUsersDBList->Enabled = DM->RegDef->CmpName("registr");
   }
  else if (FAdmLPU)
   {
    actLPUList->Visible = DM->RegDef->CmpName("registr");
    actLPUList->Enabled = DM->RegDef->CmpName("registr");
   }
  actGetUpdateCode->Visible = DM->RegDef->CmpName("registr");
  actGetUpdateCode->Enabled = DM->RegDef->CmpName("registr");
 }
// ---------------------------------------------------------------------
bool __fastcall TMainForm::CheckConnect()
 {
  bool RC = false;
  try
   {
    try
     {
      if (DM->Login())
       {
        DM->LoadRegDef();
        DM->PreloadData();
       }
      RC = true;
     }
    catch (EAccessViolation & E)
     {
      Application->MessageBox(E.Message.c_str(), L"Ошибка", MB_OK);
      Caption = Caption + "  (Ошибка соединения)";
      // Application->Terminate();
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERR("Ошибка аутентификации пользователя.\nСистемное сообщение:\n" + E.Message, "Ошибка входа в систему.");
     }
   }
  __finally
   {
    UpdateMenuItems();
    ICGB->Caption     = "УИ" + ServerStatus(ICSrvcName, actStartIC, actStopIC, actKillIC);
    ICCtrlGB->Caption = "Контроллер УИ" + ServerStatus(ICGrdSrvcName, actStartICCtrl, actStopICCtrl, actKillICCtrl);
    ICDocGB->Caption  = "УИ Документооборот" + ServerStatus(ICDocSrvcName, actStartICDoc, actStopICDoc, actKillICDoc);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject * Sender)
 {
  Application->ProcessMessages();
  // TUpdateForm * FLUpd = new TUpdateForm(this);
  try
   {
    try
     {
      // FLUpd->CheckUpdate(DM->ConnectOpt->Vals["ic_server"].AsStringDef("localhost") + ":" +
      // DM->ConnectOpt->Vals["ic_port"].AsIntDef(5100));
      if (IsServerRun(ICSrvcName) || IsServiceRun("ic_server.exe"))
       {
        if (CheckConnect())
         {
          actUpdateSearch->Enabled = ParamCount() && (ParamStr(1).Trim().LowerCase() == "/su");
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERR("Ошибка аутентификации пользователя.\nСистемное сообщение:\n" + E.Message, "Ошибка входа в систему.");
     }
   }
  __finally
   {
    // delete FLUpd;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  Action = caFree;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actHelpExecute(TObject * Sender)
 {
  Application->HelpCommand(HELP_FINDER, 4001);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actAboutExecute(TObject * Sender)
 {
  AboutDlg->FileName = icsPrePath(ParamStr(0));
  AboutDlg->Execute();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actUserListExecute(TObject * Sender)
 {
  DM->RegComp->ClsSingleEditCol = true;
  try
   {
    DM->RegComp->ShowClasses(ctList, "0", "0002");
   }
  __finally
   {
    DM->RegComp->ClsSingleEditCol = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actDBListExecute(TObject * Sender)
 {
  DM->RegComp->ClsSingleEditCol = true;
  try
   {
    DM->RegComp->ShowClasses(ctList, "0", "0008");
   }
  __finally
   {
    DM->RegComp->ClsSingleEditCol = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actRolesListExecute(TObject * Sender)
 {
  DM->RegComp->ShowClasses(ctList, "0", "0007");
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actRigthListExecute(TObject * Sender)
 {
  DM->RegComp->ShowClasses(ctList, "0", "0006");
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actRigthByRolesListExecute(TObject * Sender)
 {
  DM->RegComp->ShowClasses(ctList, "0", "0009");
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actUsersDBListExecute(TObject * Sender)
 {
  DM->RegComp->ClsSingleEditCol = false;
  try
   {
    DM->RegComp->ShowClasses(ctList, "0", "0018");
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actLPUListExecute(TObject * Sender)
 {
  DM->RegComp->ClsSingleEditCol = true;
  DM->RegComp->ShowClasses(ctList, "0", "00C7");
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actUpdateSearchExecute(TObject * Sender)
 {
  DM->Clients->IC->SetUpdateSearch(true);
  InProgress = true;
  UnicodeString SCapt = Caption;
  UnicodeString SUser = DM->Clients->dsICRestCon->UserName;
  UnicodeString SPass = DM->Clients->dsICRestCon->Password;
  try
   {
    SUUsers.clear();
    TJSONPairEnumerator * itPair;
    try
     {
      TJSONObject * RetData = DM->Clients->App->GetValById
        ("reg.s2.Select R0125 as ps1,  R0126 as ps2 From CLASS_0008 ", "0");
      itPair = RetData->GetEnumerator();
      while (itPair->MoveNext())
       {
        SUUsers[JSONEnumPairCode(itPair)] = JSONEnumPairValue(itPair);
       }
     }
    __finally
     {
     }
    // PrBar->Max      = FIdList->Count;
    PrBar->Position = 0;
    Application->ProcessMessages();
    double Delta = 0;
    double Progress = 0;
    for (TAnsiStrMap::iterator u = SUUsers.begin(); u != SUUsers.end(); u++)
     {
      DM->Clients->dsICRestCon->UserName = u->first;
      DM->Clients->dsICRestCon->Password = u->second;
      DM->Clients->SetConnectParam(u->first, u->second);
      Application->ProcessMessages();
      TJSONArray * FIdList = (TJSONArray *)DM->Clients->IC->GetIdList("reg.1000", 0, "", "");
      Delta = (double)PrBar->Max / (double)SUUsers.size() / (double)FIdList->Count;
      Application->ProcessMessages();
      UnicodeString FCode;
      TJSONObject * tmpFlList;
      for (int i = 0; (i < FIdList->Count) && FInProgress; i++)
       {
        tmpFlList = NULL;
        FCode     = ((TJSONString *)FIdList->Items[i])->Value();
        if (!FCode.Length())
         {
          tmpFlList = ((TJSONObject *)FIdList->Items[i]);
          FCode     = JSONToString(tmpFlList, "code");
         }
        DM->Clients->IC->GetValById("reg.1000:updatesearch", FCode);
        Caption = IntToStr(i) + "/" + IntToStr(FIdList->Count) + " [" + IntToStr(i * 100 / FIdList->Count) + "%]";
        Progress += Delta;
        PrBar->Position = (int)Progress;
        Application->ProcessMessages();
       }
     }
   }
  __finally
   {
    // DM->Clients->IC->SetUpdateSearch(false);
    InProgress                         = false;
    Caption                            = SCapt;
    DM->Clients->dsICRestCon->UserName = SUser;
    DM->Clients->dsICRestCon->Password = SPass;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (_MSG_QUE("Прервать?", "Сообщение") == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::SetInProgress(bool AVal)
 {
  // ApplyBtn->Caption = (AVal)? "Прервать":"Сохранить в БД";
  FInProgress    = AVal;
  PrBar->Visible = AVal;
  if (AVal)
   PrBar->Position = 0;
  // SetEnabled(!AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actSettingExecute(TObject * Sender)
 {
  DM->SrvOptDlg->Execute();
  UpdateMenuItems();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actGetUpdateCodeExecute(TObject * Sender)
 {
  TUpdateCode * Dlg = new TUpdateCode(this);
  try
   {
    Dlg->CodeMemo->Lines->Text = DM->Clients->App->GetUpdateCode();
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actDocSettingExecute(TObject * Sender)
 {
  DM->DocSrvOptDlg->Execute();
  UpdateMenuItems();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::StartServer(UnicodeString ASrvcName, UnicodeString ASrvcCapt)
 {
  DWORD FStatus;
  int RC = ServiceStatus(ASrvcName, FStatus);
  Application->ProcessMessages();
  if (!RC)
   {
    Application->ProcessMessages();
    if (FStatus == SERVICE_STOPPED)
     {
      RC = ServiceStart(ASrvcName);
      if (RC)
       _MSG_ERRA("Ошибка запуска сервера " + ASrvcCapt + "\nСистемное сообщение:\n\"" +
        Dkutils::GetAPILastErrorFormatMessage(RC) + "\"" + IntToStr(RC), EVENTLOG_ERROR_TYPE);
     }
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::StopServer(UnicodeString ASrvcName, UnicodeString ASrvcCapt)
 {
  DWORD FStatus;
  int RC = ServiceStatus(ASrvcName, FStatus);
  Application->ProcessMessages();
  if (!RC)
   {
    bool CanStartSrvc = true;
    Application->ProcessMessages();
    if (FStatus == SERVICE_RUNNING)
     {
      RC = ServiceStop(ASrvcName); // KillService(GrdSrvcName);
      if (RC)
       {
        CanStartSrvc = false;
        _MSG_ERRA("Ошибка остановки сервера " + ASrvcCapt + "\nСистемное сообщение:\n\"" +
          Dkutils::GetAPILastErrorFormatMessage(RC) + "\"", EVENTLOG_ERROR_TYPE);
       }
     }
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actStartICExecute(TObject * Sender)
 {
  // StartServer(ICSrvcName, ICSrvcCapt);
  // ServerStatus(ICSrvcName, actStartIC, actStopIC, actKillIC);
  // Sleep(50);
  // Application->ProcessMessages();
  actStartICCtrlExecute(actStartICCtrl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actStopICExecute(TObject * Sender)
 {
  actStopICCtrlExecute(actStopICCtrl);
  Sleep(50);
  Application->ProcessMessages();
  StopServer(ICSrvcName, ICSrvcCapt);
  ICGB->Caption = "УИ" + ServerStatus(ICSrvcName, actStartIC, actStopIC, actKillIC);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actStartICCtrlExecute(TObject * Sender)
 {
  StartServer(ICGrdSrvcName, ICGrdSrvcCapt);
  ICCtrlGB->Caption = "Контроллер УИ" + ServerStatus(ICGrdSrvcName, actStartICCtrl, actStopICCtrl, actKillICCtrl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actStopICCtrlExecute(TObject * Sender)
 {
  StopServer(ICGrdSrvcName, ICGrdSrvcCapt);
  ICCtrlGB->Caption = "Контроллер УИ" + ServerStatus(ICGrdSrvcName, actStartICCtrl, actStopICCtrl, actKillICCtrl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actStartICDocExecute(TObject * Sender)
 {
  StartServer(ICDocSrvcName, ICDocSrvcCapt);
  ICDocGB->Caption = "УИ Документооборот" + ServerStatus(ICDocSrvcName, actStartICDoc, actStopICDoc, actKillICDoc);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actStopICDocExecute(TObject * Sender)
 {
  StopServer(ICDocSrvcName, ICDocSrvcCapt);
  ICDocGB->Caption = "УИ Документооборот" + ServerStatus(ICDocSrvcName, actStartICDoc, actStopICDoc, actKillICDoc);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMainForm::ServerStatus(UnicodeString ASrvcName, TAction * AStart, TAction * AStop,
  TAction * AKill)
 {
  UnicodeString RC = "";
  DWORD FStatus;
  AStart->Enabled = false;
  AStop->Enabled  = false;
  AKill->Enabled  = false;
  Application->ProcessMessages();
  int SRC = ServiceStatus(ASrvcName, FStatus);
  if (!SRC)
   {
    Application->ProcessMessages();
    AStop->Enabled  = (FStatus == SERVICE_RUNNING);
    AStart->Enabled = (FStatus == SERVICE_STOPPED);
    AKill->Enabled  = (FStatus != SERVICE_STOPPED);
    Application->ProcessMessages();
   }
  else
   {
    LPVOID lpMsgBuf;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
      SRC, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
      (LPTSTR) & lpMsgBuf, 0, NULL);
    RC = static_cast<LPCTSTR>(lpMsgBuf);
    RC = " : " + RC;
    LocalFree(lpMsgBuf);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
HANDLE GetProcessHandle(wchar_t * szExeName)
 {
  HANDLE RC = NULL;
  try
   {
    Winapi::Tlhelp32::PROCESSENTRY32 Pc =
     {
      0
     };
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
    if (hSnapshot != INVALID_HANDLE_VALUE)
     {
      Pc.dwSize = sizeof(Winapi::Tlhelp32::PROCESSENTRY32);
      if (Process32First(hSnapshot, &Pc))
       {
        do
         {
          if (!_wcscmpi(Pc.szExeFile, szExeName))
           RC = OpenProcess(PROCESS_TERMINATE, false, Pc.th32ProcessID);
         }
        while (Process32Next(hSnapshot, &Pc) && !RC);
       }
      if (hSnapshot != INVALID_HANDLE_VALUE)
       CloseHandle(hSnapshot);
      if (RC == INVALID_HANDLE_VALUE)
       {
        CloseHandle(RC);
        RC = NULL;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::IsServiceRun(UnicodeString ASrvcName)
 {
  bool RC = false;
  try
   {
    try
     {
      HANDLE FExist = GetProcessHandle(ASrvcName.c_str());
      RC = FExist;
      CloseHandle(FExist);
     }
    catch (System::Sysutils::Exception & E)
     {
      MessageBox(Forms::Application->Handle, E.Message.c_str(), L"Ошибка 25", MB_ICONINFORMATION);
      RC = false;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::KillService(UnicodeString ASrvcName)
 {
  bool RC = true;
  try
   {
    try
     {
      int x;
      HANDLE kill = GetProcessHandle(ASrvcName.c_str());
      DWORD fdwExit = 0;
      GetExitCodeProcess(kill, & fdwExit);
      TerminateProcess(kill, fdwExit);
      x  = CloseHandle(kill);
      RC = false;
     }
    catch (System::Sysutils::Exception & E)
     {
      MessageBox(Forms::Application->Handle, E.Message.c_str(), L"Ошибка 23", MB_ICONINFORMATION);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::PCChange(TObject * Sender)
 {
  if (PC->ActivePage == ServiceTS)
   {
    ICGB->Caption     = "УИ" + ServerStatus(ICSrvcName, actStartIC, actStopIC, actKillIC);
    ICCtrlGB->Caption = "Контроллер УИ" + ServerStatus(ICGrdSrvcName, actStartICCtrl, actStopICCtrl, actKillICCtrl);
    ICDocGB->Caption  = "УИ Документооборот" + ServerStatus(ICDocSrvcName, actStartICDoc, actStopICDoc, actKillICDoc);
    Timer1->Enabled   = true;
   }
  else
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actKillICExecute(TObject * Sender)
 {
  KillService("ic_server.exe");
  ICGB->Caption = "УИ" + ServerStatus(ICSrvcName, actStartIC, actStopIC, actKillIC);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actKillICCtrlExecute(TObject * Sender)
 {
  KillService("ICGuardantSrvc.exe");
  ICCtrlGB->Caption = "Контроллер УИ" + ServerStatus(ICGrdSrvcName, actStartICCtrl, actStopICCtrl, actKillICCtrl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actKillICDocExecute(TObject * Sender)
 {
  KillService("ic_doc_server.exe");
  ICDocGB->Caption = "УИ Документооборот" + ServerStatus(ICDocSrvcName, actStartICDoc, actStopICDoc, actKillICDoc);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::Timer1Timer(TObject * Sender)
 {
  Timer1->Enabled   = false;
  ICGB->Caption     = "УИ" + ServerStatus(ICSrvcName, actStartIC, actStopIC, actKillIC);
  ICCtrlGB->Caption = "Контроллер УИ" + ServerStatus(ICGrdSrvcName, actStartICCtrl, actStopICCtrl, actKillICCtrl);
  ICDocGB->Caption  = "УИ Документооборот" + ServerStatus(ICDocSrvcName, actStartICDoc, actStopICDoc, actKillICDoc);
  Timer1->Enabled   = true;
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::IsServerRun(UnicodeString ASrvcName)
 {
  bool RC = false;
  try
   {
    DWORD FStatus;
    Application->ProcessMessages();
    if (!ServiceStatus(ASrvcName, FStatus))
     {
      RC = (FStatus == SERVICE_RUNNING);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
