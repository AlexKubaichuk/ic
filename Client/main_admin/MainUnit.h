//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxGraphics.hpp"
#include "dxGDIPlusClasses.hpp"
#include "ICSAboutDialog.h"
//---------------------------------------------------------------------------
#include "DMUnit.h"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
  TcxImageList *LMainIL32;
  TActionList *MainAL;
 TAction *actUserList;
 TAction *actDBList;
  TAction *actHelp;
  TAction *actAbout;
 TICSAboutDialog *AboutDlg;
 TAction *actRolesList;
 TAction *actRigthList;
 TAction *actRigthByRolesList;
 TAction *actUsersDBList;
 TAction *actLPUList;
 TProgressBar *PrBar;
 TAction *actUpdateSearch;
 TAction *actSetting;
 TAction *actGetUpdateCode;
 TAction *actDocSetting;
 TPageControl *PC;
 TTabSheet *TabSheet1;
 TTabSheet *ServiceTS;
 TImage *Image1;
 TBevel *Bevel1;
 TButton *Button5;
 TButton *Button6;
 TButton *Button4;
 TButton *Button1;
 TButton *Button3;
 TButton *Button7;
 TButton *Button2;
 TcxGroupBox *ICDocGB;
 TButton *Button8;
 TButton *Button9;
 TAction *actStartIC;
 TAction *actStopIC;
 TAction *actStartICCtrl;
 TAction *actStopICCtrl;
 TAction *actStartICDoc;
 TAction *actStopICDoc;
 TcxGroupBox *ICCtrlGB;
 TButton *Button14;
 TButton *Button15;
 TcxGroupBox *ICGB;
 TButton *Button16;
 TButton *Button17;
 TButton *Button10;
 TButton *Button11;
 TButton *Button12;
 TAction *actKillIC;
 TAction *actKillICCtrl;
 TAction *actKillICDoc;
 TTimer *Timer1;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
  void __fastcall actHelpExecute(TObject *Sender);
  void __fastcall actAboutExecute(TObject *Sender);
 void __fastcall actUserListExecute(TObject *Sender);
 void __fastcall actDBListExecute(TObject *Sender);
 void __fastcall actRolesListExecute(TObject *Sender);
 void __fastcall actRigthListExecute(TObject *Sender);
 void __fastcall actRigthByRolesListExecute(TObject *Sender);
 void __fastcall actUsersDBListExecute(TObject *Sender);
 void __fastcall actLPUListExecute(TObject *Sender);
 void __fastcall actUpdateSearchExecute(TObject *Sender);
 void __fastcall actSettingExecute(TObject *Sender);
 void __fastcall actGetUpdateCodeExecute(TObject *Sender);
 void __fastcall actDocSettingExecute(TObject *Sender);
 void __fastcall actStartICExecute(TObject *Sender);
 void __fastcall actStopICExecute(TObject *Sender);
 void __fastcall actStartICCtrlExecute(TObject *Sender);
 void __fastcall actStopICCtrlExecute(TObject *Sender);
 void __fastcall actStartICDocExecute(TObject *Sender);
 void __fastcall actStopICDocExecute(TObject *Sender);
 void __fastcall PCChange(TObject *Sender);
 void __fastcall actKillICExecute(TObject *Sender);
 void __fastcall actKillICCtrlExecute(TObject *Sender);
 void __fastcall actKillICDocExecute(TObject *Sender);
 void __fastcall Timer1Timer(TObject *Sender);

protected:
  virtual void __fastcall WndProc(Messages::TMessage &Message);
private:	// User declarations
  int FC;
  bool FInProgress;
  void __fastcall SetInProgress(bool AVal);
  bool __fastcall IsServerRun(UnicodeString ASrvcName);
  bool __fastcall IsServiceRun(UnicodeString ASrvcName);
  TAnsiStrMap SUUsers;

  void __fastcall UpdateMenuItems();
  bool __fastcall CheckConnect();
  bool __fastcall CanProgress();
  void __fastcall StartServer(UnicodeString ASrvcName, UnicodeString ASrvcCapt);
  void __fastcall StopServer(UnicodeString ASrvcName, UnicodeString ASrvcCapt);
  UnicodeString __fastcall ServerStatus(UnicodeString ASrvcName, TAction * AStart, TAction * AStop, TAction * AKill);
  bool __fastcall KillService(UnicodeString ASrvcName);

  __property bool InProgress = {read=FInProgress, write=SetInProgress};
public:		// User declarations
  __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
