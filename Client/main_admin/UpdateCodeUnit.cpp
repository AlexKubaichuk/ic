// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "UpdateCodeUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxGraphics"
#pragma link "dxBar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMemo"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TUpdateCode * UpdateCode;
// ---------------------------------------------------------------------------
__fastcall TUpdateCode::TUpdateCode(TComponent * Owner) : TForm(Owner)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateCode::dxBarButton1Click(TObject * Sender)
 {
  Clipboard()->AsText = CodeMemo->Lines->Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateCode::dxBarButton2Click(TObject * Sender)
 {
  if (SaveDlg->Execute())
   {
    CodeMemo->Lines->SaveToFile(SaveDlg->FileName);
   }
 }
// ---------------------------------------------------------------------------
