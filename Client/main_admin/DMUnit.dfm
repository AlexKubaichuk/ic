object DM: TDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 456
  Width = 661
  object SrvOptDlg: TAppOptDialog
    FormHeight = 500
    Options = SrvOpt
    OnButtonEditClick = SrvOptDlgButtonEditClick
    Left = 34
    Top = 33
  end
  object EditStyle: TcxEditStyleController
    Left = 85
    Top = 356
    PixelsPerInch = 96
  end
  object DefEditStyle: TcxDefaultEditStyleController
    Left = 150
    Top = 357
    PixelsPerInch = 96
  end
  object Skin: TdxSkinController
    Left = 27
    Top = 356
  end
  object OpenDlg: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofNoTestFileCreate, ofEnableSizing]
    Left = 272
    Top = 239
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.zxml'
    Filter = #1050#1072#1088#1090#1099' '#1055#1050' '#1059#1048'-'#1051#1055#1059' (*.zxml)|*.zxml'
    Left = 167
    Top = 303
  end
  object SrvOpt: TAppOptions
    OnLoadXML = SrvOptLoadXML
    DataProvider = SrvOptXMLData
    Left = 97
    Top = 35
  end
  object SrvOptXMLData: TAppOptXMLProv
    Left = 179
    Top = 30
  end
  object DocSrvOptDlg: TAppOptDialog
    FormHeight = 500
    Options = DocSrvOpt
    OnButtonEditClick = SrvOptDlgButtonEditClick
    Left = 34
    Top = 105
  end
  object DocSrvOpt: TAppOptions
    OnLoadXML = SrvOptLoadXML
    DataProvider = DocSrvOptXMLData
    Left = 97
    Top = 107
  end
  object DocSrvOptXMLData: TAppOptXMLProv
    Left = 179
    Top = 102
  end
  object OpenFolderDlg: TcxShellBrowserDialog
    FolderLabelCaption = #1055#1091#1090#1100' '#1082' '#1041#1044' '#1059#1048'-7.0'
    Options.ContextMenus = False
    Options.ShowHidden = True
    Options.ShowToolTip = False
    Options.ShowZipFilesWithFolders = False
    Options.TrackShellChanges = False
    Root.BrowseFolder = bfDrives
    ShowRoot = False
    Title = #1055#1091#1090#1100' '#1082' '#1041#1044' '#1059#1048'-7.0'
    Left = 392
    Top = 240
  end
end
