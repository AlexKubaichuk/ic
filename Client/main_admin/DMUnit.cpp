// ---------------------------------------------------------------------------
/*
 ����������               ���.
 �����������              �����.
 ��������                 ����.
 ������                   ����.
 ��������                 �����.
 ����                     ����
 �������                  �����.
 ���������                �����.
 �����                    �����
 �������� ���������       ����.
 ��-���������             ��-���.
 ��������������� �������� ������.
 ��������                 �����.
 ������� �                ���.�
 ���-��������             ���.
 �������������� ��������  �����.
 ������������             �����.
 ���������                �����.
 �����������              �������.
 ��������� ����           ���.��.
 ���������                ����.
 ������� ���              ��.���
 ����                     ����
 ���������                ���������
 �������� ����            ����.����
 ������� �                ���.�
 ������                   ������
 ���������� �����         ���.�����
 ������������ ��������    �������.
 ������ ���������         ����.���.
 ����� ��������� �������� ���
 */
#pragma hdrstop
#include "DMUnit.h"
#include "ExtUtils.h"
#include "msgdef.h"
#include "dsRegEDContainer.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma link "AppOptBaseProv"
#pragma link "AppOptDialog"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinsForm"
#pragma link "cxShellBrowserDialog"
#pragma resource "*.dfm"
TDM * DM;
// ---------------------------------------------------------------------------
__fastcall TDM::TDM(TComponent * Owner) : TDataModule(Owner)
 {
  FXMLList              = new TAxeXMLContainer;
  FXMLList->OnExtGetXML = FExtGetXML;
  FAppOptValNode        = new TTagNode;
  FExtFilter            = new TTagNode;
  LoadSrvOpt(SrvOpt, "srv_opt.xml");
  LoadSrvOpt(DocSrvOpt, "docsrv_opt.xml");
  UserRT   = new TTagNode;
  FClients = new TConnectDM(this, GetHostParamStr());
  // ##############################################################################
  // #                                                                            #
  // #                            TdsRegClient                                    #
  // #                                                                            #
  // ##############################################################################
  FRegComp                   = new TdsRegClient(this);
  FRegComp->XMLList          = FXMLList;
  FRegComp->OnGetClassXML    = FdsGetClassXML;
  FRegComp->OnGetCount       = FdsGetCount;
  FRegComp->OnGetIdList      = FdsGetIdList;
  FRegComp->OnGetValById     = FdsGetValById;
  FRegComp->OnGetValById10   = FdsGetValById10;
  FRegComp->OnFind           = FdsFind;
  FRegComp->OnInsertData     = FdsInsertData;
  FRegComp->OnEditData       = FdsEditData;
  FRegComp->OnDeleteData     = FdsDeleteData;
  FRegComp->FetchAll         = TdsRegFetch::Auto;
  FRegComp->ClassFetchAll    = TdsRegFetch::All;
  FRegComp->FullEdit         = false;
  FRegComp->SingleEditCol    = false;
  FRegComp->ClsSingleEditCol = false;
  FRegComp->OnExtBtnClick    = FRegCompExtEdit;
  // FRegComp->StyleController = icStyle;
  FRegComp->OnGetDefValues        = FdsOnSetDefValues;
  FRegComp->OnExtValidate         = FdsOnExtValidate;
  FRegComp->OnShowPrePlan         = NULL; // FdsOnShowPrePlan;
  FRegComp->OnPrintF63            = NULL; // FdsOnPrintF63;
  FRegComp->OnInsPriv             = NULL; // FInsPriv; // ���������� �������� ����� ��� ������ ���������
  FRegComp->StyleController       = EditStyle;
  FRegComp->DataPath              = icsPrePath(ExtractFilePath(ParamStr(0)));
  FRegComp->XMLPath               = icsPrePath(FRegComp->DataPath + "\\xmldef");
  FRegComp->UseQuickFilterSetting = false;
  FRegComp->OnExtFilterSet        = NULL; // FdsOnExtFilterSet;
  FRegComp->UseReportSetting      = false;
  FRegComp->OnPrintReportClick    = NULL; // FdsOnPrintReportClick;
  FRegDef                         = new TTagNode;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::DataModuleDestroy(TObject * Sender)
 {
  FClients->App->Logout();
  delete FExtFilter;
  delete UserRT;
  delete FRegComp;
  delete FRegDef;
  delete FAppOptValNode;
  delete FClients;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnPrintReportClick(UnicodeString ARepGUI, __int64 AUCode, UnicodeString AFilter,
  TkabProgress AProgressProc)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      // PrBar->Max = AVal;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      // Caption = FSaveCaption+"  ["+AMsg+"]";
      // PrBar->Position = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      // Caption = FSaveCaption;
      break;
     }
   }
 }
// ---------------------------------------------------------------------------
/* void __fastcall TDM::FdsOnGetReportList(TStringList *AList)
 {
 }
 //---------------------------------------------------------------------------
 */
UnicodeString __fastcall TDM::FdsOnExtFilterSet(TTagNode * ADefNode, bool & CanContinue, UnicodeString AFilterGUI)
 {
 }
// ---------------------------------------------------------------------------
/*
 void __fastcall TDM::FdsOnGetQuickFilterList(TStringList *AList)
 {
 }
 //---------------------------------------------------------------------------
 */
bool __fastcall TDM::PreloadData()
 {
  bool RC = false;
  try
   {
    LoadAppOptData();
    RC = true;
   }
  catch (System::Sysutils::Exception & E)
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetClassXML(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetClassXML(ARef);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TDM::FdsGetCount(System::UnicodeString AId, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    RC = FClients->App->GetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetIdList(AId, AMax, AFilterParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetValById(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetValById10(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->Find(AId, AFindParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsInsertData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->InsertData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetAddr(System::UnicodeString & AAddr, int AParam)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * RetData;
    UnicodeString FRetAddr;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    FRetAddr = "";
    RetData  = FClients->App->FindAddr(AAddr, "", AParam);
    itPair   = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (SameText(FClCode, "msg"))
       RC = FClStr;
      else
       FRetAddr += FClCode + "=" + FClStr + "\n";
     }
    AAddr = FRetAddr;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::AddrCodeToStr(UnicodeString AAddrCode)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FClients->App->AddrCodeToText(AAddrCode, 0x0FF);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetOrg(System::UnicodeString & AAddr)
 {
  UnicodeString RC = "error";
  /* try
   {
   TJSONObject * RetData;
   UnicodeString FRetAddr;
   UnicodeString FClCode, FClStr;
   TJSONPairEnumerator * itPair;
   FRetAddr = "";
   RetData  = FClients->Org->Find(AAddr, true);
   itPair   = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
   FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
   if (SameText(FClCode, "msg"))
   RC = FClStr;
   else
   FRetAddr += FClCode + "=" + FClStr + "\n";
   }
   AAddr = FRetAddr;
   RC = "ok";
   }
   __finally
   {
   }
   */
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::OrgCodeToStr(UnicodeString AOrgCode)
 {
  UnicodeString RC = "error";
  try
   {
    // RC = FClients->Org->OrgCodeToText(AOrgCode, "11111111");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsEditData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->EditData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->DeleteData(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FPreviewDoc(UnicodeString ATxt_Id, bool AByCode)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FHTMLPreviewDoc(UnicodeString ATxt_Id)
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FRegCompExtEdit(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource,
  bool isClicked, TObject * ACtrlList, int ABtnIdx)
 {
  bool RC = false;
#ifndef IMM_DOCUMENT
  TdsRegEDContainer * CtrList = (TdsRegEDContainer *)ACtrlList;
  bool FTemplate = !ASource;
  try
   {
    try
     {
      if (!ItTag)
       {
        Src = "_NULL_=������( �� ������ ��������)";
        // RC = false;
       }
      else
       {
        if (isClicked)
         { // click �� ������
          UnicodeString FSrc = Src.Trim();
          // <extedit name='���� � ��' uid='0011' required='1' minleft='50' linecount='2' btncount='1' length='255'/>
          if (ItTag->CmpAV("uid", "0138"))
           { // ���� � ��
            Src = SrvOpt->Vals["connecttype"].AsStringDef("1");
            Src += "=" + Src;
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "0136"))
           { // ���� � ��
            // if (Src.Length())
            // OpenDlg->FileName = Src;
            // OpenDlg->Filter = "�� \"��-���\"|ic.fdb";
            // if (OpenDlg->Execute())
            // {
            // Src = OpenDlg->FileName + "=" + OpenDlg->FileName;
            // RC  = true;
            // }
            if (Src.Length())
             OpenFolderDlg->Path = Src;
            // OpenDlg->Filter = "�� \"��-���\"|ic.fdb";
            if (OpenFolderDlg->Execute())
             {
              Src = OpenFolderDlg->Path + "=" + OpenFolderDlg->Path;
              CtrList->EditItems["0011"]->SetValue(icsPrePath(OpenFolderDlg->Path + "\\ic.fdb"));
              CtrList->EditItems["001D"]->SetValue(icsPrePath(OpenFolderDlg->Path + "\\ic_c.fdb"));
              RC = true;
             }
           }
          else if (ItTag->CmpAV("uid", "00CC"))
           { // �����
            UnicodeString FRC = GetAddr(Src, 0x1F);
            if (!SameText(FRC, "ok"))
             _MSG_ERRA(FRC, "���������");
            else
             RC = true;
           }
          else if (ItTag->CmpAV("uid", "0125,0126"))
           { // ������������, ������
            UnicodeString FId = icsNewGUID().UpperCase();
            Src = FId + "=" + FId;
            RC  = true;
           }
         }
        else
         { // ��������� ��������
          if (ItTag->CmpAV("uid", "0138"))
           { // ���� � ��
            Src = SrvOpt->Vals["connecttype"].AsStringDef("1");
            Src += "=" + Src;
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "0011"))
           { // // ���� � ��
            int FLen = CtrList->EditItems["0138"]->GetValue("").Trim().Length();
            FLen += CtrList->EditItems["0136"]->GetValue("").Trim().Length();
            if (!FLen && Src.Length())
             {
              UnicodeString DBPath = icsPrePath(ExtractFilePath(Src));
              if (DBPath.Length())
               {
                CtrList->EditItems["0138"]->SetValue("1");
                CtrList->EditItems["0136"]->SetValue(DBPath + "=" + DBPath);
               }
             }
            Src += "=" + Src;
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "0136,001D"))
           { // // ���� � ��
            Src += "=" + Src;
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "00CC"))
           { // �����
            Src += "=" + AddrCodeToStr(Src);
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "0125,0126"))
           { // ������������, ������
            Src += "=" + Src;
            RC = true;
           }
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      Src = "_NULL_=������ (" + E.Message + ")";
     }
   }
  __finally
   {
   }
#endif
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FGetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData)
 {
  bool RC = false;
  try
   {
    if (ACode)
     {
      RC = FRegComp->GetUnitData(ACode, AData);
     }
    else
     {
      FRegComp->GetSelectedUnitData(AData);
      if (AData)
       RC = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnPrintF63(__int64 Code, UnicodeString UnitStr)
 {
  try
   {
    UnicodeString F63 = "";
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnShowPrePlan(__int64 Code, UnicodeString UnitStr)
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FPlanOpenCard(__int64 Code)
 {
  UnicodeString US = "";
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadRegDef()
 {
  FRegDef->AsXML   = FClients->App->GetDefXML();
  FRegComp->RegDef = FRegDef;
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TDM::FGetXMLList()
 {
  return FXMLList;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FExtGetXML(TTagNode * ANode, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    // ANode->AsXML = FClients->Doc->GetClassZIPXML(Src);
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnSetDefValues(__int64 ARecCode, TTagNode * ANode, TdsRegEDContainer * ControlList,
  bool & Valid)
 {
  Valid = false;
  if (ANode)
   {
    try
     {
      TdsRegEDItem * tmpItem;
      UnicodeString FVal;
      if (ANode->CmpAV("uid", "00C7"))
       { // ���
        ControlList->EditItems["011E"]->SetValue(icsNewGUID().UpperCase(), "");
        Valid = true;
       }
      else if (ANode->CmpAV("uid", "0008"))
       {
        ControlList->EditItems["0138"]->SetValue(SrvOpt->Vals["connecttype"].AsStringDef("1"), "");
        ControlList->EditItems["0125"]->SetValue(icsNewGUID().UpperCase(), "");
        ControlList->EditItems["0126"]->SetValue(icsNewGUID().UpperCase(), "");
        Valid = true;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA("������ ��������� �������� �� ���������.\n��������� ���������:\n" + E.Message, "������");
     }
    catch (...)
     {
      _MSG_ERRA("������ ��������� �������� �� ���������.", "������");
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnExtValidate(__int64 ARecCode, TTagNode * ANode, TdsRegEDContainer * ControlList, bool & Valid)
 {
  // #ifndef IMM_DOCUMENT
  TdsRegEDItem * tmpItem;
  UnicodeString FVal, FVal1, FVal2;
  try
   {
    if (ANode->CmpAV("uid", "0008"))
     { // <class name='���� ������' uid='0008' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' tab=''>
      // ���� ������ ---------------------------------------------
      FVal1 = ControlList->EditItems["0011"]->GetValue("");
      // extedit name='���� � �� &quot;��-���&quot;' uid='0011' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
      FVal2 = ControlList->EditItems["001D"]->GetValue("");
      // extedit name='���� � �� &quot;��-���-���������������&quot;' uid='001D' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
      if (!(FVal1.Length() + FVal2.Length()))
       {
        _MSG_ERRA("���������� ������� ���� ��� ������� � ����� �� ��.", "������ �����");
        Valid = false;
       }
     }
    else if (ANode->CmpAV("uid", "00C7"))
     { // ���
      // ��� �����������
      FVal = ControlList->EditItems["011E"]->GetValue("").Trim();
      if (FVal.Length())
       {
        if ((FVal == "812.2741381.000") || (FVal == "812.1107704.000") || (FVal == "812.7107704.000") ||
          (FVal == "812.7177704.000") || (FVal == "812.7170425.000") || (FVal == "812.7100425.000") ||
          (FVal == "812.1100425.000") || (FVal == "812.7171319.000"))
         {
          _MSG_ERRA("��������� ��� ��� ��������������, ������� ������ ���.", "������ �����");
          Valid = false;
         }
        else
         {
          TJSONObject * RetData = NULL;
          TJSONPairEnumerator * itPair;
          bool FCont = true;
          try
           {
            FdsGetValById("reg.s2.Select Count(*) as ps1,  '" + IntToStr(ARecCode) +
              "' as ps2 From CLASS_00C7 Where code <> " + IntToStr(ARecCode) + " and Upper(R011E)='" +
              FVal.UpperCase() + "'", "0", RetData);
            itPair = RetData->GetEnumerator();
            while (itPair->MoveNext() && FCont)
             {
              if (((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0))
               {
                FCont = false;
                _MSG_ERRA("��� � ��������� ����� ��� ����������, ������� ������ ���.", "������ �����");
                Valid = false;
               }
             }
           }
          __finally
           {
            // if (RetData) delete RetData;
            // RetData = NULL;
           }
         }
       }
     }
    else if (ANode->CmpAV("uid", "0000"))
     {
     }
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA("������ �������� ������������ �������� ������.\n��������� ���������:\n" + E.Message, "������");
    Valid = false;
   }
  catch (...)
   {
    _MSG_ERRA("������ �������� ������������ �������� ������.", "������");
    Valid = false;
   }
  // #endif
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadAppOptData()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowVacSch()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowTestSch()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowCheckSch()
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetPlanOpts(UnicodeString & AOptDef)
 {
  bool RC = false;
  try
   {
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::UploadData(UnicodeString AData, TkabProgress AProgress)
 {
  try
   {
    try
     {
      int FLen;
      if (AProgress)
       AProgress(TkabProgressType::InitCommon, AData.Length(), "");
      FClients->App->UploadData("", 0);
      for (int i = 1; i <= AData.Length(); i += 2048)
       {
        FLen = 2048;
        if ((i + FLen) > AData.Length())
         FLen = AData.Length() - i + 1;
        if (AProgress)
         AProgress(TkabProgressType::CommPerc, i * 100 / AData.Length(), "");
        Application->ProcessMessages();
        FClients->App->UploadData(AData.SubString(i, FLen), 1);
        if (AProgress)
         AProgress(TkabProgressType::CommPerc, (i + 1024) * 100 / AData.Length(), "");
        Application->ProcessMessages();
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA(E.Message, "������");
     }
   }
  __finally
   {
    if (AProgress)
     AProgress(TkabProgressType::CommComplite, 0, "");
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::Connect(UnicodeString AUser, UnicodeString APaswd)
 {
  bool RC = false;
  try
   {
    Application->ProcessMessages();
    FClients->SetConnectParam(AUser, APaswd);
    try
     {
      Application->ProcessMessages();
      // FUserRt->AsXML = FClients->App->LoginUser("AdMin");
      LoadRegDef();
      Application->ProcessMessages();
      RC = true;
      // DM->Authenticate();
     }
    catch (...)
     {
     }
   }
  __finally
   {
    Application->ProcessMessages();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetHostParamStr()
 {
  UnicodeString RC = "";
  TTagNode * ConnOptNode = new TTagNode;
  try
   {
    ConnOptNode->Name                = "s";
    ConnOptNode->AV["ic_server"]     = "localhost";
    ConnOptNode->AV["ic_port"]       = SrvOpt->Vals["port"].AsIntDef(5100);
    ConnOptNode->AV["ic_doc_server"] = "localhost";
    ConnOptNode->AV["ic_doc_port"]   = DocSrvOpt->Vals["port"].AsIntDef(5200);
    ConnOptNode->AV["ic_sv_server"]  = "localhost";
    ConnOptNode->AV["ic_sv_port"]    = SrvOpt->Vals["port"].AsIntDef(5100);
    if (!ConnOptNode->AV["ic_port"].Trim().Length())
     ConnOptNode->AV["ic_port"] = "5100";
    if (!ConnOptNode->AV["ic_doc_port"].Trim().Length())
     ConnOptNode->AV["ic_doc_port"] = "5200";
    if (!ConnOptNode->AV["ic_sv_port"].Trim().Length())
     ConnOptNode->AV["ic_sv_port"] = "5100";
    RC = ConnOptNode->AsXML;
   }
  __finally
   {
    delete ConnOptNode;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::SetHostParam()
 {
  try
   {
    Application->ProcessMessages();
    FClients->SetHostParam(GetHostParamStr());
   }
  __finally
   {
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FInsPriv(TkabProgressType AStage, TTagNode * ADataDefNode, TkabCustomDataSource * ASource,
  __int64 ARecCode, TkabProgress AProgress)
 {
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::Login()
 {
  bool RC = false;
  TTagNode * LRC = new TTagNode;
  try
   {
    Application->ProcessMessages();
    try
     {
      LRC->AsXML = FClients->App->LoginSysUser("AdMin");
      if (LRC->CmpName("nouser"))
       {
        FClients->SetConnectParam("", "");
        throw System::Sysutils::Exception("������ ����� � �������.");
       }
      else if (LRC->CmpName("access_denied"))
       {
        FClients->SetConnectParam("", "");
        throw System::Sysutils::Exception("������ ����� � �������.");
       }
      RC = true;
      /* else if (LRC->CmpName("rt"))
       {
       if (LRC->Count == 1)
       {
       UserRT->AsXML = LRC->AsXML;
       }
       else
       throw System::Sysutils::Exception("������ ����� � �������.");
       }
       else
       throw System::Sysutils::Exception("������ ����� � �������."); */
      // else
      // UserRT->AsXML = "<rt><r uid='001E' v='1'/><r uid='011F' v='1'/></rt>";
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA(E.Message, "������ ����� � �������");
      // Application->Terminate();
     }
   }
  __finally
   {
    Application->ProcessMessages();
   }
  return RC;
//  bool RC = false;
//  TDBSelectUnit * SelDlg = NULL;
//  TTagNode * LRC = new TTagNode;
//  try
//   {
//    Application->ProcessMessages();
//    int i = 3;
//    bool connected = false;
//    while ((i > 0) && !connected)
//     {
//      try
//       {
//        Application->ProcessMessages();
//        // try
//        // {
//        LRC->AsXML = FClients->Admin->LoginUser("");
//        // }
//        // catch (EAbort & E)
//        // {
//        // _MSG_ERRA(E.Message, "������ ����� � �������");
//        // }
//        Application->ProcessMessages();
//        if (LRC->CmpName("nouser"))
//         {
//          FClients->SetConnectParam("", "");
//          throw System::Sysutils::Exception("������ ����� � �������.");
//         }
//        else if (LRC->CmpName("user_exist"))
//         {
//          UnicodeString FQueMSG =
//            "������������ � ����� ������� ��� ����������� � ���������.\n\n���������� ��������� ����?\n\n����� �������� ������������ ����� �������� �� ���������.";
//          if (_MSG_QUEA(FQueMSG, "������������ �����������") == ID_YES)
//           LRC->AsXML = FClients->Admin->LoginUser("exit");
//          else
//           {
//            FClients->SetConnectParam("", "");
//            throw System::Sysutils::Exception("������ ����� � �������. ������������ ��� ����������� � ���������.");
//           }
//         }
//        else if (LRC->CmpName("access_denied"))
//         {
//          FClients->SetConnectParam("", "");
//          throw System::Sysutils::Exception("������ ����� � �������. ������ ��������.");
//         }
//        else if (LRC->CmpName("rt"))
//         {
//          if (LRC->Count > 1)
//           {
//            Application->ProcessMessages();
//            SelDlg = new TDBSelectUnit(this, LRC);
//            SelDlg->ShowModal();
//            if (SelDlg->ModalResult == mrOk)
//             {
//              LRC->AsXML = FClients->Admin->LoginUser(SelDlg->DBId);
//              if (FClients->DocAdmin)
//               FClients->DocAdmin->LoginUser(SelDlg->DBId);
//             }
//            else
//             throw EAbort("�������� �������� �������������.");
//            Application->ProcessMessages();
//           }
//          if (LRC->Count == 1)
//           {
//            Application->ProcessMessages();
//            if (FClients->DocAdmin)
//             FClients->DocAdmin->LoginUser("");
//            Application->ProcessMessages();
//            ConnectOpt->Vals["l"] = FClients->dsICRestCon->UserName;
//            if (ConnectOpt->Vals["sp"].AsBoolDef(false))
//             ConnectOpt->Vals["p"] = FClients->dsICRestCon->Password;
//            Application->ProcessMessages();
//            // LRC->AsXML;
//            Application->ProcessMessages();
//            FUserRT["IC"]              = GetRTData(LRC, "003B"); // ������ � &quot;��-���&quot;' uid='003B'
//            FUserRT["ICReg"]           = GetRTData(LRC, "002A"); // ������������' uid='002A'
//            FUserRT["ICRegUList"]      = GetRTData(LRC, "0033"); // ������ � ������ ���������' uid='0033'
//            FRegComp->UnitListEnabled  = FUserRT["ICRegUList"];
//            FUserRT["ICRegUListEdit"]  = GetRTData(LRC, "0036"); // ��������������' uid='0036'
//            FRegComp->UnitListEditable = FUserRT["ICRegUListEdit"];
//            FUserRT["ICRegCList"]      = GetRTData(LRC, "0037"); // ������ � ������������' uid='0037'
//            FRegComp->ClassEnabled     = FUserRT["ICRegCList"];
//            FUserRT["ICRegCListEdit"]  = GetRTData(LRC, "0038"); // ��������������' uid='0038'
//            FRegComp->ClassEditable    = FUserRT["ICRegCListEdit"];
//            FUserRT["ICCard"]          = GetRTData(LRC, "002B"); // ������ � ���. �����' uid='002B'
//            FRegComp->OpenCardEnabled  = FUserRT["ICCard"];
//            FUserRT["ICCardEdit"]      = GetRTData(LRC, "0039"); // ��������������' uid='0039'
//            FICCardComp->CardEditable  = FUserRT["ICCardEdit"];
//            FUserRT["ICPlan"]          = GetRTData(LRC, "002C"); // ������ � ������ ������' uid='002C'
//            FUserRT["ICPlanPlaning"]   = GetRTData(LRC, "003A"); // ���������� ������������' uid='003A'
//            FICPlanComp->PlanEditable  = FUserRT["ICPlanPlaning"];
//            FUserRT["Doc"]             = GetRTData(LRC, "003C"); // ������ � &quot;��-���������������&quot;' uid='003C'
//            FUserRT["DocSpec"]         = GetRTData(LRC, "002E");
//            // ������ � ������ &quot;�������� ����������&quot;' uid='002E'
//            FDocComp->SpecEnabled     = FUserRT["DocSpec"];
//            FUserRT["DocSpecEdit"]    = GetRTData(LRC, "003D"); // ��������������' uid='003D'
//            FDocComp->SpecEditable    = FUserRT["DocSpecEdit"];
//            FUserRT["DocDoc"]         = GetRTData(LRC, "002D"); // ������ � ������ &quot;���������&quot;' uid='002D'
//            FDocComp->DocEnabled      = FUserRT["DocDoc"];
//            FUserRT["DocDocEdit"]     = GetRTData(LRC, "003E"); // ��������������' uid='003E'
//            FDocComp->DocEditable     = FUserRT["DocDocEdit"];
//            FDocComp->DocHandEditable = FUserRT["DocDocEdit"];
//            FUserRT["DocFilter"]      = GetRTData(LRC, "002F"); // ������ � ������ &quot;�������&quot;' uid='002F'
//            FDocComp->FilterEnabled   = FUserRT["DocFilter"];
//            FUserRT["DocFilterEdit"]  = GetRTData(LRC, "003F"); // ��������������' uid='003F'
//            FDocComp->FilterEditable  = FUserRT["DocFilterEdit"];
//            FUserRT["VS"]             = GetRTData(LRC, "0029"); // <binary name='����� ����' uid='0029'/>
//            connected                 = true;
//            RC                        = connected;
//            Application->ProcessMessages();
//           }
//          else
//           throw System::Sysutils::Exception("������ ����� � �������.");
//         }
//        else
//         throw EAbort("������ ����� � �������"); // System::Sysutils::Exception("������ ����� � �������.");
//       }
//      catch (TDSRestProtocolException & E)
//       {
//        if (E.Status == 403 && E.Message.Pos("Forbidden"))
//         {
//          _MSG_ERRA("������ ��������. ����� ��� ������ �����������.", "������");
//          FClients->SetConnectParam("", "");
//         }
//        else
//         {
//          _MSG_ERRA(E.Message, "������ ����� � �������");
//          if (!ConnectOptDlg->Execute())
//           i = 0;
//         }
//       }
//      catch (EAbort & E)
//       {
//        // _MSG_ERRA("�������� �������� �������������", "������ ����� � �������");
//        i = 0;
//       }
//      catch (System::Sysutils::Exception & E)
//       {
//        if (E.ClassNameIs("EAbort"))
//         i = 0;
//        else
//         {
//          _MSG_ERRA(E.Message, "������ ����� � �������");
//          if (!ConnectOptDlg->Execute())
//           i = 0;
//         }
//       }
//      i-- ;
//     }
//    if (!connected)
//     Application->Terminate();
//   }
//  __finally
//   {
//    if (SelDlg)
//     delete SelDlg;
//    Application->ProcessMessages();
//   }
//  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::SrvOptLoadXML(TTagNode * AOptNode)
 {
  UnicodeString FFN = icsPrePath(icsPrePath(ExtractFilePath(ParamStr(0))) + "\\defxml\\srvoptdef.zxml");
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   _MSG_ERRA("������ �������� �������� �������� 'srvoptdef.zxml'", "������ �������� �������� ��������");
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::LoadSrvOpt(TAppOptions * AOpt, UnicodeString AFN)
 {
  bool RC = false;
  try
   {
    try
     {
      UnicodeString FFN = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\conf\\" + AFN);
      if (!FileExists(FFN))
       {
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      ((TAppOptXMLProv *)AOpt->DataProvider)->FileName = FFN;
      AOpt->LoadXML();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::SrvOptDlgButtonEditClick(TTagNode * ANode, int AButtonIndex, AOButtonEdit * control)
 {
  UnicodeString RC = ((AOButtEdit*)control)->Text;
  try
   {
    if (ANode->CmpAV("uid", "002E"))
     {
      if (RC.Length())
       OpenFolderDlg->Path = RC;
      if (OpenFolderDlg->Execute())
       RC = OpenFolderDlg->Path;
     }
    else
     {
      if (ANode->CmpAV("uid", "0029"))
       OpenDlg->Filter = "�� \"�����\"|kladr.fdb";
      else if (ANode->CmpAV("uid", "002B"))
       OpenDlg->Filter = "��������� ��|usr.fdb";
      OpenDlg->FileName = RC;
      if (!OpenDlg->FileName.Length())
       OpenDlg->InitialDir = icsPrePath(ExtractFilePath(ParamStr(0)));
      if (OpenDlg->Execute())
       RC = OpenDlg->FileName;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
