// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
// ---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("main_admin\LoginUnit.cpp", LoginForm);
USEFORM("main_admin\MainUnit.cpp", MainForm);
USEFORM("main_admin\UpdateCodeUnit.cpp", UpdateCode);
USEFORM("Common\Update\UpdateUnit.cpp", UpdateDM); /* TDataModule: File Type */
USEFORM("main_admin\dsConnectUnit.cpp", ConnectDM); /* TDataModule: File Type */
USEFORM("main_admin\DMUnit.cpp", DM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
#include "ClientUpdateUnit.h"
// ---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    // Application->MainFormOnTaskBar = true;
    Application->Title = "������������� �� \"��-���\" ";
    Application->CreateForm(__classid(TDM), &DM);
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->Run();
   }
  catch (Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw Exception("");
     }
    catch (Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
// ---------------------------------------------------------------------------
