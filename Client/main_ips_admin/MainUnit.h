﻿//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ImgList.hpp>
#include "cxClasses.hpp"
#include "cxGraphics.hpp"
#include "dxBar.hpp"
#include "ICSAboutDialog.h"
#include "dxGDIPlusClasses.hpp"
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "DMUnit.h"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
 TICSAboutDialog *AboutDlg;
 TcxImageList *LMainIL48;
 TdxBarDockControl *MainDoc;
 TButton *Button5;
 TButton *Button6;
 TButton *Button1;
 TProgressBar *PrBar;
 TButton *Button3;
 TButton *Button7;
 TcxImageList *LMainIL32;
 TActionList *MainAL;
 TAction *actClassList;
 TAction *actPlanSetting;
 TAction *actInsertMIBP;
 TAction *actCheckUpdateCode;
 TAction *actAbout;
 TAction *actUserList;
 TButton *Button8;
 TAction *actSetDefMIBPSch;
 TAction *actCreateUpdate;
 TAction *actConnectSetting;
 TAction *actClientSetting;
 TAction *actPgmSetting;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
 void __fastcall actClassListExecute(TObject *Sender);
 void __fastcall actInsertMIBPExecute(TObject *Sender);
 void __fastcall actPlanSettingExecute(TObject *Sender);
 void __fastcall actCheckUpdateCodeExecute(TObject *Sender);
 void __fastcall actConnectSettingExecute(TObject *Sender);
 void __fastcall actClientSettingExecute(TObject *Sender);
 void __fastcall actPgmSettingExecute(TObject *Sender);
 void __fastcall actAboutExecute(TObject *Sender);
 void __fastcall actSetDefMIBPSchExecute(TObject *Sender);
 void __fastcall actCreateUpdateExecute(TObject *Sender);

private:	// User declarations
  int FC;
  void __fastcall UpdateMenuItems();
  bool __fastcall CheckConnect();
public:		// User declarations
  __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
