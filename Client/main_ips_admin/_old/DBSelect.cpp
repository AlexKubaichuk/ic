//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DBSelect.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TDBSelectUnit *DBSelectUnit;
//---------------------------------------------------------------------------
__fastcall TDBSelectUnit::TDBSelectUnit(TComponent* Owner, TTagNode * ADBDef)
 : TForm(Owner)
{
  ApplyBtn->Enabled = false;
  FDBId = "";
  TTagNode *itNode = ADBDef->GetFirstChild();
  DBNameCB->Properties->Items->Clear();
  while (itNode)
   {
     DBNameCB->Properties->Items->Add(itNode->AV["name"]);
     FDBCodes[DBNameCB->Properties->Items->Count-1] = itNode->AV["id"];
     itNode = itNode->GetNext();
   }
  if (DBNameCB->Properties->Items->Count)
   DBNameCB->ItemIndex = 0;
}
//---------------------------------------------------------------------------
void __fastcall TDBSelectUnit::DBNameCBPropertiesChange(TObject *Sender)
{
  if (DBNameCB->ItemIndex != -1)
   {
     ApplyBtn->Enabled = true;
     FDBId = FDBCodes[DBNameCB->ItemIndex];
   }
  else
   {
     ApplyBtn->Enabled = false;
     FDBId = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TDBSelectUnit::ApplyBtnClick(TObject *Sender)
{
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TDBSelectUnit::DBNameCBKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if ((Key == VK_RETURN) && ApplyBtn->Enabled)
   ApplyBtnClick(ApplyBtn);
}
//---------------------------------------------------------------------------

