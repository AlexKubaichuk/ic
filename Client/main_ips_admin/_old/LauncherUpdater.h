//---------------------------------------------------------------------------

#ifndef LauncherUpdaterH
#define LauncherUpdaterH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
/*
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <IPPeerClient.hpp>
#include <REST.Authenticator.Basic.hpp>
#include <REST.Authenticator.OAuth.hpp>
#include <REST.Authenticator.Simple.hpp>
#include <REST.Client.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include <Vcl.Menus.hpp>
#include "Htmlview.hpp"
*/
//---------------------------------------------------------------------------
class TUpdateForm : public TForm
{
__published:
 TLabel *MsgLab;
private:
  void __fastcall Msg(UnicodeString AMsg);
public:
  __fastcall TUpdateForm(TComponent* Owner);

  void __fastcall CheckUpdate(UnicodeString AURL = "");
};
//---------------------------------------------------------------------------
extern PACKAGE TUpdateForm *UpdateForm;
//---------------------------------------------------------------------------
#endif
