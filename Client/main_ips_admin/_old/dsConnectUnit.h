//---------------------------------------------------------------------------

#ifndef dsConnectUnitH
#define dsConnectUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <REST.Authenticator.Basic.hpp>
#include <REST.Authenticator.Simple.hpp>
#include <REST.Client.hpp>
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "pgmsetting.h"
#include "ServerClientClasses.h"

#include "dsRegClient.h"
//#include "dsSchEditClient.h"
//#include "dsICStoreClient.h"
//#include "dsDocClient.h"
//---------------------------------------------------------------------------
/*
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>


//---------------------------------------------------------------------------


#include <REST.Authenticator.Basic.hpp>
#include <REST.Authenticator.Simple.hpp>
#include <REST.Client.hpp>
//---------------------------------------------------------------------------
*/
class TConnectDM : public TDataModule
{
__published:	// IDE-managed Components
 TDSRestConnection *dsICRestCon;
 void __fastcall DataModuleDestroy(TObject *Sender);
 void __fastcall dsICRestConLogin(TObject *Sender, TDSRestLoginProperties *LoginProperties,
          bool &Cancel);





private:

  TdsAdminClassClient  *FAppClient;
  TdsICClassClient *FICClient;
//  TdsEIDataClassClient *FEIDataClient;
//  TdsDocClassClient *FDocClient;
//  TdsKLADRClassClient *FKLADRClient;
//  TdsOrgClassClient   *FOrgClient;
//  TdsVacStoreClassClient *FVSClient;
  UnicodeString FUserName, FPassword;
public:
  __fastcall TConnectDM(TComponent* Owner, UnicodeString AConnData);
  void __fastcall SetConnectParam(UnicodeString AUser, UnicodeString APaswd);
  void __fastcall SetHostParam(UnicodeString AConnData);

  __property TdsAdminClassClient       *Admin  = {read=FAppClient};
  __property TdsICClassClient          *App   = {read=FICClient};
//  __property TdsEIDataClassClient   *EIData = {read=FEIDataClient};
//  __property TdsDocClassClient      *Doc    = {read=FDocClient};
//  __property TdsKLADRClassClient    *KLADR  = {read=FKLADRClient};
//  __property TdsOrgClassClient      *Org    = {read=FOrgClient};
//  __property TdsVacStoreClassClient *VS     = {read=FVSClient};

};
//---------------------------------------------------------------------------
extern PACKAGE TConnectDM *ConnectDM;
//---------------------------------------------------------------------------
#endif
