// ---------------------------------------------------------------------------
#pragma hdrstop
#include "dsConnectUnit.h"
// #include "ExtUtils.h"
#include "LoginUnit.h"
#include "ExtUtils.h"
#include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TConnectDM * ConnectDM;
// ---------------------------------------------------------------------------
__fastcall TConnectDM::TConnectDM(TComponent * Owner, UnicodeString AConnData) : TDataModule(Owner)
 {
  try
   {
    FUserName = "";
    FPassword = "";
    SetHostParam(AConnData);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::DataModuleDestroy(TObject * Sender)
 {
  delete FICClient;
  delete FAppClient;
  // delete FKLADRClient;
  // delete FOrgClient;
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::dsICRestConLogin(TObject * Sender, TDSRestLoginProperties * LoginProperties, bool & Cancel)
 {
  if (FUserName.Length() && FPassword.Length())
   {
    LoginProperties->UserName = FUserName;
    LoginProperties->Password = FPassword;
   }
  else
   {
    bool LoginAdm = false;
    if (LoginAdm)
     {
      TTagNode * pid = new TTagNode;
      try
       {
        UnicodeString FPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\pid");
        if (FileExists(FPath))
         {
          pid->LoadFromZIPXMLFile(FPath);
          TTagNode * itNode = pid->GetFirstChild();
          if (itNode)
           {
            FUserName = GetRPartB(itNode->AV["PCDATA"], '-');
            itNode    = itNode->GetNext();
            if (itNode)
             {
              FPassword                 = GetRPartB(itNode->AV["PCDATA"], '-');
              LoginProperties->UserName = FUserName;
              LoginProperties->Password = FPassword;
              Cancel                    = false;
             }
           }
         }
        else
         {
          _MSG_ERRA("������ ����� � �������.", "������ ����� � �������.");
          Application->Terminate();
         }
       }
      __finally
       {
        delete pid;
       }
     }
    else
     {
      TLoginForm * LogDlg = new TLoginForm(this);
      try
       {
        try
         {
          LogDlg->ShowModal();
          Cancel = (LogDlg->ModalResult != mrOk);
          if (!Cancel)
           {
            LoginProperties->UserName = LogDlg->User;
            LoginProperties->Password = LogDlg->Pass;
            FUserName                 = LogDlg->User;
            FPassword                 = LogDlg->Pass;
           }
         }
        catch (System::Sysutils::Exception & E)
         {
          _MSG_ERRA("������ �������������� ������������.\n��������� ���������:\n" + E.Message,
          "������ ����� � �������.");
          Application->Terminate();
         }
       }
      __finally
       {
        delete LogDlg;
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::SetConnectParam(UnicodeString AUser, UnicodeString APaswd)
 {
  FUserName = AUser;
  FPassword = APaswd;
 }
// ---------------------------------------------------------------------------
void __fastcall TConnectDM::SetHostParam(UnicodeString AConnData)
 {
  TTagNode * FConnectParam = new TTagNode;
  FConnectParam->AsXML = AConnData;
  try
   {
    UnicodeString FSrvHost = FConnectParam->AV["ic_server"];
    int FSrvPort = FConnectParam->AV["ic_port"].ToIntDef(5100);
    UnicodeString FSrvDocHost = FConnectParam->AV["ic_doc_server"];
    int FSrvDocPort = FConnectParam->AV["ic_doc_port"].ToIntDef(5100);
    UnicodeString FSrvSVHost = FConnectParam->AV["ic_sv_server"];
    int FSrvSVPort = FConnectParam->AV["ic_sv_port"].ToIntDef(5100);
    dsICRestCon->Host = FSrvHost;
    dsICRestCon->Port = FSrvPort;
    FAppClient        = new TdsAdminClassClient(dsICRestCon);
    FICClient         = new TdsICClassClient(dsICRestCon);
    // FEIDataClient = new TdsEIDataClassClient(dsICRestCon);
    // FDocClient    = new TdsDocClassClient(dsICDocRestCon);
    // FKLADRClient = new TdsKLADRClassClient(dsICRestCon);
    // FOrgClient   = new TdsOrgClassClient(dsICRestCon);
    // FVSClient     = new TdsVacStoreClassClient(dsICSVRestCon);
   }
  __finally
   {
    delete FConnectParam;
   }
 }
// ---------------------------------------------------------------------------
