//---------------------------------------------------------------------------

#ifndef CheckCodeUnitH
#define CheckCodeUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxClasses.hpp"
#include "cxGraphics.hpp"
#include "dxBar.hpp"
#include <Vcl.ImgList.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMemo.hpp"
#include "cxTextEdit.hpp"
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtDlgs.hpp>
//---------------------------------------------------------------------------
class TCheckCode : public TForm
{
__published:	// IDE-managed Components
 TdxBarManager *ClassTBM;
 TdxBar *ClassTBMBar1;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton5;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TcxImageList *LClassIL32;
 TdxBarButton *PasteBarButton;
 TdxBarButton *SaveBarButton;
 TcxMemo *CodeMemo;
 TSaveDialog *SaveDlg;
 TdxBarButton *CheckBarButton;
 TcxMemo *ResMemo;
 TdxBarButton *OpenBarButton;
 TOpenTextFileDialog *OpenDlg;
 void __fastcall PasteBarButtonClick(TObject *Sender);
 void __fastcall SaveBarButtonClick(TObject *Sender);
 void __fastcall CheckBarButtonClick(TObject *Sender);
 void __fastcall OpenBarButtonClick(TObject *Sender);
 void __fastcall CodeMemoPropertiesChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
 __fastcall TCheckCode(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCheckCode *CheckCode;
//---------------------------------------------------------------------------
#endif

