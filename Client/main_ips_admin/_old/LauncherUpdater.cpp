//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LauncherUpdater.h"
#include "ExtUtils.h"
#include "XMLContainer.h"
#include "System.zip.hpp"
#include "msgdef.h"
#include "UpdateUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma resource "*.dfm"
TUpdateForm *UpdateForm;
//---------------------------------------------------------------------------
__fastcall TUpdateForm::TUpdateForm(TComponent* Owner)
 : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TUpdateForm::CheckUpdate(UnicodeString AURL)
{
  Show();
  Application->ProcessMessages();
  Close();
  return;
  Application->ProcessMessages();
  TUpdateDM *FUpd = new TUpdateDM(this);
  if (AURL.Length())
    FUpd->UpdateURL = AURL;
  try
   {
     UnicodeString FDataPath = icsPrePath(GetEnvironmentVariable("APPDATA")+"\\ic");
     UnicodeString FPath = icsPrePath(ExtractFilePath(ParamStr(0)));
     ForceDirectories(icsPrePath(FDataPath+"\\xml"));
     ForceDirectories(icsPrePath(FDataPath+"\\img"));
     Msg("�������� ������� ����� ������...");
     if (FUpd->Load("/xml/ver.xml",icsPrePath(FDataPath+"\\xml\\ver.xml")))
      {
        TTagNode *FVerNode = new TTagNode;
        try
         {
           FVerNode->LoadFromXMLFile(icsPrePath(FDataPath+"\\xml\\ver.xml"));
           if (VersionCmp(FVerNode->AV["launcher"], icsGetFileVersion(icsPrePath(FPath+"\\IC_Launcher.exe"))) > 0)
            { // �� ������� ������ ������ �������
              Msg("���������� ����� ������ ������� �������, ��������...");
              if (FUpd->Load("/xml/IC_Launcher.exe.zip",icsPrePath(FPath+"\\IC_Launcher.exe.zip")))
               {
                 // ������ ��������, ����������... --------------------------------
                 Msg("������ ��������, ����������...");
                 TZipFile *FZF = new TZipFile;
                 try
                  {
                    try
                     {
                       DeleteFile(icsPrePath(FPath+"\\IC_Launcher.exe.old"));
                       RenameFile(icsPrePath(FPath+"\\IC_Launcher.exe"),icsPrePath(FPath+"\\IC_Launcher.exe.old"));
                       Msg("��������� ���������� ������ ������� ������� (\"IC_Launcher.exe.old\")...");
                       try
                        {
                          FZF->Open(icsPrePath(FPath+"\\IC_Launcher.exe.zip"), zmRead);
                          FZF->Extract("IC_Launcher.exe",FPath);
                          Msg("�����������...");
                        }
                       catch(System::Sysutils::Exception &E)
                        {
                          _MSG_ERR(E.Message, "������");
                        }
                     }
                    catch (...)
                     {
                     }
                  }
                 __finally
                  {
                    delete FZF;
                  }
                 if (FileExists(icsPrePath(FPath+"\\IC_Launcher.exe")))
                  {
                    Msg("���������...");
                    try
                     {
                       STARTUPINFO si;
                       PROCESS_INFORMATION pi;

                       ZeroMemory( &si, sizeof(si) );
                       si.cb = sizeof(si);
                       ZeroMemory( &pi, sizeof(pi) );

                       // Start the child process.
                       if( !CreateProcess( NULL,   // No module name (use command line)
                           icsPrePath(FPath+"\\IC_Launcher.exe").c_str(),      // Command line
                           NULL,           // Process handle not inheritable
                           NULL,           // Thread handle not inheritable
                           false,          // Set handle inheritance to FALSE
                           NORMAL_PRIORITY_CLASS,              // No creation flags
                           NULL,           // Use parent's environment block
                           NULL,           // Use parent's starting directory
                           &si,            // Pointer to STARTUPINFO structure
                           &pi )           // Pointer to PROCESS_INFORMATION structure
                        )
                        {
                           _MSG_ERR( L"������ �������� �������� ���������� ����� � �����.\n��������� ���������:\n"+GetAPILastErrorFormatMessage()+L".",L"������");
                        }
                       Application->Terminate();
                     }
                    catch (System::Sysutils::Exception &E)
                     {
                        _MSG_ERR(E.Message, "������");
                     }
                  }
               }
            }
         }
        __finally
         {
            delete FVerNode;
         }
      }
     else
      _MSG_ERR("������ �������� ������� ����������.", "������");
   }
  __finally
   {
     delete FUpd;
     Close();
   }
}
//---------------------------------------------------------------------------
void __fastcall TUpdateForm::Msg(UnicodeString AMsg)
{
  MsgLab->Caption = AMsg;
  Application->ProcessMessages();
  Sleep(100);
}
//---------------------------------------------------------------------------

