// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "CheckCodeUnit.h"
#include <System.NetEncoding.hpp>
#include "DMUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxGraphics"
#pragma link "dxBar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMemo"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TCheckCode * CheckCode;
// ---------------------------------------------------------------------------
__fastcall TCheckCode::TCheckCode(TComponent * Owner) : TForm(Owner)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckCode::PasteBarButtonClick(TObject * Sender)
 {
  CodeMemo->Lines->Text = Clipboard()->AsText;
  CheckBarButtonClick(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckCode::SaveBarButtonClick(TObject * Sender)
 {
  if (SaveDlg->Execute())
   {
    CodeMemo->Lines->SaveToFile(SaveDlg->FileName);
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall GetLPartB(UnicodeString ARef, UnicodeString Sep)
 {
  return ARef.SubString(1, ARef.Pos(Sep) - 1);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall GetRPartB(UnicodeString ARef, UnicodeString Sep)
 {
  int pos = ARef.Pos(Sep);
  return ARef.SubString(pos + Sep.Length(), ARef.Length() - pos - Sep.Length() + 1);
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckCode::CheckBarButtonClick(TObject * Sender)
 {
  TBase64Encoding * B64Encode = new TBase64Encoding;
  TTagNode * dbDef = new TTagNode;
  TTagNode * itNode;
  UnicodeString Def;
  TStringStream * FStm = new TStringStream;
  TStringStream * FRCStm = new TStringStream;
  try
   {
    if (CodeMemo->Lines->Text.Length())
     {
      FStm->WriteString(CodeMemo->Lines->Text);
      FStm->Seek(0, 0);
      B64Encode->Decode(FStm, FRCStm);
      FRCStm->Seek(0, 0);
      dbDef->SetZIPXML(FRCStm);
      itNode = dbDef->GetFirstChild();
      ResMemo->Lines->Clear();
      ResMemo->Lines->BeginUpdate();
      int i = 1;
      while (itNode)
       {
        ResMemo->Lines->Add("#########################################################");
        ResMemo->Lines->Add("# " + IntToStr(i) + ".    " + itNode->AV["id"] + "            #");
        ResMemo->Lines->Add("#########################################################");
        ResMemo->Lines->Add("��: " + GetLPartB(itNode->AV["def"], "@@"));
        ResMemo->Lines->Add("`````````````````````````````````````````````````````````");
        Def = B64Encode->Decode(GetRPartB(itNode->AV["def"], "@@"));
        // <text name='������������' uid='00CA' required='1' inlist='l' cast='no' length='100'/>
        ResMemo->Lines->Add("������������:\t\t" + GetLPartB(Def, "@@"));
        Def = GetRPartB(Def, "@@");
        // <text name='������ ������������' uid='00CB' required='1' inlist='e' cast='no' linecount='2' length='255'/>
        ResMemo->Lines->Add("������ ������������:\t" + GetLPartB(Def, "@@"));
        Def = GetRPartB(Def, "@@");
        // <extedit name='�����' uid='00CC' comment='search' required='1' inlist='e' linecount='3' showtype='1' length='40'/>
        ResMemo->Lines->Add("�����:\t\t\t" + DM->AddrCodeToStr(GetLPartB(Def, "@@")));
        Def = GetRPartB(Def, "@@");
        // <text name='�.�.�. ������������' uid='00CD' inlist='e' cast='no' linecount='2' length='255'/>
        ResMemo->Lines->Add("�.�.�. ������������:\t" + GetLPartB(Def, "@@"));
        Def = GetRPartB(Def, "@@");
        // <text name='�������' uid='00CE' inlist='l' cast='no' length='20'/>
        ResMemo->Lines->Add("�������:\t\t" + GetLPartB(Def, "@@"));
        Def = GetRPartB(Def, "@@");
        // <text name='��. �����' uid='00CF' inlist='e' cast='no' length='50'/>
        ResMemo->Lines->Add("��. �����:\t\t" + Def);
        ResMemo->Lines->Add("`````````````````````````````````````````````````````````");
        Def = itNode->AV["ch"];
        // ���������
        ResMemo->Lines->Add("���������:\t" + GetLPartB(Def, '/'));
        Def = GetRPartB(Def, '/');
        // ��������
        ResMemo->Lines->Add("��������:\t" + GetLPartB(Def, '/'));
        Def = GetRPartB(Def, '/');
        // ����
        ResMemo->Lines->Add("����:\t\t" + GetLPartB(Def, '/'));
        Def = GetRPartB(Def, '/');
        // �������
        ResMemo->Lines->Add("�������:\t" + GetRPartB(Def, '/'));
        itNode = itNode->GetNext();
        i++ ;
       }
     }
   }
  __finally
   {
    delete dbDef;
    delete FStm;
    delete FRCStm;
    delete B64Encode;
    ResMemo->Lines->EndUpdate();
    // ResMemo->Perform(WM_VSCROLL, SB_TOP, 0);
    // ResMemo->CaretPos.X = 0;
    // ResMemo->CaretPos.Y = 0;
    // ResMemo->SelStart   = 0;
    // ResMemo->SelLength  = 0;
    ResMemo->SetFocus();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckCode::OpenBarButtonClick(TObject * Sender)
 {
  if (OpenDlg->Execute())
   {
    CodeMemo->Lines->LoadFromFile(OpenDlg->FileName);
    CheckBarButtonClick(this);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckCode::CodeMemoPropertiesChange(TObject * Sender)
 {
  CheckBarButton->Enabled = CodeMemo->Lines->Text.Length();
 }
// ---------------------------------------------------------------------------
