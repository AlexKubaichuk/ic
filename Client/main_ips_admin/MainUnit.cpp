﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "MainUnit.h"
#include "ExtUtils.h"
// #include "LoginUnit.h"
#include "LauncherUpdater.h"
// #include "PatGrChange.h"
#include "msgdef.h"
#include "ProgressUnit.h"
#include "UpdateMIBPForm.h"
#include "CheckCodeUnit.h"
#include "dsIPSPlanSetting.h"
// #include "SetDBPath.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxGraphics"
#pragma link "dxBar"
#pragma link "ICSAboutDialog"
#pragma link "dxGDIPlusClasses"
#pragma resource "*.dfm"
TMainForm * MainForm;
// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent * Owner) : TForm(Owner)
 {
  FC = 0;
  // icDxLocale();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::UpdateMenuItems()
 {
  UnicodeString FPreviewPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\DocPreview.exe");
  if (FileExists(FPreviewPath) && !DM->CliOpt->Vals["extview"].AsStringDef("").Length())
   DM->CliOpt->Vals["extview"] = FPreviewPath;
  DM->DocComp->ExternalView = DM->CliOpt->Vals["extview"].AsStringDef("");
 }
// ---------------------------------------------------------------------
bool __fastcall TMainForm::CheckConnect()
 {
  bool RC = false;
  TdsProgressForm * PrForm = new TdsProgressForm(this);
  try
   {
    try
     {
      // Caption = Application->Title;
      // DM->MainWnd = (TObject*)MainForm;
      if (DM->Login())
       {
//        ShowMessage("0");
        PrForm->Show();
        Application->ProcessMessages();
//        ShowMessage("1");
        DM->LoadRegDef();
//        ShowMessage("2");
        Application->ProcessMessages();
        DM->PreloadData();
        DM->Clients->App->UpdateGenCode();

//        ShowMessage("3");
        UpdateMenuItems();
//        ShowMessage("4");
        RC = true;
       }
     }
    catch (EAccessViolation & E)
     {
      Application->MessageBox(E.Message.c_str(), L"Ошибка", MB_OK);
      Caption = Caption + "  (Выборка \"Все дети\")";
     }
   }
  __finally
   {
    PrForm->Close();
    delete PrForm;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject * Sender)
 {
  Application->ProcessMessages();
  try
   {
    try
     {
      CheckConnect();
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERR("Ошибка аутентификации пользователя.\nСистемное сообщение:\n" + E.Message, "Ошибка входа в систему.");
      Application->Terminate();
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  Action = caFree;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actClassListExecute(TObject * Sender)
 {
  DM->RegComp->ClassShowExpanded = "0029";
  DM->RegComp->ShowClasses(ctList, "0", -1);
  DM->RegComp->ClassShowExpanded = "";
#ifdef IMM_SETTING
#ifdef _USE_NOM
  Function FDefQuest("ClearHash");
  DM->ICSDocEx->DM->ICSNom.Exec(FDefQuest);
#endif
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actInsertMIBPExecute(TObject * Sender)
 {
  TUpdateMIBPForm * Dlg = new TUpdateMIBPForm(this, 0);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actPlanSettingExecute(TObject * Sender)
 {
  TIPSPlanSettingForm * Dlg = new TIPSPlanSettingForm(this, DM->PlanComp->DataModule);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actCheckUpdateCodeExecute(TObject * Sender)
 {
  TCheckCode * Dlg = new TCheckCode(this);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actConnectSettingExecute(TObject *Sender)
{
  DM->ConnectOptDlg->Execute();
  UpdateMenuItems();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::actClientSettingExecute(TObject *Sender)
{
  DM->CliOptDlg->Execute();
  UpdateMenuItems();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::actPgmSettingExecute(TObject *Sender)
{
  DM->AppOptDlg->Execute();
  UpdateMenuItems();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::actAboutExecute(TObject *Sender)
{
  AboutDlg->FileName = icsPrePath(ParamStr(0));
  AboutDlg->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::actSetDefMIBPSchExecute(TObject *Sender)
{
  TUpdateMIBPForm * Dlg = new TUpdateMIBPForm(this, 2);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::actCreateUpdateExecute(TObject *Sender)
{
  TUpdateMIBPForm * Dlg = new TUpdateMIBPForm(this, 1);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
}
//---------------------------------------------------------------------------

