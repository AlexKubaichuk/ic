//---------------------------------------------------------------------------
#ifndef DMFH
#define DMFH
//---------------------------------------------------------------------------
#include <Registry.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBErrorHandler.hpp"
#include "pFIBQuery.hpp"
#include <Classes.hpp>
#include <DB.hpp>
#include <DBTables.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include "Registry_ICScomp.h"
#include "pgmsetting.h"
#include "ICSImm.h"
#include "ICSDocViewer.h"
#include "ICSDocCreator.h"
#include <Controls.hpp>
#include <ImgList.hpp>
#include "AppOptDialog.h"
#include "AppOptFIBDBProv.h"
#include "AppOptions.h"
#include "ICSClassData.h"
#include "DocOrgEdit.h"

#include "icsRegTransport.h"
#include "icsImmTransport.h"
#include "icsFIBRegDataProvider.h"
#include "icsRegXMLEIDataProvider.h"

//#include "ProgressF.h"

#include "icsOrgEdit.h"
#include "KLADR.h"

#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5

#include "msgdef.h"
#include "SrvcUtil.h"

const TColor REQ_COLOR  = 0x0090FFFF;
//---------------------------------------------------------------------------
#define quFNm(a) DM->quFree->FN(a)
#define quFNStr(a) DM->quFree->FN(a)->AsString
#define quFNInt(a) DM->quFree->FN(a)->AsInteger
#define quFNFloat(a) DM->quFree->FN(a)->AsFloat
#define quFNDate(a) DM->quFree->FN(a)->AsDateTime
#define AsTextDate AsDateTime.FormatString("dd.mm.yyyy")

#define quFNStr1(a) DM->quFree1->FN(a)->AsString
#define quFNInt1(a) DM->quFree1->FN(a)->AsInteger
#define quFNFloat1(a) DM->quFree1->FN(a)->AsFloat
#define quFNDate1(a) DM->quFree1->FN(a)->AsDateTime

// enum TDocSpecType {dstAll=0, dstPlan = 1};

#define dstAll 0
#define dstPlan 1
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TAddItem)(void *,AnsiString);
typedef void __fastcall (__closure *TDeleteItem)(void *,AnsiString);

typedef map<int,int> TIntMap;
typedef map<const AnsiString, TAnsiStrMap*> TAnsiStrMapList;
typedef map<int,TIntMap*> TIntListMap;
//---------------------------------------------------------------------------
// ����� ������ �������
enum TSystemMode
{
  smUnknown,    // �����������
  smNormal,     // ����������
  smLimited     // ������������
};
//---------------------------------------------------------------------------
struct TLPU
{
    long LPUCode, LPUPartCode;
    AnsiString Name;
    AnsiString FullName;
    AnsiString Addr;
    AnsiString CODE;
    AnsiString Email;
    AnsiString OKPO;
    AnsiString SOATO;
    AnsiString Phone;
    AnsiString OwnerFIO;

};
//---------------------------------------------------------------------------
class TDM : public TDataModule
{
__published:  // IDE-managed Components
        TpFIBDatabase *IMM_DB;
        TpFibErrorHandler *dbError;
        TpFIBQuery *quFree;
        TpFIBTransaction *trDefault;
        TpFIBTransaction *trFree;
        TRegistry_ICS *RegComp;
        TpFIBDatabase *Test_Conn_DB;
        TpFIBQuery *quFree1;
        TpFIBTransaction *trFree1;
        TpFIBDataSet *quXML;
        TpFIBTransaction *trXML;
        TFIBStringField *quXMLNAME;
        TFIBBlobField *quXMLFLD_VALUE;
        TpFIBQuery *quUpdate;
        TpFIBTransaction *trUpdate;
        TImageList *RegIL;
        TpFIBDataSet *quProb;
        TpFIBTransaction *trProb;
        TDataSource *srcProb;
        TpFIBDataSet *quPriv;
        TpFIBTransaction *trPriv;
        TDataSource *srcPriv;
        TpFIBDataSet *quPlan;
        TpFIBTransaction *trPlan;
        TDataSource *srcPlan;
        TpFIBDataSet *quExt;
        TpFIBTransaction *trExt;
        TDataSource *srcExt;
        TImageList *RegSelIL;
        TImageList *LRegIL;
        TpFIBDataSet *quExtClass;
        TFIBIntegerField *FIBIntegerField3;
        TFIBStringField *FIBStringField3;
        TMemoField *MemoField3;
        TpFIBTransaction *trExtClass;
        TDataSource *srcExtClass;
        TImageList *LRegIL32;
        void __fastcall DMCreate(TObject *Sender);
        void __fastcall DMDestroy(TObject *Sender);
        void __fastcall dbErrorFIBErrorEvent(TObject *Sender,
          EFIBError *ErrorValue, TKindIBError KindIBError, bool &DoRaise);
        AnsiString __fastcall SetValIfNotNull(AnsiString AVal, AnsiString AId,bool AAnd, bool AQuote);
        void __fastcall SetIntValIfNotNull(TFIBXSQLVAR *AField, int &Val);


private:  // User declarations
    TColor       FReqColor;
    AnsiString   SQLSel;
//    TProgressFForm *FProgressForm;
    void  __fastcall SetReqColor(TColor AReqColor)
     {
       FReqColor = AReqColor;
       RegComp->RequiredColor = AReqColor;
     }

public:   // User declarations
    Variant      ISRegEvents;
    TLPU         LPUParam;
    AnsiString   SubDir;
    TICSClassData *FClassData;
    AnsiString  _DB_NAME,RegKey,_DB_Alias;
    TAnsiStrMap VacList;
    TAnsiStrMap VacListCode;
    TAnsiStrMap ProbList;
    TAnsiStrMap ProbListCode;
    TAnsiStrMap InfList;
    TAnsiStrMap InfListCode;
    TIntListMap InfVacList;
    TIntListMap InfProbList;
    TAnsiStrMap ProfList;
    TIntListMap VacInfList;
    TIntListMap ProbInfList;
    TAxeXMLContainer *XMLList;
    TTagNode     *xmlImmCardDef;
    TTagNode     *VersionData;
    TTagNode     *xmlRegDef;
    TTagNode     *xmlVacDef;
    TTagNode     *xmlTestDef;
    AnsiString   Message,CurrentPath,ProdName,ProdAutor;
    TStringList  *sWindow;
    TProgressBar *PBar;
    TSystemMode  SystemMode;


        void __fastcall PrInit(int AMax, AnsiString AMsg);
        void __fastcall PrChange(int APercent, AnsiString AMsg);

    AnsiString __fastcall GetClassData(long ACode, AnsiString AName);

    bool       __fastcall qtExec(AnsiString ASQL, bool aCommit = false);
    bool       __fastcall qtExec1(AnsiString ASQL, bool aCommit = false);
    long       __fastcall GetMCode(AnsiString AGenName);



    __fastcall TDM(TComponent* Owner);
__published:
    AnsiString __fastcall NewGUID();
    __property TColor  ReqColor = {read=FReqColor, write=SetReqColor};
};
//---------------------------------------------------------------------------
extern AnsiString __fastcall GetXMLGUI(TTagNode* ANode);
extern TTagNode* __fastcall GetPassport(TTagNode* ANode);
extern PACKAGE TDM *DM;
//---------------------------------------------------------------------------
#endif
