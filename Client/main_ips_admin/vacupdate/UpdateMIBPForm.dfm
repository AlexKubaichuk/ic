object UpdateMIBPForm: TUpdateMIBPForm
  Left = 641
  Top = 201
  HelpContext = 10
  BorderIcons = [biSystemMenu, biMinimize, biHelp]
  BorderStyle = bsSingle
  ClientHeight = 522
  ClientWidth = 893
  Color = clBtnFace
  TransparentColorValue = clBtnText
  UseDockManager = True
  DefaultMonitor = dmDesktop
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'Default'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PC: TcxPageControl
    Left = 0
    Top = 0
    Width = 893
    Height = 522
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = AddTS
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    ExplicitHeight = 478
    ClientRectBottom = 518
    ClientRectLeft = 4
    ClientRectRight = 889
    ClientRectTop = 24
    object AddTS: TcxTabSheet
      Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077
      ImageIndex = 0
      ExplicitHeight = 450
      object Panel1: TPanel
        Left = 0
        Top = 453
        Width = 885
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitTop = 409
        DesignSize = (
          885
          41)
        object cxButton4: TcxButton
          Left = 802
          Top = 8
          Width = 75
          Height = 25
          Action = actAdd
          Anchors = [akRight, akBottom]
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          TabOrder = 0
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 425
        Height = 453
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitHeight = 409
        object Panel03: TPanel
          Left = 0
          Top = 66
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          ExplicitTop = 37
          object FullNameEDLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 119
            Height = 23
            Align = alClient
            Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            FocusControl = FullNameED
            Transparent = True
            ExplicitWidth = 112
            ExplicitHeight = 13
          end
          object FullNameED: TcxTextEdit
            AlignWithMargins = True
            Left = 128
            Top = 3
            Align = alRight
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 294
          end
        end
        object Panel04: TPanel
          Left = 0
          Top = 95
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          ExplicitTop = 66
          object NameEDLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 195
            Height = 23
            Align = alClient
            Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Transparent = True
            ExplicitWidth = 73
            ExplicitHeight = 13
          end
          object NameED: TcxTextEdit
            AlignWithMargins = True
            Left = 204
            Top = 3
            Align = alRight
            Style.Color = clInfoBk
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 218
          end
        end
        object Panel05: TPanel
          Left = 0
          Top = 124
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
          ExplicitTop = 95
          object ShortNameEDLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 303
            Height = 23
            Align = alClient
            Caption = #1050#1086#1088#1086#1090#1082#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' (3-5 '#1089#1080#1084#1074'.)'
            FocusControl = ShortNameED
            Transparent = True
            ExplicitWidth = 181
            ExplicitHeight = 13
          end
          object ShortNameED: TcxTextEdit
            AlignWithMargins = True
            Left = 312
            Top = 3
            Align = alRight
            Style.Color = clInfoBk
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 110
          end
        end
        object Panel06: TPanel
          Left = 0
          Top = 153
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 5
          ExplicitTop = 124
          object ManufCBLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 226
            Height = 23
            Align = alClient
            Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
            FocusControl = ManufCB
            Transparent = True
            ExplicitWidth = 79
            ExplicitHeight = 13
          end
          object ManufCB: TcxComboBox
            AlignWithMargins = True
            Left = 235
            Top = 3
            Align = alRight
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              #1042#1072#1082#1094#1080#1085#1072
              #1055#1088#1086#1073#1072)
            Properties.OnChange = AddTypeCBPropertiesChange
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Text = #1042#1072#1082#1094#1080#1085#1072
            Width = 187
          end
        end
        object Panel07: TPanel
          Left = 0
          Top = 182
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 6
          ExplicitTop = 153
          object CountryCBLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 226
            Height = 23
            Align = alClient
            Caption = #1057#1090#1088#1072#1085#1072
            FocusControl = CountryCB
            Transparent = True
            ExplicitWidth = 37
            ExplicitHeight = 13
          end
          object CountryCB: TcxComboBox
            AlignWithMargins = True
            Left = 235
            Top = 3
            Align = alRight
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              #1042#1072#1082#1094#1080#1085#1072
              #1055#1088#1086#1073#1072)
            Properties.OnChange = AddTypeCBPropertiesChange
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Text = #1042#1072#1082#1094#1080#1085#1072
            Width = 187
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 269
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 9
          ExplicitTop = 240
          object VacNameEDLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 226
            Height = 23
            Align = alClient
            Caption = #1055#1088#1077#1087#1072#1088#1072#1090
            FocusControl = VacNameED
            Transparent = True
            ExplicitWidth = 49
            ExplicitHeight = 13
          end
          object VacNameED: TcxTextEdit
            AlignWithMargins = True
            Left = 235
            Top = 3
            Align = alRight
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 187
          end
        end
        object Panel09: TPanel
          Left = 0
          Top = 240
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 8
          ExplicitTop = 211
          object DefDozeLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 310
            Height = 23
            Align = alClient
            Caption = #1044#1086#1079' '#1085#1072' '#1074#1074#1077#1076#1077#1085#1080#1077' ('#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102')'
            FocusControl = DefDoze
            Transparent = True
            ExplicitWidth = 169
            ExplicitHeight = 13
          end
          object DefDoze: TcxMaskEdit
            AlignWithMargins = True
            Left = 319
            Top = 3
            Align = alRight
            Properties.MaskKind = emkRegExprEx
            Properties.EditMask = '(\d)|(\d\,\d)|(\d\,\d\d)|(\d\,\d\d\d)'
            Properties.MaxLength = 0
            Style.Color = clWindow
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 103
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 385
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 13
          ExplicitTop = 331
          object VacCBLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 226
            Height = 23
            Align = alClient
            Caption = #1052#1048#1041#1055
            FocusControl = VacCB
            Transparent = True
            ExplicitWidth = 28
            ExplicitHeight = 13
          end
          object VacCB: TcxComboBox
            AlignWithMargins = True
            Left = 235
            Top = 3
            Align = alRight
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              #1042#1072#1082#1094#1080#1085#1072
              #1055#1088#1086#1073#1072)
            Properties.OnChange = AddTypeCBPropertiesChange
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Text = #1042#1072#1082#1094#1080#1085#1072
            Width = 187
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 298
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 10
          ExplicitTop = 269
          object DoseEDLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 226
            Height = 23
            Align = alClient
            Caption = #1044#1086#1079#1072
            FocusControl = DoseED
            Transparent = True
            ExplicitWidth = 25
            ExplicitHeight = 13
          end
          object DoseED: TcxMaskEdit
            AlignWithMargins = True
            Left = 235
            Top = 3
            Align = alRight
            Properties.MaskKind = emkRegExprEx
            Properties.EditMask = '(\d)|(\d\,\d)|(\d\,\d\d)|(\d\,\d\d\d)'
            Properties.MaxLength = 0
            Style.Color = clInfoBk
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 187
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 327
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 11
          ExplicitLeft = -5
          ExplicitTop = 313
          object BinaryReakChB: TcxCheckBox
            AlignWithMargins = True
            Left = 3
            Top = 3
            Align = alTop
            Caption = #1041#1080#1085#1072#1088#1085#1072#1103' '#1088#1077#1072#1082#1094#1080#1103
            Properties.OnChange = BinaryReakChBPropertiesChange
            State = cbsChecked
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 419
          end
        end
        object Panel15: TPanel
          Left = 0
          Top = 414
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 14
          ExplicitTop = 381
          object ResistReakValEDLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 321
            Height = 23
            Align = alClient
            Caption = #1047#1072#1097#1080#1090#1085#1072#1103' '#1088#1077#1072#1082#1094#1080#1103
            FocusControl = ResistReakValED
            Transparent = True
            ExplicitWidth = 96
            ExplicitHeight = 13
          end
          object ResistReakValED: TcxMaskEdit
            AlignWithMargins = True
            Left = 330
            Top = 3
            Align = alRight
            Properties.MaskKind = emkRegExprEx
            Properties.EditMask = '(\d)|(\d\d)'
            Properties.MaxLength = 0
            Style.Color = clWindow
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 92
          end
        end
        object Panel13: TPanel
          Left = 0
          Top = 356
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 12
          ExplicitLeft = -5
          ExplicitTop = 319
          object CanApplyVacChB: TcxCheckBox
            AlignWithMargins = True
            Left = 3
            Top = 3
            Align = alTop
            Caption = #1053#1077#1086#1073#1093#1086#1076#1080#1084#1086#1089#1090#1100' '#1087#1088#1080#1084#1077#1085#1077#1085#1080#1103' '#1052#1048#1041#1055
            Properties.OnChange = BinaryReakChBPropertiesChange
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 419
          end
        end
        object Panel08: TPanel
          Left = 0
          Top = 211
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 7
          ExplicitTop = 182
          object InsTypeCBLabel: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 226
            Height = 23
            Align = alClient
            Caption = #1057#1087#1086#1089#1086#1073' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103
            FocusControl = InsTypeCB
            Transparent = True
            ExplicitWidth = 101
            ExplicitHeight = 13
          end
          object InsTypeCB: TcxComboBox
            AlignWithMargins = True
            Left = 235
            Top = 3
            Align = alRight
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              #1074#1074#1077#1076#1077#1085#1080#1077' '#1052#1048#1041#1055
              #1079#1072#1073#1086#1088' '#1082#1088#1086#1074#1080
              #1089#1086#1089#1082#1086#1073)
            Properties.OnChange = AddTypeCBPropertiesChange
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 187
          end
        end
        object Panel01: TPanel
          Left = 0
          Top = 0
          Width = 425
          Height = 37
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object AddTypeCB: TcxComboBox
            Left = 13
            Top = 8
            Properties.DropDownListStyle = lsEditFixedList
            Properties.Items.Strings = (
              #1042#1072#1082#1094#1080#1085#1072
              #1055#1088#1086#1073#1072)
            Properties.OnChange = AddTypeCBPropertiesChange
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Text = #1042#1072#1082#1094#1080#1085#1072
            Width = 121
          end
        end
        object Panel02: TPanel
          Left = 0
          Top = 37
          Width = 425
          Height = 29
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label1: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 119
            Height = 23
            Align = alClient
            Caption = #1050#1086#1076' '#1074#1085#1077#1096'. '#1089#1080#1089#1090#1077#1084#1099
            FocusControl = S1078ED
            Transparent = True
            ExplicitWidth = 98
            ExplicitHeight = 13
          end
          object S1078ED: TcxTextEdit
            AlignWithMargins = True
            Left = 128
            Top = 3
            Align = alRight
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            Width = 294
          end
        end
      end
      object Panel3: TPanel
        Left = 425
        Top = 0
        Width = 460
        Height = 453
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitHeight = 409
        object Panel5: TPanel
          Left = 216
          Top = 0
          Width = 244
          Height = 453
          Align = alRight
          TabOrder = 0
          ExplicitHeight = 409
          object Label7: TLabel
            AlignWithMargins = True
            Left = 4
            Top = 4
            Width = 236
            Height = 13
            Align = alTop
            Caption = #1056#1077#1072#1082#1094#1080#1080
            FocusControl = ReakTypeChLB
            Transparent = True
            ExplicitWidth = 42
          end
          object ReakTypeChLB: TcxCheckListBox
            Left = 1
            Top = 20
            Width = 242
            Height = 432
            Align = alClient
            EditValueFormat = cvfIndices
            Items = <>
            Style.Color = clInfoBk
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            ExplicitHeight = 388
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 216
          Height = 453
          Align = alLeft
          TabOrder = 1
          ExplicitHeight = 409
          object Label5: TLabel
            AlignWithMargins = True
            Left = 4
            Top = 4
            Width = 208
            Height = 13
            Align = alTop
            Caption = #1048#1085#1092#1077#1082#1094#1080#1080
            FocusControl = InfChLB
            Transparent = True
            ExplicitWidth = 51
          end
          object InfChLB: TcxCheckListBox
            Left = 1
            Top = 20
            Width = 214
            Height = 432
            Align = alClient
            EditValueFormat = cvfIndices
            Items = <>
            Style.Color = clInfoBk
            Style.LookAndFeel.Kind = lfStandard
            Style.LookAndFeel.NativeStyle = True
            StyleDisabled.LookAndFeel.Kind = lfStandard
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleFocused.LookAndFeel.Kind = lfStandard
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.Kind = lfStandard
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 0
            ExplicitHeight = 388
          end
        end
      end
    end
    object SetupTS: TcxTabSheet
      Caption = #1057#1073#1086#1088#1082#1072' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
      ImageIndex = 1
      ExplicitHeight = 450
      object Panel22: TPanel
        Left = 0
        Top = 453
        Width = 885
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitTop = 409
        DesignSize = (
          885
          41)
        object cxButton5: TcxButton
          Left = 797
          Top = 8
          Width = 75
          Height = 25
          Action = actCreateSetup
          Anchors = [akTop, akRight]
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          TabOrder = 0
        end
        object PrBar: TcxProgressBar
          Left = 9
          Top = 12
          TabOrder = 1
          Width = 650
        end
      end
      object Panel24: TPanel
        Left = 0
        Top = 0
        Width = 885
        Height = 43
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label14: TLabel
          Left = 9
          Top = 11
          Width = 73
          Height = 13
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          FocusControl = UpdateNameED
          Transparent = True
        end
        object UpdateNameED: TcxTextEdit
          Left = 98
          Top = 7
          Style.Color = clWindow
          Style.LookAndFeel.Kind = lfStandard
          Style.LookAndFeel.NativeStyle = True
          StyleDisabled.LookAndFeel.Kind = lfStandard
          StyleDisabled.LookAndFeel.NativeStyle = True
          StyleFocused.LookAndFeel.Kind = lfStandard
          StyleFocused.LookAndFeel.NativeStyle = True
          StyleHot.LookAndFeel.Kind = lfStandard
          StyleHot.LookAndFeel.NativeStyle = True
          TabOrder = 0
          Width = 743
        end
      end
      object Panel27: TPanel
        Left = 0
        Top = 43
        Width = 885
        Height = 410
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitHeight = 366
        object Label18: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 879
          Height = 13
          Align = alTop
          Caption = #1044#1086#1087'. '#1089#1082#1088#1080#1087#1090
          Transparent = True
          ExplicitWidth = 62
        end
        object ExtScript: TMemo
          AlignWithMargins = True
          Left = 3
          Top = 22
          Width = 879
          Height = 385
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 341
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = #1057#1093#1077#1084#1099' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      ImageIndex = 2
      ExplicitHeight = 450
      object VacSchList: TcxCheckListBox
        Left = 0
        Top = 0
        Width = 885
        Height = 453
        Align = alClient
        EditValueFormat = cvfIndices
        Items = <>
        Sorted = True
        Style.Color = clInfoBk
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        ExplicitHeight = 409
      end
      object Panel7: TPanel
        Left = 0
        Top = 453
        Width = 885
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitTop = 409
        DesignSize = (
          885
          41)
        object cxButton6: TcxButton
          Left = 795
          Top = 8
          Width = 87
          Height = 25
          Action = actSaveDefSch
          Anchors = [akTop, akRight]
          LookAndFeel.Kind = lfStandard
          LookAndFeel.NativeStyle = True
          TabOrder = 0
        end
      end
    end
  end
  object MainAL: TActionList
    Images = MainIL
    Left = 113
    Top = 258
    object actGetClasses: TAction
      Tag = 2
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080' ...'
      ImageIndex = 1
    end
    object actSetDBPath: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1091#1090#1100' '#1082' '#1041#1044
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1091#1090#1100' '#1082' '#1041#1044
      ImageIndex = 22
    end
    object actAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = actAddExecute
    end
    object actCreateSetup: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100
      OnExecute = actCreateSetupExecute
    end
    object actSaveDefSch: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = actSaveDefSchExecute
    end
    object actSetGUID: TAction
      Caption = 'actSetGUID'
    end
  end
  object MainIL: TcxImageList
    FormatVersion = 1
    DesignInfo = 19071091
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007171
          71FF717171FF717171FF6D6D6DFF6D6D6DFF6D6D6DFF6D6D6DFF6D6D6DFF0000
          00000000000000000000000000000000000000000000000000006D6D6DFFB165
          4DFF8C5036FF8C5036FF8C5036FF8C5036FF6C554CFF6A635FFF717171FF6D6D
          6DFF6D6D6DFF6D6D6DFF6D6D6DFF717171FF717171FF6D6D6DFFE06642FFDC51
          31FFF96350FFFD725CFFF1684EFFD77445FFF1684EFFD24C27FF355938FF3559
          38FF2F982DFF489037FF2F982DFF256625FF355938FF6D6D6DFFD58855FFF963
          50FFFD7C64FFFC8B6DFFD58855FFFFCF9CFFF36D53FFFD6A57FF917930FF4DB3
          4DFF6AA966FF63CA63FF52C252FF3CB73CFF239023FF6D6D6DFF00000000E066
          42FFFD8469FFEB8059FFFFCF9CFFFFCF9CFFEF7656FFFD6B58FF80AB67FF80B3
          80FFFFEFDFFF4DB34DFF75D275FF4FC14FFF489037FF6D6D6DFF000000000000
          0000B1654DFF4C214FFF2E3457FF7F4C6EFFB1654DFF917930FF7CD77CFF8FE0
          8FFFFFF7E8FF000000005BC55BFF44A644FF0000000000000000000000001717
          17FF05070EFF0D2968FF143AA0FF102F95FF081D6CFF545454FF6AA966FF4273
          99FF1579BAFF247DB6FF377D57FF4D4D4DFF00000000000000003F3F3FFF1A1A
          1AFF102C5BFF1A4DB3FF1C56BCFF1B51B7FF102F95FF545454FF699AAEFF2C92
          F1FF3399FFFF3399FFFF2C92F1FF2C586FFF848484FF00000000121212FF2828
          28FF0F2D93FF2774DAFF2671D7FF2671D7FF1E5AC0FF5A6064FF3F95C3FF40A6
          FFFF40A6FFFF40A6FFFF3DA2FFFF2385C6FF6D6D6DFF000000002C2C2CFF3636
          36FF2C586FFF3191F9FF3399FFFF3694F7FF246AD0FF5A6064FF479FD0FF4BB1
          FFFF4BB1FFFF4DB3FFFF49AFFFFF2D92E6FF666666FF000000006D6D6DFF4A4A
          4AFF3F3F3FFF464646FF143FA4FF2060C6FF135F88FF7E7E7EFF57A9D7FF4DB3
          F2FF4DB3F2FF55BBFFFF51B7FFFF43A8EDFF6D6D6DFF00000000000000003838
          38FF666666FF8F8F8FFFA4A4A4FF4D4D4DFF464646FF0000000063ABD2FF247D
          B6FF57A9D7FF63ABD2FF479FD0FF1372A2FF699AAEFF00000000000000000000
          00006D6D6DFF605E5EFF605E5EFF666666FF0000000000000000000000003A8B
          B7FF84C0E4FFA3D0EAFF479FD0FF699AAEFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00003A8BB7FF3A8BB7FF3A8BB7FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000002C2C2CFF2C2C2CFF000000000000
          00002C2C2CFF2C2C2CFF00000000000000000000000000000000000000000000
          000000000000000000002C2C2CFF2C2C2CFF00009BFF00009BFF2C2C2CFF2C2C
          2CFF00009BFF00009BFF2C2C2CFF2C2C2CFF0000000000000000000000000000
          00002C2C2CFF2C2C2CFF00009BFF00009BFF00009BFF00009BFF2C2C2CFF7878
          78FF2C2C2CFF00009BFF2C2C2CFF787878FF2C2C2CFF00000000000000002C2C
          2CFF00009BFF007EBAFF007EBAFF00699BFF00699BFF00009BFF00009BFF7878
          78FF2C2C2CFF00009BFF00009BFF787878FF2C2C2CFF00000000000000002C2C
          2CFF00009BFF6C9DFFFF0093D9FF007EBAFF007EBAFF00699BFF00009BFF9292
          92FF2C2C2CFF00699BFF00009BFF929292FF2C2C2CFF00000000000000002C2C
          2CFF00009BFF6C9DFFFF6C9DFFFF0093D9FF0093D9FF007EBAFF00009BFF9292
          92FF2C2C2CFF007EBAFF00009BFF929292FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFC0C0C0FF6C9DFFFF4885FFFF0093D9FF007EBAFF00009BFF9292
          92FF2C2C2CFF007EBAFF00009BFF929292FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFC0C0C0FFB8B8B8FF9F9F9FFF246DFFFF007EBAFF00009BFF9292
          92FF2C2C2CFF007EBAFF00009BFF929292FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFC0C0C0FFC0C0C0FF6C9DFFFF4885FFFF0093D9FF00009BFF9292
          92FF2C2C2CFF0093D9FF00009BFF929292FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFC0C0C0FFC0C0C0FFB8B8B8FF4885FFFF0093D9FF00009BFF9292
          92FF2C2C2CFF0093D9FF00009BFF929292FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFC0C0C0FFC0C0C0FFC0C0C0FF6C9DFFFF6C9DFFFF00009BFFA4A0
          A0FF2C2C2CFF6C9DFFFF00009BFFA4A0A0FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFDEDEDEFFB4E6FFFFB8B8B8FF003CBAFF003CBAFFEBEBEBFFB8B8
          B8FF2C2C2CFF003CBAFFEBEBEBFFB8B8B8FF2C2C2CFF00000000000000002C2C
          2CFF00009BFFF0FBFFFFABABABFF6C9DFFFFEBEBEBFFEBEBEBFF2C2C2CFF2C2C
          2CFFEBEBEBFFEBEBEBFF2C2C2CFF2C2C2CFF0000000000000000000000000000
          00002C2C2CFF2C2C2CFFC0C0C0FFEBEBEBFF2C2C2CFF2C2C2CFF000000002C2C
          2CFF2C2C2CFF2C2C2CFF00000000000000000000000000000000000000000000
          000000000000000000002C2C2CFF2C2C2CFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          200000000000000400000000000000000000000000000000000000000000FFCC
          99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC
          99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FF0000
          000000000000000000000000000000000000535759FF1A3137FF223339FF5851
          4FFF0000000000000000000000000000000000000000FFCC99FFFFCC99FF0000
          000000000000000000000000000000000000496872FF3BBFE3FF079CC8FF0C1A
          20FF0000000000000000000000000000000000000000FFCC99FFFFCC99FF5ABA
          F5FF000000000000000000000000000000003D8092FF34C7EFFF0386A9FF0222
          2CFF000000000000000000000000000000005ABAF5FFFFCC99FFFFCC99FF0000
          00005ABAF5FF000000000000000000000000387385FF2EA4BFFF077B97FF061F
          28FF0000000000000000000000005ABAF5FF00000000FFCC99FFFFCC99FF0000
          0000000000005ABAF5FF000000000000000054646BFF4699BAFF377B97FF4552
          53FF00000000000000005ABAF5FF0000000000000000FFCC99FFFFCC99FF0000
          00000000000000000000000000004AA2CDFF4AA2CDFF70BDE3FF6BBCE2FF4AAE
          D8FF4AA2CDFF00000000000000000000000000000000FFCC99FFFFCC99FF0000
          00000000000000000000000000005ABAF5FF79C9F1FFC6C2B6FFC8C3B5FF65C0
          F5FF5ABAF5FF00000000000000000000000000000000FFCC99FFFFCC99FF0000
          000000000000000000005ABAF5FF92D2EBFF7BBED9FFCEDDE3FFD2E1E3FF59AC
          DAFF56B7ECFF5ABAF5FF000000000000000000000000FFCC99FFFFCC99FF5ABA
          F5FF5ABAF5FF000000004AAED8FFC0F2FFFF98D2FCFF46C8FDFF50CDFDFF59B9
          FBFF55C7FFFF4AAED8FF000000005ABAF5FF5ABAF5FFFFCC99FFFFCC99FF0000
          000000000000000000004AAED8FFDFEFF5FF9AE1FAFF7FE1FCFF86E3FCFF4BC2
          F9FF5ABAF5FF5ABAF5FF000000000000000000000000FFCC99FFFFCC99FF0000
          00000000000000000000000000004AAED8FFFFFFFFFFD2EFFFFFB5E7FFFF77E4
          FFFF5ABAF5FF00000000000000000000000000000000FFCC99FFFFCC99FF0000
          00000000000000000000000000005ABAF5FF5ABAF5FFB1D0D9FF8CC6D9FF5ABA
          F5FF59ACDAFF00000000000000000000000000000000FFCC99FFFFCC99FF0000
          000000000000000000005ABAF5FF00000000000000004AA2CDFF4AA2CDFF0000
          0000000000005ABAF5FF000000000000000000000000FFCC99FFFFCC99FF0000
          0000000000005ABAF5FF00000000000000000000000000000000000000000000
          000000000000000000005ABAF5FF0000000000000000FFCC99FFFFCC99FFFFCC
          99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC
          99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FFFFCC99FF}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          200000000000000400000000000000000000000000000000000000000000DBB0
          7BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB0
          7BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFF0000
          0000000000000000000000000000000000004C4C4CFF221E1BFF242323FF4949
          49FF0000000000000000000000000000000000000000DBB07BFFDBB07BFF0000
          000000000000000000000000000000000000535353FFC37C46FF9E561DFF0E0C
          09FF0000000000000000000000000000000000000000DBB07BFFDBB07BFF0000
          0000000000000000000000000000000000006D5B4DFFD17D3EFF834415FF150C
          04FF0000000000000000000000000000000000000000DBB07BFFDBB07BFF0000
          000000000000000000000000000000000000625346FF956444FF733D17FF130D
          06FF0000000000000000000000000000000000000000DBB07BFFDBB07BFF0000
          000000000000000000000000000000000000555555FF8F765CFF725D48FF4242
          42FF0000000000000000000000000000000000000000DBB07BFFDBB07BFF0000
          0000000000000000000000000000AA8558FFAA8558FFC9A376FFC79F72FFB888
          56FFAA8558FF00000000000000000000000000000000DBB07BFFDBB07BFF0000
          0000000000000000000000000000DCAA5EFFDBB07BFF0000000000000000DEAD
          68FFDCAA5EFF00000000000000000000000000000000DBB07BFFDBB07BFF0000
          00000000000000000000DCAA5EFFD6B493FFBEA082FFD0CECDFFD0D0D0FFBB96
          63FFD19F5DFFDCAA5EFF000000000000000000000000DBB07BFFDBB07BFF0000
          00000000000000000000B88856FFF5D0B6FFECCC93FFE4984BFFE59B54FFE4B1
          5CFFE8A657FFB88856FF000000000000000000000000DBB07BFFDBB07BFF0000
          00000000000000000000B88856FFE9E0D7FFEABE96FFE9AC7DFFEAAF83FFE09D
          50FFDCAA5EFFDCAA5EFF000000000000000000000000DBB07BFFDBB07BFF0000
          0000000000000000000000000000B88856FFF5F5F5FFF7E2C5FFF4D3ACFFECA7
          75FFDCAA5EFF00000000000000000000000000000000DBB07BFFDBB07BFF0000
          0000000000000000000000000000DCAA5EFFDCAA5EFFC2BAB4FFBFA791FFDCAA
          5EFFBB9663FF00000000000000000000000000000000DBB07BFFDBB07BFF0000
          00000000000000000000000000000000000000000000AA8558FFAA8558FF0000
          00000000000000000000000000000000000000000000DBB07BFFDBB07BFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000DBB07BFFDBB07BFFDBB0
          7BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB0
          7BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFFDBB07BFF}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EFEFEFFFEBEB
          EBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEB
          EBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFEBEBEBFFF0F0F0FF7D310BFF7D31
          0AFF7D310AFF7D310AFF7D310AFF7D300AFF7D310AFF7D310AFF7D310AFF7E31
          0AFF7D310AFF7D310AFF7E300AFF7D300AFF8E4019FF959595FFE6FFFFFFE1FF
          FFFFE1FFFFFFE0FFFFFFE0FFFFFFE0FFFFFFE1FFFFFFE3FFFFFFE0FFFFFFE1FF
          FFFFE1FFFFFFE0FFFFFFE0FFFFFFE0FFFFFFDEBDA3FF969696FFE0FEFFFFDAEA
          EFFFDAEBEFFFDAEBF1FFEAF7F1FFE9D3B4FF6179CBFF5A5899FFE2EFEDFFEFDC
          BEFFDDE6E2FFD9EAF1FFDAEBEFFFDBEBF1FFDCB89CFF969696FFE1FEFFFFDCEB
          EFFFDBEBF0FFE6F6F6FF002AA0FF7B6B93FF49B0E0FF4BB2DDFF896779FF002B
          B1FFFDE2B7FFDBEDF3FFDBEBF0FFDAECF0FFE1BA99FF969696FFE3FEFFFFDDEC
          F0FFDDECF2FFFFF7CCFF83F1FFFF50AFDEFF61CEF2FF57CBF1FF359BD3FF26D1
          FFFFFFCE7BFFE2E4DBFFDCECEFFFDEEDF0FFE5BE98FF969696FFE1FEFFFFDDEC
          F0FFE6F4F3FF0036A1FF4C9ED6FF6ED3F6FF5CCEF6FF51BCE6FF4DCBEFFF3AAD
          DEFF003DADFFFDF2D8FFDEECF1FFDFEDF1FFE7BE97FF969696FFE3FFFFFFDFED
          F0FFE3EEF0FF84E4FFFF7CDAFAFF59CDF4FFF0DDD8FF4C3528FF2191E1FF25B8
          E4FF0FBEEDFFF6EFE8FFDEECF0FFDFEDF1FFEBC096FF969696FFE2FEFFFFDFEC
          F2FFE0ECF1FFE5F0F2FF517AC7FF55CDF5FFE7DDDBFF46332FFF4FE5FFFF2C9C
          D4FFF1ECDFFFE0EEF2FFDFEDF0FFE0EEF2FFECC095FF969696FFE4FEFFFFE1ED
          F0FFE0EDF0FFE3EEF2FF9BE1F6FF67D1F4FFC9BFB9FF282124FF7AE4FEFF54CC
          EEFFE4EEF2FFE0EDF1FFE0EDF1FFE1EEF1FFF1C393FF969696FFE5FFFFFFE3EE
          F2FFE2EFF2FFE3EEF1FFE4EFF1FFE8F1F4FFD3CCBEFF5E6162FFEFFAFFFFE6EF
          F2FFE3EEF2FFE3EFF2FFE3EFF2FFE2F0F4FFF4C590FF949494FFDCDFD1FFD9D4
          BCFFDAD4BCFFDAD4BCFFDAD4BCFFDAD4BDFFDAD4BCFFDAD5BDFFDAD4BCFFDAD4
          BCFFD9D4BCFFD9D3BBFFD9D3BBFFDAD4BBFFF1BA72FF959595FFB95F00FFB55B
          00FFB45C00FFB45B00FFB45D00FFB55C00FFB45B00FFB45B00FFB55B00FFB55E
          00FFB35C00FFC1721AFFCA7C29FF795F5FFFDF8114FFACACACFFD8811BFFE087
          1EFFE0851AFFE08419FFDF8012FFE0851AFFE0861DFFE0851DFFE0841AFFDF82
          14FFDE7E0EFFDD7B0BFFDC7906FFE78208FFDB8E32FFFEFEFEFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FFCD
          B4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCD
          B4FFFFCDB4FFFFCDB4FF0000000000000000000000000000000000000000FFB5
          90FFFFE6B4FFFFE6B4FFFFE6B4FFFFE6B4FFFFE6B4FFFFE6B4FFFFE6B4FFFFE6
          B4FFFFE6B4FFFFB590FF0000000000000000000000000000000000000000FFB5
          90FFFFEBC2FFFFEBC2FFFFEBC2FF000007FF20373EFF2D5463FFFFEBC2FFFFEB
          C2FFFFEBC2FFFFB590FF0000000000000000000000000000000000000000FFB5
          90FFFFEFCFFFFFEFCFFFFFEFCFFF053E8AFF6DBBDBFFA3F3FFFF4898C0FF2186
          BEFFFFEFCFFFFFB590FF0000000000000000000000000000000000000000FFB5
          90FFFFF3DAFFFFF3DAFFFFF3DAFFFFF3DAFF358CC1FF92D7E6FFA7EEF7FF3B96
          C9FF0086D6FFFFB590FF0000000000000000000000000000000000000000FFB5
          90FFFFF6E4FFFFF6E4FFFFF6E4FFFFF6E4FFFFF6E4FF70C0DDFF8ED1E3FF2896
          CEFF1EA0E1FF099FE7FF0000000000000000000000000000000000000000FFB5
          90FFFFF9ECFFFFB590FFFFB590FFFFB590FFFFF9ECFF2B7EBBFF37A7D2FF36B4
          DFFF38B2E7FF1D9CE0FF079CE7FF00000000000000000000000000000000FFB5
          90FFFFFBF3FFFFFBF3FFFFFBF3FFFFFBF3FFFFFBF3FFFFFBF3FF569EBCFF77EB
          F9FF34AEDFFF37B3EAFF1D9CDCFF5F86A6FF000000000000000000000000FFB5
          90FFFFFEFCFFFFB590FFFFB590FFFFB590FFFFFEFCFFFFB590FFFFB590FF3A8E
          B4FF5DD8E7FF3CB2DDFF99AAA2FF5E59A4FF0F039EFF0000000000000000FFB5
          90FFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFB590FFFFB590FFFFEB
          C2FF418CB8FF9EBBAFFF6B7DCFFF001BD9FF0012C3FF000095FF00000000FFB5
          90FFFFFEFCFFFFB590FFFFB590FFFFB590FFFFFEFCFFFFB590FFFFEBC2FFFFCD
          B4FFFFB590FF4C58A7FF084FFAFF0039ECFF0119C6FF0000000000000000FFB5
          90FFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFB590FFFFCDB4FFFFB5
          90FF00000000000000000934DAFF0040EEFF000000000000000000000000FFCD
          B4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FFCDB4FFFFCDB4FFFFCDB4FFFFCD
          B4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FF00000000000000000000
          000000000000000000000000000000000000FFB590FFFFE6B4FFFFE6B4FFFFE6
          B4FFFFE6B4FFFFE6B4FFFFE6B4FFFFE6B4FFFFB590FF00000000000000000000
          00000000000000000000FFCDB4FFFFCDB4FFFFB590FFFFEFCFFFFFEFCFFFFFEF
          CFFFFFEFCFFFFFEFCFFFFFEFCFFFFFEFCFFFFFB590FF00000000000000000000
          00000000000000000000FFB590FFFFE6B4FFFFB590FFFFF3DAFFFFB590FFFFB5
          90FFFFB590FFFFB590FFFFF3DAFFFFF3DAFFFFB590FF00000000000000000000
          0000FFCDB4FFFFCDB4FFFFB590FFFFEFCFFFFFB590FFFFF6E4FFFFF6E4FFFFF6
          E4FFFFF6E4FFFFF6E4FFFFF6E4FFFFF6E4FFFFB590FF00000000000000000000
          0000FFB590FFFFE6B4FFFFB590FFFFF3DAFFFFB590FFFFF9ECFFFFB590FFFFB5
          90FFFFB590FFFFB590FFFFF9ECFFFFF9ECFFFFB590FF00000000000000000000
          0000FFB590FFFFEFCFFFFFB590FFFFF6E4FFFFB590FFFFFBF3FFFFFBF3FFFFFB
          F3FFFFFBF3FFFFFBF3FFFFFBF3FFFFFBF3FFFFB590FF00000000000000000000
          0000FFB590FFFFF3DAFFFFB590FFFFF9ECFFFFB590FFFFFEFCFFFFB590FFFFFE
          FCFFFFB590FFFFB590FFFFB590FFFFB590FFFFB590FF00000000000000000000
          0000FFB590FFFFF6E4FFFFB590FFFFFBF3FFFFB590FFFFFEFCFFFFFEFCFFFFFE
          FCFFFFB590FFFFB590FFFFEBC2FFFFCDB4FFFFB590FF00000000000000000000
          0000FFB590FFFFF9ECFFFFB590FFFFFEFCFFFFB590FFFFFEFCFFFFFEFCFFFFFE
          FCFFFFB590FFFFEBC2FFFFCDB4FFFFB590FF0000000000000000000000000000
          0000FFB590FFFFFBF3FFFFB590FFFFFEFCFFFFB590FFFFFEFCFFFFFEFCFFFFFE
          FCFFFFB590FFFFCDB4FFFFB590FF000000000000000000000000000000000000
          0000FFB590FFFFFEFCFFFFB590FFFFFEFCFFFFCDB4FFFFCDB4FFFFCDB4FFFFCD
          B4FFFFCDB4FFFFCDB4FF00000000000000000000000000000000000000000000
          0000FFB590FFFFFEFCFFFFB590FFFFFEFCFFFFFEFCFFFFFEFCFFFFB590FFFFCD
          B4FFFFB590FF0000000000000000000000000000000000000000000000000000
          0000FFB590FFFFFEFCFFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCD
          B4FF000000000000000000000000000000000000000000000000000000000000
          0000FFB590FFFFFEFCFFFFFEFCFFFFFEFCFFFFB590FFFFCDB4FFFFB590FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FFFFCDB4FF000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000840000FFFFFFFFFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000840000FF840000FF840000FFFFFFFFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000840000FF840000FF840000FF840000FFFFFFFFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000840000FF840000FF840000FFFFFFFFFF840000FF840000FFFFFFFFFF0000
          0000000000000000000000000000000000000000000000000000000000008400
          00FF840000FF840000FFFFFFFFFF0000000000000000840000FF840000FFFFFF
          FFFF000000000000000000000000000000000000000000000000000000000000
          0000840000FFFFFFFFFF00000000000000000000000000000000840000FFFFFF
          FFFF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008400
          00FFFFFFFFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000840000FFFFFFFFFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000840000FFFFFFFFFF000000000000000000000000000000000000
          00FF00000000000000FF00000000000000FF000000FF00000000000000000000
          0000000000FF000000FF840000FFFFFFFFFF0000000000000000000000000000
          00FF00000000000000FF00000000000000FF00000000000000FF000000000000
          00FF000000000000000000000000840000FFFFFFFFFF00000000000000000000
          00FF000000FF000000FF00000000000000FF000000FF00000000000000000000
          00FF00000000000000000000000000000000840000FF00000000000000000000
          00FF00000000000000FF00000000000000FF00000000000000FF000000000000
          00FF000000000000000000000000000000000000000000000000000000000000
          0000000000FF0000000000000000000000FF000000FF00000000000000000000
          0000000000FF000000FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000989898FF8E8E8EFFCC66
          00FFCC6600FFCC6600FFCC6600FFCC6600FFCC6600FFCC6600FF000000000000
          0000000000000000000096847DFFE0B4B3FFB2958AFFF9C5C3FFFFCACAFFCE6C
          08FFFFE2B4FFFFE2B4FFFFE2B4FFFFE2B4FFFFE2B4FFCE6C08FF000000000000
          000000000000BDA199FFEDB8B1FFFFC3C3FFE7B7AEFFFFC3C4FFFFC7C7FFD073
          0FFFFFE7C0FFFF0000FFFF0000FFFF0000FFFFE7C0FFD0730FFF000000000000
          0000B3988AFFB18787FFD4AA9AFFFFBCBDFFFFBCBCFFFFBCBCFFFFBCBCFFD379
          17FFFFECCDFFFFECCDFFFFECCDFFFFECCDFFFFECCDFFD37917FF00000000C4A9
          A1FFF9B2AFFFFFB5B5FFFFB6B6FFFFB5B5FFFEB4B4FFF0B4AEFFF8B3B0FFD580
          1FFFFFF0D9FFFF0000FFFF0000FFFF0000FFFFF0D9FFD5801FFF000000000000
          0000CDA895FFFFAFAFFFFFAEAEFFECA3A3FF0000000000000000BEA598FFD786
          27FFFFF5E6FFFFF5E6FFFFF5E6FFFFF5E6FFFFF5E6FFD78627FF000000009B92
          90FFB79288FFFCA7A5FFFFA8A8FFA1A1A1FF000000000000000000000000D98D
          2EFFFFFAF2FFFFFAF2FFFFFAF2FFD98D2EFFD98D2EFFD98D2EFF00000000D6A2
          93FFFFA1A1FFFFA1A1FFFFA1A1FF00000000000000000000000000000000DC93
          36FFFFFFFFFFFFFFFFFFFFFFFFFFDC9336FFC0C0C0FF0000000000000000D2A2
          91FFFF9A9AFFFF9B9BFFFF9B9BFFADADADFF000000000000000000000000DE9A
          3EFFDE9A3EFFDE9A3EFFDE9A3EFFDE9A3EFF000000000000000000000000BEA5
          98FFBEA598FFF39491FFFA9494FF646464FF0000000000000000000000000000
          0000C2A69FFFF69392FFFA9494FFFC9394FF848484FF00000000000000000000
          0000EB928EFFF48E8EFFF48E8EFFAC6F6FFFADADADFF00000000000000000000
          0000C7AC98FFF58D8EFFF78B8DFF91918FFF000000000000000000000000C7A9
          A1FFE08B86FFED8686FFED8686FFEC8686FF875D5DFF868686FF00000000A999
          94FFE68A86FFEC8686FFEC8686FF9F6566FF0000000000000000000000000000
          0000BEA598FFBEA598FFCE9A8AFFE57F7FFFE57F7FFFE37F7FFFDA7E7DFFE67F
          7FFFE57F7FFFE47F7FFFE67E7FFFDA7C7CFF0000000000000000000000000000
          000000000000BEA598FFD6817BFFDE7878FFDE7676FFDE7878FFDE7878FFDE77
          77FFDE7878FFB27170FFC5A697FF000000000000000000000000000000000000
          00000000000000000000CC8E81FFD96F70FFCBAE9DFFD67070FFD76E6FFFCAA0
          8EFFD86F70FFD87171FF00000000000000000000000000000000000000000000
          000000000000000000000000000000000000C4A89CFFCE6C6AFFD26667FFBEAA
          A4FFCA857DFF0000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFF0CAA6FFF0CAA6FFF0CAA6FFF0CAA6FFF0CA
          A6FFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFF0CAA6FFF0CAA6FFF0CAA6FFF0CAA6FFF0CA
          A6FFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFF0CAA6FFF0CAA6FFF0CAA6FFF0CAA6FFF0CA
          A6FFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          0000FF0000FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FF00000000FF0000FF0000000000000000000000000000
          0000FF0000FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFC0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FF00000000FF0000FF0000000000000000000000000000
          0000FF0000FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF848484FF8484
          84FF848484FFC0C0C0FF00000000FF0000FF0000000000000000000000000000
          0000FF0000FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF848484FF8484
          84FFC0C0C0FF0000000000000000FF0000FF0000000000000000FF0000FFFF00
          00FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFC0C0C0FF848484FFC0C0
          C0FF00000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF00000000FF00
          00FFFF0000FFFF0000FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FF0000
          00000000000000000000FF0000FFFF0000FFFF0000FF00000000000000000000
          0000FF0000FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000F0CAA6FFC0C0C0FFF0CA
          A6FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000F0CAA6FFF0CAA6FFC0C0C0FFFFFFFFFFC0C0
          C0FFF0CAA6FF0000000000000000000000000000000000000000000000000000
          00000000000000000000F0CAA6FF00000000C0C0C0FFFFFFFFFFC0C0C0FFFFFF
          FFFFC0C0C0FFF0CAA6FF00000000000000000000000000000000000000000000
          000000000000F0CAA6FFC0C0C0FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0
          C0FFFFFFFFFFC0C0C0FFF0CAA6FF00000000000000000000000000000000F0CA
          A6FFF0CAA6FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC0C0C0FFFFFFFFFFC0C0C0FFF0CAA6FF0000000000000000F0CAA6FF0000
          0000C0C0C0FFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FFFFFFFFFFC0C0C0FFF0CAA6FF00000000C0C0C0FFC0C0
          C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFC0C0C0FFF0CAA6FFC0C0C0FFFFFF
          FFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFC0C0C0FF00000000C0C0
          C0FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFC0C0C0FF00000000000000000000
          0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFF
          FFFFFFFFFFFFFFFFFFFFC0C0C0FFFF0000FF0000000000000000000000000000
          0000FF0000FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0
          C0FFC0C0C0FFC0C0C0FF00000000FF0000FF0000000000000000FF0000FFFF00
          00FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF0000
          000000000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF00000000FF00
          00FFFF0000FFFF0000FF00000000C0C0C0FFC0C0C0FFC0C0C0FF000000000000
          00000000000000000000FF0000FFFF0000FFFF0000FF00000000000000000000
          0000FF0000FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FFC0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FF00000000C0C0C0FFB4FF
          FFFFB4FFFFFFB4FFFFFFC0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0C0FF00000000C0C0C0FFB4FF
          FFFFB4FFFFFFB4FFFFFFC0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0C0FF00000000C0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FFC0C0C0FF0000000000000000C0C0C0FF000000000000
          0000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FF00000000000000000000
          0000C0C0C0FF000000000000000000000000C0C0C0FF00000000C0C0C0FF0000
          00000000000000000000C0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FF0000000000000000C0C0C0FF00000000B4FFFFFF00000000C0C0
          C0FF0000000000000000C0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFC0C0C0FFC0C0C0FF00000000B4FFFFFF00000000B4FFFFFF0000
          0000C0C0C0FFC0C0C0FFC0C0C0FF000000000000000000000000000000000000
          0000FF0000FF0000000000000000C0C0C0FF00000000B4FFFFFF00000000C0C0
          C0FF000000000000000000000000FF0000FF0000000000000000000000000000
          0000FF0000FF000000000000000000000000C0C0C0FF00000000C0C0C0FF0000
          0000000000000000000000000000FF0000FF0000000000000000000000000000
          0000FF0000FF00000000000000000000000000000000C0C0C0FF000000000000
          0000000000000000000000000000FF0000FF0000000000000000000000000000
          0000FF0000FF0000000000000000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0
          C0FF000000000000000000000000FF0000FF0000000000000000FF0000FFFF00
          00FFFF0000FFFF0000FFFF0000FFC0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0
          C0FF00000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF00000000FF00
          00FFFF0000FFFF0000FF00000000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0
          C0FF0000000000000000FF0000FFFF0000FFFF0000FF00000000000000000000
          0000FF0000FF0000000000000000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0
          C0FF000000000000000000000000FF0000FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFF0CAA6FFF0CAA6FFF0CAA6FFF0CAA6FFF0CA
          A6FFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFF0CAA6FFF0CAA6FFF0CAA6FFF0CAA6FFF0CA
          A6FFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          000000000000C0C0C0FFFFFFFFFFF0CAA6FFF0CAA6FFF0CAA6FFF0CAA6FFF0CA
          A6FFFFFFFFFFC0C0C0FF00000000000000000000000000000000000000000000
          00000000FFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FF000000000000FFFF0000000000000000000000000000
          FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFC0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFC0C0C0FF848484FF8484
          84FF848484FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000
          00000000FFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF848484FF8484
          84FFC0C0C0FF00000000000000000000FFFF0000000000000000000000000000
          00000000FFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF848484FFC0C0
          C0FF0000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FF0000
          00000000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000FFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000F0CAA6FFC0C0C0FFF0CA
          A6FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000F0CAA6FFF0CAA6FFC0C0C0FFFFFFFFFFC0C0
          C0FFF0CAA6FF0000000000000000000000000000000000000000000000000000
          00000000000000000000F0CAA6FF00000000C0C0C0FFFFFFFFFFC0C0C0FFFFFF
          FFFFC0C0C0FFF0CAA6FF00000000000000000000000000000000000000000000
          000000000000F0CAA6FFC0C0C0FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0
          C0FFFFFFFFFFC0C0C0FFF0CAA6FF00000000000000000000000000000000F0CA
          A6FFF0CAA6FFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC0C0C0FFFFFFFFFFC0C0C0FFF0CAA6FF0000000000000000F0CAA6FF0000
          0000C0C0C0FFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFC0C0C0FFFFFFFFFFC0C0C0FFF0CAA6FF00000000C0C0C0FFC0C0
          C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFC0C0C0FFF0CAA6FFC0C0C0FFFFFF
          FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFC0C0C0FF000000000000
          FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFF
          FFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000
          00000000FFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0
          C0FFC0C0C0FFC0C0C0FF000000000000FFFF0000000000000000000000000000
          00000000FFFF00000000C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF0000
          00000000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFF0000000000000000C0C0C0FFC0C0C0FFC0C0C0FF000000000000
          00000000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000FFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000C0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FFC0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FF00000000C0C0C0FFB4FF
          FFFFB4FFFFFFB4FFFFFFC0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0C0FF00000000C0C0C0FFB4FF
          FFFFB4FFFFFFB4FFFFFFC0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0C0FF00000000C0C0C0FFC0C0
          C0FFC0C0C0FFC0C0C0FFC0C0C0FF0000000000000000C0C0C0FF000000000000
          0000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FF00000000000000000000
          0000C0C0C0FF000000000000000000000000C0C0C0FF00000000C0C0C0FF0000
          00000000000000000000C0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FF0000000000000000C0C0C0FF00000000B4FFFFFF00000000C0C0
          C0FF0000000000000000C0C0C0FF000000000000000000000000000000000000
          0000C0C0C0FFC0C0C0FFC0C0C0FF00000000B4FFFFFF00000000B4FFFFFF0000
          0000C0C0C0FFC0C0C0FFC0C0C0FF000000000000000000000000000000000000
          00000000FFFF0000000000000000C0C0C0FF00000000B4FFFFFF00000000C0C0
          C0FF0000000000000000000000000000FFFF0000000000000000000000000000
          FFFF0000FFFF0000FFFF0000000000000000C0C0C0FF00000000C0C0C0FF0000
          000000000000000000000000FFFF0000FFFF0000FFFF000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000000000000000C0C0C0FF000000000000
          0000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000
          00000000FFFF0000000000000000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0
          C0FF0000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFF0000000000000000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0
          C0FF0000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFF0000000000000000C0C0C0FFB4FFFFFFB4FFFFFFB4FFFFFFC0C0
          C0FF0000000000000000000000000000FFFF0000000000000000000000000000
          00000000FFFF0000000000000000C0C0C0FFC0C0C0FFC0C0C0FFC0C0C0FFC0C0
          C0FF0000000000000000000000000000FFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007171
          71FF717171FF717171FF6C6C6CFF6C6C6CFF6C6C6CFF6C6C6CFF6C6C6CFF0000
          00000000000000000000000000000000000000000000000000006C6C6CFF7676
          76FF5B5B5BFF5B5B5BFF5B5B5BFF5B5B5BFF595959FF646464FF717171FF6C6C
          6CFF6C6C6CFF6C6C6CFF6C6C6CFF717171FF717171FF6C6C6CFF828282FF7474
          74FF8E8E8EFF999999FF8D8D8DFF858585FF8D8D8DFF6C6C6CFF424242FF4242
          42FF515151FF5A5A5AFF515151FF3A3A3AFF424242FF6C6C6CFF909090FF8E8E
          8EFF9F9F9FFFA6A6A6FF909090FFCECECEFF919191FF949494FF686868FF6F6F
          6FFF7D7D7DFF858585FF777777FF656565FF474747FF6C6C6CFF000000008282
          82FFA3A3A3FF969696FFCECECEFFCECECEFF939393FF959595FF868686FF9191
          91FFEFEFEFFF6F6F6FFF949494FF757575FF5A5A5AFF6C6C6CFF000000000000
          0000767676FF3E3E3EFF3D3D3DFF686868FF767676FF686868FF9A9A9AFFAAAA
          AAFFF4F4F4FFAAAAAAFF7E7E7EFF646464FF0000000000000000000000001212
          12FF080808FF343434FF525252FF464646FF303030FF545454FF7D7D7DFF6F6F
          6FFF6D6D6DFF727272FF595959FF4D4D4DFF00000000000000003F3F3FFF1A1A
          1AFF323232FF5E5E5EFF646464FF616161FF464646FF545454FF909090FF8F8F
          8FFF999999FF999999FF8F8F8FFF515151FF848484FF00000000121212FF2C2C
          2CFF454545FF7C7C7CFF7A7A7AFF7A7A7AFF686868FF5F5F5FFF878787FFA1A1
          A1FFA1A1A1FFA1A1A1FF9F9F9FFF7A7A7AFF6C6C6CFF000000002C2C2CFF3636
          36FF515151FF939393FF999999FF959595FF747474FF5F5F5FFF929292FFAAAA
          AAFFAAAAAAFFAAAAAAFFA7A7A7FF8C8C8CFF666666FF000000006C6C6CFF4D4D
          4DFF3F3F3FFF454545FF525252FF6C6C6CFF535353FF7E7E7EFF9D9D9DFFA6A6
          A6FFA6A6A6FFAFAFAFFFADADADFF9D9D9DFF6C6C6CFF00000000000000003F3F
          3FFF666666FF8F8F8FFFA4A4A4FF4D4D4DFF454545FF00000000A0A0A0FF7272
          72FF9D9D9DFFA0A0A0FF929292FF626262FF909090FF00000000000000000000
          00006C6C6CFF5E5E5EFF5E5E5EFF666666FF0000000000000000000000007E7E
          7EFFB8B8B8FFC9C9C9FF929292FF909090FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00007E7E7EFF7E7E7EFF7E7E7EFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000454545FF454545FF000000000000
          0000454545FF454545FF00000000000000000000000000000000000000000000
          00000000000000000000454545FF454545FF6C6C6CFF6C6C6CFF454545FF4545
          45FF6C6C6CFF6C6C6CFF454545FF454545FF0000000000000000000000000000
          0000454545FF454545FF6C6C6CFF6C6C6CFF6C6C6CFF6C6C6CFF454545FF9292
          92FF454545FF6C6C6CFF454545FF929292FF454545FF00000000000000004545
          45FF6C6C6CFF787878FF787878FF6C6C6CFF6C6C6CFF6C6C6CFF6C6C6CFF9292
          92FF454545FF6C6C6CFF6C6C6CFF929292FF454545FF00000000000000004545
          45FF6C6C6CFFD2D2D2FF858585FF787878FF787878FF6C6C6CFF6C6C6CFFABAB
          ABFF454545FF6C6C6CFF6C6C6CFFABABABFF454545FF00000000000000004545
          45FF6C6C6CFFD2D2D2FFD2D2D2FF858585FF858585FF787878FF6C6C6CFFABAB
          ABFF454545FF787878FF6C6C6CFFABABABFF454545FF00000000000000004545
          45FF6C6C6CFFDEDEDEFFD2D2D2FFC0C0C0FF858585FF787878FF6C6C6CFFABAB
          ABFF454545FF787878FF6C6C6CFFABABABFF454545FF00000000000000004545
          45FF6C6C6CFFDEDEDEFFD2D2D2FFB8B8B8FFABABABFF787878FF6C6C6CFFABAB
          ABFF454545FF787878FF6C6C6CFFABABABFF454545FF00000000000000004545
          45FF6C6C6CFFDEDEDEFFDEDEDEFFD2D2D2FFC0C0C0FF858585FF6C6C6CFFABAB
          ABFF454545FF858585FF6C6C6CFFABABABFF454545FF00000000000000004545
          45FF6C6C6CFFDEDEDEFFDEDEDEFFD2D2D2FFC0C0C0FF858585FF6C6C6CFFABAB
          ABFF454545FF858585FF6C6C6CFFABABABFF454545FF00000000000000004545
          45FF6C6C6CFFDEDEDEFFDEDEDEFFDEDEDEFFD2D2D2FFD2D2D2FF6C6C6CFFB8B8
          B8FF454545FFD2D2D2FF6C6C6CFFB8B8B8FF454545FF00000000000000004545
          45FF6C6C6CFFF8F8F8FFF8F8F8FFD2D2D2FF787878FF787878FFFFFFFFFFD2D2
          D2FF454545FF787878FFFFFFFFFFD2D2D2FF454545FF00000000000000004545
          45FF6C6C6CFFFFFFFFFFC5C5C5FFD2D2D2FFFFFFFFFFFFFFFFFF454545FF4545
          45FFFFFFFFFFFFFFFFFF454545FF454545FF0000000000000000000000000000
          0000454545FF454545FFDEDEDEFFFFFFFFFF454545FF454545FF000000004545
          45FF454545FF454545FF00000000000000000000000000000000000000000000
          00000000000000000000454545FF454545FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          00000000000000000000808080FF808080FF808080FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          00FF0000000000000000808080FF0000000000000000808080FF000000000000
          00000000000000000000000000FF00000000000000FF00000000000000FF0000
          00FF000000FF00000000808080FF0000000000000000808080FF000000000000
          0000000000FF0000000000000000000000000000000000000000000000FF0000
          00FF0000000000000000808080FF808080FF808080FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          00000000000000000000808080FF0000000000000000808080FF000000000000
          00FF000000000000000000000000000000000000000000000000000000000000
          00000000000000000000808080FF0000000000000000808080FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000FF00000000808080FF808080FF808080FF00000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00FF000000000000000000000000000000000000000000000000000000FF0000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          00000000000000000000000000FF000000000000000000000000000000000000
          00FF000000000000000000000000000000000000000000000000808080FF0000
          00FF000000FF000000FF808080FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00FF00000000000000FF00000000000000000000000000000000000000000000
          0000000000FF00000000000000FF000000000000000000000000000000008080
          80FF000000FF808080FF00000000000000000000000000000000000000000000
          000000000000000000FF000000FF000000000000000000000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          0000000000FF000000FF000000FF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000848484FF000000FF000000FF000000FF000000FF00000000000000003131
          42FF181039FF313142FF6B6B6BFF8C8C8CFF0000000000000000000000000000
          0000848484FF4A7B7BFF427373FF426B63FF000000FF0000000029086BFF4A10
          D6FF4200D6FF4208D6FF311084FF7B7B7BFF848484FF00000000000000000000
          0000848484FF4A8C84FF4A7B7BFF427373FF000000FF000000004A10D6FF6329
          DEFF00000000000000006329DEFF21085AFF737373FF00000000000000000000
          0000313131FF4A948CFF4A8C84FF4A7B7BFF000000FF848484FF7342DEFF6329
          DEFF4A10D6FF00000000000000003908BDFF737373FF00000000000000008484
          84FF525252FF52A59CFF4A948CFF4A8C84FF000000FF000000FF4A10D6FF0000
          00006329DEFF4A10D6FF000000007B42FFFF212121FF00000000000000008484
          84FF5AB5ADFF52A59CFF4A948CFF4A8C84FF4A8C84FF000000FF4A29B5FF0000
          0000000000006329DEFF4A10D6FF6329DEFF313142FF00000000000000008484
          84FF73BDB5FF63ADADFF4A948CFF4A948CFF4A8C84FF000000FF5A5A9CFF6329
          DEFF00000000000000006329DEFF4208D6FF525252FF00000000000000008484
          84FF7BC6BDFF73BDB5FF52A5A5FF4A948CFF42847BFF000000FF526BA5FF634A
          ADFF4A18CEFF4200D6FF4A10D6FF7B63B5FFCECECEFF00000000000000000000
          000000000000636363FF313131FF181818FF393939FF6B84BDFF7384ADFF4A63
          94FF4A638CFF4A5A7BFF000000FFDEDECEFF0000000000000000000000000000
          0000393939FF9CCECEFF8CA5A5FF637B7BFF000000FF7B94C6FF6B84BDFF526B
          A5FF4A6394FF425A84FF000000FF000000000000000000000000000000000000
          0000737373FFD6D6D6FF9CCECEFF7B9494FF000000FF636363FF7384ADFF526B
          A5FF4A6394FF080808FF00000000000000000000000000000000000000000000
          0000636363FFADDEDEFFCED6D6FF949C9CFF101008FF00000000636363FF3131
          31FF181818FF0000000000000000000000000000000000000000000000000000
          0000000000005A5A5AFF737373FF5A5A5AFF00000000393939FF9CADCEFF8C94
          ADFF636B84FF000000FF00000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000737373FFD6D6D6FF9CAD
          CEFF6B7394FF000000FF00000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000636363FFADBDDEFFCED6
          D6FF7B9494FF101008FF00000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000005A5A5AFF7373
          73FF5A5A5AFF0000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF48C2FFFF48C2FFFF48C2FFFF48C2FFFF48
          C2FF00000000FF48C2FFFF48C2FFFF48C2FFFF48C2FFFF48C2FF000000000000
          0000000000000000000000000000FF48C2FFF0CAA6FFF0CAA6FFF0CAA6FFFF48
          C2FF00000000FF48C2FFF0CAA6FFF0CAA6FFF0CAA6FFFF48C2FF000000000000
          0000000000000000000000000000FF48C2FFFF48C2FFFF48C2FFFF48C2FFFF48
          C2FF00000000FF48C2FFFF48C2FFFF48C2FFFF48C2FFFF48C2FF000000000000
          00000000000000000000000000000000000000000000FF48C2FF000000000000
          0000000000000000000000000000FF48C2FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF48C2FFFF48
          C2FF00000000FF48C2FFFF48C2FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FF48C2FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF48C2FFFF48
          C2FFFF48C2FFFF48C2FFFF48C2FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF48C2FFF0CA
          A6FFF0CAA6FFF0CAA6FFFF48C2FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF48C2FFFF48
          C2FFFF48C2FFFF48C2FFFF48C2FF000000000000000000000000000000000000
          000000000000000000000000000000000000FF0000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FFFF0000FFFF0000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF0000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FFFF0000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FF0000FFFF0000FFFF0000FFFF0000FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000BAFF0000BAFF0000BAFF0000BAFF0000
          BAFF000000000000BAFF0000BAFF0000BAFF0000BAFF0000BAFF000000000000
          00000000000000000000000000000000BAFF0000FFFF0000FFFF0000FFFF0000
          BAFF000000000000BAFF0000FFFF0000FFFF0000FFFF0000BAFF000000000000
          00000000000000000000000000000000BAFF0000BAFF0000BAFF0000BAFF0000
          BAFF000000000000BAFF0000BAFF0000BAFF0000BAFF0000BAFF000000000000
          00000000000000000000000000000000000000000000000000FF000000000000
          0000000000000000000000000000000000FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000FF0000
          00FF00000000000000FF000000FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000BAFF0000
          BAFF0000BAFF0000BAFF0000BAFF000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000BAFF0000
          FFFF0000FFFF0000FFFF0000BAFF000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000BAFF0000
          BAFF0000BAFF0000BAFF0000BAFF000000000000000000000000000000000000
          000000000000000000000000000000000000FF0000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FFFF0000FFFF0000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF0000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FFFF0000FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FF0000FFFF0000FFFF0000FFFF0000FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000858585FF9F9F
          9FFF9F9F9FFF5F5F5FFF1F1F1FFF393939FF393939FF00000000000000000000
          00000000000000000000000000000000000000000000000000FFABABABFFC0C0
          C0FFD2D2D2FF9F9F9FFF808080FFEBEBEBFF9F9F9FFF858585FF000000000000
          00000000000000000000000000000000000000000000000000FF090909FF4545
          45FF454545FF121212FF5F5F5FFF6C6C6CFF454545FF090909FF000000000000
          00000000000000000000000000000000000000000000090909FF393939FF5252
          52FF525252FF090909FF393939FF9F9F9FFF9F9F9FFF1F1F1FFF000000000000
          000000000000000000000000000000000000000000001F1F1FFF393939FF9F9F
          9FFF9F9F9FFF1F1F1FFF6C6C6CFFABABABFFABABABFF9F9F9FFF000000000000
          00000000000000000000000000000000000000000000121212FF454545FF4545
          45FF6C6C6CFF1F1F1FFF787878FFABABABFFABABABFFABABABFF000000000000
          00000000000000000000000000000000000000000000000000002C2C2CFF4545
          45FF1F1F1FFF000000006C6C6CFFC0C0C0FFABABABFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FF48
          48FFFF6C6CFFFF8548FFD94600FF2A3E00FF007C28FF007C28FF005D1EFF0000
          0000000000000000000000000000FF0000FF0000000000000000000000FFFF9D
          6CFFFFB590FFFFDA90FFFF6C6CFF808080FFEBEBEBFF6CFF6CFF48FF48FF0000
          00000000000000000000FF0000FFFF0000FFFF0000FF00000000000000000909
          09FF00329BFF00329BFF003E00FF5F5F5FFF007EBAFF007C54FF000000000000
          000000000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF090909FF003F
          5DFF003CBAFF003CBAFF090909FF003F5DFF4885FFFF4885FFFF002A3EFF0000
          0000000000000000000000000000FF0000FF00000000000000001F1F1FFF003F
          5DFF4885FFFF4885FFFF00143EFF007EBAFF48C2FFFF48C2FFFF4885FFFF0000
          00000000000000000000FF0000FFFF0000FF0000000000000000121212FF4545
          45FF00329BFF007EBAFF00143EFF0093D9FF48C2FFFF48C2FFFF48C2FFFFFF00
          00FFFF0000FFFF0000FFFF0000FF000000000000000000000000000000002C2C
          2CFF454545FF1F1F1FFF009B9BFF009B9BFF90B5FFFF6C9DFFFF000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E0000FF1E00
          00FF220000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000500E09FF984F37FF924A
          33FF8D3A1BFF550800FF00000000000000000000000000000000000000000000
          0000000000FF000000FF000000FF000000FF5C2828FFC37E62FFF4B18BFFE8A2
          7CFFDC8E65FFB15123FF470502FF000000FF0000000000000000000000000C72
          A5FF0C72A5FF0C72A5FF0C72A5FF0C72A5FF360000FFD79C83FFF7BFA3FFEBB1
          95FFDE9D7FFFCF7F57FF5A130AFF000000FF000000FF00000000189AC6FF1B9C
          C7FF9CFFFFFF6BD7FFFF6BD7FFFF6BD7FFFF3D0000FFDDA791FFFFE5C9FFFFD2
          B7FFFABC9CFFDA8F6AFF57140CFF0C72A5FF000000FF00000000189AC6FF199A
          C6FF79E4F0FF9CFFFFFF7BE3FFFF7BE3FFFF440404FFDFA791FFB98D7CFFBB83
          72FFAD7766FFC9815EFF621C12FF197A9DFF000000FF00000000189AC6FF25A2
          CFFF3FB8D7FF9CFFFFFF84EBFFFF84EBFFFF360000FF804339FF905749FF8C4E
          41FF75311DFF6D1E0EFF50110CFF189AC6FF000000FF000000FF189AC6FF42B3
          E2FF20A0C9FFA5FFFFFF94F7FFFF94F7FFFF572626FFB77E6CFFFFFFDFFFFFE2
          C2FFFFB58AFFA04119FF390000FF5BBCCEFF0C72A5FF000000FF189AC6FF6FD5
          FDFF189AC6FF89F0F7FF9CFFFFFF9CFFFFFF9CFFFFFF5E201BFFB28070FFAB72
          62FFA5634CFF662115FF9CFFFFFF96F9FBFF187A9BFF000000FF189AC6FF84D7
          FFFF189AC6FF6BBFDAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E201BFF5E20
          1BFF5E201BFFFFFFFFFFFFFFFFFFFFFFFFFF187DA1FF000000FF189AC6FF84EB
          FFFF4FC1E2FF189AC6FF189AC6FF189AC6FF189AC6FF189AC6FF189AC6FF189A
          C6FF189AC6FF189AC6FF189AC6FF189AC6FF1889B1FF00000000189AC6FF9CF3
          FFFF8CF3FFFF8CF3FFFF8CF3FFFF8CF3FFFF8CF3FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF189AC6FF197A9DFF0000000000000000189AC6FFFFFF
          FFFF9CFFFFFF9CFFFFFF9CFFFFFF9CFFFFFFFFFFFFFF189AC6FF189AC6FF189A
          C6FF189AC6FF189AC6FF189AC6FF0000000000000000000000000000000021A2
          CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF189AC6FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000021A2CEFF21A2CEFF21A2CEFF21A2CEFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000008080000080
          80000080800000808000008080006A2C8F000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          800000808000008080009A3FCF00662A8A000080800000808000A8A39F00B4B0
          AC00C0BBB700C6C1BC00C0BBB700B4B0AC009F99940000808000008080000080
          800000808000008080009C40D2008034AC0062288400542270004B1F6500421B
          5900401A5600FCF8F400FBF7F200EFE6DC00DCD2C800C6C0B90000808000387D
          5B0067A98900993ECD009E41D500A242DA00A041D700A142D800A041D7008F3A
          BF00662A8900451C5D00FFFFFF00FAF6F200E3D8CD00C4BDB70000808000BFDC
          CE00B259E600AE51E500AA49E400A846E300AA49E400B159E600C37EEC00CE96
          EF00CA8CEE00B159E600692B8E00FFFFFF00E6DCD100BDB8B30000808000C6E2
          D500CD92EF00D098F000C787ED00B867E800A644DF00963DCA00FFFFFF00FFFF
          FE00FFFFFF00D29DF000BB6FE9008436B200EDE0D300B7AFA60000808000CDE6
          DA00C6E2D500BCDCCC00D3A0F100D19BF000BF74EA008034AD00BEBDB400B2B2
          B2002276CD00889AAB00E9E0D700B762E700BEB6AF000080800000808000DEEF
          E700C7E3D500B9D5C700B1C1CD00A6B4C100CF99F000B35BE60041799100668C
          B5004E82B9003A7DC30070707000D5CBC1000080800000808000008080000080
          800000808000B0CAC0009FB1C300A4B4C2009CAAA8000080800099ACC0007E33
          A900829DBA005B8AB80000808000008080000080800000808000008080000080
          8000943DC70060288200C9D6E400D6E1ED009AACBC00A9B1BA00BCCADA00A443
          DD0055237200411B5800B1B3B500008080000080800000808000008080000080
          8000C078EB00A846E3005C267C00F5F9FD00BDCBD800B9C3CB00E0E9F300A945
          E300913CC400712E980051216D004A1E64000080800000808000008080000080
          8000A4BCC400CB8FEE00B866E80076309F004B1F6500461D5E00732F9B00963E
          CB00A745E200A443DD00963DCA006E2D94000080800000808000008080000080
          8000BDD2D900BED2DB00D29DF000CD92EF00BD71EA00AF51E500AC4BE400AC4C
          E400AC4DE400AB4AE400AA49E400008080000080800000808000008080000080
          8000FFFFFF00CCE1E700ADC6CE00AABFC900D29DF000CF97EF00CC90EE00CA8C
          EE00C27AEB00B054E5009C40D200008080000080800000808000008080000080
          800000808000FFFFFF00FCFEFE00D4E8EC00BAD5DB00D1DFE300008080000080
          8000CC91EE00B158E60000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000CC90EE00B867E80000808000008080000080800000808000}
        MaskColor = clOlive
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000100
          0000031C1C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00005B5B00014E4E000072
          7200006E6E0002010100002F2F0000464600FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF000292920003A5A500003232000881
          81005BC3C300002C2C00148D8D002681810002000000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00041F1F00004141003AAAAA0046ADAD002B8B
          8B0070F0F0003476760064CCCC004B8A8A00004F4F00002B2B00FF00FF00FF00
          FF00C5A6A600905A5A00540E0E000164640034C0C0005EBEBE0087CFCF006876
          7600566A6A002F50500052D6D600359797002EB0B0001F343400FF00FF00B68E
          8E00A47B7B00CAB4B400BCB2B2004F8D8D001862620073D7D7008B7F7F00C4BF
          BF00B3B0B00038292900449B9B006AC2C20048474700FF00FF00C19D9D00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF004944440073DCDC007DCDCD00BCBC
          BC00B1ACAC001630300045DFDF002FA4A400FF00FF00FF00FF003E121200513A
          3A00523C3C003A1717004B070700672F2F003F5757003BAFAF00211C1C009B9C
          9C00A3A4A400363030003B5D5D007EAEAE00FF00FF00FF00FF00100000000000
          0000000000000000000000000000000000000300000026060600110000008467
          6700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0044101000624A
          4A00644D4D00502B2B0062414100806A6A005C3737002E080800190202001B00
          000070505000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E0C7C700FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0058313100250000006B3737009974
          7400472B2B003B181800B0929200D7B4B400FF00FF00FF00FF00FF00FF00C7A3
          A300AC848400BB9A9A00AD8D8D004019190024000000814F4F00FF00FF00FF00
          FF00FF00FF00755353003D18180035161600683F3F00FF00FF00FF00FF00FF00
          FF0060424200844C4C005F2B2B0087707000D6C1C100FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF006B3C3C002C11110039171700B2838300FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        MaskColor = clFuchsia
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF0000
          00FF000000FF000000FF000000FF000000000000000000000000848484FFC6C6
          C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FF000000FF000000FFC6C6C6FF0000
          00FF008484FF000000FF000000FF000000FF000000FF00000000848484FFC6C6
          C6FFC6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFF008484FF008484FF000000FF0000
          00FF00FFFFFF008484FF000000FF008484FF00FFFFFF000000FF848484FFC6C6
          C6FFC6C6C6FF848484FF000000FFC6C6C6FF000000FF00FFFFFF008484FF0000
          00FFFFFFFFFF008484FF008484FF00FFFFFF000000FF00000000848484FFC6C6
          C6FFC6C6C6FF848484FF000000FF000000FF008484FF000000FF00FFFFFFFFFF
          FFFF00FFFFFFFFFFFFFF00FFFFFF000000FF008484FF000000FF848484FFC6C6
          C6FFC6C6C6FF848484FF000000FF000000FF008484FF00FFFFFFFFFFFFFF0000
          00FF000000FF000000FFFFFFFFFF00FFFFFF008484FF000000FF848484FFC6C6
          C6FFC6C6C6FF848484FF848484FF008484FF000000FFFFFFFFFF000000FF8484
          84FFFFFFFFFF000000FF000000FF008484FF000000FF000000FF848484FFC6C6
          C6FFC6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFF000000FF008484FF00FFFFFF8484
          84FFFFFFFFFF000000FF008484FF00FFFFFF000000FF00000000848484FFC6C6
          C6FFC6C6C6FF848484FF000000FFC6C6C6FF008484FF00FFFFFF000000FF8484
          84FFFFFFFFFF000000FF000000FF008484FF000000FF00000000848484FFC6C6
          C6FFC6C6C6FF848484FF000000FFFFFFFFFF008484FF008484FFC6C6C6FF8484
          84FF848484FFC6C6C6FF000000FF000000000000000000000000848484FFC6C6
          C6FFC6C6C6FF848484FF000000FF000000FFC6C6C6FFFFFFFFFFC6C6C6FFC6C6
          C6FFC6C6C6FFC6C6C6FF000000FF000000000000000000000000848484FFC6C6
          C6FFC6C6C6FF848484FF848484FF848484FF848484FFFFFFFFFFC6C6C6FFC6C6
          C6FFC6C6C6FFC6C6C6FF000000FF000000000000000000000000848484FFC6C6
          C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6
          C6FFC6C6C6FFC6C6C6FF000000FF000000000000000000000000848484FFC6C6
          C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6C6FFC6C6
          C6FFC6C6C6FFC6C6C6FF000000FF000000000000000000000000848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF000000FF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000FCFCFCFFEAEAEAFFCBCBCBFFB6B6B6FFB1B1B1FFB3B3B3FFC3C3
          C3FFE3E3E3FFF8F8F8FFFEFEFEFF000000000000000000000000000000000000
          0000F5F5F5FFC1C1C1FFB1A49EFFEAD5CAFFF0DCD1FFF0DCD1FFEFDBD0FFC0AF
          A5FF585858FFA7A7A7FFEAEAEAFFFEFEFEFF000000000000000000000000F3F3
          F3FFA7A6A6FFF2DFD5FFF2F4F5FFEBEEEFFFDFE2E3FFECEFF1FFEDF0F2FFEFF2
          F2FFF4E9E3FF988982FF838383FFE5E5E5FFFEFEFEFF00000000F9F9F9FFBDB8
          B5FFF6F1EFFFF0F4F5FFD5AB95FFBB5B2AFFC06133FFC67349FFBB5827FFC987
          67FFEDF0F2FFF4F6F6FFC1AEA4FF8C8C8CFFEFEFEFFF00000000DCDCDCFFF6E8
          E1FFF2F7F8FFC36D43FFC36231FFCA622EFFC1C4C5FFFFFFFFFFC76330FFC462
          32FFBC5A28FFECEAE9FFF6F7F7FF726863FFBCBCBCFFFCFCFCFFE6D3C8FFF6F8
          F9FFCE8866FFC66433FFCC6734FFCC6633FFC98563FFDDA88FFFCC6431FFCC66
          34FFC66333FFBE5D2CFFF1F5F7FFF2DFD5FF757575FFF0F0F0FFF4E5DCFFF4F4
          F4FFC35E2BFFCD6836FFCC6734FFCC6532FFCB957CFFEAC0AAFFCB622EFFCC66
          33FFCC6734FFC56332FFDCB6A3FFF6F6F5FF595654FFDBDBDBFFFCF8F5FFE4BC
          A9FFCC6938FFCE6836FFCC6633FFCC6632FFC1AEA5FFFFFFFFFFCA5D28FFCC66
          33FFCD6734FFCB6735FFC77349FFF7F9FBFFBFADA5FFCECECEFFFFFEFEFFDFA0
          81FFD26E3CFFCE6936FFCC6633FFCC6633FFC66839FFF1F6F8FFFAF1ECFFCA60
          2BFFCC6734FFCE6937FFC86939FFF9FCFEFFC9B7AEFFCCCCCCFFFEFBFBFFECB9
          9FFFDA7644FFD06B38FFCC6633FFCC6633FFCC6633FFC5693AFFFCFFFFFFF5E0
          D7FFCD6531FFD26D3BFFCE7143FFFFFFFFFFC8B7ADFFD5D5D5FFF8ECE6FFFEFA
          F7FFE8824FFFD8723FFFCF9071FFE7B9A3FFCB622DFFCC6633FFE1CDC2FFFFFF
          FFFFD06733FFD87441FFE5AB8EFFFFFEFEFF988E88FFEAEAEAFFF0DBD0FFFFFF
          FFFFFDA374FFF48F5CFFC69F8CFFFFFFFFFFE6B299FFD17F56FFFCFEFFFFEDED
          ECFFDF7641FFE07744FFFFFFFFFFF6E9E1FFA9A9A9FFF9F9F9FFF4F3F2FFFDFA
          F9FFFFFFFFFFFFB580FFFFA26EFFD7CECBFFF5F9FBFFF7FBFCFFF1F6F9FFF19C
          73FFFA9360FFFEE2D2FFFFFFFFFFCFBDB3FFE4E4E4FFFEFEFEFFFEFEFEFFEEDD
          D3FFFFFFFFFFFFFFFFFFFFEFC6FFFFDDA6FFFFC696FFFDBD8CFFFFBB87FFFFC2
          8EFFFFFCF8FFFFFFFFFFF1DDD2FFD0D0D0FFFCFCFCFF0000000000000000FEFE
          FEFFEEDDD5FFFCF7F5FFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFCFFFFFFFDFFFFFF
          FFFFFFFFFFFFEFDBD0FFD4D4D4FFFBFBFBFF0000000000000000000000000000
          0000FEFEFEFFF4F4F3FFEFDCD0FFF6E9E1FFFDFAF9FFFEFCFBFFF9EEE9FFEFDB
          D0FFD1C7C2FFEDEDEDFFFDFDFDFF000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000005022
          34FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000CB9F72FF7A43
          32FF000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000CDAA8DFFEBC39BFF8655
          4AFF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000085626DFF8C6053FFA37B65FFC4A382FFEBC39BFFB188
          68FF4D1924FF502234FF00000000000000000000000000000000000000000000
          0000B29892FFB89B7EFFC4A78AFFD0AF8DFFDDBA96FFEEC9A4FFFCD4AEFFDDBA
          96FFC4A78AFFBA9673FF4D1924FF00000000000000000000000000000000BB99
          89FFC3AB94FFDDC2A7FFF9D8B8FFF2CFAEFFD17B4CFFCF8256FFE7986AFFCF82
          56FFF9D5B1FFE0BD9BFFC4A78AFF74413FFF0000000000000000B79F88FFC1AE
          9BFFE9D0B8FFFFE3C7FFFFE2C4FFFFE1C3FFC09170FF952E00FF873A14FFFFE9
          CBFFFFDDBAFFFFDDBAFFE8C7A7FFC7AB8FFF4D1924FF00000000BCA799FFE4CD
          B5FFFFE7D1FFFFE6CDFFFFE5CBFFFFE4C9FFC4997BFF9A3300FFA1562EFFFFE4
          C9FFFFE1C3FFFFE0C0FFFFE0C0FFE2C3ABFFC4A382FF502234FFD0C1B2FFF9E6
          D3FFFFEAD5FFFFE9D4FFFFE9D2FFFFE8D1FFC4997BFF9A3300FFA05831FFFFE7
          CEFFFFE4C9FFFFE3C9FFFFE3C7FFF8DCC1FFD4BBA5FF663D4DFFD6CABEFFFFEF
          DFFFFFEEDDFFFFEDDCFFFFEDDBFFFFEBD8FFC49C81FF9A3300FFA05831FFFFEB
          D7FFFFE7D1FFFFE7CEFFFFE6CDFFFFE7CEFFDDC9B3FF663D4DFFDCCFC7FFFFF4
          E7FFFFF2E5FFFFF1E2FFFFF0E0FFFFF1E2FFA9633FFF9C3400FFA25A35FFFFF0
          DCFFFFEBD7FFFFEBD7FFFFE9D4FFFFEFD9FFDDC9B3FF694151FFDDD7D1FFFBF4
          ECFFFFF4ECFFFFF3E9FFFFF3E9FFDAC6B6FFA37B65FF996E5FFFA37B65FFFFF2
          E5FFFFEFDFFFFFEEDDFFFFEDDBFFFCF0DFFFE4D2C0FF00000000E3DCD6FFEEE8
          E2FFFFFFFDFFFFF8F1FFFFF8F1FFFFF7EEFFFFFBF3FFF1DBC7FFFFF8EEFFFFF3
          E9FFFFF4E7FFFFF1E4FFFFFFFBFFF2E4D5FFAE8B80FF0000000000000000E3E2
          DFFFF6F1EBFFFFFFFFFFFFFCF7FFFFFCF7FF8E5031FFC5612FFFA9633FFFFFFB
          F3FFFFF8F1FFFFFFFFFFF6ECE4FFEADBCDFF0000000000000000000000000000
          0000E7E4E1FFF2F2F1FFFFFFFDFFFFFFFFFFBCA799FF530F00FFC9B6A6FFFFFF
          FFFFFEFDFAFFF6F1EBFFE2D5CCFF000000000000000000000000000000000000
          000000000000F6F1EBFFEDEBE9FFF2F2F1FFFCFCFBFFFFFFFFFFFCFCFBFFF5F4
          F2FFF0EBE6FFF7F0E8FF00000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000BAFF0000
          BAFF0000BAFF0000BAFF0000BAFF0000BAFF0000000000000000000000000000
          BAFF0000BAFF0000BAFF0000BAFF0000BAFF0000BAFF000000000000BAFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000BAFF0000000000000000000000000000
          BAFF0000FFFF0000FFFF0000FFFF0000FFFF0000BAFF000000000000BAFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000BAFF0000000000000000000000000000
          BAFF0000FFFF0000FFFF0000FFFF0000FFFF0000BAFF000000000000BAFF0000
          BAFF0000BAFF0000BAFF0000BAFF0000BAFF0000000000000000000000000000
          BAFF0000BAFF0000BAFF0000BAFF0000BAFF0000BAFF00000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000FF000000FF000000FF000000FF000000FF00000000000000FF0000
          00FF000000FF000000FF000000FF000000000000000000000000000000000000
          000000000000000000000000000000000000000000FF000000FF000000FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000BAFF0000BAFF0000BAFF0000BAFF0000BAFF0000
          BAFF0000BAFF0000000000000000000000000000000000000000000000000000
          000000000000000000000000BAFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          FFFF0000BAFF0000000000000000000000000000000000000000000000000000
          000000000000000000000000BAFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          FFFF0000BAFF0000000000000000000000000000000000000000000000000000
          000000000000000000000000BAFF0000BAFF0000BAFF0000BAFF0000BAFF0000
          BAFF0000BAFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000007C54FF007C
          54FF007C54FF007C54FF007C54FF007C54FF000000000000000000000000007C
          54FF007C54FF007C54FF007C54FF007C54FF007C54FF00000000007C54FF00FF
          00FF00FF00FF00FF00FF00FF00FF007C54FF000000000000000000000000007C
          54FF00FF00FF00FF00FF00FF00FF00FF00FF007C54FF00000000007C54FF00FF
          00FF00FF00FF00FF00FF00FF00FF007C54FF000000000000000000000000007C
          54FF00FF00FF00FF00FF00FF00FF00FF00FF007C54FF00000000007C54FF007C
          54FF007C54FF007C54FF007C54FF007C54FF000000000000000000000000007C
          54FF007C54FF007C54FF007C54FF007C54FF007C54FF00000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000FF000000000000000000000000000000000000
          0000000000FF000000FF000000FF000000FF000000FF00000000000000FF0000
          00FF000000FF000000FF000000FF000000000000000000000000000000000000
          000000000000000000000000000000000000000000FF000000FF000000FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000007C54FF007C54FF007C54FF007C54FF007C54FF007C
          54FF007C54FF0000000000000000000000000000000000000000000000000000
          00000000000000000000007C54FF00FF00FF00FF00FF00FF00FF00FF00FF00FF
          00FF007C54FF0000000000000000000000000000000000000000000000000000
          00000000000000000000007C54FF00FF00FF00FF00FF00FF00FF00FF00FF00FF
          00FF007C54FF0000000000000000000000000000000000000000000000000000
          00000000000000000000007C54FF007C54FF007C54FF007C54FF007C54FF007C
          54FF007C54FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000868686FF868686FF868686FF868686FF8686
          86FF868686FF868686FF00000000000000000000000000000000000000000000
          00000000000000000000949494FF606060FF454545FF434343FF554F4FFF817B
          7BFF4E4E4EFF555555FF737373FF868686FF0000000000000000000000000000
          000000000000646464FFA6A6A6FFF1F1F1FFB2B2B2FF9E8B8BFFA68686FFB6B3
          B3FFE5E5E5FFB4B4B4FF575757FF505050FF7A7A7AFF00000000000000000000
          00009B9B9BFFEEEEEEFFF1F1F1FFE8E8E8FFACACACFF939393FF5F5F5FFF6D6D
          6DFF8C8C8CFFBCBCBCFFE9E9E9FFB1B1B1FF717171FF0000000000000000B5B5
          B5FFFAFAFAFFF2F2F2FFD8D8D8FF959595FF7E7E7EFF979797FFB2B2B2FFA6A6
          A6FF949494FF8E8E8EFF898989FF646464FF7A7A7AFF0000000000000000B5B5
          B5FFD8D8D8FFA0A0A0FF9B9B9BFFC9C9C9FFABABABFF8D8D8DFF808080FF8383
          83FF9F9F9FFFBFBFBFFF7AAA8AFF585858FF7C7C7CFF00000000000000007676
          76FFA6A6A6FFD9D9D9FFD8D8D8FFD8D8D8FFD8D8D8FFE0E0E0FFCFCFCFFFBEBE
          BEFF9D9D9DFF898989FF678471FF575757FF828282FF00000000000000009E9E
          9EFFE1E1E1FFD8D8D8FFD2D2D2FFC9C9C9FFD5D5D5FFCECECEFFC2C2C2FFC0C0
          C0FFCCCCCCFFD4D4D4FFCCCCCCFF717171FF868686FF0000000000000000B5B5
          B5FFD5D5D5FFC2C2C2FFAAAAAAFFA5A5A5FFCFCFCFFFF0F0F0FFEDEDEDFFEAEA
          EAFFD9D9D9FFBFBFBFFFBEBEBEFF888888FF0000000000000000000000000000
          0000B5B5B5FFC9C9C9FFECECECFFB2B2B2FFBABABAFFAEAEAEFFB6B6B6FFBDBD
          BDFFBDBDBDFFB3B3B3FF868686FF000000000000000000000000000000000000
          000000000000C4BBBBFFFFD4C5FFFFD2C7FFFBD7CEFFEFDCD5FFE5E1DFFFE7E7
          E7FF858585FF868686FF00000000000000000000000000000000000000000000
          000000000000D5BFB9FFFFDCB8FFFFDCB8FFFFDCB8FFFFD6B3FFFFD4AFFF7A63
          61FF868686FF0000000000000000000000000000000000000000000000000000
          000000000000CBABA1FFFFE5CBFFFFE5CBFFFFE5CBFFFFE5CBFFF9DDC4FF7367
          67FF000000000000000000000000000000000000000000000000000000000000
          000000000000DFC3B7FFFFEDDCFFFFEDDCFFFFEDDCFFFFEDDCFFDBC1B5FF9292
          92FF000000000000000000000000000000000000000000000000000000000000
          0000CEBAB9FFFFF7EEFFFFF7EEFFFFF7EEFFFFF7EEFFFFF7EEFFB0A19DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000E6D8D7FFE6D8D7FFE6D8D7FFE6D8D7FFE6D8D7FFE6D8D7FFE6D8D7FF0000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF0000FF000000FFFF00
          00FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF0000FFFF0000FF000000FFFFFFFFFF0000
          00FFFF0000FF0000000000000000000000000000000000000000000000000000
          00000000000000000000FF0000FF00000000000000FFFFFFFFFF000000FFFFFF
          FFFF000000FFFF0000FF00000000000000000000000000000000000000000000
          000000000000FF0000FF000000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00FFFFFFFFFF000000FFFF0000FF00000000000000000000000000000000FF00
          00FFFF0000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF000000FFFFFFFFFF000000FFFF0000FF0000000000000000FF0000FF0000
          0000000000FFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF000000FFFFFFFFFF000000FFFF0000FF00000000000000FF0000
          00FFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFF000000FFFF0000FF000000FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFF000000FF000000000000
          00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF00000000000000000000
          0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF
          FFFFFFFFFFFFFFFFFFFF000000FF000000FF0000000000000000000000000000
          000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00FF000000FF000000FF00000000000000000000000000000000000000000000
          00000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF000000FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000FF000000FF000000FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
end
