//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dirscan.h"
#include "ExtUtils.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TDirScan::TDirScan()
{
  FCanSubdir = false;
  FDir = "";
  FFileMask = "*.*";
}
//---------------------------------------------------------------------------
void __fastcall TDirScan::FSetDir(AnsiString ADir)
{
  FDir = icsPrePath(ADir).Trim();
}
//---------------------------------------------------------------------------
void __fastcall TDirScan::Execute()
{
  if (!FOnIterateItem)
   throw EDirScanError("������. ����������� ���������� ������� OnIterateItem.");
  if (FDir.IsEmpty())
   throw EDirScanError("������. �� ������� ����� ��� ������������.");
  if (!DirectoryExists(FDir))
   throw EDirScanError("������. ��������� ����� ��� ������������ ����������� ��� �� ��������.");
  try
   {
     FDirScan(FDir);
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TDirScan::FDirScan(AnsiString AFN)
{
  TSearchRec file_info;
  try
   {
     if (!FindFirst(icsPrePath(AFN+"\\"+FFileMask), faAnyFile, file_info))
      {
        do
         {
           if (FOnIterateItem && (file_info.Name.Trim() != "." )&&(file_info.Name.Trim() != ".." ))
            FOnIterateItem(icsPrePath(AFN+"\\"+file_info.Name), file_info);
           if ((file_info.Attr&faDirectory) && FCanSubdir)
            {
              if ((file_info.Name.Trim() != "." )&&(file_info.Name.Trim() != ".." ))
               FDirScan(icsPrePath(AFN+"\\"+file_info.Name));
            }
         }
        while (!FindNext(file_info));
      }
   }
  __finally
   {
     FindClose ( file_info );
   }
}
//---------------------------------------------------------------------------

