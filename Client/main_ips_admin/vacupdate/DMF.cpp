//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#include <winsock.h>
#pragma hdrstop

#include "DMF.h"
#include "DocExDFM.h"
#include "ExtSpecList.h"
#include "ExtUtils.h"
#include "SQLCreator.h"
#include "msgdef.h"
//#include "ExtProc.h"
//#include "Choice.h"
#include "RegEDContainer.h"

#ifndef IMM_DOCUMENT
#endif
#ifdef IMM_CARD_EDIT
 #include "OtvodEdit.h"
 #include "PrivEdit.h"
 #include "ProbEdit.h"
#endif

#include "DATEUTIL.hpp"
//#include "RegEI.h"
//#include "ImmEI.h"

#include "ExtSpecList.h"
#include "ExtFilterList.h"

#include "FilterEditEx.h"

//---------------------------------------------------------------------------
// ��� "���������", �� �������� �������� ������ � ������� "����" (CARD_1084)
// enum TPlanItemSource
// {
//   pisUnknown        = -1,
//   pisOffPlan        = 0,        // ����������
//   pisManual         = 1,        // ������ ����������
//   pisPlanerClndr    = 2,        // ����������� �� ���������
//   pisPlanerOffClndr = 3,        // ����������� �� �� ���������
//   pisPlanerPrev     = 4         // ����������� ��� ���������������
// };
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "FIBDatabase"
#pragma link "FIBDataSet"
#pragma link "FIBQuery"
#pragma link "pFIBDatabase"
#pragma link "pFIBDataSet"
#pragma link "pFIBErrorHandler"
#pragma link "pFIBQuery"
#pragma link "Registry_ICScomp"
#pragma link "ICSImm"
#pragma link "ICSDocViewer"
#pragma link "AppOptDialog"
#pragma link "AppOptFIBDBProv"
#pragma link "AppOptions"
#pragma link "KLADR"
#pragma resource "*.dfm"
TDM *DM;

//---------------------------------------------------------------------------
__fastcall TDM::TDM(TComponent* Owner)
    : TDataModule(Owner)
{
//  FProgressForm = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TDM::DMCreate(TObject *Sender)
{
  FReqColor = clInfoBk;
  XMLList = new TAxeXMLContainer;
  xmlImmCardDef = new TTagNode(NULL);
  xmlRegDef        = new TTagNode(NULL);
  xmlVacDef        = new TTagNode(NULL);
  xmlTestDef       = new TTagNode(NULL);
  sWindow = new TStringList;
  SystemMode = smNormal;
  CurrentPath = GetCurrentDir();
  try
   {
     ISRegEvents = Variant::CreateObject("ICSRegEventsSys.ICSRegEvents");
   }
  catch(...)
   {
   }
#ifdef _IMM_DEBUG
  RegComp->FullEdit = true;
#endif
}
//---------------------------------------------------------------------------
void __fastcall TDM::DMDestroy(TObject *Sender)
{
  if (sWindow)     delete sWindow;
//  if (FProgressForm) delete FProgressForm;
//  FProgressForm = NULL;
  if(!(DM->ISRegEvents.IsEmpty() || DM->ISRegEvents.IsNull()))
   {
     try
      {
        Function fDeInit("Deinit");
        DM->ISRegEvents.Exec(fDeInit);
      }
     catch(Exception &E)
      {
        _MSG_ERRA("������ ������ ������� DeInit ��� ������� ������� ������������.\n��������� ���������:\n"+E.Message,"������ �������������");
      }
   }
  RegComp->Active = false;
  delete FClassData;
  if (XMLList)     delete XMLList;

  for (TIntListMap::iterator i = InfVacList.begin(); i != InfVacList.end(); i++)
   delete i->second;
  for (TIntListMap::iterator i = InfProbList.begin(); i != InfProbList.end(); i++)
   delete i->second;
  for (TIntListMap::iterator i = VacInfList.begin(); i != VacInfList.end(); i++)
   delete i->second;
  for (TIntListMap::iterator i = ProbInfList.begin(); i != ProbInfList.end(); i++)
   delete i->second;
}
//---------------------------------------------------------------------------
void __fastcall TDM::dbErrorFIBErrorEvent(TObject *Sender,
      EFIBError *ErrorValue, TKindIBError KindIBError, bool &DoRaise)
{
  DoRaise = true;
}
//---------------------------------------------------------------------------
long __fastcall TDM::GetMCode(AnsiString AGenName)
{
   long RC = 0;
   try
    {
      quFree1->Close();
      if (!quFree1->Transaction->Active) quFree1->Transaction->StartTransaction();
      quFree1->ExecProcedure(AGenName.c_str());
      RC = quFree1->FN("MCODE")->AsInteger;
      quFree1->Transaction->Commit();
    }
   __finally
    {
      return RC;
    }
}
//---------------------------------------------------------------------------
bool __fastcall TDM::qtExec(AnsiString ASQL, bool aCommit)
{
  try
   {
     if (!trFree->Active) trFree->StartTransaction();
     quFree->Close();
     quFree->SQL->Clear();
     quFree->SQL->Text = ASQL;
     quFree->Prepare();
     quFree->ExecQuery();
     if (aCommit) trFree->Commit();
     return true;
   }
  catch (...)
   {
     return false;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TDM::qtExec1(AnsiString ASQL, bool aCommit)
{
  try
   {
     if (!trFree1->Active) trFree1->StartTransaction();
     quFree1->Close();
     quFree1->SQL->Clear();
     quFree1->SQL->Text = ASQL;
     quFree1->Prepare();
     quFree1->ExecQuery();
     if (aCommit) trFree1->Commit();
   }
  catch (...)
   {
     return false;
   }
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TDM::PrInit(int AMax, AnsiString AMsg)
{
//  if (FProgressForm)
//   FProgressForm->CPrBar->Position = 1;
}
//---------------------------------------------------------------------------
void __fastcall TDM::PrChange(int APercent, AnsiString AMsg)
{
//  if (FProgressForm)
//   FProgressForm->CPrBar->Position = APercent;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TDM::GetClassData(long ACode, AnsiString AName)
{
  AnsiString RC = "";
  try
   {
     RC = FClassData->GetData(AName, ACode);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
AnsiString __fastcall GetXMLGUI(TTagNode* ANode)
{
  return (ANode->GetRoot()->GetChildByName("passport")->AV["gui"]);
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetPassport(TTagNode* ANode)
{
  return ANode->GetRoot()->GetChildByName("passport");
}
//---------------------------------------------------------------------------
void __fastcall TDM::SetIntValIfNotNull(TFIBXSQLVAR *AField, int &Val)
{
  if (!AField->IsNull && (AField->AsInteger))
   Val = AField->AsInteger;
}
//---------------------------------------------------------------------------
#include <rpc.h>
AnsiString __fastcall TDM::NewGUID()
{
  AnsiString RC = "";
  try
   {
     UUID FGUI;
     if (UuidCreate(&FGUI) == RPC_S_OK)
      {
        unsigned char * strGUID[1];
        if (UuidToString(&FGUI,strGUID) == RPC_S_OK)
         RC = AnsiString((char*)(strGUID[0]));
      }
   }
  __finally
   {
   }
  if (RC.Trim().Length())
   return RC.Trim().UpperCase();
  else
   throw Exception("������ ������������ GUID");
}
//---------------------------------------------------------------------------
AnsiString __fastcall TDM::SetValIfNotNull(AnsiString AVal, AnsiString AId,bool AAnd, bool AQuote)
{
  AnsiString RC = "";
  try
   {
     if ((!AQuote&&(AVal.ToIntDef(0) > 0))||(AQuote && AVal.Trim().Length()))
      {
        if (AAnd) RC = " and";
        if (AQuote)
         RC += " "+DM->RegComp->UpperFunc+"("+AId+")='"+AVal.Trim().UpperCase()+"'";
        else
         RC += " "+AId+"="+AVal.Trim();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------


