//---------------------------------------------------------------------------

#ifndef icsICConstDefH
#define icsICConstDefH
//---------------------------------------------------------------------------
#include "icsICConst.hpp"
#include "cxClasses.hpp"
#define FMT(f) cxGetResourceString(f)
#define FMT1(f,p1) Format(cxGetResourceString(f),OPENARRAY( TVarRec, (p1) ) )
#define FMT2(f,p1,p2) Format(cxGetResourceString(f),OPENARRAY( TVarRec, (p1,p2) ) )
#define FMT3(f,p1,p2,p3) Format(cxGetResourceString(f),OPENARRAY( TVarRec, (p1,p2,p3) ) )
#define FMT4(f,p1,p2,p3,p4) Format(cxGetResourceString(f),OPENARRAY( TVarRec, (p1,p2,p3,p4) ) )
#define FMT5(f,p1,p2,p3,p4,p5) Format(cxGetResourceString(f),OPENARRAY( TVarRec, (p1,p2,p3,p4,p5) ) )
#define FMT6(f,p1,p2,p3,p4,p5,p6) Format(cxGetResourceString(f),OPENARRAY( TVarRec, (p1,p2,p3,p4,p5,p6) ) )

#define cFMT(f) FMT(f).c_str()
#define cFMT1(f,p1) FMT1(f,p1).c_str()
#define cFMT2(f,p1,p2) FMT2(f,p1,p2).c_str()
#define cFMT3(f,p1,p2,p3) FMT3(f,p1,p2,p3).c_str()
#define cFMT4(f,p1,p2,p3,p4) FMT4(f,p1,p2,p3,p4).c_str()
#define cFMT5(f,p1,p2,p3,p4,p5) FMT5(f,p1,p2,p3,p4,p5).c_str()
#define cFMT6(f,p1,p2,p3,p4,p5,p6) FMT6(f,p1,p2,p3,p4,p5,p6).c_str()
#endif
