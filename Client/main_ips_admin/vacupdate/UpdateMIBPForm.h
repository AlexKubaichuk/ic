//----------------------------------------------------------------------------
#ifndef UpdateMIBPFormH
#define UpdateMIBPFormH
//----------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxCheckListBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxTextEdit.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "cxCustomData.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
//----------------------------------------------------------------------------
#include "DMUnit.h"
#include "InsertClassItem.h"
//----------------------------------------------------------------------------
class TUpdateMIBPForm : public TForm
{
__published:
        TcxPageControl *PC;
        TcxTabSheet *AddTS;
        TcxTabSheet *SetupTS;
        TActionList *MainAL;
        TAction *actGetClasses;
        TAction *actSetDBPath;
 TAction *actSetGUID;
        TAction *actAdd;
        TcxImageList *MainIL;
        TAction *actCreateSetup;
        TcxTabSheet *cxTabSheet1;
        TcxCheckListBox *VacSchList;
        TAction *actSaveDefSch;
 TPanel *Panel1;
 TcxButton *cxButton4;
 TPanel *Panel2;
 TPanel *Panel3;
 TPanel *Panel5;
 TPanel *Panel6;
 TLabel *Label7;
 TcxCheckListBox *ReakTypeChLB;
 TLabel *Label5;
 TcxCheckListBox *InfChLB;
 TPanel *Panel03;
 TPanel *Panel04;
 TLabel *NameEDLabel;
 TcxTextEdit *NameED;
 TPanel *Panel05;
 TLabel *ShortNameEDLabel;
 TcxTextEdit *ShortNameED;
 TPanel *Panel06;
 TLabel *ManufCBLabel;
 TcxComboBox *ManufCB;
 TPanel *Panel07;
 TLabel *CountryCBLabel;
 TcxComboBox *CountryCB;
 TPanel *Panel10;
 TPanel *Panel09;
 TLabel *DefDozeLabel;
 TcxMaskEdit *DefDoze;
 TLabel *VacNameEDLabel;
 TcxTextEdit *VacNameED;
 TPanel *Panel14;
 TPanel *Panel11;
 TLabel *DoseEDLabel;
 TcxMaskEdit *DoseED;
 TLabel *VacCBLabel;
 TcxComboBox *VacCB;
 TPanel *Panel12;
 TPanel *Panel15;
 TLabel *ResistReakValEDLabel;
 TcxMaskEdit *ResistReakValED;
 TPanel *Panel13;
 TPanel *Panel08;
 TLabel *InsTypeCBLabel;
 TcxComboBox *InsTypeCB;
 TLabel *FullNameEDLabel;
 TcxTextEdit *FullNameED;
 TPanel *Panel01;
 TcxComboBox *AddTypeCB;
 TPanel *Panel7;
 TcxButton *cxButton6;
 TPanel *Panel22;
 TcxButton *cxButton5;
 TcxProgressBar *PrBar;
 TPanel *Panel24;
 TLabel *Label14;
 TcxTextEdit *UpdateNameED;
 TPanel *Panel27;
 TLabel *Label18;
 TMemo *ExtScript;
 TPanel *Panel02;
 TLabel *Label1;
 TcxTextEdit *S1078ED;
 TcxCheckBox *BinaryReakChB;
 TcxCheckBox *CanApplyVacChB;
    void __fastcall FormShow(TObject *Sender);
    bool __fastcall CheckConnect();
        void __fastcall BinaryReakChBPropertiesChange(TObject *Sender);
        void __fastcall AddTypeCBPropertiesChange(TObject *Sender);
        void __fastcall actAddExecute(TObject *Sender);
        void __fastcall actCreateSetupExecute(TObject *Sender);
        void __fastcall actSaveDefSchExecute(TObject *Sender);
 void __fastcall GridCustomDrawDataCell(TcxCustomTreeList *Sender, TcxCanvas *ACanvas,
          TcxTreeListEditCellViewInfo *AViewInfo, bool &ADone);
private:
    TAnsiStrMap FFLList;
    TAnsiStrMap FFLTypes;
    int FCheckVacCode, FCheckTestCode;
  void __fastcall CreateSetupExecute(UnicodeString AVer, UnicodeString AScript, bool AVac, bool AVacSch, bool ATest, bool ATestSch );

  Variant __fastcall GetCBData(TcxComboBox *AData);
  TInsertClassItem * __fastcall NewInsertClassItem (UnicodeString AId);

    void __fastcall FillChLB();
    void __fastcall AddVac();
    void __fastcall AddTest();
    bool __fastcall CheckInput();
    void __fastcall ClearFields();
    void __fastcall FUpdateGen(const UnicodeString &AUID);
    int  __fastcall CheckedItems(TcxCheckListBox *ASrc);
    TJSONObject * __fastcall GetClassData(UnicodeString ASQL, UnicodeString ARecId);

    bool __fastcall CheckSetupInput();
    UnicodeString __fastcall DisableTriggers(UnicodeString AClsID);
    UnicodeString __fastcall EnableTriggers(UnicodeString AClsID);
    UnicodeString __fastcall AddDeleteScript(UnicodeString AClsID);
    void          __fastcall ExecCommand(UnicodeString ACmd);
public:
    virtual __fastcall TUpdateMIBPForm(TComponent *Owner, short AIdx);
};
//----------------------------------------------------------------------------
extern TUpdateMIBPForm *UpdateMIBPForm;
//----------------------------------------------------------------------------
#endif

