unit icsICConst;
//#include "icsICConstDef.h"

interface

resourcestring
// Common
   icsICCommonErrorCaption = '������';
   icsICCommonErrorLoadCaption = '������ �������� ���������';
   icsICCommonWarningCaption = '��������������';
   icsICCommonMsgCaption = '���������';
   icsICCommonErrorAddRecMsg = '������ ���������� ������';

//DocTemplate

   icsICDocTemplateCaption = '������������ �������';
   icsICDocTemplateCancelBtnCaption = '������';
   icsICDocTemplateOkBtnCaption = '���������';

//Connect

   icsICConnectErrorReadProjectDef = '������ ������ �������� �������';
   icsICConnectErrorReadProjectDef2 = '������ ����������� ���������� ����������� � ��';
   icsICConnectErrorProjectDefMissing = '����������� ���� �������� �������';
   icsICConnectKeyMissing = '��������� ������� ����� ������ !!!';
   icsICConnectKeyOldVal = '         ���� ������ ������ !!!'#13#10'  ��� ���������� ������ ����� ������'#13#10'   ��������� � ������� �������������'#13#10'��� "���������������� ����������� �������".'#13#10'���������� ���������� �� ������ �������� '#13#10'   � ������� "�������"|"� ���������".';
   icsICConnectProjectCaption = '�������� �������';
   icsICConnectRegistryCaption = '��������_������������';
   icsICConnectImmCardCaption = '��������_���._�����';
   icsICConnectVacSchCaption = '��������_����_����������_������';
   icsICConnectTestSchCaption = '��������_����_����������_����';
   icsICConnectNomenklatorCaption = '��������_�������_�������';
   icsICConnectCheckDocFuncCaption = '�������� ������� (���������)...';
   icsICConnectErrorLoadNomDLL = '������ �������� ������ �������� �������������� ���������(���������)';
   icsICConnectErrorInitNomDLL = '������ ������������� ������ �������� �������������� ���������(���������)';
   icsICConnectZaklCaption = '����������';
   icsICConnectEKGCaption = '���';
   icsICConnectErrorConnectCaption = '������ �����������.';
   icsICConnectErrorReadSetParams = '������ ������ ���������� ���������';
   icsICConnectMissingSettingSection = '����������� ������ �������� � ����� �������� �������';
   icsICConnectMissingEKGSection = '����������� ������ ��� � ����� �������� �������';
   icsICConnectMissingEKGDataDef = '����������� �������� ����� ������ ��� ���';
   icsICConnectErrorReadImportConf = '������ ������ ������������ �������';
   icsICConnectTablesCaption = '������� ...';
   icsICConnectRoutinesCaption = '������ ...';
   icsICConnectRulesCaption = '������� ...';
   icsICConnectBrokenFuncRef = '������ �� ������������� �������;'#13#10'   ������="%s";'#13#10'   UID="%s"';
   icsICConnectBrokenDefRef = '������ �� ������������� �������� "%s"';
   icsICConnectMissingRef1 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s";'#13#10'   �� ������� "%s"';
   icsICConnectMissingRef2 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s";'#13#10'   ������� "������ �������� ������"';
   icsICConnectMissingRef3 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s";'#13#10'   �������� ������� -> UID="%s"';
   icsICConnectMissingRef4 = '������ �� ������������� ������� "%s" � �������� ��������;'#13#10'   ������="%s"';
   icsICConnectMissingRef5 = '������ �� ������������� ������ � �������� ��������;'#13#10'   ������="%s"';
   icsICConnectMissingRef6 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s"';
   icsICConnectErrorProjectElemType = '�� ���������� ��� �������� ������� "%s"';
   icsICConnectMissingLPUParam = '� ���������� ��������� �� ������ ��������� ���';
   icsICConnectCreateInqListCaption = '������������ ������ ������������ ...';
   icsICConnectCreateRuleListCaption = '������������ ������ �������� ������ ...';
   icsICConnectCreateSQLQuery = '���������� �������� ��� �������� ������ ...';
   icsICConnectFuncCheckCaption = '�������� ������� ...';
   icsICConnectErrorInitAutoFunc = '������ ������������� ������ �������� �������������� ���������';
   icsICConnectErrorLoadAutoFunc = '������ �������� ������ �������� �������������� ���������';

   icsICConnectLabVerCaption = '������';

//ProfSet

   icsICProfSetErrorSetMsg = '����������� ������������� ���� �������� �� ������ !';

   icsICProfSetCaption = '������������� �������� �� ������';
   icsICProfSetLabel1Caption = '�����';
   icsICProfSetLabel3Caption = '�� ������������� �������';
   icsICProfSetOkBtnCaption = '���������';
   icsICProfSetCancelBtnCaption = '������';
   icsICProfSetLabel4Caption = '�����';
   icsICProfSetLabel2Caption = '������������� �������';

//DMF

   icsICDMFErrorLoadEKGDLL = '������ �������� ������ �������� ��������� ���';
   icsICDMFErrorInEKGDLL = '������ � ������ ����� ���';
   icsICDMFInqDataCountCaption = '������������: %s%s';
   icsICDMFInqDataLastCaption = ', ���������: %s';
   icsICDMFMissingXMLRef = '������ �� ������������� XML GUI="%s"';
   icsICDMFInqDataCaption = '������ ������������';
   icsICDMFMissingDefDestData = '����������� �������� �������������� ������';
   icsICDMFMissingDefScale = '����������� �������� �����';
   icsICDMFMissingOutVal = '����������� �������� ��� ��������� �������';
   icsICDMFNewInqCaption = '����� ������������';
   icsICDMFEditInqCaption = '������������� ��������� ������������';
   icsICDMFEKGViewCaption = '�������� ���';
   icsICDMFVNCaption = '������ ������������� ���������';
   icsICDMFErrorInqExists = '������������ �� "%s �." ��� ����������.'#13#10'� ���� ���� ����� ���� ������ ���� ������������.';
   icsICDMFErrorCreateInputForm = '������ ������������ ����� ����� ������������';

//Main

   icsICMainErrorCreateReportNotFull = '������������ ���������� ����������, ������������ ��������� �� � ������ ������.';
   icsICMainFailPgmLoadMsg = '���������� � �������������� ���������'#13#10'��� ���������� ���������';
   icsICMainErrorKeyCaption = '  (������� "��� ��������")';
   icsICMainErrorCreateInputForm = '������ ������������ ����� ����� ������������';
   icsICMainPatListCaption = '������ ���������';
   icsICMainCardCaption = '����� ������������ <%s>';
   icsICMainErrorCreateRoportNoDef = '������ ������������ ������ (����������� �������� ������)';
   icsICMainChoiceCreateCaption = '������������ ������� ��� "%s"';
   icsICMainCreateReportCaption = '������������ "%s"';
   icsICMainCreatePatListCaption = '������������ ������ ���������';
   icsICMainCreateReportCountConf = '���������� ���������� ����� 40,'#13#10' ������������ ����� ������ ���������� �����.'#13#10'���������� ?';
   icsICMainCreateReportCountCaption = '������������ ����������  (�����: %s)';
   icsICMainCalcReportCountCaption = '������ ����������  ( %s �� %s)';
   icsICMainCalcTimeCaption = '�������� %s';
   icsICMainCalcTimeCaptionFormat = 'n ���. s ���.';
   icsICMainCalcTimeCaptionFormatEx = 'h ���. n ���. s ���.';
   icsICMainIncReportCountCaption = '������������ ���������� ( %s �� %s)';
   icsICMainCommReportCaption = '����������';
   icsICMain13thMsg = ''#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10'                                        ��, �����..., �� ����....                                         '#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10'';
   icsICMain13thCaption = '������ ��������� ��������';

   icsICMainChoiceLibCaption = '���������� ��������';

   icsICMainactShowUListCaption = '&������ ���������';
   icsICMainactShowUListHint = '������� ������ ���������';
   icsICMainactSpecListOpenCaption = '������ � �����������';
   icsICMainactSpecListOpenHint = '������� ������ �������� ����������';
   icsICMainactDocBankOpenCaption = '���� ����������';
   icsICMainactDocBankOpenHint = '���� �������������� ����������';
   icsICMainactDocChoiceCaption = '���������� ��������';
   icsICMainactDocChoiceHint = '���������� ��������';
   icsICMainactSubordOrgCaption = '����������������� �����������';
   icsICMainactSubordOrgHint = '����������������� ����������� ...';
   icsICMainactValExecCaption = '�������+';
   icsICMainactValExecHint = '�������+';
   icsICMainactSettingCaption = '&���������';
   icsICMainactDocExportCaption = '���������, �������� ����������';
   icsICMainactDocExportHint = '�������� ����������, �������� ����������';
   icsICMainactDocImportCaption = '���������, �������� ����������';
   icsICMainactDocImportHint = '���� ����������, �������� ����������';
   icsICMainactCardExportCaption = '����� ������������';
   icsICMainactCardExportHint = '�������� ���� ������������';
   icsICMainactCardImportCaption = '����� ������������';
   icsICMainactCardImportHint = '���� ���� ������������';
   icsICMainactShowClassesCaption = '�����������';
   icsICMainactShowClassesHint = '�����������';
   icsICMainactGrOrgPatChangeCaption = '������ ���������� �������������� ���������� ���������';
   icsICMainactGrOrgPatChangeHint = '������ ���������� �������������� ���������� ���������';
   icsICMainactHelpCaption = '����� &�������';
   icsICMainactHelpHint = '����� �������';
   icsICMainactAboutCaption = '� &���������';
   icsICMainactAboutHint = '� ���������';
   icsICMainICSAboutDlgTitle = '����';

   icsICMainMainTBMBar1Caption = '����';
   icsICMainMainTBMBar2Caption = '������������';
   icsICMainMainTBMBar3Caption = '���������';
   icsICMainMainTBMBar4Caption = '�������������� ���������';

   icsICMainMainTBMBarCategories1 = '������������';
   icsICMainMainTBMBarCategories2 = '���������';
   icsICMainMainTBMBarCategories3 = '����/��������';
   icsICMainMainTBMBarCategories4 = '������';
   icsICMainMainTBMBarCategories5 = '������';
   icsICMainbiRegCaption = '&������������';
   icsICMainWindowBICaption = '&����';
   icsICMainHelpBICaption = '&?';
   icsICMainMorrBICaption = '�������������� �� ������������';
   icsICMainMorrBIHint = '�������������� �� ������������';
   icsICMainbiServiceCaption = '������';
   icsICMainbiGrChangeCaption = '������ � �������� ���������';
   icsICMainbiDocWorkCaption = '&���������';
   icsICMainPersonalGroupReportBICaption = '����������';
   icsICMainReportGroupBICaption = '������, ������';
   icsICMainbiEICaption = '����/�������� ������';
   icsICMainbiExportCaption = '��������';
   icsICMainbiImportCaption = '�������';
   icsICMainbiExitCaption = '�&����';
   icsICMainbiExitHint = '�����';
   icsICMainErrorOpenCard1 = '������ �������� ����� ������������, �� �������� ��� ��������.';
   icsICMainErrorOpenCard2 = '������ �������� ����� ������������, � ������� �������� "������ ������������", �� �������� �� ������ ������������.';

//ModuleType

   icsICModuleTypeErrorNoModules = '����������� ������������������ �������� ������������';
   icsICModuleTypeErrorRegistryKey = '��������� �������� ����� �������';

   icsICModuleTypeCaption = '����� �������� ������������';
   icsICModuleTypeOkBtnCaption = '���������';
   icsICModuleTypeCancelBtnCaption = '������';
   icsICModuleTypecxLabel1Caption = '������������������ ������� ������������ ������������ "����"';

//ICSDBConnect

   icsICICSDBConnectPropertyMissingDatabase = '�� ������ �������� "Database"';
   icsICICSDBConnectPropertyMissingQuery = '�� ������ �������� "Query"';
   icsICICSDBConnectPropertyMissingXMLList = '�� ������ �������� "XMLList"';
   icsICICSDBConnectPropertyMissingDBName = '�� ������ �������� "DBName"';
   icsICICSDBConnectPropertyMissingVersion = '�� ������ �������� "Version"';
   icsICICSDBConnectErrorCommCaption = '������ ��� ����������� � ��, ����������� �� ���������.';
   icsICICSDBConnectErrorConfDefRead = '������ ������ �������� ������������.';
   icsICICSDBConnectErrorDBVersion = '������ �� �� ������������� ���������.'#13#10'������ "%s", ��������� (%s)';
   icsICICSDBConnectMissingVersionsDef = '����������� �������� ������';
   icsICICSDBConnectMissingVersionDef = '����������� �������� ������';
   icsICICSDBConnectLoadDefsCaption = '�������� ��������';
   icsICICSDBConnectErrorLoadDef = '������ �������� �������� "%s"';
   icsICICSDBConnectErrorConfRead = '������ ������ ������������';
   icsICICSDBConnectCheckReqDefCaption = '�������� ������� ������������ ��������';
   icsICICSDBConnectMissingConfDef = '����������� �������� ������������';
   icsICICSDBConnectErrorConnect = '%s'#13#10'���: %s'#13#10'�������������� ���: %s';
   icsICICSDBConnectSysError = '%s'#13#10'��������� ��������� "%s"';
   icsICICSDBConnectMissingHandlerOnGetDBName = '����������� ���������� ������� "OnGetDBName"';

//

//   icsIC = '';

implementation

end.
