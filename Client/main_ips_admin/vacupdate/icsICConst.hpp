// Borland C++ Builder
// Copyright (c) 1995, 2002 by Borland Software Corporation
// All rights reserved

// (DO NOT EDIT: machine generated header) 'icsICConst.pas' rev: 6.00

#ifndef icsICConstHPP
#define icsICConstHPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Icsicconst
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern PACKAGE System::ResourceString _icsICCommonErrorCaption;
#define Icsicconst_icsICCommonErrorCaption System::LoadResourceString(&Icsicconst::_icsICCommonErrorCaption)
extern PACKAGE System::ResourceString _icsICCommonErrorLoadCaption;
#define Icsicconst_icsICCommonErrorLoadCaption System::LoadResourceString(&Icsicconst::_icsICCommonErrorLoadCaption)
extern PACKAGE System::ResourceString _icsICCommonWarningCaption;
#define Icsicconst_icsICCommonWarningCaption System::LoadResourceString(&Icsicconst::_icsICCommonWarningCaption)
extern PACKAGE System::ResourceString _icsICCommonMsgCaption;
#define Icsicconst_icsICCommonMsgCaption System::LoadResourceString(&Icsicconst::_icsICCommonMsgCaption)
extern PACKAGE System::ResourceString _icsICCommonErrorAddRecMsg;
#define Icsicconst_icsICCommonErrorAddRecMsg System::LoadResourceString(&Icsicconst::_icsICCommonErrorAddRecMsg)
extern PACKAGE System::ResourceString _icsICDocTemplateCaption;
#define Icsicconst_icsICDocTemplateCaption System::LoadResourceString(&Icsicconst::_icsICDocTemplateCaption)
extern PACKAGE System::ResourceString _icsICDocTemplateCancelBtnCaption;
#define Icsicconst_icsICDocTemplateCancelBtnCaption System::LoadResourceString(&Icsicconst::_icsICDocTemplateCancelBtnCaption)
extern PACKAGE System::ResourceString _icsICDocTemplateOkBtnCaption;
#define Icsicconst_icsICDocTemplateOkBtnCaption System::LoadResourceString(&Icsicconst::_icsICDocTemplateOkBtnCaption)
extern PACKAGE System::ResourceString _icsICConnectErrorReadProjectDef;
#define Icsicconst_icsICConnectErrorReadProjectDef System::LoadResourceString(&Icsicconst::_icsICConnectErrorReadProjectDef)
extern PACKAGE System::ResourceString _icsICConnectErrorReadProjectDef2;
#define Icsicconst_icsICConnectErrorReadProjectDef2 System::LoadResourceString(&Icsicconst::_icsICConnectErrorReadProjectDef2)
extern PACKAGE System::ResourceString _icsICConnectErrorProjectDefMissing;
#define Icsicconst_icsICConnectErrorProjectDefMissing System::LoadResourceString(&Icsicconst::_icsICConnectErrorProjectDefMissing)
extern PACKAGE System::ResourceString _icsICConnectKeyMissing;
#define Icsicconst_icsICConnectKeyMissing System::LoadResourceString(&Icsicconst::_icsICConnectKeyMissing)
extern PACKAGE System::ResourceString _icsICConnectKeyOldVal;
#define Icsicconst_icsICConnectKeyOldVal System::LoadResourceString(&Icsicconst::_icsICConnectKeyOldVal)
extern PACKAGE System::ResourceString _icsICConnectProjectCaption;
#define Icsicconst_icsICConnectProjectCaption System::LoadResourceString(&Icsicconst::_icsICConnectProjectCaption)
extern PACKAGE System::ResourceString _icsICConnectRegistryCaption;
#define Icsicconst_icsICConnectRegistryCaption System::LoadResourceString(&Icsicconst::_icsICConnectRegistryCaption)
extern PACKAGE System::ResourceString _icsICConnectImmCardCaption;
#define Icsicconst_icsICConnectImmCardCaption System::LoadResourceString(&Icsicconst::_icsICConnectImmCardCaption)
extern PACKAGE System::ResourceString _icsICConnectVacSchCaption;
#define Icsicconst_icsICConnectVacSchCaption System::LoadResourceString(&Icsicconst::_icsICConnectVacSchCaption)
extern PACKAGE System::ResourceString _icsICConnectTestSchCaption;
#define Icsicconst_icsICConnectTestSchCaption System::LoadResourceString(&Icsicconst::_icsICConnectTestSchCaption)
extern PACKAGE System::ResourceString _icsICConnectNomenklatorCaption;
#define Icsicconst_icsICConnectNomenklatorCaption System::LoadResourceString(&Icsicconst::_icsICConnectNomenklatorCaption)
extern PACKAGE System::ResourceString _icsICConnectCheckDocFuncCaption;
#define Icsicconst_icsICConnectCheckDocFuncCaption System::LoadResourceString(&Icsicconst::_icsICConnectCheckDocFuncCaption)
extern PACKAGE System::ResourceString _icsICConnectErrorLoadNomDLL;
#define Icsicconst_icsICConnectErrorLoadNomDLL System::LoadResourceString(&Icsicconst::_icsICConnectErrorLoadNomDLL)
extern PACKAGE System::ResourceString _icsICConnectErrorInitNomDLL;
#define Icsicconst_icsICConnectErrorInitNomDLL System::LoadResourceString(&Icsicconst::_icsICConnectErrorInitNomDLL)
extern PACKAGE System::ResourceString _icsICConnectZaklCaption;
#define Icsicconst_icsICConnectZaklCaption System::LoadResourceString(&Icsicconst::_icsICConnectZaklCaption)
extern PACKAGE System::ResourceString _icsICConnectEKGCaption;
#define Icsicconst_icsICConnectEKGCaption System::LoadResourceString(&Icsicconst::_icsICConnectEKGCaption)
extern PACKAGE System::ResourceString _icsICConnectErrorConnectCaption;
#define Icsicconst_icsICConnectErrorConnectCaption System::LoadResourceString(&Icsicconst::_icsICConnectErrorConnectCaption)
extern PACKAGE System::ResourceString _icsICConnectErrorReadSetParams;
#define Icsicconst_icsICConnectErrorReadSetParams System::LoadResourceString(&Icsicconst::_icsICConnectErrorReadSetParams)
extern PACKAGE System::ResourceString _icsICConnectMissingSettingSection;
#define Icsicconst_icsICConnectMissingSettingSection System::LoadResourceString(&Icsicconst::_icsICConnectMissingSettingSection)
extern PACKAGE System::ResourceString _icsICConnectMissingEKGSection;
#define Icsicconst_icsICConnectMissingEKGSection System::LoadResourceString(&Icsicconst::_icsICConnectMissingEKGSection)
extern PACKAGE System::ResourceString _icsICConnectMissingEKGDataDef;
#define Icsicconst_icsICConnectMissingEKGDataDef System::LoadResourceString(&Icsicconst::_icsICConnectMissingEKGDataDef)
extern PACKAGE System::ResourceString _icsICConnectErrorReadImportConf;
#define Icsicconst_icsICConnectErrorReadImportConf System::LoadResourceString(&Icsicconst::_icsICConnectErrorReadImportConf)
extern PACKAGE System::ResourceString _icsICConnectTablesCaption;
#define Icsicconst_icsICConnectTablesCaption System::LoadResourceString(&Icsicconst::_icsICConnectTablesCaption)
extern PACKAGE System::ResourceString _icsICConnectRoutinesCaption;
#define Icsicconst_icsICConnectRoutinesCaption System::LoadResourceString(&Icsicconst::_icsICConnectRoutinesCaption)
extern PACKAGE System::ResourceString _icsICConnectRulesCaption;
#define Icsicconst_icsICConnectRulesCaption System::LoadResourceString(&Icsicconst::_icsICConnectRulesCaption)
extern PACKAGE System::ResourceString _icsICConnectBrokenFuncRef;
#define Icsicconst_icsICConnectBrokenFuncRef System::LoadResourceString(&Icsicconst::_icsICConnectBrokenFuncRef)
extern PACKAGE System::ResourceString _icsICConnectBrokenDefRef;
#define Icsicconst_icsICConnectBrokenDefRef System::LoadResourceString(&Icsicconst::_icsICConnectBrokenDefRef)
extern PACKAGE System::ResourceString _icsICConnectMissingRef1;
#define Icsicconst_icsICConnectMissingRef1 System::LoadResourceString(&Icsicconst::_icsICConnectMissingRef1)
extern PACKAGE System::ResourceString _icsICConnectMissingRef2;
#define Icsicconst_icsICConnectMissingRef2 System::LoadResourceString(&Icsicconst::_icsICConnectMissingRef2)
extern PACKAGE System::ResourceString _icsICConnectMissingRef3;
#define Icsicconst_icsICConnectMissingRef3 System::LoadResourceString(&Icsicconst::_icsICConnectMissingRef3)
extern PACKAGE System::ResourceString _icsICConnectMissingRef4;
#define Icsicconst_icsICConnectMissingRef4 System::LoadResourceString(&Icsicconst::_icsICConnectMissingRef4)
extern PACKAGE System::ResourceString _icsICConnectMissingRef5;
#define Icsicconst_icsICConnectMissingRef5 System::LoadResourceString(&Icsicconst::_icsICConnectMissingRef5)
extern PACKAGE System::ResourceString _icsICConnectMissingRef6;
#define Icsicconst_icsICConnectMissingRef6 System::LoadResourceString(&Icsicconst::_icsICConnectMissingRef6)
extern PACKAGE System::ResourceString _icsICConnectErrorProjectElemType;
#define Icsicconst_icsICConnectErrorProjectElemType System::LoadResourceString(&Icsicconst::_icsICConnectErrorProjectElemType)
extern PACKAGE System::ResourceString _icsICConnectMissingLPUParam;
#define Icsicconst_icsICConnectMissingLPUParam System::LoadResourceString(&Icsicconst::_icsICConnectMissingLPUParam)
extern PACKAGE System::ResourceString _icsICConnectCreateInqListCaption;
#define Icsicconst_icsICConnectCreateInqListCaption System::LoadResourceString(&Icsicconst::_icsICConnectCreateInqListCaption)
extern PACKAGE System::ResourceString _icsICConnectCreateRuleListCaption;
#define Icsicconst_icsICConnectCreateRuleListCaption System::LoadResourceString(&Icsicconst::_icsICConnectCreateRuleListCaption)
extern PACKAGE System::ResourceString _icsICConnectCreateSQLQuery;
#define Icsicconst_icsICConnectCreateSQLQuery System::LoadResourceString(&Icsicconst::_icsICConnectCreateSQLQuery)
extern PACKAGE System::ResourceString _icsICConnectFuncCheckCaption;
#define Icsicconst_icsICConnectFuncCheckCaption System::LoadResourceString(&Icsicconst::_icsICConnectFuncCheckCaption)
extern PACKAGE System::ResourceString _icsICConnectErrorInitAutoFunc;
#define Icsicconst_icsICConnectErrorInitAutoFunc System::LoadResourceString(&Icsicconst::_icsICConnectErrorInitAutoFunc)
extern PACKAGE System::ResourceString _icsICConnectErrorLoadAutoFunc;
#define Icsicconst_icsICConnectErrorLoadAutoFunc System::LoadResourceString(&Icsicconst::_icsICConnectErrorLoadAutoFunc)
extern PACKAGE System::ResourceString _icsICConnectLabVerCaption;
#define Icsicconst_icsICConnectLabVerCaption System::LoadResourceString(&Icsicconst::_icsICConnectLabVerCaption)
extern PACKAGE System::ResourceString _icsICProfSetErrorSetMsg;
#define Icsicconst_icsICProfSetErrorSetMsg System::LoadResourceString(&Icsicconst::_icsICProfSetErrorSetMsg)
extern PACKAGE System::ResourceString _icsICProfSetCaption;
#define Icsicconst_icsICProfSetCaption System::LoadResourceString(&Icsicconst::_icsICProfSetCaption)
extern PACKAGE System::ResourceString _icsICProfSetLabel1Caption;
#define Icsicconst_icsICProfSetLabel1Caption System::LoadResourceString(&Icsicconst::_icsICProfSetLabel1Caption)
extern PACKAGE System::ResourceString _icsICProfSetLabel3Caption;
#define Icsicconst_icsICProfSetLabel3Caption System::LoadResourceString(&Icsicconst::_icsICProfSetLabel3Caption)
extern PACKAGE System::ResourceString _icsICProfSetOkBtnCaption;
#define Icsicconst_icsICProfSetOkBtnCaption System::LoadResourceString(&Icsicconst::_icsICProfSetOkBtnCaption)
extern PACKAGE System::ResourceString _icsICProfSetCancelBtnCaption;
#define Icsicconst_icsICProfSetCancelBtnCaption System::LoadResourceString(&Icsicconst::_icsICProfSetCancelBtnCaption)
extern PACKAGE System::ResourceString _icsICProfSetLabel4Caption;
#define Icsicconst_icsICProfSetLabel4Caption System::LoadResourceString(&Icsicconst::_icsICProfSetLabel4Caption)
extern PACKAGE System::ResourceString _icsICProfSetLabel2Caption;
#define Icsicconst_icsICProfSetLabel2Caption System::LoadResourceString(&Icsicconst::_icsICProfSetLabel2Caption)
extern PACKAGE System::ResourceString _icsICDMFErrorLoadEKGDLL;
#define Icsicconst_icsICDMFErrorLoadEKGDLL System::LoadResourceString(&Icsicconst::_icsICDMFErrorLoadEKGDLL)
extern PACKAGE System::ResourceString _icsICDMFErrorInEKGDLL;
#define Icsicconst_icsICDMFErrorInEKGDLL System::LoadResourceString(&Icsicconst::_icsICDMFErrorInEKGDLL)
extern PACKAGE System::ResourceString _icsICDMFInqDataCountCaption;
#define Icsicconst_icsICDMFInqDataCountCaption System::LoadResourceString(&Icsicconst::_icsICDMFInqDataCountCaption)
extern PACKAGE System::ResourceString _icsICDMFInqDataLastCaption;
#define Icsicconst_icsICDMFInqDataLastCaption System::LoadResourceString(&Icsicconst::_icsICDMFInqDataLastCaption)
extern PACKAGE System::ResourceString _icsICDMFMissingXMLRef;
#define Icsicconst_icsICDMFMissingXMLRef System::LoadResourceString(&Icsicconst::_icsICDMFMissingXMLRef)
extern PACKAGE System::ResourceString _icsICDMFInqDataCaption;
#define Icsicconst_icsICDMFInqDataCaption System::LoadResourceString(&Icsicconst::_icsICDMFInqDataCaption)
extern PACKAGE System::ResourceString _icsICDMFMissingDefDestData;
#define Icsicconst_icsICDMFMissingDefDestData System::LoadResourceString(&Icsicconst::_icsICDMFMissingDefDestData)
extern PACKAGE System::ResourceString _icsICDMFMissingDefScale;
#define Icsicconst_icsICDMFMissingDefScale System::LoadResourceString(&Icsicconst::_icsICDMFMissingDefScale)
extern PACKAGE System::ResourceString _icsICDMFMissingOutVal;
#define Icsicconst_icsICDMFMissingOutVal System::LoadResourceString(&Icsicconst::_icsICDMFMissingOutVal)
extern PACKAGE System::ResourceString _icsICDMFNewInqCaption;
#define Icsicconst_icsICDMFNewInqCaption System::LoadResourceString(&Icsicconst::_icsICDMFNewInqCaption)
extern PACKAGE System::ResourceString _icsICDMFEditInqCaption;
#define Icsicconst_icsICDMFEditInqCaption System::LoadResourceString(&Icsicconst::_icsICDMFEditInqCaption)
extern PACKAGE System::ResourceString _icsICDMFEKGViewCaption;
#define Icsicconst_icsICDMFEKGViewCaption System::LoadResourceString(&Icsicconst::_icsICDMFEKGViewCaption)
extern PACKAGE System::ResourceString _icsICDMFVNCaption;
#define Icsicconst_icsICDMFVNCaption System::LoadResourceString(&Icsicconst::_icsICDMFVNCaption)
extern PACKAGE System::ResourceString _icsICDMFErrorInqExists;
#define Icsicconst_icsICDMFErrorInqExists System::LoadResourceString(&Icsicconst::_icsICDMFErrorInqExists)
extern PACKAGE System::ResourceString _icsICDMFErrorCreateInputForm;
#define Icsicconst_icsICDMFErrorCreateInputForm System::LoadResourceString(&Icsicconst::_icsICDMFErrorCreateInputForm)
extern PACKAGE System::ResourceString _icsICMainErrorCreateReportNotFull;
#define Icsicconst_icsICMainErrorCreateReportNotFull System::LoadResourceString(&Icsicconst::_icsICMainErrorCreateReportNotFull)
extern PACKAGE System::ResourceString _icsICMainFailPgmLoadMsg;
#define Icsicconst_icsICMainFailPgmLoadMsg System::LoadResourceString(&Icsicconst::_icsICMainFailPgmLoadMsg)
extern PACKAGE System::ResourceString _icsICMainErrorKeyCaption;
#define Icsicconst_icsICMainErrorKeyCaption System::LoadResourceString(&Icsicconst::_icsICMainErrorKeyCaption)
extern PACKAGE System::ResourceString _icsICMainErrorCreateInputForm;
#define Icsicconst_icsICMainErrorCreateInputForm System::LoadResourceString(&Icsicconst::_icsICMainErrorCreateInputForm)
extern PACKAGE System::ResourceString _icsICMainPatListCaption;
#define Icsicconst_icsICMainPatListCaption System::LoadResourceString(&Icsicconst::_icsICMainPatListCaption)
extern PACKAGE System::ResourceString _icsICMainCardCaption;
#define Icsicconst_icsICMainCardCaption System::LoadResourceString(&Icsicconst::_icsICMainCardCaption)
extern PACKAGE System::ResourceString _icsICMainErrorCreateRoportNoDef;
#define Icsicconst_icsICMainErrorCreateRoportNoDef System::LoadResourceString(&Icsicconst::_icsICMainErrorCreateRoportNoDef)
extern PACKAGE System::ResourceString _icsICMainChoiceCreateCaption;
#define Icsicconst_icsICMainChoiceCreateCaption System::LoadResourceString(&Icsicconst::_icsICMainChoiceCreateCaption)
extern PACKAGE System::ResourceString _icsICMainCreateReportCaption;
#define Icsicconst_icsICMainCreateReportCaption System::LoadResourceString(&Icsicconst::_icsICMainCreateReportCaption)
extern PACKAGE System::ResourceString _icsICMainCreatePatListCaption;
#define Icsicconst_icsICMainCreatePatListCaption System::LoadResourceString(&Icsicconst::_icsICMainCreatePatListCaption)
extern PACKAGE System::ResourceString _icsICMainCreateReportCountConf;
#define Icsicconst_icsICMainCreateReportCountConf System::LoadResourceString(&Icsicconst::_icsICMainCreateReportCountConf)
extern PACKAGE System::ResourceString _icsICMainCreateReportCountCaption;
#define Icsicconst_icsICMainCreateReportCountCaption System::LoadResourceString(&Icsicconst::_icsICMainCreateReportCountCaption)
extern PACKAGE System::ResourceString _icsICMainCalcReportCountCaption;
#define Icsicconst_icsICMainCalcReportCountCaption System::LoadResourceString(&Icsicconst::_icsICMainCalcReportCountCaption)
extern PACKAGE System::ResourceString _icsICMainCalcTimeCaption;
#define Icsicconst_icsICMainCalcTimeCaption System::LoadResourceString(&Icsicconst::_icsICMainCalcTimeCaption)
extern PACKAGE System::ResourceString _icsICMainCalcTimeCaptionFormat;
#define Icsicconst_icsICMainCalcTimeCaptionFormat System::LoadResourceString(&Icsicconst::_icsICMainCalcTimeCaptionFormat)
extern PACKAGE System::ResourceString _icsICMainCalcTimeCaptionFormatEx;
#define Icsicconst_icsICMainCalcTimeCaptionFormatEx System::LoadResourceString(&Icsicconst::_icsICMainCalcTimeCaptionFormatEx)
extern PACKAGE System::ResourceString _icsICMainIncReportCountCaption;
#define Icsicconst_icsICMainIncReportCountCaption System::LoadResourceString(&Icsicconst::_icsICMainIncReportCountCaption)
extern PACKAGE System::ResourceString _icsICMainCommReportCaption;
#define Icsicconst_icsICMainCommReportCaption System::LoadResourceString(&Icsicconst::_icsICMainCommReportCaption)
extern PACKAGE System::ResourceString _icsICMain13thMsg;
#define Icsicconst_icsICMain13thMsg System::LoadResourceString(&Icsicconst::_icsICMain13thMsg)
extern PACKAGE System::ResourceString _icsICMain13thCaption;
#define Icsicconst_icsICMain13thCaption System::LoadResourceString(&Icsicconst::_icsICMain13thCaption)
extern PACKAGE System::ResourceString _icsICMainChoiceLibCaption;
#define Icsicconst_icsICMainChoiceLibCaption System::LoadResourceString(&Icsicconst::_icsICMainChoiceLibCaption)
extern PACKAGE System::ResourceString _icsICMainactShowUListCaption;
#define Icsicconst_icsICMainactShowUListCaption System::LoadResourceString(&Icsicconst::_icsICMainactShowUListCaption)
extern PACKAGE System::ResourceString _icsICMainactShowUListHint;
#define Icsicconst_icsICMainactShowUListHint System::LoadResourceString(&Icsicconst::_icsICMainactShowUListHint)
extern PACKAGE System::ResourceString _icsICMainactSpecListOpenCaption;
#define Icsicconst_icsICMainactSpecListOpenCaption System::LoadResourceString(&Icsicconst::_icsICMainactSpecListOpenCaption)
extern PACKAGE System::ResourceString _icsICMainactSpecListOpenHint;
#define Icsicconst_icsICMainactSpecListOpenHint System::LoadResourceString(&Icsicconst::_icsICMainactSpecListOpenHint)
extern PACKAGE System::ResourceString _icsICMainactDocBankOpenCaption;
#define Icsicconst_icsICMainactDocBankOpenCaption System::LoadResourceString(&Icsicconst::_icsICMainactDocBankOpenCaption)
extern PACKAGE System::ResourceString _icsICMainactDocBankOpenHint;
#define Icsicconst_icsICMainactDocBankOpenHint System::LoadResourceString(&Icsicconst::_icsICMainactDocBankOpenHint)
extern PACKAGE System::ResourceString _icsICMainactDocChoiceCaption;
#define Icsicconst_icsICMainactDocChoiceCaption System::LoadResourceString(&Icsicconst::_icsICMainactDocChoiceCaption)
extern PACKAGE System::ResourceString _icsICMainactDocChoiceHint;
#define Icsicconst_icsICMainactDocChoiceHint System::LoadResourceString(&Icsicconst::_icsICMainactDocChoiceHint)
extern PACKAGE System::ResourceString _icsICMainactSubordOrgCaption;
#define Icsicconst_icsICMainactSubordOrgCaption System::LoadResourceString(&Icsicconst::_icsICMainactSubordOrgCaption)
extern PACKAGE System::ResourceString _icsICMainactSubordOrgHint;
#define Icsicconst_icsICMainactSubordOrgHint System::LoadResourceString(&Icsicconst::_icsICMainactSubordOrgHint)
extern PACKAGE System::ResourceString _icsICMainactValExecCaption;
#define Icsicconst_icsICMainactValExecCaption System::LoadResourceString(&Icsicconst::_icsICMainactValExecCaption)
extern PACKAGE System::ResourceString _icsICMainactValExecHint;
#define Icsicconst_icsICMainactValExecHint System::LoadResourceString(&Icsicconst::_icsICMainactValExecHint)
extern PACKAGE System::ResourceString _icsICMainactSettingCaption;
#define Icsicconst_icsICMainactSettingCaption System::LoadResourceString(&Icsicconst::_icsICMainactSettingCaption)
extern PACKAGE System::ResourceString _icsICMainactDocExportCaption;
#define Icsicconst_icsICMainactDocExportCaption System::LoadResourceString(&Icsicconst::_icsICMainactDocExportCaption)
extern PACKAGE System::ResourceString _icsICMainactDocExportHint;
#define Icsicconst_icsICMainactDocExportHint System::LoadResourceString(&Icsicconst::_icsICMainactDocExportHint)
extern PACKAGE System::ResourceString _icsICMainactDocImportCaption;
#define Icsicconst_icsICMainactDocImportCaption System::LoadResourceString(&Icsicconst::_icsICMainactDocImportCaption)
extern PACKAGE System::ResourceString _icsICMainactDocImportHint;
#define Icsicconst_icsICMainactDocImportHint System::LoadResourceString(&Icsicconst::_icsICMainactDocImportHint)
extern PACKAGE System::ResourceString _icsICMainactCardExportCaption;
#define Icsicconst_icsICMainactCardExportCaption System::LoadResourceString(&Icsicconst::_icsICMainactCardExportCaption)
extern PACKAGE System::ResourceString _icsICMainactCardExportHint;
#define Icsicconst_icsICMainactCardExportHint System::LoadResourceString(&Icsicconst::_icsICMainactCardExportHint)
extern PACKAGE System::ResourceString _icsICMainactCardImportCaption;
#define Icsicconst_icsICMainactCardImportCaption System::LoadResourceString(&Icsicconst::_icsICMainactCardImportCaption)
extern PACKAGE System::ResourceString _icsICMainactCardImportHint;
#define Icsicconst_icsICMainactCardImportHint System::LoadResourceString(&Icsicconst::_icsICMainactCardImportHint)
extern PACKAGE System::ResourceString _icsICMainactShowClassesCaption;
#define Icsicconst_icsICMainactShowClassesCaption System::LoadResourceString(&Icsicconst::_icsICMainactShowClassesCaption)
extern PACKAGE System::ResourceString _icsICMainactShowClassesHint;
#define Icsicconst_icsICMainactShowClassesHint System::LoadResourceString(&Icsicconst::_icsICMainactShowClassesHint)
extern PACKAGE System::ResourceString _icsICMainactGrOrgPatChangeCaption;
#define Icsicconst_icsICMainactGrOrgPatChangeCaption System::LoadResourceString(&Icsicconst::_icsICMainactGrOrgPatChangeCaption)
extern PACKAGE System::ResourceString _icsICMainactGrOrgPatChangeHint;
#define Icsicconst_icsICMainactGrOrgPatChangeHint System::LoadResourceString(&Icsicconst::_icsICMainactGrOrgPatChangeHint)
extern PACKAGE System::ResourceString _icsICMainactHelpCaption;
#define Icsicconst_icsICMainactHelpCaption System::LoadResourceString(&Icsicconst::_icsICMainactHelpCaption)
extern PACKAGE System::ResourceString _icsICMainactHelpHint;
#define Icsicconst_icsICMainactHelpHint System::LoadResourceString(&Icsicconst::_icsICMainactHelpHint)
extern PACKAGE System::ResourceString _icsICMainactAboutCaption;
#define Icsicconst_icsICMainactAboutCaption System::LoadResourceString(&Icsicconst::_icsICMainactAboutCaption)
extern PACKAGE System::ResourceString _icsICMainactAboutHint;
#define Icsicconst_icsICMainactAboutHint System::LoadResourceString(&Icsicconst::_icsICMainactAboutHint)
extern PACKAGE System::ResourceString _icsICMainICSAboutDlgTitle;
#define Icsicconst_icsICMainICSAboutDlgTitle System::LoadResourceString(&Icsicconst::_icsICMainICSAboutDlgTitle)
extern PACKAGE System::ResourceString _icsICMainMainTBMBar1Caption;
#define Icsicconst_icsICMainMainTBMBar1Caption System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBar1Caption)
extern PACKAGE System::ResourceString _icsICMainMainTBMBar2Caption;
#define Icsicconst_icsICMainMainTBMBar2Caption System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBar2Caption)
extern PACKAGE System::ResourceString _icsICMainMainTBMBar3Caption;
#define Icsicconst_icsICMainMainTBMBar3Caption System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBar3Caption)
extern PACKAGE System::ResourceString _icsICMainMainTBMBar4Caption;
#define Icsicconst_icsICMainMainTBMBar4Caption System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBar4Caption)
extern PACKAGE System::ResourceString _icsICMainMainTBMBarCategories1;
#define Icsicconst_icsICMainMainTBMBarCategories1 System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBarCategories1)
extern PACKAGE System::ResourceString _icsICMainMainTBMBarCategories2;
#define Icsicconst_icsICMainMainTBMBarCategories2 System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBarCategories2)
extern PACKAGE System::ResourceString _icsICMainMainTBMBarCategories3;
#define Icsicconst_icsICMainMainTBMBarCategories3 System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBarCategories3)
extern PACKAGE System::ResourceString _icsICMainMainTBMBarCategories4;
#define Icsicconst_icsICMainMainTBMBarCategories4 System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBarCategories4)
extern PACKAGE System::ResourceString _icsICMainMainTBMBarCategories5;
#define Icsicconst_icsICMainMainTBMBarCategories5 System::LoadResourceString(&Icsicconst::_icsICMainMainTBMBarCategories5)
extern PACKAGE System::ResourceString _icsICMainbiRegCaption;
#define Icsicconst_icsICMainbiRegCaption System::LoadResourceString(&Icsicconst::_icsICMainbiRegCaption)
extern PACKAGE System::ResourceString _icsICMainWindowBICaption;
#define Icsicconst_icsICMainWindowBICaption System::LoadResourceString(&Icsicconst::_icsICMainWindowBICaption)
extern PACKAGE System::ResourceString _icsICMainHelpBICaption;
#define Icsicconst_icsICMainHelpBICaption System::LoadResourceString(&Icsicconst::_icsICMainHelpBICaption)
extern PACKAGE System::ResourceString _icsICMainMorrBICaption;
#define Icsicconst_icsICMainMorrBICaption System::LoadResourceString(&Icsicconst::_icsICMainMorrBICaption)
extern PACKAGE System::ResourceString _icsICMainMorrBIHint;
#define Icsicconst_icsICMainMorrBIHint System::LoadResourceString(&Icsicconst::_icsICMainMorrBIHint)
extern PACKAGE System::ResourceString _icsICMainbiServiceCaption;
#define Icsicconst_icsICMainbiServiceCaption System::LoadResourceString(&Icsicconst::_icsICMainbiServiceCaption)
extern PACKAGE System::ResourceString _icsICMainbiGrChangeCaption;
#define Icsicconst_icsICMainbiGrChangeCaption System::LoadResourceString(&Icsicconst::_icsICMainbiGrChangeCaption)
extern PACKAGE System::ResourceString _icsICMainbiDocWorkCaption;
#define Icsicconst_icsICMainbiDocWorkCaption System::LoadResourceString(&Icsicconst::_icsICMainbiDocWorkCaption)
extern PACKAGE System::ResourceString _icsICMainPersonalGroupReportBICaption;
#define Icsicconst_icsICMainPersonalGroupReportBICaption System::LoadResourceString(&Icsicconst::_icsICMainPersonalGroupReportBICaption)
extern PACKAGE System::ResourceString _icsICMainReportGroupBICaption;
#define Icsicconst_icsICMainReportGroupBICaption System::LoadResourceString(&Icsicconst::_icsICMainReportGroupBICaption)
extern PACKAGE System::ResourceString _icsICMainbiEICaption;
#define Icsicconst_icsICMainbiEICaption System::LoadResourceString(&Icsicconst::_icsICMainbiEICaption)
extern PACKAGE System::ResourceString _icsICMainbiExportCaption;
#define Icsicconst_icsICMainbiExportCaption System::LoadResourceString(&Icsicconst::_icsICMainbiExportCaption)
extern PACKAGE System::ResourceString _icsICMainbiImportCaption;
#define Icsicconst_icsICMainbiImportCaption System::LoadResourceString(&Icsicconst::_icsICMainbiImportCaption)
extern PACKAGE System::ResourceString _icsICMainbiExitCaption;
#define Icsicconst_icsICMainbiExitCaption System::LoadResourceString(&Icsicconst::_icsICMainbiExitCaption)
extern PACKAGE System::ResourceString _icsICMainbiExitHint;
#define Icsicconst_icsICMainbiExitHint System::LoadResourceString(&Icsicconst::_icsICMainbiExitHint)
extern PACKAGE System::ResourceString _icsICMainErrorOpenCard1;
#define Icsicconst_icsICMainErrorOpenCard1 System::LoadResourceString(&Icsicconst::_icsICMainErrorOpenCard1)
extern PACKAGE System::ResourceString _icsICMainErrorOpenCard2;
#define Icsicconst_icsICMainErrorOpenCard2 System::LoadResourceString(&Icsicconst::_icsICMainErrorOpenCard2)
extern PACKAGE System::ResourceString _icsICModuleTypeErrorNoModules;
#define Icsicconst_icsICModuleTypeErrorNoModules System::LoadResourceString(&Icsicconst::_icsICModuleTypeErrorNoModules)
extern PACKAGE System::ResourceString _icsICModuleTypeErrorRegistryKey;
#define Icsicconst_icsICModuleTypeErrorRegistryKey System::LoadResourceString(&Icsicconst::_icsICModuleTypeErrorRegistryKey)
extern PACKAGE System::ResourceString _icsICModuleTypeCaption;
#define Icsicconst_icsICModuleTypeCaption System::LoadResourceString(&Icsicconst::_icsICModuleTypeCaption)
extern PACKAGE System::ResourceString _icsICModuleTypeOkBtnCaption;
#define Icsicconst_icsICModuleTypeOkBtnCaption System::LoadResourceString(&Icsicconst::_icsICModuleTypeOkBtnCaption)
extern PACKAGE System::ResourceString _icsICModuleTypeCancelBtnCaption;
#define Icsicconst_icsICModuleTypeCancelBtnCaption System::LoadResourceString(&Icsicconst::_icsICModuleTypeCancelBtnCaption)
extern PACKAGE System::ResourceString _icsICModuleTypecxLabel1Caption;
#define Icsicconst_icsICModuleTypecxLabel1Caption System::LoadResourceString(&Icsicconst::_icsICModuleTypecxLabel1Caption)
extern PACKAGE System::ResourceString _icsICICSDBConnectPropertyMissingDatabase;
#define Icsicconst_icsICICSDBConnectPropertyMissingDatabase System::LoadResourceString(&Icsicconst::_icsICICSDBConnectPropertyMissingDatabase)
extern PACKAGE System::ResourceString _icsICICSDBConnectPropertyMissingQuery;
#define Icsicconst_icsICICSDBConnectPropertyMissingQuery System::LoadResourceString(&Icsicconst::_icsICICSDBConnectPropertyMissingQuery)
extern PACKAGE System::ResourceString _icsICICSDBConnectPropertyMissingXMLList;
#define Icsicconst_icsICICSDBConnectPropertyMissingXMLList System::LoadResourceString(&Icsicconst::_icsICICSDBConnectPropertyMissingXMLList)
extern PACKAGE System::ResourceString _icsICICSDBConnectPropertyMissingDBName;
#define Icsicconst_icsICICSDBConnectPropertyMissingDBName System::LoadResourceString(&Icsicconst::_icsICICSDBConnectPropertyMissingDBName)
extern PACKAGE System::ResourceString _icsICICSDBConnectPropertyMissingVersion;
#define Icsicconst_icsICICSDBConnectPropertyMissingVersion System::LoadResourceString(&Icsicconst::_icsICICSDBConnectPropertyMissingVersion)
extern PACKAGE System::ResourceString _icsICICSDBConnectErrorCommCaption;
#define Icsicconst_icsICICSDBConnectErrorCommCaption System::LoadResourceString(&Icsicconst::_icsICICSDBConnectErrorCommCaption)
extern PACKAGE System::ResourceString _icsICICSDBConnectErrorConfDefRead;
#define Icsicconst_icsICICSDBConnectErrorConfDefRead System::LoadResourceString(&Icsicconst::_icsICICSDBConnectErrorConfDefRead)
extern PACKAGE System::ResourceString _icsICICSDBConnectErrorDBVersion;
#define Icsicconst_icsICICSDBConnectErrorDBVersion System::LoadResourceString(&Icsicconst::_icsICICSDBConnectErrorDBVersion)
extern PACKAGE System::ResourceString _icsICICSDBConnectMissingVersionsDef;
#define Icsicconst_icsICICSDBConnectMissingVersionsDef System::LoadResourceString(&Icsicconst::_icsICICSDBConnectMissingVersionsDef)
extern PACKAGE System::ResourceString _icsICICSDBConnectMissingVersionDef;
#define Icsicconst_icsICICSDBConnectMissingVersionDef System::LoadResourceString(&Icsicconst::_icsICICSDBConnectMissingVersionDef)
extern PACKAGE System::ResourceString _icsICICSDBConnectLoadDefsCaption;
#define Icsicconst_icsICICSDBConnectLoadDefsCaption System::LoadResourceString(&Icsicconst::_icsICICSDBConnectLoadDefsCaption)
extern PACKAGE System::ResourceString _icsICICSDBConnectErrorLoadDef;
#define Icsicconst_icsICICSDBConnectErrorLoadDef System::LoadResourceString(&Icsicconst::_icsICICSDBConnectErrorLoadDef)
extern PACKAGE System::ResourceString _icsICICSDBConnectErrorConfRead;
#define Icsicconst_icsICICSDBConnectErrorConfRead System::LoadResourceString(&Icsicconst::_icsICICSDBConnectErrorConfRead)
extern PACKAGE System::ResourceString _icsICICSDBConnectCheckReqDefCaption;
#define Icsicconst_icsICICSDBConnectCheckReqDefCaption System::LoadResourceString(&Icsicconst::_icsICICSDBConnectCheckReqDefCaption)
extern PACKAGE System::ResourceString _icsICICSDBConnectMissingConfDef;
#define Icsicconst_icsICICSDBConnectMissingConfDef System::LoadResourceString(&Icsicconst::_icsICICSDBConnectMissingConfDef)
extern PACKAGE System::ResourceString _icsICICSDBConnectErrorConnect;
#define Icsicconst_icsICICSDBConnectErrorConnect System::LoadResourceString(&Icsicconst::_icsICICSDBConnectErrorConnect)
extern PACKAGE System::ResourceString _icsICICSDBConnectSysError;
#define Icsicconst_icsICICSDBConnectSysError System::LoadResourceString(&Icsicconst::_icsICICSDBConnectSysError)
extern PACKAGE System::ResourceString _icsICICSDBConnectMissingHandlerOnGetDBName;
#define Icsicconst_icsICICSDBConnectMissingHandlerOnGetDBName System::LoadResourceString(&Icsicconst::_icsICICSDBConnectMissingHandlerOnGetDBName)

}	/* namespace Icsicconst */
using namespace Icsicconst;
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// icsICConst
