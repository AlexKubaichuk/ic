//---------------------------------------------------------------------------

#ifndef ProgressFH
#define ProgressFH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Animate.hpp"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TProgressFForm : public TForm
{
__published:	// IDE-managed Components
        TAnimatedImage *AnimatedImage1;
        TProgressBar *CPrBar;
        TProgressBar *PrBar;
        TLabel *MsgLab;
        TLabel *OperLab;
        TLabel *TimeLab;
private:	// User declarations
public:		// User declarations
        __fastcall TProgressFForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TProgressFForm *ProgressFForm;
//---------------------------------------------------------------------------
#endif
