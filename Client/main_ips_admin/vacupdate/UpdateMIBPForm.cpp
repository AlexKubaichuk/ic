#include <vcl.h>
#pragma hdrstop
#include "UpdateMIBPForm.h"
#include "msgdef.h"
// #include "DocListEx.h"
// #include "icDxLocale.h"
// #include "About.h"
// #include "Setting.h"
// #include "Connect.h"
// #include "DocOrgList.h"
#include "ExtUtils.h"
#include <process.h>
// #include "math.h"
// #include "SetDBPath.h"
// #include "ProgressF.h"
// #include "dirscan.h"
// #define itTag ((TTagNode*)uiTag)
// #define MenuTB MainTBM->Bars->Items[0]
// #define RegTB  MainTBM->Bars->Items[1]
// ---------------------------------------------------------------------
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxCheckListBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxTextEdit"
#pragma link "dxBarBuiltInMenu"
#pragma link "cxCustomData"
#pragma link "cxInplaceContainer"
#pragma link "cxStyles"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma resource "*.dfm"
TUpdateMIBPForm * UpdateMIBPForm;
#define _UPD_VAC_SCH_SRC_67   "Source: \"XML\\12063611-00008cd7-cd89.zxml\"; DestDir: \"{app}\\Script\"; Flags: ignoreversion"
#define _UPD_VAC_SCH_SRC_70   "Source: \"XML\\12063611-00008cd7-cd89.zxml\"; DestDir: \"{app}\\defxml\"; Flags: ignoreversion;"
#define _UPD_VAC_SCH_67       "Filename: \"{app}\\Script\\FBBlob.exe\";   Parameters: \" \"\"{reg:HKLM\\{#_RegKey}\\DataBase,Path|HKLM\\{#_RegKey64}\\DataBase,Path}\"\" BINARY_DATA.FLD_VALUE NAME='12063611-00008CD7-CD89' \"\"{app}\\Script\\12063611-00008cd7-cd89.zxml\"\" \"; StatusMsg: \"���������� �� (����� ���������� ������) ...\";"
#define _UPD_TEST_SCH_SRC_67  "Source: \"XML\\001D3500-00005882-DC28.zxml\"; DestDir: \"{app}\\Script\"; Flags: ignoreversion"
#define _UPD_TEST_SCH_SRC_70  "Source: \"XML\\001D3500-00005882-DC28.zxml\"; DestDir: \"{app}\\defxml\"; Flags: ignoreversion;"
#define _UPD_TEST_SCH_67      "Filename: \"{app}\\Script\\FBBlob.exe\";   Parameters: \" \"\"{reg:HKLM\\{#_RegKey}\\DataBase,Path|HKLM\\{#_RegKey64}\\DataBase,Path}\"\" BINARY_DATA.FLD_VALUE NAME='001D3500-00005882-DC28' \"\"{app}\\Script\\001D3500-00005882-DC28.zxml\"\" \"; StatusMsg: \"���������� �� (����� ���������� ����) ...\"; Check: ToolCheck;"
#define _UPD_CHECK_SCH_67     "Filename: \"{app}\\Script\\check_sch.exe\";   Parameters: \"-a\"; StatusMsg: \"���������� ��  ...\"; Check: ToolCheck;"
// ---------------------------------------------------------------------
__fastcall TUpdateMIBPForm::TUpdateMIBPForm(TComponent * Owner, short AIdx) : TForm(Owner)
 {
  // icDxLocale();
  PC->Properties->HideTabs = true;
  AddTypeCBPropertiesChange(NULL);
  BinaryReakChBPropertiesChange(NULL);
  PC->ActivePageIndex = AIdx;
 }
// ---------------------------------------------------------------------
bool __fastcall TUpdateMIBPForm::CheckConnect()
 {
  FillChLB();
  ClearFields();
  return true;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TUpdateMIBPForm::GetClassData(UnicodeString ASQL, UnicodeString ARecId)
 {
  return DM->Clients->App->GetValById(ASQL, ARecId);
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::FillChLB()
 {
  TJSONObject * RetData;
  TJSONPairEnumerator * itPair;
  InfChLB->Items->BeginUpdate();
  // MIBPChLB->Items->BeginUpdate();
  ReakTypeChLB->Items->BeginUpdate();
  VacSchList->Items->BeginUpdate();
  try
   {
    TcxCheckListBoxItem * tmpItem;
    // ��������
    InfChLB->Items->Clear();
    RetData = GetClassData("reg.s2.Select CODE as ps1, R003c as ps2 From class_003a order by R003c ", "0");
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      tmpItem          = InfChLB->Items->Add();
      tmpItem->Checked = false;
      tmpItem->Text    = JSONEnumPairValue(itPair);
      tmpItem->Tag     = JSONEnumPairCode(itPair).ToIntDef(0);
     }
    // ���������������
    // MIBPChLB->Items->Clear();
    VacCB->Properties->Items->Clear();
    RetData = GetClassData("reg.s2.Select CODE as ps1, R0040 as ps2 From class_0035 order by R0040 ", "0");
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      // tmpItem          = MIBPChLB->Items->Add();
      // tmpItem->Checked = false;
      // tmpItem->Text    = JSONEnumPairValue(itPair);
      // tmpItem->Tag     = JSONEnumPairCode(itPair).ToIntDef(0);
      VacCB->Properties->Items->AddObject(JSONEnumPairValue(itPair), (TObject *)JSONEnumPairCode(itPair).ToIntDef(0));
     }
    // �������
    ReakTypeChLB->Items->Clear();
    RetData = GetClassData("reg.s2.Select CODE as ps1, R008e as ps2 From class_0038 order by R008e ", "0");
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      tmpItem          = ReakTypeChLB->Items->Add();
      tmpItem->Checked = false;
      tmpItem->Text    = JSONEnumPairValue(itPair);
      tmpItem->Tag     = JSONEnumPairCode(itPair).ToIntDef(0);
     }
    // �������������
    ManufCB->Properties->Items->Clear();
    RetData = GetClassData("reg.s2.Select CODE as ps1, R019B as ps2 From class_019A order by R019B ", "0");
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      ManufCB->Properties->Items->AddObject(JSONEnumPairValue(itPair), (TObject *)JSONEnumPairCode(itPair).ToIntDef(0));
     }
    // ������
    CountryCB->Properties->Items->Clear();
    RetData = GetClassData("reg.s2.Select CODE as ps1, R0199 as ps2 From class_0198 order by R0199 ", "0");
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      CountryCB->Properties->Items->AddObject(JSONEnumPairValue(itPair),
        (TObject *)JSONEnumPairCode(itPair).ToIntDef(0));
     }
    // �����
    RetData = GetClassData("reg.s2.Select CODE as ps1, R002C as ps2 From class_002A order by R002C ", "0");
    itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
     }
    VacSchList->Items->Clear();
    TTagNode * FVacSchDef = DM->XMLList->GetXML("12063611-00008cd7-cd89")->GetChildByName("schemalist", true)
      ->GetFirstChild();
    while (FVacSchDef)
     {
      RetData          = GetClassData("reg.f.0035.0040", FVacSchDef->AV["ref"]);
      tmpItem          = VacSchList->Items->Add();
      tmpItem->Checked = FVacSchDef->AV["def"].ToIntDef(0);
      tmpItem->Text    = ((TJSONString *)RetData->Pairs[0]->JsonValue)->Value() + "->" + FVacSchDef->AV["name"];
      tmpItem->Tag     = UIDInt(FVacSchDef->AV["uid"]);
      FVacSchDef       = FVacSchDef->GetNext();
     }
    // UnicodeString FCount;
    // DM->qtExec("Select Count(*) as RCOUNT From class_0035", false);
    // FCount = "���������� ������/����. ������: " + DM->quFree->FN("RCOUNT")->AsString + ", ����: ";
    // DM->qtExec("Select Count(*) as RCOUNT From class_002A", false);
    // FCount += DM->quFree->FN("RCOUNT")->AsString;
    // Caption = FCount;
   }
  __finally
   {
    InfChLB->Items->EndUpdate();
    // MIBPChLB->Items->EndUpdate();
    ReakTypeChLB->Items->EndUpdate();
    VacSchList->Items->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::FormShow(TObject * Sender)
 {
  CheckConnect();
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::AddTypeCBPropertiesChange(TObject * Sender)
 {
  FullNameEDLabel->Enabled                = !AddTypeCB->ItemIndex;
  FullNameEDLabel->FocusControl->Enabled  = !AddTypeCB->ItemIndex;
  ShortNameEDLabel->Enabled               = !AddTypeCB->ItemIndex;
  ShortNameEDLabel->FocusControl->Enabled = !AddTypeCB->ItemIndex;
  // MIBPChLB->Enabled                           = !AddTypeCB->ItemIndex;
  VacNameEDLabel->Enabled                     = AddTypeCB->ItemIndex;
  VacNameEDLabel->FocusControl->Enabled       = AddTypeCB->ItemIndex;
  CanApplyVacChB->Enabled                     = AddTypeCB->ItemIndex;
  VacCBLabel->Enabled                         = AddTypeCB->ItemIndex;
  VacCBLabel->FocusControl->Enabled           = AddTypeCB->ItemIndex;
  InsTypeCBLabel->Enabled                     = AddTypeCB->ItemIndex;
  InsTypeCBLabel->FocusControl->Enabled       = AddTypeCB->ItemIndex;
  ManufCBLabel->Enabled                       = !AddTypeCB->ItemIndex;
  ManufCBLabel->FocusControl->Enabled         = !AddTypeCB->ItemIndex;
  CountryCBLabel->Enabled                     = !AddTypeCB->ItemIndex;
  CountryCBLabel->FocusControl->Enabled       = !AddTypeCB->ItemIndex;
  DefDozeLabel->Enabled                       = !AddTypeCB->ItemIndex;
  DefDozeLabel->FocusControl->Enabled         = !AddTypeCB->ItemIndex;
  ResistReakValEDLabel->Enabled               = AddTypeCB->ItemIndex;
  ResistReakValEDLabel->FocusControl->Enabled = AddTypeCB->ItemIndex;
  // Pause1EDLabel->Enabled                      = AddTypeCB->ItemIndex;
  // Pause1EDLabel->FocusControl->Enabled        = AddTypeCB->ItemIndex;
  // Pause2EDLabel->Enabled                      = AddTypeCB->ItemIndex;
  // Pause2EDLabel->FocusControl->Enabled        = AddTypeCB->ItemIndex;
  // Pause3EDLabel->Enabled                      = AddTypeCB->ItemIndex;
  // Pause3EDLabel->FocusControl->Enabled        = AddTypeCB->ItemIndex;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::BinaryReakChBPropertiesChange(TObject * Sender)
 {
  ReakTypeChLB->Enabled = !BinaryReakChB->Checked;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::ClearFields()
 {
  NameED->Text            = "";
  FullNameED->Text        = "";
  VacNameED->Text         = "";
  ShortNameED->Text       = "";
  DoseED->Text            = "0,5";
  BinaryReakChB->Checked  = true;
  CanApplyVacChB->Checked = false;
  VacCB->ItemIndex        = -1;
  ResistReakValED->Text   = "";
  // Pause1ED->Text          = "30";
  // Pause2ED->Text          = "365";
  // Pause3ED->Text          = "3";
  // for (int i = 0; i < MIBPChLB->Items->Count; i++)
  // {
  // if ((MIBPChLB->Items->Items[i]->Tag == 1) || (MIBPChLB->Items->Items[i]->Tag == 2))
  // MIBPChLB->Items->Items[i]->Checked = true;
  // else
  // MIBPChLB->Items->Items[i]->Checked = false;
  // }
  for (int i = 0; i < InfChLB->Items->Count; i++)
   InfChLB->Items->Items[i]->Checked = false;
  for (int i = 0; i < ReakTypeChLB->Items->Count; i++)
   ReakTypeChLB->Items->Items[i]->Checked = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::FUpdateGen(const UnicodeString & AUID)
 {
  /*
   DM->qtExec("Select Max(code) as MCODE from class_"+AUID, false);
   if (DM->quFree->FN("MCODE")->IsNull)
   DM->qtExec("SET GENERATOR GEN_"+AUID+"_ID TO 1", false);
   else
   DM->qtExec("SET GENERATOR GEN_"+AUID+"_ID TO "+DM->quFree->FN("MCODE")->AsString, false);
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::actAddExecute(TObject * Sender)
 {
  if (CheckInput())
   {
    try
     {
      // FUpdateGen("0035");
      // FUpdateGen("0042");
      // FUpdateGen("002D");
      // FUpdateGen("0039");
      // FUpdateGen("002A");
      // FUpdateGen("0079");
      // FUpdateGen("002B");
      // FUpdateGen("0046");
      // FUpdateGen("0066");
      if (AddTypeCB->ItemIndex)
       AddTest();
      else
       AddVac();
     }
    __finally
     {
      FillChLB();
      ClearFields();
      AddTypeCB->SetFocus();
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TUpdateMIBPForm::CheckInput()
 {
  bool RC = false;
  try
   {
    int infCount, reakCount;
    infCount = CheckedItems(InfChLB);
    if (ReakTypeChLB->Enabled)
     {
      reakCount = CheckedItems(ReakTypeChLB);
     }
    else
     reakCount = -1;
    if (!NameED->Text.Trim().Length())
     {
      NameED->SetFocus();
      _MSG_ERR("���������� ������� ������������.", "������");
     }
    else if (!DoseED->Text.Trim().Length())
     {
      DoseED->SetFocus();
      _MSG_ERR("���������� ������� ����.", "������");
     }
    else if (infCount == 0)
     {
      InfChLB->SetFocus();
      _MSG_ERR("���������� ������� ��������.", "������");
     }
    else if ((infCount > 1) && AddTypeCB->ItemIndex)
     {
      InfChLB->SetFocus();
      _MSG_ERR("��� ����� ����� ���� ������� ������ ���� ��������.", "������");
     }
    else if (reakCount == 0)
     {
      ReakTypeChLB->SetFocus();
      _MSG_ERR("���������� ������� ������� �������.", "������");
     }
    else
     RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TUpdateMIBPForm::CheckedItems(TcxCheckListBox * ASrc)
 {
  int RC = 0;
  try
   {
    for (int i = 0; i < ASrc->Items->Count; i++)
     if (ASrc->Items->Items[i]->Checked)
      RC++ ;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TUpdateMIBPForm::GetCBData(TcxComboBox * AData)
 {
  Variant RC = Variant::Empty();
  try
   {
    if (AData->ItemIndex != -1)
     RC = (int)(AData->Properties->Items->Objects[AData->ItemIndex]);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TInsertClassItem * __fastcall TUpdateMIBPForm::NewInsertClassItem(UnicodeString AId)
 {
  TInsertClassItem * RC = NULL;
  TTagNode * FDef = DM->RegDef->GetTagByUID(AId);
  RC               = new TInsertClassItem(FDef);
  RC->OnGetValById = DM->OnGetValById;
  RC->OnInsertData = DM->InsertData;
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::AddVac()
 {
  TInsertClassItem * FMIBP = NewInsertClassItem("0035");
  TInsertClassItem * FMIBPInf = NewInsertClassItem("002D");
  TInsertClassItem * FMIBPDose = NewInsertClassItem("0193");
  TInsertClassItem * FMIBPReack = NewInsertClassItem("0039");
  UnicodeString MIBPRecId, DoseRecId, MIBPInfRecId, MIBPReackRecId, ExecRC;
  try
   {
    // ��������� ����
    FMIBP->NewRec();
    FMIBP->Data->Value["331A"] = S1078ED->Text.Trim();
    FMIBP->Data->Value["0040"] = NameED->Text.Trim();
    FMIBP->Data->Value["008F"] = FullNameED->Text.Trim();
    FMIBP->Data->Value["006D"] = ShortNameED->Text.Trim();
    FMIBP->Data->Value["0191"] = GetCBData(ManufCB);
    FMIBP->Data->Value["0192"] = GetCBData(CountryCB);
    FMIBP->Data->Value["0064"] = BinaryReakChB->Checked;
    FMIBP->Execute(MIBPRecId, ExecRC);
    if (SameText(ExecRC, "ok"))
     {
      FMIBPDose->NewRec();
      FMIBPDose->Data->Value["0194"] = MIBPRecId;
      FMIBPDose->Data->Value["0195"] = StrToFloat(DoseED->Text);
      FMIBPDose->Execute(DoseRecId, ExecRC);
      // ���������� -> ����
      for (int i = 0; i < InfChLB->Items->Count; i++)
       {
        if (InfChLB->Items->Items[i]->Checked)
         {
          FMIBPInf->NewRec();
          FMIBPInf->Data->Value["003E"] = InfChLB->Items->Items[i]->Tag;
          FMIBPInf->Data->Value["003B"] = MIBPRecId;
          FMIBPInf->Execute(MIBPInfRecId, ExecRC);
         }
       }
      // ������� ������� �� ����
      if (ReakTypeChLB->Enabled)
       {
        for (int i = 0; i < ReakTypeChLB->Items->Count; i++)
         {
          if (ReakTypeChLB->Items->Items[i]->Checked)
           {
            FMIBPReack->NewRec();
            FMIBPReack->Data->Value["0090"] = MIBPRecId;
            FMIBPReack->Data->Value["0091"] = ReakTypeChLB->Items->Items[i]->Tag;
            FMIBPReack->Execute(MIBPReackRecId, ExecRC);
           }
         }
       }
     }
    // �� ������������� ����
    // ���������� � planSetting
   }
  __finally
   {
    delete FMIBP;
    delete FMIBPInf;
    delete FMIBPDose;
    delete FMIBPReack;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::AddTest()
 {
  TInsertClassItem * FTest = NewInsertClassItem("002A");
  TInsertClassItem * FTestInf = NewInsertClassItem("0079");
  TInsertClassItem * FTestReack = NewInsertClassItem("002B");
  UnicodeString TestRecId, TestInfRecId, TestReackRecId, ExecRC; // DoseRecId, MIBPReackRecId,
  try
   {
    int infCode = 0;
    for (int i = 0; i < InfChLB->Items->Count; i++)
     {
      if (InfChLB->Items->Items[i]->Checked)
       {
        infCode = InfChLB->Items->Items[i]->Tag;
        break;
       }
     }
    FTest->NewRec();
    FTest->Data->Value["0064"] = BinaryReakChB->Checked;
    FTest->Data->Value["331B"] = S1078ED->Text.Trim();
    FTest->Data->Value["002C"] = NameED->Text.Trim();
    FTest->Data->Value["003D"] = infCode;
    if (InsTypeCB->ItemIndex != -1)
     FTest->Data->Value["115B"] = InsTypeCB->ItemIndex + 1;
    if (CanApplyVacChB->Checked && (VacCB->ItemIndex != -1))
     FTest->Data->Value["0067"] = GetCBData(VacCB);
    FTest->Data->Value["0063"] = StrToFloat(DoseED->Text);
    FTest->Execute(TestRecId, ExecRC);
    if (SameText(ExecRC, "ok"))
     {
      // ���������� -> �����/��������
      FTestInf->NewRec();
      FTestInf->Data->Value["007B"] = infCode;
      FTestInf->Data->Value["007A"] = TestRecId;
      FTest->Execute(TestInfRecId, ExecRC);
      // ������� ������� �� �����/��������
      if (ReakTypeChLB->Enabled)
       {
        for (int i = 0; i < ReakTypeChLB->Items->Count; i++)
         {
          if (ReakTypeChLB->Items->Items[i]->Checked)
           {
            FTestReack->NewRec();
            FTestReack->Data->Value["002E"] = TestRecId;
            FTestReack->Data->Value["0037"] = ReakTypeChLB->Items->Items[i]->Tag;
            FTestReack->Execute(TestReackRecId, ExecRC);
           }
         }
       }
     }
   }
  __finally
   {
    delete FTest;
    delete FTestInf;
    delete FTestReack;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TUpdateMIBPForm::CheckSetupInput()
 {
  bool RC = false;
  try
   {
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::actCreateSetupExecute(TObject * Sender)
 {
  if (CheckSetupInput())
   {
    CreateSetupExecute("7", "vac7_update", true, true, true, true); // ������ �������
    CreateSetupExecute("7", "vac7_update", true, false, true, false); // ��� ����
    // CreateSetupExecute("6.7", "vac_update",true, true, true, true); //������
    // CreateSetupExecute("6.7", "vac_update",true, false, true, false); //��� ����
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::CreateSetupExecute(UnicodeString AVer, UnicodeString AScript, bool AVac, bool AVacSch,
  bool ATest, bool ATestSch)
 {
  UnicodeString FAppName, FOutFileName;
  FAppName = FOutFileName = "";
  TStringList * FISSScript = new TStringList;
  PrBar->Position = 0;
  FCheckVacCode   = 0;
  FCheckTestCode  = 0;
  try
   {
    TReplaceFlags repF;
    repF << rfReplaceAll;
    UnicodeString FCurDir = ExtractFilePath(ParamStr(0));
    UnicodeString FScriptFN = icsPrePath(FCurDir + "\\Data\\" + AScript + ".iss");
    UnicodeString FScriptFN_ch = icsPrePath(FCurDir + "\\Data\\" + AScript + "_ch.iss");
    UnicodeString FDestScriptFN = "";
    UnicodeString FDestScriptFN_ch = "";
    if (AVac && AVacSch && ATest && ATestSch)
     {
      FDestScriptFN    = icsPrePath(FCurDir + "\\_" + AScript + ".iss");
      FDestScriptFN_ch = icsPrePath(FCurDir + "\\_" + AScript + "ch.iss");
     }
    else
     {
      FDestScriptFN = AScript;
      if (AVac)
       FDestScriptFN += ".v";
      if (AVacSch)
       FDestScriptFN += ".vsh";
      if (ATest)
       FDestScriptFN += ".t";
      if (ATestSch)
       FDestScriptFN += ".tsh";
      FDestScriptFN_ch = icsPrePath(FCurDir + "\\_" + FDestScriptFN + "_ch.iss");
      FDestScriptFN    = icsPrePath(FCurDir + "\\_" + FDestScriptFN + ".iss");
     }
    if (FileExists(FScriptFN))
     {
      if (UpdateNameED->Text.Trim().Length())
       FAppName = UpdateNameED->Text.Trim();
      else
       {
        if (AVac && ATest)
         FAppName = "���������� ������ ������ � ����";
        else if (AVac)
         FAppName = "���������� ������ ������";
        else if (ATest)
         FAppName = "���������� ������ ����";
        if (AVacSch && ATestSch)
         FAppName += ", ���� �� ����������";
        else if (AVacSch)
         {
          if (AVac && ATest)
           FAppName += ", ���� ���������� ������";
          else
           FAppName += ", ���� �� ����������";
         }
        else if (ATestSch)
         {
          if (AVac && ATest)
           FAppName += ", ���� ���������� ����";
          else
           FAppName += ", ���� �� ����������";
         }
        FAppName += " " + Date().FormatString("yyyy") + ".";
       }
      // if (OutFNED->Text.Trim().Length())
      // FOutFileName = OutFNED->Text.Trim();
      // else
      // {
      if (AVac && ATest)
       FOutFileName = "vac_update";
      else if (AVac)
       FOutFileName = "vac_update";
      else if (ATest)
       FOutFileName = "test_update";
      FOutFileName += "_" + Sysutils::StringReplace(AVer, ".", "", repF) + "_" + Date().FormatString("yyyy.mm.dd");
      if (!AVacSch && !ATestSch)
       FOutFileName += ".���_����";
      else if (!AVacSch)
       {
        FOutFileName += ".���_����_������";
       }
      else if (!ATestSch)
       {
        FOutFileName += ".���_����_����";
       }
      // if ((CheckVacCB->ItemIndex == -1) && (CheckTestCB->ItemIndex == -1))
      // FOutFileName += ".���_��������";
      // else if (CheckVacCB->ItemIndex == -1)
      // {
      // FOutFileName += ".���_��������_������";
      // }
      // else if (CheckTestCB->ItemIndex == -1)
      // {
      // FOutFileName += ".���_��������_����";
      // }
      // }
      FISSScript->LoadFromFile(FScriptFN);
      PrBar->Position = 5;
      Application->ProcessMessages(); // ++++
      UnicodeString FScript = FISSScript->Text;
      FScript = Sysutils::StringReplace(FScript, "_APPNAME_", FAppName, repF);
      FScript = Sysutils::StringReplace(FScript, "_VER_", AVer, repF);
      FScript = Sysutils::StringReplace(FScript, "_APPDATE_", Date().FormatString("dd.mm.yyyy"), repF);
      FScript = Sysutils::StringReplace(FScript, "_Y_RIGHT_", Date().FormatString("yyyy"), repF);
      FScript = Sysutils::StringReplace(FScript, "_UPDFILE_", "���_�����." + FOutFileName, repF);
      // int FCheckVacCode = 0;
      // int FCheckTestCode = 0;
      if (!DirectoryExists(icsPrePath(FCurDir + "\\XML")))
       ForceDirectories(icsPrePath(FCurDir + "\\XML"));
      TTagNode * nd = new TTagNode;
      try
       {
        nd->AsXML = DM->Clients->App->GetPlanOpt();
        nd->GetChildByName("passport", true)->AV["version"] = Date().FormatString("yyyy.mm.dd");
        nd->SaveToZIPXMLFile(icsPrePath(FCurDir + "\\XML\\planSetting.zxml"), "");
       }
      __finally
       {
        delete nd;
       }
      // DM->XMLList->GetXML("planSetting")->SaveToZIPXMLFile(icsPrePath(FCurDir + "\\XML\\planSetting.zxml"), "");
      if (AVac)
       {
        if (AVacSch)
         {
          DM->XMLList->GetXML("12063611-00008cd7-cd89")->SaveToZIPXMLFile
            (icsPrePath(FCurDir + "\\XML\\12063611-00008cd7-cd89.zxml"), "");
          if (AVer == "6.7")
           FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_SRC_", _UPD_VAC_SCH_SRC_67, repF);
          else
           FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_SRC_", _UPD_VAC_SCH_SRC_70, repF);
          if (AVer == "6.7")
           FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_", _UPD_VAC_SCH_67, repF);
         }
        else
         {
          FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_SRC_", "", repF);
          if (AVer == "6.7")
           FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_", "", repF);
         }
        // if (CheckVacCB->ItemIndex != -1)
        // FCheckVacCode = (int)CheckVacCB->Properties->Items->Objects[CheckVacCB->ItemIndex];
       }
      else
       {
        FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_SRC_", "", repF);
        if (AVer == "6.7")
         FScript = Sysutils::StringReplace(FScript, "_UPD_VAC_SCH_", "", repF);
       }
      PrBar->Position = 15;
      Application->ProcessMessages(); // ++++
      if (ATest)
       {
        if (ATestSch)
         {
          DM->XMLList->GetXML("001D3500-00005882-DC28")->SaveToZIPXMLFile
            (icsPrePath(FCurDir + "\\XML\\001D3500-00005882-DC28.zxml"), "");
          if (AVer == "6.7")
           FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_SRC_", _UPD_TEST_SCH_SRC_67, repF);
          else
           FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_SRC_", _UPD_TEST_SCH_SRC_70, repF);
          if (AVer == "6.7")
           FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_", _UPD_TEST_SCH_67, repF);
         }
        else
         {
          FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_SRC_", "\n", repF);
          if (AVer == "6.7")
           FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_", "\n", repF);
         }
        // if (CheckTestCB->ItemIndex != -1)
        // FCheckTestCode = (int)CheckTestCB->Properties->Items->Objects[CheckTestCB->ItemIndex];
       }
      else
       {
        FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_SRC_", "\n", repF);
        if (AVer == "6.7")
         FScript = Sysutils::StringReplace(FScript, "_UPD_TEST_SCH_", "\n", repF);
       }
      if (AVer == "6.7")
       {
        if (ATestSch || AVacSch)
         FScript = Sysutils::StringReplace(FScript, "_UPD_CHECK_SCH_", _UPD_CHECK_SCH_67, repF);
        else
         FScript = Sysutils::StringReplace(FScript, "_UPD_CHECK_SCH_", "\n", repF);
       }
      PrBar->Position = 20;
      Application->ProcessMessages(); // ++++
      if (AVer == "6.7")
       {
        if (FCheckVacCode && FCheckTestCode)
         FScript = Sysutils::StringReplace(FScript, "_CHECK_SCRIPT_",
          "\"\"select pr.code, ts.code from class_0035 pr, CLASS_002A ts where  pr.code = " + IntToStr(FCheckVacCode) +
          " and ts.code = " + IntToStr(FCheckTestCode) + "\"\"", repF);
        else if (FCheckVacCode)
         FScript = Sysutils::StringReplace(FScript, "_CHECK_SCRIPT_",
          "\"\"select pr.code from class_0035 pr where  pr.code = " + IntToStr(FCheckVacCode) + "\"\"", repF);
        else if (FCheckTestCode)
         FScript = Sysutils::StringReplace(FScript, "_CHECK_SCRIPT_",
          "\"\"select ts.code from CLASS_002A ts where  ts.code = " + IntToStr(FCheckTestCode) + "\"\"", repF);
        else
         FScript = Sysutils::StringReplace(FScript, "_CHECK_SCRIPT_", "", repF);
       }
      FISSScript->Text = FScript;
      FISSScript->SaveToFile(FDestScriptFN);
      PrBar->Position = 30;
      FISSScript->LoadFromFile(FScriptFN_ch);
      PrBar->Position = 35;
      Application->ProcessMessages(); // ++++
      FScript          = FISSScript->Text;
      FScript          = Sysutils::StringReplace(FScript, "_APPNAME_", FAppName, repF);
      FScript          = Sysutils::StringReplace(FScript, "_VER_", AVer, repF);
      FScript          = Sysutils::StringReplace(FScript, "_APPDATE_", Date().FormatString("dd.mm.yyyy"), repF);
      FScript          = Sysutils::StringReplace(FScript, "_Y_RIGHT_", Date().FormatString("yyyy"), repF);
      FScript          = Sysutils::StringReplace(FScript, "_OUT_FILE_NAME_", FOutFileName, repF);
      FScript          = Sysutils::StringReplace(FScript, "_UPDFILE_", "���_�����." + FOutFileName, repF);
      FISSScript->Text = FScript;
      FISSScript->SaveToFile(FDestScriptFN_ch);
      PrBar->Position = 40;
      Application->ProcessMessages(); // ++++
      FScript = "";
      TStringList * FClassList = new TStringList;
      TTagNode * FDataVer = new TTagNode;
      try
       {
        if (AVer == "6.7")
         FClassList->Text = "003A\n0035\n0042\n002D\n0039\n002A\n0079\n002B\n0046\n0066\n000A";
        else
         FClassList->Text = "0198\n019A\n003A\n0035\n002D\n002A\n013E\n0079\n0141\n0193\n0038\n0039\n002B\n319D";
        PrBar->Properties->Max = FClassList->Count * 3;
        // disable triggers
        for (int i = 0; i < FClassList->Count; i++)
         {
          FScript += DisableTriggers(FClassList->Strings[i]);
          PrBar->Position++ ;
          Application->ProcessMessages();
         }
        // copy tables data
        for (int i = 0; i < FClassList->Count; i++)
         {
          FScript += AddDeleteScript(FClassList->Strings[i]);
          PrBar->Position++ ;
          Application->ProcessMessages();
         }
        // enable triggers
        for (int i = 0; i < FClassList->Count; i++)
         {
          FScript += EnableTriggers(FClassList->Strings[i]);
          PrBar->Position++ ;
          Application->ProcessMessages();
         }
        if (ExtScript->Lines->Text.Trim().Length())
         FScript += "\n" + ExtScript->Lines->Text.Trim();
        FISSScript->Text = FScript;
        FISSScript->SaveToFile(icsPrePath(FCurDir + "\\update.sql"), TEncoding::UTF8);
        FDataVer->Name       = "versioninfo";
        FDataVer->AV["app"]  = DM->Clients->App->GetMinDataVersion();
        FDataVer->AV["data"] = Date().FormatString("yyyy.mm.dd");
        FDataVer->SaveToXMLFile(icsPrePath(FCurDir + "\\dataver.xml"));
        PrBar->Position = 100;
        Application->ProcessMessages(); // ++++
        ExecCommand("\"" + icsPrePath(FCurDir + "\\iss\\Compil32.exe") + "\" /cc \"" + FDestScriptFN + "\"");
        ExecCommand("\"" + icsPrePath(FCurDir + "\\iss\\Compil32.exe") + "\" /cc \"" + FDestScriptFN_ch + "\"");
        // if (FileExists("_ch" + FOutFileName))
        // DeleteFile("_ch" + FOutFileName);
       }
      __finally
       {
        delete FClassList;
        delete FDataVer;
       }
     }
    else
     _MSG_ERR("����������� ������ �������.", "������");
   }
  __finally
   {
    delete FISSScript;
    PrBar->Position = 0;
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TUpdateMIBPForm::DisableTriggers(UnicodeString AClsID)
 {
  return DM->Clients->App->DisableTriggers(70, AClsID);
  /*
   UnicodeString RC = "";
   try
   {
   DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_" +
   AClsID.UpperCase() + "%'", false);
   while (!DM->quFree->Eof)
   {
   if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_" + AClsID.UpperCase() + "_BI5") &&
   (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_" + AClsID.UpperCase() + "_BU5"))
   {
   RC += "ALTER TRIGGER " + DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() + " INACTIVE;\n";
   }
   DM->quFree->Next();
   }
   DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_A" +
   AClsID.UpperCase() + "%'", false);
   while (!DM->quFree->Eof)
   {
   if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A" + AClsID.UpperCase() + "_BI5")
   && (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A" + AClsID.UpperCase() + "_BU5"))
   {
   RC += "ALTER TRIGGER " + DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() + " INACTIVE;\n";
   }
   DM->quFree->Next();
   }
   }
   __finally
   {
   }
   return RC;
   */ }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TUpdateMIBPForm::EnableTriggers(UnicodeString AClsID)
 {
  return DM->Clients->App->EnableTriggers(70, AClsID);
  /*
   UnicodeString RC = "";
   try
   {
   DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_" +
   AClsID.UpperCase() + "%'", false);
   while (!DM->quFree->Eof)
   {
   if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_" + AClsID.UpperCase() + "_BI5") &&
   (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_" + AClsID.UpperCase() + "_BU5"))
   {
   RC += "ALTER TRIGGER " + DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() + " ACTIVE;\n";
   }
   DM->quFree->Next();
   }
   DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_A" +
   AClsID.UpperCase() + "%'", false);
   while (!DM->quFree->Eof)
   {
   if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A" + AClsID.UpperCase() + "_BI5")
   && (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A" + AClsID.UpperCase() + "_BU5"))
   {
   RC += "ALTER TRIGGER " + DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() + " ACTIVE;\n";
   }
   DM->quFree->Next();
   }
   }
   __finally
   {
   }
   return RC;
   */ }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TUpdateMIBPForm::AddDeleteScript(UnicodeString AClsID)
 {
  return DM->Clients->App->AddDeleteScript(70, AClsID);
  /*
   UnicodeString RC = "Delete from class_" + AClsID + ";\nDelete from class_a" + AClsID + ";\n";
   FFLList.clear();
   FFLTypes.clear();
   TReplaceFlags repF;
   repF << rfReplaceAll;
   try
   {
   TTagNode * FClsDefNode = DM->xmlRegDef->GetTagByUID(AClsID);
   if (FClsDefNode)
   {
   TTagNode * itCls = FClsDefNode->GetFirstChild()->GetNext();
   while (itCls)
   {
   FFLList[itCls->AV["uid"]] = "R" + itCls->AV["uid"];
   if (itCls->CmpName("text,date,datetime,time,extedit"))
   FFLTypes[itCls->AV["uid"]] = "C";
   else
   FFLTypes[itCls->AV["uid"]] = "I";
   itCls = itCls->GetNext();
   }
   UnicodeString FInsertFl, FInsertVal;
   DM->qtExec("Select * From class_" + AClsID + "", false);
   while (!DM->quFree->Eof)
   {
   FInsertFl  = AClsID + " (code";
   FInsertVal = ") values (" + DM->quFree->FN("CODE")->AsString;
   for (TAnsiStrMap::iterator i = FFLList.begin(); i != FFLList.end(); i++)
   {
   FInsertFl += ", " + i->second;
   if (DM->quFree->FN(i->second)->IsNull)
   FInsertVal += ", null";
   else
   {
   if (FFLTypes[i->first] == "C")
   FInsertVal += ", '" + Sysutils::StringReplace(DM->quFree->FN(i->second)->AsString, "'", "`", repF) + "'";
   else
   FInsertVal += "," + Sysutils::StringReplace(DM->quFree->FN(i->second)->AsString, ",", ".", repF);
   }
   }
   FInsertFl += FInsertVal += ");\n";
   RC += "Insert into class_" + FInsertFl + "Insert into class_A" + FInsertFl;
   DM->quFree->Next();
   }
   }
   else
   {
   _MSG_ERR("������ � ��������� ����������� ID=\"" + AClsID + "\".", "������");
   }
   }
   __finally
   {
   }
   return RC;
   */ }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::actSaveDefSchExecute(TObject * Sender)
 {
  /*
   TTagNode * FVacSchDef = DM->XMLList->GetXML("12063611-00008cd7-cd89");
   for (int i = 0; i < VacSchList->Items->Count; i++)
   {
   FVacSchDef->GetTagByUID(UIDStr(VacSchList->Items->Items[i]->Tag))->AV["def"] =
   IntToStr((int)VacSchList->Items->Items[i]->Checked);
   }
   TFileStream * BinData = NULL;
   try
   {
   // BinData = new TFileStream("C:\\Program Files\\AXE\\samples\\��������\\prep\\11381C3E-00003981-56B0.zxml",fmOpenRead);
   DM->quFree->Close();
   if (DM->quFree->Transaction->Active)
   DM->quFree->Transaction->Commit();
   DM->quFree->Transaction->StartTransaction();
   DM->quFree->SQL->Clear();
   DM->quFree->SQL->Add("Update BINARY_DATA Set FLD_VALUE = ?DDD Where NAME='12063611-00008CD7-CD89'");
   DM->quFree->ParamByName("DDD")->AsString = FVacSchDef->AsZIPXML;
   DM->quFree->Prepare();
   DM->quFree->ExecQuery();
   DM->quFree->Transaction->Commit();
   }
   __finally
   {
   // if (BinData) delete BinData;
   }
   */ }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::GridCustomDrawDataCell(TcxCustomTreeList * Sender, TcxCanvas * ACanvas,
  TcxTreeListEditCellViewInfo * AViewInfo, bool & ADone)
 {
  // if (!AViewInfo->Selected)
  // {
  // paint the focused node
  // ACanvas->Brush->Color = 0x80FFFF;
  // ACanvas->FillRect(AViewInfo->EditRect, clRed);
  // TcxBorders fff;
  // fff = cxBordersAll;
  // ACanvas->FrameRect(AViewInfo->BoundsRect);//, clDefault, 1, fff);
  ACanvas->MoveTo(AViewInfo->BoundsRect.left - 1, AViewInfo->BoundsRect.top);
  ACanvas->LineTo(AViewInfo->BoundsRect.right - 1, AViewInfo->BoundsRect.top);
  ACanvas->LineTo(AViewInfo->BoundsRect.right - 1, AViewInfo->BoundsRect.bottom * 2);
  ACanvas->LineTo(AViewInfo->BoundsRect.left - 1, AViewInfo->BoundsRect.bottom * 2);
  ACanvas->LineTo(AViewInfo->BoundsRect.left - 1, AViewInfo->BoundsRect.top);
  // ACanvas->FillRect(AViewInfo->EditRect, clRed);
  UnicodeString DDD = "wewerw wer wer wer werwe rwer";
  ACanvas->DrawTexT("wewerw wer wer wer werwe rwer", AViewInfo->BoundsRect, cxWordBreak);
  // }
  ADone = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TUpdateMIBPForm::ExecCommand(UnicodeString ACmd)
 {
  try
   {
    try
     {
      STARTUPINFO si;
      PROCESS_INFORMATION pi;
      ZeroMemory(& si, sizeof(si));
      si.cb = sizeof(si);
      ZeroMemory(& pi, sizeof(pi));
      // Start the child process.
      if (CreateProcess(NULL, // No module name (use command line)
        ACmd.c_str(), // Command line
        NULL, // Process handle not inheritable
        NULL, // Thread handle not inheritable
        false, // Set handle inheritance to FALSE
        NORMAL_PRIORITY_CLASS, // No creation flags
        NULL, // Use parent's environment block
        NULL, // Use parent's starting directory
        &si, // Pointer to STARTUPINFO structure
        &pi) // Pointer to PROCESS_INFORMATION structure
        )
       {
        // ���� ���������� �������������.
        WaitForInputIdle(pi.hProcess, INFINITE);
        // ���� ���������� ��������.
        WaitForSingleObject(pi.hProcess, INFINITE);
        // �������� ��� ����������.
        // GetExitCodeProcess(pi.hProcess, ExitCode);
        // ��������� ���������� ��������.
        CloseHandle(pi.hThread);
        // ��������� ���������� ������.
        CloseHandle(pi.hProcess);
       }
      else
       {
        // _MSG_ERR(L"������ �������� �������� ���������� ����� � �����.\n��������� ���������:\n" +
        // GetAPILastErrorFormatMessage() + L".", L"������");
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERR(E.Message, "������ 21");
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
