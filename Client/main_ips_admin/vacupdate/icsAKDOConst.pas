unit icsAKDOConst;
//#include "icsAKDOConstDef.h"

interface

resourcestring
// Common
   icsAKDOCommonErrorCaption = '������';
   icsAKDOCommonErrorLoadCaption = '������ �������� ���������';
   icsAKDOCommonWarningCaption = '��������������';
   icsAKDOCommonMsgCaption = '���������';
   icsAKDOCommonErrorAddRecMsg = '������ ���������� ������';

//DocTemplate

   icsAKDODocTemplateCaption = '������������ �������';
   icsAKDODocTemplateCancelBtnCaption = '������';
   icsAKDODocTemplateOkBtnCaption = '���������';

//Connect

   icsAKDOConnectErrorReadProjectDef = '������ ������ �������� �������';
   icsAKDOConnectErrorReadProjectDef2 = '������ ����������� ���������� ����������� � ��';
   icsAKDOConnectErrorProjectDefMissing = '����������� ���� �������� �������';
   icsAKDOConnectKeyMissing = '��������� ������� ����� ������ !!!';
   icsAKDOConnectKeyOldVal = '         ���� ������ ������ !!!'#13#10'  ��� ���������� ������ ����� ������'#13#10'   ��������� � ������� �������������'#13#10'��� "���������������� ����������� �������".'#13#10'���������� ���������� �� ������ �������� '#13#10'   � ������� "�������"|"� ���������".';
   icsAKDOConnectProjectCaption = '�������� �������';
   icsAKDOConnectRegistryCaption = '�������� ������������';
   icsAKDOConnectNomenklatorCaption = '�������� ������� �������';
   icsAKDOConnectCheckDocFuncCaption = '�������� ������� (���������)...';
   icsAKDOConnectErrorLoadNomDLL = '������ �������� ������ �������� �������������� ���������(���������)';
   icsAKDOConnectErrorInitNomDLL = '������ ������������� ������ �������� �������������� ���������(���������)';
   icsAKDOConnectZaklCaption = '����������';
   icsAKDOConnectEKGCaption = '���';
   icsAKDOConnectErrorConnectCaption = '������ �����������.';
   icsAKDOConnectErrorReadSetParams = '������ ������ ���������� ���������';
   icsAKDOConnectMissingSettingSection = '����������� ������ �������� � ����� �������� �������';
   icsAKDOConnectMissingEKGSection = '����������� ������ ��� � ����� �������� �������';
   icsAKDOConnectMissingEKGDataDef = '����������� �������� ����� ������ ��� ���';
   icsAKDOConnectErrorReadImportConf = '������ ������ ������������ �������';
   icsAKDOConnectTablesCaption = '������� ...';
   icsAKDOConnectRoutinesCaption = '������ ...';
   icsAKDOConnectRulesCaption = '������� ...';
   icsAKDOConnectBrokenFuncRef = '������ �� ������������� �������;'#13#10'   ������="%s";'#13#10'   UID="%s"';
   icsAKDOConnectBrokenDefRef = '������ �� ������������� �������� "%s"';
   icsAKDOConnectMissingRef1 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s";'#13#10'   �� ������� "%s"';
   icsAKDOConnectMissingRef2 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s";'#13#10'   ������� "������ �������� ������"';
   icsAKDOConnectMissingRef3 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s";'#13#10'   �������� ������� -> UID="%s"';
   icsAKDOConnectMissingRef4 = '������ �� ������������� ������� "%s" � �������� ��������;'#13#10'   ������="%s"';
   icsAKDOConnectMissingRef5 = '������ �� ������������� ������ � �������� ��������;'#13#10'   ������="%s"';
   icsAKDOConnectMissingRef6 = '������ �� ������������� ������� � �������� ��������;'#13#10'   ������="%s"';
   icsAKDOConnectErrorProjectElemType = '�� ���������� ��� �������� ������� "%s"';
   icsAKDOConnectMissingLPUParam = '� ���������� ��������� �� ������ ��������� ���';
   icsAKDOConnectCreateInqListCaption = '������������ ������ ������������ ...';
   icsAKDOConnectCreateRuleListCaption = '������������ ������ �������� ������ ...';
   icsAKDOConnectCreateSQLQuery = '���������� �������� ��� �������� ������ ...';
   icsAKDOConnectFuncCheckCaption = '�������� ������� ...';
   icsAKDOConnectErrorInitAutoFunc = '������ ������������� ������ �������� �������������� ���������';
   icsAKDOConnectErrorLoadAutoFunc = '������ �������� ������ �������� �������������� ���������';

   icsAKDOConnectLabVerCaption = '������';

//ProfSet

   icsAKDOProfSetErrorSetMsg = '����������� ������������� ���� �������� �� ������ !';

   icsAKDOProfSetCaption = '������������� �������� �� ������';
   icsAKDOProfSetLabel1Caption = '�����';
   icsAKDOProfSetLabel3Caption = '�� ������������� �������';
   icsAKDOProfSetOkBtnCaption = '���������';
   icsAKDOProfSetCancelBtnCaption = '������';
   icsAKDOProfSetLabel4Caption = '�����';
   icsAKDOProfSetLabel2Caption = '������������� �������';

//DMF

   icsAKDODMFErrorLoadEKGDLL = '������ �������� ������ �������� ��������� ���';
   icsAKDODMFErrorInEKGDLL = '������ � ������ ����� ���';
   icsAKDODMFInqDataCountCaption = '������������: %s%s';
   icsAKDODMFInqDataLastCaption = ', ���������: %s';
   icsAKDODMFMissingXMLRef = '������ �� ������������� XML GUI="%s"';
   icsAKDODMFInqDataCaption = '������ ������������';
   icsAKDODMFMissingDefDestData = '����������� �������� �������������� ������';
   icsAKDODMFMissingDefScale = '����������� �������� �����';
   icsAKDODMFMissingOutVal = '����������� �������� ��� ��������� �������';
   icsAKDODMFNewInqCaption = '����� ������������';
   icsAKDODMFEditInqCaption = '������������� ��������� ������������';
   icsAKDODMFEKGViewCaption = '�������� ���';
   icsAKDODMFVNCaption = '������ ������������� ���������';
   icsAKDODMFErrorInqExists = '������������ �� "%s �." ��� ����������.'#13#10'� ���� ���� ����� ���� ������ ���� ������������.';
   icsAKDODMFErrorCreateInputForm = '������ ������������ ����� ����� ������������';

//Main

   icsAKDOMainErrorCreateReportNotFull = '������������ ���������� ����������, ������������ ��������� �� � ������ ������.';
   icsAKDOMainFailPgmLoadMsg = '���������� � �������������� ���������'#13#10'��� ���������� ���������';
   icsAKDOMainErrorKeyCaption = '  (������� "��� ��������")';
   icsAKDOMainErrorCreateInputForm = '������ ������������ ����� ����� ������������';
   icsAKDOMainPatListCaption = '������ ���������';
   icsAKDOMainCardCaption = '����� ������������ <%s>';
   icsAKDOMainErrorCreateRoportNoDef = '������ ������������ ������ (����������� �������� ������)';
   icsAKDOMainChoiceCreateCaption = '������������ ������� ��� "%s"';
   icsAKDOMainCreateReportCaption = '������������ "%s"';
   icsAKDOMainCreatePatListCaption = '������������ ������ ���������';
   icsAKDOMainCreateReportCountConf = '���������� ���������� ����� 40,'#13#10' ������������ ����� ������ ���������� �����.'#13#10'���������� ?';
   icsAKDOMainCreateReportCountCaption = '������������ ����������  (�����: %s)';
   icsAKDOMainCalcReportCountCaption = '������ ����������  ( %s �� %s)';
   icsAKDOMainCalcTimeCaption = '�������� %s';
   icsAKDOMainCalcTimeCaptionFormat = 'n ���. s ���.';
   icsAKDOMainCalcTimeCaptionFormatEx = 'h ���. n ���. s ���.';
   icsAKDOMainIncReportCountCaption = '������������ ���������� ( %s �� %s)';
   icsAKDOMainCommReportCaption = '����������';
   icsAKDOMain13thMsg = ''#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10'                                        ��, �����..., �� ����....                                         '#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10#13#10'';
   icsAKDOMain13thCaption = '������ ��������� ��������';

   icsAKDOMainChoiceLibCaption = '���������� ��������';

   icsAKDOMainactShowUListCaption = '&������ ���������';
   icsAKDOMainactShowUListHint = '������� ������ ���������';
   icsAKDOMainactSpecListOpenCaption = '������ � �����������';
   icsAKDOMainactSpecListOpenHint = '������� ������ �������� ����������';
   icsAKDOMainactDocBankOpenCaption = '���� ����������';
   icsAKDOMainactDocBankOpenHint = '���� �������������� ����������';
   icsAKDOMainactDocChoiceCaption = '���������� ��������';
   icsAKDOMainactDocChoiceHint = '���������� ��������';
   icsAKDOMainactSubordOrgCaption = '����������������� �����������';
   icsAKDOMainactSubordOrgHint = '����������������� ����������� ...';
   icsAKDOMainactValExecCaption = '�������+';
   icsAKDOMainactValExecHint = '�������+';
   icsAKDOMainactSettingCaption = '&���������';
   icsAKDOMainactDocExportCaption = '���������, �������� ����������';
   icsAKDOMainactDocExportHint = '�������� ����������, �������� ����������';
   icsAKDOMainactDocImportCaption = '���������, �������� ����������';
   icsAKDOMainactDocImportHint = '���� ����������, �������� ����������';
   icsAKDOMainactCardExportCaption = '����� ������������';
   icsAKDOMainactCardExportHint = '�������� ���� ������������';
   icsAKDOMainactCardImportCaption = '����� ������������';
   icsAKDOMainactCardImportHint = '���� ���� ������������';
   icsAKDOMainactShowClassesCaption = '�����������';
   icsAKDOMainactShowClassesHint = '�����������';
   icsAKDOMainactGrOrgPatChangeCaption = '������ ���������� �������������� ���������� ���������';
   icsAKDOMainactGrOrgPatChangeHint = '������ ���������� �������������� ���������� ���������';
   icsAKDOMainactHelpCaption = '����� &�������';
   icsAKDOMainactHelpHint = '����� �������';
   icsAKDOMainactAboutCaption = '� &���������';
   icsAKDOMainactAboutHint = '� ���������';
   icsAKDOMainICSAboutDlgTitle = '����';

   icsAKDOMainMainTBMBar1Caption = '����';
   icsAKDOMainMainTBMBar2Caption = '������������';
   icsAKDOMainMainTBMBar3Caption = '���������';
   icsAKDOMainMainTBMBar4Caption = '�������������� ���������';

   icsAKDOMainMainTBMBarCategories1 = '������������';
   icsAKDOMainMainTBMBarCategories2 = '���������';
   icsAKDOMainMainTBMBarCategories3 = '����/��������';
   icsAKDOMainMainTBMBarCategories4 = '������';
   icsAKDOMainMainTBMBarCategories5 = '������';
   icsAKDOMainbiRegCaption = '&������������';
   icsAKDOMainWindowBICaption = '&����';
   icsAKDOMainHelpBICaption = '&?';
   icsAKDOMainMorrBICaption = '�������������� �� ������������';
   icsAKDOMainMorrBIHint = '�������������� �� ������������';
   icsAKDOMainbiServiceCaption = '������';
   icsAKDOMainbiGrChangeCaption = '������ � �������� ���������';
   icsAKDOMainbiDocWorkCaption = '&���������';
   icsAKDOMainPersonalGroupReportBICaption = '����������';
   icsAKDOMainReportGroupBICaption = '������, ������';
   icsAKDOMainbiEICaption = '����/�������� ������';
   icsAKDOMainbiExportCaption = '��������';
   icsAKDOMainbiImportCaption = '�������';
   icsAKDOMainbiExitCaption = '�&����';
   icsAKDOMainbiExitHint = '�����';
   icsAKDOMainErrorOpenCard1 = '������ �������� ����� ������������, �� �������� ��� ��������.';
   icsAKDOMainErrorOpenCard2 = '������ �������� ����� ������������, � ������� �������� "������ ������������", �� �������� �� ������ ������������.';

//ModuleType

   icsAKDOModuleTypeErrorNoModules = '����������� ������������������ �������� ������������';
   icsAKDOModuleTypeErrorRegistryKey = '��������� �������� ����� �������';

   icsAKDOModuleTypeCaption = '����� �������� ������������';
   icsAKDOModuleTypeOkBtnCaption = '���������';
   icsAKDOModuleTypeCancelBtnCaption = '������';
   icsAKDOModuleTypecxLabel1Caption = '������������������ ������� ������������ ������������ "����"';

//ICSDBConnect

   icsAKDOICSDBConnectPropertyMissingDatabase = '�� ������ �������� "Database"';
   icsAKDOICSDBConnectPropertyMissingQuery = '�� ������ �������� "Query"';
   icsAKDOICSDBConnectPropertyMissingXMLList = '�� ������ �������� "XMLList"';
   icsAKDOICSDBConnectPropertyMissingDBName = '�� ������ �������� "DBName"';
   icsAKDOICSDBConnectPropertyMissingVersion = '�� ������ �������� "Version"';
   icsAKDOICSDBConnectErrorCommCaption = '������ ��� ����������� � ��, ����������� �� ���������.';
   icsAKDOICSDBConnectErrorConfDefRead = '������ ������ �������� ������������.';
   icsAKDOICSDBConnectErrorDBVersion = '������ �� �� ������������� ���������.'#13#10'������ "%s", ��������� (%s)';
   icsAKDOICSDBConnectMissingVersionsDef = '����������� �������� ������';
   icsAKDOICSDBConnectMissingVersionDef = '����������� �������� ������';
   icsAKDOICSDBConnectLoadDefsCaption = '�������� ��������';
   icsAKDOICSDBConnectErrorLoadDef = '������ �������� �������� "%s"';
   icsAKDOICSDBConnectErrorConfRead = '������ ������ ������������';
   icsAKDOICSDBConnectCheckReqDefCaption = '�������� ������� ������������ ��������';
   icsAKDOICSDBConnectMissingConfDef = '����������� �������� ������������';
   icsAKDOICSDBConnectErrorConnect = '%s'#13#10'���: %s'#13#10'�������������� ���: %s';
   icsAKDOICSDBConnectSysError = '%s'#13#10'��������� ��������� "%s"';
   icsAKDOICSDBConnectMissingHandlerOnGetDBName = '����������� ���������� ������� "OnGetDBName"';

//

//   icsAKDO = '';

implementation

end.
