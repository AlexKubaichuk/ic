//---------------------------------------------------------------------------

#ifndef dirscanH
#define dirscanH
//---------------------------------------------------------------------------
#include <Classes.hpp>
typedef void __fastcall (__closure *TFileIterateItem)(AnsiString, TSearchRec);
//---------------------------------------------------------------------------
class PACKAGE EDirScanError : public Exception
{
public:
     __fastcall EDirScanError(const AnsiString Msg) : Sysutils::Exception(Msg) { };
};
//---------------------------------------------------------------------------
class TDirScan : public TObject
{
__published:	// IDE-managed Components
private:	// User declarations
        AnsiString FDir,FFileMask;
        TFileIterateItem FOnIterateItem;
        bool FCanSubdir;
        void __fastcall FSetDir(AnsiString ADir);
        void __fastcall FDirScan(AnsiString AFN);
public:		// User declarations
        __fastcall TDirScan();
        __property AnsiString Dir  = {read=FDir,write=FSetDir};
        __property AnsiString FileMask  = {read=FFileMask,write=FFileMask};
        __property bool CanSubdir  = {read=FCanSubdir,write=FCanSubdir};
        void __fastcall Execute();
        __property TFileIterateItem OnIterateItem = {read=FOnIterateItem,write=FOnIterateItem};
};
#endif
