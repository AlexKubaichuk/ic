//---------------------------------------------------------------------------
#include <vcl.h>
#include <IniFiles.hpp>
#pragma hdrstop

#include "Connect.h"
#include "SetDBPath.h"
//#include "ModuleType.h"
#include "DMF.h"
#include "ExtUtils.h"
#include "DocExDFM.h"
#include "ExtFunc.h"
#include "icsICConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxLabel"
#pragma link "cxProgressBar"
#pragma resource "*.dfm"

TConnectForm *ConnectForm;
//HINSTANCE fProc;
//int (FAR* CallProc)(HWND *Parent,quest *Src);
#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif
//---------------------------------------------------------------------------
__fastcall TConnectForm::TConnectForm(TComponent* Owner)
    : TForm(Owner)
{
  FOnLoadImportConfig = NULL;
  LabVer->Caption = FMT(icsICConnectLabVerCaption);
//  InqXMLList = new TStringList;
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::Connect()
{
  TRegIniFile  *ConnectIni = new TRegIniFile ("IMM");
  ConnectIni->RootKey = HKEY_LOCAL_MACHINE;
  try
   {
     try
      {
        AnsiString DBName,ProjXMLPath,RegXMLPath,NmklPath,XMLProjGUI, FLogin, FPasswd;
        AnsiString xxCount = "0";
        Application->CreateForm(__classid(TDM), &DM);

        DM->RegKey = K_REG;
        ConnectIni->OpenKey(DM->RegKey.c_str(),false);
        AnsiString FDBUser,FDBPassword;
        AnsiString FDBCharset = ConnectIni->ReadString("DataBase","charset","");


    DM->CurrentPath = icsPrePath(ConnectIni->ReadString("CurrentVersion","AppPath",GetCurrentDir()));

        ConnectIni->CloseKey();

        if (FileExists(icsPrePath(DM->CurrentPath+"\\ic.ico").c_str()))/*"+DM->SubDir+"\\*/
         Application->Icon->LoadFromFile(icsPrePath(DM->CurrentPath+"\\ic.ico").c_str()); /*\\"+DM->SubDir+"*/
        DM->RegComp->RegistryKey = DM->RegKey+"\\ClassifSize";
        // ������ �������� �������

        if (FileExists(icsPrePath(DM->CurrentPath+"\\ProjectDef.zxml").c_str()))
         {
           TTagNode *prDef = new TTagNode(NULL);
           bool FDefLoad = false;
           try
            {
              prDef->LoadFromZIPXMLFile(icsPrePath(DM->CurrentPath+"\\ProjectDef.zxml").c_str());
              TTagNode *prPassport = prDef->GetChildByName("PASSPORT");
              if (prPassport)
               {
                 DM->ProdName = _PRODNAME;
                 ProdName->Caption = DM->ProdName;
                 Autor->Caption    = prPassport->AV["autor"];
                 Version->Caption  = icsGetFileVersion(Application->ExeName);
                 Application->Title = DM->ProdName;
                 DM->_DB_NAME = prPassport->AV["dbname"].Trim().UpperCase();
                 if (DM->_DB_NAME.Length())
                  DM->_DB_Alias = ChangeFileExt(DM->_DB_NAME,"")+"_db";
                 else
                  DM->_DB_Alias = "ic_db";
                 ProdName->Update();
                 FDefLoad = true;
                 FLogin = "";
                 FPasswd = "";
                 DBName = "";
               }
            }
           __finally
            {
              if (prDef) delete prDef;
            }
           if (!FDefLoad)
            {
              _MSG_ERR(FMT(icsICConnectErrorReadProjectDef),FMT(icsICCommonErrorLoadCaption));
              return false;
            }
         }
        else
         {
           _MSG_ERR(FMT(icsICConnectErrorProjectDefMissing),FMT(icsICCommonErrorLoadCaption));
           Close();
           return false;
         }
        Show();
        Application->ProcessMessages();
        //************************************************************************
        bool IsConnected = false;
        ConMod = new TICSDBConnect;
        try
         {
           AnsiString FSrv;
           FSrv = "";
#ifdef CLIENT
           icsGetClientDBParam("IMM",
                                     DM->RegKey,   DM->_DB_NAME,
                                     DM->_DB_Alias , DM->Test_Conn_DB,
                                     true, FLogin, FPasswd, FSrv, DBName);
#else
           icsGetFullDBParam("IMM",
                                     DM->RegKey,   DM->_DB_NAME,
                                     DM->_DB_Alias , DM->Test_Conn_DB,
                                     true, FLogin, FPasswd, FSrv, DBName);
#endif
           ConMod->Database           = DM->IMM_DB;
           ConMod->CheckDBName        = DM->_DB_Alias;
           ConMod->Query              = DM->quFree;
           ConMod->DBName             = DBName;
           ConMod->DBLogin            = FLogin;
           ConMod->DBPwd              = FPasswd;

           ConMod->Version            = "6.7.0.3;6.7.0.0;6.8.1.0";
           ConMod->XMLList            = DM->XMLList;
           ConMod->OnProgressInit     = PrInit;
           ConMod->OnProgress         = PrCh;
           ConMod->OnProgressComplite = PrComp;

           ConMod->CheckModules->Add("import_cfg");
//           ConMod->CheckModules->Add("project");

           ConMod->OnGetDBName      = FGetDBName;
           ConMod->OnLoadImportCfg  = FLoadImportCfg;
           AnsiString FReqMod = "registry="+FMT(icsICConnectRegistryCaption)+";";
#ifdef _USE_NOM
           FReqMod +=           "nomenklator="+FMT(icsICConnectNomenklatorCaption)+";";
#endif
           FReqMod +=           "immcard="+FMT(icsICConnectImmCardCaption)+";";
           FReqMod +=           "vac="+FMT(icsICConnectVacSchCaption)+";";
           FReqMod +=           "probe="+FMT(icsICConnectTestSchCaption);
           AnsiString FCurVer;
           IsConnected = (ConMod->Connect(FReqMod, FCurVer) == dcrOk);
           if (IsConnected)
            {
              DBName           = ConMod->DBName;
              FDBUser          = ConMod->DBLogin;
              FDBPassword      = FPasswd;


              DM->RegComp->DBCharset  = FDBCharset;
              DM->RegComp->DBUser     = FDBUser;
              DM->RegComp->DBPassword = FDBPassword;

              DM->VersionData = DM->XMLList->GetXML("ICS_DATA_DEF")->GetChildByName("versioninfo",true);

              DM->xmlRegDef      = DM->XMLList->GetXML(ConMod->LoadedModulesTypes["registry"]);
              DM->xmlImmCardDef  = DM->XMLList->GetXML(ConMod->LoadedModulesTypes["immcard"]);
              DM->xmlVacDef      = DM->XMLList->GetXML(ConMod->LoadedModulesTypes["vac"]);
              DM->xmlTestDef     = DM->XMLList->GetXML(ConMod->LoadedModulesTypes["probe"]);
            }
         }
        __finally
         {
           if (ConMod)
            {
              ConMod->OnProgressInit = NULL;
              ConMod->OnProgress = NULL;
              ConMod->OnProgressComplite = NULL;
              delete ConMod;
            }
         }
        if (IsConnected)
         {
   /* TODO -okab -ccreate :
   �������� ������������� �������� �� ���������
   ����� �������� AppOptions */
//        DM->CurrentPath = icsPrePath(DM->AppOpt->Vals["AppPath"]);
           PrBar->Position = 100;
           LoadLPUParam();
           if (PrepareInqData(DBName))
            {
              PrBar->Properties->Max = 1000;
        DM->FClassData = new TICSClassData(DM->xmlRegDef, DM->quFree);
        DM->FClassData->UseHash = true;
        //******************************************************************************
        // ------         �������������� ����-������                 -------------------
        //******************************************************************************
#ifndef IMM_DOCUMENT
        if (DM->trFree->Active) DM->trFree->Commit();
        DM->qtExec("Select CODE, NAME From OKVP");
        while (!DM->quFree->Eof)
         {
           DM->ProfList[quFNStr("CODE")] = quFNStr("NAME");
           DM->quFree->Next();
         }
        DM->qtExec("Select CODE, R0040 From CLASS_0035");
        while (!DM->quFree->Eof)
         {
           DM->VacList[quFNStr("CODE")] = quFNStr("R0040");
           DM->VacListCode[quFNStr("R0040")] = quFNStr("CODE");
           DM->quFree->Next();
         }
        DM->qtExec("Select CODE, R002C From CLASS_002A");
        while (!DM->quFree->Eof)
         {
           DM->ProbList[quFNStr("CODE")] = quFNStr("R002C");
           DM->ProbListCode[quFNStr("R002C")] = quFNStr("CODE");
           DM->quFree->Next();
         }
        DM->qtExec("Select CODE, R003C From CLASS_003A");
        while (!DM->quFree->Eof)
         {
           DM->InfList[quFNStr("CODE")] = quFNStr("R003C");
           DM->InfListCode[quFNStr("R003C")] = quFNStr("CODE");
           DM->quFree->Next();
         }
        TIntMap *vti;
        TIntListMap::iterator RC;
        DM->qtExec("Select R003E, R003B From CLASS_002D");
        while (!DM->quFree->Eof)
         {
           RC = DM->InfVacList.find(quFNInt("R003E"));
           if (RC == DM->InfVacList.end())
            DM->InfVacList[quFNInt("R003E")] = new TIntMap;
           vti = DM->InfVacList[quFNInt("R003E")];
           (*vti)[quFNInt("R003B")] = 1;

           RC = DM->VacInfList.find(quFNInt("R003B"));
           if (RC == DM->VacInfList.end())
            DM->VacInfList[quFNInt("R003B")] = new TIntMap;
           vti = DM->VacInfList[quFNInt("R003B")];
           (*vti)[quFNInt("R003E")] = 1;

           DM->quFree->Next();
         }
        DM->qtExec("Select R007B, R007A From CLASS_0079");
        while (!DM->quFree->Eof)
         {
           RC = DM->InfProbList.find(quFNInt("R007B"));
           if (RC == DM->InfProbList.end())
            DM->InfProbList[quFNInt("R007B")] = new TIntMap;
           vti = DM->InfProbList[quFNInt("R007B")];
           (*vti)[quFNInt("R007A")] = 1;

           RC = DM->ProbInfList.find(quFNInt("R007A"));
           if (RC == DM->ProbInfList.end())
            DM->ProbInfList[quFNInt("R007A")] = new TIntMap;
           vti = DM->ProbInfList[quFNInt("R007A")];
           (*vti)[quFNInt("R007B")] = 1;

           DM->quFree->Next();
         }
        DM->trFree->Commit();
        //******************************************************************************
        // ------         ������������� ������ ������������         --------------------
        //******************************************************************************
        DM->RegComp->ClassTreeColor = LtColor(clBtnFace,32);
        DM->RegComp->DBName = DBName;
        DM->RegComp->XML_AV1_Name = "";
        DM->RegComp->OnLoadXML = LoadRegXML;
        DM->RegComp->OnInit = ConCallBack;
        DM->RegComp->Active = true;
        DM->RegComp->OnInit = NULL;
        DM->RegComp->OnLoadXML = NULL;
        //******************************************************************************
        // ---- ������������� ����� ������������ �����  ----------------
        //******************************************************************************
              DM->ReqColor =  clInfoBk;
              DM->RegComp->RequiredColor = DM->ReqColor;
        //******************************************************************************
#endif
            }
           Close();
           return true;
         }
        else
         {
           Close();
           return false;
         }
      }
     catch(Exception& E)
      {
        _MSG_ERR(E.Message,FMT(icsICCommonErrorLoadCaption));
        Close();
        return false;
      }
     catch (...)
      {
        _MSG_ERR(FMT(icsICCommonErrorLoadCaption),FMT(icsICCommonErrorCaption));
        Close();
        return false;
      }
   }
  __finally
   {
     ConnectIni->CloseKey();
     delete ConnectIni;
   }
  return true; 
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::FGetDBName(const AnsiString AMessage, AnsiString &AName, AnsiString &ALogin, AnsiString &APwd)
{
  _MSG_ERR(AMessage,FMT(icsICConnectErrorConnectCaption));
  bool FRC = false;
  TSetDBPathForm *Dlg = NULL;
  try
   {
#ifdef CLIENT
     Dlg = new TSetDBPathForm(this, "IMM", DM->RegKey, DM->_DB_NAME , DM->_DB_Alias , DM->Test_Conn_DB, false, true);
#else
     Dlg = new TSetDBPathForm(this, "IMM", DM->RegKey, DM->_DB_NAME , DM->_DB_Alias , DM->Test_Conn_DB, false);
#endif
     if (Dlg)
      {
        Dlg->ShowModal();
        FRC = (Dlg->ModalResult == mrOk);
        if (FRC)
         {
           Application->ProcessMessages();
           AnsiString FLogin, FPasswd, FSrv, FDBPath;
#ifdef CLIENT
           icsGetClientDBParam("IMM",
                                     DM->RegKey,  DM->_DB_NAME,
                                     DM->_DB_Alias , DM->Test_Conn_DB,
                                     true, FLogin, FPasswd, FSrv, FDBPath);
#else
           icsGetFullDBParam("IMM",
                                     DM->RegKey,  DM->_DB_NAME,
                                     DM->_DB_Alias , DM->Test_Conn_DB,
                                     true, FLogin, FPasswd, FSrv, FDBPath);
#endif


           AName   = FDBPath;
           ALogin  = FLogin;
           APwd    = FPasswd;
           Application->ProcessMessages();
         }
      }
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
  return FRC;
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::FLoadImportCfg(TTagNode *ACfgNode)
{
  bool FRC = true;
  if (ACfgNode)
   {
     if (ACfgNode->CmpName("imp_conf"))
      {
        TTagNode *tmp;
        DM->xmlRegDef          = DM->XMLList->GetXML(ConMod->LoadedModulesTypes["registry"]);
        try
         {
           // ��������� ��������� ������������ ������ ��� ���������������
           TTagNode *FConf = ACfgNode->GetChildByName("classes",true);
           SetDelegateAttr(FConf,DM->xmlRegDef);
           // ��������� ��������� ������������ ������ ��� ������ ���������
           FConf = ACfgNode->GetChildByName("unit",true);
           if (SetDelegateAttr(FConf,DM->xmlRegDef))
            {
              tmp = DM->xmlRegDef->GetChildByName("unit")->GetChildByName("description",true);
              tmp->AV["insertlabel"] = "";
              tmp->AV["deletelabel"] = "";
            }
           // ��������� ��������� ������������ ������ ��� ���. �����
//           FConf = tmpConf->GetChildByName("units",true);
//           SetDelegateAttr(FConf,DM->xmlImmCardDef);
           DM->SystemMode = smLimited;
         }
        catch (...)
         {
           _MSG_ERR(FMT(icsICConnectErrorReadImportConf),FMT(icsICConnectErrorReadSetParams));
           FRC = false;
         }
      }
     else if (FOnLoadImportConfig)
      {
        FRC = FOnLoadImportConfig(ACfgNode);
      }
   }
  return FRC;
}
//---------------------------------------------------------------------------
/*
bool __fastcall TConnectForm::SetUIDRefers(TTagNode *itTag, AnsiString &UID)
{
   PrBar->Position++;
   Application->ProcessMessages();
   if (itTag->CmpName("table"))
    {
      MsgLab->Caption = FMT(icsICConnectTablesCaption);
      MsgLab->Update();
      Application->ProcessMessages();
    }
   if (itTag->CmpName("routine"))
    {
      MsgLab->Caption = FMT(icsICConnectRoutinesCaption);
      MsgLab->Update();
      Application->ProcessMessages();
    }
   if (itTag->CmpName("rule"))
    {
      MsgLab->Caption = FMT(icsICConnectRulesCaption);
      MsgLab->Update();
      Application->ProcessMessages();
    }
   xAtt = NULL;
   if (!itTag->CmpName("objref"))
    xAtt = itTag->GetAttrByName("ref");
   if (xAtt)
    {
      if (itTag->AV["ref"] != "no")
       {
         GUI = _GUI(xAtt->Value);
         if (GUI.Length() == 22)
          {
            // ������� ������
            int Ind = InqXMLList->IndexOf(GUI);
            if (Ind != -1)
             { // ������ �� ������� � XML �� ssdl-o
               iTag = (TTagNode*)InqXMLList->Objects[Ind];
               if (iTag->CmpName("ssdl-o"))
                {
                  xAtt->Ref = iTag->GetTagByUID(_UID(xAtt->Value)); // �������� ����� ������� ��� ������������� �������� �� UID�
                  iTag = iTag->GetTagByUID(xAtt->Ref->AV["scaleuid"]); // �������� ����� ������� ��� ������������� ��������
                  if (iTag)
                   {
                     xAtt->Ref->GetAttrByName("scaleuid")->Ref = iTag;
                   }
                }
               else if (iTag->CmpName("ssdl-f"))
                { // ������ �� �������
                  xAtt->Ref = iTag->GetTagByUID(_UID(xAtt->Value));
                  if (!xAtt->Ref)
                   _MSG_ERR(FMT2(icsICConnectBrokenFuncRef,_UID(xAtt->Value),itTag->AV["uid"]),FMT(icsICCommonErrorCaption));
                }
               else
                _MSG_ERR(FMT1(icsICConnectBrokenDefRef,GUI),FMT(icsICCommonErrorCaption));
             }
            else
             _MSG_ERR(FMT1(icsICConnectBrokenDefRef,GUI),FMT(icsICCommonErrorCaption));
          }
         else
          {
            // ������ �� ������� � �������� ��������
            xAtt->Ref = RuleTag->GetTagByUID(_UID(xAtt->Value));
            if (!xAtt->Ref)
             {
               if (itTag->CmpName("tabarg"))
                 _MSG_ERR(FMT2(icsICConnectMissingRef1,_UID(xAtt->Value),itTag->GetParent()->GetParent()->AV["name"]),FMT(icsICCommonErrorCaption));
               else if (itTag->CmpName("rulelist"))
                 _MSG_ERR(FMT1(icsICConnectMissingRef2,_UID(xAtt->Value)),FMT(icsICCommonErrorCaption));
               else if (itTag->CmpName("rule"))
                 _MSG_ERR(FMT2(icsICConnectMissingRef3,_UID(xAtt->Value),itTag->AV["uid"]),FMT(icsICCommonErrorCaption));
               else if (itTag->CmpName("name"))
                 _MSG_ERR(FMT2(icsICConnectMissingRef4,itTag->AV["name"],_UID(xAtt->Value)),FMT(icsICCommonErrorCaption));
               else if (itTag->CmpName("call"))
                 _MSG_ERR(FMT1(icsICConnectMissingRef5,_UID(xAtt->Value)),FMT(icsICCommonErrorCaption));
               else
                 _MSG_ERR(FMT1(icsICConnectMissingRef6,_UID(xAtt->Value)),FMT(icsICCommonErrorCaption));
             }
          }
       }
    }
  return false;
}
//---------------------------------------------------------------------------
*/
void __fastcall TConnectForm::NumTag(TTagNode *ANode, AnsiString Num)
{
   TTagNode *itTag;
   int i = 1;
   if (ANode->CmpName("inquirer,group"))
    {
      itTag = ANode->GetFirstChild();
      while (itTag)
       {
         if (itTag->CmpName("group,question"))
          if (Num.Length()) NumTag(itTag, Num+"."+IntToStr(i++));
          else              NumTag(itTag, IntToStr(i++));
         itTag = itTag->GetNext();
       }
      if (ANode->CmpName("inquirer"))
       {
         PrBar->Position += sStep;
         Application->ProcessMessages();
       }
      if (ANode->CmpName("group"))
       ANode->AV["number"] = Num;
    }
   else if (ANode->CmpName("question"))
    {
      ANode->AV["number"] = Num;
    }
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::ConCallBack(int APercent,AnsiString Mess)
{
  if (APercent == 0)
   {
     PrBar->Properties->Max = 100;
     MsgLab->Caption = Mess;
     MsgLab->Update();
     Application->ProcessMessages();
   }
  PrBar->Position = APercent;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::LoadXMLs(TTagNode *itTag, AnsiString &UID)
{
   if (itTag->CmpName("project_main"))
    {
      // �������� �������� �������� ��������� ������������
      InqXMLList->AddObject(itTag->AV["objref"],(TObject*)DM->XMLList->GetXML(itTag->AV["objref"]));
      // �������� �������� �������� ������ ��������� ������������
      InqXMLList->AddObject(itTag->AV["rulesref"],(TObject*)DM->XMLList->GetXML(itTag->AV["rulesref"]));
      if (itTag->AV["funcref"].Length() == 22)
       {
         // �������� �������� ������� ���� ������� ����
         InqXMLList->AddObject(itTag->AV["funcref"],(TObject*)DM->XMLList->GetXML(itTag->AV["funcref"]));
       }
    }
   else if (itTag->CmpName("sub_project"))
    {
      // �������� �������� �������� ��������������� ������������
      InqXMLList->AddObject(itTag->AV["objref"],(TObject*)DM->XMLList->GetXML(itTag->AV["objref"]));
      if (itTag->AV["module_type"].UpperCase()=="SSDL")
       {
          // �������� �������� �������� ������ ��������������� ������������
          InqXMLList->AddObject(itTag->AV["rulesref"],(TObject*)DM->XMLList->GetXML(itTag->AV["rulesref"]));
       }
      else
       {
         if (itTag->AV["rulesref"].Length() == 22)
          {
             // ��� ������� � ����� add_ssdl
             // �������� �������� �������� ������ ��������������� ������������ ���� ������� ����
             InqXMLList->AddObject(itTag->AV["rulesref"],(TObject*)DM->XMLList->GetXML(itTag->AV["rulesref"]));
          }
       }
    }
   else if (itTag->CmpName("extdef"))
    {
      InqXMLList->AddObject(itTag->AV["gui"],(TObject*)DM->XMLList->GetXML(itTag->AV["gui"]));
    }
   else if (itTag->CmpName("report,sub_report,extdef,ssdl_project,passport"))
    {
       ;
    }
   else
    {
      _MSG_ERR(FMT1(icsICConnectErrorProjectElemType,itTag->Name),FMT(icsICCommonErrorCaption));
      UID = "-1";
      return true;
    }
   return false;
}
//---------------------------------------------------------------------------
/*
bool __fastcall TConnectForm::CreateSQLs(TTagNode *itTag, AnsiString &UID)
{
   if (itTag->GetAttrByName("ref")&&itTag->CmpName("name,case"))
    {
      if (itTag->GetAttrByName("ref")->Ref)
       {
         iTag = itTag->GetAttrByName("ref")->Ref;
         if (iTag->CmpName("question,out,distout"))
          {
            tmpTabName = GetFullTabName(iTag);
            if (tmpTables->IndexOf(tmpTabName) == -1)
             tmpTables->Add(tmpTabName);
            if (iTag->CmpName("distout"))
             tmpTabName = tmpTabName+".PD"+GetXMLProjectCode(iTag)+iTag->AV["uid"].UpperCase();
            else
             tmpTabName = tmpTabName+".P"+GetXMLProjectCode(iTag)+iTag->AV["uid"].UpperCase();
            if (tmpFields->IndexOf(tmpTabName) == -1)
             tmpFields->Add(tmpTabName);
          }
       }
    }
   PrBar->Position++;
   Application->ProcessMessages();
   return false;
}
//---------------------------------------------------------------------------
*/
void __fastcall TConnectForm::LoadLPUParam()
{
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::PrepareInqData(AnsiString DBName)
{
  bool FRC = true;
  return FRC;
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::LoadRegXML(TTagNode *ARegXML, TTagNode *ASelRegXML)
{
  ARegXML->Assign(DM->xmlRegDef,true);
  ASelRegXML->Assign(DM->xmlRegDef,true);
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::PrInit(int AMax, AnsiString AMsg)
{
  MsgLab->Caption = AMsg;
  MsgLab->Update();
  PrBar->Properties->Max = AMax;
  PrBar->Position = 0;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::PrCh(int APercent, AnsiString AMsg)
{
  if (AMsg.Length())
   {
     MsgLab->Caption = AMsg;
     MsgLab->Update();
   }
  if (APercent != -1)
   PrBar->Position += APercent;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::PrComp(AnsiString AMsg)
{
  MsgLab->Caption = "";
  MsgLab->Update();
  PrBar->Properties->Max = 0;
  PrBar->Position = 0;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::SetDelegateAttr(TTagNode *AConf, TTagNode *ADataDef)
{
   if (!AConf) return false;
   TTagNode *itNode = AConf->GetFirstChild();
   bool RC = false;
   while (itNode)
    {
      RC |= FSetDelegateAttr(itNode, ADataDef);
      itNode = itNode->GetNext();
    }
   return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TConnectForm::FSetDelegateAttr(TTagNode *AConf, TTagNode *ADataDef)
{
   TTagNode *tmp = ADataDef->GetTagByUID(AConf->AV["ref"]);
   bool RC = false;
   if (tmp)
    {
      RC = true;
      tmp->AV["isedit"] = "no";
      if (tmp->CmpAV("required","yes"))
       tmp->AV["default"] = "1";
      if (tmp->CmpName("class,unit") && tmp->AV["uid"].Length())
       {
         tmp = tmp->GetChildByName("description",true);
         tmp->AV["insertlabel"] = "";
         tmp->AV["deletelabel"] = "";
       }
    }
   TTagNode *itNode = AConf->GetFirstChild();
   while (itNode)
    {
      RC |= FSetDelegateAttr(itNode, ADataDef);
      itNode = itNode->GetNext();
    }
   return RC;
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::FormDestroy(TObject *Sender)
{
  if (InqXMLList) delete InqXMLList;
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::LoadIMMXML(TTagNode *AImmNode, TTagNode *ARegNode)
{
  AImmNode->Assign(DM->xmlImmCardDef,true);
  ARegNode->Assign(DM->xmlRegDef,true);
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::LoadArcIMMXML(TTagNode *AImmNode, TTagNode *ARegNode)
{
  AImmNode->Assign(DM->xmlImmCardDef,true);
  ARegNode->Assign(DM->xmlRegDef,true);
  TTagNode *tmp;
  TTagNode *itUnit = AImmNode->GetChildByName("unit");
  while (itUnit)
   {
     tmp = itUnit->GetChildByName("description");
     if (tmp)
      {
        tmp->AV["insertlabel"] = "";
        tmp->AV["editlabel"] = "";
        tmp->AV["deletelabel"] = "";
      }
     itUnit = itUnit->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall TConnectForm::SetDBParam(TpFIBDatabase *ADB, AnsiString ADBCharset,AnsiString ADBUser, AnsiString ADBPassword)
{
  ADB->DBParams->Clear();
  ADB->DBParams->Add(("user_name="+ADBUser.LowerCase()).c_str());
  ADB->DBParams->Add(("password="+ADBPassword.LowerCase()).c_str());
  ADB->DBParams->Add("sql_role_name=");
  if (ADBCharset.Length())
   ADB->DBParams->Add(("lc_ctype="+ADBCharset.LowerCase()).c_str());
}
//---------------------------------------------------------------------------

