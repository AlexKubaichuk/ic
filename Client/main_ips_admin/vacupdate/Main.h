//----------------------------------------------------------------------------
#ifndef MainH
#define MainH
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <ActnList.hpp>
#include <DBActns.hpp>
#include <StdActns.hpp>
#include "DMF.h"
#include "ICSAboutDialog.h"
#include <Dialogs.hpp>
#include "dxBar.hpp"
#include "cxGraphics.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxTextEdit.hpp"
#include "cxCheckListBox.hpp"
#include "cxButtons.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
//#include "ICSHelpCapture.h"
//----------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
        TcxPageControl *PC;
        TcxTabSheet *AddTS;
        TcxTabSheet *SetupTS;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TcxTextEdit *NameED;
        TcxTextEdit *FullNameED;
        TcxTextEdit *VacNameED;
        TcxCheckBox *BinaryReakChB;
        TcxCheckListBox *MIBPChLB;
        TcxCheckListBox *InfChLB;
        TcxCheckListBox *ReakTypeChLB;
        TcxComboBox *AddTypeCB;
        TcxButton *cxButton1;
        TcxButton *cxButton2;
        TcxButton *cxButton3;
        TcxButton *cxButton4;
        TcxCheckBox *CanApplyVacChB;
        TcxComboBox *VacCB;
        TcxMaskEdit *DoseED;
        TcxMaskEdit *ResistReakValED;
        TcxMaskEdit *Pause1ED;
        TcxMaskEdit *Pause2ED;
        TcxMaskEdit *Pause3ED;
        TActionList *MainAL;
        TAction *actGetClasses;
        TAction *actSetDBPath;
        TAction *actAbout;
        TAction *Action1;
        TAction *actAdd;
        TICSAboutDialog *AboutDlg;
        TcxImageList *MainIL;
        TcxMaskEdit *VerED;
        TLabel *Label13;
        TLabel *Label14;
        TcxTextEdit *UpdateNameED;
        TcxButton *cxButton5;
        TAction *actCreateSetup;
        TcxComboBox *CheckVacCB;
        TLabel *Label15;
        TcxComboBox *CheckTestCB;
        TLabel *Label16;
        TcxCheckBox *UpdVacChB;
        TcxCheckBox *UpdTestChB;
        TcxProgressBar *PrBar;
        TcxCheckBox *VacSchUpdateChB;
        TcxCheckBox *TestSchUpdateChB;
        TLabel *Label17;
        TcxTextEdit *OutFNED;
        TcxTabSheet *cxTabSheet1;
        TcxCheckListBox *VacSchList;
        TcxButton *cxButton6;
        TAction *actSaveDefSch;
        TMemo *ExtScript;
        TLabel *Label18;
    void __fastcall FormShow(TObject *Sender);
    bool __fastcall CheckConnect();
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall actGetInfClassExecute(TObject *Sender);
        void __fastcall actAboutExecute(TObject *Sender);
        void __fastcall actSetDBPathExecute(TObject *Sender);
        void __fastcall Action1Execute(TObject *Sender);
        void __fastcall BinaryReakChBPropertiesChange(TObject *Sender);
        void __fastcall AddTypeCBPropertiesChange(TObject *Sender);
        void __fastcall actAddExecute(TObject *Sender);
        void __fastcall actCreateSetupExecute(TObject *Sender);
        void __fastcall UpdVacChBClick(TObject *Sender);
        void __fastcall UpdTestChBClick(TObject *Sender);
        void __fastcall actSaveDefSchExecute(TObject *Sender);
private:
    TAnsiStrMap FFLList;
    TAnsiStrMap FFLTypes;
    void __fastcall FillChLB();
    void __fastcall AddVac();
    void __fastcall AddTest();
    bool __fastcall CheckInput();
    void __fastcall ClearFields();
    void __fastcall FUpdateGen(const AnsiString &AUID);
    void __fastcall FSetQuery(const AnsiString &AVal);
    int  __fastcall CheckedItems(TcxCheckListBox *ASrc);

    bool __fastcall CheckSetupInput();
    AnsiString __fastcall DisableTriggers(const AnsiString &AClsID);
    AnsiString __fastcall EnableTriggers(const AnsiString &AClsID);
    AnsiString __fastcall AddDeleteScript(const AnsiString &AClsID);
public:
    virtual __fastcall TMainForm(TComponent *Owner);
};
//----------------------------------------------------------------------------
extern TMainForm *MainForm;
//----------------------------------------------------------------------------
#endif

