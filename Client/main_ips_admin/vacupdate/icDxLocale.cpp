//---------------------------------------------------------------------------


#pragma hdrstop

#include "icDxLocale.h"

#include "cxEditConsts.hpp"
#include "cxExtEditConsts.hpp"
#include "cxFilterConsts.hpp"
#include "cxGridPopupMenuConsts.hpp"
#include "cxPCConsts.hpp"
#include "dxDockConsts.hpp"
#include "dxThemeConsts.hpp"
#include "dxExtCtrlsStrs.hpp"
#include "dxPSRes.hpp"
#include "dxBarStrs.hpp"
#include "cxFilterControlStrs.hpp"
#include "cxGridStrs.hpp"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
void __fastcall icDxLocale()
{
//#include "cxEditConsts.hpp"
   cxSetResourceString(&_cxSBlobButtonCancel,"&������");
   cxSetResourceString(&_cxSBlobButtonClose,"&�������");
//   cxSetResourceString(&_cxSBlobButtonOK,"&OK");
//   cxSetResourceString(&_cxSBlobMemo,"(MEMO)");
//   cxSetResourceString(&_cxSBlobMemoEmpty,"(memo)");
//   cxSetResourceString(&_cxSBlobPicture,"(PICTURE)");
//   cxSetResourceString(&_cxSBlobPictureEmpty,"(picture)");
//   cxSetResourceString(&_cxSDateBOM,"bom");
//   cxSetResourceString(&_cxSDateEOM,"eom");
   cxSetResourceString(&_cxSDateError,"������������ ����");
//   cxSetResourceString(&_cxSDateFifth,"fifth");
//   cxSetResourceString(&_cxSDateFirst,"first");
//   cxSetResourceString(&_cxSDateFourth,"fourth");
//   cxSetResourceString(&_cxSDateFriday,"Friday");
//   cxSetResourceString(&_cxSDateMonday,"Monday");
//   cxSetResourceString(&_cxSDateNow,"now");
   cxSetResourceString(&_cxSDatePopupClear,"��������");
   cxSetResourceString(&_cxSDatePopupToday,"�������");
//   cxSetResourceString(&_cxSDateSaturday,"Saturday");
//   cxSetResourceString(&_cxSDateSecond,"second");
//   cxSetResourceString(&_cxSDateSeventh,"seventh");
//   cxSetResourceString(&_cxSDateSixth,"sixth");
//   cxSetResourceString(&_cxSDateSunday,"Sunday");
//   cxSetResourceString(&_cxSDateThird,"third");
//   cxSetResourceString(&_cxSDateThursday,"Thursday");
//   cxSetResourceString(&_cxSDateToday,"today");
//   cxSetResourceString(&_cxSDateTomorrow,"tomorrow");
//   cxSetResourceString(&_cxSDateTuesday,"Tuesday");
//   cxSetResourceString(&_cxSDateWednesday,"Wednesday");
//   cxSetResourceString(&_cxSDateYesterday,"yesterday");

//   cxSetResourceString(&_cxSEditCheckBoxChecked,"True");
//   cxSetResourceString(&_cxSEditCheckBoxGrayed,"");
//   cxSetResourceString(&_cxSEditCheckBoxUnchecked,"False");
//   cxSetResourceString(&_cxSEditInvalidRepositoryItem,"The repository item is not acceptable");
//   cxSetResourceString(&_cxSEditNumericValueConvertError,"Could not convert to numeric value");
//   cxSetResourceString(&_cxSEditPopupCircularReferencingError,"Circular referencing is not allowed");
//   cxSetResourceString(&_cxSEditTimeConvertError,"Could not convert to time");
//   cxSetResourceString(&_cxSEditValidateErrorText,"Invalid input value. Use escape key to abandon changes");
//   cxSetResourceString(&_cxSEditValueOutOfBounds,"Value out of bounds");
   cxSetResourceString(&_cxSMenuItemCaptionCopy,"&����������");
   cxSetResourceString(&_cxSMenuItemCaptionCut,"&��������");
   cxSetResourceString(&_cxSMenuItemCaptionDelete,"&�������");
   cxSetResourceString(&_cxSMenuItemCaptionLoad,"&��������...");
   cxSetResourceString(&_cxSMenuItemCaptionPaste,"���&�����");
   cxSetResourceString(&_cxSMenuItemCaptionSave,"��������� &���...");
//   cxSetResourceString(&_cxSRadioGroupDefaultCaption,"");
//   cxSetResourceString(&_scxMaskEditEmptyMaskCollectionFile,"The mask collection file is empty");
//   cxSetResourceString(&_scxMaskEditIllegalFileFormat,"Illegal file format");
//   cxSetResourceString(&_scxMaskEditInvalidEditValue,"The edit value is invalid");
//   cxSetResourceString(&_scxMaskEditMaskCollectionFiles,"Mask collection files");
//   cxSetResourceString(&_scxMaskEditNoMask,"None");
//   cxSetResourceString(&_scxMaskEditRegExprError,"Regular expression errors:");
//   cxSetResourceString(&_scxRegExprCantCreateEmptyAlt,"The alternative should not be empty");
//   cxSetResourceString(&_scxRegExprCantCreateEmptyBlock,"The block should not be empty");
//   cxSetResourceString(&_scxRegExprCantCreateEmptyEnum,"Can't create empty enumeration");
//   cxSetResourceString(&_scxRegExprCantUsePlusQuantifier,"The '+' quantifier cannot be applied here");
//   cxSetResourceString(&_scxRegExprCantUseStarQuantifier,"The '*' quantifier cannot be applied here");
//   cxSetResourceString(&_scxRegExprChar,"Char");
//   cxSetResourceString(&_scxRegExprEmptySourceStream,"The source stream is empty");
//   cxSetResourceString(&_scxRegExprHexNumberExpected,"Hexadecimal number expected but '%s' found");
//   cxSetResourceString(&_scxRegExprHexNumberExpected0,"Hexadecimal number expected");
//   cxSetResourceString(&_scxRegExprIllegalQuantifier,"Illegal quantifier '%s'");
//   cxSetResourceString(&_scxRegExprIllegalSymbol,"Illegal '%s'");
//   cxSetResourceString(&_scxRegExprIncorrectSpace,"The space character is not allowed after '\\'");
//   cxSetResourceString(&_scxRegExprLine,"Line");
//   cxSetResourceString(&_scxRegExprMissing,"Missing '%s'");
//   cxSetResourceString(&_scxRegExprNotSupportQuantifier,"The parameter quantifiers are not supported");
//   cxSetResourceString(&_scxRegExprSubrangeOrder,"The starting character of the subrange must be less than the finishing one");
//   cxSetResourceString(&_scxRegExprTooBigReferenceNumber,"Too big reference number");
//   cxSetResourceString(&_scxRegExprUnnecessary,"Unnecessary '%s'");
//   cxSetResourceString(&_scxSHyperLinkPrefix,"http://");
//   cxSetResourceString(&_scxSCalcError,"Error");

//#include "cxFilterConsts.hpp"
//   cxSetResourceString(&_cxSFilterAddCondition,"�������� &�������");
//   cxSetResourceString(&_cxSFilterAddGroup,"�������� &Group");
//   cxSetResourceString(&_cxSFilterBoolOperatorAnd,"AND");
//   cxSetResourceString(&_cxSFilterBoolOperatorNotAnd,"NOT AND");
//   cxSetResourceString(&_cxSFilterBoolOperatorNotOr,"NOT OR");
//   cxSetResourceString(&_cxSFilterBoolOperatorOr,"OR");
//   cxSetResourceString(&_cxSFilterClearAll,"Clear &All");
//   cxSetResourceString(&_cxSFilterControlDialogActionApplyCaption,"&���������");//   cxSetResourceString(&_cxSFilterControlDialogActionCancelCaption,"������");//   cxSetResourceString(&_cxSFilterControlDialogActionOkCaption,"Ok");//   cxSetResourceString(&_cxSFilterControlDialogActionOpenCaption,"&�������...");//   cxSetResourceString(&_cxSFilterControlDialogActionSaveCaption,"&��������� ���...");//   cxSetResourceString(&_cxSFilterControlDialogCaption,"Filter builder");
//   cxSetResourceString(&_cxSFilterControlDialogFileExt,"flt");
//   cxSetResourceString(&_cxSFilterControlDialogFileFilter,"Filters (*.flt)|*.flt'");
//   cxSetResourceString(&_cxSFilterControlDialogNewFile,"�����.flt");
//   cxSetResourceString(&_cxSFilterControlDialogOpenDialogCaption,"Open an existing filter");//   cxSetResourceString(&_cxSFilterControlDialogSaveDialogCaption,"Save the active filter to file");//   cxSetResourceString(&_cxSFilterControlNullString,"<empty>");
//   cxSetResourceString(&_cxSFilterDialogCaption,"Custom Filter");
//   cxSetResourceString(&_cxSFilterDialogCharactersSeries,"to represent any series of characters");
//   cxSetResourceString(&_cxSFilterDialogInvalidValue,"Invalid value");
//   cxSetResourceString(&_cxSFilterDialogOperationAnd,"AND");
//   cxSetResourceString(&_cxSFilterDialogOperationOr,"OR");
//   cxSetResourceString(&_cxSFilterDialogRows,"Show rows where:");
//   cxSetResourceString(&_cxSFilterDialogSingleCharacter,"to represent any single character");
//   cxSetResourceString(&_cxSFilterDialogUse,"Use");
//   cxSetResourceString(&_cxSFilterErrorBuilding,"Can''t build filter from source");
//   cxSetResourceString(&_cxSFilterFooterAddCondition,"press button to add new condition");
//   cxSetResourceString(&_cxSFilterGroupCaption,"applies to the following conditions");
//   cxSetResourceString(&_cxSFilterRemoveRow,"&Remove Row");
//   cxSetResourceString(&_cxSFilterRootButtonCaption,"Filter");
//   cxSetResourceString(&_cxSFilterRootGroupCaption,"<root>");

  cxSetResourceString(&_dxSBAR_LOOKUPDIALOGCAPTION,"Select value");
  cxSetResourceString(&_dxSBAR_LOOKUPDIALOGOK,"���������");
  cxSetResourceString(&_dxSBAR_LOOKUPDIALOGCANCEL,"������");

  cxSetResourceString(&_dxSBAR_DIALOGOK,"���������");
  cxSetResourceString(&_dxSBAR_DIALOGCANCEL,"������");
  cxSetResourceString(&_dxSBAR_COLOR_STR_0,"Black");
  cxSetResourceString(&_dxSBAR_COLOR_STR_1,"Maroon");
  cxSetResourceString(&_dxSBAR_COLOR_STR_2,"Green");
  cxSetResourceString(&_dxSBAR_COLOR_STR_3,"Olive");
  cxSetResourceString(&_dxSBAR_COLOR_STR_4,"Navy");
  cxSetResourceString(&_dxSBAR_COLOR_STR_5,"Purple");
  cxSetResourceString(&_dxSBAR_COLOR_STR_6,"Teal");
  cxSetResourceString(&_dxSBAR_COLOR_STR_7,"Gray");
  cxSetResourceString(&_dxSBAR_COLOR_STR_8,"Silver");
  cxSetResourceString(&_dxSBAR_COLOR_STR_9,"Red");
  cxSetResourceString(&_dxSBAR_COLOR_STR_10,"Lime");
  cxSetResourceString(&_dxSBAR_COLOR_STR_11,"Yellow");
  cxSetResourceString(&_dxSBAR_COLOR_STR_12,"Blue");
  cxSetResourceString(&_dxSBAR_COLOR_STR_13,"Fuchsia");
  cxSetResourceString(&_dxSBAR_COLOR_STR_14,"Aqua");
  cxSetResourceString(&_dxSBAR_COLOR_STR_15,"White");
  cxSetResourceString(&_dxSBAR_COLORAUTOTEXT,"(automatic)");
  cxSetResourceString(&_dxSBAR_COLORCUSTOMTEXT,"(custom)");
  cxSetResourceString(&_dxSBAR_DATETODAY,"Today");
  cxSetResourceString(&_dxSBAR_DATECLEAR,"Clear");
  cxSetResourceString(&_dxSBAR_DATEDIALOGCAPTION,"Select the date");
  cxSetResourceString(&_dxSBAR_TREEVIEWDIALOGCAPTION,"Select item");
  cxSetResourceString(&_dxSBAR_IMAGEDIALOGCAPTION,"Select item");
  cxSetResourceString(&_dxSBAR_IMAGEINDEX,"Image Index");
  cxSetResourceString(&_dxSBAR_IMAGETEXT,"Text");
  cxSetResourceString(&_dxSBAR_PLACEFORCONTROL,"The place for the ");
  cxSetResourceString(&_dxSBAR_CANTASSIGNCONTROL,"You cannot assign the same control to more than one TdxBarControlContainerItem.");

  cxSetResourceString(&_dxSBAR_WANTTORESETTOOLBAR,"Are you sure you want to reset the changes made to the ''%s'' toolbar?");
  cxSetResourceString(&_dxSBAR_WANTTORESETUSAGEDATA,"This will delete the record of the commands you''ve used in this application and restore the default set of visible commands to the menus and toolbars. It will not undo any explicit customizations.   Are you sure you want to proceed?");
  cxSetResourceString(&_dxSBAR_BARMANAGERMORETHENONE ,"A Form should contain only a single TdxBarManager");
  cxSetResourceString(&_dxSBAR_BARMANAGERBADOWNER,"TdxBarManager should have as its Owner - TForm (TCustomForm)");
  cxSetResourceString(&_dxSBAR_NOBARMANAGERS,"There are no TdxBarManagers available");
  cxSetResourceString(&_dxSBAR_WANTTODELETETOOLBAR,"Are you sure you want to delete the ''%s'' toolbar?");
  cxSetResourceString(&_dxSBAR_WANTTODELETECATEGORY,"Are you sure you want to delete the ''%s'' category?");
  cxSetResourceString(&_dxSBAR_WANTTOCLEARCOMMANDS,"Are you sure you want to delete all commands in the ''%s'' category?");
  cxSetResourceString(&_dxSBAR_RECURSIVESUBITEMS,"You cannot create recursive subitems");
  cxSetResourceString(&_dxSBAR_COMMANDNAMECANNOTBEBLANK,"A command name cannot be blank. Please enter a name.");
  cxSetResourceString(&_dxSBAR_TOOLBAREXISTS,"A toolbar named ''%s'' already exists. Type another name.");
  cxSetResourceString(&_dxSBAR_RECURSIVEGROUPS,"You cannot create recursive groups");

  cxSetResourceString(&_dxSBAR_DEFAULTCATEGORYNAME,"Default");

  cxSetResourceString(&_dxSBAR_CP_ADDSUBITEM,"Add &SubItem");
  cxSetResourceString(&_dxSBAR_CP_ADDBUTTON,"Add &Button");
  cxSetResourceString(&_dxSBAR_CP_ADDITEM,"Add &Item");
  cxSetResourceString(&_dxSBAR_CP_DELETEBAR,"Delete Bar");

  cxSetResourceString(&_dxSBAR_CP_RESET,"&Reset");
  cxSetResourceString(&_dxSBAR_CP_DELETE,"&Delete");
  cxSetResourceString(&_dxSBAR_CP_NAME,"&Name:");
  cxSetResourceString(&_dxSBAR_CP_CAPTION,"&Caption:");
  cxSetResourceString(&_dxSBAR_CP_DEFAULTSTYLE,"Defa&ult style");
  cxSetResourceString(&_dxSBAR_CP_TEXTONLYALWAYS,"&Text Only (Always)");
  cxSetResourceString(&_dxSBAR_CP_TEXTONLYINMENUS,"Text &Only (in Menus)");
  cxSetResourceString(&_dxSBAR_CP_IMAGEANDTEXT,"Image &and Text");
  cxSetResourceString(&_dxSBAR_CP_BEGINAGROUP,"Begin a &Group");
  cxSetResourceString(&_dxSBAR_CP_VISIBLE,"&Visible");
  cxSetResourceString(&_dxSBAR_CP_MOSTRECENTLYUSED,"&Most recently used");

  cxSetResourceString(&_dxSBAR_ADDEX,"Add...");
  cxSetResourceString(&_dxSBAR_RENAMEEX,"Rename...");
  cxSetResourceString(&_dxSBAR_DELETE,"Delete");
  cxSetResourceString(&_dxSBAR_CLEAR,"Clear");
  cxSetResourceString(&_dxSBAR_VISIBLE,"Visible");
  cxSetResourceString(&_dxSBAR_OK,"���������");
  cxSetResourceString(&_dxSBAR_CANCEL,"������");
  cxSetResourceString(&_dxSBAR_SUBMENUEDITOR,"SubMenu Editor...");
  cxSetResourceString(&_dxSBAR_SUBMENUEDITORCAPTION,"ExpressBars SubMenu Editor");
  cxSetResourceString(&_dxSBAR_INSERTEX,"Insert...");

  cxSetResourceString(&_dxSBAR_MOVEUP,"Move Up");
  cxSetResourceString(&_dxSBAR_MOVEDOWN,"Move Down");
  cxSetResourceString(&_dxSBAR_POPUPMENUEDITOR,"PopupMenu Editor...");
  cxSetResourceString(&_dxSBAR_TABSHEET1," ������ &������������ ");
  cxSetResourceString(&_dxSBAR_TABSHEET2," &������� ");
  cxSetResourceString(&_dxSBAR_TABSHEET3," �&�������� ");
  cxSetResourceString(&_dxSBAR_TOOLBARS,"������ &������������:");
  cxSetResourceString(&_dxSBAR_TNEW,"&�����...");
  cxSetResourceString(&_dxSBAR_TRENAME,"�������������...");
  cxSetResourceString(&_dxSBAR_TDELETE,"&�������");
  cxSetResourceString(&_dxSBAR_TRESET,"&�����...");
  cxSetResourceString(&_dxSBAR_CLOSE,"�������");
  cxSetResourceString(&_dxSBAR_CAPTION,"���������");
  cxSetResourceString(&_dxSBAR_CATEGORIES,"���������:");
  cxSetResourceString(&_dxSBAR_COMMANDS,"�������:");
  cxSetResourceString(&_dxSBAR_DESCRIPTION,"��������  ");

  cxSetResourceString(&_dxSBAR_CUSTOMIZE,"&���������...");
  cxSetResourceString(&_dxSBAR_ADDREMOVEBUTTONS,"&�������� ��� ������� ������");
  cxSetResourceString(&_dxSBAR_MOREBUTTONS,"More Buttons");
  cxSetResourceString(&_dxSBAR_RESETTOOLBAR,"�&����");
  cxSetResourceString(&_dxSBAR_EXPAND,"���������� (Ctrl-Down)");
  cxSetResourceString(&_dxSBAR_DRAGTOMAKEMENUFLOAT,"Drag to make this menu float");

  cxSetResourceString(&_dxSBAR_TOOLBARNEWNAME ,"Custom ");
  cxSetResourceString(&_dxSBAR_CATEGORYADD ,"Add Category");
  cxSetResourceString(&_dxSBAR_CATEGORYINSERT ,"Insert Category");
  cxSetResourceString(&_dxSBAR_CATEGORYRENAME ,"Rename Category");
  cxSetResourceString(&_dxSBAR_TOOLBARADD ,"Add Toolbar");
  cxSetResourceString(&_dxSBAR_TOOLBARRENAME ,"Rename Toolbar");
  cxSetResourceString(&_dxSBAR_CATEGORYNAME ,"&Category name:");
  cxSetResourceString(&_dxSBAR_TOOLBARNAME ,"&Toolbar name:");
  cxSetResourceString(&_dxSBAR_CUSTOMIZINGFORM,"Customization Form...");

  cxSetResourceString(&_dxSBAR_MODIFY,"... modify");
  cxSetResourceString(&_dxSBAR_PERSMENUSANDTOOLBARS,"Personalized Menus and Toolbars  ");
  cxSetResourceString(&_dxSBAR_MENUSSHOWRECENTITEMS,"Me&nus show recently used commands first");
  cxSetResourceString(&_dxSBAR_SHOWFULLMENUSAFTERDELAY,"Show f&ull menus after a short delay");
  cxSetResourceString(&_dxSBAR_RESETUSAGEDATA,"&Reset my usage data");

  cxSetResourceString(&_dxSBAR_OTHEROPTIONS,"Other  ");
  cxSetResourceString(&_dxSBAR_LARGEICONS,"&Large icons");
  cxSetResourceString(&_dxSBAR_HINTOPT1,"Show Tool&Tips on toolbars");
  cxSetResourceString(&_dxSBAR_HINTOPT2,"Show s&hortcut keys in ToolTips");
  cxSetResourceString(&_dxSBAR_MENUANIMATIONS,"&Menu animations:");
  cxSetResourceString(&_dxSBAR_MENUANIM1,"(None)");
  cxSetResourceString(&_dxSBAR_MENUANIM2,"Random");
  cxSetResourceString(&_dxSBAR_MENUANIM3,"Unfold");
  cxSetResourceString(&_dxSBAR_MENUANIM4,"Slide");
  cxSetResourceString(&_dxSBAR_MENUANIM5,"Fade");
  cxSetResourceString(&_scxGridNoDataInfoText,"");

  cxSetResourceString(&_scxGridRecursiveLevels,"�� �� ������ ��������� ����������� ������.");
  cxSetResourceString(&_scxGridDeletingConfirmationCaption,"�������������"); //Confirm
  cxSetResourceString(&_scxGridDeletingFocusedConfirmationText,"������� ������?"); //Delete record?
  cxSetResourceString(&_scxGridDeletingSelectedConfirmationText,"������� ��� ���������� ������?");
  cxSetResourceString(&_scxGridNoDataInfoText,"<����������� ������ ��� �����������>");
  cxSetResourceString(&_scxGridNewItemRowInfoText,"������� ��� ���������� ������");
  cxSetResourceString(&_scxGridFilterIsEmpty,"<������ ������>");
  cxSetResourceString(&_scxGridCustomizationFormCaption,"���������");
  cxSetResourceString(&_scxGridCustomizationFormColumnsPageCaption,"�������");
  cxSetResourceString(&_scxGridGroupByBoxCaption,"���������� ��������� ������� ��� ����������� �� ������ �������");
  cxSetResourceString(&_scxGridFilterCustomizeButtonCaption,"���������...");
  cxSetResourceString(&_scxGridColumnsQuickCustomizationHint,"������� ��� ������ ������������ �������");
  cxSetResourceString(&_scxGridCustomizationFormBandsPageCaption,"Bands");
  cxSetResourceString(&_scxGridBandsQuickCustomizationHint,"Click here to select visible bands");
  cxSetResourceString(&_scxGridCustomizationFormRowsPageCaption,"������");
  cxSetResourceString(&_scxGridConverterIntermediaryMissing,"Missing an intermediary component!\nPlease add a %s component to the form.");
  cxSetResourceString(&_scxGridConverterNotExistGrid,"cxGrid does not exist");
  cxSetResourceString(&_scxGridConverterNotExistComponent,"Component does not exist");
  cxSetResourceString(&_scxImportErrorCaption,"Import error");
  cxSetResourceString(&_scxNotExistGridView,"Grid view does not exist");
  cxSetResourceString(&_scxNotExistGridLevel,"Active grid level does not exist");
  cxSetResourceString(&_scxCantCreateExportOutputFile,"Can't create the export output file");
  cxSetResourceString(&_cxSEditRepositoryExtLookupComboBoxItem,"ExtLookupComboBox|Represents an ultra-advanced lookup using the QuantumGrid as its drop down control");
  cxSetResourceString(&_scxGridChartValueHintFormat,"%s for %s is %s");  // series display text, category, value
  // base operators                      
  cxSetResourceString(&_cxSFilterOperatorEqual ,"�����");
  cxSetResourceString(&_cxSFilterOperatorNotEqual ,"�� �����");
  cxSetResourceString(&_cxSFilterOperatorLess ,"������");
  cxSetResourceString(&_cxSFilterOperatorLessEqual ,"������ ��� �����");
  cxSetResourceString(&_cxSFilterOperatorGreater ,"������");
  cxSetResourceString(&_cxSFilterOperatorGreaterEqual ,"������ ��� �����");
  cxSetResourceString(&_cxSFilterOperatorLike ,"��������");
  cxSetResourceString(&_cxSFilterOperatorNotLike ,"�� ��������");
  cxSetResourceString(&_cxSFilterOperatorBetween ,"�����");
  cxSetResourceString(&_cxSFilterOperatorNotBetween ,"�� �����");
  cxSetResourceString(&_cxSFilterOperatorInList ,"������ � ");
  cxSetResourceString(&_cxSFilterOperatorNotInList ,"�� ������ � ");

  cxSetResourceString(&_cxSFilterOperatorYesterday ,"�����");
  cxSetResourceString(&_cxSFilterOperatorToday ,"�������");
  cxSetResourceString(&_cxSFilterOperatorTomorrow ,"������");

  cxSetResourceString(&_cxSFilterOperatorLast7Days ,"��������� 7 ����");
  cxSetResourceString(&_cxSFilterOperatorLastWeek ,"��������� ������");
  cxSetResourceString(&_cxSFilterOperatorLast14Days ,"��������� 14 ����");
  cxSetResourceString(&_cxSFilterOperatorLastTwoWeeks ,"��������� 2 ������");
  cxSetResourceString(&_cxSFilterOperatorLast30Days ,"��������� 30 ����");
  cxSetResourceString(&_cxSFilterOperatorLastMonth ,"��������� �����");
  cxSetResourceString(&_cxSFilterOperatorLastYear ,"��������� ���");
  cxSetResourceString(&_cxSFilterOperatorPast ,"�����");

  cxSetResourceString(&_cxSFilterOperatorThisWeek ,"������");
  cxSetResourceString(&_cxSFilterOperatorThisMonth ,"�����");
  cxSetResourceString(&_cxSFilterOperatorThisYear ,"���");

  cxSetResourceString(&_cxSFilterOperatorNext7Days ,"��������� 7 ����");
  cxSetResourceString(&_cxSFilterOperatorNextWeek ,"��������� ������");
  cxSetResourceString(&_cxSFilterOperatorNext14Days ,"��������� 14 ����");
  cxSetResourceString(&_cxSFilterOperatorNextTwoWeeks ,"��������� 2 ������");
  cxSetResourceString(&_cxSFilterOperatorNext30Days ,"��������� 30 ����");
  cxSetResourceString(&_cxSFilterOperatorNextMonth ,"��������� �����");
  cxSetResourceString(&_cxSFilterOperatorNextYear ,"��������� ���");
  cxSetResourceString(&_cxSFilterOperatorFuture ,"� �������");

  cxSetResourceString(&_cxSFilterAndCaption ,"�");
  cxSetResourceString(&_cxSFilterOrCaption ,"���");
  cxSetResourceString(&_cxSFilterNotCaption ,"��");
  cxSetResourceString(&_cxSFilterBlankCaption ,"�����");

  // derived
  cxSetResourceString(&_cxSFilterOperatorIsNull ,"�����");
  cxSetResourceString(&_cxSFilterOperatorIsNotNull ,"�� �����");
  cxSetResourceString(&_cxSFilterOperatorBeginsWith ,"���������� �");
  cxSetResourceString(&_cxSFilterOperatorDoesNotBeginWith ,"�� ���������� �");
  cxSetResourceString(&_cxSFilterOperatorEndsWith ,"������������ ��");
  cxSetResourceString(&_cxSFilterOperatorDoesNotEndWith ,"�� ������������ ��");
  cxSetResourceString(&_cxSFilterOperatorContains ,"��������");
  cxSetResourceString(&_cxSFilterOperatorDoesNotContain ,"�� ��������");
  // filter listbox's values
  cxSetResourceString(&_cxSFilterBoxAllCaption ,"(���)");
  cxSetResourceString(&_cxSFilterBoxCustomCaption ,"(���������...)");
  cxSetResourceString(&_cxSFilterBoxBlanksCaption ,"(������)");
  cxSetResourceString(&_cxSFilterBoxNonBlanksCaption ,"(��������)");

  // cxFilterBoolOperator
  cxSetResourceString(&_cxSFilterBoolOperatorAnd,"�");        // all
  cxSetResourceString(&_cxSFilterBoolOperatorOr,"���");          // any
  cxSetResourceString(&_cxSFilterBoolOperatorNotAnd,"� ��"); // not all
  cxSetResourceString(&_cxSFilterBoolOperatorNotOr,"��� ��");   // not any
  //
  cxSetResourceString(&_cxSFilterRootButtonCaption,"������");
  cxSetResourceString(&_cxSFilterAddCondition,"�������� &�������");
  cxSetResourceString(&_cxSFilterAddGroup,"�������� &������");
  cxSetResourceString(&_cxSFilterRemoveRow,"&������� ������");
  cxSetResourceString(&_cxSFilterClearAll,"�������� &��");
  cxSetResourceString(&_cxSFilterFooterAddCondition,"������� ������ ��� ���������� ������ �������");

  cxSetResourceString(&_cxSFilterGroupCaption,"����������� � ��������� ��������");
  cxSetResourceString(&_cxSFilterRootGroupCaption,"<������>");
  cxSetResourceString(&_cxSFilterControlNullString,"<�����>");

  cxSetResourceString(&_cxSFilterErrorBuilding,"���������� ������� ������ �� ���������");

  //FilterDialog
  cxSetResourceString(&_cxSFilterDialogCaption,"Custom Filter");
  cxSetResourceString(&_cxSFilterDialogInvalidValue,"��������� ��������");
  cxSetResourceString(&_cxSFilterDialogUse,"�����������");
  cxSetResourceString(&_cxSFilterDialogSingleCharacter,"������ ������ ���������� �������");
  cxSetResourceString(&_cxSFilterDialogCharactersSeries,"������ ����� ������������������ ��������");
  cxSetResourceString(&_cxSFilterDialogOperationAnd,"�");
  cxSetResourceString(&_cxSFilterDialogOperationOr,"���");
  cxSetResourceString(&_cxSFilterDialogRows,"���������� ������ ��������������� �������:");

  // FilterControlDialog
  cxSetResourceString(&_cxSFilterControlDialogCaption,"�������� �������");
  cxSetResourceString(&_cxSFilterControlDialogNewFile,"�����.flt");
  cxSetResourceString(&_cxSFilterControlDialogOpenDialogCaption,"������� ������");
  cxSetResourceString(&_cxSFilterControlDialogSaveDialogCaption,"��������� ������� ������ � ����");
  cxSetResourceString(&_cxSFilterControlDialogActionSaveCaption,"&��������� ���...");
  cxSetResourceString(&_cxSFilterControlDialogActionOpenCaption,"&�������...");
  cxSetResourceString(&_cxSFilterControlDialogActionApplyCaption,"&���������");
  cxSetResourceString(&_cxSFilterControlDialogActionOkCaption,"�������");
  cxSetResourceString(&_cxSFilterControlDialogActionCancelCaption,"������");
  cxSetResourceString(&_cxSFilterControlDialogFileExt,"flt");
  cxSetResourceString(&_cxSFilterControlDialogFileFilter,"������� (*.flt)|*.flt");
/*
                        = ' ,"������� (*.flt)|*.flt");


  cxSetResourceString(&_sdxAutoColorText ,"");  //Auto';
  cxSetResourceString(&_sdxCustomColorText ,"");  //Custom...';

  cxSetResourceString(&_sdxSysColorScrollBar ,"");  //ScrollBar';
  cxSetResourceString(&_sdxSysColorBackground ,"");  //Background';
  cxSetResourceString(&_sdxSysColorActiveCaption ,"");  //Active Caption';
  cxSetResourceString(&_sdxSysColorInactiveCaption ,"");  //Inactive Caption';
  cxSetResourceString(&_sdxSysColorMenu ,"");  //Menu';
  cxSetResourceString(&_sdxSysColorWindow ,"");  //Window';
  cxSetResourceString(&_sdxSysColorWindowFrame ,"");  //Window Frame';
  cxSetResourceString(&_sdxSysColorMenuText ,"");  //Menu Text';
  cxSetResourceString(&_sdxSysColorWindowText ,"");  //Window Text';
  cxSetResourceString(&_sdxSysColorCaptionText ,"");  //Caption Text';
  cxSetResourceString(&_sdxSysColorActiveBorder ,"");  //Active Border';
  cxSetResourceString(&_sdxSysColorInactiveBorder ,"");  //Inactive Border';
  cxSetResourceString(&_sdxSysColorAppWorkSpace ,"");  //App Workspace';
  cxSetResourceString(&_sdxSysColorHighLight ,"");  //Highlight';
  cxSetResourceString(&_sdxSysColorHighLighText ,"");  //Highlight Text';
  cxSetResourceString(&_sdxSysColorBtnFace ,"");  //Button Face';
  cxSetResourceString(&_sdxSysColorBtnShadow ,"");  //Button Shadow';
  cxSetResourceString(&_sdxSysColorGrayText ,"");  //Gray Text';
  cxSetResourceString(&_sdxSysColorBtnText ,"");  //Button Text';
  cxSetResourceString(&_sdxSysColorInactiveCaptionText ,"");  //Inactive Caption Text';
  cxSetResourceString(&_sdxSysColorBtnHighligh ,"");  //Button Highlight';
  cxSetResourceString(&_sdxSysColor3DDkShadow ,"");  //3DDk Shadow';
  cxSetResourceString(&_sdxSysColor3DLight ,"");  //3DLight';
  cxSetResourceString(&_sdxSysColorInfoText ,"");  //Info Text';
  cxSetResourceString(&_sdxSysColorInfoBk ,"");  //InfoBk';

  cxSetResourceString(&_sdxPureColorBlack ,"");  //Black';
  cxSetResourceString(&_sdxPureColorRed ,"");  //Red';
  cxSetResourceString(&_sdxPureColorLime ,"");  //Lime';
  cxSetResourceString(&_sdxPureColorYellow ,"");  //Yellow';
  cxSetResourceString(&_sdxPureColorGreen ,"");  //Green';
  cxSetResourceString(&_sdxPureColorTeal ,"");  //Teal';
  cxSetResourceString(&_sdxPureColorAqua ,"");  //Aqua';
  cxSetResourceString(&_sdxPureColorBlue ,"");  //Blue';
  cxSetResourceString(&_sdxPureColorWhite ,"");  //White';
  cxSetResourceString(&_sdxPureColorOlive ,"");  //Olive';
  cxSetResourceString(&_sdxPureColorMoneyGreen ,"");  //Money Green';
  cxSetResourceString(&_sdxPureColorNavy ,"");  //Navy';
  cxSetResourceString(&_sdxPureColorSkyBlue ,"");  //Sky Blue';
  cxSetResourceString(&_sdxPureColorGray ,"");  //Gray';
  cxSetResourceString(&_sdxPureColorMedGray ,"");  //Medium Gray';
  cxSetResourceString(&_sdxPureColorSilver ,"");  //Silver';
  cxSetResourceString(&_sdxPureColorMaroon ,"");  //Maroon';
  cxSetResourceString(&_sdxPureColorPurple ,"");  //Purple';
  cxSetResourceString(&_sdxPureColorFuchsia ,"");  //Fuchsia';
  cxSetResourceString(&_sdxPureColorCream ,"");  //Cream';

  cxSetResourceString(&_sdxBrushStyleSolid ,"");  //Solid';
  cxSetResourceString(&_sdxBrushStyleClear ,"");  //Clear';
  cxSetResourceString(&_sdxBrushStyleHorizontal ,"");  //Horizontal';
  cxSetResourceString(&_sdxBrushStyleVertical ,"");  //Vertical';
  cxSetResourceString(&_sdxBrushStyleFDiagonal ,"");  //FDiagonal';
  cxSetResourceString(&_sdxBrushStyleBDiagonal ,"");  //BDiagonal';
  cxSetResourceString(&_sdxBrushStyleCross ,"");  //Cross';
  cxSetResourceString(&_sdxBrushStyleDiagCross ,"");  //DiagCross';

  cxSetResourceString(&_sdxBtnOK ,"");  //OK';
  cxSetResourceString(&_sdxBtnOKAccelerated ,"");  //&OK';
  cxSetResourceString(&_sdxBtnCancel ,"");  //Cancel';
  cxSetResourceString(&_sdxBtnClose ,"");  //Close';
  cxSetResourceString(&_sdxBtnApply ,"");  //&Apply';
  cxSetResourceString(&_sdxBtnHelp ,"");  //&Help';
  cxSetResourceString(&_sdxBtnFix ,"");  //&Fix';
  cxSetResourceString(&_sdxBtnNew ,"");  //&New...';
  cxSetResourceString(&_sdxBtnIgnore ,"");  //&Ignore';
  cxSetResourceString(&_sdxBtnYes ,"");  //&Yes';
  cxSetResourceString(&_sdxBtnNo ,"");  //&No';
  cxSetResourceString(&_sdxBtnEdit ,"");  //&Edit...';
  cxSetResourceString(&_sdxBtnReset ,"");  //&Reset';
  cxSetResourceString(&_sdxBtnAdd ,"");  //&Add';
  cxSetResourceString(&_sdxBtnAddComposition ,"");  //Add &Composition';
  cxSetResourceString(&_sdxBtnDefault ,"");  //&Default...';
  cxSetResourceString(&_sdxBtnDelete ,"");  //&Delete...';
  cxSetResourceString(&_sdxBtnDescription ,"");  //&Description...';
  cxSetResourceString(&_sdxBtnCopy ,"");  //&Copy...';
  cxSetResourceString(&_sdxBtnYesToAll ,"");  //Yes To &All';
  cxSetResourceString(&_sdxBtnRestoreDefaults ,"");  //&Restore Defaults';
  cxSetResourceString(&_sdxBtnRestoreOriginal ,"");  //Restore &Original';
  cxSetResourceString(&_sdxBtnTitleProperties ,"");  //Title Properties...';
  cxSetResourceString(&_sdxBtnProperties ,"");  //P&roperties...';
  cxSetResourceString(&_sdxBtnNetwork ,"");  //Net&work...';
  cxSetResourceString(&_sdxBtnBrowse ,"");  //&Browse...';
  cxSetResourceString(&_sdxBtnPageSetup ,"");  //Pa&ge Setup...';
  cxSetResourceString(&_sdxBtnPrintPreview ,"");  //Print Pre&view...';
  cxSetResourceString(&_sdxBtnPreview ,"");  //Pre&view...';
  cxSetResourceString(&_sdxBtnPrint ,"");  //Print...';
  cxSetResourceString(&_sdxBtnOptions ,"");  //&Options...';
  cxSetResourceString(&_sdxBtnStyleOptions ,"");  //Style Options...';
  cxSetResourceString(&_sdxBtnDefinePrintStyles ,"");  //&Define Styles...';
  cxSetResourceString(&_sdxBtnPrintStyles ,"");  //Print Styles';
  cxSetResourceString(&_sdxBtnBackground ,"");  //Background';
  cxSetResourceString(&_sdxBtnShowToolBar ,"");  //Show &ToolBar';
  cxSetResourceString(&_sdxBtnDesign ,"");  //D&esign...';
  cxSetResourceString(&_sdxBtnMoveUp ,"");  //Move &Up';
  cxSetResourceString(&_sdxBtnMoveDown ,"");  //Move Dow&n';

  cxSetResourceString(&_sdxBtnMoreColors ,"");  //&More Colors...';
  cxSetResourceString(&_sdxBtnFillEffects ,"");  //&Fill Effects...';
  cxSetResourceString(&_sdxBtnNoFill ,"");  //&No Fill';
  cxSetResourceString(&_sdxBtnAutomatic ,"");  //&Automatic';
  cxSetResourceString(&_sdxBtnNone ,"");  //&None';

  cxSetResourceString(&_sdxBtnOtherTexture ,"");  //Other Te&xture...';
  cxSetResourceString(&_sdxBtnInvertColors ,"");  //I&nvert Colors';
  cxSetResourceString(&_sdxBtnSelectPicture ,"");  //Se&lect Picture...';

  cxSetResourceString(&_sdxEditReports ,"");  //Edit Reports';
  cxSetResourceString(&_sdxComposition ,"");  //Composition';
  cxSetResourceString(&_sdxReportTitleDlgCaption ,"");  //Report Title';
  cxSetResourceString(&_sdxMode ,"");  //&Mode:';
  cxSetResourceString(&_sdxText ,"");  //&Text';
  cxSetResourceString(&_sdxProperties ,"");  //&Properties';
  cxSetResourceString(&_sdxAdjustOnScale ,"");  //&Adjust on Scale';
  cxSetResourceString(&_sdxTitleModeNone ,"");  //None';
  cxSetResourceString(&_sdxTitleModeOnEveryTopPage ,"");  //On Every Top Page';
  cxSetResourceString(&_sdxTitleModeOnFirstPage ,"");  //On First Page';

  cxSetResourceString(&_sdxEditDescription ,"");  //Edit Description';
  cxSetResourceString(&_sdxRename ,"");  //Rena&me';
  cxSetResourceString(&_sdxSelectAll ,"");  //&Select All';

  cxSetResourceString(&_sdxAddReport ,"");  //Add Report';
  cxSetResourceString(&_sdxAddAndDesignReport ,"");  //Add and D&esign Report...';
  cxSetResourceString(&_sdxNewCompositionCaption ,"");  //New Composition';
  cxSetResourceString(&_sdxName ,"");  //&Name:';
  cxSetResourceString(&_sdxCaption ,"");  //&Caption:';
  cxSetResourceString(&_sdxAvailableSources ,"");  //&Available Source(s)';
  cxSetResourceString(&_sdxOnlyComponentsInActiveForm ,"");  //Only Components in Active &Form';
  cxSetResourceString(&_sdxOnlyComponentsWithoutLinks ,"");  //Only Components &without Existing ReportLinks';
  cxSetResourceString(&_sdxItemName ,"");  //Name';
  cxSetResourceString(&_sdxItemDescription ,"");  //Description';

  cxSetResourceString(&_sdxConfirmDeleteItem ,"");  //Do you want to delete next items: %s ?';
  cxSetResourceString(&_sdxAddItemsToComposition ,"");  //Add Items to Composition';
  cxSetResourceString(&_sdxHideAlreadyIncludedItems ,"");  //Hide Already &Included Items';
  cxSetResourceString(&_sdxAvailableItems ,"");  //A&vailable Items';
  cxSetResourceString(&_sdxItems ,"");  //&Items';
  cxSetResourceString(&_sdxEnable ,"");  //&Enable';
  cxSetResourceString(&_sdxOptions ,"");  //Options';
  cxSetResourceString(&_sdxShow ,"");  //Show';
  cxSetResourceString(&_sdxPaintItemsGraphics ,"");  //&Paint Item Graphics';
  cxSetResourceString(&_sdxDescription ,"");  //&Description:';

  cxSetResourceString(&_sdxNewReport ,"");  //NewReport';

  cxSetResourceString(&_sdxOnlySelected ,"");  //Only &Selected';
  cxSetResourceString(&_sdxExtendedSelect ,"");  //&Extended Select';
  cxSetResourceString(&_sdxIncludeFixed ,"");  //&Include Fixed';

  cxSetResourceString(&_sdxFonts ,"");  //Fonts';
  cxSetResourceString(&_sdxBtnFont ,"");  //Fo&nt...';
  cxSetResourceString(&_sdxBtnEvenFont ,"");  //E&ven Font...';
  cxSetResourceString(&_sdxBtnOddFont ,"");  //Odd Fo&nt...';
  cxSetResourceString(&_sdxBtnFixedFont ,"");  //F&ixed Font...';
  cxSetResourceString(&_sdxBtnGroupFont ,"");  //Grou&p Font...';
  cxSetResourceString(&_sdxBtnChangeFont ,"");  //Change Fo&nt...';

  cxSetResourceString(&_sdxFont ,"");  //Font';
  cxSetResourceString(&_sdxOddFont ,"");  //Odd Font';
  cxSetResourceString(&_sdxEvenFont ,"");  //Even Font';
  cxSetResourceString(&_sdxPreviewFont ,"");  //Preview Font';
  cxSetResourceString(&_sdxCaptionNodeFont ,"");  //Level Caption Font';
  cxSetResourceString(&_sdxGroupNodeFont ,"");  //Group Node Font';
  cxSetResourceString(&_sdxGroupFooterFont ,"");  //Group Footer Font';
  cxSetResourceString(&_sdxHeaderFont ,"");  //Header Font';
  cxSetResourceString(&_sdxFooterFont ,"");  //Footer Font';
  cxSetResourceString(&_sdxBandFont ,"");  //Band Font';

  cxSetResourceString(&_sdxTransparent ,"");  //&Transparent';
  cxSetResourceString(&_sdxFixedTransparent ,"");  //Fi&xed Transparent';
  cxSetResourceString(&_sdxCaptionTransparent ,"");  //Caption Transparent';
  cxSetResourceString(&_sdxGroupTransparent ,"");  //Group Transparent';

  cxSetResourceString(&_sdxGraphicAsTextValue ,"");  //(GRAPHIC)';
  cxSetResourceString(&_sdxColors ,"");  //Colors';
  cxSetResourceString(&_sdxColor ,"");  //Co&lor:';
  cxSetResourceString(&_sdxOddColor ,"");  //Odd Co&lor:';
  cxSetResourceString(&_sdxEvenColor ,"");  //E&ven Color:';
  cxSetResourceString(&_sdxPreviewColor ,"");  //&Preview Color:';
  cxSetResourceString(&_sdxBandColor ,"");  //&Band Color:';
  cxSetResourceString(&_sdxLevelCaptionColor ,"");  //Le&vel Caption Color:';
  cxSetResourceString(&_sdxHeaderColor ,"");  //H&eader Color:';
  cxSetResourceString(&_sdxGroupNodeColor ,"");  //Group &Node Color:';
  cxSetResourceString(&_sdxGroupFooterColor ,"");  //&Group Footer Color:';
  cxSetResourceString(&_sdxFooterColor ,"");  //Foo&ter Color:';
  cxSetResourceString(&_sdxFixedColor ,"");  //F&ixed Color:';
  cxSetResourceString(&_sdxGroupColor ,"");  //Grou&p Color:';
  cxSetResourceString(&_sdxCaptionColor ,"");  //Caption Color:';
  cxSetResourceString(&_sdxGridLinesColor ,"");  //Gri&d Line Color:';

  cxSetResourceString(&_sdxBands ,"");  //&Bands';
  cxSetResourceString(&_sdxLevelCaptions ,"");  //Levels &Caption';
  cxSetResourceString(&_sdxHeaders ,"");  //H&eaders';
  cxSetResourceString(&_sdxFooters ,"");  //Foote&rs';
  cxSetResourceString(&_sdxGroupFooters ,"");  //&Group Footers';
  cxSetResourceString(&_sdxPreview ,"");  //Previe&w';
  cxSetResourceString(&_sdxPreviewLineCount ,"");  //Preview Line Coun&t:';
  cxSetResourceString(&_sdxAutoCalcPreviewLineCount ,"");  //A&uto Calculate Preview Lines';

  cxSetResourceString(&_sdxGrid ,"");  //Grid Lines';
  cxSetResourceString(&_sdxNodesGrid ,"");  //Node Grid Lines';
  cxSetResourceString(&_sdxGroupFooterGrid ,"");  //GroupFooter Grid Lines';

  cxSetResourceString(&_sdxStateImages ,"");  //&State Images';
  cxSetResourceString(&_sdxImages ,"");  //&Images';

  cxSetResourceString(&_sdxTextAlign ,"");  //Text&Align';
  cxSetResourceString(&_sdxTextAlignHorz ,"");  //Hori&zontally';
  cxSetResourceString(&_sdxTextAlignVert ,"");  //&Vertically';
  cxSetResourceString(&_sdxTextAlignLeft ,"");  //Left';
  cxSetResourceString(&_sdxTextAlignCenter ,"");  //Center';
  cxSetResourceString(&_sdxTextAlignRight ,"");  //Right';
  cxSetResourceString(&_sdxTextAlignTop ,"");  //Top';
  cxSetResourceString(&_sdxTextAlignVCenter ,"");  //Center';
  cxSetResourceString(&_sdxTextAlignBottom ,"");  //Bottom';
  cxSetResourceString(&_sdxBorderLines ,"");  //&Border';
  cxSetResourceString(&_sdxHorzLines ,"");  //Hori&zontal Lines';
  cxSetResourceString(&_sdxVertLines ,"");  //&Vertical Lines';
  cxSetResourceString(&_sdxFixedHorzLines ,"");  //Fi&xed Horizontal Lines';
  cxSetResourceString(&_sdxFixedVertLines ,"");  //Fixe&d Vertical Lines';
  cxSetResourceString(&_sdxFlatCheckMarks ,"");  //F&lat CheckMarks';
  cxSetResourceString(&_sdxCheckMarksAsText ,"");  //&Display CheckMarks as Text';

  cxSetResourceString(&_sdxRowAutoHeight ,"");  //Ro&w AutoHeight';
  cxSetResourceString(&_sdxEndEllipsis ,"");  //&EndEllipsis';

  cxSetResourceString(&_sdxDrawBorder ,"");  //&Draw Border';
  cxSetResourceString(&_sdxFullExpand ,"");  //Full &Expand';
  cxSetResourceString(&_sdxBorderColor ,"");  //&Border Color:';
  cxSetResourceString(&_sdxAutoNodesExpand ,"");  //A&uto Nodes Expand';
  cxSetResourceString(&_sdxExpandLevel ,"");  //Expand &Level:';
  cxSetResourceString(&_sdxFixedRowOnEveryPage ,"");  //Fixed Rows';

  cxSetResourceString(&_sdxDrawMode ,"");  //Draw &Mode:';
  cxSetResourceString(&_sdxDrawModeStrict ,"");  //Strict';
  cxSetResourceString(&_sdxDrawModeOddEven ,"");  //Odd/Even Rows Mode';
  cxSetResourceString(&_sdxDrawModeChess ,"");  //Chess Mode';
  cxSetResourceString(&_sdxDrawModeBorrow ,"");  //Borrow From Source';

  cxSetResourceString(&_sdx3DEffects ,"");  //3D Effects';
  cxSetResourceString(&_sdxUse3DEffects ,"");  //Use &3D Effects';
  cxSetResourceString(&_sdxSoft3D ,"");  //Sof&t3D';

  cxSetResourceString(&_sdxBehaviors ,"");  //Behaviors';
  cxSetResourceString(&_sdxMiscellaneous ,"");  //Miscellaneous';
  cxSetResourceString(&_sdxOnEveryPage ,"");  //On Every Page';
  cxSetResourceString(&_sdxNodeExpanding ,"");  //Node Expanding';
  cxSetResourceString(&_sdxSelection ,"");  //Selection';
  cxSetResourceString(&_sdxNodeAutoHeight ,"");  //&Node Auto Height';
  cxSetResourceString(&_sdxTransparentGraphics ,"");  //&Transparent Graphics';
  cxSetResourceString(&_sdxAutoWidth ,"");  //Auto &Width';

  cxSetResourceString(&_sdxDisplayGraphicsAsText ,"");  //Display Graphic As &Text';
  cxSetResourceString(&_sdxTransparentColumnGraphics ,"");  //Transparent &Graphics';

  cxSetResourceString(&_sdxBandsOnEveryPage ,"");  //Bands';
  cxSetResourceString(&_sdxHeadersOnEveryPage ,"");  //Headers';
  cxSetResourceString(&_sdxFootersOnEveryPage ,"");  //Footers';
  cxSetResourceString(&_sdxGraphics ,"");  //&Graphics';

  { Common messages }

  cxSetResourceString(&_sdxOutOfResources ,"");  //Out of Resources';
  cxSetResourceString(&_sdxFileAlreadyExists ,"");  //File "%s" Already Exists.';
  cxSetResourceString(&_sdxConfirmOverWrite ,"");  //File "%s" already exists. Overwrite ?';
  cxSetResourceString(&_sdxInvalidFileName ,"");  //Invalid File Name "%s"';
  cxSetResourceString(&_sdxRequiredFileName ,"");  //Enter file name.';
  cxSetResourceString(&_sdxOutsideMarginsMessage =
    'One or more margins are set outside the printable area of the page.' + #13#10 +
    'Do you want to continue ?';
  cxSetResourceString(&_sdxOutsideMarginsMessage2 =
    'One or more margins are set outside the printable area of the page.' + #13#10 +
    'Choose the Fix button to increase the appropriate margins.';
  cxSetResourceString(&_sdxInvalidMarginsMessage =
    'One or more margins are set to the invalid values.' + #13#10 +
    'Choose the Fix button to correct this problem.' + #13#10 +
    'Choose the Restore button to restore original values.';
  cxSetResourceString(&_sdxInvalidMargins ,"");  //One or more margins have invalid values';
  cxSetResourceString(&_sdxOutsideMargins ,"");  //One or more margins are set outside the printable area of the page';
  cxSetResourceString(&_sdxThereAreNowItemsForShow ,"");  //There are no items in this view';
*/
  //{ Color palette }

  cxSetResourceString(&_sdxPageBackground ,"��� ��������");  // Page Background';
  cxSetResourceString(&_sdxPenColor ,"Pen Color");  //Pen Color';
  cxSetResourceString(&_sdxFontColor ,"���� ������");  //Font Color';
  cxSetResourceString(&_sdxBrushColor ,"Brush Color");  //Brush Color';
  cxSetResourceString(&_sdxHighLight ,"���������");  //HighLight';
/*
  { Color names }
  
  cxSetResourceString(&_sdxColorBlack ,"");  //Black';
  cxSetResourceString(&_sdxColorDarkRed ,"");  //Dark Red';
  cxSetResourceString(&_sdxColorRed ,"");  //Red';
  cxSetResourceString(&_sdxColorPink ,"");  //Pink';
  cxSetResourceString(&_sdxColorRose ,"");  //Rose';
  cxSetResourceString(&_sdxColorBrown ,"");  //Brown';
  cxSetResourceString(&_sdxColorOrange ,"");  //Orange';
  cxSetResourceString(&_sdxColorLightOrange ,"");  //Light Orange';
  cxSetResourceString(&_sdxColorGold ,"");  //Gold';
  cxSetResourceString(&_sdxColorTan ,"");  //Tan';
  cxSetResourceString(&_sdxColorOliveGreen ,"");  //Olive Green';
  cxSetResourceString(&_sdxColorDrakYellow ,"");  //Dark Yellow';
  cxSetResourceString(&_sdxColorLime ,"");  //Lime';
  cxSetResourceString(&_sdxColorYellow ,"");  //Yellow';
  cxSetResourceString(&_sdxColorLightYellow ,"");  //Light Yellow';
  cxSetResourceString(&_sdxColorDarkGreen ,"");  //Dark Green';
  cxSetResourceString(&_sdxColorGreen ,"");  //Green';
  cxSetResourceString(&_sdxColorSeaGreen ,"");  //Sea Green';
  cxSetResourceString(&_sdxColorBrighthGreen ,"");  //Bright Green';
  cxSetResourceString(&_sdxColorLightGreen ,"");  //Light Green';
  cxSetResourceString(&_sdxColorDarkTeal ,"");  //Dark Teal';
  cxSetResourceString(&_sdxColorTeal ,"");  //Teal';
  cxSetResourceString(&_sdxColorAqua ,"");  //Aqua';
  cxSetResourceString(&_sdxColorTurquoise ,"");  //Turquoise';
  cxSetResourceString(&_sdxColorLightTurquoise ,"");  //Light Turquoise';
  cxSetResourceString(&_sdxColorDarkBlue ,"");  //Dark Blue';
  cxSetResourceString(&_sdxColorBlue ,"");  //Blue';
  cxSetResourceString(&_sdxColorLightBlue ,"");  //Light Blue';
  cxSetResourceString(&_sdxColorSkyBlue ,"");  //Sky Blue';
  cxSetResourceString(&_sdxColorPaleBlue ,"");  //Pale Blue';
  cxSetResourceString(&_sdxColorIndigo ,"");  //Indigo';
  cxSetResourceString(&_sdxColorBlueGray ,"");  //Blue Gray';
  cxSetResourceString(&_sdxColorViolet ,"");  //Violet';
  cxSetResourceString(&_sdxColorPlum ,"");  //Plum';
  cxSetResourceString(&_sdxColorLavender ,"");  //Lavender';
  cxSetResourceString(&_sdxColorGray80 ,"");  //Gray-80%';
  cxSetResourceString(&_sdxColorGray50 ,"");  //Gray-50%';
  cxSetResourceString(&_sdxColorGray40 ,"");  //Gray-40%';
  cxSetResourceString(&_sdxColorGray25 ,"");  //Gray-25%';
  cxSetResourceString(&_sdxColorWhite ,"");  //White';
 
  { FEF Dialog }
  
  cxSetResourceString(&_sdxTexture ,"");  //&Texture';
  cxSetResourceString(&_sdxPattern ,"");  //&Pattern';
  cxSetResourceString(&_sdxPicture ,"");  //P&icture';
  cxSetResourceString(&_sdxForeground ,"");  //&Foreground';
  cxSetResourceString(&_sdxBackground ,"");  //&Background';
  cxSetResourceString(&_sdxSample ,"");  //Sample:';

  cxSetResourceString(&_sdxFEFCaption ,"");  //Fill Effects';
  cxSetResourceString(&_sdxPaintMode ,"");  //Paint &Mode';
  cxSetResourceString(&_sdxPaintModeCenter ,"");  //Center';
  cxSetResourceString(&_sdxPaintModeStretch ,"");  //Stretch';
  cxSetResourceString(&_sdxPaintModeTile ,"");  //Tile';
  cxSetResourceString(&_sdxPaintModeProportional ,"");  //Proportional';

  { Pattern names }
  
  cxSetResourceString(&_sdxPatternGray5 ,"");  //5%';
  cxSetResourceString(&_sdxPatternGray10 ,"");  //10%';
  cxSetResourceString(&_sdxPatternGray20 ,"");  //20%';
  cxSetResourceString(&_sdxPatternGray25 ,"");  //25%';
  cxSetResourceString(&_sdxPatternGray30 ,"");  //30%';
  cxSetResourceString(&_sdxPatternGray40 ,"");  //40%';
  cxSetResourceString(&_sdxPatternGray50 ,"");  //50%';
  cxSetResourceString(&_sdxPatternGray60 ,"");  //60%';
  cxSetResourceString(&_sdxPatternGray70 ,"");  //70%';
  cxSetResourceString(&_sdxPatternGray75 ,"");  //75%';
  cxSetResourceString(&_sdxPatternGray80 ,"");  //80%';
  cxSetResourceString(&_sdxPatternGray90 ,"");  //90%';
  cxSetResourceString(&_sdxPatternLightDownwardDiagonal ,"");  //Light downward diagonal';
  cxSetResourceString(&_sdxPatternLightUpwardDiagonal ,"");  //Light upward diagonal';
  cxSetResourceString(&_sdxPatternDarkDownwardDiagonal ,"");  //Dark downward diagonal';
  cxSetResourceString(&_sdxPatternDarkUpwardDiagonal ,"");  //Dark upward diagonal';
  cxSetResourceString(&_sdxPatternWideDownwardDiagonal ,"");  //Wide downward diagonal';
  cxSetResourceString(&_sdxPatternWideUpwardDiagonal ,"");  //Wide upward diagonal';
  cxSetResourceString(&_sdxPatternLightVertical ,"");  //Light vertical';
  cxSetResourceString(&_sdxPatternLightHorizontal ,"");  //Light horizontal';
  cxSetResourceString(&_sdxPatternNarrowVertical ,"");  //Narrow vertical';
  cxSetResourceString(&_sdxPatternNarrowHorizontal ,"");  //Narrow horizontal';
  cxSetResourceString(&_sdxPatternDarkVertical ,"");  //Dark vertical';
  cxSetResourceString(&_sdxPatternDarkHorizontal ,"");  //Dark horizontal';
  cxSetResourceString(&_sdxPatternDashedDownward ,"");  //Dashed downward';
  cxSetResourceString(&_sdxPatternDashedUpward ,"");  //Dashed upward';
  cxSetResourceString(&_sdxPatternDashedVertical ,"");  //Dashed vertical';
  cxSetResourceString(&_sdxPatternDashedHorizontal ,"");  //Dashed horizontal';
  cxSetResourceString(&_sdxPatternSmallConfetti ,"");  //Small confetti';
  cxSetResourceString(&_sdxPatternLargeConfetti ,"");  //Large confetti';
  cxSetResourceString(&_sdxPatternZigZag ,"");  //Zig zag';
  cxSetResourceString(&_sdxPatternWave ,"");  //Wave';
  cxSetResourceString(&_sdxPatternDiagonalBrick ,"");  //Diagonal brick';
  cxSetResourceString(&_sdxPatternHorizantalBrick ,"");  //Horizontal brick';
  cxSetResourceString(&_sdxPatternWeave ,"");  //Weave';
  cxSetResourceString(&_sdxPatternPlaid ,"");  //Plaid';
  cxSetResourceString(&_sdxPatternDivot ,"");  //Divot';
  cxSetResourceString(&_sdxPatternDottedGrid ,"");  //Dottedgrid';
  cxSetResourceString(&_sdxPatternDottedDiamond ,"");  //Dotted diamond';
  cxSetResourceString(&_sdxPatternShingle ,"");  //Shingle';
  cxSetResourceString(&_sdxPatternTrellis ,"");  //Trellis';
  cxSetResourceString(&_sdxPatternSphere ,"");  //Sphere';
  cxSetResourceString(&_sdxPatternSmallGrid ,"");  //Small grid';
  cxSetResourceString(&_sdxPatternLargeGrid ,"");  //Large grid';
  cxSetResourceString(&_sdxPatternSmallCheckedBoard ,"");  //Small checked board';
  cxSetResourceString(&_sdxPatternLargeCheckedBoard ,"");  //Large checked board';
  cxSetResourceString(&_sdxPatternOutlinedDiamond ,"");  //Outlined diamond';
  cxSetResourceString(&_sdxPatternSolidDiamond ,"");  //Solid diamond';

  { Texture names }
  
  cxSetResourceString(&_sdxTextureNewSprint ,"");  //Newsprint';
  cxSetResourceString(&_sdxTextureGreenMarble ,"");  //Green marble';
  cxSetResourceString(&_sdxTextureBlueTissuePaper ,"");  //Blue tissue paper';
  cxSetResourceString(&_sdxTexturePapyrus ,"");  //Papyrus';
  cxSetResourceString(&_sdxTextureWaterDroplets ,"");  //Water droplets';
  cxSetResourceString(&_sdxTextureCork ,"");  //Cork';
  cxSetResourceString(&_sdxTextureRecycledPaper ,"");  //Recycled paper';
  cxSetResourceString(&_sdxTextureWhiteMarble ,"");  //White marble';
  cxSetResourceString(&_sdxTexturePinkMarble ,"");  //Pink marble';
  cxSetResourceString(&_sdxTextureCanvas ,"");  //Canvas';
  cxSetResourceString(&_sdxTexturePaperBag ,"");  //Paper bag';
  cxSetResourceString(&_sdxTextureWalnut ,"");  //Walnut';
  cxSetResourceString(&_sdxTextureParchment ,"");  //Parchment';
  cxSetResourceString(&_sdxTextureBrownMarble ,"");  //Brown marble';
  cxSetResourceString(&_sdxTexturePurpleMesh ,"");  //Purple mesh';
  cxSetResourceString(&_sdxTextureDenim ,"");  //Denim';
  cxSetResourceString(&_sdxTextureFishFossil ,"");  //Fish fossil';
  cxSetResourceString(&_sdxTextureOak ,"");  //Oak';
  cxSetResourceString(&_sdxTextureStationary ,"");  //Stationary';
  cxSetResourceString(&_sdxTextureGranite ,"");  //Granite';
  cxSetResourceString(&_sdxTextureBouquet ,"");  //Bouquet';
  cxSetResourceString(&_sdxTextureWonenMat ,"");  //Woven mat';
  cxSetResourceString(&_sdxTextureSand ,"");  //Sand';
  cxSetResourceString(&_sdxTextureMediumWood ,"");  //Medium wood';

  cxSetResourceString(&_sdxFSPCaption ,"");  //Picture Preview';
  cxSetResourceString(&_sdxWidth ,"");  //Width';
  cxSetResourceString(&_sdxHeight ,"");  //Height';

  { Brush Dialog }
  
  cxSetResourceString(&_sdxBrushDlgCaption ,"");  //Brush properties';
  cxSetResourceString(&_sdxStyle ,"");  //&Style:';

  { Enter New File Name dialog }
  
  cxSetResourceString(&_sdxENFNCaption ,"");  //Choose New File Name';
  cxSetResourceString(&_sdxEnterNewFileName ,"");  //Enter New File Name';

  { Define styles dialog }
  
  cxSetResourceString(&_sdxDefinePrintStylesCaption ,"");  //Define Print Styles';
  cxSetResourceString(&_sdxDefinePrintStylesTitle ,"");  //Print &Styles:';
  cxSetResourceString(&_sdxDefinePrintStylesWarningDelete ,"");  //Do you want to delete "%s" ?';
  cxSetResourceString(&_sdxDefinePrintStylesWarningClear ,"");  //Do you want to delete all not built-in styles ?';
  cxSetResourceString(&_sdxClear ,"");  //C&lear...';

  { Print device }
  
  cxSetResourceString(&_sdxCustomSize ,"");  //Custom Size';
  cxSetResourceString(&_sdxDefaultTray ,"");  //Default Tray';
  cxSetResourceString(&_sdxInvalidPrintDevice ,"");  //Printer selected is not valid';
  cxSetResourceString(&_sdxNotPrinting ,"");  //Printer is not currently printing';
  cxSetResourceString(&_sdxPrinting ,"");  //Printing in progress';
  cxSetResourceString(&_sdxDeviceOnPort ,"");  //%s on %s';
  cxSetResourceString(&_sdxPrinterIndexError ,"");  //Printer index out of range';
  cxSetResourceString(&_sdxNoDefaultPrintDevice ,"");  //There is no default printer selected';

  { Edit AutoText Entries Dialog }
  
  cxSetResourceString(&_sdxAutoTextDialogCaption ,"");  //Edit AutoText Entries';
  cxSetResourceString(&_sdxEnterAutoTextEntriesHere ,"");  // Enter A&utoText Entries Here: ';
*/
  //{ Print dialog }
  
  cxSetResourceString(&_sdxPrintDialogCaption ,"������");  //Print';
  cxSetResourceString(&_sdxPrintDialogPrinter ,"������� ");  // Printer ';
  cxSetResourceString(&_sdxPrintDialogName ,"&������������");  //&Name:';
  cxSetResourceString(&_sdxPrintDialogStatus ,"������:");  //Status:';
  cxSetResourceString(&_sdxPrintDialogType ,"���:");  //Type:';
  cxSetResourceString(&_sdxPrintDialogWhere ,"");  //Where:';
  cxSetResourceString(&_sdxPrintDialogComment ,"�����������:");  //Comment:';
  cxSetResourceString(&_sdxPrintDialogPrintToFile ,"������ � &����");  //Print to &File';
  cxSetResourceString(&_sdxPrintDialogPageRange ,"�������� ������� ");  // Page range ';
  cxSetResourceString(&_sdxPrintDialogAll ,"&���");  //&All';
  cxSetResourceString(&_sdxPrintDialogCurrentPage ,"&�������");  //Curr&ent Page';
  cxSetResourceString(&_sdxPrintDialogSelection ,"&�����");  //&Selection';
  cxSetResourceString(&_sdxPrintDialogPages ,"�&��.");  //&Pages:';
  cxSetResourceString(&_sdxPrintDialogRangeLegend,"������� ����� �������� �/��� �������� �������\n���������� �������. ������: 1,3,5-12.");  //    'Enter page number and/or page ranges' + #10#13 +    'separated by commas. For example: 1,3,5-12.';
  cxSetResourceString(&_sdxPrintDialogCopies ,"����� ");  // Copies ';
  cxSetResourceString(&_sdxPrintDialogNumberOfPages ,"���������� �������:");  //N&umber of Pages:';
  cxSetResourceString(&_sdxPrintDialogNumberOfCopies ,"���������� �����:");  //Number of &Copies:';
  cxSetResourceString(&_sdxPrintDialogCollateCopies ,"����������� �����");  //Colla&te Copies';
  cxSetResourceString(&_sdxPrintDialogAllPages ,"���");  //All';
  cxSetResourceString(&_sdxPrintDialogEvenPages ,"׸����");  //Even';
  cxSetResourceString(&_sdxPrintDialogOddPages ,"��������");  //Odd';
  cxSetResourceString(&_sdxPrintDialogPrintStyles ,"����� ������ ");  // Print St&yles ';
/*
  { PrintToFile Dialog }
  
  cxSetResourceString(&_sdxPrintDialogOpenDlgTitle ,"");  //Choose File Name';
  cxSetResourceString(&_sdxPrintDialogOpenDlgAllFiles ,"");  //All Files';
  cxSetResourceString(&_sdxPrintDialogOpenDlgPrinterFiles ,"");  //Printer Files';
  cxSetResourceString(&_sdxPrintDialogPageNumbersOutOfRange ,"");  //Page numbers out of range (%d - %d)';
  cxSetResourceString(&_sdxPrintDialogInvalidPageRanges ,"");  //Invalid page ranges';
  cxSetResourceString(&_sdxPrintDialogRequiredPageNumbers ,"");  //Enter page numbers';
  cxSetResourceString(&_sdxPrintDialogNoPrinters = 
    'No printers are installed. To install a printer, ' + 
    'point to Settings on the Windows Start menu, click Printers, and then double-click Add Printer. ' + 
    'Follow the instructions in the wizard.';
  cxSetResourceString(&_sdxPrintDialogInPrintingState ,"");  //Printer is currently printing.' + #10#13 +
    'Please wait.';

  { Printer State }
  
  cxSetResourceString(&_sdxPrintDialogPSPaused ,"");  //Paused';
  cxSetResourceString(&_sdxPrintDialogPSPendingDeletion ,"");  //Pending Deletion';
  cxSetResourceString(&_sdxPrintDialogPSBusy ,"");  //Busy';
  cxSetResourceString(&_sdxPrintDialogPSDoorOpen ,"");  //Door Open';
  cxSetResourceString(&_sdxPrintDialogPSError ,"");  //Error';
  cxSetResourceString(&_sdxPrintDialogPSInitializing ,"");  //Initializing';
  cxSetResourceString(&_sdxPrintDialogPSIOActive ,"");  //IO Active';
  cxSetResourceString(&_sdxPrintDialogPSManualFeed ,"");  //Manual Feed';
  cxSetResourceString(&_sdxPrintDialogPSNoToner ,"");  //No Toner';
  cxSetResourceString(&_sdxPrintDialogPSNotAvailable ,"");  //Not Available';
  cxSetResourceString(&_sdxPrintDialogPSOFFLine ,"");  //Offline';
  cxSetResourceString(&_sdxPrintDialogPSOutOfMemory ,"");  //Out of Memory';
  cxSetResourceString(&_sdxPrintDialogPSOutBinFull ,"");  //Output Bin Full';
  cxSetResourceString(&_sdxPrintDialogPSPagePunt ,"");  //Page Punt';
  cxSetResourceString(&_sdxPrintDialogPSPaperJam ,"");  //Paper Jam';
  cxSetResourceString(&_sdxPrintDialogPSPaperOut ,"");  //Paper Out';
  cxSetResourceString(&_sdxPrintDialogPSPaperProblem ,"");  //Paper Problem';
  cxSetResourceString(&_sdxPrintDialogPSPrinting ,"");  //Printing';
  cxSetResourceString(&_sdxPrintDialogPSProcessing ,"");  //Processing';
  cxSetResourceString(&_sdxPrintDialogPSTonerLow ,"");  //Toner Low';
  cxSetResourceString(&_sdxPrintDialogPSUserIntervention ,"");  //User Intervention';
  cxSetResourceString(&_sdxPrintDialogPSWaiting ,"");  //Waiting';
  cxSetResourceString(&_sdxPrintDialogPSWarningUp ,"");  //Warming Up';
  cxSetResourceString(&_sdxPrintDialogPSReady ,"");  //Ready';
  cxSetResourceString(&_sdxPrintDialogPSPrintingAndWaiting ,"");  //Printing: %d document(s) waiting';

  cxSetResourceString(&_sdxLeftMargin ,"");  //Left Margin';
  cxSetResourceString(&_sdxTopMargin ,"");  //Top Margin';
  cxSetResourceString(&_sdxRightMargin ,"");  //Right Margin';
  cxSetResourceString(&_sdxBottomMargin ,"");  //Bottom Margin';
  cxSetResourceString(&_sdxGutterMargin ,"");  //Gutter';
  cxSetResourceString(&_sdxHeaderMargin ,"");  //Header';
  cxSetResourceString(&_sdxFooterMargin ,"");  //Footer';

  cxSetResourceString(&_sdxUnitsInches ,"");  //"';
  cxSetResourceString(&_sdxUnitsCentimeters ,"");  //cm';
  cxSetResourceString(&_sdxUnitsMillimeters ,"");  //mm';
  cxSetResourceString(&_sdxUnitsPoints ,"");  //pt';
  cxSetResourceString(&_sdxUnitsPicas ,"");  //pi';

  cxSetResourceString(&_sdxUnitsDefaultName ,"");  //Default';
  cxSetResourceString(&_sdxUnitsInchesName ,"");  //Inches';
  cxSetResourceString(&_sdxUnitsCentimetersName ,"");  //Centimeters';
  cxSetResourceString(&_sdxUnitsMillimetersName ,"");  //Millimeters';
  cxSetResourceString(&_sdxUnitsPointsName ,"");  //Points';
  cxSetResourceString(&_sdxUnitsPicasName ,"");  //Picas';

  cxSetResourceString(&_sdxPrintPreview ,"");  //Print Preview';
  cxSetResourceString(&_sdxReportDesignerCaption ,"");  //Format Report';
  cxSetResourceString(&_sdxCompositionDesignerCaption ,"");  //Composition Editor';

  cxSetResourceString(&_sdxComponentNotSupportedByLink ,"");  //Component "%s" not supported by TdxComponentPrinter';
  cxSetResourceString(&_sdxComponentNotSupported ,"");  //Component "%s" not supported by TdxComponentPrinter';
  cxSetResourceString(&_sdxPrintDeviceNotReady ,"");  //Printer has not been installed or is not ready';
  cxSetResourceString(&_sdxUnableToGenerateReport ,"");  //Unable to generate report';
  cxSetResourceString(&_sdxPreviewNotRegistered ,"");  //There is no registered preview form';
  cxSetResourceString(&_sdxComponentNotAssigned ,"");  //%s' + #13#10 + 'Not assigned "Component" property';
  cxSetResourceString(&_sdxPrintDeviceIsBusy ,"");  //Printer is busy';
  cxSetResourceString(&_sdxPrintDeviceError ,"");  //Printer has encountered error !';
  cxSetResourceString(&_sdxMissingComponent ,"");  //Missing "Component" property';
  cxSetResourceString(&_sdxDataProviderDontPresent ,"");  //There are no Links with Assigned Component in Composition';
  cxSetResourceString(&_sdxBuildingReport ,"");  //Building report: Completed %d%%';                            // obsolete
  cxSetResourceString(&_sdxPrintingReport ,"");  //Printing report: Completed %d page(s). Press Esc to cancel'; // obsolete
  cxSetResourceString(&_sdxDefinePrintStylesMenuItem ,"");  //Define Print &Styles...';
  cxSetResourceString(&_sdxAbortPrinting ,"");  //Abort printing ?';
  cxSetResourceString(&_sdxStandardStyle ,"");  //Standard Style';

  cxSetResourceString(&_sdxFontStyleBold ,"");  //Bold';
  cxSetResourceString(&_sdxFontStyleItalic ,"");  //Italic';
  cxSetResourceString(&_sdxFontStyleUnderline ,"");  //Underline';
  cxSetResourceString(&_sdxFontStyleStrikeOut ,"");  //StrikeOut';
  cxSetResourceString(&_sdxPt ,"");  //pt.';

  cxSetResourceString(&_sdxNoPages ,"");  //There are no pages to display';
  cxSetResourceString(&_sdxPageWidth ,"");  //Page Width';
  cxSetResourceString(&_sdxWholePage ,"");  //Whole Page';
  cxSetResourceString(&_sdxTwoPages ,"");  //Two Pages';
  cxSetResourceString(&_sdxFourPages ,"");  //Four Pages';
  cxSetResourceString(&_sdxWidenToSourceWidth ,"");  //Widen to Source Width';

  cxSetResourceString(&_sdxMenuBar ,"");  //MenuBar';
  cxSetResourceString(&_sdxStandardBar ,"");  //Standard';
  cxSetResourceString(&_sdxHeaderFooterBar ,"");  //Header and Footer';
  cxSetResourceString(&_sdxShortcutMenusBar ,"");  //Shortcut Menus';
  cxSetResourceString(&_sdxAutoTextBar ,"");  //AutoText';

  cxSetResourceString(&_sdxMenuFile ,"");  //&File';
  cxSetResourceString(&_sdxMenuFileDesign ,"");  //&Design...';
  cxSetResourceString(&_sdxMenuFilePrint ,"");  //&Print...';
  cxSetResourceString(&_sdxMenuFilePageSetup ,"");  //Page Set&up...';
  cxSetResourceString(&_sdxMenuPrintStyles ,"");  //Print Styles';
  cxSetResourceString(&_sdxMenuFileExit ,"");  //&Close';

  cxSetResourceString(&_sdxMenuEdit ,"");  //&Edit';
  cxSetResourceString(&_sdxMenuEditCut ,"");  //Cu&t';
  cxSetResourceString(&_sdxMenuEditCopy ,"");  //&Copy';
  cxSetResourceString(&_sdxMenuEditPaste ,"");  //&Paste';
  cxSetResourceString(&_sdxMenuEditDelete ,"");  //&Delete';
  cxSetResourceString(&_sdxMenuEditFind ,"");  //&Find...';
  cxSetResourceString(&_sdxMenuEditFindNext ,"");  //Find Ne&xt';
  cxSetResourceString(&_sdxMenuEditReplace ,"");  //&Replace...';

  cxSetResourceString(&_sdxMenuLoad ,"");  //&Load...';
  cxSetResourceString(&_sdxMenuPreview ,"");  //Pre&view...';

  cxSetResourceString(&_sdxMenuInsert ,"");  //&Insert';
  cxSetResourceString(&_sdxMenuInsertAutoText ,"");  //&AutoText';
  cxSetResourceString(&_sdxMenuInsertEditAutoTextEntries ,"");  //AutoTe&xt...';
  cxSetResourceString(&_sdxMenuInsertAutoTextEntries ,"");  //List of AutoText Entries';
  cxSetResourceString(&_sdxMenuInsertAutoTextEntriesSubItem ,"");  //In&sert AutoText';
  cxSetResourceString(&_sdxMenuInsertPageNumber ,"");  //&Page Number';
  cxSetResourceString(&_sdxMenuInsertTotalPages ,"");  //&Number of Pages';
  cxSetResourceString(&_sdxMenuInsertPageOfPages ,"");  //Pa&ge Number of Pages';
  cxSetResourceString(&_sdxMenuInsertDateTime ,"");  //Date and Time';
  cxSetResourceString(&_sdxMenuInsertDate ,"");  //&Date';
  cxSetResourceString(&_sdxMenuInsertTime ,"");  //&Time';
  cxSetResourceString(&_sdxMenuInsertUserName ,"");  //&User Name';
  cxSetResourceString(&_sdxMenuInsertMachineName ,"");  //&Machine Name';

  cxSetResourceString(&_sdxMenuView ,"");  //&View';
  cxSetResourceString(&_sdxMenuViewMargins ,"");  //&Margins';
  cxSetResourceString(&_sdxMenuViewFlatToolBarButtons ,"");  //&Flat ToolBar Buttons';
  cxSetResourceString(&_sdxMenuViewLargeToolBarButtons ,"");  //&Large ToolBar Buttons';
  cxSetResourceString(&_sdxMenuViewMarginsStatusBar ,"");  //M&argins Bar';
  cxSetResourceString(&_sdxMenuViewPagesStatusBar ,"");  //&Status Bar';
  cxSetResourceString(&_sdxMenuViewToolBars ,"");  //&Toolbars';
  cxSetResourceString(&_sdxMenuViewPagesHeaders ,"");  //Page &Headers';
  cxSetResourceString(&_sdxMenuViewPagesFooters ,"");  //Page Foote&rs';
  cxSetResourceString(&_sdxMenuViewSwitchToLeftPart ,"");  //Switch to Left Part';
  cxSetResourceString(&_sdxMenuViewSwitchToRightPart ,"");  //Switch to Right Part';
  cxSetResourceString(&_sdxMenuViewSwitchToCenterPart ,"");  //Switch to Center Part';
  cxSetResourceString(&_sdxMenuViewHFSwitchHeaderFooter ,"");  //&Show Header/Footer';
  cxSetResourceString(&_sdxMenuViewHFClose ,"");  //&Close';
*/
  cxSetResourceString(&_sdxMenuZoom ,"");  //&Zoom';
  cxSetResourceString(&_sdxMenuZoomPercent100 ,"");  //Percent &100';
  cxSetResourceString(&_sdxMenuZoomPageWidth ,"");  //Page &Width';
  cxSetResourceString(&_sdxMenuZoomWholePage ,"");  //W&hole Page';
  cxSetResourceString(&_sdxMenuZoomTwoPages ,"");  //&Two Pages';
  cxSetResourceString(&_sdxMenuZoomFourPages ,"");  //&Four Pages';
  cxSetResourceString(&_sdxMenuZoomMultiplyPages ,"");  //&Multiple Pages';
  cxSetResourceString(&_sdxMenuZoomWidenToSourceWidth ,"");  //Widen To S&ource Width';
  cxSetResourceString(&_sdxMenuZoomSetup ,"");  //&Setup...';

  cxSetResourceString(&_sdxMenuPages ,"");  //&Pages';

  cxSetResourceString(&_sdxMenuGotoPage ,"");  //&Go';
  cxSetResourceString(&_sdxMenuGotoPageFirst ,"");  //&First Page';
  cxSetResourceString(&_sdxMenuGotoPagePrev ,"");  //&Previous Page';
  cxSetResourceString(&_sdxMenuGotoPageNext ,"");  //&Next Page';
  cxSetResourceString(&_sdxMenuGotoPageLast ,"");  //&Last Page';
  cxSetResourceString(&_sdxMenuActivePage ,"");  //&Active Page:';

  cxSetResourceString(&_sdxMenuFormat ,"");  //F&ormat';
  cxSetResourceString(&_sdxMenuFormatHeaderAndFooter ,"");  //&Header and Footer';
  cxSetResourceString(&_sdxMenuFormatAutoTextEntries ,"");  //&Auto Text Entries...';
  cxSetResourceString(&_sdxMenuFormatDateTime ,"");  //Date And &Time...';
  cxSetResourceString(&_sdxMenuFormatPageNumbering ,"");  //Page &Numbering...';
  cxSetResourceString(&_sdxMenuFormatPageBackground ,"");  //Bac&kground...';
  cxSetResourceString(&_sdxMenuFormatShrinkToPage ,"");  //&Fit to Page';
  cxSetResourceString(&_sdxMenuShowEmptyPages ,"");  //Show &Empty Pages';
  cxSetResourceString(&_sdxMenuFormatHFBackground ,"");  //Header/Footer Background...';
  cxSetResourceString(&_sdxMenuFormatHFClear ,"");  //Clear Text';

  cxSetResourceString(&_sdxMenuTools ,"");  //&Tools';
  cxSetResourceString(&_sdxMenuToolsCustomize ,"");  //&Customize...';
  cxSetResourceString(&_sdxMenuToolsOptions ,"");  //&Options...';

  cxSetResourceString(&_sdxMenuHelp ,"");  //&Help';
  cxSetResourceString(&_sdxMenuHelpTopics ,"");  //Help &Topics...';
  cxSetResourceString(&_sdxMenuHelpAbout ,"");  //&About...';

  cxSetResourceString(&_sdxMenuShortcutPreview ,"");  //Preview';
  cxSetResourceString(&_sdxMenuShortcutAutoText ,"");  //AutoText';

  cxSetResourceString(&_sdxMenuBuiltInMenus ,"");  //Built-in Menus';
  cxSetResourceString(&_sdxMenuShortCutMenus ,"");  //Shortcut Menus';
  cxSetResourceString(&_sdxMenuNewMenu ,"");  //New Menu';
/*
  { Hints }
  
  cxSetResourceString(&_sdxHintFileDesign ,"");  //Design Report';
  cxSetResourceString(&_sdxHintFilePrint ,"");  //Print';
  cxSetResourceString(&_sdxHintFilePrintDialog ,"");  //Print Dialog';
  cxSetResourceString(&_sdxHintFilePageSetup ,"");  //Page Setup';
  cxSetResourceString(&_sdxHintFileExit ,"");  //Close Preview';

  cxSetResourceString(&_sdxHintEditFind ,"");  //Find';
  cxSetResourceString(&_sdxHintEditFindNext ,"");  //Find Next';
  cxSetResourceString(&_sdxHintEditReplace ,"");  //Replace';

  cxSetResourceString(&_sdxHintInsertEditAutoTextEntries ,"");  //Edit AutoText Entries';
  cxSetResourceString(&_sdxHintInsertPageNumber ,"");  //Insert Page Number';
  cxSetResourceString(&_sdxHintInsertTotalPages ,"");  //Insert Number of Pages';
  cxSetResourceString(&_sdxHintInsertPageOfPages ,"");  //Insert Page Number of Pages';
  cxSetResourceString(&_sdxHintInsertDateTime ,"");  //Insert Date and Time';
  cxSetResourceString(&_sdxHintInsertDate ,"");  //Insert Date';
  cxSetResourceString(&_sdxHintInsertTime ,"");  //Insert Time';
  cxSetResourceString(&_sdxHintInsertUserName ,"");  //Insert User Name';
  cxSetResourceString(&_sdxHintInsertMachineName ,"");  //Insert Machine Name';

  cxSetResourceString(&_sdxHintViewMargins ,"");  //View Margins';
  cxSetResourceString(&_sdxHintViewLargeButtons ,"");  //View Large Buttons';
  cxSetResourceString(&_sdxHintViewMarginsStatusBar ,"");  //View Margins Status Bar';
  cxSetResourceString(&_sdxHintViewPagesStatusBar ,"");  //View Page Status Bar';
  cxSetResourceString(&_sdxHintViewPagesHeaders ,"");  //View Page Header';
  cxSetResourceString(&_sdxHintViewPagesFooters ,"");  //View Page Footer';
  cxSetResourceString(&_sdxHintViewSwitchToLeftPart ,"");  //Switch to Left Header/Footer Part';
  cxSetResourceString(&_sdxHintViewSwitchToRightPart ,"");  //Switch to Right Header/Footer Part';
  cxSetResourceString(&_sdxHintViewSwitchToCenterPart ,"");  //Switch to Center Header/Footer Part';
  cxSetResourceString(&_sdxHintViewHFSwitchHeaderFooter ,"");  //Switch Between Header and Footer';
  cxSetResourceString(&_sdxHintViewHFClose ,"");  //Close';

  cxSetResourceString(&_sdxHintViewZoom ,"");  //Zoom';
  cxSetResourceString(&_sdxHintZoomPercent100 ,"");  //Zoom 100%';
  cxSetResourceString(&_sdxHintZoomPageWidth ,"");  //Zoom Page Width';
  cxSetResourceString(&_sdxHintZoomWholePage ,"");  //Whole Page';
  cxSetResourceString(&_sdxHintZoomTwoPages ,"");  //Two Pages';
  cxSetResourceString(&_sdxHintZoomFourPages ,"");  //Four Pages';
  cxSetResourceString(&_sdxHintZoomMultiplyPages ,"");  //Multiple Pages';
  cxSetResourceString(&_sdxHintZoomWidenToSourceWidth ,"");  //Widen To Source Width';
  cxSetResourceString(&_sdxHintZoomSetup ,"");  //Setup Zoom Factor';

  cxSetResourceString(&_sdxHintFormatDateTime ,"");  //Format Date and Time';
  cxSetResourceString(&_sdxHintFormatPageNumbering ,"");  //Format Page Number';
  cxSetResourceString(&_sdxHintFormatPageBackground ,"");  //Background';
  cxSetResourceString(&_sdxHintFormatShrinkToPage ,"");  //Fit To Page';
  cxSetResourceString(&_sdxHintFormatHFBackground ,"");  //Header/Footer Background';
  cxSetResourceString(&_sdxHintFormatHFClear ,"");  //Clear Header/Footer Text';

  cxSetResourceString(&_sdxHintGotoPageFirst ,"");  //First Page';
  cxSetResourceString(&_sdxHintGotoPagePrev ,"");  //Previous Page';
  cxSetResourceString(&_sdxHintGotoPageNext ,"");  //Next Page';
  cxSetResourceString(&_sdxHintGotoPageLast ,"");  //Last Page';
  cxSetResourceString(&_sdxHintActivePage ,"");  //Active Page';

  cxSetResourceString(&_sdxHintToolsCustomize ,"");  //Customize Toolbars';
  cxSetResourceString(&_sdxHintToolsOptions ,"");  //Options';

  cxSetResourceString(&_sdxHintHelpTopics ,"");  //Help Topics';
  cxSetResourceString(&_sdxHintHelpAbout ,"");  //About';

  cxSetResourceString(&_sdxPopupMenuLargeButtons ,"");  //&Large Buttons';
  cxSetResourceString(&_sdxPopupMenuFlatButtons ,"");  //&Flat Buttons';

  cxSetResourceString(&_sdxPaperSize ,"");  //Paper Size:';
  cxSetResourceString(&_sdxStatus ,"");  //Status:';
  cxSetResourceString(&_sdxStatusReady ,"");  //Ready';
  cxSetResourceString(&_sdxStatusPrinting ,"");  //Printing. Completed %d page(s)';
  cxSetResourceString(&_sdxStatusGenerateReport ,"");  //Generating Report. Completed %d%%';

  cxSetResourceString(&_sdxHintDoubleClickForChangePaperSize ,"");  //Double Click for Change Paper Size';
  cxSetResourceString(&_sdxHintDoubleClickForChangeMargins ,"");  //Double Click for Change Margins';

  { Date&Time Formats Dialog }
  
  cxSetResourceString(&_sdxDTFormatsCaption ,"");  //Date and Time';
  cxSetResourceString(&_sdxDTFormatsAvailableDateFormats ,"");  //&Available Date Formats:';
  cxSetResourceString(&_sdxDTFormatsAvailableTimeFormats ,"");  //Available &Time Formats:';
  cxSetResourceString(&_sdxDTFormatsAutoUpdate ,"");  //&Update Automatically';
  cxSetResourceString(&_sdxDTFormatsChangeDefaultFormat =
    'Do you want to change the default date and time formats to match "%s"  - "%s" ?';

  { PageNumber Formats Dialog }

  cxSetResourceString(&_sdxPNFormatsCaption ,"");  //Page Number Format';
  cxSetResourceString(&_sdxPageNumbering ,"");  //Page Numbering';
  cxSetResourceString(&_sdxPNFormatsNumberFormat ,"");  //Number &Format:';
  cxSetResourceString(&_sdxPNFormatsContinueFromPrevious ,"");  //&Continue from Previous Section';
  cxSetResourceString(&_sdxPNFormatsStartAt ,"");  //Start &At:';
  cxSetResourceString(&_sdxPNFormatsChangeDefaultFormat =
    'Do you want to change the default Page numbering format to match "%s" ?';

  { Zoom Dialog }
  
  cxSetResourceString(&_sdxZoomDlgCaption ,"");  //Zoom';
  cxSetResourceString(&_sdxZoomDlgZoomTo ,"");  // Zoom To ';
  cxSetResourceString(&_sdxZoomDlgPageWidth ,"");  //Page &Width';
  cxSetResourceString(&_sdxZoomDlgWholePage ,"");  //W&hole Page';
  cxSetResourceString(&_sdxZoomDlgTwoPages ,"");  //&Two Pages';
  cxSetResourceString(&_sdxZoomDlgFourPages ,"");  //&Four Pages';
  cxSetResourceString(&_sdxZoomDlgManyPages ,"");  //&Many Pages:';
  cxSetResourceString(&_sdxZoomDlgPercent ,"");  //P&ercent:';
  cxSetResourceString(&_sdxZoomDlgPreview ,"");  // Preview ';
  cxSetResourceString(&_sdxZoomDlgFontPreview ,"");  // 12pt Times New Roman ';
  cxSetResourceString(&_sdxZoomDlgFontPreviewString ,"");  //AaBbCcDdEeXxYyZz';

  { Select page X x Y }
  
  cxSetResourceString(&_sdxPages ,"");  //Pages';
  cxSetResourceString(&_sdxCancel ,"");  //Cancel';

  { Preferences dialog }
  
  cxSetResourceString(&_sdxPreferenceDlgCaption ,"");  //Options';
  cxSetResourceString(&_sdxPreferenceDlgTab1 ,"");  //&General';
  cxSetResourceString(&_sdxPreferenceDlgTab2 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab3 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab4 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab5 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab6 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab7 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab8 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab9 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgTab10 ,"");  //';
  cxSetResourceString(&_sdxPreferenceDlgShow ,"");  // &Show ';
  cxSetResourceString(&_sdxPreferenceDlgMargins ,"");  //&Margins ';
  cxSetResourceString(&_sdxPreferenceDlgMarginsHints ,"");  //Margins &Hints';
  cxSetResourceString(&_sdxPreferenceDlgMargingWhileDragging ,"");  //Margins Hints While &Dragging';
  cxSetResourceString(&_sdxPreferenceDlgLargeBtns ,"");  //&Large Toolbar Buttons';
  cxSetResourceString(&_sdxPreferenceDlgFlatBtns ,"");  //&Flat Toolbar Buttons';
  cxSetResourceString(&_sdxPreferenceDlgMarginsColor ,"");  //Margins &Color:';
  cxSetResourceString(&_sdxPreferenceDlgMeasurementUnits ,"");  //Measurement &Units:';
  cxSetResourceString(&_sdxPreferenceDlgSaveForRunTimeToo ,"");  //Save for &RunTime too';
  cxSetResourceString(&_sdxPreferenceDlgZoomScroll ,"");  //&Zoom on roll with IntelliMouse';
  cxSetResourceString(&_sdxPreferenceDlgZoomStep ,"");  //Zoom Ste&p:';
  
  { Page Setup }
  
  cxSetResourceString(&_sdxCloneStyleCaptionPrefix ,"");  //Copy (%d) of ';
  cxSetResourceString(&_sdxInvalideStyleCaption ,"");  //The style name "%s" already exists. Please supply another name.';

  cxSetResourceString(&_sdxPageSetupCaption ,"");  //Page Setup';
  cxSetResourceString(&_sdxStyleName ,"");  //Style &Name:';

  cxSetResourceString(&_sdxPage ,"");  //&Page';
  cxSetResourceString(&_sdxMargins ,"");  //&Margins';
  cxSetResourceString(&_sdxHeaderFooter ,"");  //&Header\Footer';
  cxSetResourceString(&_sdxScaling ,"");  //&Scaling';

  cxSetResourceString(&_sdxPaper ,"");  // Paper ';
  cxSetResourceString(&_sdxPaperType ,"");  //T&ype';
  cxSetResourceString(&_sdxPaperDimension ,"");  //Dimension';
  cxSetResourceString(&_sdxPaperWidth ,"");  //&Width:';
  cxSetResourceString(&_sdxPaperHeight ,"");  //H&eight:';
  cxSetResourceString(&_sdxPaperSource ,"");  //Paper so&urce';

  cxSetResourceString(&_sdxOrientation ,"");  // Orientation ';
  cxSetResourceString(&_sdxPortrait ,"");  //P&ortrait';
  cxSetResourceString(&_sdxLandscape ,"");  //&Landscape';
  cxSetResourceString(&_sdxPrintOrder ,"");  // Print Order ';
  cxSetResourceString(&_sdxDownThenOver ,"");  //&Down, then over';
  cxSetResourceString(&_sdxOverThenDown ,"");  //O&ver, then down';
  cxSetResourceString(&_sdxShading ,"");  // Shading ';
  cxSetResourceString(&_sdxPrintUsingGrayShading ,"");  //Print using &gray shading';

  cxSetResourceString(&_sdxCenterOnPage ,"");  //Center on page';
  cxSetResourceString(&_sdxHorizontally ,"");  //Hori&zontally';
  cxSetResourceString(&_sdxVertically ,"");  //&Vertically';

  cxSetResourceString(&_sdxHeader ,"");  //Header ';
  cxSetResourceString(&_sdxBtnHeaderFont ,"");  //&Font...';
  cxSetResourceString(&_sdxBtnHeaderBackground ,"");  //&Background';
  cxSetResourceString(&_sdxFooter ,"");  //Footer ';
  cxSetResourceString(&_sdxBtnFooterFont ,"");  //Fo&nt...';
  cxSetResourceString(&_sdxBtnFooterBackground ,"");  //Back&ground';

  cxSetResourceString(&_sdxTop ,"");  //&Top:';
  cxSetResourceString(&_sdxLeft ,"");  //&Left:';
  cxSetResourceString(&_sdxRight ,"");  //Ri&ght:';
  cxSetResourceString(&_sdxBottom ,"");  //&Bottom:';
  cxSetResourceString(&_sdxHeader2 ,"");  //H&eader:';
  cxSetResourceString(&_sdxFooter2 ,"");  //Foote&r:';

  cxSetResourceString(&_sdxAlignment ,"");  //Alignment';
  cxSetResourceString(&_sdxVertAlignment ,"");  // Vertical Alignment ';
  cxSetResourceString(&_sdxReverseOnEvenPages ,"");  //&Reverse on even pages';

  cxSetResourceString(&_sdxAdjustTo ,"");  //&Adjust To:';
  cxSetResourceString(&_sdxFitTo ,"");  //&Fit To:';
  cxSetResourceString(&_sdxPercentOfNormalSize ,"");  //% normal size';
  cxSetResourceString(&_sdxPagesWideBy ,"");  //page(s) &wide by';
  cxSetResourceString(&_sdxTall ,"");  //&tall';

  cxSetResourceString(&_sdxOf ,"");  //Of';
  cxSetResourceString(&_sdxLastPrinted ,"");  //Last Printed ';
  cxSetResourceString(&_sdxFileName ,"");  //Filename ';
  cxSetResourceString(&_sdxFileNameAndPath ,"");  //Filename and path ';
  cxSetResourceString(&_sdxPrintedBy ,"");  //Printed By ';
  cxSetResourceString(&_sdxPrintedOn ,"");  //Printed On ';
  cxSetResourceString(&_sdxCreatedBy ,"");  //Created By ';
  cxSetResourceString(&_sdxCreatedOn ,"");  //Created On ';
  cxSetResourceString(&_sdxConfidential ,"");  //Confidential';

  { HF function }
  
  cxSetResourceString(&_sdxHFFunctionNameUnknown ,"");  //Unknown';
  cxSetResourceString(&_sdxHFFunctionNamePageNumber ,"");  //Page Number';
  cxSetResourceString(&_sdxHFFunctionNameTotalPages ,"");  //Total Pages';
  cxSetResourceString(&_sdxHFFunctionNamePageOfPages ,"");  //Page # of Pages #';
  cxSetResourceString(&_sdxHFFunctionNameDateTime ,"");  //Date and Time';
  cxSetResourceString(&_sdxHFFunctionNameDate ,"");  //Date';
  cxSetResourceString(&_sdxHFFunctionNameTime ,"");  //Time';
  cxSetResourceString(&_sdxHFFunctionNameUserName ,"");  //User Name';
  cxSetResourceString(&_sdxHFFunctionNameMachineName ,"");  //Machine Name';

  cxSetResourceString(&_sdxHFFunctionHintPageNumber ,"");  //Page Number';
  cxSetResourceString(&_sdxHFFunctionHintTotalPages ,"");  //Total Pages';
  cxSetResourceString(&_sdxHFFunctionHintPageOfPages ,"");  //Page # of Pages #';
  cxSetResourceString(&_sdxHFFunctionHintDateTime ,"");  //Date and Time Printed';
  cxSetResourceString(&_sdxHFFunctionHintDate ,"");  //Date Printed';
  cxSetResourceString(&_sdxHFFunctionHintTime ,"");  //Time Printed';
  cxSetResourceString(&_sdxHFFunctionHintUserName ,"");  //User Name';
  cxSetResourceString(&_sdxHFFunctionHintMachineName ,"");  //Machine Name';

  cxSetResourceString(&_sdxHFFunctionTemplatePageNumber ,"");  //Page #';
  cxSetResourceString(&_sdxHFFunctionTemplateTotalPages ,"");  //Total Pages';
  cxSetResourceString(&_sdxHFFunctionTemplatePageOfPages ,"");  //Page # of Pages #';
  cxSetResourceString(&_sdxHFFunctionTemplateDateTime ,"");  //Date & Time Printed';
  cxSetResourceString(&_sdxHFFunctionTemplateDate ,"");  //Date Printed';
  cxSetResourceString(&_sdxHFFunctionTemplateTime ,"");  //Time Printed';
  cxSetResourceString(&_sdxHFFunctionTemplateUserName ,"");  //User Name';
  cxSetResourceString(&_sdxHFFunctionTemplateMachineName ,"");  //Machine Name';

  { Designer strings }
  
  { Months }
  
  cxSetResourceString(&_sdxJanuary ,"");  //January';
  cxSetResourceString(&_sdxFebruary ,"");  //February';
  cxSetResourceString(&_sdxMarch ,"");  //March';
  cxSetResourceString(&_sdxApril ,"");  //April';
  cxSetResourceString(&_sdxMay ,"");  //May';
  cxSetResourceString(&_sdxJune ,"");  //June';
  cxSetResourceString(&_sdxJuly ,"");  //July';
  cxSetResourceString(&_sdxAugust ,"");  //August';
  cxSetResourceString(&_sdxSeptember ,"");  //September';
  cxSetResourceString(&_sdxOctober ,"");  //October';
  cxSetResourceString(&_sdxNovember ,"");  //November';
  cxSetResourceString(&_sdxDecember ,"");  //December';

  cxSetResourceString(&_sdxEast ,"");  //East';
  cxSetResourceString(&_sdxWest ,"");  //West';
  cxSetResourceString(&_sdxSouth ,"");  //South';
  cxSetResourceString(&_sdxNorth ,"");  //North';

  cxSetResourceString(&_sdxTotal ,"");  //Total';

  { dxFlowChart }
  
  cxSetResourceString(&_sdxPlan ,"");  //Plan';
  cxSetResourceString(&_sdxSwimmingPool ,"");  //Swimming-pool';
  cxSetResourceString(&_sdxAdministration ,"");  //Administration';
  cxSetResourceString(&_sdxPark ,"");  //Park';
  cxSetResourceString(&_sdxCarParking ,"");  //Car-Parking';

  { dxOrgChart }
  
  cxSetResourceString(&_sdxCorporateHeadquarters ,"");  //Corporate' + #13#10 + 'Headquarters';
  cxSetResourceString(&_sdxSalesAndMarketing ,"");  //Sales and' + #13#10 + 'Marketing';
  cxSetResourceString(&_sdxEngineering ,"");  //Engineering';
  cxSetResourceString(&_sdxFieldOfficeCanada ,"");  //Field Office:' + #13#10 + 'Canada';

  { dxMasterView }
  
  cxSetResourceString(&_sdxOrderNoCaption ,"");  //OrderNo';
  cxSetResourceString(&_sdxNameCaption ,"");  //Name';
  cxSetResourceString(&_sdxCountCaption ,"");  //Count';
  cxSetResourceString(&_sdxCompanyCaption ,"");  //Company';
  cxSetResourceString(&_sdxAddressCaption ,"");  //Address';
  cxSetResourceString(&_sdxPriceCaption ,"");  //Price';
  cxSetResourceString(&_sdxCashCaption ,"");  //Cash';

  cxSetResourceString(&_sdxName1 ,"");  //Jennie Valentine';
  cxSetResourceString(&_sdxName2 ,"");  //Sam Hill';
  cxSetResourceString(&_sdxCompany1 ,"");  //Jennie Inc.';
  cxSetResourceString(&_sdxCompany2 ,"");  //Daimler-Chrysler AG';
  cxSetResourceString(&_sdxAddress1 ,"");  //123 Home Lane';
  cxSetResourceString(&_sdxAddress2 ,"");  //9333 Holmes Dr.';

  { dxTreeList }
  
  cxSetResourceString(&_sdxCountIs ,"");  //Count is: %d';
  cxSetResourceString(&_sdxRegular ,"");  //Regular';
  cxSetResourceString(&_sdxIrregular ,"");  //Irregular';

  cxSetResourceString(&_sdxTLBand ,"");  //Item Data';
  cxSetResourceString(&_sdxTLColumnName ,"");  //Name';
  cxSetResourceString(&_sdxTLColumnAxisymmetric ,"");  //Axisymmetric';
  cxSetResourceString(&_sdxTLColumnItemShape ,"");  //Shape';

  cxSetResourceString(&_sdxItemShapeAsText ,"");  //(Graphic)';

  cxSetResourceString(&_sdxItem1Name ,"");  //Cylinder';
  cxSetResourceString(&_sdxItem2Name ,"");  //Cone';
  cxSetResourceString(&_sdxItem3Name ,"");  //Pyramid';
  cxSetResourceString(&_sdxItem4Name ,"");  //Box';
  cxSetResourceString(&_sdxItem5Name ,"");  //Free Surface';

  cxSetResourceString(&_sdxItem1Description ,"");  //';
  cxSetResourceString(&_sdxItem2Description ,"");  //Axisymmetric geometry figure';
  cxSetResourceString(&_sdxItem3Description ,"");  //Axisymmetric geometry figure';
  cxSetResourceString(&_sdxItem4Description ,"");  //Acute-angled geometry figure';
  cxSetResourceString(&_sdxItem5Description ,"");  //';
  cxSetResourceString(&_sdxItem6Description ,"");  //';
  cxSetResourceString(&_sdxItem7Description ,"");  //Simple extrusion surface';

  { PS 2.3 }
  
  { Patterns common }

  cxSetResourceString(&_sdxPatternIsNotRegistered ,"");  //Pattern "%s" is not registered';

  { Excel edge patterns }
  
  cxSetResourceString(&_sdxSolidEdgePattern ,"");  //Solid';
  cxSetResourceString(&_sdxThinSolidEdgePattern ,"");  //Medium Solid';    
  cxSetResourceString(&_sdxMediumSolidEdgePattern ,"");  //Medium Solid';  
  cxSetResourceString(&_sdxThickSolidEdgePattern ,"");  //Thick Solid';
  cxSetResourceString(&_sdxDottedEdgePattern ,"");  //Dotted';
  cxSetResourceString(&_sdxDashedEdgePattern ,"");  //Dashed';
  cxSetResourceString(&_sdxDashDotDotEdgePattern ,"");  //Dash Dot Dot';
  cxSetResourceString(&_sdxDashDotEdgePattern ,"");  //Dash Dot';
  cxSetResourceString(&_sdxSlantedDashDotEdgePattern ,"");  //Slanted Dash Dot';
  cxSetResourceString(&_sdxMediumDashDotDotEdgePattern ,"");  //Medium Dash Dot Dot';
  cxSetResourceString(&_sdxHairEdgePattern ,"");  //Hair';
  cxSetResourceString(&_sdxMediumDashDotEdgePattern ,"");  //Medium Dash Dot';
  cxSetResourceString(&_sdxMediumDashedEdgePattern ,"");  //Medium Dashed';
  cxSetResourceString(&_sdxDoubleLineEdgePattern ,"");  //Double Line';

  { Excel fill patterns names} 
   
  cxSetResourceString(&_sdxSolidFillPattern ,"");  //Solid';
  cxSetResourceString(&_sdxGray75FillPattern ,"");  //75% Gray';
  cxSetResourceString(&_sdxGray50FillPattern ,"");  //50% Gray';
  cxSetResourceString(&_sdxGray25FillPattern ,"");  //25% Gray';
  cxSetResourceString(&_sdxGray125FillPattern ,"");  //12.5% Gray';
  cxSetResourceString(&_sdxGray625FillPattern ,"");  //6.25% Gray';
  cxSetResourceString(&_sdxHorizontalStripeFillPattern ,"");  //Horizontal Stripe';
  cxSetResourceString(&_sdxVerticalStripeFillPattern ,"");  //Vertical Stripe';
  cxSetResourceString(&_sdxReverseDiagonalStripeFillPattern ,"");  //Reverse Diagonal Stripe';
  cxSetResourceString(&_sdxDiagonalStripeFillPattern ,"");  //Diagonal Stripe';
  cxSetResourceString(&_sdxDiagonalCrossHatchFillPattern ,"");  //Diagonal Cross Hatch';
  cxSetResourceString(&_sdxThickCrossHatchFillPattern ,"");  //Thick Cross Hatch';
  cxSetResourceString(&_sdxThinHorizontalStripeFillPattern ,"");  //Thin Horizontal Stripe';
  cxSetResourceString(&_sdxThinVerticalStripeFillPattern ,"");  //Thin Vertical Stripe';
  cxSetResourceString(&_sdxThinReverseDiagonalStripeFillPattern ,"");  //Thin Reverse Diagonal Stripe';
  cxSetResourceString(&_sdxThinDiagonalStripeFillPattern ,"");  //Thin Diagonal Stripe';
  cxSetResourceString(&_sdxThinHorizontalCrossHatchFillPattern ,"");  //Thin Horizontal Cross Hatch';
  cxSetResourceString(&_sdxThinDiagonalCrossHatchFillPattern ,"");  //Thin Diagonal Cross Hatch';
  
  { cxSpreadSheet }
  
  cxSetResourceString(&_sdxShowRowAndColumnHeadings ,"");  //&Row and Column Headings';
  cxSetResourceString(&_sdxShowGridLines ,"");  //GridLines';
  cxSetResourceString(&_sdxSuppressSourceFormats ,"");  //&Suppress Source Formats';
  cxSetResourceString(&_sdxRepeatHeaderRowAtTop ,"");  //Repeat Header Row at Top'; 
  cxSetResourceString(&_sdxDataToPrintDoesNotExist = 
    'Cannot activate ReportLink because PrintingSystem did not find anything to print.';
 
  { Designer strings } 

  { Short names of month }
  
  cxSetResourceString(&_sdxJanuaryShort ,"");  //Jan';
  cxSetResourceString(&_sdxFebruaryShort ,"");  //Feb';
  cxSetResourceString(&_sdxMarchShort ,"");  //March';  
  cxSetResourceString(&_sdxAprilShort ,"");  //April';  
  cxSetResourceString(&_sdxMayShort ,"");  //May';    
  cxSetResourceString(&_sdxJuneShort ,"");  //June';    
  cxSetResourceString(&_sdxJulyShort ,"");  //July';
  cxSetResourceString(&_sdxAugustShort ,"");  //Aug';
  cxSetResourceString(&_sdxSeptemberShort ,"");  //Sept';
  cxSetResourceString(&_sdxOctoberShort ,"");  //Oct';
  cxSetResourceString(&_sdxNovemberShort ,"");  //Nov';
  cxSetResourceString(&_sdxDecemberShort ,"");  //Dec';

  { TreeView }
  
  cxSetResourceString(&_sdxTechnicalDepartment ,"");  //Technical Department';
  cxSetResourceString(&_sdxSoftwareDepartment ,"");  //Software Department';
  cxSetResourceString(&_sdxSystemProgrammers ,"");  //Core Developers';
  cxSetResourceString(&_sdxEndUserProgrammers ,"");  //GUI Developers';
  cxSetResourceString(&_sdxBetaTesters ,"");  //Beta Testers';
  cxSetResourceString(&_sdxHumanResourceDepartment ,"");  //Human Resource Department';

  { misc. }
  
  cxSetResourceString(&_sdxTreeLines ,"");  //&TreeLines';
  cxSetResourceString(&_sdxTreeLinesColor ,"");  //T&ree Line Color:';
  cxSetResourceString(&_sdxExpandButtons ,"");  //E&xpand Buttons';
  cxSetResourceString(&_sdxCheckMarks ,"");  //Check Marks';
  cxSetResourceString(&_sdxTreeEffects ,"");  //Tree Effects';
  cxSetResourceString(&_sdxAppearance ,"");  //Appearance';

  { Designer previews }
  
  { Localize if you want (they are used inside FormatReport dialog -> ReportPreview) }

  cxSetResourceString(&_sdxCarLevelCaption ,"");  //Cars';
  
  cxSetResourceString(&_sdxManufacturerBandCaption ,"");  //Manufacturer Data';  
  cxSetResourceString(&_sdxModelBandCaption ,"");  //Car Data';
  
  cxSetResourceString(&_sdxManufacturerNameColumnCaption ,"");  //Name';
  cxSetResourceString(&_sdxManufacturerLogoColumnCaption ,"");  //Logo';  
  cxSetResourceString(&_sdxManufacturerCountryColumnCaption ,"");  //Country';  
  cxSetResourceString(&_sdxCarModelColumnCaption ,"");  //Model';
  cxSetResourceString(&_sdxCarIsSUVColumnCaption ,"");  //SUV';
  cxSetResourceString(&_sdxCarPhotoColumnCaption ,"");  //Photo';  

  cxSetResourceString(&_sdxCarManufacturerName1 ,"");  //BMW';
  cxSetResourceString(&_sdxCarManufacturerName2 ,"");  //Ford';
  cxSetResourceString(&_sdxCarManufacturerName3 ,"");  //Audi';
  cxSetResourceString(&_sdxCarManufacturerName4 ,"");  //Land Rover';

  cxSetResourceString(&_sdxCarManufacturerCountry1 ,"");  //Germany';
  cxSetResourceString(&_sdxCarManufacturerCountry2 ,"");  //United States';
  cxSetResourceString(&_sdxCarManufacturerCountry3 ,"");  //Germany';
  cxSetResourceString(&_sdxCarManufacturerCountry4 ,"");  //United Kingdom';
  
  cxSetResourceString(&_sdxCarModel1 ,"");  //X5 4.8is';
  cxSetResourceString(&_sdxCarModel2 ,"");  //Excursion';
  cxSetResourceString(&_sdxCarModel3 ,"");  //S8 Quattro';
  cxSetResourceString(&_sdxCarModel4 ,"");  //G4 Challenge';
  
  cxSetResourceString(&_sdxTrue ,"");  //True';
  cxSetResourceString(&_sdxFalse ,"");  //False';  

  { PS 2.4 }
  
  { dxPrnDev.pas }
  
  cxSetResourceString(&_sdxAuto ,"");  //Auto';
  cxSetResourceString(&_sdxCustom ,"");  //Custom'; 
  cxSetResourceString(&_sdxEnv ,"");  //Env'; 

  { Grid 4 }

  cxSetResourceString(&_sdxLookAndFeelFlat ,"");  //Flat';
  cxSetResourceString(&_sdxLookAndFeelStandard ,"");  //Standard';
  cxSetResourceString(&_sdxLookAndFeelUltraFlat ,"");  //UltraFlat';    

  cxSetResourceString(&_sdxViewTab ,"");  //View';
  cxSetResourceString(&_sdxBehaviorsTab ,"");  //Behaviors';
  cxSetResourceString(&_sdxPreviewTab ,"");  //Preview';
  cxSetResourceString(&_sdxCardsTab ,"");  //Cards';

  cxSetResourceString(&_sdxFormatting ,"");  //Formatting';
  cxSetResourceString(&_sdxLookAndFeel ,"");  //Look and Feel';
  cxSetResourceString(&_sdxLevelCaption ,"");  //&Caption';
  cxSetResourceString(&_sdxFilterBar ,"");  //&Filter Bar';  
  cxSetResourceString(&_sdxRefinements ,"");  //Refinements';
  cxSetResourceString(&_sdxProcessSelection ,"");  //Process &Selection';
  cxSetResourceString(&_sdxProcessExactSelection ,"");  //Process E&xact Selection';
  cxSetResourceString(&_sdxExpanding ,"");  //Expanding';
  cxSetResourceString(&_sdxGroups ,"");  //&Groups';
  cxSetResourceString(&_sdxDetails ,"");  //&Details';
  cxSetResourceString(&_sdxStartFromActiveDetails ,"");  //Start from Active Details';
  cxSetResourceString(&_sdxOnlyActiveDetails ,"");  //Only Active Details';
  cxSetResourceString(&_sdxVisible ,"");  //&Visible';
  cxSetResourceString(&_sdxPreviewAutoHeight ,"");  //A&uto Height';
  cxSetResourceString(&_sdxPreviewMaxLineCount ,"");  //&Max Line Count: ';
  cxSetResourceString(&_sdxSizes ,"");  //Sizes';
  cxSetResourceString(&_sdxKeepSameWidth ,"");  //&Keep Same Width';
  cxSetResourceString(&_sdxKeepSameHeight ,"");  //Keep Same &Height';
  cxSetResourceString(&_sdxFraming ,"");  //Framing';  
  cxSetResourceString(&_sdxSpacing ,"");  //Spacing';
  cxSetResourceString(&_sdxShadow ,"");  //Shadow';
  cxSetResourceString(&_sdxDepth ,"");  //&Depth:';
  cxSetResourceString(&_sdxPosition ,"");  //&Position';
  cxSetResourceString(&_sdxPositioning ,"");  //Positioning';
  cxSetResourceString(&_sdxHorizontal ,"");  //H&orizontal:';
  cxSetResourceString(&_sdxVertical ,"");  //V&ertical:';

  cxSetResourceString(&_sdxSummaryFormat ,"");  //Count = 0';

  cxSetResourceString(&_sdxCannotUseOnEveryPageMode =
    'Cannot Use OnEveryPage Mode'+ #13#10 +
    #13#10 +
    'You should or(and)' + #13#10 +
    '  - Collapse all Master Records' + #13#10 +
    '  - Toggle "Unwrap" Option off on "Behaviors" Tab';

  cxSetResourceString(&_sdxIncorrectBandHeadersState =
    'Cannot Use BandHeaders OnEveryPage Mode' + #13#10 +
    #13#10 +
    'You should either:' + #13#10 +  
    '  - Set Caption OnEveryPage Option On' + #13#10 + 
    '  - Set Caption Visible Option Off';
  cxSetResourceString(&_sdxIncorrectHeadersState = 
    'Cannot Use Headers OnEveryPage Mode' + #13#10 +
    #13#10 +
    'You should either:' + #13#10 +  
    '  - Set Caption and Band OnEveryPage Option On' + #13#10 +
    '  - Set Caption and Band Visible Option Off';
  cxSetResourceString(&_sdxIncorrectFootersState = 
   'Cannot Use Footers OnEveryPage Mode' + #13#10 +
    #13#10 +
    'You should either:' + #13#10 +
    '  - Set FilterBar OnEveryPage Option On' + #13#10 +
    '  - Set FilterBar Visible Option Off';

  cxSetResourceString(&_sdxCharts ,"");  //Charts';
  
  { PS 3 }

  cxSetResourceString(&_sdxTPicture ,"");  //TPicture';
  cxSetResourceString(&_sdxCopy ,"");  //&Copy';
  cxSetResourceString(&_sdxSave ,"");  //&Save...';
  cxSetResourceString(&_sdxBaseStyle ,"");  //Base Style';
  
  cxSetResourceString(&_sdxComponentAlreadyExists ,"");  //Component named "%s" already exists';
  cxSetResourceString(&_sdxInvalidComponentName ,"");  //"%s" is not a valid component name';
  
  { shapes }
 
  cxSetResourceString(&_sdxRectangle ,"");  //Rectangle';
  cxSetResourceString(&_sdxSquare ,"");  //Square';
  cxSetResourceString(&_sdxEllipse ,"");  //Ellipse';
  cxSetResourceString(&_sdxCircle ,"");  //Circle';
  cxSetResourceString(&_sdxRoundRect ,"");  //RoundRect';
  cxSetResourceString(&_sdxRoundSquare ,"");  //RoundSquare';

  { standard pattern names}

  cxSetResourceString(&_sdxHorizontalFillPattern ,"");  //Horizontal';
  cxSetResourceString(&_sdxVerticalFillPattern ,"");  //Vertical';
  cxSetResourceString(&_sdxFDiagonalFillPattern ,"");  //FDiagonal';
  cxSetResourceString(&_sdxBDiagonalFillPattern ,"");  //BDiagonal';
  cxSetResourceString(&_sdxCrossFillPattern ,"");  //Cross';
  cxSetResourceString(&_sdxDiagCrossFillPattern ,"");  //DiagCros';
  
  { explorer }
                                                             
  cxSetResourceString(&_sdxCyclicIDReferences ,"");  //Cyclic ID references %s and %s';
  cxSetResourceString(&_sdxLoadReportDataToFileTitle ,"");  //Load Report';
  cxSetResourceString(&_sdxSaveReportDataToFileTitle ,"");  //Save Report As';
  cxSetResourceString(&_sdxInvalidExternalStorage ,"");  //Invalid External Storage';
  cxSetResourceString(&_sdxLinkIsNotIncludedInUsesClause = 
    'ReportFile contains ReportLink "%0:s"' + #13#10 + 
    'Unit with declaration of "%0:s" must be included in uses clause';
  cxSetResourceString(&_sdxInvalidStorageVersion ,"");  //Invalid Storage Verison: %d';
  cxSetResourceString(&_sdxPSReportFiles ,"");  //Report Files';
  cxSetResourceString(&_sdxReportFileLoadError = 
    'Cannot load Report File "%s".' + #13#10 + 
    'File is corrupted or is locked by another User or Application.' + #13#10 + 
    #13#10 +
    'Original Report will be restored.';
  
  cxSetResourceString(&_sdxNone ,"");  //(None)';
  cxSetResourceString(&_sdxReportDocumentIsCorrupted ,"");  //(File is not a ReportDocument or Corrupted)';
  
  cxSetResourceString(&_sdxCloseExplorerHint ,"");  //Close Explorer';
  cxSetResourceString(&_sdxExplorerCaption ,"");  //Explorer';
  cxSetResourceString(&_sdxExplorerRootFolderCaption ,"");  //Root';
  cxSetResourceString(&_sdxNewExplorerFolderItem ,"");  //New Folder';
  cxSetResourceString(&_sdxCopyOfItem ,"");  //Copy of ';
  cxSetResourceString(&_sdxReportExplorer ,"");  //Report Explorer';
                                
  cxSetResourceString(&_sdxDataLoadErrorText ,"");  //Cannot load Report Data';
  cxSetResourceString(&_sdxDBBasedExplorerItemDataLoadError = 
    'Cannot load Report Data.' + #13#10 + 
    'Data are corrupted or are locked';
  cxSetResourceString(&_sdxFileBasedExplorerItemDataLoadError = 
    'Cannot load Report Data.' + #13#10 + 
    'File is corruted or is locked by another User or Application';
  cxSetResourceString(&_sdxDeleteNonEmptyFolderMessageText ,"");  //Folder "%s" is not Empty. Delete anyway?';
  cxSetResourceString(&_sdxDeleteFolderMessageText ,"");  //Delete Folder "%s" ?';
  cxSetResourceString(&_sdxDeleteItemMessageText ,"");  //Delete Item "%s" ?';
  cxSetResourceString(&_sdxCannotRenameFolderText ,"");  //Cannot rename folder "%s". A folder with name "%s" already exists. Specify a different name.';
  cxSetResourceString(&_sdxCannotRenameItemText ,"");  //Cannot rename item "%s". An item with name "%s" already exists. Specify a different name.';
  cxSetResourceString(&_sdxOverwriteFolderMessageText = 
    'This folder "%s" already contains folder named "%s".' + #13#10 + 
    #13#10 + 
    'If the items in existing folder have the same name as items in the' + #13#10 + 
    'folder you are moving or copying, they will be replaced. Do you still?' + #13#10 +
    'want to move or copy the folder?';
  cxSetResourceString(&_sdxOverwriteItemMessageText = 
    'This Folder "%s" already contains item named "%s".' + #13#10 + 
    #13#10 + 
    'Would you like to overwrite existing item?';
  cxSetResourceString(&_sdxSelectNewRoot ,"");  //Select new Root Directory where the Reports will be stored';
  cxSetResourceString(&_sdxInvalidFolderName ,"");  //Invalid Folder Name "%s"';
  cxSetResourceString(&_sdxInvalidReportName ,"");  //Invalid Report Name "%s"';
  
  cxSetResourceString(&_sdxExplorerBar ,"");  //Explorer';

  cxSetResourceString(&_sdxMenuFileSave ,"");  //&Save';
  cxSetResourceString(&_sdxMenuFileSaveAs ,"");  //Save &As...';
  cxSetResourceString(&_sdxMenuFileLoad ,"");  //&Load';
  cxSetResourceString(&_sdxMenuFileClose ,"");  //U&nload';
  cxSetResourceString(&_sdxHintFileSave ,"");  //Save Report';
  cxSetResourceString(&_sdxHintFileSaveAs ,"");  //Save Report As';
  cxSetResourceString(&_sdxHintFileLoad ,"");  //Load Report';
  cxSetResourceString(&_sdxHintFileClose ,"");  //Unload Report';
  
  cxSetResourceString(&_sdxMenuExplorer ,"");  //E&xplorer';
  cxSetResourceString(&_sdxMenuExplorerCreateFolder ,"");  //Create &Folder';
  cxSetResourceString(&_sdxMenuExplorerDelete ,"");  //&Delete...';
  cxSetResourceString(&_sdxMenuExplorerRename ,"");  //Rena&me';
  cxSetResourceString(&_sdxMenuExplorerProperties ,"");  //&Properties...';
  cxSetResourceString(&_sdxMenuExplorerRefresh ,"");  //Refresh';
  cxSetResourceString(&_sdxMenuExplorerChangeRootPath ,"");  //Change Root...';
  cxSetResourceString(&_sdxMenuExplorerSetAsRoot ,"");  //Set As Root';
  cxSetResourceString(&_sdxMenuExplorerGoToUpOneLevel ,"");  //Up One Level';

  cxSetResourceString(&_sdxHintExplorerCreateFolder ,"");  //Create New Folder';
  cxSetResourceString(&_sdxHintExplorerDelete ,"");  //Delete';
  cxSetResourceString(&_sdxHintExplorerRename ,"");  //Rename';
  cxSetResourceString(&_sdxHintExplorerProperties ,"");  //Properties';
  cxSetResourceString(&_sdxHintExplorerRefresh ,"");  //Refresh';
  cxSetResourceString(&_sdxHintExplorerChangeRootPath ,"");  //Change Root';
  cxSetResourceString(&_sdxHintExplorerSetAsRoot ,"");  //Set Current Folder as Root';
  cxSetResourceString(&_sdxHintExplorerGoToUpOneLevel ,"");  //Up One Level';
  
  cxSetResourceString(&_sdxMenuViewExplorer ,"");  //E&xplorer';
  cxSetResourceString(&_sdxHintViewExplorer ,"");  //Show Explorer';

  cxSetResourceString(&_sdxSummary ,"");  //Summary';
  cxSetResourceString(&_sdxCreator ,"");  //Creato&r:';
  cxSetResourceString(&_sdxCreationDate ,"");  //Create&d:';
 
  cxSetResourceString(&_sdxMenuViewThumbnails ,"");  //Th&umbnails';
  cxSetResourceString(&_sdxMenuThumbnailsLarge ,"");  //&Large Thumbnails';
  cxSetResourceString(&_sdxMenuThumbnailsSmall ,"");  //&Small Thumbnails';
  
  cxSetResourceString(&_sdxHintViewThumbnails ,"");  //Show Thumbnails';
  cxSetResourceString(&_sdxHintThumbnailsLarge ,"");  //Switch to large thumbnails';
  cxSetResourceString(&_sdxHintThumbnailsSmall ,"");  //Switch to small thumbnails';
    
  cxSetResourceString(&_sdxMenuFormatTitle ,"");  //T&itle...';
  cxSetResourceString(&_sdxHintFormatTitle ,"");  //Format Report Title';

  cxSetResourceString(&_sdxHalf ,"");  //Half';
  cxSetResourceString(&_sdxPredefinedFunctions ,"");  // Predefined Functions '; // dxPgsDlg.pas
  cxSetResourceString(&_sdxZoomParameters ,"");  // Zoom &Parameters ';          // dxPSPrvwOpt.pas

  cxSetResourceString(&_sdxWrapData ,"");  //&Wrap Data';

  cxSetResourceString(&_sdxMenuShortcutExplorer ,"");  //Explorer';  
  cxSetResourceString(&_sdxExplorerToolBar ,"");  //Explorer';

  cxSetResourceString(&_sdxMenuShortcutThumbnails ,"");  //Thumbnails';
  
  { TreeView New}
  
  cxSetResourceString(&_sdxButtons ,"");  //Buttons';

  { ListView }

  cxSetResourceString(&_sdxBtnHeadersFont ,"");  //&Headers Font...';
  cxSetResourceString(&_sdxHeadersTransparent ,"");  //Transparent &Headers';
  cxSetResourceString(&_sdxHintListViewDesignerMessage ,"");  // Most Options Are Being Taken Into Account Only In Detailed View';
  cxSetResourceString(&_sdxColumnHeaders ,"");  //&Column Headers';
  
  { Group LookAndFeel Names }

  cxSetResourceString(&_sdxReportGroupNullLookAndFeel ,"");  //Null';
  cxSetResourceString(&_sdxReportGroupStandardLookAndFeel ,"");  //Standard';
  cxSetResourceString(&_sdxReportGroupOfficeLookAndFeel ,"");  //Office';  
  cxSetResourceString(&_sdxReportGroupWebLookAndFeel ,"");  //Web';
  
  cxSetResourceString(&_sdxLayoutGroupDefaultCaption ,"");  //Layout Group';
  cxSetResourceString(&_sdxLayoutItemDefaultCaption ,"");  //Layout Item';

  { Designer Previews}

  { Localize if you want (they are used inside FormatReport dialog -> ReportPreview) }
    
  cxSetResourceString(&_sdxCarManufacturerName5 ,"");  //DaimlerChrysler AG';
  cxSetResourceString(&_sdxCarManufacturerCountry5 ,"");  //Germany';
  cxSetResourceString(&_sdxCarModel5 ,"");  //Maybach 62';

  cxSetResourceString(&_sdxLuxurySedans ,"");  //Luxury Sedans';
  cxSetResourceString(&_sdxCarManufacturer ,"");  //Manufacturer';
  cxSetResourceString(&_sdxCarModel ,"");  //Model';
  cxSetResourceString(&_sdxCarEngine ,"");  //Engine';
  cxSetResourceString(&_sdxCarTransmission ,"");  //Transmission';
  cxSetResourceString(&_sdxCarTires ,"");  //Tires';
  cxSetResourceString(&_sdx760V12Manufacturer ,"");  //BMW';
  cxSetResourceString(&_sdx760V12Model ,"");  //760Li V12';
  cxSetResourceString(&_sdx760V12Engine ,"");  //6.0L DOHC V12 438 HP 48V DI Valvetronic 12-cylinder engine with 6.0-liter displacement, dual overhead cam valvetrain';
  cxSetResourceString(&_sdx760V12Transmission ,"");  //Elec 6-Speed Automatic w/Steptronic';
  cxSetResourceString(&_sdx760V12Tires ,"");  //P245/45R19 Fr - P275/40R19 Rr Performance. Low Profile tires with 245mm width, 19.0" rim';
      
  { Styles }

  cxSetResourceString(&_sdxBandBackgroundStyle ,"");  //BandBackground';
  cxSetResourceString(&_sdxBandHeaderStyle ,"");  //BandHeader';
  cxSetResourceString(&_sdxCaptionStyle ,"");  //Caption';
  cxSetResourceString(&_sdxCardCaptionRowStyle ,"");  //Card Caption Row';
  cxSetResourceString(&_sdxCardRowCaptionStyle ,"");  //Card Row Caption';
  cxSetResourceString(&_sdxCategoryStyle ,"");  //Category';
  cxSetResourceString(&_sdxContentStyle ,"");  //Content';
  cxSetResourceString(&_sdxContentEvenStyle ,"");  //Content Even Rows';
  cxSetResourceString(&_sdxContentOddStyle ,"");  //Content Odd Rows';
  cxSetResourceString(&_sdxFilterBarStyle ,"");  //Filter Bar';
  cxSetResourceString(&_sdxFooterStyle ,"");  //Footer';
  cxSetResourceString(&_sdxFooterRowStyle ,"");  //Footer Row';
  cxSetResourceString(&_sdxGroupStyle ,"");  //Group';
  cxSetResourceString(&_sdxHeaderStyle ,"");  //Header';
  cxSetResourceString(&_sdxIndentStyle ,"");  //Indent';
  cxSetResourceString(&_sdxPreviewStyle ,"");  //Preview';
  cxSetResourceString(&_sdxSelectionStyle ,"");  //Selection';

  cxSetResourceString(&_sdxStyles ,"");  //Styles';
  cxSetResourceString(&_sdxStyleSheets ,"");  //Style Sheets';
  cxSetResourceString(&_sdxBtnTexture ,"");  //&Texture...';
  cxSetResourceString(&_sdxBtnTextureClear ,"");  //Cl&ear';
  cxSetResourceString(&_sdxBtnColor ,"");  //Co&lor...';
  cxSetResourceString(&_sdxBtnSaveAs ,"");  //Save &As...';
  cxSetResourceString(&_sdxBtnRename ,"");  //&Rename...';
  
  cxSetResourceString(&_sdxLoadBitmapDlgTitle ,"");  //Load Texture';
  
  cxSetResourceString(&_sdxDeleteStyleSheet ,"");  //Delete StyleSheet Named "%s"?';
  cxSetResourceString(&_sdxUnnamedStyleSheet ,"");  //Unnamed';
  cxSetResourceString(&_sdxCreateNewStyleQueryNamePrompt ,"");  //Enter New StyleSheet Name: ';
  cxSetResourceString(&_sdxStyleSheetNameAlreadyExists ,"");  //StyleSheet named "%s" Already Exists';

  cxSetResourceString(&_sdxCannotLoadImage ,"");  //Cannot Load Image "%s"';
  cxSetResourceString(&_sdxUseNativeStyles ,"");  //&Use Native Styles';
  cxSetResourceString(&_sdxSuppressBackgroundBitmaps ,"");  //&Suppress Background Textures';
  cxSetResourceString(&_sdxConsumeSelectionStyle ,"");  //Consume Selection Style';
  
  { Grid4 new }

  cxSetResourceString(&_sdxSize ,"");  //Size';
  cxSetResourceString(&_sdxLevels ,"");  //Levels';
  cxSetResourceString(&_sdxUnwrap ,"");  //&Unwrap';
  cxSetResourceString(&_sdxUnwrapTopLevel ,"");  //Un&wrap Top Level';
  cxSetResourceString(&_sdxRiseActiveToTop ,"");  //Rise Active Level onto Top';
  cxSetResourceString(&_sdxCannotUseOnEveryPageModeInAggregatedState = 
    'Cannot Use OnEveryPage Mode'+ #13#10 + 
    'While Performing in Aggregated State';

  cxSetResourceString(&_sdxPagination ,"");  //Pagination';
  cxSetResourceString(&_sdxByBands ,"");  //By Bands';
  cxSetResourceString(&_sdxByColumns ,"");  //By Columns';
  cxSetResourceString(&_sdxByRows ,"");  //By &Rows';
  cxSetResourceString(&_sdxByTopLevelGroups ,"");  //By TopLevel Groups'; 
  cxSetResourceString(&_sdxOneGroupPerPage ,"");  //One Group Per Page'; 

  cxSetResourceString(&_sdxSkipEmptyViews ,"");  //Skip Empty Views';

  {* For those who will translate *}
  {* You should also check "sdxCannotUseOnEveryPageMode" resource string - see above *}
  {* It was changed to "- Toggle "Unwrap" Option off on "Behaviors" Tab"*}
   
  { TL 4 }
  cxSetResourceString(&_sdxBorders ,"");  //Borders';
  cxSetResourceString(&_sdxExplicitlyExpandNodes ,"");  //Explicitly Expand Nodes';
  cxSetResourceString(&_sdxNodes ,"");  //&Nodes';
  cxSetResourceString(&_sdxSeparators ,"");  //Separators';
  cxSetResourceString(&_sdxThickness ,"");  //Thickness';
  cxSetResourceString(&_sdxTLIncorrectHeadersState = 
    'Cannot Use Headers OnEveryPage Mode' + #13#10 + 
    #13#10 +
    'You should either:' + #13#10 +  
    '  - Set Band OnEveryPage Option On' + #13#10 +
    '  - Set Band Visible Option Off';

  { cxVerticalGrid }

  cxSetResourceString(&_sdxRows ,"");  //&Rows'; 

  cxSetResourceString(&_sdxMultipleRecords ,"");  //&Multiple Records';
  cxSetResourceString(&_sdxBestFit ,"");  //&Best Fit';
  cxSetResourceString(&_sdxKeepSameRecordWidths ,"");  //&Keep Same Record Widths';
  cxSetResourceString(&_sdxWrapRecords ,"");  //&Wrap Records';

  cxSetResourceString(&_sdxByWrapping ,"");  //By &Wrapping';
  cxSetResourceString(&_sdxOneWrappingPerPage ,"");  //&One Wrapping Per Page';

  {new in 3.01}
  cxSetResourceString(&_sdxCurrentRecord ,"");  //Current Record';
  cxSetResourceString(&_sdxLoadedRecords ,"");  //Loaded Records';
  cxSetResourceString(&_sdxAllRecords ,"");  //All Records';
  
  { Container Designer }
  
  cxSetResourceString(&_sdxPaginateByControlDetails ,"");  //Control Details';
  cxSetResourceString(&_sdxPaginateByControls ,"");  //Controls';
  cxSetResourceString(&_sdxPaginateByGroups ,"");  //Groups';
  cxSetResourceString(&_sdxPaginateByItems ,"");  //Items';
  
  cxSetResourceString(&_sdxControlsPlace ,"");  //Controls Place';
  cxSetResourceString(&_sdxExpandHeight ,"");  //Expand Height';
  cxSetResourceString(&_sdxExpandWidth ,"");  //Expand Width';
  cxSetResourceString(&_sdxShrinkHeight ,"");  //Shrink Height';
  cxSetResourceString(&_sdxShrinkWidth ,"");  //Shrink Width';
  
  cxSetResourceString(&_sdxCheckAll ,"");  //Check &All';
  cxSetResourceString(&_sdxCheckAllChildren ,"");  //Check All &Children';
  cxSetResourceString(&_sdxControlsTab ,"");  //Controls';
  cxSetResourceString(&_sdxExpandAll ,"");  //E&xpand All';
  cxSetResourceString(&_sdxHiddenControlsTab ,"");  //Hidden Controls';
  cxSetResourceString(&_sdxReportLinksTab ,"");  //Aggregated Designers';
  cxSetResourceString(&_sdxAvailableLinks ,"");  //&Available Links:';
  cxSetResourceString(&_sdxAggregatedLinks ,"");  //A&ggregated Links:';
  cxSetResourceString(&_sdxTransparents ,"");  //Transparents';
  cxSetResourceString(&_sdxUncheckAllChildren ,"");  //Uncheck &All Children';
  
  cxSetResourceString(&_sdxRoot ,"");  //&Root';
  cxSetResourceString(&_sdxRootBorders ,"");  //Root &Borders';
  cxSetResourceString(&_sdxControls ,"");  //&Controls';
  cxSetResourceString(&_sdxContainers ,"");  //C&ontainers';

  cxSetResourceString(&_sdxHideCustomContainers ,"");  //&Hide Custom Containers';

  { General }

  // FileSize abbreviation

  cxSetResourceString(&_sdxBytes ,"");  //Bytes';
  cxSetResourceString(&_sdxKiloBytes ,"");  //KB';
  cxSetResourceString(&_sdxMegaBytes ,"");  //MB';
  cxSetResourceString(&_sdxGigaBytes ,"");  //GB';

  // Misc.

  cxSetResourceString(&_sdxThereIsNoPictureToDisplay ,"");  //There is no picture to display';
  cxSetResourceString(&_sdxInvalidRootDirectory ,"");  //Directory "%s" does not exists. Continue selection ?';
  cxSetResourceString(&_sdxPressEscToCancel ,"");  //Press Esc to cancel';
  cxSetResourceString(&_sdxMenuFileRebuild ,"");  //&Rebuild';
  cxSetResourceString(&_sdxBuildingReportStatusText ,"");  //Building report - Press Esc to cancel';
  cxSetResourceString(&_sdxPrintingReportStatusText ,"");  //Printing report - Press Esc to cancel';
  
  cxSetResourceString(&_sdxBuiltIn ,"");  //[BuiltIn]';
  cxSetResourceString(&_sdxUserDefined ,"");  //[User Defined]';
  cxSetResourceString(&_sdxNewStyleRepositoryWasCreated ,"");  //New StyleRepository "%s" was created and assigned';

  { new in PS 3.1}
  cxSetResourceString(&_sdxLineSpacing ,"");  //&Line spacing:';
  cxSetResourceString(&_sdxTextAlignJustified ,"");  //Justified';
  cxSetResourceString(&_sdxSampleText ,"");  //Sample Text Sample Text';
  
  cxSetResourceString(&_sdxCardsRows ,"");  //&Cards';
  cxSetResourceString(&_sdxTransparentRichEdits ,"");  //Transparent &RichEdit Content';

  cxSetResourceString(&_sdxIncorrectFilterBarState =
    'Cannot Use FilterBar OnEveryPage Mode' + #13#10 +
    #13#10 +
    'You should either:' + #13#10 +  
    '  - Set Caption OnEveryPage Option On' + #13#10 +
    '  - Set Caption Visible Option Off';
  cxSetResourceString(&_sdxIncorrectBandHeadersState2 = 
    'Cannot Use BandHeaders OnEveryPage Mode' + #13#10 +
    #13#10 +
    'You should either:' + #13#10 +  
    '  - Set Caption and FilterBar OnEveryPage Option On' + #13#10 + 
    '  - Set Caption and FilterBar Visible Option Off';
  cxSetResourceString(&_sdxIncorrectHeadersState2 =
    'Cannot Use Headers OnEveryPage Mode' + #13#10 +
    #13#10 +
    'You should either:' + #13#10 +  
    '  - Set Caption, FilterBar and Band OnEveryPage Option On' + #13#10 +
    '  - Set Caption, FilterBar and Band Visible Option Off';

 { new in PS 3.2}   
  cxSetResourceString(&_sdxAvailableReportLinks ,"");  //Available ReportLinks';
  cxSetResourceString(&_sdxBtnRemoveInconsistents ,"");  //Remove Unneeded';
  cxSetResourceString(&_sdxColumnHeadersOnEveryPage ,"");  //Column &Headers';
  
 { Scheduler }   

  cxSetResourceString(&_sdxNotes ,"");  //Notes';
  cxSetResourceString(&_sdxTaskPad ,"");  //TaskPad';
  cxSetResourceString(&_sdxPrimaryTimeZone ,"");  //Primary';
  cxSetResourceString(&_sdxSecondaryTimeZone ,"");  //Secondary';

  cxSetResourceString(&_sdxDay ,"");  //Day';
  cxSetResourceString(&_sdxWeek ,"");  //Week';
  cxSetResourceString(&_sdxMonth ,"");  //Month';

  cxSetResourceString(&_sdxSchedulerSchedulerHeader ,"");  //Scheduler Header';
  cxSetResourceString(&_sdxSchedulerContent ,"");  //Content';
  cxSetResourceString(&_sdxSchedulerDateNavigatorContent ,"");  //DateNavigator Content';
  cxSetResourceString(&_sdxSchedulerDateNavigatorHeader ,"");  //DateNavigator Header';
  cxSetResourceString(&_sdxSchedulerDayHeader ,"");  //Day Header';
  cxSetResourceString(&_sdxSchedulerEvent ,"");  //Event';
  cxSetResourceString(&_sdxSchedulerResourceHeader ,"");  //Resource Header';
  cxSetResourceString(&_sdxSchedulerNotesAreaBlank ,"");  //Notes Area (Blank)';
  cxSetResourceString(&_sdxSchedulerNotesAreaLined ,"");  //Notes Area (Lined)';
  cxSetResourceString(&_sdxSchedulerTaskPad ,"");  //TaskPad';
  cxSetResourceString(&_sdxSchedulerTimeRuler ,"");  //Time Ruler';
  
  cxSetResourceString(&_sdxPrintStyleNameDaily ,"");  //Daily';
  cxSetResourceString(&_sdxPrintStyleNameWeekly ,"");  //Weekly';
  cxSetResourceString(&_sdxPrintStyleNameMonthly ,"");  //Monthly';
  cxSetResourceString(&_sdxPrintStyleNameDetails ,"");  //Details';
  cxSetResourceString(&_sdxPrintStyleNameMemo ,"");  //Memo';
  cxSetResourceString(&_sdxPrintStyleNameTrifold ,"");  //Trifold';

  cxSetResourceString(&_sdxPrintStyleCaptionDaily ,"");  //Daily Style';
  cxSetResourceString(&_sdxPrintStyleCaptionWeekly ,"");  //Weekly Style';
  cxSetResourceString(&_sdxPrintStyleCaptionMonthly ,"");  //Monthly Style';
  cxSetResourceString(&_sdxPrintStyleCaptionDetails ,"");  //Calendar Details Style';
  cxSetResourceString(&_sdxPrintStyleCaptionMemo ,"");  //Memo Style';
  cxSetResourceString(&_sdxPrintStyleCaptionTimeLine ,"");  //TimeLine Style';
  cxSetResourceString(&_sdxPrintStyleCaptionTrifold ,"");  //Tri-fold Style';
  cxSetResourceString(&_sdxPrintStyleCaptionYearly ,"");  //Yearly Style';
  cxSetResourceString(&_sdxPrintStyleShowEventImages ,"");  //Show Event Images';
  cxSetResourceString(&_sdxPrintStyleShowResourceImages ,"");  //Show Resource Images';

  cxSetResourceString(&_sdxTabPrintStyles ,"");  //Print Styles';

  cxSetResourceString(&_sdxPrintStyleDontPrintWeekEnds ,"");  //&Don''t Print Weekends';
  cxSetResourceString(&_sdxPrintStyleWorkTimeOnly ,"");  //&Work Time Only';

  cxSetResourceString(&_sdxPrintStyleInclude ,"");  //Include:';
  cxSetResourceString(&_sdxPrintStyleIncludeTaskPad ,"");  //Task&Pad';
  cxSetResourceString(&_sdxPrintStyleIncludeNotesAreaBlank ,"");  //Notes Area (&Blank)';
  cxSetResourceString(&_sdxPrintStyleIncludeNotesAreaLined ,"");  //Notes Area (&Lined)';
  cxSetResourceString(&_sdxPrintStyleLayout ,"");  //&Layout:';
  cxSetResourceString(&_sdxPrintStylePrintFrom ,"");  //Print &From:';
  cxSetResourceString(&_sdxPrintStylePrintTo ,"");  //Print &To:';

  cxSetResourceString(&_sdxPrintStyleDailyLayout1PPD ,"");  //1 page/day';
  cxSetResourceString(&_sdxPrintStyleDailyLayout2PPD ,"");  //2 pages/day';

  cxSetResourceString(&_sdxPrintStyleWeeklyArrange ,"");  //&Arrange:';
  cxSetResourceString(&_sdxPrintStyleWeeklyArrangeT2B ,"");  //Top to bottom';
  cxSetResourceString(&_sdxPrintStyleWeeklyArrangeL2R ,"");  //Left to right';
  cxSetResourceString(&_sdxPrintStyleWeeklyLayout1PPW ,"");  //1 page/week';
  cxSetResourceString(&_sdxPrintStyleWeeklyLayout2PPW ,"");  //2 pages/week';
  cxSetResourceString(&_sdxPrintStyleWeeklyDaysLayout ,"");  //&Days layout:';
  cxSetResourceString(&_sdxPrintStyleWeeklyDaysLayoutTC ,"");  //Two columns';
  cxSetResourceString(&_sdxPrintStyleWeeklyDaysLayoutOC ,"");  //One column';

  cxSetResourceString(&_sdxPrintStyleMemoStartEachItemOnNewPage ,"");  //Start Each Item On New Page';
  cxSetResourceString(&_sdxPrintStyleMemoPrintOnlySelectedEvents ,"");  //Print Only Selected Events';

  cxSetResourceString(&_sdxPrintStyleMonthlyLayout1PPM ,"");  //1 page/month';
  cxSetResourceString(&_sdxPrintStyleMonthlyLayout2PPM ,"");  //2 pages/month';
  cxSetResourceString(&_sdxPrintStyleMonthlyPrintExactly1MPP ,"");  //Print &Exactly One Month Per Page';

  cxSetResourceString(&_sdxPrintStyleTrifoldSectionModeDailyCalendar ,"");  //Daily Calendar';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionModeWeeklyCalendar ,"");  //Weekly Calendar';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionModeMonthlyCalendar ,"");  //Monthly Calendar';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionModeTaskPad ,"");  //TaskPad';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionModeNotesBlank ,"");  //Notes (Blank)';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionModeNotesLined ,"");  //Notes (Lined)';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionLeft ,"");  //&Left Section:';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionMiddle ,"");  //&Middle Section:';
  cxSetResourceString(&_sdxPrintStyleTrifoldSectionRight ,"");  //&Right Section:';

  cxSetResourceString(&_sdxPrintStyleMonthPerPage ,"");  //&Months/Page:';
  cxSetResourceString(&_sdxPrintStyleYearly1MPP ,"");  //1 month/page';
  cxSetResourceString(&_sdxPrintStyleYearly2MPP ,"");  //2 months/page';
  cxSetResourceString(&_sdxPrintStyleYearly3MPP ,"");  //3 months/page';
  cxSetResourceString(&_sdxPrintStyleYearly4MPP ,"");  //4 months/page';
  cxSetResourceString(&_sdxPrintStyleYearly6MPP ,"");  //6 months/page';
  cxSetResourceString(&_sdxPrintStyleYearly12MPP ,"");  //12 months/page';

  cxSetResourceString(&_sdxPrintStylePrimaryPageScalesOnly ,"");  //Primary Page Scales Only';
  cxSetResourceString(&_sdxPrintStylePrimaryPageHeadersOnly ,"");  //Primary Page Headers Only';

  cxSetResourceString(&_sdxPrintStyleDetailsStartNewPageEach ,"");  //Start a New Page Each:';

  cxSetResourceString(&_sdxSuppressContentColoration ,"");  //Suppress &Content Coloration';
  cxSetResourceString(&_sdxOneResourcePerPage ,"");  //One &Resource Per Page';

  cxSetResourceString(&_sdxPrintRanges ,"");  //Print Ranges';
  cxSetResourceString(&_sdxPrintRangeStart ,"");  //&Start:';
  cxSetResourceString(&_sdxPrintRangeEnd ,"");  //&End:';
  cxSetResourceString(&_sdxHideDetailsOfPrivateAppointments ,"");  //&Hide Details of Private Appointments';
  cxSetResourceString(&_sdxResourceCountPerPage ,"");  //&Resources/Page:';

  cxSetResourceString(&_sdxSubjectLabelCaption ,"");  //Subject:';
  cxSetResourceString(&_sdxLocationLabelCaption ,"");  //Location:';
  cxSetResourceString(&_sdxStartLabelCaption ,"");  //Start:';
  cxSetResourceString(&_sdxFinishLabelCaption ,"");  //End:';
  cxSetResourceString(&_sdxShowTimeAsLabelCaption ,"");  //Show Time As:';
  cxSetResourceString(&_sdxRecurrenceLabelCaption ,"");  //Recurrence:';
  cxSetResourceString(&_sdxRecurrencePatternLabelCaption ,"");  //Recurrence Pattern:';

  //messages
  cxSetResourceString(&_sdxSeeAboveMessage ,"");  //Please See Above';
  cxSetResourceString(&_sdxAllDayMessage ,"");  //All Day';
  cxSetResourceString(&_sdxContinuedMessage ,"");  //Continued';
  cxSetResourceString(&_sdxShowTimeAsFreeMessage ,"");  //Free';
  cxSetResourceString(&_sdxShowTimeAsTentativeMessage ,"");  //Tentative';
  cxSetResourceString(&_sdxShowTimeAsOutOfOfficeMessage ,"");  //Out of Office';

  cxSetResourceString(&_sdxRecurrenceNoneMessage ,"");  //(none)';
  scxRecurrenceDailyMessage ,"");  //Daily';
  scxRecurrenceWeeklyMessage ,"");  //Weekly';
  scxRecurrenceMonthlyMessage ,"");  //Monthly';
  scxRecurrenceYearlyMessage ,"");  //Yearly';

  //error messages
  cxSetResourceString(&_sdxInconsistentTrifoldStyle ,"");  //The Tri-fold style requires at least one calendar section. ' + 'Select Daily, Weekly, or Monthly Calendar for one of section under Options.';
  cxSetResourceString(&_sdxBadTimePrintRange ,"");  //The hours to print are not valid. The start time must precede the end time.';
  cxSetResourceString(&_sdxBadDatePrintRange ,"");  //The date in the End box cannot be prior to the date in the Start box.';
  cxSetResourceString(&_sdxCannotPrintNoSelectedItems ,"");  //Cannot print unless an item is selected. Select an item, and then try to print again.';
  cxSetResourceString(&_sdxCannotPrintNoItemsAvailable ,"");  //No items available within the specified print range.';

  { PivotGrid }

  cxSetResourceString(&_sdxColumnFields ,"");  //&Column Fields';
  cxSetResourceString(&_sdxDataFields ,"");  //&Data Fields';
  cxSetResourceString(&_sdxFiterFields ,"");  //&Filter Fields';
  cxSetResourceString(&_sdxPrefilter ,"");  //&Prefilter';
  cxSetResourceString(&_sdxRowFields ,"");  //&Row Fields';

  cxSetResourceString(&_sdxAutoColumnsExpand ,"");  //A&uto Columns Expand';
  cxSetResourceString(&_sdxAutoRowsExpand ,"");  //Auto &Rows Expand';

  // styles
  cxSetResourceString(&_sdxPivotGridColumnHeader ,"");  //Column Header';
  cxSetResourceString(&_sdxPivotGridContent ,"");  //Content';
  cxSetResourceString(&_sdxPivotGridFieldHeader ,"");  //Field Header';
  cxSetResourceString(&_sdxPivotGridHeaderBackground ,"");  //Header Background';
  cxSetResourceString(&_sdxPivotGridRowHeader ,"");  //Row Header';
  cxSetResourceString(&_sdxPivotGridPrefilter ,"");  //Prefilter';

  // PivotPreview fields
  cxSetResourceString(&_sdxUnitPrice ,"");  //Unit Price';
  cxSetResourceString(&_sdxCarName ,"");  //Car Name';
  cxSetResourceString(&_sdxQuantity ,"");  //Quantity';
  cxSetResourceString(&_sdxPaymentAmount ,"");  //Payment Amount';
  cxSetResourceString(&_sdxPurchaseQuarter ,"");  //Purchase Quarter';
  cxSetResourceString(&_sdxPurchaseMonth ,"");  //Purchase Month';
  cxSetResourceString(&_sdxPaymentType ,"");  //Payment Type';
  cxSetResourceString(&_sdxCompanyName ,"");  //Company Name';


*/

}
//---------------------------------------------------------------------------
