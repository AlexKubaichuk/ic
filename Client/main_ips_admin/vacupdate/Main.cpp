#include <vcl.h>
#pragma hdrstop


#include "Main.h"
//#include "DocListEx.h"
#include "icDxLocale.h"
//#include "About.h"
//#include "Setting.h"
#include "Connect.h"
//#include "DocOrgList.h"
#include "ExtUtils.h"
#include "math.h"
                                                  
//#include "ProgressF.h"

#include "dirscan.h"

//#define itTag ((TTagNode*)uiTag)
//#define MenuTB MainTBM->Bars->Items[0]
//#define RegTB  MainTBM->Bars->Items[1]
//---------------------------------------------------------------------
#pragma link "ICSAboutDialog"
#pragma link "dxBar"
#pragma link "cxGraphics"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxTextEdit"
#pragma link "cxCheckListBox"
#pragma link "cxButtons"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent *Owner)
	: TForm(Owner)
{
  PC->ActivePage = AddTS;
  icDxLocale();
  AddTypeCBPropertiesChange(NULL);
  BinaryReakChBPropertiesChange(NULL);
}
//---------------------------------------------------------------------
bool __fastcall TMainForm::CheckConnect()
{
  TConnectForm *ConnectDlg = new TConnectForm(this);
  if (ConnectDlg->Connect())
   {
     FillChLB();
     ClearFields();
   }
  else
   {
     delete ConnectDlg;
     MessageBox(Handle,"���������� � �������������� ��������� \n��� ���������� ���������","������ �������� ���������",MB_ICONSTOP);
     Application->Terminate();
     return false;
   }
  if (ConnectDlg)
   delete ConnectDlg;
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FillChLB()
{
  InfChLB->Items->BeginUpdate();
  MIBPChLB->Items->BeginUpdate();
  ReakTypeChLB->Items->BeginUpdate();
  VacSchList->Items->BeginUpdate();
  try
   {
     TcxCheckListBoxItem *tmpItem;
     // ��������
     InfChLB->Items->Clear();
     DM->qtExec("Select * From class_003a order by R003c", false);
     while (!DM->quFree->Eof)
      {
        tmpItem = InfChLB->Items->Add();
        tmpItem->Checked = false;
        tmpItem->Text = DM->quFree->FN("R003c")->AsString;
        tmpItem->Tag = DM->quFree->FN("CODE")->AsInteger;
        DM->quFree->Next();
      }
     // ���������������
     MIBPChLB->Items->Clear();
     VacCB->Properties->Items->Clear();
     CheckVacCB->Properties->Items->Clear();
     DM->qtExec("Select * From class_0035 order by R0040", false);
     while (!DM->quFree->Eof)
      {
        tmpItem = MIBPChLB->Items->Add();
        tmpItem->Checked = false;
        tmpItem->Text = DM->quFree->FN("R0040")->AsString;
        tmpItem->Tag = DM->quFree->FN("CODE")->AsInteger;
        VacCB->Properties->Items->AddObject(DM->quFree->FN("R0040")->AsString,(TObject*)DM->quFree->FN("CODE")->AsInteger);
        CheckVacCB->Properties->Items->AddObject(DM->quFree->FN("R0040")->AsString,(TObject*)DM->quFree->FN("CODE")->AsInteger);
        DM->quFree->Next();
      }
     // �������
     ReakTypeChLB->Items->Clear();
     DM->qtExec("Select * From class_0038 order by R008e", false);
     while (!DM->quFree->Eof)
      {
        tmpItem = ReakTypeChLB->Items->Add();
        tmpItem->Checked = false;
        tmpItem->Text = DM->quFree->FN("R008e")->AsString;
        tmpItem->Tag = DM->quFree->FN("CODE")->AsInteger;
        DM->quFree->Next();
      }
     // �����
     CheckTestCB->Properties->Items->Clear();
     DM->qtExec("Select * From class_002A order by R002C", false);
     while (!DM->quFree->Eof)
      {
        CheckTestCB->Properties->Items->AddObject(DM->quFree->FN("R002C")->AsString,(TObject*)DM->quFree->FN("CODE")->AsInteger);
        DM->quFree->Next();
      }


     VacSchList->Items->Clear();
     TTagNode *FVacSchDef  = DM->XMLList->GetXML("12063611-00008cd7-cd89")->GetChildByName("schemalist", true)->GetFirstChild();
     while (FVacSchDef)
      {
        DM->qtExec("Select R0040 From class_0035 where code="+FVacSchDef->AV["ref"]+" order by R0040", false);

        tmpItem = VacSchList->Items->Add();
        tmpItem->Checked = FVacSchDef->AV["def"].ToIntDef(0);
        tmpItem->Text = DM->quFree->FN("R0040")->AsString+"->"+FVacSchDef->AV["name"];
        tmpItem->Tag = UIDInt(FVacSchDef->AV["uid"]);

        FVacSchDef = FVacSchDef->GetNext();
      }

     AnsiString FCount;
     DM->qtExec("Select Count(*) as RCOUNT From class_0035", false);
     FCount = "���������� ������/����. ������: "+DM->quFree->FN("RCOUNT")->AsString+", ����: ";
     DM->qtExec("Select Count(*) as RCOUNT From class_002A", false);
     FCount += DM->quFree->FN("RCOUNT")->AsString;
     Caption = FCount;
   }
  __finally
   {
     InfChLB->Items->EndUpdate();
     MIBPChLB->Items->EndUpdate();
     ReakTypeChLB->Items->EndUpdate();
     VacSchList->Items->EndUpdate();
   }

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject *Sender)
{
  CheckConnect();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    DM->RegComp->Active = false;
    Action = caFree;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::actGetInfClassExecute(TObject *Sender)
{
   DM->RegComp->ClassShowExpanded = "0029,0044,0059,00A6";
   DM->RegComp->ShowClasses(ctList,"0,1,3,2",-1);
   DM->RegComp->ClassShowExpanded = "";
   DM->FClassData->ClearHash("");
   FillChLB();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::actAboutExecute(TObject *Sender)
{
  if (DM->VersionData)
   AboutDlg->VIXML->Text = DM->VersionData->AsXML;
  AboutDlg->Execute();
}
//---------------------------------------------------------------------------
#include "SetDBPath.h"
void __fastcall TMainForm::actSetDBPathExecute(TObject *Sender)
{
  TSetDBPathForm* Dlg = NULL;
  try
   {
     Dlg = new TSetDBPathForm(this, "IMM", DM->RegKey, DM->_DB_NAME , DM->_DB_Alias , DM->Test_Conn_DB, false);
     Dlg->ShowModal();
   }
  __finally
   {
     if (Dlg)
      {
        delete Dlg;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Action1Execute(TObject *Sender)
{
  TTagNode *itClassForm = DM->xmlRegDef->GetChildByName("classes")->GetFirstChild();
  TTagNode *itClass;
  AnsiString FTabName,FATabName, FGUID;
  int FCount, FACount;
  while (itClassForm)
   {
     itClass = itClassForm->GetFirstChild();
     while (itClass)
      {
        FTabName = "CLASS_"+itClass->AV["uid"].UpperCase();
        FATabName= "CLASS_A"+itClass->AV["uid"].UpperCase();

        DM->qtExec("Select Count(CODE) as RCOUNT From "+FTabName,false);
        FCount = DM->quFree->FN("RCOUNT")->AsInteger;
        DM->qtExec("Select Count(CODE) as RCOUNT From "+FATabName,false);
        FACount = DM->quFree->FN("RCOUNT")->AsInteger;
        DM->qtExec("Select CODE From "+FTabName,false);
        while (!DM->quFree->Eof)
         {
           FGUID = DM->NewGUID().UpperCase();
           DM->qtExec1("Update "+FTabName+" Set CGUID='"+FGUID+"' Where CODE="+DM->quFree->FN("CODE")->AsString, true);
           if (FACount == FCount)
            DM->qtExec1("Update "+FATabName+" Set CGUID='"+FGUID+"' Where CODE="+DM->quFree->FN("CODE")->AsString, true);
           DM->quFree->Next();
         }
        itClass = itClass->GetNext();
      }
     itClassForm = itClassForm->GetNext();
   }
  ShowMessage("Ok");
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::AddTypeCBPropertiesChange(TObject *Sender)
{
  FullNameED->Enabled      = !AddTypeCB->ItemIndex;
  MIBPChLB->Enabled        = !AddTypeCB->ItemIndex;
  VacNameED->Enabled       = AddTypeCB->ItemIndex;
  CanApplyVacChB->Enabled  = AddTypeCB->ItemIndex;
  VacCB->Enabled           = AddTypeCB->ItemIndex;
  ResistReakValED->Enabled = AddTypeCB->ItemIndex;
  Pause1ED->Enabled        = AddTypeCB->ItemIndex;
  Pause2ED->Enabled        = AddTypeCB->ItemIndex;
  Pause3ED->Enabled        = AddTypeCB->ItemIndex;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BinaryReakChBPropertiesChange(TObject *Sender)
{
  ReakTypeChLB->Enabled = !BinaryReakChB->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ClearFields()
{
  NameED->Text = "";
  FullNameED->Text = "";
  VacNameED->Text = "";
  DoseED->Text = "0,5";
  BinaryReakChB->Checked = true;
  CanApplyVacChB->Checked = false;
  VacCB->ItemIndex = -1;
  ResistReakValED->Text = "";
  Pause1ED->Text = "30";
  Pause2ED->Text = "365";
  Pause3ED->Text = "3";

  for (int i = 0; i < MIBPChLB->Items->Count; i++)
   {
     if ((MIBPChLB->Items->Items[i]->Tag == 1) || (MIBPChLB->Items->Items[i]->Tag == 2))
      MIBPChLB->Items->Items[i]->Checked = true;
     else
      MIBPChLB->Items->Items[i]->Checked = false;
   }
  for (int i = 0; i < InfChLB->Items->Count; i++)
   InfChLB->Items->Items[i]->Checked = false;
  for (int i = 0; i < ReakTypeChLB->Items->Count; i++)
   ReakTypeChLB->Items->Items[i]->Checked = false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FUpdateGen(const AnsiString &AUID)
{
  DM->qtExec("Select Max(code) as MCODE from class_"+AUID, false);
  if (DM->quFree->FN("MCODE")->IsNull)
   DM->qtExec("SET GENERATOR GEN_"+AUID+"_ID TO 1", false);
  else
   DM->qtExec("SET GENERATOR GEN_"+AUID+"_ID TO "+DM->quFree->FN("MCODE")->AsString, false);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::actAddExecute(TObject *Sender)
{
  if (CheckInput())
   {
     try
      {
        FUpdateGen("0035");
        FUpdateGen("0042");
        FUpdateGen("002D");
        FUpdateGen("0039");
        FUpdateGen("002A");
        FUpdateGen("0079");
        FUpdateGen("002B");
        FUpdateGen("0046");
        FUpdateGen("0066");
        if (AddTypeCB->ItemIndex)
         AddTest();
        else
         AddVac();
      }
     __finally
      {
        FillChLB();
        ClearFields();
        AddTypeCB->SetFocus();
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::CheckInput()
{
  bool RC = false;
  try
   {
     int infCount, reakCount;
     infCount = CheckedItems(InfChLB);
     if (ReakTypeChLB->Enabled)
      {
        reakCount = CheckedItems(ReakTypeChLB);
      }
     else
      reakCount = -1;
     if (!NameED->Text.Trim().Length())
      {
        NameED->SetFocus();
        _MSG_ERR("���������� ������� ������������.","������");
      }
     else if (!DoseED->Text.Trim().Length())
      {
        DoseED->SetFocus();
        _MSG_ERR("���������� ������� ����.","������");
      }
     else if (infCount == 0)
      {
        InfChLB->SetFocus();
        _MSG_ERR("���������� ������� ��������.","������");
      }
     else if ((infCount > 1) && AddTypeCB->ItemIndex)
      {
        InfChLB->SetFocus();
        _MSG_ERR("��� ����� ����� ���� ������� ������ ���� ��������.","������");
      }
     else if (reakCount == 0)
      { 
        ReakTypeChLB->SetFocus();
        _MSG_ERR("���������� ������� ������� �������.","������");
      }
     else
      RC = true;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TMainForm::CheckedItems(TcxCheckListBox *ASrc)
{
  int RC = 0;
  try
   {
     for (int i = 0; i < ASrc->Items->Count; i++)
      if (ASrc->Items->Items[i]->Checked)
       RC++;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FSetQuery(const AnsiString &AVal)
{
  DM->quFree->Close();
  DM->quFree->SQL->Text = AVal;
  DM->quFree->Prepare();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::AddVac()
{
  if (DM->quFree->Transaction->Active) DM->quFree->Transaction->Commit();
  DM->quFree->Transaction->StartTransaction();
  // ����
  FSetQuery("Insert into class_0035 (CODE, R0040, R008F, R006A, R0064) values (?pCODE, ?pR0040, ?pR008F, ?pR006A, ?pR0064)");
  int FVacCode = DM->GetMCode("MCODE_0035");
  DM->quFree->ParamByName("pCODE")->AsInteger  = FVacCode;
  DM->quFree->ParamByName("pR0040")->AsString  = NameED->Text.Trim();
  DM->quFree->ParamByName("pR008F")->AsString  = FullNameED->Text.Trim();
  DM->quFree->ParamByName("pR006A")->AsFloat   = StrToFloat(DoseED->Text);
  DM->quFree->ParamByName("pR0064")->AsInteger = BinaryReakChB->Checked;
  DM->quFree->ExecQuery();
  // ������������� ����
  FSetQuery("Insert into class_0042 (CODE, R0065, R0043) values (?pCODE, ?pR0065, ?pR0043)");
  for (int i = 0; i < MIBPChLB->Items->Count; i++)
   {
     if (!MIBPChLB->Items->Items[i]->Checked)
      {
        DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_0042");
        DM->quFree->ParamByName("pR0065")->AsInteger = FVacCode;
        DM->quFree->ParamByName("pR0043")->AsInteger = MIBPChLB->Items->Items[i]->Tag;
        DM->quFree->ExecQuery();
        DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_0042");
        DM->quFree->ParamByName("pR0065")->AsInteger = MIBPChLB->Items->Items[i]->Tag;
        DM->quFree->ParamByName("pR0043")->AsInteger = FVacCode;
        DM->quFree->ExecQuery();
      }
   }
  // ���������� -> ����
  FSetQuery("Insert into class_002D (CODE, R003E, R003B) values (?pCODE, ?pR003E, ?pR003B)");
  for (int i = 0; i < InfChLB->Items->Count; i++)
   {
     if (InfChLB->Items->Items[i]->Checked)
      {
        DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_002D");
        DM->quFree->ParamByName("pR003E")->AsInteger = InfChLB->Items->Items[i]->Tag;
        DM->quFree->ParamByName("pR003B")->AsInteger = FVacCode;
        DM->quFree->ExecQuery();
      }
   }
  // ������� ������� �� ����
  if (ReakTypeChLB->Enabled)
   {
     FSetQuery("Insert into class_0039 (CODE, R0090, R0091) values (?pCODE, ?pR0090, ?pR0091)");
     for (int i = 0; i < ReakTypeChLB->Items->Count; i++)
      {
        if (ReakTypeChLB->Items->Items[i]->Checked)
         {
           DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_0039");
           DM->quFree->ParamByName("pR0090")->AsInteger = FVacCode;
           DM->quFree->ParamByName("pR0091")->AsInteger = ReakTypeChLB->Items->Items[i]->Tag;
           DM->quFree->ExecQuery();
         }
      }
   }
  if (DM->quFree->Transaction->Active) DM->quFree->Transaction->Commit();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::AddTest()
{
  if (DM->quFree->Transaction->Active) DM->quFree->Transaction->Commit();
  DM->quFree->Transaction->StartTransaction();
  // �����/��������
  FSetQuery("Insert into class_002A (CODE, R003D, R002C, R0062, R0063, R0069, R0067, R0068, R006B) values (?pCODE, ?pR003D, ?pR002C, ?pR0062, ?pR0063, ?pR0069, ?pR0067, ?pR0068, ?pR006B)");
  int FTestCode = DM->GetMCode("MCODE_002A");
  int infCode = 0;
  for (int i = 0; i < InfChLB->Items->Count; i++)
   {
     if (InfChLB->Items->Items[i]->Checked)
      {
        infCode = InfChLB->Items->Items[i]->Tag;
        break;
      }
   }

  DM->quFree->ParamByName("pCODE")->AsInteger = FTestCode;
  DM->quFree->ParamByName("pR003D")->AsInteger  = infCode;
  DM->quFree->ParamByName("pR002C")->AsString  = NameED->Text.Trim();
  DM->quFree->ParamByName("pR0062")->AsString  = VacNameED->Text.Trim();
  DM->quFree->ParamByName("pR0063")->AsFloat   = StrToFloat(DoseED->Text);
  DM->quFree->ParamByName("pR0069")->AsInteger  = CanApplyVacChB->Checked;
  if (CanApplyVacChB->Checked && (VacCB->ItemIndex != -1))
   {
     DM->quFree->ParamByName("pR0067")->AsInteger = (int)VacCB->Properties->Items->Objects[VacCB->ItemIndex];
   }
  if (ResistReakValED->Text.Trim().Length())
   DM->quFree->ParamByName("pR0068")->AsInteger   = ResistReakValED->Text.Trim().ToIntDef(0);
  else
   DM->quFree->ParamByName("pR0068")->Clear();
  DM->quFree->ParamByName("pR006B")->AsInteger    = BinaryReakChB->Checked;
  DM->quFree->ExecQuery();

  // ���������� -> �����/��������
  FSetQuery("Insert into class_0079 (CODE, R007B, R007A) values (?pCODE, ?pR007B, ?pR007A)");
        DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_0079");
        DM->quFree->ParamByName("pR007B")->AsInteger = infCode;
        DM->quFree->ParamByName("pR007A")->AsInteger = FTestCode;
        DM->quFree->ExecQuery();
  // ������� ������� �� �����/��������
  if (ReakTypeChLB->Enabled)
   {
     FSetQuery("Insert into class_002B (CODE, R002E, R0037) values (?pCODE, ?pR002E, ?pR0037)");
     for (int i = 0; i < ReakTypeChLB->Items->Count; i++)
      {
        if (ReakTypeChLB->Items->Items[i]->Checked)
         {
           DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_002B");
           DM->quFree->ParamByName("pR002E")->AsInteger = FTestCode;
           DM->quFree->ParamByName("pR0037")->AsInteger = ReakTypeChLB->Items->Items[i]->Tag;
           DM->quFree->ExecQuery();
         }
      }
   }
  // ����� "���������� - �����/��������"
  FSetQuery("Insert into class_0046 (CODE, R004C, R004B, R004D) values (?pCODE, ?pR004C, ?pR004B, ?pR004D)");

  for (int i = 0; i < InfChLB->Items->Count; i++)
   {
     DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_0046");
     DM->quFree->ParamByName("pR004C")->AsInteger = InfChLB->Items->Items[i]->Tag;
     DM->quFree->ParamByName("pR004B")->AsInteger = FTestCode;
     if (InfChLB->Items->Items[i]->Checked)
      DM->quFree->ParamByName("pR004D")->AsInteger = Pause2ED->Text.ToIntDef(1);
     else
      DM->quFree->ParamByName("pR004D")->AsInteger = Pause1ED->Text.ToIntDef(1);
     DM->quFree->ExecQuery();
   }
  // ����� "�����/�������� - ����������"
  FSetQuery("Insert into class_0066 (CODE, R0074, R0073, R0075) values (?pCODE, ?pR0074, ?pR0073, ?pR0075)");

  for (int i = 0; i < InfChLB->Items->Count; i++)
   {
     DM->quFree->ParamByName("pCODE")->AsInteger  = DM->GetMCode("MCODE_0066");
     DM->quFree->ParamByName("pR0074")->AsInteger = FTestCode;
     DM->quFree->ParamByName("pR0073")->AsInteger = InfChLB->Items->Items[i]->Tag;
     DM->quFree->ParamByName("pR0075")->AsInteger  = Pause3ED->Text.ToIntDef(1);
     DM->quFree->ExecQuery();
   }
  if (DM->quFree->Transaction->Active) DM->quFree->Transaction->Commit();
}
//---------------------------------------------------------------------------
bool __fastcall TMainForm::CheckSetupInput()
{
  bool RC = false;
  try
   {
     /*if (!UpdateNameED->Text.Trim().Length())
      {
        UpdateNameED->SetFocus();
        _MSG_ERR("���������� ������� ������������ ����������.","������");
      }
     else */if (!VerED->Text.Trim().Length())
      {
        VerED->SetFocus();
        _MSG_ERR("���������� ������� ������ ���������, ��� ������� �������� ����������.","������");
      }
     else
      RC = true;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::actCreateSetupExecute(TObject *Sender)
{
  if (CheckSetupInput())
   {
     AnsiString FAppName, FOutFileName, FVer;
     FAppName = FOutFileName = FVer = "";
     TStringList *FISSScript = new TStringList;
     PrBar->Position = 0;
     try
      {
        TReplaceFlags repF;
        repF << rfReplaceAll;

        AnsiString FCurDir = ExtractFilePath(ParamStr(0));
        AnsiString FScriptFN = icsPrePath(FCurDir+"\\Data\\vac_update.iss");
        AnsiString FDestScriptFN = icsPrePath(FCurDir+"\\_vac_update.iss");

        FVer = VerED->Text.Trim();
        if (FileExists(FScriptFN))
         {
           if (UpdateNameED->Text.Trim().Length())
            FAppName = UpdateNameED->Text.Trim();
           else
            {
              if (UpdVacChB->Checked && UpdTestChB->Checked)
               FAppName = "���������� ������ ������ � ����";
              else if (UpdVacChB->Checked)
               FAppName = "���������� ������ ������";
              else if (UpdTestChB->Checked)
               FAppName = "���������� ������ ����";

              if (VacSchUpdateChB->Checked && TestSchUpdateChB->Checked)
               FAppName += ", ���� �� ����������";
              else if (VacSchUpdateChB->Checked)
               {
                 if (UpdVacChB->Checked && UpdTestChB->Checked)
                  FAppName += ", ���� ���������� ������";
                 else
                  FAppName += ", ���� �� ����������";
               }
              else if (TestSchUpdateChB->Checked)
               {
                 if (UpdVacChB->Checked && UpdTestChB->Checked)
                  FAppName += ", ���� ���������� ����";
                 else
                  FAppName += ", ���� �� ����������";
               }
              FAppName += " "+Date().FormatString("yyyy")+".";
            }

           if (OutFNED->Text.Trim().Length())
            FOutFileName = OutFNED->Text.Trim();
           else
            {
              if (UpdVacChB->Checked && UpdTestChB->Checked)
               FOutFileName = "vac_update";
              else if (UpdVacChB->Checked)
               FOutFileName = "vac_update";
              else if (UpdTestChB->Checked)
               FOutFileName = "test_update";

              FOutFileName += "_" + StringReplace(FVer,".", "",repF)+"_"+Date().FormatString("yyyy");

              if (!VacSchUpdateChB->Checked && !TestSchUpdateChB->Checked)
               FOutFileName += ".���_����";
              else if (!VacSchUpdateChB->Checked)
               {
                 FOutFileName += ".���_����_������";
               }
              else if (!TestSchUpdateChB->Checked)
               {
                 FOutFileName += ".���_����_����";
               }

              if ((CheckVacCB->ItemIndex == -1) && (CheckTestCB->ItemIndex == -1))
               FOutFileName += ".���_��������";
              else if (CheckVacCB->ItemIndex == -1)
               {
                 FOutFileName += ".���_��������_������";
               }
              else if (CheckTestCB->ItemIndex == -1)
               {
                 FOutFileName += ".���_��������_����";
               }
            }

           FISSScript->LoadFromFile(FScriptFN);
           PrBar->Position = 5;Application->ProcessMessages(); // ++++
           AnsiString FScript = FISSScript->Text;

           FScript = StringReplace(FScript,"_APPNAME_", FAppName,repF);
           FScript = StringReplace(FScript,"_VER_",     FVer,repF);
           FScript = StringReplace(FScript,"_APPDATE_", Date().FormatString("dd.mm.yyyy"),repF);
           FScript = StringReplace(FScript,"_Y_RIGHT_", Date().FormatString("yyyy"),repF);
           FScript = StringReplace(FScript,"_OUT_FILE_NAME_", FOutFileName,repF);

           int FCheckVacCode = 0;
           int FCheckTestCode = 0;
           if (!DirectoryExists(icsPrePath(FCurDir+"\\XML")))
            ForceDirectories(icsPrePath(FCurDir+"\\XML"));
           if (UpdVacChB->Checked)
            {
              if (VacSchUpdateChB->Checked)
               {
                 DM->XMLList->GetXML("12063611-00008cd7-cd89")->SaveToZIPXMLFile(icsPrePath(FCurDir+"\\XML\\12063611-00008cd7-cd89.zxml"),"");
                 FScript = StringReplace(FScript,"_UPD_VAC_SCH_SRC_","Source: \"XML\\12063611-00008cd7-cd89.zxml\"; DestDir: \"{app}\\Script\"; Flags: ignoreversion",repF);
                 FScript = StringReplace(FScript,"_UPD_VAC_SCH_","Filename: \"{app}\\Script\\FBBlob.exe\";   Parameters: \" \"\"{reg:HKLM\\{#_RegKey}\\DataBase,Path|HKLM\\{#_RegKey64}\\DataBase,Path}\"\" BINARY_DATA.FLD_VALUE NAME='12063611-00008CD7-CD89' \"\"{app}\\Script\\12063611-00008cd7-cd89.zxml\"\" \"; StatusMsg: \"���������� �� (����� ���������� ������) ...\"; Check: ToolCheck;",repF);
               }
              else
               {
                 FScript = StringReplace(FScript,"_UPD_VAC_SCH_SRC_","",repF);
                 FScript = StringReplace(FScript,"_UPD_VAC_SCH_","",repF);
               }
              if (CheckVacCB->ItemIndex != -1)
               FCheckVacCode = (int)CheckVacCB->Properties->Items->Objects[CheckVacCB->ItemIndex];
            }
           else
            {
              FScript = StringReplace(FScript,"_UPD_VAC_SCH_SRC_","",repF);
              FScript = StringReplace(FScript,"_UPD_VAC_SCH_","",repF);
            }
           PrBar->Position = 15;Application->ProcessMessages(); // ++++
           if (UpdTestChB->Checked)
            {
              if (TestSchUpdateChB->Checked)
               {
                 DM->XMLList->GetXML("001D3500-00005882-DC28")->SaveToZIPXMLFile(icsPrePath(FCurDir+"\\XML\\001D3500-00005882-DC28.zxml"),"");
                 FScript = StringReplace(FScript,"_UPD_TEST_SCH_SRC_","Source: \"XML\\001D3500-00005882-DC28.zxml\"; DestDir: \"{app}\\Script\"; Flags: ignoreversion",repF);
                 FScript = StringReplace(FScript,"_UPD_TEST_SCH_","Filename: \"{app}\\Script\\FBBlob.exe\";   Parameters: \" \"\"{reg:HKLM\\{#_RegKey}\\DataBase,Path|HKLM\\{#_RegKey64}\\DataBase,Path}\"\" BINARY_DATA.FLD_VALUE NAME='001D3500-00005882-DC28' \"\"{app}\\Script\\001D3500-00005882-DC28.zxml\"\" \"; StatusMsg: \"���������� �� (����� ���������� ����) ...\"; Check: ToolCheck;",repF);
               }
              else
               {
                 FScript = StringReplace(FScript,"_UPD_TEST_SCH_SRC_","\n",repF);
                 FScript = StringReplace(FScript,"_UPD_TEST_SCH_","\n",repF);
               }
              if (CheckTestCB->ItemIndex != -1)
               FCheckTestCode = (int)CheckTestCB->Properties->Items->Objects[CheckTestCB->ItemIndex];
            }
           else
            {
              FScript = StringReplace(FScript,"_UPD_TEST_SCH_SRC_","\n",repF);
              FScript = StringReplace(FScript,"_UPD_TEST_SCH_","\n",repF);
            }

           if (TestSchUpdateChB->Checked || VacSchUpdateChB->Checked)
            FScript = StringReplace(FScript,"_UPD_CHECK_SCH_","Filename: \"{app}\\Script\\check_sch.exe\";   Parameters: \"-a\"; StatusMsg: \"���������� ��  ...\"; Check: ToolCheck;",repF);
           else
            FScript = StringReplace(FScript,"_UPD_CHECK_SCH_","\n",repF);

           PrBar->Position = 20;Application->ProcessMessages(); // ++++
           if (FCheckVacCode && FCheckTestCode)
            FScript = StringReplace(FScript,"_CHECK_SCRIPT_","\"\"select pr.code, ts.code from class_0035 pr, CLASS_002A ts where  pr.code = "+IntToStr(FCheckVacCode)+" and ts.code = "+IntToStr(FCheckTestCode)+"\"\"",repF);
           else if (FCheckVacCode)
            FScript = StringReplace(FScript,"_CHECK_SCRIPT_","\"\"select pr.code from class_0035 pr where  pr.code = "+IntToStr(FCheckVacCode)+"\"\"",repF);
           else if (FCheckTestCode)
            FScript = StringReplace(FScript,"_CHECK_SCRIPT_","\"\"select ts.code from CLASS_002A ts where  ts.code = "+IntToStr(FCheckTestCode)+"\"\"",repF);
           else
            FScript = StringReplace(FScript,"_CHECK_SCRIPT_","",repF);

           FISSScript->Text = FScript;
           FISSScript->SaveToFile(FDestScriptFN);
           PrBar->Position = 30;Application->ProcessMessages(); // ++++
           FScript = "";


           TStringList *FClassList = new TStringList;

           try
            {
              FClassList->Text = "003A\n0035\n0042\n002D\n0039\n002A\n0079\n002B\n0046\n0066\n000A";
              PrBar->Properties->Max = FClassList->Count*3;
              // disable triggers
              for (int i = 0; i < FClassList->Count; i++)
               {
                 FScript += DisableTriggers(FClassList->Strings[i]);
                 PrBar->Position++; Application->ProcessMessages();
               }
              // copy tables data
              for (int i = 0; i < FClassList->Count; i++)
               {
                 FScript += AddDeleteScript(FClassList->Strings[i]);
                 PrBar->Position++; Application->ProcessMessages();
               }
              // enable triggers
              for (int i = 0; i < FClassList->Count; i++)
               {
                 FScript += EnableTriggers(FClassList->Strings[i]);
                 PrBar->Position++; Application->ProcessMessages();
               }
              if (ExtScript->Lines->Text.Trim().Length())
               FScript += "\n"+ExtScript->Lines->Text.Trim();
              FISSScript->Text = FScript;
              FISSScript->SaveToFile(icsPrePath(FCurDir+"\\update.sql"));
              PrBar->Position = 100;Application->ProcessMessages(); // ++++
              WinExec(("\""+icsPrePath(FCurDir+"\\iss\\Compil32.exe")+"\" /cc \""+FDestScriptFN+"\"").c_str(),SW_SHOW);
            }
           __finally
            {
              delete FClassList;
            }
         }
        else
         _MSG_ERR("����������� ������ �������.","������");
      }
     __finally
      {
        delete FISSScript;
        PrBar->Position = 0;
      }
//     if (CheckVacCB->ItemIndex != -1)
//      {
//      }
//     else if (CheckTestCB->ItemIndex != -1)
//      {
//      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UpdVacChBClick(TObject *Sender)
{
  actCreateSetup->Enabled = UpdVacChB->Checked || UpdTestChB->Checked;
  if (UpdVacChB->Checked)
   {
     VacSchUpdateChB->Enabled = true;
     VacSchUpdateChB->Checked = true;
   }
  else
   {
     VacSchUpdateChB->Checked = false;
     VacSchUpdateChB->Enabled = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::UpdTestChBClick(TObject *Sender)
{
  actCreateSetup->Enabled = UpdVacChB->Checked || UpdTestChB->Checked;
  if (UpdTestChB->Checked)
   {
     TestSchUpdateChB->Checked = true;
     TestSchUpdateChB->Enabled = true;
   }
  else
   {
     TestSchUpdateChB->Checked = false;
     TestSchUpdateChB->Enabled = false;
   }
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::DisableTriggers(const AnsiString &AClsID)
{
  AnsiString RC = "";
  try
   {
     DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_"+AClsID.UpperCase()+"%'", false);
     while (!DM->quFree->Eof)
      {
        if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_"+AClsID.UpperCase()+"_BI5") && (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_"+AClsID.UpperCase()+"_BU5"))
         {
           RC += "ALTER TRIGGER "+DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase()+" INACTIVE;\n";
         }
        DM->quFree->Next();
      }
     DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_A"+AClsID.UpperCase()+"%'", false);
     while (!DM->quFree->Eof)
      {
        if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A"+AClsID.UpperCase()+"_BI5") && (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A"+AClsID.UpperCase()+"_BU5"))
         {
           RC += "ALTER TRIGGER "+DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase()+" INACTIVE;\n";
         }
        DM->quFree->Next();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::EnableTriggers(const AnsiString &AClsID)
{
  AnsiString RC = "";
  try
   {
     DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_"+AClsID.UpperCase()+"%'", false);
     while (!DM->quFree->Eof)
      {
        if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_"+AClsID.UpperCase()+"_BI5") && (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_"+AClsID.UpperCase()+"_BU5"))
         {
           RC += "ALTER TRIGGER "+DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase()+" ACTIVE;\n";
         }
        DM->quFree->Next();
      }
     DM->qtExec("Select RDB$TRIGGER_NAME From RDB$TRIGGERS Where Upper(RDB$RELATION_NAME) LIKE 'CLASS_A"+AClsID.UpperCase()+"%'", false);
     while (!DM->quFree->Eof)
      {
        if ((DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A"+AClsID.UpperCase()+"_BI5") && (DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase() != "CLASS_A"+AClsID.UpperCase()+"_BU5"))
         {
           RC += "ALTER TRIGGER "+DM->quFree->FN("RDB$TRIGGER_NAME")->AsString.Trim().UpperCase()+" ACTIVE;\n";
         }
        DM->quFree->Next();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainForm::AddDeleteScript(const AnsiString &AClsID)
{
  AnsiString RC = "Delete from class_"+AClsID+";\nDelete from class_a"+AClsID+";\n";
  FFLList.clear();
  FFLTypes.clear();
  TReplaceFlags repF;
  repF << rfReplaceAll;
  try
   {
     TTagNode *FClsDefNode = DM->xmlRegDef->GetTagByUID(AClsID);
     if (FClsDefNode)
      {
        TTagNode *itCls = FClsDefNode->GetFirstChild()->GetNext();
        while (itCls)
         {
           FFLList[itCls->AV["uid"]] = "R"+itCls->AV["uid"];
           if (itCls->CmpName("text,date,datetime,time,extedit"))
            FFLTypes[itCls->AV["uid"]] = "C";
           else
            FFLTypes[itCls->AV["uid"]] = "I";
           itCls = itCls->GetNext();
         }

        AnsiString FInsertFl, FInsertVal;
        DM->qtExec("Select * From class_"+AClsID+"", false);
        while (!DM->quFree->Eof)
         {
           FInsertFl  = AClsID+" (code";
           FInsertVal = ") values ("+ DM->quFree->FN("CODE")->AsString;
           for (TAnsiStrMap::iterator i = FFLList.begin(); i != FFLList.end(); i++ )
            {
              FInsertFl  += ", "+i->second;
              if (DM->quFree->FN(i->second)->IsNull)
               FInsertVal += ", null";
              else
               {
                 if (FFLTypes[i->first] == "C")
                  FInsertVal += ", '"+StringReplace(DM->quFree->FN(i->second)->AsString,"'","`",repF)+"'";
                 else
                  FInsertVal += ","+StringReplace(DM->quFree->FN(i->second)->AsString,",",".",repF);
               }
            }
           FInsertFl += FInsertVal += ");\n";
           RC += "Insert into class_"+FInsertFl+"Insert into class_A"+FInsertFl;
           DM->quFree->Next();
         }
      }
     else
      {
        _MSG_ERR("������ � ��������� ����������� ID=\""+AClsID+"\".","������");
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::actSaveDefSchExecute(TObject *Sender)
{
  TTagNode *FVacSchDef  = DM->XMLList->GetXML("12063611-00008cd7-cd89");
  for (int i = 0; i < VacSchList->Items->Count; i++)
   {
     FVacSchDef->GetTagByUID(UIDStr(VacSchList->Items->Items[i]->Tag))->AV["def"] = IntToStr((int)VacSchList->Items->Items[i]->Checked);
   }
  TFileStream *BinData = NULL;
  try
   {
//     BinData = new TFileStream("C:\\Program Files\\AXE\\samples\\��������\\prep\\11381C3E-00003981-56B0.zxml",fmOpenRead);
     DM->quFree->Close();
     if (DM->quFree->Transaction->Active) DM->quFree->Transaction->Commit();
     DM->quFree->Transaction->StartTransaction();
     DM->quFree->SQL->Clear();
     DM->quFree->SQL->Add("Update BINARY_DATA Set FLD_VALUE = ?DDD Where NAME='12063611-00008CD7-CD89'");
     DM->quFree->ParamByName("DDD")->AsString = FVacSchDef->AsZIPXML;
     DM->quFree->Prepare();
     DM->quFree->ExecQuery();
     DM->quFree->Transaction->Commit();
   }
  __finally
   {
//     if (BinData) delete BinData;
   }
}
//---------------------------------------------------------------------------

