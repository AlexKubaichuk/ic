//---------------------------------------------------------------------------
#ifndef ConnectH
#define ConnectH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLabel.hpp"
#include "cxProgressBar.hpp"
//---------------------------------------------------------------------------
#include "AxeUtil.h"
#include "ICSDBConnect.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TICSLoadImportCfgEvent)(TTagNode *ACfgNode);
class PACKAGE EConnectError : public Exception
{
public:
     __fastcall EConnectError(const AnsiString Msg) : Sysutils::Exception(Msg) { };
};
//---------------------------------------------------------------------------
class TConnectForm : public TForm
{
__published:
        TLabel *MsgLab;
        TLabel *Version;
        TLabel *Autor;
        TcxProgressBar *PrBar;
        TcxLabel *LabVer;
        TcxLabel *ProdName;
        TImage *Image1;
        void __fastcall FormDestroy(TObject *Sender);
private:
//     TAttr *xAtt;
     AnsiString GUI;
     TStringList  *InqXMLList;
     TICSLoadImportCfgEvent FOnLoadImportConfig;
     bool   ErrFix;
     AnsiString SD,tmpTabName;
     TStringList  *tmpFields;
     TStringList  *tmpTables;
     int    sStep,ssdl_oCount;
     TTagNode  *iTag;
     TTagNode  *RuleTag;
     void __fastcall PrInit(int AMax, AnsiString AMsg);
     void __fastcall PrCh(int APercent, AnsiString AMsg);
     void __fastcall PrComp(AnsiString AMsg);
     bool __fastcall FGetDBName(const AnsiString AMessage, AnsiString &AName, AnsiString &ALogin, AnsiString &APwd);
     bool __fastcall FLoadImportCfg(TTagNode *ACfgNode);
     void __fastcall LoadLPUParam();
     bool __fastcall PrepareInqData(AnsiString DBName);
     void __fastcall LoadRegXML(TTagNode *ARegXML, TTagNode *ASelRegXML);
//     bool __fastcall CreateSQLs(TTagNode *itTag, AnsiString &UID);
     bool __fastcall LoadXMLs(TTagNode *itTag, AnsiString &UID);
     void __fastcall ConCallBack(int APercent,AnsiString Mess);
//     bool __fastcall SetUIDRefers(TTagNode *itTag, AnsiString &UID);
     void __fastcall NumTag(TTagNode *ANode, AnsiString Num);
     bool __fastcall SetDelegateAttr(TTagNode *AConf, TTagNode *ADataDef);
     bool __fastcall FSetDelegateAttr(TTagNode *AConf, TTagNode *ADataDef);
     void __fastcall LoadIMMXML(TTagNode *AImmNode, TTagNode *ARegNode);
     void __fastcall LoadArcIMMXML(TTagNode *AImmNode, TTagNode *ARegNode);
     void __fastcall SetDBParam(TpFIBDatabase *ADB, AnsiString ADBCharset,AnsiString ADBUser, AnsiString ADBPassword);
     int   tagCount;
     TICSDBConnect *ConMod;
public:
    __fastcall TConnectForm(TComponent* Owner);
    bool __fastcall Connect();
    __property TICSLoadImportCfgEvent OnLoadImportConfig = {read=FOnLoadImportConfig,write=FOnLoadImportConfig};

};
//---------------------------------------------------------------------------
extern MODE;
extern PACKAGE TConnectForm *ConnectForm;
//---------------------------------------------------------------------------
#endif

