﻿// ---------------------------------------------------------------------------
/*
 Туберкулез               Туб.
 Полиомиелит              Полио.
 Дифтерия                 Дифт.
 Коклюш                   Кокл.
 Столбняк                 Столб.
 Корь                     Корь
 Паротит                  Парот.
 Бешенство                Бешен.
 Грипп                    Грипп
 Клещевой энцефалит       Клещ.
 Ку-лихорадка             Ку-лих.
 Менингококковая инфекция Менинг.
 Краснуха                 Красн.
 Гепатит В                Геп.В
 ХИБ-инфекция             ХИБ.
 Пневмококковая инфекция  Пневм.
 Трианатоксин             Триан.
 Туляремия                Туляр.
 Лептоспироз              Лептосп.
 Сибирская язва           Сиб.яз.
 Бруцеллез                Бруц.
 Брюшной тиф              Бр.тиф
 Чума                     Чума
 Глобулины                Глобулины
 Ветряная оспа            Ветр.оспа
 Гепатит А                Геп.А
 Холера                   Холера
 Дизентерия Зонне         Диз.Зонне
 Ротавирусная инфекция    Ротавир.
 Желтая лихорадка         Желт.лих.
 Вирус папилломы человека ВПЧ
 */
#pragma hdrstop
#include "DMUnit.h"
#include "ExtUtils.h"
#include "msgdef.h"
#include "FilterEditEx.h"
#include "MainLPUCodeEdit.h"
// #include "SetDefAddr.h"
#include <IdHTTP.hpp>
#include "DBSelect.h"
// #include "SetSkin.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma link "AppOptBaseProv"
#pragma link "AppOptDialog"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinsForm"
#pragma resource "*.dfm"
TDM * DM;
// ---------------------------------------------------------------------------
__fastcall TDM::TDM(TComponent * Owner) : TDataModule(Owner)
 {
  FXMLList              = new TAxeXMLContainer;
  FXMLList->OnExtGetXML = FExtGetXML;
  FAppOptValNode        = new TTagNode;
  FCliOptValNode        = new TTagNode;
  FExtFilter            = new TTagNode;
  FDataPath             = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\" + PRODFOLDER);
  FXMLPath              = icsPrePath(FDataPath + "\\xml");
  FReqColor             = clInfoBk;
  UnicodeString FFN = icsPrePath(FXMLPath + "\\" + PRODFOLDER + "_opt.xml");
  if (!FileExists(FFN))
   {
    ForceDirectories(FXMLPath);
    TTagNode * tmp = new TTagNode;
    try
     {
      tmp->Encoding = "utf-8";
      tmp->AsXML    =
        "<g><i n='0001' v='localhost'/><i n='0002' v='5100'/><i n='0003' v='localhost'/><i n='0004' v='5100'/><i n='0005' v='localhost'/><i n='0006' v='5100'/><i n='0016' v=''/><i n='0014' v=''/><i n='0015' v=''/></g>";
      tmp->SaveToXMLFile(FFN, "");
     }
    __finally
     {
      delete tmp;
     }
   }
  ConnectOptXMLData->FileName = FFN;
  ConnectOpt->LoadXML();
  FClients = new TConnectDM(this, GetHostParamStr());
  // ##############################################################################
  // #                                                                            #
  // #                            TdsEIData  (приём/передача)                     #
  // #                                                                            #
  // ##############################################################################
  // EIDataComp = new TdsEIData(this, FClients);
  // ##############################################################################
  // #                                                                            #
  // #                            TdsRegClient                                    #
  // #                                                                            #
  // ##############################################################################
  FRegComp                   = new TdsRegClient(this);
  FRegComp->XMLList          = FXMLList;
  FRegComp->OnGetClassXML    = FdsGetClassXML;
  FRegComp->OnGetCount       = FdsGetCount;
  FRegComp->OnExtCount       = FdsGetExtCount;
  FRegComp->OnGetIdList      = FdsGetIdList;
  FRegComp->OnGetValById     = FdsGetValById;
  FRegComp->OnGetValById10   = FdsGetValById10;
  FRegComp->OnFind           = FdsFind;
  FRegComp->OnInsertData     = FdsInsertData;
  FRegComp->OnEditData       = FdsEditData;
  FRegComp->OnDeleteData     = FdsDeleteData;
  FRegComp->FetchAll         = TdsRegFetch::Auto;
  FRegComp->ClassFetchAll    = TdsRegFetch::Auto;
  FRegComp->FullEdit         = true;//FileExists(icsPrePath(FDataPath + "\\fulledit.cfg"));
  FRegComp->SingleEditCol    = false;
  FRegComp->ClsSingleEditCol = true;
  FRegComp->OnExtBtnClick    = FRegCompExtEdit;
  // FRegComp->StyleController = icStyle;
  FRegComp->OnGetDefValues   = FdsOnSetDefValues;
  FRegComp->OnExtValidate    = FdsOnExtValidate;
  FRegComp->OnOpenCard       = FOpenCard;
  // FRegComp->OnShowPrePlan    = FdsOnShowPrePlan;
  FRegComp->OnPrintF63       = FdsOnPrintF63;
  // FRegComp->OnImportData     = EIDataComp->ImportData; // приём карт
  // FRegComp->OnExportData     = EIDataComp->ExportData; // выдача карт :)
  FRegComp->OnInsPriv        = FInsPriv; // добавление прививки пробы для группы пациентов
  // FRegComp->OnInsPTCData     = FInsPrivTest; // добавление прививки пробы для группы пациентов
  FRegComp->StyleController  = EditStyle;
  FRegComp->XMLPath          = FXMLPath;
  FRegComp->DataPath         = FDataPath;
  FRegComp->OnCtrlDataChange = FRegCompCtrlDataChange;
  // ##############################################################################
  // #                                                                            #
  // #                            TdsICCardClient                                 #
  // #                                                                            #
  // ##############################################################################
  // FICCardComp                  = new TdsICCardClient(this);
  // FICCardComp->XMLList         = FXMLList;
  // FICCardComp->OnGetClassXML   = FdsGetClassXML;
  // FICCardComp->OnGetCount      = FdsGetCount;
  // FICCardComp->OnGetIdList     = FdsGetIdList;
  // FICCardComp->OnGetValById    = FdsGetValById;
  // FICCardComp->OnGetValById10  = FdsGetValById10;
  // FICCardComp->OnFind          = FdsFind;
  // FICCardComp->OnInsertData    = FdsInsertData;
  // FICCardComp->OnEditData      = FdsEditData;
  // FICCardComp->OnDeleteData    = FdsDeleteData;
  // FICCardComp->OnGetF63        = FdsGetF63;
  // FICCardComp->OnGetVacDefDoze = FdsGetVacDefDoze;
  // FICCardComp->OnGetDefValues  = FdsOnSetDefValues;
  // FICCardComp->OnExtBtnClick   = FRegCompExtEdit;
  // FICCardComp->OnGetUnitData   = FGetUnitData;
  // FICCardComp->OnGetPatSetting = FGetUnitPlanOptions;
  // FICCardComp->OnSetPatSetting = FSetUnitPlanOptions;
  // FICCardComp->OnGetPrivVar    = FGetPrivVar;
  // FICCardComp->OnGetOtvodInf   = FGetOtvodInf;
  // FICCardComp->OnHTMLPreview   = FHTMLPreviewDoc;
  // FICCardComp->OnCreatePlan    = FPatCreatePlan;
  // FICCardComp->StyleController = EditStyle;
  // FICCardComp->XMLPath         = FXMLPath;
  // FICCardComp->DataPath      = FDataPath;
  // FICCardComp->OnGetListById = FdsGetListById;
  // FICCardComp->OnExtValidate = FdsOnExtValidate;
  // ##############################################################################
  // #                                                                            #
  // #                            TdsSchEditClient                                #
  // #                                                                            #
  // ##############################################################################
  FICSchComp          = new TdsSchEditClient(this);
  FICSchComp->XMLList = FXMLList;
  // FICSchComp->OnGetClassXML   = FdsGetClassXML;
  // FICSchComp->OnGetCount      = FdsGetCount;
  // FICSchComp->OnGetIdList     = FdsGetIdList;
  FICSchComp->OnGetValById   = FdsGetValById;
  FICSchComp->OnGetValById10 = FdsGetValById10;
  // FICSchComp->OnFind          = FdsFind;
  // FICSchComp->OnInsertData    = FdsInsertData;
  // FICSchComp->OnEditData      = FdsEditData;
  // FICSchComp->OnDeleteData    = FdsDeleteData;
  // FICSchComp->OnGetDefValues  = FdsOnSetDefValues;
  // FICSchComp->OnExtBtnClick   = FRegCompExtEdit;
  FICSchComp->OnGetPlanOpts = FdsGetPlanOpts;
  // FICSchComp->NmklGUI = tmp->AV["xmlref"];
  FICSchComp->OnCallDefProc   = FdsDocCallDefProc;
  FICSchComp->StyleController = EditStyle;
  FICSchComp->XMLPath         = FXMLPath;
  // FICSchComp->DataPath        = FDataPath;
  FICSchComp->OnSaveXML     = FOnSaveScheme;
  FICSchComp->OnGetListById = FdsGetListById;
  // FICSchComp->OnExtValidate   = FdsOnExtValidate;
  // FICSchComp->Nom             = Nom;
  // ##############################################################################
  // #                                                                            #
  // #                            TdsICPlanClient                                 #
  // #                                                                            #
  // ##############################################################################
  FICPlanComp                 = new TdsICPlanClient(this);
  FICPlanComp->XMLList        = FXMLList;
  FICPlanComp->OnGetClassXML  = FdsGetClassXML;
  FICPlanComp->OnGetCount     = FdsGetCount;
  FICPlanComp->OnGetIdList    = FdsGetIdList;
  FICPlanComp->OnGetValById   = FdsGetValById;
  FICPlanComp->OnGetValById10 = FdsGetValById10;
  FICPlanComp->OnFind         = FdsFind;
  FICPlanComp->OnInsertData   = FdsInsertData;
  FICPlanComp->OnEditData     = FdsEditData;
  FICPlanComp->OnDeleteData   = FdsDeleteData;
  // FICPlanComp->OnCreatePlan              = FdsPlanCreate;
  FICPlanComp->OnDeletePlan              = FdsDeletePlan;
  // FICPlanComp->OnDeletePlanItem          = FdsDeletePlanItem;
  FICPlanComp->OnStartCreatePlan         = FdsStartCreatePlan;
  FICPlanComp->OnStopCreatePlan          = FdsStopCreatePlan;
  FICPlanComp->OnCheckCreatePlanProgress = FdsCheckCreatePlanProgress;
  FICPlanComp->OnGetPlanPrintFormats     = FdsGetPlanPrintFormats;
  FICPlanComp->OnPlanPrint               = FdsPlanPrint;
  // FICPlanComp->OnCardOpen                = FPlanOpenCard;
  FICPlanComp->OnPreviewDoc              = FPreviewDoc;
  FICPlanComp->OnGetPlanTemplateDef      = FGetPlanTemplateDef;
  // FICPlanComp->OnPlanPrivInsert          = FICCardComp->PlanPrivInsert;
  // FICPlanComp->OnPlanTestInsert          = FICCardComp->PlanTestInsert;
  // FICPlanComp->OnPlanCheckInsert         = FICCardComp->PlanCheckInsert;
  FICPlanComp->OnExtBtnClick             = FRegCompExtEdit;
  FICPlanComp->OnGetUnitData             = FGetUnitData;
  FICPlanComp->AppOpt                    = AppOpt;
  FICPlanComp->OnShowVacSch              = FdsShowVacSch;
  FICPlanComp->OnShowTestSch             = FdsShowTestSch;
  FICPlanComp->OnShowCheckSch            = FdsShowCheckSch;
  FICPlanComp->StyleController           = EditStyle;
  // FICPlanComp->XMLPath                   = FXMLPath;
  FICPlanComp->DataPath          = FDataPath;
  FICPlanComp->OnPrePostTestList = ShowPrePostTestList;
  FICPlanComp->OnGetPlanOpts     = FdsGetPlanOpts;
  FICPlanComp->OnSetPlanOpts     = FdsSetPlanOpts;
  FICPlanComp->OnGetListById     = FdsGetListById;
  FICPlanComp->OnExtValidate     = FdsOnExtValidate;
  FICPlanComp->OnGetF63          = FdsGetShortF63;
  FICPlanComp->OnGetVacDefDoze   = FdsGetVacDefDoze;
  FICPlanComp->OnGetAddrStr      = AddrCodeToStr;
  FICPlanComp->OnGetMKBStr       = MKBCodeToStr;
  FICPlanComp->OnGetOrgStr       = OrgCodeToStr;
  FICPlanComp->OnGetUchStr       = UchCodeToStr;
  // ##############################################################################
  // #                                                                            #
  // #                            TdsICStoreClient                                #
  // #                                                                            #
  // ##############################################################################
  // FICStoreComp                  = new TdsICStoreClient(this);
  // FICStoreComp->XMLList         = FXMLList;
  // FICStoreComp->OnGetClassXML   = FdsVSGetClassXML;
  // FICStoreComp->OnGetCount      = FdsVSGetCount;
  // FICStoreComp->OnGetIdList     = FdsVSGetIdList;
  // FICStoreComp->OnGetValById    = FdsVSGetValById;
  // FICStoreComp->OnGetValById10  = FdsVSGetValById10;
  // FICStoreComp->OnFind          = FdsVSFind;
  // FICStoreComp->OnInsertData    = FdsVSInsertData;
  // FICStoreComp->OnEditData      = FdsVSEditData;
  // FICStoreComp->OnDeleteData    = FdsVSDeleteData;
  // FICStoreComp->OnGetMIBPForm   = FdsVSGetMIBPForm;
  // FICStoreComp->OnExtBtnClick   = FRegCompExtEdit;
  // FICStoreComp->StyleController = EditStyle;
  // FICStoreComp->XMLPath         = FXMLPath;
  // FICStoreComp->DataPath        = FDataPath;
  // // FICStoreComp->StyleController = icStyle;
  // FICStoreComp->RegComp       = FRegComp;
  // FICStoreComp->OnExtValidate = FdsOnExtValidate;
  // ##############################################################################
  // #                                                                            #
  // #                            TdsDocClient                                    #
  // #                                                                            #
  // ##############################################################################
  FDocComp          = new TdsDocClient(this);
  FDocComp->XMLList = FXMLList;
  FDocComp->SubDir  = "IC";
  // FDocComp->Connection      = FClients->Doc;
  FDocComp->OnExtBtnClick = FRegCompExtEdit;
  // FDocComp->StyleController = icStyle;
  FDocComp->OnGetClassXML          = FdsDocGetClassXML;
  FDocComp->OnGetClassZIPXML       = FdsDocGetClassZIPXML;
  FDocComp->OnGetCount             = FdsDocGetCount;
  FDocComp->OnGetIdList            = FdsDocGetIdList;
  FDocComp->OnGetValById           = FdsDocGetValById;
  FDocComp->OnMainAppGetValById    = FdsGetValById;
  FDocComp->OnGetValById10         = FdsDocGetValById10;
  FDocComp->OnFind                 = FdsDocFind;
  FDocComp->OnImportData           = FdsDocImportData;
  FDocComp->OnInsertData           = FdsDocInsertData;
  FDocComp->OnEditData             = FdsDocEditData;
  FDocComp->OnDeleteData           = FdsDocDeleteData;
  FDocComp->OnGetSpec              = FdsDocGetSpec;
  FDocComp->OnGetDocById           = FdsDocGetDocById;
  FDocComp->OnGetDocPreview        = FdsDocGetDocPreview;
  FDocComp->OnLoadDocPreview       = FdsDocLoadDocPreview;
  FDocComp->OnCheckByPeriod        = FdsDocCheckByPeriod;
  FDocComp->OnDocListClearByDate   = FdsDocDocListClearByDate;
  FDocComp->OnCheckDocCreate       = FdsDocCheckDocCreate;
  FDocComp->OnCheckExistDocCreate  = FdsDocCheckExistDocCreate;
  FDocComp->OnDocCreate            = FdsDocDocCreate;
  FDocComp->OnDocCreateCheckFilter = FdsDocCreateCheckFilter;
  FDocComp->OnSaveDoc              = FdsDocSaveDoc;
  FDocComp->OnStartDocCreate       = FdsDocStartDocCreate;
  FDocComp->OnDocCreateProgress    = FdsDocDocCreateProgress;
  FDocComp->OnGetQList             = FdsDocGetQList;
  FDocComp->OnGetDocCountBySpec    = FdsDocGetDocCountBySpec;
  FDocComp->OnIsMainOwnerCode      = FdsDocIsMainOwnerCode;
  FDocComp->OnCopySpec             = FdsDocCopySpec;
  FDocComp->OnCheckSpecExists      = FdsDocCheckSpecExists;
  FDocComp->OnSetText              = FdsDocSetText;
  FDocComp->OnGetText              = FdsDocGetText;
  FDocComp->OnCallDefProc          = FdsDocCallDefProc;
  FDocComp->OnCallDocDefProc       = FdsDocCallDocDefProc;
  FDocComp->StyleController        = EditStyle;
  FDocComp->XMLPath                = FXMLPath;
  FDocComp->DataPath               = FDataPath;
  FDocComp->OnExtValidate          = FdsOnExtValidate;
  FDocComp->OnCanHandEdit          = FdsCanHandEdit;
  // *****************************************************
  FRegComp->UseQuickFilterSetting  = true;
  FRegComp->OnExtFilterSet         = FdsOnExtFilterSet;
  FRegComp->OnGetQuickFilterList   = FDocComp->GetQuickList; // FdsOnGetQuickFilterList;
  FRegComp->UseReportSetting       = true;
  FRegComp->OnGetReportList        = FDocComp->GetQuickList; // FdsOnGetReportList;
  FRegComp->OnPrintReportClick     = FdsOnPrintReportClick;
  FICPlanComp->OnGetReportList     = FDocComp->GetQuickList; // FdsOnGetReportList;
  FRegDef                          = new TTagNode;
  Application->ProcessMessages();
  Application->OnException = FAppExept;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::DataModuleDestroy(TObject * Sender)
 {
  try
   {
    FClients->Admin->Logout();
   }
  catch (...)
   {
   }
  Sleep(100);
  delete FExtFilter;
  delete FDocComp;
  // delete FICStoreComp;
  // delete FICCardComp;
  delete FRegComp;
  // delete EIDataComp;
  delete FRegDef;
  delete FAppOptValNode;
  delete FCliOptValNode;
  delete FClients;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnPrintReportClick(UnicodeString ARepGUI, __int64 AUCode, UnicodeString AFilter,
  TkabProgress AProgressProc)
 {
  UnicodeString RepGUI;
  if (AProgressProc)
   RepGUI = FDocComp->CreateDocument(AProgressProc, ARepGUI, AFilter);
  else
   RepGUI = FDocComp->CreateDocument(CheckProgress, ARepGUI, AFilter);
  if (RepGUI.Length())
   FDocComp->PreviewDocument(RepGUI, CheckProgress);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      // PrBar->Max = AVal;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      // Caption = FSaveCaption+"  ["+AMsg+"]";
      // PrBar->Position = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      // Caption = FSaveCaption;
      break;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
/* void __fastcall TDM::FdsOnGetReportList(TStringList *AList)
 {
 FDocComp->GetReportList(AList);
 }
 //---------------------------------------------------------------------------
 */
UnicodeString __fastcall TDM::FdsOnExtFilterSet(TTagNode * ADefNode, bool & CanContinue, UnicodeString AFilterGUI)
 {
  UnicodeString RC = "";
  try
   {
    if (SameText(AFilterGUI, "clear"))
     {
      FExtFilter->AsXML = "<node/>";
     }
    else
     {
      if (AFilterGUI.Length())
       {
        bool FShowEditor = true;
        bool FTemplate = false;
        UnicodeString FFltRC = FClients->Doc->CheckFilter(AFilterGUI);
        bool ChRC = false;
        bool FConcr = false;
        if (SameText(FFltRC, "error"))
         { // фильтр кривой.
          RC          = FFltRC;
          FShowEditor = false;
         }
        else if (SameText(FFltRC, "ok"))
         { // фильтр не требует конкретизации и формируется на уровне сервера по кодам
          if (AFilterGUI.Pos("<?xml"))
           {
            FTemplate = true;
            RC        = AFilterGUI;
            FFltRC    = AFilterGUI;
           }
          else
           {
            FShowEditor = false;
            RC          = "{\"GUI\":\"" + AFilterGUI + "\"}";
           }
          CanContinue = true;
         }
        if (FShowEditor)
         { // фильтр требует конкретизации.
          TFilterEditExForm * Dlg = new TFilterEditExForm(this);
          TTagNode * __tmpFlt = new TTagNode;
          try
           {
            __tmpFlt->AsXML    = FFltRC;
            Dlg->OnCallDefProc = FdsDocCallDefProc;
            if (FTemplate)
             Dlg->EditType = fetTemplate;
            else
             Dlg->EditType = fetKonkr;
            Dlg->Nom          = FDocComp->Nom;
            Dlg->XMLContainer = FXMLList;
            Dlg->Filter       = __tmpFlt;
            Dlg->ShowModal();
            if (Dlg->ModalResult == mrOk)
             {
              TTagNode * FCondRules = NULL;
              if (Dlg->Filter->CmpName("region"))
               {
                if (Dlg->Filter->GetChildByName("condrules"))
                 FCondRules = Dlg->Filter->GetChildByName("condrules");
               }
              else if (Dlg->Filter->CmpName("condrules"))
               FCondRules = Dlg->Filter;
              if (FCondRules)
               {
                RC          = Dlg->Filter->AsXML;
                CanContinue = true;
               }
             }
           }
          __finally
           {
            delete Dlg;
            delete __tmpFlt;
           }
         }
       }
      else
       {
        TFilterEditExForm * Dlg = new TFilterEditExForm(this);
        try
         {
          // UnicodeString flName;
          // long Code;
          Dlg->OnCallDefProc = FdsDocCallDefProc;
          Dlg->EditType      = fetTemplate;
          Dlg->Nom           = FDocComp->Nom;
          Dlg->XMLContainer  = FXMLList;
          Dlg->CanKonkr      = true;
          if (FExtFilter->CmpName("region, condrules"))
           Dlg->Filter = FExtFilter;
          else
           {
            TTagNode * tmpFlt = new TTagNode;
            tmpFlt->AsXML =
              "<?xml version='1.0' encoding='utf-8'?>" "<condrules uniontype='and'>"
              " <RAND not='0' pure='1' comment='Пациент'>" "  <IERef uid='000C' ref='0E291426-00005882-2493.0560' not='1' incltoname='1'>"
              "   <text name='&amp;Статус обслуживания=Архив' value='5' ref='0'/>" "   <text name='sql'/>" "  </IERef>"
              " </RAND>" "</condrules>";
            Dlg->Filter = tmpFlt;
            delete tmpFlt;
           }
          // Dlg->New();
          Dlg->Caption = "Параметры отбора";
          Dlg->ShowModal();
          if (Dlg->ModalResult == mrOk)
           {
            RC = Dlg->Filter->AsXML;
            FExtFilter->Assign(Dlg->Filter, true);
            CanContinue = true;
           }
         }
        __finally
         {
          delete Dlg;
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/*
 void __fastcall TDM::FdsOnGetQuickFilterList(TStringList *AList)
 {
 FDocComp->GetQFilterList(AList);
 }
 //---------------------------------------------------------------------------
 */
bool __fastcall TDM::PreloadData()
 {
  bool RC = false;
  try
   {
    LoadCliOptData();
    LoadAppOptData();
    // FICCardComp->PreloadData();
    FICPlanComp->PreloadData();
    FICNom                 = new TdsICNomSupport(FDocComp->XMLList, "40381E23-92155860-4448");
    FICNom->OnGetValById   = FdsGetValById;
    FICNom->OnGetClassXML  = FdsGetClassXML;
    FICNom->OnExtBtnClick  = FRegCompExtEdit;
    FICNom->DataPath       = FDataPath;
    FDocNom                = new TdsDocNomSupport(FDocComp->XMLList, "40381E23-92155860-4448");
    FDocNom->OnGetValById  = FdsDocGetValById;
    FDocNom->OnGetClassXML = FdsDocGetClassXML;
    __int64 FLPUCode = FClients->App->LPUCode();
    // EIDataComp->LPUCode     = FLPUCode;
    // FICCardComp->LPUCode    = FLPUCode;
    FICPlanComp->LPUCode    = FLPUCode;
    FDocComp->MainOwnerCode = FClients->Doc->MainOwnerCode();
    FICNom->LPUCode         = FLPUCode;
    FDocNom->LPUCode        = FLPUCode;
    FDocNom->DataPath       = FDataPath;
    FICSchComp->PreloadData();
    FICSchComp->Nom = FDocComp->Nom;
    UnicodeString FlfKind = CliOpt->Vals["skkind"].AsStringDef("lfStandard");
    if (FlfKind == "lfStandard")
     Skin->Kind = lfStandard;
    else if (FlfKind == "lfFlat")
     Skin->Kind = lfFlat;
    else if (FlfKind == "lfUltraFlat")
     Skin->Kind = lfUltraFlat;
    else if (FlfKind == "lfOffice11")
     Skin->Kind = lfOffice11;
    // Skin->Kind        = (TcxLookAndFeelKind)CliOpt->Vals["kind"].AsIntDef(0);
    Skin->NativeStyle = CliOpt->Vals["sknativestyle"].AsIntDef(false);
    Skin->SkinName    = CliOpt->Vals["skinname"].AsStringDef("");
    Application->ProcessMessages();
    if (CliOpt->Vals["ReqColor"].AsIntDef(false))
     {
      FReqColor = clYellow;
     }
    FRegComp->RequiredColor = FReqColor;
    // FICCardComp->RequiredColor = FReqColor;
    FICPlanComp->RequiredColor = FReqColor;
    FICSchComp->RequiredColor  = FReqColor;
    // EIDataComp->RequiredColor = FReqColor;
    // FICStoreComp->RequiredColor = FReqColor;
    FDocComp->RequiredColor     = FReqColor;
    FDocComp->ExternalView      = CliOpt->Vals["extview"].AsStringDef("");
    RC                          = true;
   }
  catch (System::Sysutils::Exception & E)
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetClassXML(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetClassXML(ARef);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSGetClassXML(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->GetClassXML(ARef);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TDM::FdsGetCount(System::UnicodeString AId, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    UnicodeString EC = "";
    RC = FClients->App->GetCount(AId, AFilterParam, EC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsGetExtCount(UnicodeString & ARC)
 {
  try
   {
    ARC = "GetCount";
    FClients->App->GetCount("reg.1000", "", ARC);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TDM::FdsVSGetCount(System::UnicodeString AId, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    RC = FClients->VS->GetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetListById(System::UnicodeString AId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    TJSONMap::iterator FRC = FDataRequest.find(AId.UpperCase());
    if (FRC == FDataRequest.end())
     FDataRequest[AId.UpperCase()] = (TJSONObject *)TJSONObject::ParseJSONValue
       (FClients->App->GetValById(AId, "0")->ToString());
    ARC = FDataRequest[AId.UpperCase()];
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::GetListById(System::UnicodeString AId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    RC = FdsGetListById(AId, ARC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetIdList(AId, AMax, AFilterParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->GetIdList(AId, AMax, AFilterParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetValById(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetValById10(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->GetValById(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetQList(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    if (SameText(ARecId, "set"))
     { // set
      FClients->Doc->SetQList(AId, ARC);
      RC = true;
     }
    else
     { // get
      ARC = FClients->Doc->GetQList(AId);
      RC  = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->GetValById10(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->Find(AId, AFindParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->Find(AId, AFindParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsInsertData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    // ShowMessage("11.4.1.1");
    ARC = FClients->App->InsertData(AId, AValue, ARecID);
    // ShowMessage("11.4.1.2");
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::InsertData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  // ShowMessage("11.4.1");
  return FdsInsertData(AId, AValue, ARecID, ARC);
  // ShowMessage("11.4.2");
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSInsertData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->InsertData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetAddr(System::UnicodeString & AAddr, System::UnicodeString ADefAddr, int AParam)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * RetData;
    UnicodeString FRetAddr;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    FRetAddr = "";
    RetData  = FClients->App->FindAddr(AAddr, DM->CliOpt->Vals["defAddr"].AsStringDef(""), AParam);
    itPair   = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (SameText(FClCode, "msg"))
       RC = FClStr;
      else
       FRetAddr += FClCode + "=" + FClStr + "\n";
     }
    AAddr = FRetAddr;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::AddrCodeToStr(UnicodeString AAddrCode)
 {
  UnicodeString RC = "error";
  try
   {
    RC = AddrCodeToStrF(AAddrCode, 0xFF);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::AddrCodeToStrF(UnicodeString AAddrCode, int AParam)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FClients->App->AddrCodeToText(AAddrCode, AParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetOrg(System::UnicodeString & AOrg)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * RetData;
    UnicodeString FRetAddr;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    FRetAddr = "";
    RetData  = FClients->App->FindOrg(AOrg);
    itPair   = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (SameText(FClCode, "msg"))
       RC = FClStr;
      else
       FRetAddr += FClCode + "=" + FClStr + "\n";
     }
    AOrg = FRetAddr;
    RC = "ok";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::OrgCodeToStr(UnicodeString AOrgCode)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FClients->App->OrgCodeToText(AOrgCode, "11111111");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::UchCodeToStr(UnicodeString AOrgCode)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * Uch = FClients->App->GetValById("reg.f.000E.000F", AOrgCode);
    if (Uch)
     RC = ((TJSONString *)Uch->Pairs[0]->JsonValue)->Value();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetMKB(System::UnicodeString & AMKB)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * RetData;
    UnicodeString FRetMKB;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    FRetMKB = "";
    RetData = FClients->App->FindMKB(AMKB);
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (SameText(FClCode, "msg"))
       RC = FClStr;
      else
       FRetMKB += FClCode + "=" + FClStr + "\n";
     }
    AMKB = FRetMKB;
    RC = "ok";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::MKBCodeToStr(UnicodeString AMKBCode)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FClients->App->MKBCodeToText(AMKBCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsEditData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->EditData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSEditData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->EditData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->DeleteData(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSDeleteData(System::UnicodeString AId, System::UnicodeString ARecId,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->DeleteData(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetF63(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  TTagNode * FParams = new TTagNode;
  TTagNode * tmpParam;
  FParams->Name = "p";
  try
   {
    bool FFull63 = (ARC.Trim().LowerCase() == "all");
    FParams->AddChild("i", "n=Full")->AV["v"] = IntToStr((int)FFull63);
    FParams->AddChild("i", "n=Vac")->AV["v"] = (FFull63) ? UnicodeString("1") : CliOpt->Vals["f63Vac"].AsString;
    // . Признак необходимости помещать в документ список сделанных прививок
    FParams->AddChild("i", "n=Test")->AV["v"] = (FFull63) ? UnicodeString("1") : CliOpt->Vals["f63Test"].AsString;
    // Признак необходимости помещать в документ список сделанных проб
    FParams->AddChild("i", "n=Otv")->AV["v"] = (FFull63) ? UnicodeString("1") : CliOpt->Vals["f63Otv"].AsString;
    // . Признак необходимости помещать в документ список медотводов
    FParams->AddChild("i", "n=Plan")->AV["v"] = (FFull63) ? UnicodeString("1") : CliOpt->Vals["f63Plan"].AsString;
    // Признак необходимости помещать в документ план на указанный месяц ( см. PlanMonth )
    FParams->AddChild("i", "n=tbsNotCheck")->AV["v"] = CliOpt->Vals["f63tbsNotCheck"].AsString;
    // .                  Реакция не проверена, отображать как:
    FParams->AddChild("i", "n=tbsComm")->AV["v"] = CliOpt->Vals["f63tbsComm"].AsString;
    // .                          Наличие общей/местной реакции, отображать как:
    FParams->AddChild("i", "n=tbsNotComm")->AV["v"] = CliOpt->Vals["f63tbsNotComm"].AsString;
    // .                    Отсутствие общей/местной реакции, отображать как:
    FParams->AddChild("i", "n=otherNotCheck")->AV["v"] = CliOpt->Vals["f63otherNotCheck"].AsString;
    // .              Реакция не проверена, отображать как:
    FParams->AddChild("i", "n=otherComm")->AV["v"] = CliOpt->Vals["f63otherComm"].AsString;
    // .                      Наличие общей/местной реакции, отображать как:
    FParams->AddChild("i", "n=otherNotComm")->AV["v"] = CliOpt->Vals["f63otherNotComm"].AsString;
    // .                Отсутствие общей/местной реакции, отображать как:
    ARC = FClients->App->GetF63(ARef, FParams->AsXML);
    RC  = true;
   }
  __finally
   {
    delete FParams;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetShortF63(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  TTagNode * FParams = new TTagNode;
  TTagNode * tmpParam;
  FParams->Name = "p";
  try
   {
    FParams->AddChild("i", "n=Full")->AV["v"] = "short";
    FParams->AddChild("i", "n=Vac")->AV["v"] = "1";
    // . Признак необходимости помещать в документ список сделанных прививок
    FParams->AddChild("i", "n=Test")->AV["v"] = "1"; // Признак необходимости помещать в документ список сделанных проб
    FParams->AddChild("i", "n=Otv")->AV["v"] = "1"; // . Признак необходимости помещать в документ список медотводов
    FParams->AddChild("i", "n=Plan")->AV["v"] = "1";
    // Признак необходимости помещать в документ план на указанный месяц ( см. PlanMonth )
    FParams->AddChild("i", "n=tbsNotCheck")->AV["v"] = CliOpt->Vals["f63tbsNotCheck"].AsString;
    // .                  Реакция не проверена, отображать как:
    FParams->AddChild("i", "n=tbsComm")->AV["v"] = CliOpt->Vals["f63tbsComm"].AsString;
    // .                          Наличие общей/местной реакции, отображать как:
    FParams->AddChild("i", "n=tbsNotComm")->AV["v"] = CliOpt->Vals["f63tbsNotComm"].AsString;
    // .                    Отсутствие общей/местной реакции, отображать как:
    FParams->AddChild("i", "n=otherNotCheck")->AV["v"] = CliOpt->Vals["f63otherNotCheck"].AsString;
    // .              Реакция не проверена, отображать как:
    FParams->AddChild("i", "n=otherComm")->AV["v"] = CliOpt->Vals["f63otherComm"].AsString;
    // .                      Наличие общей/местной реакции, отображать как:
    FParams->AddChild("i", "n=otherNotComm")->AV["v"] = CliOpt->Vals["f63otherNotComm"].AsString;
    // .                Отсутствие общей/местной реакции, отображать как:
    ARC = FClients->App->GetF63(ARef, FParams->AsXML);
    RC  = true;
   }
  __finally
   {
    delete FParams;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsGetVacDefDoze(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode)
 {
  UnicodeString RC = "0,0";
  try
   {
    RC = FClients->App->GetVacDefDoze(APrivDate, APatBirthday, AVacCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetVacDefDoze(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode)
 {
  return FdsGetVacDefDoze(APrivDate, APatBirthday, AVacCode);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsVSGetMIBPForm(System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->VS->GetMIBPForm();
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetClassXML(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetClassXML(ARef);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetClassZIPXML(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetClassZIPXML(ARef);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TDM::FdsDocGetCount(System::UnicodeString AId, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    RC = FClients->Doc->GetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam,
  TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetIdList(AId, AMax, AFilterParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetValById(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetValById10(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->Find(AId, AFindParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsDocImportData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->Doc->ImportData(AId, AValue, ARecID);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocInsertData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->InsertData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocEditData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->EditData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocDeleteData(System::UnicodeString AId, System::UnicodeString ARecId,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->DeleteData(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsDocGetText(System::UnicodeString ATabId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString & AData, System::UnicodeString & ARC)
 {
  try
   {
    AData = FClients->Doc->GetTextData(ATabId, AFlId, ARecId, ARC);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsDocSetText(System::UnicodeString ATabId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString AData, System::UnicodeString & ARC)
 {
  try
   {
    if (AData.Length() < 5200)
     {
      ARC = FClients->Doc->SetTextData(ATabId, AFlId, ARecId, AData);
     }
    else
     {
      int FLen = 5120;
      FClients->Doc->SetTextData("BEGIN#" + ATabId, AFlId, ARecId, AData.SubString(1, FLen));
      for (int i = FLen + 1; i <= AData.Length(); i += FLen)
       {
        if ((i + FLen) > AData.Length())
         {
          FLen = AData.Length() - i + 1;
          ARC  = FClients->Doc->SetTextData("END#" + ATabId, AFlId, ARecId, AData.SubString(i, FLen));
         }
        else
         ARC = FClients->Doc->SetTextData("PROGRESS#" + ATabId, AFlId, ARecId, AData.SubString(i, FLen));
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetSpec(System::UnicodeString AId, int & ASpecType, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetSpecById(AId, ASpecType);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetDocById(System::UnicodeString AId, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->GetDocById(AId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FPreviewDoc(UnicodeString ATxt_Id, bool AByCode)
 {
  if (AByCode)
   FDocComp->PreviewDocument(ATxt_Id, NULL);
  else
   FDocComp->HTMLPreview(ATxt_Id);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FHTMLPreviewDoc(UnicodeString ATxt_Id)
 {
  FDocComp->HTMLPreview(ATxt_Id);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocGetDocPreview(System::UnicodeString AId, System::UnicodeString & ARC, int & Al, int & At,
  int & Ar, int & Ab, double & Aps, int & Ao)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->CreateDocPreview(AId, Al, At, Ar, Ab, Aps, Ao);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsDocLoadDocPreview(System::UnicodeString & AMsg)
 {
  return FClients->Doc->LoadDocPreviewProgress(AMsg);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocCheckByPeriod(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI,
  System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->CheckByPeriod(FCreateByType, ACode, ASpecGUI);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocDocListClearByDate(TDate ADate)
 {
  bool RC = false;
  try
   {
    FClients->Doc->DocListClearByDate(ADate);
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TDM::FdsDocGetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString & ARCMsg)
 {
  int RC = 0;
  try
   {
    RC = FClients->Doc->GetDocCountBySpec(ASpecOWNER, ASpecGUI, ARCMsg);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocIsMainOwnerCode(UnicodeString ASpecOwner)
 {
  bool RC = false;
  try
   {
    RC = FClients->Doc->IsMainOwnerCode(ASpecOwner);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocCopySpec(UnicodeString AName, UnicodeString & ANewGUI, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->Doc->CopySpec(AName, ANewGUI);
    RC  = SameText(ARC, "ok");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TDM::FdsDocCheckSpecExists(UnicodeString AName, UnicodeString & AGUI, bool AIsEdit)
 {
  int RC = 0;
  try
   {
    RC = FClients->Doc->CheckSpecExists(AName, AGUI, AIsEdit);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocCheckDocCreate(UnicodeString ASpecGUI, int & ACreateType, int ADefCreateType,
  int & AGetCreateType, int & AGetPeriod)
 {
  bool RC = false;
  try
   {
    RC = FClients->Doc->CheckDocCreateType(ASpecGUI, ACreateType, ADefCreateType, AGetCreateType, AGetPeriod);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocCheckExistDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, int APerType,
  TDate APeriodDateFr, TDate FPeriodDateTo, System::UnicodeString & ADocGUI)
 {
  bool RC = false;
  try
   {
    ADocGUI = FClients->Doc->DocCreateCheckExist(ASpecGUI, AAuthor, APerType, APeriodDateFr, FPeriodDateTo);
    RC      = ADocGUI.Length();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsDocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilter,
  UnicodeString & AFilterName, UnicodeString & AFilterGUI)
 {
  UnicodeString RC = "ошибка";
  try
   {
    RC = FClients->Doc->DocCreateCheckFilter(ASpecGUI, AFilter, AFilterName, AFilterGUI);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, bool AConcr,
  int ACreateType, int APerType, TDate APeriodDateFr, TDate FPeriodDateTo, System::UnicodeString & ADocGUI)
 {
  bool RC = false;
  try
   {
    ADocGUI = FClients->Doc->DocCreate(ASpecGUI, AAuthor, AFilter, AConcr, ACreateType, APerType, APeriodDateFr,
      FPeriodDateTo);
    RC = ADocGUI.Length();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsDocSaveDoc(UnicodeString ADocGUI)
 {
  try
   {
    FClients->Doc->SaveDoc(ADocGUI);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocStartDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter,
  UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo,
  bool ANotSave)
 {
  bool RC = false;
  try
   {
    UnicodeString DCRC = FClients->Doc->StartDocCreate(ASpecGUI, AAuthor, AFilter, AFilterNameGUI, AConcr, ACreateType,
      APerType, APeriodDateFr, APeriodDateTo, ANotSave);
    RC = SameText(DCRC, "ok");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsDocDocCreateProgress(System::UnicodeString & ADocGUI)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->Doc->DocCreateProgress(ADocGUI);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocCallDefProc(System::UnicodeString AProcName, TTagNode * AIENode, bool ANeedKonkr,
  System::UnicodeString & AText)
 {
  bool RC = false;
  try
   {
    UnicodeString FIEXML;
    RC = FICNom->GetDVal(AProcName, AIENode, AText, ANeedKonkr, FIEXML);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDocCallDocDefProc(System::UnicodeString AProcName, TTagNode * AIENode, bool ANeedKonkr,
  System::UnicodeString & AText)
 {
  bool RC = false;
  try
   {
    UnicodeString FIEXML;
    RC = FDocNom->GetDVal(AProcName, AIENode, AText, ANeedKonkr, FIEXML);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FRegCompCtrlDataChange(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource,
  bool isAppend, TObject * ACtrlList, int ABtnIdx)
 {
  bool RC = false;
#ifndef IMM_DOCUMENT
  TdsRegEDContainer * CtrList = (TdsRegEDContainer *)ACtrlList;
  bool FTemplate = !ASource;
  try
   {
    try
     {
      if (!ItTag)
       {
        // Src = "_NULL_=Ошибка( не задано описание)";
        // RC = false;
       }
      else
       {
        if (ItTag->CmpAV("uid", "0025"))
         { // организация О, Д
          if (Src.Length())
           {
            UnicodeString OrgCode = GetLPartB(Src, '@');
            if (OrgCode.Length())
             {
              OrgCode = GetRPartB(OrgCode, '.');
              int OrgType = OrgCode.ToIntDef(0);
              if (OrgType == 2)
               { // обслуживаемая
                // Статус обслуживания = Обслуживается по организации value='2'
                CtrList->EditItems["32B3"]->SetValue("2", "");
                CtrList->EditItems["00F0"]->SetValue("1", "");
               }
              else if (OrgType == 3)
               { // контролируемая
                // Статус обслуживания = Контролируется value='4
                CtrList->EditItems["32B3"]->SetValue("4", "");
                CtrList->EditItems["00F0"]->SetValue("0", "");
               }
              else
               CtrList->EditItems["00F0"]->SetValue("0", "");
             }
           }
          // UnicodeString FRC = GetOrg(Src);
          // if (!SameText(FRC, "ok"))
          // _MSG_ERRA(FRC, "Сообщение");
          // else
          // RC = true;
         }
        /*
         if (isAppend)
         {                                 // click по кнопке
         UnicodeString FSrc = Src.Trim(); // Производитель МИБП,ЛПУ,Подразделение ЛПУ,Пациент,Архив адресов,
         if (ItTag->CmpAV("uid", "0025"))
         { // организация О, Д
         UnicodeString FRC = GetOrg(Src);
         if (!SameText(FRC, "ok"))
         _MSG_ERRA(FRC, "Сообщение");
         else
         RC = true;
         }
         }
         else
         { // получение значения
         if (ItTag->CmpAV("uid", "0025"))
         { // организация О, Д
         Src += "=" + OrgCodeToStr(Src);
         RC = true;
         }
         }
         */
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      Src = "_NULL_=Ошибка (" + E.Message + ")";
     }
   }
  __finally
   {
   }
#endif
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FRegCompExtEdit(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource,
  bool isClicked, TObject * ACtrlList, int ABtnIdx)
 {
  bool RC = false;
#ifndef IMM_DOCUMENT
  TdsRegEDContainer * CtrList = (TdsRegEDContainer *)ACtrlList;
  bool FTemplate = !ASource;
  try
   {
    try
     {
      if (!ItTag)
       {
        Src = "_NULL_=Ошибка( не задано описание)";
        // RC = false;
       }
      else
       {
        if (isClicked)
         { // click по кнопке
          UnicodeString FSrc = Src.Trim(); // Производитель МИБП,ЛПУ,Подразделение ЛПУ,Пациент,Архив адресов,
          if (ItTag->CmpAV("uid", "019D, 00CC, 00E4, 0136, 015A"))
           { // адрес
            UnicodeString FRC = GetAddr(Src, CliOpt->Vals["defAddr"].AsStringDef(""), 0x1F);
            if (!SameText(FRC, "ok"))
             _MSG_ERRA(FRC, "Сообщение");
            else
             RC = true;
           }
          else if (ItTag->CmpAV("uid", "0102"))
           { // адрес, участки по адресам, детализация до улицы, без неформализованных адресов
            UnicodeString FRC = GetAddr(Src, CliOpt->Vals["defAddr"].AsStringDef(""), 0x18);
            if (!SameText(FRC, "ok"))
             _MSG_ERRA(FRC, "Сообщение");
            else
             RC = true;
           }
          else if (ItTag->CmpAV("uid", "0025"))
           { // организация О, Д
            UnicodeString FRC = GetOrg(Src);
            if (!SameText(FRC, "ok"))
             _MSG_ERRA(FRC, "Сообщение");
            else
             RC = true;
           }
          else if (ItTag->CmpAV("uid", "1082,3082,3304,32FF"))
           { // отвод, заболевание
            UnicodeString FRC = GetMKB(Src);
            if (!SameText(FRC, "ok"))
             _MSG_ERRA(FRC, "Сообщение");
            else
             RC = true;
           }
         }
        else
         { // получение значения
          if (ItTag->CmpAV("uid", "019D, 00CC, 00E4, 0136, 015A, 0102"))
           { // адрес
            Src += "=" + AddrCodeToStrF(Src, 0xFE);
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "0025"))
           { // организация О, Д
            Src += "=" + OrgCodeToStr(Src);
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "1082,3082,3304,32FF"))
           { // отвод, заболевание
            Src += "=" + MKBCodeToStr(Src);
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "1092,1093,1107"))
           { // схемы
            Src += "=" + Src;
            RC = true;
           }
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      Src = "_NULL_=Ошибка (" + E.Message + ")";
     }
   }
  __finally
   {
   }
#endif
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FGetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData)
 {
  bool RC = false;
  try
   {
    if (ACode)
     {
      RC = FRegComp->GetUnitData(ACode, AData);
     }
    else
     {
      FRegComp->GetSelectedUnitData(AData);
      if (AData)
       RC = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FPatCreatePlan(__int64 ACode)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->CreateUnitPlan(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/* UnicodeString __fastcall TDM::FdsPlanCreate(TJSONObject * AVal, UnicodeString & ARecId)
 {
 UnicodeString RC = "";
 try
 {
 //    RC = FClients->App->CreatePlan(AVal, ARecId);
 }
 __finally
 {
 }
 return RC;
 }
 */
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsDeletePlan(UnicodeString ARecId)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->DeletePlan(ARecId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsDeletePlanItem(UnicodeString AId, UnicodeString ARecId)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->DeletePatientPlan(AId, ARecId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsStartCreatePlan(TJSONObject * AVal)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->StartCreatePlan(AVal);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsStopCreatePlan(void)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->StopCreatePlan();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsCheckCreatePlanProgress(UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->CheckCreatePlanProgress(ARecId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsGetPlanPrintFormats(TJSONObject *& AData)
 {
  try
   {
    AData = FClients->App->GetPlanPrintFormats();
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FdsPlanPrint(__int64 ACode)
 {
  UnicodeString RC = "ОШИБКА";
  try
   {
    RC = FClients->App->PrintPlan(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FGetUnitPlanOptions(__int64 ACode)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->GetPatPlanOptions(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetUnitPlanOptions(__int64 ACode)
 {
  return FGetUnitPlanOptions(ACode);
 }
// ---------------------------------------------------------------------------
int __fastcall TDM::InfInPlan(TTagNode * AComRoot, TTagNode * APersRoot, UnicodeString AInf)
 {
  int RC = 0;
  try
   {
    if (AComRoot)
     RC = (int)(bool)((int)AComRoot->GetChildByAV("vpr", "infref", AInf));
    if (APersRoot && !RC)
     RC = (int)(bool)((int)APersRoot->GetChildByAV("vpr", "infref", AInf));
    if (RC)
     RC = 1;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FSetUnitPlanOptions(__int64 APatCode, System::UnicodeString AData)
 {
  try
   {
    FClients->App->SetPatPlanOptions(APatCode, AData);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FGetPrivVar(__int64 ACode)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->GetPrivVar(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FGetOtvodInf(__int64 ACode)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClients->App->GetOtvodInf(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FOpenCard(__int64 Code, UnicodeString UnitStr)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnPrintF63(__int64 Code, UnicodeString UnitStr)
 {
  try
   {
    UnicodeString F63 = UnitStr;
    if (FdsGetF63(IntToStr(Code), F63))
     {
      FHTMLPreviewDoc(F63);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadRegDef()
 {
  UnicodeString RegDefXML = "";
  try
   {
    RegDefXML = FClients->App->GetDefXML();
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA(E.Message, "Ошибка 1 загрузки описания регистратуры");
   }
  try
   {
    FRegDef->AsXML = RegDefXML;
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA(E.Message, "Ошибка 2 загрузки описания регистратуры");
    TStringList * msg = new TStringList;
    try
     {
      msg->Text = RegDefXML;
      msg->SaveToFile("err.xml");
     }
    __finally
     {
      delete msg;
     }
   }
  try
   {
    FRegComp->RegDef     = FRegDef;
    //FICCardComp->RegDef  = FRegDef;
    FICPlanComp->RegDef  = FRegDef;
    //FICStoreComp->RegDef = FRegDef;
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA(E.Message, "Ошибка 3 загрузки описания регистратуры");
   }
  try
   {
    FDocComp->RegDef->AsXML = FClients->Doc->GetDefXML();
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA(E.Message, "Ошибка 4 загрузки описания регистратуры");
   }
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TDM::FGetXMLList()
 {
  return FXMLList;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FExtGetXML(TTagNode * ANode, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    ANode->AsXML = FClients->Doc->GetClassZIPXML(Src);
    RC           = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnSetDefValues(__int64 ARecCode, TTagNode * ANode, TdsRegEDContainer * ControlList,
  bool & Valid)
 {
  if (ANode)
   {
    try
     {
      TTagNode * flNameField = ANode->GetChildByAV("", "fl", "cguid", true);
      if (flNameField)
       ControlList->EditItems[flNameField->AV["uid"]]->SetValue(icsNewGUID().UpperCase(), "");
      TdsRegEDItem * tmpItem;
      UnicodeString FVal;
      if (ANode->CmpAV("uid", "1000"))
       { // список пациентов
        if (FICPlanComp->LPUCode)
         ControlList->EditItems["00F1"]->SetValue(IntToStr(FICPlanComp->LPUCode), "");
       }
      else if (ANode->CmpAV("uid", "00AA"))
       { // список мед. персонала
        if (!ControlList->IsTemplate("0120"))
         if (FICPlanComp->LPUCode)
          ControlList->EditItems["0120"]->SetValue(IntToStr(FICPlanComp->LPUCode), "");
       }
      else if (ANode->CmpAV("uid", "000C"))
       { // отделения
        if (!ControlList->IsTemplate("00DB"))
         if (FICPlanComp->LPUCode)
          ControlList->EditItems["00DB"]->SetValue(IntToStr(FICPlanComp->LPUCode), "");
       }
      else if (ANode->CmpAV("uid", "000E"))
       { // участки
        if (!ControlList->IsTemplate("00F6"))
         if (FICPlanComp->LPUCode)
          ControlList->EditItems["00F6"]->SetValue(IntToStr(FICPlanComp->LPUCode), "");
       }
      else if (ANode->CmpAV("uid", "00E8"))
       { // участки по адресам
        if (!ControlList->IsTemplate("00FD"))
         if (FICPlanComp->LPUCode)
          ControlList->EditItems["00FD"]->SetValue(IntToStr(FICPlanComp->LPUCode), "");
       }
      else if (ANode->CmpAV("uid", "0001"))
       { // организации
        if (!ControlList->IsTemplate("3196"))
         if (FICPlanComp->LPUCode)
          ControlList->EditItems["3196"]->SetValue(IntToStr(FICPlanComp->LPUCode), "");
       }
      else if (ANode->CmpAV("uid", "00C7"))
       { // ЛПУ
        ControlList->EditItems["011E"]->SetValue(icsNewGUID().UpperCase(), "");
       }
      else if (ANode->CmpAV("uid", "1003"))
       { // прививки
        if (FICPlanComp->LPUCode)
         {
          ControlList->EditItems["017F"]->SetEnable(false);
          ControlList->EditItems["017F"]->SetValue(IntToStr(FICPlanComp->LPUCode), false);
         }
       }
      else if (ANode->CmpAV("uid", "102F"))
       { // пробы
        if (FICPlanComp->LPUCode)
         {
          ControlList->EditItems["018B"]->SetEnable(false);
          ControlList->EditItems["018B"]->SetValue(IntToStr(FICPlanComp->LPUCode), false);
         }
       }
      else if (ANode->CmpAV("uid", "1100"))
       { // проверки
        if (FICPlanComp->LPUCode)
         {
          ControlList->EditItems["1108"]->SetEnable(false);
          ControlList->EditItems["1108"]->SetValue(IntToStr(FICPlanComp->LPUCode), false);
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA("Ошибка получения значения по умолчанию.\nСистемное сообщение:\n" + E.Message, "Ошибка");
      Valid = false;
     }
    catch (...)
     {
      _MSG_ERRA("Ошибка получения значения по умолчанию.", "Ошибка");
      Valid = false;
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsCanHandEdit(UnicodeString ASpecGUID)
 {
  return FClients->Doc->CanHandEdit(ASpecGUID);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnExtValidate(__int64 ARecCode, TTagNode * ANode, TdsRegEDContainer * ControlList, bool & Valid)
 {
  // #ifndef IMM_DOCUMENT
  TdsRegEDItem * tmpItem;
  UnicodeString FVal, FVal1, FVal2;
  try
   {
    if (!Valid)
     { // Valid = false  - вызов до внутренней проверки контролов
      TTagNode * flNameField = ANode->GetChildByAV("", "fl", "cguid", true);
      if (flNameField)
       {
        FVal = ControlList->EditItems[flNameField->AV["uid"]]->GetValue("").Trim();
        if (!FVal.Length())
         ControlList->EditItems[flNameField->AV["uid"]]->SetValue(icsNewGUID().UpperCase(), "");
       }
     }
    else
     { // Valid = true  - вызов поле внутренней проверки контролов
      if (ANode->CmpAV("uid", "1000"))
       { // список пациентов
        // дата рождение ---------------------------------------------
        FVal = ControlList->EditItems["0031"]->GetValue("");
        TDateTime FDate;
        if (TryStrToDate(FVal, FDate))
         {
          TDateTime CurrDate, LastDate, OutDate;
          CurrDate = TDateTime::CurrentDate();
          LastDate = Dateutils::IncYear(CurrDate, -150);
          if (FDate > CurrDate)
           {
            _MSG_ERRA("Дата рождения не может быть больше текущей даты.", "Ошибка ввода");
            Valid = false;
           }
          else if (FDate < LastDate)
           {
            _MSG_ERRA("Некорректная дата рождения.\n Возраст не может быть больше 150 лет.", "Ошибка ввода");
            Valid = false;
           }
         }
        // № карты ---------------------------------------------
        FVal = ControlList->EditItems["0122"]->GetValue("").Trim();
        if (FVal.Length())
         {
          // DM->qtExec("Select Count(*) as RCOUNT From "+DM->RegComp->GetUnitTabName()+" Where code <> "+IntToStr(ARecCode)+" and "+DM->RegComp->UpperFunc+"(R0122)='"+FVal.UpperCase()+"'",false);
          // if (DM->quFree->FN("RCOUNT")->AsInteger)
          // {
          // _MSG_ERRA("Пациент с указанным номером карты уже существует.","Ошибка ввода");
          // Valid = false;
          // }
         }
        // проверка соответствия участка адресу ---------------------------------------------
        if (ControlList->EditItems["0022"]->GetValue("").ToIntDef(0))
         { // Адрес из справочника - checked && прикреплён - checked
          /*
           UnicodeString FTown    = ControlList->EditItems["0032"]->GetValue("");
           UnicodeString FStreet  = ControlList->EditItems["00FF"]->GetValue("");
           UnicodeString FHouse   = ControlList->EditItems["0100"]->GetValue("");
           UnicodeString FFlat    = ControlList->EditItems["0033"]->GetValue("");
           UnicodeString FUch     = ControlList->EditItems["00B3"]->GetValue("");
           int FUchCode;
           TStringList *FLPUUchList = new TStringList;
           try
           {
           UchByAddr(FTown, FStreet, FHouse, FFlat, "", FUchCode, FLPUUchList);
           if (FUch.Length() && (FUch.ToIntDef(-1) != -1))
           { // установлен участок
           if (FUchCode != -1)
           { // найден участок по адресу
           int idx = FLPUUchList->IndexOfObject((TObject*)FUch.ToIntDef(-1));
           if (idx == -1)
           { // установленный участок отсутствует в списке участков определённых по адресу
           UnicodeString FMsg = "Внимание!!!\nУказанный Вами адрес не относится\nк обслуживаемым адресам\nучастка \""+GetClassData(FUch.ToIntDef(-1),"000E")+"\"\n\nСохранить изменения?";
           Valid = (_MSG_QUEA(FMsg,"Предупреждение") == ID_YES);
           }
           // иначе
           // установленный участок присутствует в списке участков определённых по адресу, значит всё чудно, что выбрали, то выбрали
           }
           else
           { // по адресу ни чего не нашли, но участок указан, предупреждаем
           UnicodeString FMsg = "Внимание!!!\nУказанный Вами адрес не относится\nк обслуживаемым адресам ЛПУ.\n\nСохранить изменения?";
           Valid = (_MSG_QUEA(FMsg,"Предупреждение") == ID_YES);
           }
           }
           else
           { // участок не установлен
           if (FUchCode != -1)
           { // найден участок по адресу, предупреждаем
           UnicodeString FMsg = "Внимание!!!\nУказанный Вами адрес обслуживается одним из участков, но поле \"Участок\" не заполнено.\n\nСохранить изменения?";
           Valid = (_MSG_QUEA(FMsg,"Предупреждение") == ID_YES);
           }
           //иначе
           // участок не установлен, по адресу не найдено ни чего, тогда и фиг с ними %)
           }
           }
           __finally
           {
           delete FLPUUchList;
           }
           */
         }
        // проверка социального статуса
        FVal = ControlList->EditItems["0025"]->GetValue("").Trim(); // организация
        FVal1 = ControlList->EditItems["0023"]->GetValue("").Trim(); // социальный статус
        if ((FVal1.ToIntDef(-1) == 0) && FVal.Length())
         { // указан социальный статус "безработный" и указана организация
          _MSG_ERRA(
            "Указана организация, социальный статус не может быть - \"Безработный\", укажите другой социальный статус.",
            "Ошибка ввода");
          Valid = false;
         }
        // проверка статуса обслуживания
        // FVal  = ControlList->EditItems["0025"]->GetValue("").Trim(); // организация
        FVal1 = ControlList->EditItems["00B3"]->GetValue("").Trim(); // участок
        FVal2 = ControlList->EditItems["32B3"]->GetValue("").Trim(); // Статус обслуживания
        if ((FVal2.ToIntDef(-1) == 1) && !FVal1.ToIntDef(0))
         { // <choicevalue name='Обслуживается по участку' value='1'/>
          _MSG_ERRA("Указан cтатус обслуживания  - 'Обслуживается по участку', но не введен участок.", "Ошибка ввода");
          Valid = false;
         }
        else if ((FVal2.ToIntDef(-1) == 2) && !FVal.Length())
         { // <choicevalue name='Обслуживается по организации' value='2'/>
          _MSG_ERRA("Указан cтатус обслуживания  - 'Обслуживается по организации', но не введена организация.",
            "Ошибка ввода");
          Valid = false;
         }
        else if ((FVal2.ToIntDef(-1) == 3) && !FVal1.ToIntDef(0))
         { // <choicevalue name='Обслуживается по участку' value='1'/>
          _MSG_ERRA("Указан cтатус обслуживания  - 'Обслуживается по полису', но не введен участок.", "Ошибка ввода");
          Valid = false;
         }
        else if ((FVal2.ToIntDef(-1) == 4) && !FVal.Length())
         { // <choicevalue name='Обслуживается по организации' value='2'/>
          _MSG_ERRA("Указан cтатус обслуживания  - 'Контролируется', но не введена организация.", "Ошибка ввода");
          Valid = false;
         }
        // проверка полиса ОМС
        FVal = ControlList->EditItems["0093"]->GetValue("").Trim(); // страховая
        FVal1 = ControlList->EditItems["009A"]->GetValue("").Trim(); // номер полиса
        if (FVal.Length())
         { // указана Страх. компания, проверяем наличие номера
          if (!FVal1.Length())
           { // не указан номер полиса
            _MSG_ERRA("Необходимо ввести номер полиса.", "Ошибка ввода");
            Valid = false;
           }
         }
        else
         { // не указана страховая
          if (FVal1.Length())
           { // указан номер полиса
            _MSG_ERRA("Ввведён номер полиса. Необходимо указать страховую компанию.", "Ошибка ввода");
            Valid = false;
           }
         }
        // проверка удостоверения личности
        FVal = ControlList->EditItems["009C"]->GetValue("").Trim(); // уд. личности
        FVal1 = ControlList->EditItems["009D"]->GetValue("").Trim(); // серия
        FVal2 = ControlList->EditItems["009E"]->GetValue("").Trim(); // Номер
        if (FVal.ToIntDef(0))
         { // указано уд. личности  , проверяем наличие серии и номера
          if (!FVal1.Length())
           { // не указана серия
            _MSG_ERRA("Необходимо ввести серию удостоверения личности.", "Ошибка ввода");
            Valid = false;
           }
          else
           {
            if (!FVal2.Length())
             { // не указан номер у/л
              _MSG_ERRA("Необходимо ввести номер удостоверения личности.", "Ошибка ввода");
              Valid = false;
             }
           }
         }
        else
         {
          if (FVal2.Length() || FVal1.Length())
           { // не указана удостоверение личности
            _MSG_ERRA("Необходимо указать вид удостоверения личности.", "Ошибка ввода");
            Valid = false;
           }
         }
        // проверка (сохранение адреса) ---------------------------------------------
        FVal = ControlList->EditItems["0136"]->GetValue("").Trim();
        if (FVal.Length())
         {
          FVal = FVal;
         }
       }
      else if (ANode->CmpAV("uid", "00C7"))
       { // ЛПУ
        // код организации
        FVal = ControlList->EditItems["011E"]->GetValue("").Trim();
        if (FVal.Length())
         {
          if ((FVal == "812.2741381.000") || (FVal == "812.1107704.000") || (FVal == "812.7107704.000") ||
            (FVal == "812.7177704.000") || (FVal == "812.7170425.000") || (FVal == "812.7100425.000") ||
            (FVal == "812.1100425.000") || (FVal == "812.7171319.000"))
           {
            _MSG_ERRA("Указанный код ЛПУ зарезервирован, укажите другой код.", "Ошибка ввода");
            Valid = false;
           }
          else
           {
            TJSONObject * RetData = NULL;
            TJSONPairEnumerator * itPair;
            bool FCont = true;
            try
             {
              FdsGetValById("reg.s2.Select Count(*) as ps1,  '" + IntToStr(ARecCode) +
                "' as ps2 From CLASS_00C7 Where code <> " + IntToStr(ARecCode) + " and Upper(R011E)='" +
                FVal.UpperCase() + "'", "0", RetData);
              itPair = RetData->GetEnumerator();
              while (itPair->MoveNext() && FCont)
               {
                if (((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0))
                 {
                  FCont = false;
                  _MSG_ERRA("ЛПУ с указанным кодом уже существует, укажите другой код.", "Ошибка ввода");
                  Valid = false;
                 }
               }
             }
            __finally
             {
              // if (RetData) delete RetData;
              // RetData = NULL;
             }
           }
         }
       }
      else if (ANode->CmpAV("uid", "32F2"))
       { // Обслуживаемые организации    проверка на отсутствие в списке контролируемых
        /*
         UnicodeString OrgCode = GetLPartB(Src, '@');
         if (OrgCode.Length())
         OrgCode = GetLPartB(OrgCode, '.');
         */
        /*
         <class name='Обслуживаемые организации' uid='32F2' inlist='l'>
         <choiceDB name='Организация' uid='32F4' ref='0001' required='1' inlist='l'/>
         <class name='Контролируемые организации' uid='32F3' inlist='l'>
         <choiceDB name='Организация' uid='32F5' ref='0001' required='1' inlist='l'/>
         */
        // код организации
        FVal = ControlList->EditItems["32F4"]->GetValue("").Trim();
        if (FVal.Length())
         {
          TJSONObject * RetData = NULL;
          TJSONPairEnumerator * itPair;
          bool FCont = true;
          try
           {
            FdsGetValById("reg.s2.Select R32F5 as ps1,  '1' as ps2 From CLASS_32F3 Where R32F5 = " + FVal, "0",
            RetData);
            itPair = RetData->GetEnumerator();
            while (itPair->MoveNext() && FCont)
             {
              if (((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0))
               {
                FCont = false;
                _MSG_ERRA(
                  "Данная организация находится в списке контролируемых организаций, укажите другую организацию.",
                  "Ошибка ввода");
                Valid = false;
               }
             }
           }
          __finally
           {
            // if (RetData) delete RetData;
            // RetData = NULL;
           }
         }
       }
      else if (ANode->CmpAV("uid", "32F3"))
       { // Контролируемые организации, проверка на отсутствие в списке обслуживаемых
        /*
         UnicodeString OrgCode = GetLPartB(Src, '@');
         if (OrgCode.Length())
         OrgCode = GetLPartB(OrgCode, '.');
         */
        /*
         <class name='Обслуживаемые организации' uid='32F2' inlist='l'>
         <choiceDB name='Организация' uid='32F4' ref='0001' required='1' inlist='l'/>
         <class name='Контролируемые организации' uid='32F3' inlist='l'>
         <choiceDB name='Организация' uid='32F5' ref='0001' required='1' inlist='l'/>
         */
        // код организации
        FVal = ControlList->EditItems["32F5"]->GetValue("").Trim();
        if (FVal.Length())
         {
          TJSONObject * RetData = NULL;
          TJSONPairEnumerator * itPair;
          bool FCont = true;
          try
           {
            FdsGetValById("reg.s2.Select R32F4 as ps1,  '1' as ps2 From CLASS_32F2 Where R32F4 = " + FVal, "0",
            RetData);
            itPair = RetData->GetEnumerator();
            while (itPair->MoveNext() && FCont)
             {
              if (((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0))
               {
                FCont = false;
                _MSG_ERRA("Данная организация находится в списке обслуживаемых организаций, укажите другую организацию."
                  , "Ошибка ввода");
                Valid = false;
               }
             }
           }
          __finally
           {
            // if (RetData) delete RetData;
            // RetData = NULL;
           }
         }
       }
      else if (ANode->CmpAV("uid", "0000"))
       {
       }
     }
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA("Ошибка проверки корректности введённых данных.\nСистемное сообщение:\n" + E.Message, "Ошибка");
    Valid = false;
   }
  catch (...)
   {
    _MSG_ERRA("Ошибка проверки корректности введённых данных.", "Ошибка");
    Valid = false;
   }
  // #endif
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::FGetPlanTemplateDef(int AType)
 {
  return FClients->App->GetPlanTemplateDef(AType);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowVacSch()
 {
  FICSchComp->ShowSch(TSchemeObjType::Vac);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowTestSch()
 {
  FICSchComp->ShowSch(TSchemeObjType::Test);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowCheckSch()
 {
  FICSchComp->ShowSch(TSchemeObjType::Check);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetPlanOpts(UnicodeString & AOptDef)
 {
  bool RC = false;
  try
   {
    AOptDef = FClients->App->GetPlanOpt();
    RC      = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsSetPlanOpts(UnicodeString AOptDef)
 {
  try
   {
    FClients->App->SetPlanOpt(AOptDef);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
TICSNomenklator * __fastcall TDM::FGetNom()
 {
  if (FDocComp)
   return FDocComp->Nom;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FOnSaveScheme(TTagNode * TagNode, TSchemeObjType AType, TkabProgress AProgress)
 {
  bool RC = false;
  try
   {
    try
     {
      UploadData(TagNode->AsXML, AProgress);
      RC = FClients->App->SetClassZIPXML(TagNode->GetChildByName("passport", true)->AV["gui"]);
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA(E.Message, "Ошибка");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::UploadData(UnicodeString AData, TkabProgress AProgress)
 {
  try
   {
    try
     {
      int FLen;
      if (AProgress)
       AProgress(TkabProgressType::InitCommon, AData.Length(), "");
      FClients->App->UploadData("", 0);
      for (int i = 1; i <= AData.Length(); i += 2048)
       {
        FLen = 2048;
        if ((i + FLen) > AData.Length())
         FLen = AData.Length() - i + 1;
        if (AProgress)
         AProgress(TkabProgressType::CommPerc, i * 100 / AData.Length(), "");
        Application->ProcessMessages();
        FClients->App->UploadData(AData.SubString(i, FLen), 1);
        if (AProgress)
         AProgress(TkabProgressType::CommPerc, (i + 1024) * 100 / AData.Length(), "");
        Application->ProcessMessages();
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA(E.Message, "Ошибка");
     }
   }
  __finally
   {
    if (AProgress)
     AProgress(TkabProgressType::CommComplite, 0, "");
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::Connect(UnicodeString AUser, UnicodeString APaswd)
 {
  bool RC = false;
  try
   {
    Application->ProcessMessages();
    FClients->SetConnectParam(AUser, APaswd);
    try
     {
      Application->ProcessMessages();
      LoadRegDef();
      Application->ProcessMessages();
      RC = true;
     }
    catch (...)
     {
     }
   }
  __finally
   {
    Application->ProcessMessages();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetHostParamStr()
 {
  UnicodeString RC = "";
  TTagNode * ConnOptNode = new TTagNode;
  try
   {
    ConnOptNode->Name                = "s";
    ConnOptNode->AV["ic_server"]     = ConnectOpt->Vals["ic_server"].AsStringDef("localhost");
    ConnOptNode->AV["ic_port"]       = IntToStr(ConnectOpt->Vals["ic_port"].AsIntDef(5100));
    ConnOptNode->AV["ic_doc_server"] = ConnectOpt->Vals["ic_doc_server"].AsStringDef("localhost");
    ConnOptNode->AV["ic_doc_port"]   = IntToStr(ConnectOpt->Vals["ic_doc_port"].AsIntDef(5100));
    ConnOptNode->AV["ic_sv_server"]  = ConnectOpt->Vals["ic_sv_server"].AsStringDef("localhost");
    ConnOptNode->AV["ic_sv_port"]    = IntToStr(ConnectOpt->Vals["ic_sv_port"].AsIntDef(5100));
    if (!ConnOptNode->AV["ic_server"].Trim().Length())
     ConnOptNode->AV["ic_server"] = "localhost";
    if (!ConnOptNode->AV["ic_port"].Trim().Length())
     ConnOptNode->AV["ic_port"] = "5100";
    if (!ConnOptNode->AV["ic_doc_server"].Trim().Length())
     ConnOptNode->AV["ic_doc_server"] = "localhost";
    if (!ConnOptNode->AV["ic_doc_port"].Trim().Length())
     ConnOptNode->AV["ic_doc_port"] = "5100";
    if (!ConnOptNode->AV["ic_sv_server"].Trim().Length())
     ConnOptNode->AV["ic_sv_server"] = "localhost";
    if (!ConnOptNode->AV["ic_sv_port"].Trim().Length())
     ConnOptNode->AV["ic_sv_port"] = "5100";
    RC = ConnOptNode->AsXML;
   }
  __finally
   {
    delete ConnOptNode;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::SetHostParam()
 {
  try
   {
    Application->ProcessMessages();
    FClients->SetHostParam(GetHostParamStr());
   }
  __finally
   {
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FInsPriv(TkabProgressType AStage, TTagNode * ADataDefNode, TkabCustomDataSource * ASource,
  __int64 ARecCode, TkabProgress AProgress)
 {
  bool RC = false;
  try
   {
    if (AStage == TkabProgressType::InitStage)
     {
      FCur   = 0;
      FCount = ARecCode;
      RC     = true;
     }
    else if (AStage == TkabProgressType::StageInc)
     {
      FCur++ ;
      AProgress(TkabProgressType::StageInc, 100, IntToStr(FCur) + "/" + IntToStr(FCount));
      Sleep(10);
      Application->ProcessMessages();
     }
    else if (AStage == TkabProgressType::StageComplite)
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::GetRTData(TTagNode * ANode, UnicodeString AId)
 {
  bool RC = false;
  try
   {
    if (ANode->GetTagByUID(AId))
     RC = ANode->GetTagByUID(AId)->AV["v"].ToIntDef(0);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::Login()
 {
  bool RC = false;
  TDBSelectUnit * SelDlg = NULL;
  TTagNode * LRC = new TTagNode;
  try
   {
    Application->ProcessMessages();
    int i = 3;
    bool connected = false;
    while ((i > 0) && !connected)
     {
      try
       {
        Application->ProcessMessages();
        // try
        // {
        LRC->AsXML = FClients->Admin->LoginUser("");
        // }
        // catch (EAbort & E)
        // {
        // _MSG_ERRA(E.Message, "Ошибка входа в систему");
        // }
        Application->ProcessMessages();
        if (LRC->CmpName("nouser"))
         {
          FClients->SetConnectParam("", "");
          throw System::Sysutils::Exception("Ошибка входа в систему.");
         }
        else if (LRC->CmpName("user_exist"))
         {
          UnicodeString FQueMSG =
            "Пользователь с таким логином уже авторизован в программе.\n\nПроизвести повторный вход?\n\nРанее вошедший пользователь будет отключен от программы.";
          if (_MSG_QUEA(FQueMSG, "Пользователь авторизован") == ID_YES)
           LRC->AsXML = FClients->Admin->LoginUser("exit");
          else
           {
            FClients->SetConnectParam("", "");
            throw System::Sysutils::Exception("Ошибка входа в систему. Пользователь уже авторизован в программе.");
           }
         }
        else if (LRC->CmpName("access_denied"))
         {
          FClients->SetConnectParam("", "");
          throw System::Sysutils::Exception("Ошибка входа в систему. Доступ запрещен.");
         }
        else if (LRC->CmpName("rt"))
         {
          if (LRC->Count > 1)
           {
            Application->ProcessMessages();
            SelDlg = new TDBSelectUnit(this, LRC);
            SelDlg->ShowModal();
            if (SelDlg->ModalResult == mrOk)
             LRC->AsXML = FClients->Admin->LoginUser(SelDlg->DBId);
            else
             throw EAbort("Операция отменена пользователем.");
            Application->ProcessMessages();
           }
          if (LRC->Count == 1)
           {
            Application->ProcessMessages();

            Application->ProcessMessages();
            ConnectOpt->Vals["l"] = FClients->dsICRestCon->UserName;
            if (ConnectOpt->Vals["sp"].AsBoolDef(false))
             ConnectOpt->Vals["p"] = FClients->dsICRestCon->Password;
            Application->ProcessMessages();
            // LRC->AsXML;
            Application->ProcessMessages();
            FUserRT["IC"]              = GetRTData(LRC, "003B"); // Доступ к &quot;УИ-ЛПУ&quot;' uid='003B'
            FUserRT["ICReg"]           = GetRTData(LRC, "002A"); // Регистратура' uid='002A'
            FUserRT["ICRegUList"]      = GetRTData(LRC, "0033"); // Доступ к списку пациентов' uid='0033'
            FRegComp->UnitListEnabled  = FUserRT["ICRegUList"];
            FUserRT["ICRegUListEdit"]  = GetRTData(LRC, "0036"); // Редактирование' uid='0036'
            FRegComp->UnitListEditable = FUserRT["ICRegUListEdit"];
            FUserRT["ICRegCList"]      = GetRTData(LRC, "0037"); // Доступ к справочникам' uid='0037'
            FRegComp->ClassEnabled     = FUserRT["ICRegCList"];
            FUserRT["ICRegCListEdit"]  = GetRTData(LRC, "0038"); // Редактирование' uid='0038'
            FRegComp->ClassEditable    = FUserRT["ICRegCListEdit"];
            FUserRT["ICCard"]          = GetRTData(LRC, "002B"); // Доступ к имм. карте' uid='002B'
            FRegComp->OpenCardEnabled  = FUserRT["ICCard"];
            FUserRT["ICCardEdit"]      = GetRTData(LRC, "0039"); // Редактирование' uid='0039'
            // FICCardComp->CardEditable  = FUserRT["ICCardEdit"];
            FUserRT["ICPlan"]          = GetRTData(LRC, "002C"); // Доступ к списку планов' uid='002C'
            FUserRT["ICPlanPlaning"]   = GetRTData(LRC, "003A"); // Выполнение планирования' uid='003A'
            FICPlanComp->PlanEditable  = FUserRT["ICPlanPlaning"];
            FUserRT["Doc"]             = GetRTData(LRC, "003C"); // Доступ к &quot;УИ-Документооборот&quot;' uid='003C'
            FUserRT["DocSpec"]         = GetRTData(LRC, "002E");
            // Доступ к списку &quot;Описания документов&quot;' uid='002E'
            FDocComp->SpecEnabled     = FUserRT["DocSpec"];
            FUserRT["DocSpecEdit"]    = GetRTData(LRC, "003D"); // Редактирование' uid='003D'
            FDocComp->SpecEditable    = FUserRT["DocSpecEdit"];
            FUserRT["DocDoc"]         = GetRTData(LRC, "002D"); // Доступ к списку &quot;Документы&quot;' uid='002D'
            FDocComp->DocEnabled      = FUserRT["DocDoc"];
            FUserRT["DocDocEdit"]     = GetRTData(LRC, "003E"); // Редактирование' uid='003E'
            FDocComp->DocEditable     = FUserRT["DocDocEdit"];
            FDocComp->DocHandEditable = FUserRT["DocDocEdit"];
            FUserRT["DocFilter"]      = GetRTData(LRC, "002F"); // Доступ к списку &quot;Фильтры&quot;' uid='002F'
            FDocComp->FilterEnabled   = FUserRT["DocFilter"];
            FUserRT["DocFilterEdit"]  = GetRTData(LRC, "003F"); // Редактирование' uid='003F'
            FDocComp->FilterEditable  = FUserRT["DocFilterEdit"];
            FUserRT["VS"]             = GetRTData(LRC, "0029"); // <binary name='Склад МИБП' uid='0029'/>
            connected                 = true;
            RC                        = connected;
            Application->ProcessMessages();
           }
          else
           throw System::Sysutils::Exception("Ошибка входа в систему.");
         }
        else
         throw EAbort("Ошибка входа в систему"); // System::Sysutils::Exception("Ошибка входа в систему.");
       }
      catch (TDSRestProtocolException & E)
       {
        if (E.Status == 403 && E.Message.Pos("Forbidden"))
         {
          _MSG_ERRA("Доступ запрещен. Логин или пароль некорректны.", "Ошибка");
          FClients->SetConnectParam("", "");
         }
        else
         {
          _MSG_ERRA(E.Message, "Ошибка входа в систему");
          if (!ConnectOptDlg->Execute())
           i = 0;
         }
       }
      catch (EAbort & E)
       {
        // _MSG_ERRA("Операция отменена пользователем", "Ошибка входа в систему");
        i = 0;
       }
      catch (System::Sysutils::Exception & E)
       {
        if (E.ClassNameIs("EAbort"))
         i = 0;
        else
         {
          _MSG_ERRA(E.Message, "Ошибка входа в систему");
          if (!ConnectOptDlg->Execute())
           i = 0;
         }
       }
      i-- ;
     }
    if (!connected)
     Application->Terminate();
   }
  __finally
   {
    if (SelDlg)
     delete SelDlg;
    Application->ProcessMessages();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FGetUserRt(UnicodeString AId)
 {
  bool RC = false;
  try
   {
    TStrBoolMap::iterator FRC = FUserRT.find(AId);
    if (FRC != FUserRT.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::ConnectOptDlgAfterComponentCreate(TTagNode * ANode, TComponent * component)
 {
  //
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::CliOptDlgAfterComponentCreate(TTagNode * ANode, TComponent * component)
 {
  /*
   if (ANode->CmpAV("uid","002E"))
   {
   TDocOrgEditForm *Dlg = NULL;
   try
   {
   try
   {
   if (DM->ICSDocEx->DM->trOrg->Active) DM->ICSDocEx->DM->trOrg->Commit();
   DM->ICSDocEx->DM->trOrg->StartTransaction();
   if (!DM->ICSDocEx->DM->quOrg->Active) DM->ICSDocEx->DM->quOrg->CloseOpen(true);
   DM->ICSDocEx->DM->quOrg->Filtered = false;
   DM->ICSDocEx->DM->quOrg->Filter = "SUBORD=2";
   DM->ICSDocEx->DM->quOrg->Filtered = true;
   DM->ICSDocEx->DM->qtExecDoc("Select Count(*) as RCOUNT From SUBORDORG where SUBORD=2", false);
   if (!DM->ICSDocEx->DM->quFreeDoc->FN("RCOUNT")->AsInteger)
   {
   DM->ICSDocEx->DM->quOrg->Insert();
   DM->ICSDocEx->DM->quOrg->FN("CODE")->AsString      = NewGUID();
   DM->ICSDocEx->DM->quOrg->FN("NAME")->AsString      = "Наименование организации";
   DM->ICSDocEx->DM->quOrg->FN("FULLNAME")->AsString  = "Полное наименование организации";
   DM->ICSDocEx->DM->quOrg->FN("ADDR")->AsString      = "Адрес организации";
   DM->ICSDocEx->DM->quOrg->FN("SUBORD")->AsInteger  = 2;
   DM->ICSDocEx->DM->quOrg->FN("OKPO")->AsString     = "0";
   DM->ICSDocEx->DM->quOrg->FN("SOATO")->AsString    = "0";
   DM->ICSDocEx->DM->quOrg->FN("OKATO")->AsString     = "0";
   DM->ICSDocEx->DM->quOrg->FN("OKVED")->AsString    = "0";
   DM->ICSDocEx->DM->quOrg->FN("EMAIL")->AsString     = "";
   DM->ICSDocEx->DM->quOrg->FN("PHONE")->AsString     = "";
   DM->ICSDocEx->DM->quOrg->FN("OWNERFIO")->AsString     = "";

   DM->ICSDocEx->DM->quOrg->Post();
   }
   ICSDocEx->DM->quOrg->Edit();
   throw Exception("TDocOrgEditForm is not ported to XE6");
   }
   catch (...)
   {
   _MSG_ERRA("Ошибка модификации данных","Ошибка");
   ICSDocEx->DM->trOrg->Rollback();
   }
   }
   __finally
   {
   ICSDocEx->DM->quOrg->EnableControls();
   }
   }
   else if (ANode->CmpAV("uid","003E"))
   { // Планирование вакцинаций
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "0044";
   DM->RegComp->ShowClasses(ctList,"1",-1);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   else if (ANode->CmpAV("uid","003F"))
   { // Схемы применения вакцин
   #ifdef IMM_PLAN_VAC_SCH
   #endif
   }
   else if (ANode->CmpAV("uid","0040"))
   { // Схемы применения проб
   #ifdef IMM_PLAN_PROB_SCH
   #endif
   }
   else if (ANode->CmpAV("uid","0041"))
   { // Список условий для схем
   #ifdef IMM_PLAN_SETTING
   #endif
   }
   else if (ANode->CmpAV("uid","0042"))
   { // Список пауз
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "00A6";
   DM->RegComp->ShowClasses(ctList,"2",-1);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   else if (ANode->CmpAV("uid","0043"))
   { // Установка приоритетов вакцинаций
   #ifndef IMM_DOCUMENT
   TPrivPriorForm *Dlg = NULL;
   try
   {
   Dlg = new TPrivPriorForm(this);
   Dlg->ShowModal();
   }
   __finally
   {
   if (Dlg) delete Dlg;
   }
   #endif
   }
   else if (ANode->CmpAV("uid","0044"))
   { // Предварительные пробы
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "0059";
   DM->RegComp->ShowClasses(ctList,"3",0);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::AppOptDlgAfterComponentCreate(TTagNode * ANode, TComponent * component)
 {
  /*
   if (ANode->CmpAV("uid","002E"))
   {
   TDocOrgEditForm *Dlg = NULL;
   try
   {
   try
   {
   if (DM->ICSDocEx->DM->trOrg->Active) DM->ICSDocEx->DM->trOrg->Commit();
   DM->ICSDocEx->DM->trOrg->StartTransaction();
   if (!DM->ICSDocEx->DM->quOrg->Active) DM->ICSDocEx->DM->quOrg->CloseOpen(true);
   DM->ICSDocEx->DM->quOrg->Filtered = false;
   DM->ICSDocEx->DM->quOrg->Filter = "SUBORD=2";
   DM->ICSDocEx->DM->quOrg->Filtered = true;
   DM->ICSDocEx->DM->qtExecDoc("Select Count(*) as RCOUNT From SUBORDORG where SUBORD=2", false);
   if (!DM->ICSDocEx->DM->quFreeDoc->FN("RCOUNT")->AsInteger)
   {
   DM->ICSDocEx->DM->quOrg->Insert();
   DM->ICSDocEx->DM->quOrg->FN("CODE")->AsString      = NewGUID();
   DM->ICSDocEx->DM->quOrg->FN("NAME")->AsString      = "Наименование организации";
   DM->ICSDocEx->DM->quOrg->FN("FULLNAME")->AsString  = "Полное наименование организации";
   DM->ICSDocEx->DM->quOrg->FN("ADDR")->AsString      = "Адрес организации";
   DM->ICSDocEx->DM->quOrg->FN("SUBORD")->AsInteger  = 2;
   DM->ICSDocEx->DM->quOrg->FN("OKPO")->AsString     = "0";
   DM->ICSDocEx->DM->quOrg->FN("SOATO")->AsString    = "0";
   DM->ICSDocEx->DM->quOrg->FN("OKATO")->AsString     = "0";
   DM->ICSDocEx->DM->quOrg->FN("OKVED")->AsString    = "0";
   DM->ICSDocEx->DM->quOrg->FN("EMAIL")->AsString     = "";
   DM->ICSDocEx->DM->quOrg->FN("PHONE")->AsString     = "";
   DM->ICSDocEx->DM->quOrg->FN("OWNERFIO")->AsString     = "";

   DM->ICSDocEx->DM->quOrg->Post();
   }
   ICSDocEx->DM->quOrg->Edit();
   throw Exception("TDocOrgEditForm is not ported to XE6");
   }
   catch (...)
   {
   _MSG_ERRA("Ошибка модификации данных","Ошибка");
   ICSDocEx->DM->trOrg->Rollback();
   }
   }
   __finally
   {
   ICSDocEx->DM->quOrg->EnableControls();
   }
   }
   else if (ANode->CmpAV("uid","003E"))
   { // Планирование вакцинаций
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "0044";
   DM->RegComp->ShowClasses(ctList,"1",-1);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   else if (ANode->CmpAV("uid","003F"))
   { // Схемы применения вакцин
   #ifdef IMM_PLAN_VAC_SCH
   #endif
   }
   else if (ANode->CmpAV("uid","0040"))
   { // Схемы применения проб
   #ifdef IMM_PLAN_PROB_SCH
   #endif
   }
   else if (ANode->CmpAV("uid","0041"))
   { // Список условий для схем
   #ifdef IMM_PLAN_SETTING
   #endif
   }
   else if (ANode->CmpAV("uid","0042"))
   { // Список пауз
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "00A6";
   DM->RegComp->ShowClasses(ctList,"2",-1);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   else if (ANode->CmpAV("uid","0043"))
   { // Установка приоритетов вакцинаций
   #ifndef IMM_DOCUMENT
   TPrivPriorForm *Dlg = NULL;
   try
   {
   Dlg = new TPrivPriorForm(this);
   Dlg->ShowModal();
   }
   __finally
   {
   if (Dlg) delete Dlg;
   }
   #endif
   }
   else if (ANode->CmpAV("uid","0044"))
   { // Предварительные пробы
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "0059";
   DM->RegComp->ShowClasses(ctList,"3",0);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::ConnectOptDlgExternEditClick(TTagNode * ANode, TAOVarMap & vars)
 {
  //
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::CliOptDlgExternEditClick(TTagNode * ANode, TAOVarMap & vars)
 {
  if (ANode->CmpAV("uid", "0019")) // Темы
   {
   }
  else if (ANode->CmpAV("uid", "0022")) // адрес по умолчанию
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::AppOptDlgExternEditClick(TTagNode * ANode, TAOVarMap & vars)
 {
  if (ANode->CmpAV("uid", "0001")) // Своё ЛПУ
   {
    TMainLPUEditForm * Dlg = new TMainLPUEditForm(this, FdsGetValById);
    try
     {
      Dlg->LPUCode = FICPlanComp->LPUCode;
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       {
        if (Dlg->LPUCode != FICPlanComp->LPUCode)
         { // меняем своё ЛПУ
          FClients->App->SetMainLPU(Dlg->LPUCode);
          FClients->Doc->SetMainLPU(Dlg->LPUCode);
          FClients->VS->SetMainLPU(Dlg->LPUCode);
          // FICCardComp->LPUCode    = Dlg->LPUCode;
          FICPlanComp->LPUCode    = Dlg->LPUCode;
          FDocComp->MainOwnerCode = FClients->Doc->MainOwnerCode();
         }
       }
     }
    __finally
     {
      delete Dlg;
     }
   }
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #     Загрузка описаний настроек                                             #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TDM::ConnectOptLoadXML(TTagNode * AOptNode)
 {
  try
   {
    AOptNode->Encoding = "utf-8";
    AOptNode->AsXML    =
      "<AppOptions>" " <passport title='Настройки соединения' GUI='ICS_CON_OPT' autor='KAB' version='1' release='0' timestamp='20161021000000'/>"
      " <content>" "  <group name='grCommon' uid='0000' caption='Общее'>"
      "   <group name='grICSrv' uid='0011' caption='&quot;УИ&quot;'>" "    <text name='ic_server' uid='0001' default='localhost' caption='Сервер'/>"
      "    <digit name='ic_port' uid='0002' default='5100' caption='Порт' min='0' max='99999'/>" "   </group>"
      "   <group name='grICDocSrv' uid='0012' caption='&quot;УИ-Документооборот&quot;'>" "    <text name='ic_doc_server' uid='0003' default='localhost' caption='Сервер'/>"
      "    <digit name='ic_doc_port' uid='0004' default='5100' caption='Порт' min='0' max='99999'/>" "   </group>"
      "   <group name='grICSVSrv' uid='0013' caption='&quot;УИ-Склад МИБП&quot;'>" "    <text name='ic_sv_server' uid='0005' default='localhost' caption='Сервер'/>"
      "    <digit name='ic_sv_port' uid='0006' default='5100' caption='Порт' min='0' max='99999'/>" "   </group>"
      "  </group>" "  <servicegroup name='ExtVars' uid='000F'>" "   <text name='l' uid='0014' caption='l'/>"
      "   <text name='p' uid='0015' caption='p'/>" "   <binary name='sp' uid='0016' caption='sp'/>" "  </servicegroup>"
      " </content>" "</AppOptions>";
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::CliOptLoadXML(TTagNode * AOptNode)
 {
  AOptNode->AsXML = FClients->App->GetClassZIPXML("ICS_CLI_OPT");
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::AppOptLoadXML(TTagNode * AOptNode)
 {
  AOptNode->AsXML = FClients->App->GetClassZIPXML("ICS_APP_OPT");
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #     Сохранение настроек, если не задан локальный файл                      #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TDM::CliOptXMLDataWriteVal(TObject * Sender)
 {
  FClients->App->SetCliOptData(FCliOptValNode->AsXML);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::AppOptXMLDataWriteVal(TObject * Sender)
 {
  FClients->App->SetAppOptData(FAppOptValNode->AsXML);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #     Загрузка значений настроек                                             #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadCliOptData()
 {
  FCliOptValNode->AsXML     = FClients->App->GetCliOptData();
  CliOptXMLData->ValuesNode = FCliOptValNode;
  CliOpt->LoadXML();
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadAppOptData()
 {
  FAppOptValNode->AsXML     = FClients->App->GetAppOptData();
  AppOptXMLData->ValuesNode = FAppOptValNode;
  AppOpt->LoadXML();
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::ShowPrePostTestList(TObject * Sender)
 {
  RegComp->ShowClasses(ctList, "1", "006C");
  // RegComp->ClassShowExpanded = "0059";
  // RegComp->ShowClasses(ctList, "1", 1);
  // RegComp->ClassShowExpanded = "";
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::OnGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC)
 {
  return FdsGetValById(AId, ARecId, RC);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
 {
  ARC = FXMLList->GetXML(AId);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::OnExtBtnClick(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource,
  bool isClicked, TObject * ACtrlList, int ABtnIdx)
 {
  return FRegCompExtEdit(ItTag, Src, ASource, isClicked, ACtrlList, ABtnIdx);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::PatGrChData(__int64 AUCode, TdsRegTemplateData * ANewPatData)
 {
  bool RC = false;
  try
   {
    RC = Clients->App->PatGrChData(AUCode, ANewPatData->ToJSONString());
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::GetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC)
 {
  return FdsGetValById(AId, ARecId, RC);
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FAppExept(System::TObject * Sender, System::Sysutils::Exception * E)
 {
  if (E->ClassNameIs("TDSRestProtocolException"))
   {
    TDSRestProtocolException * Ex = ((TDSRestProtocolException *)E);
    if (Ex->Status == 403 && Ex->ResponseText.Pos("SessionExpired"))
     {
      _MSG_ERRA("Ваша сессия завершена. Для повторного входа презапустите программу.", "Ошибка");
     }
    else if (Ex->Status == 403 && Ex->ResponseText.Pos("Forbidden"))
     {
      _MSG_ERRA("Доступ запрещен. Логин или пароль некорректны.", "Ошибка");
     }
    else
     _MSG_ERRA(E->Message, "Ошибка");
   }
  else
   _MSG_ERRA(E->Message, "Ошибка");
 }

// ---------------------------------------------------------------------------
