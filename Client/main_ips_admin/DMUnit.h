﻿//---------------------------------------------------------------------------

#ifndef DMUnitH
#define DMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Dialogs.hpp>
#include "AppOptBaseProv.h"
#include "AppOptDialog.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsForm.hpp"
//---------------------------------------------------------------------------
#include "pgmsetting.h"
#include "dsConnectUnit.h"
//#include "dsEIDataUnit.h"

#include "dsRegClient.h"

//#include "dsICCardClient.h"
#include "dsICPlanClient.h"
#include "dsSchEditClient.h"

//#include "dsICStoreClient.h"
#include "dsDocClient.h"

#include "dsDocNomSupport.h"
#include "dsICNomSupport.h"
#include "XMLContainer.h"
#include "JSONUtils.h"
//#include <REST.Exception.hpp>
/*
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>


#include "AppOptBaseProv.h"
#include "AppOptDialog.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsForm.hpp"
//---------------------------------------------------------------------------
#include "ServerClientClasses.h"


#include <REST.Authenticator.Basic.hpp>
#include <REST.Authenticator.Simple.hpp>
#include <REST.Client.hpp>
*/
class TDM : public TDataModule
{
__published:	// IDE-managed Components
 TAppOptDialog *CliOptDlg;
 TAppOptions *CliOpt;
 TAppOptXMLProv *CliOptXMLData;
 TAppOptDialog *AppOptDlg;
 TAppOptions *AppOpt;
 TAppOptXMLProv *AppOptXMLData;
 TcxEditStyleController *EditStyle;
 TcxDefaultEditStyleController *DefEditStyle;
 TdxSkinController *Skin;
 TOpenDialog *OpenDlg;
 TSaveDialog *SaveDlg;
 TAppOptions *ConnectOpt;
 TAppOptXMLProv *ConnectOptXMLData;
 TAppOptDialog *ConnectOptDlg;
 void __fastcall DataModuleDestroy(TObject *Sender);
 void __fastcall CliOptDlgAfterComponentCreate(TTagNode *ANode, TComponent *component);
 void __fastcall CliOptLoadXML(TTagNode *AOptNode);
 void __fastcall AppOptXMLDataWriteVal(TObject *Sender);
 void __fastcall AppOptLoadXML(TTagNode *AOptNode);
 void __fastcall AppOptDlgAfterComponentCreate(TTagNode *ANode, TComponent *component);
 void __fastcall AppOptDlgExternEditClick(TTagNode *ANode, TAOVarMap &vars);
 void __fastcall CliOptXMLDataWriteVal(TObject *Sender);
 void __fastcall ConnectOptDlgAfterComponentCreate(TTagNode *ANode, TComponent *component);
 void __fastcall ConnectOptDlgExternEditClick(TTagNode *ANode, TAOVarMap &vars);
 void __fastcall CliOptDlgExternEditClick(TTagNode *ANode, TAOVarMap &vars);
 void __fastcall ConnectOptLoadXML(TTagNode *AOptNode);


private:
  typedef map<UnicodeString, TJSONObject*> TJSONMap;
  TConnectDM       *FClients;
  TdsRegClient     *FRegComp;
  TdsRegTemplateData * FTemplateData;

//  TdsICCardClient  *FICCardComp;
  TdsICPlanClient  *FICPlanComp;
  TdsSchEditClient *FICSchComp;
//  TdsEIData        *EIDataComp;
  TColor           FReqColor;

//  TdsICStoreClient *FICStoreComp;
  TdsDocClient     *FDocComp;
  TTagNode         *FExtFilter;

  TdsBaseNomSupport *FICNom;
  TdsBaseNomSupport *FDocNom;
  TJSONMap          FDataRequest;

  TAxeXMLContainer* FXMLList;
  TAxeXMLContainer* __fastcall FGetXMLList();
  bool __fastcall FExtGetXML(TTagNode* ANode, UnicodeString &Src);

  TTagNode *FRegDef;
  TTagNode *FAppOptValNode;
  TTagNode *FCliOptValNode;

  UnicodeString FDataPath;
  UnicodeString FXMLPath;
  int FCur;
  int FCount;
  typedef map<UnicodeString, bool> TStrBoolMap;
  TStrBoolMap  FUserRT;


  TICSNomenklator* __fastcall FGetNom();

  __property TICSNomenklator*  Nom                = {read=FGetNom};

  void __fastcall UploadData(UnicodeString AData, TkabProgress AProgress = NULL);
  void __fastcall ShowPrePostTestList(TObject *Sender);

  bool __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  void __fastcall LoadCliOptData();
  void __fastcall LoadAppOptData();
  void __fastcall FdsGetPlanPrintFormats(TJSONObject* &AData);
  UnicodeString __fastcall FdsPlanPrint(__int64 ACode);
  bool __fastcall FdsGetClassXML(System::UnicodeString ARef, System::UnicodeString &RC);
  bool __fastcall FdsVSGetClassXML(System::UnicodeString ARef, System::UnicodeString &ARC);
  __int64 __fastcall FdsGetCount(System::UnicodeString AId, UnicodeString AFilterParam);
  void __fastcall FdsGetExtCount(UnicodeString &ARC);
  __int64 __fastcall FdsVSGetCount(System::UnicodeString AId, UnicodeString AFilterParam);
  bool __fastcall FdsGetListById(System::UnicodeString AId, TJSONObject *& ARC);
  bool __fastcall FdsGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& RC);
  bool __fastcall FdsVSGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& ARC);
  bool __fastcall FdsGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall FdsGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall FdsVSGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC);
  bool __fastcall FdsVSGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC);
  bool __fastcall FdsDocGetQList(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC);
  bool __fastcall FdsFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& RC);
  bool __fastcall FdsVSFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& ARC);
  bool __fastcall FdsInsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsVSInsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsEditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsVSEditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString &RC);
  bool __fastcall FdsVSDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString &ARC);
  bool __fastcall FdsGetF63(System::UnicodeString ARef, System::UnicodeString &RC);
  bool __fastcall FdsGetShortF63(System::UnicodeString ARef, System::UnicodeString & ARC);
  UnicodeString __fastcall FdsGetVacDefDoze(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode);
  bool __fastcall FdsVSGetMIBPForm(System::UnicodeString &RC);
  bool __fastcall FdsGetPlanOpts(UnicodeString &AOptDef);
  void __fastcall FdsSetPlanOpts(UnicodeString AOptDef);

  bool __fastcall FdsDocGetClassXML(System::UnicodeString ARef, System::UnicodeString &RC);
  bool __fastcall FdsDocGetClassZIPXML(System::UnicodeString ARef, System::UnicodeString &RC);
  __int64 __fastcall FdsDocGetCount(System::UnicodeString AId, UnicodeString AFilterParam);
  bool __fastcall FdsDocGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& RC);
  bool __fastcall FdsDocGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall FdsDocGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall FdsDocFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& RC);
  UnicodeString __fastcall FdsDocImportData(UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID);
  bool __fastcall FdsDocInsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsDocEditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsDocDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString &RC);
  void __fastcall FdsDocGetText(System::UnicodeString ATabId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString &AData, System::UnicodeString &ARC);
  void __fastcall FdsDocSetText(System::UnicodeString ATabId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AData, System::UnicodeString &ARC);
  bool __fastcall FdsDocGetSpec(System::UnicodeString AId, int  &ASpecType, System::UnicodeString &ARC);
  bool __fastcall FdsDocGetDocById(System::UnicodeString AId, System::UnicodeString &ARC);
  bool __fastcall FdsDocGetDocPreview(System::UnicodeString AId, System::UnicodeString &ARC, int &Al, int &At, int &Ar, int &Ab, double &Aps, int &Ao);
  bool __fastcall FdsDocCheckByPeriod(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI, System::UnicodeString &ARC);
  bool __fastcall FdsDocDocListClearByDate(TDate ADate);
  int  __fastcall FdsDocGetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString &ARCMsg);
  bool __fastcall FdsDocIsMainOwnerCode(UnicodeString ASpecOwner);
  bool __fastcall FdsDocCopySpec(UnicodeString AName, UnicodeString &ANewGUI, System::UnicodeString &ARC);
  int  __fastcall FdsDocCheckSpecExists(UnicodeString AName, UnicodeString &AGUI, bool AIsEdit);
  bool __fastcall FdsDocCheckDocCreate(UnicodeString ASpecGUI, int &ACreateType, int ADefCreateType, int &AGetCreateType, int &AGetPeriod);
  bool __fastcall FdsDocCheckExistDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, int APerType, TDate APeriodDateFr, TDate FPeriodDateTo, System::UnicodeString &ADocGUI);
  UnicodeString __fastcall FdsDocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilter, UnicodeString &AFilterName, UnicodeString &AFilterGUI);
  bool __fastcall FdsDocDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate FPeriodDateTo, System::UnicodeString &ADocGUI);
  bool __fastcall FdsDocStartDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo, bool ANotSave);
  UnicodeString __fastcall FdsDocDocCreateProgress(System::UnicodeString &ADocGUI);
  void __fastcall FdsDocSaveDoc(UnicodeString ADocGUI);

  bool __fastcall FdsDocCallDefProc(System::UnicodeString AProcName, TTagNode* AIENode, bool ANeedKonkr, System::UnicodeString &AText);
  bool __fastcall FdsDocCallDocDefProc(System::UnicodeString AProcName, TTagNode* AIENode, bool ANeedKonkr, System::UnicodeString &AText);

  void __fastcall FOpenCard(__int64 Code, UnicodeString UnitStr);
  void __fastcall FdsOnPrintF63(__int64 Code, UnicodeString UnitStr);

  bool __fastcall FGetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData);
  void __fastcall FPreviewDoc(UnicodeString ATxt_Id, bool AByCode);
  void __fastcall FHTMLPreviewDoc(UnicodeString ATxt_Id);

  void __fastcall FdsShowVacSch();
  void __fastcall FdsShowTestSch();
  void __fastcall FdsShowCheckSch();

  UnicodeString __fastcall FGetUnitPlanOptions(__int64 ACode);
  void          __fastcall FSetUnitPlanOptions(__int64 APatCode, System::UnicodeString AData);
  UnicodeString __fastcall FGetPrivVar(__int64 ACode);
  UnicodeString __fastcall FGetOtvodInf(__int64 ACode);
  UnicodeString __fastcall FPatCreatePlan(__int64 ACode);
//  UnicodeString __fastcall FdsPlanCreate(TJSONObject *AVal, UnicodeString &ARecId);
  UnicodeString __fastcall FdsDeletePlan(UnicodeString ARecId);
  UnicodeString __fastcall FdsDeletePlanItem(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall FdsStartCreatePlan(TJSONObject *AVal);
  UnicodeString __fastcall FdsStopCreatePlan(void);
  UnicodeString __fastcall FdsCheckCreatePlanProgress(UnicodeString &ARecId);
  bool          __fastcall FRegCompExtEdit(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource, bool isClicked, TObject* ACtrlList, int ABtnIdx);
  bool          __fastcall FRegCompCtrlDataChange(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource, bool isAppend, TObject * ACtrlList, int ABtnIdx);
  UnicodeString __fastcall FdsOnExtFilterSet(TTagNode *ADefNode, bool &CanContinue, UnicodeString AFilterGUI);
//  void          __fastcall FdsOnGetQuickFilterList(TStringList *AList);
  void          __fastcall FdsOnSetDefValues(__int64 ARecCode, TTagNode *ANode, TdsRegEDContainer *ControlList, bool &Valid);
  void          __fastcall FdsOnExtValidate(__int64 ARecCode, TTagNode *ANode, TdsRegEDContainer *ControlList, bool &Valid);

//  void          __fastcall FdsOnGetReportList(TStringList *AList);
  void          __fastcall FdsOnPrintReportClick(UnicodeString ARepGUI, __int64 AUCode, UnicodeString AFilter, TkabProgress AProgressProc = NULL);


//  UnicodeString __fastcall FGetAddrCode(UnicodeString AVal, TdsRegEDContainer *ACtrList, bool ATemplate);
  UnicodeString __fastcall GetOrg(System::UnicodeString &AOrg);
  UnicodeString __fastcall OrgCodeToStr(UnicodeString AOrgCode);
  UnicodeString __fastcall GetMKB(System::UnicodeString &AAddr);
  UnicodeString __fastcall MKBCodeToStr(UnicodeString AOrgCode);
//  UnicodeString __fastcall FGetOrgCode(UnicodeString AVal, TdsRegEDContainer *ACtrList, bool ATemplate);
  UnicodeString __fastcall FdsDocLoadDocPreview(System::UnicodeString &AMsg);
  UnicodeString __fastcall FGetPlanTemplateDef(int AType);
  bool __fastcall FOnSaveScheme(TTagNode *TagNode, TSchemeObjType AType, TkabProgress AProgress);
  UnicodeString __fastcall GetHostParamStr();
  bool __fastcall FInsPriv(TkabProgressType AStage, TTagNode *ADataDefNode, TkabCustomDataSource* ASource, __int64 ARecCode, TkabProgress AProgress);
  bool __fastcall FGetUserRt(UnicodeString AId);
  UnicodeString __fastcall UchCodeToStr(UnicodeString  AOrgCode);
  bool __fastcall GetRTData(TTagNode *ANode, UnicodeString AId);
  bool __fastcall FdsCanHandEdit(UnicodeString ASpecGUID);
  void __fastcall FAppExept(System::TObject* Sender, System::Sysutils::Exception* E);

public:
  UnicodeString __fastcall GetUnitPlanOptions(__int64 ACode);
  int __fastcall InfInPlan(TTagNode *AComRoot, TTagNode *APersRoot, UnicodeString AInf);
  UnicodeString __fastcall GetAddr(System::UnicodeString &AAddr, System::UnicodeString ADefAddr, int AParam);
  UnicodeString __fastcall AddrCodeToStr(UnicodeString AAddrCode);
  UnicodeString __fastcall AddrCodeToStrF(UnicodeString  AAddrCode, int AParam);
  bool __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall Connect(UnicodeString AUser, UnicodeString APaswd);
  void __fastcall SetHostParam();
  void __fastcall LoadRegDef();
  bool __fastcall InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  UnicodeString __fastcall GetVacDefDoze(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode);
  bool __fastcall GetListById(System::UnicodeString AId, TJSONObject *& ARC);

  bool __fastcall OnGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
//  FRegComp->OnGetValById     = FdsGetValById;
//  bool __fastcall FdsGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);

  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ARC);

  bool __fastcall OnExtBtnClick(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource, bool isClicked, TObject * ACtrlList, int ABtnIdx);
  bool __fastcall PatGrChData(__int64 AUCode, TdsRegTemplateData * ANewPatData);

  __property TTagNode *RegDef   = {read=FRegDef};

  __property TdsRegClient     *RegComp   = {read=FRegComp};
//  __property TdsICCardClient  *CardComp  = {read=FICCardComp};
  __property TdsICPlanClient  *PlanComp  = {read=FICPlanComp};
//  __property TdsICStoreClient *StoreComp = {read=FICStoreComp};
  __property TdsDocClient     *DocComp   = {read=FDocComp};
  __property TAxeXMLContainer *XMLList    = {read=FGetXMLList};

  __property UnicodeString DataPath   = {read=FDataPath};
  __property UnicodeString XMLPath    = {read=FXMLPath};
  __property TConnectDM*   Clients    = {read=FClients};

  __fastcall TDM(TComponent* Owner);
  bool __fastcall PreloadData();
  bool __fastcall Login();
  __property bool RT[UnicodeString AId] = {read=FGetUserRt};
};
//---------------------------------------------------------------------------
extern PACKAGE TDM *DM;
//---------------------------------------------------------------------------
#endif
