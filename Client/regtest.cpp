//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("Pkg\dsRegistry\Src\dsRegTemplateUnit.cpp", dsRegTemplateForm);
USEFORM("RegTest\LoginUnit.cpp", LoginForm);
USEFORM("RegTest\MainUnit.cpp", MainForm);
USEFORM("templatetest.cpp", Form1);
USEFORM("RegTest\dsProgress.cpp", dsProgressForm);
USEFORM("Pkg\dsRegistry\Src\dsRegTreeClass.cpp", dsRegTreeClassForm);
USEFORM("Pkg\dsRegistry\Src\dsUnitListClientUnit.cpp", dsUnitListClientForm);
USEFORM("RegTest\DMUnit.cpp", DM); /* TDataModule: File Type */
USEFORM("Pkg\dsRegistry\Src\dsClassifClientUnit.cpp", dsClassifClientForm);
USEFORM("Pkg\dsRegistry\Src\dsClassifEditClientUnit.cpp", dsClassEditClientForm);
USEFORM("Pkg\dsRegistry\Src\dsRegDMUnit.cpp", dsRegDM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
 try
 {
   Application->Initialize();
   Application->MainFormOnTaskBar = true;
   Application->CreateForm(__classid(TDM), &DM);
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->Run();
 }
 catch (Exception &exception)
 {
   Application->ShowException(&exception);
 }
 catch (...)
 {
   try
   {
    throw Exception("");
   }
   catch (Exception &exception)
   {
    Application->ShowException(&exception);
   }
 }
 return 0;
}
//---------------------------------------------------------------------------
