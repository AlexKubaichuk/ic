//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LoginUnit.h"
#include "DMUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TLoginForm *LoginForm;
//---------------------------------------------------------------------------
__fastcall TLoginForm::TLoginForm(TComponent* Owner)
 : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLoginForm::UserEDPropertiesChange(TObject *Sender)
{
  ApplyBtn->Enabled = UserED->Text.Trim().Length() && PassED->Text.Trim().Length();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TLoginForm::FGetUser()
{
  return UserED->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TLoginForm::FGetPass()
{
  return PassED->Text.Trim();
}
//---------------------------------------------------------------------------

