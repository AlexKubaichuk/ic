//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxGraphics.hpp"
#include "dxGDIPlusClasses.hpp"
#include "ICSAboutDialog.h"
//---------------------------------------------------------------------------
#include "DMUnit.h"
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include "cxCheckBox.hpp"
#include "cxPC.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "cxClasses.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
  TcxImageList *LMainIL32;
  TActionList *MainAL;
  TAction *actHelp;
  TAction *actAbout;
  TcxImageList *MainIL;
 TICSAboutDialog *AboutDlg;
 TAction *actClientSetting;
 TProgressBar *PrBar;
 TOpenDialog *OpenDlg;
 TFDConnection *IcConnection;
 TFDGUIxWaitCursor *FDGUIxWaitCursor;
 TFDTableAdapter *FDTableAdapter1;
 TcxPageControl *cxPageControl1;
 TcxTabSheet *FindTS;
 TcxTabSheet *KLADRTS;
 TLabel *Label2;
 TcxButtonEdit *KLADRDBBE;
 TLabel *Label1;
 TcxButtonEdit *DBPathBE;
 TButton *LoadKLADR;
 TcxTreeList *KLADRTL;
 TcxTreeListColumn *cxTreeListColumn1;
 TcxTreeListColumn *cxTreeListColumn4;
 TcxTreeList *AddrTL;
 TcxTreeListColumn *cxTreeList1Column1;
 TcxTreeListColumn *AddrTLColumn2;
 TcxTreeListColumn *cxTreeList1Column2;
 TcxTreeListColumn *AddrTLColumn1;
 TcxStyleRepository *cxStyleRepository1;
 TcxStyle *cxStyle1;
 TSaveDialog *SaveDlg;
 TcxTreeListColumn *AddrTLColumn3;
 TPanel *Panel1;
 TButton *CheckBtn;
 TButton *SaveBtn;
 TFDConnection *KLADRConect;
  void __fastcall FormShow(TObject *Sender);
  void __fastcall actHelpExecute(TObject *Sender);
  void __fastcall actAboutExecute(TObject *Sender);
 void __fastcall actClientSettingExecute(TObject *Sender);
 void __fastcall DBPathBEPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall CheckBtnClick(TObject *Sender);
 void __fastcall KLADRDBBEPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall LoadKLADRClick(TObject *Sender);
 void __fastcall cxTreeList1Column1PropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall SaveBtnClick(TObject *Sender);
 void __fastcall AddrTLEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);









protected:
  virtual void __fastcall WndProc(Messages::TMessage &Message);
private:	// User declarations
  int FC;
  bool FInProgress;
  TAnsiStrMap SUUsers;
  UnicodeString DefAddrCode;

  void __fastcall SetInProgress(bool AVal);

  void __fastcall UpdateMenuItems();
  bool __fastcall CheckConnect();
  bool __fastcall CanProgress();
  UnicodeString __fastcall GetAddr(System::UnicodeString AOldAddr, System::UnicodeString & AAddr, System::UnicodeString ADefAddr, int AParam);

  __property bool InProgress = {read=FInProgress, write=SetInProgress};
public:		// User declarations
  __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
