//---------------------------------------------------------------------------

#ifndef DMUnitH
#define DMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Dialogs.hpp>
#include "AppOptBaseProv.h"
#include "AppOptDialog.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsForm.hpp"
//---------------------------------------------------------------------------
#include "pgmsetting.h"
#include "dsConnectUnit.h"

#include "dsRegClient.h"

//#include "dsICPlanClient.h"
//#include "dsSchEditClient.h"

//#include "dsICStoreClient.h"
//#include "dsDocClient.h"

//#include "dsDocNomSupport.h"
//#include "dsICNomSupport.h"
#include "XMLContainer.h"
/*
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>


#include "AppOptBaseProv.h"
#include "AppOptDialog.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinsForm.hpp"
//---------------------------------------------------------------------------
#include "ServerClientClasses.h"


#include <REST.Authenticator.Basic.hpp>
#include <REST.Authenticator.Simple.hpp>
#include <REST.Client.hpp>
*/
class TDM : public TDataModule
{
__published:	// IDE-managed Components
 TAppOptDialog *ConnectOptDlg;
 TAppOptions *ConnectOpt;
 TAppOptXMLProv *ConnectOptXMLData;
 TAppOptDialog *SrvOptDlg;
 TcxEditStyleController *EditStyle;
 TcxDefaultEditStyleController *DefEditStyle;
 TdxSkinController *Skin;
 TOpenDialog *OpenDlg;
 TSaveDialog *SaveDlg;
 TAppOptions *SrvOpt;
 TAppOptXMLProv *SrvOptXMLData;
 void __fastcall DataModuleDestroy(TObject *Sender);
 void __fastcall ConnectOptDlgAfterComponentCreate(TTagNode *ANode, TComponent *component);
 void __fastcall ConnectOptLoadXML(TTagNode *AOptNode);
 void __fastcall SrvOptLoadXML(TTagNode *AOptNode);
private:

  TConnectDM       *FClients;
  TdsRegClient     *FRegComp;

//  TdsICPlanClient  *FICPlanComp;
//  TdsSchEditClient *FICSchComp;
//  TdsEIData        *EIDataComp;

//  TdsICStoreClient *FICStoreComp;
//  TdsDocClient     *FDocComp;
  TTagNode         *FExtFilter;

  TAxeXMLContainer* FXMLList;
  TAxeXMLContainer* __fastcall FGetXMLList();
  bool __fastcall FExtGetXML(TTagNode* ANode, UnicodeString &Src);

  TTagNode *FRegDef;
  TTagNode *FAppOptValNode;

  UnicodeString FDataPath;
  UnicodeString FXMLPath;
  int FCur;
  int FCount;
  TTagNode *UserRT;



  void __fastcall UploadData(UnicodeString AData, TkabProgress AProgress = NULL);

  void __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  void __fastcall LoadAppOptData();
  bool __fastcall FdsGetClassXML(System::UnicodeString ARef, System::UnicodeString &RC);
  __int64 __fastcall FdsGetCount(System::UnicodeString AId, UnicodeString AFilterParam);
  bool __fastcall FdsGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& RC);
  bool __fastcall FdsGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall FdsGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& RC);
  bool __fastcall FdsFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& RC);
  bool __fastcall FdsInsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsEditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString &ARecID, System::UnicodeString &ARC);
  bool __fastcall FdsDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString &RC);
  bool __fastcall FdsGetPlanOpts(UnicodeString &AOptDef);


  void __fastcall FOpenCard(__int64 Code, UnicodeString UnitStr);
  void __fastcall FdsOnPrintF63(__int64 Code, UnicodeString UnitStr);
  void __fastcall FdsOnShowPrePlan(__int64 Code, UnicodeString UnitStr);

  bool __fastcall FPlanOpenCard(__int64 Code);
  bool __fastcall FGetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData);
  void __fastcall FPreviewDoc(UnicodeString ATxt_Id, bool AByCode);
  void __fastcall FHTMLPreviewDoc(UnicodeString ATxt_Id);

  void __fastcall FdsShowVacSch();
  void __fastcall FdsShowTestSch();
  void __fastcall FdsShowCheckSch();

  bool          __fastcall FRegCompExtEdit(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource, bool isClicked, TObject* ACtrlList, int ABtnIdx);
  UnicodeString __fastcall FdsOnExtFilterSet(TTagNode *ADefNode, bool &CanContinue, UnicodeString AFilterGUI);
//  void          __fastcall FdsOnGetQuickFilterList(TStringList *AList);
  void          __fastcall FdsOnSetDefValues(__int64 ARecCode, TTagNode *ANode, TdsRegEDContainer *ControlList, bool &Valid);
  void          __fastcall FdsOnExtValidate(__int64 ARecCode, TTagNode *ANode, TdsRegEDContainer *ControlList, bool &Valid);

//  void          __fastcall FdsOnGetReportList(TStringList *AList);
  void          __fastcall FdsOnPrintReportClick(UnicodeString ARepGUI, __int64 AUCode, UnicodeString AFilter, TkabProgress AProgressProc = NULL);


  bool __fastcall LoadSrvOpt();
//  UnicodeString __fastcall FGetAddrCode(UnicodeString AVal, TdsRegEDContainer *ACtrList, bool ATemplate);
  UnicodeString __fastcall GetOrg(System::UnicodeString &AAddr);
  UnicodeString __fastcall OrgCodeToStr(UnicodeString AOrgCode);
//  UnicodeString __fastcall FGetOrgCode(UnicodeString AVal, TdsRegEDContainer *ACtrList, bool ATemplate);
  UnicodeString __fastcall GetHostParamStr();
  bool __fastcall FInsPriv(TkabProgressType AStage, TTagNode *ADataDefNode, TkabCustomDataSource* ASource, __int64 ARecCode, TkabProgress AProgress);
public:
  bool __fastcall Connect(UnicodeString AUser, UnicodeString APaswd);
  void __fastcall SetHostParam();
  void __fastcall LoadRegDef();
  UnicodeString __fastcall AddrCodeToStr(UnicodeString AAddrCode, int AParam = 0x0FF);
  UnicodeString __fastcall GetAddr(System::UnicodeString &AAddr, int AParam);

  __property TdsRegClient     *RegComp   = {read=FRegComp};
  __property TAxeXMLContainer *XMLList    = {read=FGetXMLList};

  __property UnicodeString DataPath   = {read=FDataPath};
  __property UnicodeString XMLPath    = {read=FXMLPath};
  __property TConnectDM*   Clients    = {read=FClients};

  __fastcall TDM(TComponent* Owner);
  bool __fastcall PreloadData();
  void __fastcall Login();
  __property TTagNode *RT = {read=UserRT};
};
//---------------------------------------------------------------------------
extern PACKAGE TDM *DM;
//---------------------------------------------------------------------------
#endif
