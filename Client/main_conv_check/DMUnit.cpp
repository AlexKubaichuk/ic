// ---------------------------------------------------------------------------
/*
 ����������               ���.
 �����������              �����.
 ��������                 ����.
 ������                   ����.
 ��������                 �����.
 ����                     ����
 �������                  �����.
 ���������                �����.
 �����                    �����
 �������� ���������       ����.
 ��-���������             ��-���.
 ��������������� �������� ������.
 ��������                 �����.
 ������� �                ���.�
 ���-��������             ���.
 �������������� ��������  �����.
 ������������             �����.
 ���������                �����.
 �����������              �������.
 ��������� ����           ���.��.
 ���������                ����.
 ������� ���              ��.���
 ����                     ����
 ���������                ���������
 �������� ����            ����.����
 ������� �                ���.�
 ������                   ������
 ���������� �����         ���.�����
 ������������ ��������    �������.
 ������ ���������         ����.���.
 ����� ��������� �������� ���
 */
#pragma hdrstop
#include "DMUnit.h"
#include "ExtUtils.h"
#include "msgdef.h"
#include "dsRegEDContainer.h"
#include "DBSelect.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma link "AppOptBaseProv"
#pragma link "AppOptDialog"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinsForm"
#pragma resource "*.dfm"
TDM * DM;
// ---------------------------------------------------------------------------
__fastcall TDM::TDM(TComponent * Owner) : TDataModule(Owner)
 {
  FXMLList              = new TAxeXMLContainer;
  FXMLList->OnExtGetXML = FExtGetXML;
  FAppOptValNode        = new TTagNode;
  FExtFilter            = new TTagNode;
  FDataPath             = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\ic");
  FXMLPath              = icsPrePath(FDataPath + "\\xml");
  UnicodeString FFN = icsPrePath(FXMLPath + "\\ic_opt.xml");
  if (!FileExists(FFN))
   {
    ForceDirectories(FXMLPath);
    TTagNode * tmp = new TTagNode;
    try
     {
      tmp->Encoding = "utf-8";
      tmp->AsXML    =
       "<g><i n='0001' v='localhost'/><i n='0002' v='5100'/><i n='0003' v='localhost'/><i n='0004' v='5100'/><i n='0005' v='localhost'/><i n='0006' v='5100'/><i n='0016' v=''/><i n='0014' v=''/><i n='0015' v=''/></g>";
      tmp->SaveToXMLFile(FFN, "");
     }
    __finally
     {
      delete tmp;
     }
   }
  ConnectOptXMLData->FileName = FFN;
  ConnectOpt->LoadXML();

  // LoadSrvOpt();

  UserRT   = new TTagNode;
  FClients = new TConnectDM(this, GetHostParamStr());
  // ##############################################################################
  // #                                                                            #
  // #                            TdsRegClient                                    #
  // #                                                                            #
  // ##############################################################################
  FRegComp                   = new TdsRegClient(this);
  FRegComp->XMLList          = FXMLList;
  FRegComp->OnGetClassXML    = FdsGetClassXML;
  FRegComp->OnGetCount       = FdsGetCount;
  FRegComp->OnGetIdList      = FdsGetIdList;
  FRegComp->OnGetValById     = FdsGetValById;
  FRegComp->OnGetValById10   = FdsGetValById10;
  FRegComp->OnFind           = FdsFind;
  FRegComp->OnInsertData     = FdsInsertData;
  FRegComp->OnEditData       = FdsEditData;
  FRegComp->OnDeleteData     = FdsDeleteData;
  FRegComp->FetchAll         = TdsRegFetch::Auto;
  FRegComp->ClassFetchAll    = TdsRegFetch::All;
  FRegComp->FullEdit         = FileExists(icsPrePath(FDataPath + "\\fulledit.cfg"));
  FRegComp->SingleEditCol    = false;
  FRegComp->ClsSingleEditCol = false;
  FRegComp->OnExtBtnClick    = FRegCompExtEdit;
  // FRegComp->StyleController = icStyle;
  FRegComp->OnGetDefValues        = FdsOnSetDefValues;
  FRegComp->OnExtValidate         = FdsOnExtValidate;
  FRegComp->OnShowPrePlan         = FdsOnShowPrePlan;
  FRegComp->OnPrintF63            = FdsOnPrintF63;
  FRegComp->OnInsPriv             = FInsPriv; // ���������� �������� ����� ��� ������ ���������
  FRegComp->StyleController       = EditStyle;
  FRegComp->XMLPath               = FXMLPath;
  FRegComp->DataPath              = FDataPath;
  FRegComp->UseQuickFilterSetting = true;
  FRegComp->OnExtFilterSet        = FdsOnExtFilterSet;
  FRegComp->UseReportSetting      = true;
  FRegComp->OnPrintReportClick    = FdsOnPrintReportClick;
  FRegDef                         = new TTagNode;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::DataModuleDestroy(TObject * Sender)
 {
  try
   {
    FClients->Adm->Logout();
   }
  catch (...)
   {
   }
  Sleep(100);
  delete FExtFilter;
  delete UserRT;
  delete FRegComp;
  delete FRegDef;
  delete FAppOptValNode;
  delete FClients;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnPrintReportClick(UnicodeString ARepGUI, __int64 AUCode, UnicodeString AFilter,
 TkabProgress AProgressProc)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      // PrBar->Max = AVal;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      // Caption = FSaveCaption+"  ["+AMsg+"]";
      // PrBar->Position = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      // Caption = FSaveCaption;
      break;
     }
   }
 }
// ---------------------------------------------------------------------------
/* void __fastcall TDM::FdsOnGetReportList(TStringList *AList)
 {
 }
 //---------------------------------------------------------------------------
 */
UnicodeString __fastcall TDM::FdsOnExtFilterSet(TTagNode * ADefNode, bool & CanContinue, UnicodeString AFilterGUI)
 {
 }
// ---------------------------------------------------------------------------
/*
 void __fastcall TDM::FdsOnGetQuickFilterList(TStringList *AList)
 {
 }
 //---------------------------------------------------------------------------
 */
bool __fastcall TDM::PreloadData()
 {
  bool RC = false;
  try
   {
    LoadAppOptData();
    RC = true;
   }
  catch (System::Sysutils::Exception & E)
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetClassXML(System::UnicodeString ARef, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetClassXML(ARef);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TDM::FdsGetCount(System::UnicodeString AId, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    UnicodeString EC = "";
    RC = FClients->App->GetCount(AId, AFilterParam, EC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetIdList(System::UnicodeString AId, int AMax, UnicodeString AFilterParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetIdList(AId, AMax, AFilterParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetValById(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->GetValById10(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsFind(System::UnicodeString AId, UnicodeString AFindParam, TJSONObject *& ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->Find(AId, AFindParam);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsInsertData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
 System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->InsertData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetAddr(System::UnicodeString & AAddr, int AParam)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * RetData;
    UnicodeString FRetAddr;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    FRetAddr = "";
    RetData  = FClients->App->FindAddr(AAddr, "", AParam);
    itPair   = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (SameText(FClCode, "msg"))
       RC = FClStr;
      else
       FRetAddr += FClCode + "=" + FClStr + "\n";
     }
    AAddr = FRetAddr;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::AddrCodeToStr(UnicodeString AAddrCode, int AParam)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FClients->App->AddrCodeToText(AAddrCode, AParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetOrg(System::UnicodeString & AAddr)
 {
  UnicodeString RC = "error";
  /* try
   {
   TJSONObject * RetData;
   UnicodeString FRetAddr;
   UnicodeString FClCode, FClStr;
   TJSONPairEnumerator * itPair;
   FRetAddr = "";
   RetData  = FClients->Org->Find(AAddr, true);
   itPair   = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
   FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
   if (SameText(FClCode, "msg"))
   RC = FClStr;
   else
   FRetAddr += FClCode + "=" + FClStr + "\n";
   }
   AAddr = FRetAddr;
   RC = "ok";
   }
   __finally
   {
   }
   */
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::OrgCodeToStr(UnicodeString AOrgCode)
 {
  UnicodeString RC = "error";
  try
   {
    // RC = FClients->Org->OrgCodeToText(AOrgCode, "11111111");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsEditData(System::UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecID,
 System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->EditData(AId, AValue, ARecID);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsDeleteData(System::UnicodeString AId, System::UnicodeString ARecId, System::UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    ARC = FClients->App->DeleteData(AId, ARecId);
    RC  = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FPreviewDoc(UnicodeString ATxt_Id, bool AByCode)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FHTMLPreviewDoc(UnicodeString ATxt_Id)
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FRegCompExtEdit(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource,
 bool isClicked, TObject * ACtrlList, int ABtnIdx)
 {
  bool RC = false;
  #ifndef IMM_DOCUMENT
  TdsRegEDContainer * CtrList = (TdsRegEDContainer *)ACtrlList;
  bool FTemplate = !ASource;
  try
   {
    try
     {
      if (!ItTag)
       {
        Src = "_NULL_=������( �� ������ ��������)";
        // RC = false;
       }
      else
       {
        if (isClicked)
         { // click �� ������
          UnicodeString FSrc = Src.Trim();
          // <extedit name='���� � ��' uid='0011' required='1' minleft='50' linecount='2' btncount='1' length='255'/>
          if (ItTag->CmpAV("uid", "0011"))
           { // ���� � ��
            if (Src.Length())
             OpenDlg->FileName = Src;
            OpenDlg->Filter = "�� \"��-���\"|ic.fdb";
            if (OpenDlg->Execute())
             {
              Src = OpenDlg->FileName + "=" + OpenDlg->FileName;
              RC  = true;
             }
           }
          else if (ItTag->CmpAV("uid", "001D"))
           { // ���� � ��
            if (Src.Length())
             OpenDlg->FileName = Src;
            OpenDlg->Filter = "�� \"��-���������������\"|ic_c.fdb";
            if (OpenDlg->Execute())
             {
              Src = OpenDlg->FileName + "=" + OpenDlg->FileName;
              RC  = true;
             }
           }
          else if (ItTag->CmpAV("uid", "00CC"))
           { // �����
            UnicodeString FRC = GetAddr(Src, 0x1F);
            if (!SameText(FRC, "ok"))
             _MSG_ERRA(FRC, "���������");
            else
             RC = true;
           }
          else if (ItTag->CmpAV("uid", "0125,0126"))
           { // ������������, ������
            UnicodeString FId = icsNewGUID().UpperCase();
            Src = FId + "=" + FId;
            RC  = true;
           }
         }
        else
         { // ��������� ��������
          if (ItTag->CmpAV("uid", "0011,001D"))
           { // // ���� � ��
            Src += "=" + Src;
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "00CC"))
           { // �����
            Src += "=" + AddrCodeToStr(Src);
            RC = true;
           }
          else if (ItTag->CmpAV("uid", "0125,0126"))
           { // ������������, ������
            Src += "=" + Src;
            RC = true;
           }
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      Src = "_NULL_=������ (" + E.Message + ")";
     }
   }
  __finally
   {
   }
  #endif
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FGetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData)
 {
  bool RC = false;
  try
   {
    if (ACode)
     {
      RC = FRegComp->GetUnitData(ACode, AData);
     }
    else
     {
      FRegComp->GetSelectedUnitData(AData);
      if (AData)
       RC = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnPrintF63(__int64 Code, UnicodeString UnitStr)
 {
  try
   {
    UnicodeString F63 = "";
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnShowPrePlan(__int64 Code, UnicodeString UnitStr)
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FPlanOpenCard(__int64 Code)
 {
  UnicodeString US = "";
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadRegDef()
 {
  FRegDef->AsXML   = FClients->App->GetDefXML();
  FRegComp->RegDef = FRegDef;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::ConnectOptDlgAfterComponentCreate(TTagNode * ANode, TComponent * component)
 {
  /*
   if (ANode->CmpAV("uid","002E"))
   {
   TDocOrgEditForm *Dlg = NULL;
   try
   {
   try
   {
   if (DM->ICSDocEx->DM->trOrg->Active) DM->ICSDocEx->DM->trOrg->Commit();
   DM->ICSDocEx->DM->trOrg->StartTransaction();
   if (!DM->ICSDocEx->DM->quOrg->Active) DM->ICSDocEx->DM->quOrg->CloseOpen(true);
   DM->ICSDocEx->DM->quOrg->Filtered = false;
   DM->ICSDocEx->DM->quOrg->Filter = "SUBORD=2";
   DM->ICSDocEx->DM->quOrg->Filtered = true;
   DM->ICSDocEx->DM->qtExecDoc("Select Count(*) as RCOUNT From SUBORDORG where SUBORD=2", false);
   if (!DM->ICSDocEx->DM->quFreeDoc->FN("RCOUNT")->AsInteger)
   {
   DM->ICSDocEx->DM->quOrg->Insert();
   DM->ICSDocEx->DM->quOrg->FN("CODE")->AsString      = NewGUID();
   DM->ICSDocEx->DM->quOrg->FN("NAME")->AsString      = "������������ �����������";
   DM->ICSDocEx->DM->quOrg->FN("FULLNAME")->AsString  = "������ ������������ �����������";
   DM->ICSDocEx->DM->quOrg->FN("ADDR")->AsString      = "����� �����������";
   DM->ICSDocEx->DM->quOrg->FN("SUBORD")->AsInteger  = 2;
   DM->ICSDocEx->DM->quOrg->FN("OKPO")->AsString     = "0";
   DM->ICSDocEx->DM->quOrg->FN("SOATO")->AsString    = "0";
   DM->ICSDocEx->DM->quOrg->FN("OKATO")->AsString     = "0";
   DM->ICSDocEx->DM->quOrg->FN("OKVED")->AsString    = "0";
   DM->ICSDocEx->DM->quOrg->FN("EMAIL")->AsString     = "";
   DM->ICSDocEx->DM->quOrg->FN("PHONE")->AsString     = "";
   DM->ICSDocEx->DM->quOrg->FN("OWNERFIO")->AsString     = "";

   DM->ICSDocEx->DM->quOrg->Post();
   }
   ICSDocEx->DM->quOrg->Edit();
   throw Exception("TDocOrgEditForm is not ported to XE6");
   }
   catch (...)
   {
   _MSG_ERRA("������ ����������� ������","������");
   ICSDocEx->DM->trOrg->Rollback();
   }
   }
   __finally
   {
   ICSDocEx->DM->quOrg->EnableControls();
   }
   }
   else if (ANode->CmpAV("uid","003E"))
   { // ������������ ����������
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "0044";
   DM->RegComp->ShowClasses(ctList,"1",-1);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   else if (ANode->CmpAV("uid","003F"))
   { // ����� ���������� ������
   #ifdef IMM_PLAN_VAC_SCH
   #endif
   }
   else if (ANode->CmpAV("uid","0040"))
   { // ����� ���������� ����
   #ifdef IMM_PLAN_PROB_SCH
   #endif
   }
   else if (ANode->CmpAV("uid","0041"))
   { // ������ ������� ��� ����
   #ifdef IMM_PLAN_SETTING
   #endif
   }
   else if (ANode->CmpAV("uid","0042"))
   { // ������ ����
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "00A6";
   DM->RegComp->ShowClasses(ctList,"2",-1);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   else if (ANode->CmpAV("uid","0043"))
   { // ��������� ����������� ����������
   #ifndef IMM_DOCUMENT
   TPrivPriorForm *Dlg = NULL;
   try
   {
   Dlg = new TPrivPriorForm(this);
   Dlg->ShowModal();
   }
   __finally
   {
   if (Dlg) delete Dlg;
   }
   #endif
   }
   else if (ANode->CmpAV("uid","0044"))
   { // ��������������� �����
   #ifdef IMM_PLAN_SETTING
   DM->RegComp->ClassShowExpanded = "0059";
   DM->RegComp->ShowClasses(ctList,"3",0);
   DM->RegComp->ClassShowExpanded = "";
   #endif
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::ConnectOptLoadXML(TTagNode * AOptNode)
 {
  try
   {
    AOptNode->Encoding = "utf-8";
    AOptNode->AsXML    =
     "<AppOptions>" " <passport title='��������� ����������' GUI='ICS_CON_OPT' autor='KAB' version='1' release='0' timestamp='20161021000000'/>"
     " <content>" "  <group name='grCommon' uid='0000' caption='�����'>"
     "   <group name='grICSrv' uid='0011' caption='&quot;��&quot;'>" "    <text name='ic_server' uid='0001' default='localhost' caption='������'/>"
     "    <digit name='ic_port' uid='0002' default='5100' caption='����' min='0' max='99999'/>" "   </group>"
     "   <group name='grICDocSrv' uid='0012' caption='&quot;��-���������������&quot;'>" "    <text name='ic_doc_server' uid='0003' default='localhost' caption='������'/>"
     "    <digit name='ic_doc_port' uid='0004' default='5100' caption='����' min='0' max='99999'/>" "   </group>"
     "   <group name='grICSVSrv' uid='0013' caption='&quot;��-����� ����&quot;'>" "    <text name='ic_sv_server' uid='0005' default='localhost' caption='������'/>"
     "    <digit name='ic_sv_port' uid='0006' default='5100' caption='����' min='0' max='99999'/>" "   </group>"
     "  </group>" "  <servicegroup name='ExtVars' uid='000F'>" "   <text name='l' uid='0014' caption='l'/>"
     "   <text name='p' uid='0015' caption='p'/>" "   <binary name='sp' uid='0016' caption='sp'/>" "  </servicegroup>"
     " </content>" "</AppOptions>";
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TDM::FGetXMLList()
 {
  return FXMLList;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FExtGetXML(TTagNode * ANode, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    // ANode->AsXML = FClients->Doc->GetClassZIPXML(Src);
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnSetDefValues(__int64 ARecCode, TTagNode * ANode, TdsRegEDContainer * ControlList,
 bool & Valid)
 {
  Valid = false;
  if (ANode)
   {
    try
     {
      TdsRegEDItem * tmpItem;
      UnicodeString FVal;
      if (ANode->CmpAV("uid", "00C7"))
       { // ���
        ControlList->EditItems["011E"]->SetValue(icsNewGUID().UpperCase(), "");
        Valid = true;
       }
      else if (ANode->CmpAV("uid", "0008"))
       {
        ControlList->EditItems["0125"]->SetValue(icsNewGUID().UpperCase(), "");
        ControlList->EditItems["0126"]->SetValue(icsNewGUID().UpperCase(), "");
        Valid = true;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA("������ ��������� �������� �� ���������.\n��������� ���������:\n" + E.Message, "������");
     }
    catch (...)
     {
      _MSG_ERRA("������ ��������� �������� �� ���������.", "������");
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsOnExtValidate(__int64 ARecCode, TTagNode * ANode, TdsRegEDContainer * ControlList, bool & Valid)
 {
  // #ifndef IMM_DOCUMENT
  TdsRegEDItem * tmpItem;
  UnicodeString FVal, FVal1, FVal2;
  try
   {
    if (ANode->CmpAV("uid", "0008"))
     { // <class name='���� ������' uid='0008' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' tab=''>
      // ���� ������ ---------------------------------------------
      FVal1 = ControlList->EditItems["0011"]->GetValue("");
      // extedit name='���� � �� &quot;��-���&quot;' uid='0011' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
      FVal2 = ControlList->EditItems["001D"]->GetValue("");
      // extedit name='���� � �� &quot;��-���-���������������&quot;' uid='001D' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
      if (!(FVal1.Length() + FVal2.Length()))
       {
        _MSG_ERRA("���������� ������� ���� ��� ������� � ����� �� ��.", "������ �����");
        Valid = false;
       }
     }
    else if (ANode->CmpAV("uid", "00C7"))
     { // ���
      // ��� �����������
      FVal = ControlList->EditItems["011E"]->GetValue("").Trim();
      if (FVal.Length())
       {
        if ((FVal == "812.2741381.000") || (FVal == "812.1107704.000") || (FVal == "812.7107704.000") ||
         (FVal == "812.7177704.000") || (FVal == "812.7170425.000") || (FVal == "812.7100425.000") ||
         (FVal == "812.1100425.000") || (FVal == "812.7171319.000"))
         {
          _MSG_ERRA("��������� ��� ��� ��������������, ������� ������ ���.", "������ �����");
          Valid = false;
         }
        else
         {
          TJSONObject * RetData = NULL;
          TJSONPairEnumerator * itPair;
          bool FCont = true;
          try
           {
            FdsGetValById("reg.s2.Select Count(*) as ps1,  '" + IntToStr(ARecCode) +
             "' as ps2 From CLASS_00C7 Where code <> " + IntToStr(ARecCode) + " and Upper(R011E)='" + FVal.UpperCase() +
             "'", "0", RetData);
            itPair = RetData->GetEnumerator();
            while (itPair->MoveNext() && FCont)
             {
              if (((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0))
               {
                FCont = false;
                _MSG_ERRA("��� � ��������� ����� ��� ����������, ������� ������ ���.", "������ �����");
                Valid = false;
               }
             }
           }
          __finally
           {
            // if (RetData) delete RetData;
            // RetData = NULL;
           }
         }
       }
     }
    else if (ANode->CmpAV("uid", "0000"))
     {
     }
   }
  catch (System::Sysutils::Exception & E)
   {
    _MSG_ERRA("������ �������� ������������ �������� ������.\n��������� ���������:\n" + E.Message, "������");
    Valid = false;
   }
  catch (...)
   {
    _MSG_ERRA("������ �������� ������������ �������� ������.", "������");
    Valid = false;
   }
  // #endif
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::LoadAppOptData()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowVacSch()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowTestSch()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::FdsShowCheckSch()
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FdsGetPlanOpts(UnicodeString & AOptDef)
 {
  bool RC = false;
  try
   {
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::UploadData(UnicodeString AData, TkabProgress AProgress)
 {
  try
   {
    try
     {
      int FLen;
      if (AProgress)
       AProgress(TkabProgressType::InitCommon, AData.Length(), "");
      FClients->App->UploadData("", 0);
      for (int i = 1; i <= AData.Length(); i += 2048)
       {
        FLen = 2048;
        if ((i + FLen) > AData.Length())
         FLen = AData.Length() - i + 1;
        if (AProgress)
         AProgress(TkabProgressType::CommPerc, i * 100 / AData.Length(), "");
        Application->ProcessMessages();
        FClients->App->UploadData(AData.SubString(i, FLen), 1);
        if (AProgress)
         AProgress(TkabProgressType::CommPerc, (i + 1024) * 100 / AData.Length(), "");
        Application->ProcessMessages();
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERRA(E.Message, "������");
     }
   }
  __finally
   {
    if (AProgress)
     AProgress(TkabProgressType::CommComplite, 0, "");
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::Connect(UnicodeString AUser, UnicodeString APaswd)
 {
  bool RC = false;
  try
   {
    Application->ProcessMessages();
    FClients->SetConnectParam(AUser, APaswd);
    try
     {
      Application->ProcessMessages();
      // FUserRt->AsXML = FClients->App->LoginUser("AdMin");
      LoadRegDef();
      Application->ProcessMessages();
      RC = true;
      // DM->Authenticate();
     }
    catch (...)
     {
     }
   }
  __finally
   {
    Application->ProcessMessages();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDM::GetHostParamStr()
 {
  UnicodeString RC = "";
  TTagNode * ConnOptNode = new TTagNode;
  try
   {
    ConnOptNode->Name                = "s";
    ConnOptNode->AV["ic_server"]     = ConnectOpt->Vals["ic_server"].AsStringDef("localhost");
    ConnOptNode->AV["ic_port"]       = IntToStr(ConnectOpt->Vals["ic_port"].AsIntDef(5100));
    ConnOptNode->AV["ic_doc_server"] = ConnectOpt->Vals["ic_doc_server"].AsStringDef("localhost");
    ConnOptNode->AV["ic_doc_port"]   = IntToStr(ConnectOpt->Vals["ic_doc_port"].AsIntDef(5100));
    ConnOptNode->AV["ic_sv_server"]  = ConnectOpt->Vals["ic_sv_server"].AsStringDef("localhost");
    ConnOptNode->AV["ic_sv_port"]    = IntToStr(ConnectOpt->Vals["ic_sv_port"].AsIntDef(5100));
    if (!ConnOptNode->AV["ic_server"].Trim().Length())
     ConnOptNode->AV["ic_server"] = "localhost";
    if (!ConnOptNode->AV["ic_port"].Trim().Length())
     ConnOptNode->AV["ic_port"] = "5100";
    if (!ConnOptNode->AV["ic_doc_server"].Trim().Length())
     ConnOptNode->AV["ic_doc_server"] = "localhost";
    if (!ConnOptNode->AV["ic_doc_port"].Trim().Length())
     ConnOptNode->AV["ic_doc_port"] = "5100";
    if (!ConnOptNode->AV["ic_sv_server"].Trim().Length())
     ConnOptNode->AV["ic_sv_server"] = "localhost";
    if (!ConnOptNode->AV["ic_sv_port"].Trim().Length())
     ConnOptNode->AV["ic_sv_port"] = "5100";
    RC = ConnOptNode->AsXML;
   }
  __finally
   {
    delete ConnOptNode;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::SetHostParam()
 {
  try
   {
    Application->ProcessMessages();
    FClients->SetHostParam(GetHostParamStr());
   }
  __finally
   {
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::FInsPriv(TkabProgressType AStage, TTagNode * ADataDefNode, TkabCustomDataSource * ASource,
 __int64 ARecCode, TkabProgress AProgress)
 {
  bool RC = false;
  try
   {
    if (AStage == TkabProgressType::InitStage)
     {
      FCur   = 0;
      FCount = ARecCode;
      RC     = true;
      /*
       SaveDlg->FileName = Date().FormatString("�����_��_yyyy.mm.dd");
       if (SaveDlg->Execute())
       {
       Application->ProcessMessages();
       if (IEData) delete IEData;
       IEData = new TTagNode;
       Application->ProcessMessages();
       IEData->AsXML = FClients->EIData->GetExportDataHeader();
       Application->ProcessMessages();
       IEContent = IEData->GetChildByName("content");
       Application->ProcessMessages();
       Application->ProcessMessages();
       if (IEContent)
       {
       AProgress(TkabProgressType::InitCommon, ARecCode, "�������� ������ ...");
       Application->ProcessMessages();
       RC = true;
       }
       Application->ProcessMessages();
       }
       */
     }
    else if (AStage == TkabProgressType::StageInc)
     {
      FCur++ ;
      AProgress(TkabProgressType::StageInc, 100, IntToStr(FCur) + "/" + IntToStr(FCount));
      Sleep(10);
      /*
       IEContent->AddChild("ur")->AV["PCDATA"] = FClients->EIData->GetPatDataById(IntToStr(ARecCode));
       Sleep(10);
       */
      Application->ProcessMessages();
     }
    else if (AStage == TkabProgressType::StageComplite)
     {
      /*
       AProgress(TkabProgressType::CommComplite, 0, "");
       IEData->SaveToZIPXMLFile(SaveDlg->FileName);
       delete IEData; IEData = NULL;
       */
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::Login()
 {
  TDBSelectUnit * SelDlg = NULL;
  TTagNode * LRC = new TTagNode;
  try
   {
    Application->ProcessMessages();
    int i = 3;
    bool connected = false;
    while ((i > 0) && !connected)
     {
      try
       {
        Application->ProcessMessages();
        LRC->AsXML = FClients->Adm->LoginUser("");
        Application->ProcessMessages();
        if (LRC->CmpName("nouser"))
         {
          FClients->SetConnectParam("", "");
          throw System::Sysutils::Exception("������ ����� � �������.");
         }
        else if (LRC->CmpName("access_denied"))
         {
          FClients->SetConnectParam("", "");
          throw System::Sysutils::Exception("������ ����� � �������.");
         }
        else if (LRC->CmpName("rt"))
         {
          if (LRC->Count > 1)
           {
            Application->ProcessMessages();
            SelDlg = new TDBSelectUnit(this, LRC);
            SelDlg->ShowModal();
            if (SelDlg->ModalResult == mrOk)
             LRC->AsXML = FClients->Adm->LoginUser(SelDlg->DBId);
            else
             throw EAbort("�������� �������� �������������.");
            Application->ProcessMessages();
           }
          if (LRC->Count == 1)
           {
            Application->ProcessMessages();
            ConnectOpt->Vals["l"] = FClients->dsICRestCon->UserName;
            if (ConnectOpt->Vals["sp"].AsBoolDef(false))
             ConnectOpt->Vals["p"] = FClients->dsICRestCon->Password;
            Application->ProcessMessages();
            // LRC->AsXML;
            Application->ProcessMessages();
            connected = true;
            Application->ProcessMessages();
           }
          else
           {
            throw System::Sysutils::Exception("������ ����� � �������.");
           }
         }
        else
         {
          throw System::Sysutils::Exception("������ ����� � �������.");
         }
       }
      catch (System::Sysutils::Exception & E)
       {
        if (E.ClassNameIs("EAbort"))
         i = 0;
        else
         {
          _MSG_ERRA(E.Message, "������ ����� � �������");
          if (!ConnectOptDlg->Execute())
           i = 0;
         }
       }
      i-- ;
     }
    if (!connected)
     Application->Terminate();
   }
  __finally
   {
    if (SelDlg)
     delete SelDlg;
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDM::SrvOptLoadXML(TTagNode * AOptNode)
 {
  UnicodeString FFN = icsPrePath(icsPrePath(ExtractFilePath(ParamStr(0))) + "\\defxml\\srvoptdef.zxml");
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   _MSG_ERRA("������ �������� �������� �������� 'vssrvoptdef.zxml'", "������ �������� �������� ��������");
 }
// ---------------------------------------------------------------------------
bool __fastcall TDM::LoadSrvOpt()
 {
  bool RC = false;
  try
   {
    try
     {
      UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
      UnicodeString FFN = icsPrePath(FXMLPath + "\\srv_opt.xml");
      if (!FileExists(FFN))
       {
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      SrvOptXMLData->FileName = FFN;
      SrvOpt->LoadXML();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------

