object DM: TDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 456
  Width = 661
  object ConnectOptDlg: TAppOptDialog
    FormHeight = 500
    Options = ConnectOpt
    AfterComponentCreate = ConnectOptDlgAfterComponentCreate
    Left = 23
    Top = 24
  end
  object ConnectOpt: TAppOptions
    OnLoadXML = ConnectOptLoadXML
    DataProvider = ConnectOptXMLData
    Left = 100
    Top = 23
  end
  object ConnectOptXMLData: TAppOptXMLProv
    Left = 185
    Top = 24
  end
  object SrvOptDlg: TAppOptDialog
    FormHeight = 500
    Options = SrvOpt
    Left = 26
    Top = 97
  end
  object EditStyle: TcxEditStyleController
    Left = 85
    Top = 356
    PixelsPerInch = 96
  end
  object DefEditStyle: TcxDefaultEditStyleController
    Left = 150
    Top = 357
    PixelsPerInch = 96
  end
  object Skin: TdxSkinController
    Left = 27
    Top = 356
  end
  object OpenDlg: TOpenDialog
    Filter = #1041#1044' '#1055#1050' "'#1059#1048'-'#1051#1055#1059'"|ic*.fdb'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 168
    Top = 183
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.zxml'
    Filter = #1050#1072#1088#1090#1099' '#1055#1050' '#1059#1048'-'#1051#1055#1059' (*.zxml)|*.zxml'
    Left = 167
    Top = 239
  end
  object SrvOpt: TAppOptions
    OnLoadXML = SrvOptLoadXML
    DataProvider = SrvOptXMLData
    Left = 89
    Top = 99
  end
  object SrvOptXMLData: TAppOptXMLProv
    Left = 171
    Top = 94
  end
end
