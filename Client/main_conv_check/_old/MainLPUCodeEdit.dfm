object MainLPUEditForm: TMainLPUEditForm
  Left = 615
  Top = 149
  BorderStyle = bsSingle
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1051#1055#1059
  ClientHeight = 101
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 64
    Width = 387
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 98
    DesignSize = (
      387
      37)
    object OkBtn: TcxButton
      Left = 213
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 301
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object LPUClassPanel: TPanel
    Left = 0
    Top = 0
    Width = 387
    Height = 64
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 1
    ExplicitHeight = 98
    object LPULab: TLabel
      AlignWithMargins = True
      Left = 6
      Top = 6
      Width = 27
      Height = 13
      Align = alTop
      Caption = #1051#1055#1059':'
      FocusControl = LPUCB
    end
    object LPUCB: TcxComboBox
      AlignWithMargins = True
      Left = 6
      Top = 25
      Align = alTop
      Properties.DropDownListStyle = lsEditFixedList
      Style.Color = clInfoBk
      TabOrder = 0
      Width = 375
    end
  end
end
