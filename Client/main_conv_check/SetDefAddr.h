//---------------------------------------------------------------------------

#ifndef SetDefAddrH
#define SetDefAddrH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxListBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "DMUnit.h"

//---------------------------------------------------------------------------
class TDefAddrForm : public TForm
{
__published:	// IDE-managed Components
 TcxButtonEdit *AddrBE;
 TPanel *BtnPanel;
 TcxButton *OkBtn;
 TcxButton *CancelBtn;
 TcxListBox *AddrLB;
 void __fastcall AddrBEPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall OkBtnClick(TObject *Sender);
 void __fastcall AddrLBDblClick(TObject *Sender);
 void __fastcall AddrLBKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
 void __fastcall AddrLBExit(TObject *Sender);
 void __fastcall AddrBEKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
 void __fastcall AddrBEPropertiesChange(TObject *Sender);

private:	// User declarations
 UnicodeString FAddr;
 UnicodeString FStrAddr;

 TAnsiStrMap    FFindedObjects;
public:		// User declarations
  __fastcall TDefAddrForm(TComponent* Owner);
  __property UnicodeString Addr={read=FAddr};
  __property UnicodeString StrAddr={read=FStrAddr};
};
//---------------------------------------------------------------------------
extern PACKAGE TDefAddrForm *DefAddrForm;
//---------------------------------------------------------------------------
#endif
