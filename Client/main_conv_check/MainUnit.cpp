﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "SetDefAddr.h"
#include "MainUnit.h"
#include "ExtUtils.h"
#include "kabQueryUtils.h"
// #include "LoginUnit.h"
#include "JSONUtils.h"
#include "msgdef.h"
#include "KLADRParsers.h"
// #include "SetDBPath.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxGraphics"
#pragma link "dxGDIPlusClasses"
#pragma link "ICSAboutDialog"
#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "cxCheckBox"
#pragma link "cxPC"
#pragma link "dxBarBuiltInMenu"
#pragma link "cxClasses"
#pragma resource "*.dfm"
TMainForm * MainForm;
// ---------------------------------------------------------------------------
#define RegTB  MainTBM->Bars->Items[0]
// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent * Owner) : TForm(Owner)
 {
  FC          = 0;
  DefAddrCode = "";
  // icDxLocale();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::UpdateMenuItems()
 {
  // <binary name='Администрирование' uid='001E' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  // <binary name='Ведение списка ЛПУ' uid='011F' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  bool FAdm = true;
  bool FAdmLPU = true;
  // if (DM->RT->GetTagByUID("001E"))
  // FAdm = DM->RT->GetTagByUID("001E")->AV["v"].ToIntDef(0);
  // if (DM->RT->GetTagByUID("011F"))
  // FAdmLPU = DM->RT->GetTagByUID("011F")->AV["v"].ToIntDef(0);
 }
// ---------------------------------------------------------------------
bool __fastcall TMainForm::CheckConnect()
 {
  DM->Login();
  DM->LoadRegDef();
  try
   {
    UpdateMenuItems();
   }
  catch (EAccessViolation & E)
   {
    Application->MessageBox(E.Message.c_str(), L"Ошибка", MB_OK);
   }
  DM->PreloadData();
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject * Sender)
 {
  Application->ProcessMessages();
  try
   {
    try
     {
      CheckConnect();
      TDefAddrForm * Dlg = new TDefAddrForm(this);
      try
       {
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
          DefAddrCode = Dlg->Addr;
          Caption = Caption + "  " +Dlg->StrAddr;

         }
       }
      __finally
       {
        delete Dlg;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      _MSG_ERR("Ошибка аутентификации пользователя.\nСистемное сообщение:\n" + E.Message, "Ошибка входа в систему.");
      Application->Terminate();
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actClientSettingExecute(TObject * Sender)
 {
  DM->ConnectOptDlg->Execute();
  UpdateMenuItems();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actHelpExecute(TObject * Sender)
 {
  Application->HelpCommand(HELP_FINDER, 4001);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::actAboutExecute(TObject * Sender)
 {
  AboutDlg->FileName = icsPrePath(ParamStr(0));
  AboutDlg->Execute();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (_MSG_QUE("Прервать?", "Сообщение") == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::SetInProgress(bool AVal)
 {
  CheckBtn->Caption = (AVal) ? "Прервать" : "Проверить";
  FInProgress       = AVal;
  PrBar->Visible    = AVal;
  if (AVal)
   PrBar->Position = 0;
  // SetEnabled(!AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::DBPathBEPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  if (OpenDlg->Execute())
   {
    DBPathBE->Text = OpenDlg->FileName;
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMainForm::GetAddr(System::UnicodeString AOldAddr, System::UnicodeString & AAddr,
  System::UnicodeString ADefAddr, int AParam)
 {
  UnicodeString RC = "error";
  try
   {
    TJSONObject * RetData;
    UnicodeString FRetAddr;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    FRetAddr = "";
    RetData  = DM->Clients->App->FindAddr(AAddr, ADefAddr, AParam);
    adresCode AddrCode;
    TcxTreeListNode * curNode = NULL;
    TcxTreeListNode * tmpNode;
    if (AddrTL->SelectionCount)
     curNode = AddrTL->Selections[0];
    if (curNode)
     {
      curNode->Values[1] = false;
      curNode->Texts[2]  = "";
      curNode->Texts[3]  = "";
     }
    if (curNode->Count)
     curNode->DeleteChildren();
    itPair = RetData->GetEnumerator();
    while (curNode && itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (!SameText(FClCode, "msg"))
       {
        AddrCode = ParseAddrStr(FClCode, false);
        if (RetData->Count > 1)
         {
          if (RetData->Count > 2)
           tmpNode = curNode->AddChild();
          else
           tmpNode = curNode;
          tmpNode->Values[1] = false;
          if ((RetData->Count == 2) && AddrCode.StreetID)
           tmpNode->Values[1] = true;
          tmpNode->Texts[0] = AAddr;
          tmpNode->Texts[2] = FClStr;
          tmpNode->Texts[3] = FClCode;
          tmpNode->Texts[4] = AOldAddr;
         }
       }
      /*
       if (SameText(FClCode, "msg"))
       RC = FClStr;
       else
       //FRetAddr +=  FClCode + "=" +  FClStr + "\n";
       */
     }
    if (curNode)
     {
      curNode->Expand(true);
     }
    AAddr = FRetAddr;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::CheckBtnClick(TObject * Sender)
 {
  int a = 0;
  if (!InProgress && DBPathBE->Text.Trim().Length())
   {
    InProgress = true;
    ConnectDB(IcConnection, DBPathBE->Text.Trim());
    TkabQueryHelper * QH = new TkabQueryHelper(IcConnection);
    TFDQuery * FQ19 = QH->CreateTempQuery();
    TFDQuery * FQ18 = QH->CreateTempQuery();
    TcxTreeListNode * AddrNode;
    UnicodeString FAddr;
    AddrTL->BeginUpdate();
    AddrTL->Clear();
    TcxTreeListNode * NFAddrNode = AddrTL->Root->AddChild();
    TcxTreeListNode * ManyAddrNode = AddrTL->Root->AddChild();
    TcxTreeListNode * ComplitAddrNode = AddrTL->Root->AddChild();
    TcxTreeListNode * tmpNode;
    NFAddrNode->Texts[0]      = "Соответствия не найдены";
    ManyAddrNode->Texts[0]    = "Множественные соответствия";
    ComplitAddrNode->Texts[0] = "Соответствия определены";
    int NFAddrCount, ManyAddrCount, ComplitAddrCount;
    TJSONObject * RetData;
    UnicodeString FClCode, FClStr;
    TJSONPairEnumerator * itPair;
    adresCode AddrCode;
    try
     {
      NFAddrCount      = 0;
      ManyAddrCount    = 0;
      ComplitAddrCount = 0;
      QH->Exec(FQ19, false,
        // "Select Count(*) as RCOUNT from class_0019 left join class_0018 on (class_0019.code=class_0018.r0019) ");
        "Select distinct count(*) as RCOUNT from ( Select distinct R001A,R001B from class_0019 left join class_0018 on (class_0019.code=class_0018.r0019) union Select distinct R001A,R001B from class_a0019 left join class_a0018 on (class_a0019.code=class_a0018.r0019) ) ");
      int AddrCount = FQ19->FieldByName("RCOUNT")->AsInteger;
      QH->Exec(FQ19, false,
        // "Select distinct (R001A||', '||R001B) as Addr from class_0019 left join class_0018 on (class_0019.code=class_0018.r0019) order by R001A, R001B");
        "Select distinct (R001A||', '||R001B) as Addr from ( Select distinct R001A,R001B from class_0019 left join class_0018 on (class_0019.code=class_0018.r0019) union Select distinct R001A,R001B from class_a0019 left join class_a0018 on (class_a0019.code=class_a0018.r0019) ) order by R001A,R001B");
      // left join class_0017 on (class_0018.code=class_0017.r0018) left join casebook1 on (class_0017.code=casebook1.r0032)
      // QH->Exec(FQ18, false, "Select * from class_0018");
      PrBar->Max      = AddrCount * 2;
      PrBar->Position = 0;
      while (!FQ19->Eof && FInProgress)
       {
        // AddrNode           = AddrTL->Root->AddChild();
        PrBar->Position++ ;
        Application->ProcessMessages();
        FAddr   = FQ19->FieldByName("Addr")->AsString;
        RetData = DM->Clients->App->FindAddr(FAddr, DefAddrCode, 0x38);
        itPair  = RetData->GetEnumerator();
        while (itPair->MoveNext())
         {
          FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
          FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
          if (!SameText(FClCode, "msg"))
           {
            AddrCode = ParseAddrStr(FClCode, false);
            if (RetData->Count == 1)
             AddrNode = NFAddrNode->AddChild();
            else if (RetData->Count == 2)
             {
              if (AddrCode.StreetID)
               AddrNode = ComplitAddrNode->AddChild();
              else
               AddrNode = NFAddrNode->AddChild();
             }
            else if (FAddr.Length())
             AddrNode = ManyAddrNode->AddChild();
            if (RetData->Count > 2)
             {
              if (FAddr.Length())
               {
                AddrNode->Texts[0] = FAddr;
                AddrNode->Texts[4] = FQ19->FieldByName("Addr")->AsString;
               }
              tmpNode = AddrNode->AddChild();
              tmpNode->Texts[0]  = FQ19->FieldByName("Addr")->AsString;
              tmpNode->Values[1] = false;
              tmpNode->Texts[2]  = FClStr;
              tmpNode->Texts[3]  = FClCode;
              tmpNode->Texts[4]  = FQ19->FieldByName("Addr")->AsString;
             }
            else
             {
              AddrNode->Values[1] = false;
              AddrNode->Texts[0]  = FAddr;
              if ((RetData->Count == 2) && AddrCode.StreetID)
               AddrNode->Values[1] = true;
              AddrNode->Texts[2] = FClStr;
              AddrNode->Texts[3] = FClCode;
              AddrNode->Texts[4] = FAddr;
             }
            if (FAddr.Length())
             {
              if (RetData->Count == 1)
               NFAddrCount++ ;
              else if (RetData->Count == 2)
               {
                if (AddrCode.StreetID)
                 ComplitAddrCount++ ;
                else
                 NFAddrCount++ ;
               }
              else
               ManyAddrCount++ ;
             }
            FAddr = "";
           }
         }
        // GetAddr(FAddr, DefAddrCode, 0x1F);
        // AddrNode->Texts[1] = FAddr;
        FQ19->Next();
        PrBar->Position++ ;
        Application->ProcessMessages();
       }
      NFAddrNode->Texts[0] = "Соответствия не найдены (" + IntToStr(NFAddrCount) + ")";
      ManyAddrNode->Texts[0]    = "Множественные соответствия (" + IntToStr(ManyAddrCount) + ")";
      ComplitAddrNode->Texts[0] = "Соответствия определены (" + IntToStr(ComplitAddrCount) + ")";
     }
    __finally
     {
      QH->DeleteTempQuery(FQ18);
      QH->DeleteTempQuery(FQ19);
      delete QH;
      DisconnectDB(IcConnection);
      InProgress = false;
      AddrTL->EndUpdate();
     }
   }
  else
   {
    InProgress = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::KLADRDBBEPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  if (OpenDlg->Execute())
   {
    KLADRDBBE->Text = OpenDlg->FileName;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::LoadKLADRClick(TObject * Sender)
 {
  int a = 0;
  if (!InProgress && KLADRDBBE->Text.Trim().Length())
   {
    InProgress = true;
    ConnectDB(KLADRConect, KLADRDBBE->Text.Trim());
    TkabQueryHelper * QH = new TkabQueryHelper(KLADRConect);
    TFDQuery * FQ14 = QH->CreateTempQuery();
    TFDQuery * FQ5 = QH->CreateTempQuery();
    // TFDQuery * FQ18 = QH->CreateTempQuery();
    // TcxTreeListNode * AddrNode;
    // UnicodeString FAddr;
    KLADRTL->BeginUpdate();
    KLADRTL->Clear();
    // TcxTreeListNode * NFAddrNode = AddrTL->Root->AddChild();
    // TcxTreeListNode * ManyAddrNode = AddrTL->Root->AddChild();
    // TcxTreeListNode * ComplitAddrNode = AddrTL->Root->AddChild();
    TcxTreeListNode * tmpNode;
    TcxTreeListNode * ND1, *ND2, *ND3, *CN;
    ND1 = ND2 = ND3 = NULL;
    int ID1, ID2, ID3, ID4;
    int qID1, qID2, qID3, qID4;
    ID1 = ID2 = ID3 = ID4 = -1;
    try
     {
      QH->Exec(FQ14, false, "Select Count(*) as RCOUNT from kladr14");
      int AddrCount = FQ14->FieldByName("RCOUNT")->AsInteger;
      QH->Exec(FQ14, false, "Select * from kladr14 order by ID1, ID2, ID3, ID4");
      // QH->Exec(FQ18, false, "Select * from class_0018");
      PrBar->Max      = AddrCount * 2;
      PrBar->Position = 0;
      while (!FQ14->Eof && FInProgress)
       {
        qID1 = FQ14->FieldByName("ID1")->AsInteger;
        qID2 = FQ14->FieldByName("ID2")->AsInteger;
        qID3 = FQ14->FieldByName("ID3")->AsInteger;
        qID4 = FQ14->FieldByName("ID4")->AsInteger;
        CN   = NULL;
        // 2 3 4
        // 0 0 40
        // 0 2 0
        if (qID1)
         {
          if (!(qID2 + qID3 + qID4))
           {
            ND1 = KLADRTL->Root->AddChild(); // регион
            CN  = ND1;
            ND2 = ND3 = NULL;
           }
          else
           {
            if (qID2 + qID3)
             {
              if (ID2 != qID2)
               {
                ND2 = ND1->AddChild();
                ND3 = NULL;
                CN  = ND2;
               }
              else if (ID3 != qID3)
               {
                if (ND2)
                 ND3 = ND2->AddChild();
                else
                 ND3 = ND1->AddChild();
                CN = ND3;
               }
              else
               {
                if (ND3)
                 CN = ND3->AddChild();
                else if (ND2)
                 CN = ND2->AddChild();
                else
                 CN = ND1->AddChild();
               }
             }
            else
             {
              CN = ND1->AddChild();
              // ND2 = ND3 = NULL;
              // CN  = ND2;
             }
           }
          ID1 = qID1;
          ID2 = qID2;
          ID3 = qID3;
          ID4 = qID4;
          QH->Exec(FQ5, false, "Select * from kladr5 where ID1=" + IntToStr(ID1) + " and ID2=" + IntToStr(ID2) +
            " and ID3 = " + IntToStr(ID3) + " and ID4=" + IntToStr(ID4) + " order by name");
          while (!FQ5->Eof && FInProgress)
           {
            FQ5->Next();
            if (CN)
             {
              tmpNode           = CN->AddChild();
              tmpNode->Texts[0] = FQ5->FieldByName("Name")->AsString;
             }
           }
         }
        if (CN)
         CN->Texts[0] = FQ14->FieldByName("Name")->AsString;
        PrBar->Position++ ;
        Application->ProcessMessages();
        FQ14->Next();
        PrBar->Position++ ;
        Application->ProcessMessages();
       }
     }
    __finally
     {
      QH->DeleteTempQuery(FQ14);
      QH->DeleteTempQuery(FQ5);
      delete QH;
      DisconnectDB(KLADRConect);
      InProgress = false;
      KLADRTL->EndUpdate();
     }
   }
  else
   {
    InProgress = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::cxTreeList1Column1PropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  UnicodeString Addr = ((TcxButtonEdit *)Sender)->Text;
  if (AddrTL->SelectionCount)
   GetAddr(AddrTL->Selections[0]->Texts[4], Addr, DefAddrCode, 0x3F);
  else
   GetAddr("", Addr, DefAddrCode, 0x3F);
  // ShowMessage(Addr);
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::SaveBtnClick(TObject * Sender)
 {
  if (SaveDlg->Execute())
   {
    TTagNode * SetNode = new TTagNode;
    SetNode->Name      = "addr";
    SetNode->AV["def"] = DefAddrCode;
    TTagNode * tmpNode;
    try
     {
      for (int i = 0; i < AddrTL->AbsoluteCount; i++)
       {
        if (!AddrTL->AbsoluteItems[i]->Values[1].IsNull() && AddrTL->AbsoluteItems[i]->Values[1])
         {
          tmpNode          = SetNode->AddChild("i");
          tmpNode->AV["s"] = AddrTL->AbsoluteItems[i]->Texts[4];
          tmpNode->AV["t"] = AddrTL->AbsoluteItems[i]->Texts[2];
          tmpNode->AV["c"] = AddrTL->AbsoluteItems[i]->Texts[3];
         }
       }
     }
    __finally
     {
      SetNode->SaveToZIPXMLFile(SaveDlg->FileName, "");
      delete SetNode;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::AddrTLEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn, bool & Allow)
 {
  Allow = false;
  if (AColumn && Sender && Sender->SelectionCount)
   {
     if (!AColumn->Position->ColIndex)
      Allow = Sender->Selections[0]->Level == 1;
     else if (AColumn->Position->ColIndex == 1)
      Allow = Sender->Selections[0]->Texts[3].Trim().Length() && Sender->Selections[0]->Texts[4].Trim().Length();
   }
 }
// ---------------------------------------------------------------------------
