//---------------------------------------------------------------------------

#ifndef UpdateCodeUnitH
#define UpdateCodeUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxClasses.hpp"
#include "cxGraphics.hpp"
#include "dxBar.hpp"
#include <Vcl.ImgList.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMemo.hpp"
#include "cxTextEdit.hpp"
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TUpdateCode : public TForm
{
__published:	// IDE-managed Components
 TdxBarManager *ClassTBM;
 TdxBar *ClassTBMBar1;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton5;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TcxImageList *LClassIL32;
 TdxBarButton *dxBarButton1;
 TdxBarButton *dxBarButton2;
 TcxMemo *CodeMemo;
 TSaveDialog *SaveDlg;
 void __fastcall dxBarButton1Click(TObject *Sender);
 void __fastcall dxBarButton2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
 __fastcall TUpdateCode(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TUpdateCode *UpdateCode;
//---------------------------------------------------------------------------
#endif

