//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("Utils\DocAuthorCodes\MainUnit.cpp", MainForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
 try
 {
   Application->Initialize();
   Application->MainFormOnTaskBar = true;
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->Run();
 }
 catch (System::Sysutils::Exception &exception)
 {
   Application->ShowException(&exception);
 }
 catch (...)
 {
   try
   {
    throw System::Sysutils::Exception("");
   }
   catch (System::Sysutils::Exception &exception)
   {
    Application->ShowException(&exception);
   }
 }
 return 0;
}
//---------------------------------------------------------------------------

