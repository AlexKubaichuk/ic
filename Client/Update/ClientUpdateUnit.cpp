﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "ClientUpdateUnit.h"
#include "ExtUtils.h"
#include "XMLContainer.h"
#include "System.zip.hpp"
#include "msgdef.h"
#include "UpdateUnit.h"
#include "pgmsetting.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "AppOptBaseProv"
#pragma link "AppOptDialog"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "dxGDIPlusClasses"
#pragma link "AppOptBaseProv"
#pragma link "AppOptDialog"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "dxGDIPlusClasses"
#pragma resource "*.dfm"
TClientUpdateForm * ClientUpdateForm;
// ---------------------------------------------------------------------------
__fastcall TClientUpdateForm::TClientUpdateForm(TComponent * Owner) : TForm(Owner)
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TClientUpdateForm::CheckUpdate(bool AUpdateApp)
 {
  bool Connected = false;
  Show();
  Application->ProcessMessages();
  TUpdateDM * FUpd = new TUpdateDM(this);
  try
   {
    UnicodeString FDataPath = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\" + PRODFOLDER);
    UnicodeString FPath = icsPrePath(ExtractFilePath(ParamStr(0)));
    UnicodeString FXMLPath = icsPrePath(FDataPath + "\\xml");
    UnicodeString FFN = icsPrePath(FXMLPath + "\\ic_opt.xml");
    if (!FileExists(FFN))
     {
      ForceDirectories(FXMLPath);
      TTagNode * tmp = new TTagNode;
      try
       {
        tmp->Encoding = "utf-8";
        tmp->AsXML    =
          "<g><i n='0001' v='localhost'/><i n='0002' v='5100'/><i n='0003' v='localhost'/><i n='0004' v='5100'/><i n='0005' v='localhost'/><i n='0006' v='5100'/><i n='0016' v=''/><i n='0014' v=''/><i n='0015' v=''/></g>";
        tmp->SaveToXMLFile(FFN, "");
       }
      __finally
       {
        delete tmp;
       }
     }
    ConnectOptXMLData->FileName = FFN;
    ConnectOpt->LoadXML();
    do
     {
      FUpd->UpdateURL = ConnectOpt->Vals["ic_server"].AsStringDef("localhost") + ":" +
        ConnectOpt->Vals["ic_port"].AsStringDef("5100");
      try
       {
        if (!(ForceDirectories(icsPrePath(FDataPath + "\\xml")) && ForceDirectories(icsPrePath(FDataPath + "\\img"))
          && ForceDirectories(icsPrePath(FDataPath + "\\css"))))
         {
          _MSG_ERR(
            "Ошибка создания служебных папок \"xml\", \"img\", \"css\".\nВозможно отсутствуют необходимые права доступа.",
            "Ошибка");
         }
        else
         {
          Msg("Проверка наличия новых версий...");
          if (FileExists(icsPrePath(FDataPath + "\\xml\\ver.xml")))
           CopyFile(icsPrePath(FDataPath + "\\xml\\ver.xml").c_str(),
            icsPrePath(FDataPath + "\\xml\\ver.old.xml").c_str(), false);
          if (FUpd->Load("/client_src/xml/ver.xml", icsPrePath(FDataPath + "\\xml\\ver.xml")))
           {
            TTagNode * FVerNode = new TTagNode;
            TTagNode * FOldVerNode = new TTagNode;
            FOldVerNode->Name = "ver";
            try
             {
              FVerNode->LoadFromXMLFile(icsPrePath(FDataPath + "\\xml\\ver.xml"));
              if (VersionCmp(FVerNode->AV["main"], icsGetFileVersion(icsPrePath(FPath + "\\ic_client.exe"))) > 0)
               { // на сервере свежая версия клиента
                if (AUpdateApp)
                 {
                  Msg("Обнаружена новая версия клиента, загрузка...");
                  if (FUpd->Load("/client_src/ic_client.exe.zip", icsPrePath(FDataPath + "\\ic_client.exe.zip")))
                   {
                    // Клиент загружен, распаковка... --------------------------------
                    Msg("Клиент загружен, распаковка...");
                    TZipFile * FZF = new TZipFile;
                    try
                     {
                      try
                       {
                        DeleteFile(icsPrePath(FPath + "\\ic_client.exe.old"));
                        RenameFile(icsPrePath(FPath + "\\ic_client.exe"), icsPrePath(FPath + "\\ic_client.exe.old"));
                        Msg("Сохранили предыдущую версию клиента (\"ic_client.exe.old\")...");
                        FZF->Open(icsPrePath(FDataPath + "\\ic_client.exe.zip"), zmRead);
                        FZF->Extract("ic_client.exe", FPath);
                        Msg("Распаковали...");
                       }
                      catch (System::Sysutils::Exception & E)
                       {
                        _MSG_ERR("Ошибка обновления клиента.\n\nСистемное сообщение:\n" + E.Message, "Ошибка");
                       }
                      catch (...)
                       {
                        _MSG_ERR("Ошибка обновления клиента.", "Ошибка");
                       }
                     }
                    __finally
                     {
                      delete FZF;
                     }
                   }
                 }
                else
                 {
                  if (ExtractFileName(ParamStr(0)).LowerCase() == "ic_client.exe")
                   ShowMessage("Обнаружена новая версия клиента, запустите лончер для загрузки обновления.");
                 }
               }
              if (FileExists(icsPrePath(FDataPath + "\\xml\\ver.old.xml")))
               FOldVerNode->LoadFromXMLFile(icsPrePath(FDataPath + "\\xml\\ver.old.xml"));
              if (VersionCmp(FVerNode->AV["img"], FOldVerNode->AV["img"]) > 0)
               { // Загрузка ресурсов... ------------------------------------------
                Msg("Загрузка ресурсов...");
                if (FUpd->Load("/client_src/img.zip", icsPrePath(FDataPath + "\\img\\img.zip")))
                 {
                  Msg("Ресурсы загружены, распаковка...");
                  TZipFile * FZF = new TZipFile;
                  try
                   {
                    try
                     {
                      try
                       {
                        FZF->Open(icsPrePath(FDataPath + "\\img\\img.zip"), zmRead);
                        FZF->ExtractAll(FDataPath);
                        Msg("Распаковали ресурсы...");
                       }
                      catch (System::Sysutils::Exception & E)
                       {
                        _MSG_ERR(E.Message, "Ошибка");
                       }
                     }
                    catch (...)
                     {
                     }
                   }
                  __finally
                   {
                    delete FZF;
                   }
                 }
               }
              if (VersionCmp(FVerNode->AV["cliopt"], FOldVerNode->AV["cliopt"]) > 0)
               { // Загрузка описания настроек клиента ... ------------------------------------------
                Msg("Загрузка доп. описаний...");
                if (!FUpd->Load("/client_src/xml/ICS_CLI_OPT.zxml", icsPrePath(FDataPath + "\\xml\\ICS_CLI_OPT.zxml")))
                 _MSG_ERR("Ошибка загрузки описания настроек клиента.", "Ошибка");
               }
              if (VersionCmp(FVerNode->AV["appopt"], FOldVerNode->AV["appopt"]) > 0)
               { // Загрузка описаний настроек программы ... ------------------------------------------
                Msg("Загрузка доп. описаний...");
                if (!FUpd->Load("/client_src/xml/ICS_APP_OPT.zxml", icsPrePath(FDataPath + "\\xml\\ICS_APP_OPT.zxml")))
                 _MSG_ERR("Ошибка загрузки описания настроек программы.", "Ошибка");
               }
              // Загрузка hand_edit_style.css
              FUpd->Load("/css/hand_edit_style.css", icsPrePath(FDataPath + "\\css\\hand_edit_style.css"));
              Msg("Загрузка программы предварительного просмотра...");
              if (FUpd->Load("/client_src/DocPreview.exe.zip", icsPrePath(FDataPath + "\\DocPreview.exe.zip")))
               {
                Msg("Ресурсы загружены, распаковка...");
                TZipFile * FZF = new TZipFile;
                try
                 {
                  try
                   {
                    try
                     {
                      DeleteFile(icsPrePath(FPath + "\\DocPreview.exe.old"));
                      RenameFile(icsPrePath(FPath + "\\DocPreview.exe"), icsPrePath(FPath + "\\DocPreview.exe.old"));
                      Msg("Сохранили предыдущую версию программы просмотрв документов (\"DocPreview.exe.old\")...");
                      FZF->Open(icsPrePath(FDataPath + "\\DocPreview.exe.zip"), zmRead);
                      FZF->Extract("DocPreview.exe", FPath);
                      Msg("Распаковали...");
                     }
                    catch (System::Sysutils::Exception & E)
                     {
                      _MSG_ERR(E.Message, "Ошибка");
                     }
                   }
                  catch (...)
                   {
                   }
                 }
                __finally
                 {
                  delete FZF;
                 }
               }
             }
            __finally
             {
              delete FVerNode;
              delete FOldVerNode;
             }
           }
          else
           _MSG_ERR("Ошибка проверки наличия обновлений.", "Ошибка");
         }
        Connected = true;
        if (AUpdateApp && FileExists(icsPrePath(FPath + "\\ic_client.exe")))
         {
          Msg("Запускаем...");
          try
           {
            STARTUPINFO si;
            PROCESS_INFORMATION pi;
            ZeroMemory(& si, sizeof(si));
            si.cb = sizeof(si);
            ZeroMemory(& pi, sizeof(pi));
            // Start the child process.
            if (!CreateProcess(NULL, // No module name (use command line)
              icsPrePath(FPath + "\\ic_client.exe").c_str(), // Command line
              NULL, // Process handle not inheritable
              NULL, // Thread handle not inheritable
              false, // Set handle inheritance to FALSE
              NORMAL_PRIORITY_CLASS, // No creation flags
              NULL, // Use parent's environment block
              NULL, // Use parent's starting directory
              &si, // Pointer to STARTUPINFO structure
              &pi) // Pointer to PROCESS_INFORMATION structure
              )
             {
              _MSG_ERR(L"Ошибка создания процесса добавления файла в архив.\nСистемное сообщение:\n" +
                GetAPILastErrorFormatMessage() + L".", L"Ошибка");
             }
           }
          catch (System::Sysutils::Exception & E)
           {
            _MSG_ERR(E.Message, "Ошибка 21");
           }
         }
       }
      catch (System::Sysutils::Exception & E)
       {
        _MSG_ERR(E.Message, "Ошибка 22");
        Connected = false;
       }
      if (!Connected)
       Connected = !ConnectOptDlg->Execute();
     }
    while (!Connected);
   }
  __finally
   {
    delete FUpd;
    Close();
   }
  return Connected;
 }
// ---------------------------------------------------------------------------
void __fastcall TClientUpdateForm::Msg(UnicodeString AMsg)
 {
  MsgLab->Caption = AMsg;
  Application->ProcessMessages();
  Sleep(50);
 }
// ---------------------------------------------------------------------------
void __fastcall TClientUpdateForm::ConnectOptLoadXML(TTagNode * AOptNode)
 {
  try
   {
    AOptNode->Encoding = "utf-8";
    AOptNode->AsXML    =
      "<AppOptions>" " <passport title='Настройки соединения' GUI='ICS_CON_OPT' autor='KAB' version='1' release='0' timestamp='20161021000000'/>"
      " <content>" "  <group name='grCommon' uid='0000' caption='Общее'>"
      "   <group name='grICSrv' uid='0011' caption='&quot;УИ&quot;'>" "    <text name='ic_server' uid='0001' default='localhost' caption='Сервер'/>"
      "    <digit name='ic_port' uid='0002' default='5100' caption='Порт' min='0' max='99999'/>" "   </group>"
      "   <group name='grICDocSrv' uid='0012' caption='&quot;УИ-Документооборот&quot;'>" "    <text name='ic_doc_server' uid='0003' default='localhost' caption='Сервер'/>"
      "    <digit name='ic_doc_port' uid='0004' default='5100' caption='Порт' min='0' max='99999'/>" "   </group>"
      "   <group name='grICSVSrv' uid='0013' caption='&quot;УИ-Склад МИБП&quot;'>" "    <text name='ic_sv_server' uid='0005' default='localhost' caption='Сервер'/>"
      "    <digit name='ic_sv_port' uid='0006' default='5100' caption='Порт' min='0' max='99999'/>" "   </group>"
      "  </group>" "  <servicegroup name='ExtVars' uid='000F'>" "   <text name='l' uid='0014' caption='l'/>"
      "   <text name='p' uid='0015' caption='p'/>" "   <binary name='sp' uid='0016' caption='sp'/>" "  </servicegroup>"
      " </content>" "</AppOptions>";
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
