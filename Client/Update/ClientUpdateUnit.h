﻿//---------------------------------------------------------------------------

#ifndef ClientUpdateUnitH
#define ClientUpdateUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "AppOptBaseProv.h"
#include "AppOptDialog.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "dxGDIPlusClasses.hpp"
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TClientUpdateForm : public TForm
{
__published:
 TLabel *MsgLab;
 TAppOptDialog *ConnectOptDlg;
 TAppOptions *ConnectOpt;
 TAppOptXMLProv *ConnectOptXMLData;
 TImage *Image1;
 TLabel *Label1;
 void __fastcall ConnectOptLoadXML(TTagNode *AOptNode);
private:
  void __fastcall Msg(UnicodeString AMsg);
public:
  __fastcall TClientUpdateForm(TComponent* Owner);

  bool __fastcall CheckUpdate(bool AUpdateApp);
};
//---------------------------------------------------------------------------
extern PACKAGE TClientUpdateForm *ClientUpdateForm;
//---------------------------------------------------------------------------
#endif
