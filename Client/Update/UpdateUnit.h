﻿//---------------------------------------------------------------------------

#ifndef UpdateUnitH
#define UpdateUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <IPPeerClient.hpp>
#include <REST.Authenticator.Basic.hpp>
#include <REST.Client.hpp>
//---------------------------------------------------------------------------
class PACKAGE TicsVerParser : public TObject
{
private:
        int  FIntVer[4];
        int  FMajor;
        int  FMinor;
        int  FRelease;
        int  FBuild;
        UnicodeString FVerSrc;
        int __fastcall FGetVerPart(int AIdx);
        int __fastcall FGetMajor();
        int __fastcall FGetMinor();
        int __fastcall FGetRelease();
        int __fastcall FGetBuild();

        UnicodeString __fastcall FGetVerStr();
public:
        __fastcall TicsVerParser(UnicodeString AVerSrc);

        __property int Major   = {read = FGetMajor};
        __property int Minor   = {read = FGetMinor};
        __property int Release = {read = FGetRelease};
        __property int Build   = {read = FGetBuild};
        __property int Part[int AIdx]   = {read = FGetVerPart};

        __property UnicodeString VerSrc = {read = FVerSrc};
        __property UnicodeString VerStr = {read = FGetVerStr};
};
//---------------------------------------------------------------------------
class TUpdateDM : public TDataModule
{
__published:
 TRESTClient *Client;
 TRESTRequest *Request;
 TRESTResponse *Response;
 THTTPBasicAuthenticator *HTTPBasicAuth;
private:
  void          __fastcall FSetURL(UnicodeString AUrl);
  UnicodeString __fastcall FGetURL();
public:
  bool __fastcall Load(UnicodeString ASrc, UnicodeString ADest);
 __fastcall TUpdateDM(TComponent* Owner);
 __property UnicodeString UpdateURL = {read = FGetURL, write = FSetURL};
};
//---------------------------------------------------------------------------
extern PACKAGE TUpdateDM *UpdateDM;
extern PACKAGE int  __fastcall VersionCmp(UnicodeString AVersion1, UnicodeString AVersion2);
extern PACKAGE UnicodeString __fastcall GetAPILastErrorFormatMessage();
//---------------------------------------------------------------------------
#endif
