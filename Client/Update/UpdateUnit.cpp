﻿//---------------------------------------------------------------------------


#pragma hdrstop

#include "UpdateUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TUpdateDM *UpdateDM;
//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                             TicsVerParser                             ##
//##                                                                       ##
//###########################################################################
__fastcall TicsVerParser::TicsVerParser(UnicodeString AVerSrc)
{
  FIntVer[0] = FIntVer[1] = FIntVer[2] = FIntVer[3] = 0;
  if (AVerSrc.Length())
  {
    FVerSrc = AVerSrc;
    wchar_t *szFileVersion = new wchar_t[AVerSrc.Length() + 1];
    szFileVersion[0] = '\0';
    wcscpy(szFileVersion, AVerSrc.c_str());
    int FVerPart = 0;
    wchar_t *p = wcstok(szFileVersion, L".,");
    while (p)
    {
      FIntVer[FVerPart] = _wtoi(p);
      p = wcstok( NULL, L".," );
      FVerPart++;
    }
    delete [] szFileVersion;
  }
}
//---------------------------------------------------------------------------
int __fastcall TicsVerParser::FGetVerPart(int AIdx)
{
  if ((AIdx>=0)&&(AIdx<4))
   return FIntVer[AIdx];
  else
   return 0;
}
//---------------------------------------------------------------------------
int __fastcall TicsVerParser::FGetMajor()
{
  return FIntVer[0];
}
//---------------------------------------------------------------------------
int __fastcall TicsVerParser::FGetMinor()
{
  return FIntVer[1];
}
//---------------------------------------------------------------------------
int __fastcall TicsVerParser::FGetRelease()
{
  return FIntVer[2];
}
//---------------------------------------------------------------------------
int __fastcall TicsVerParser::FGetBuild()
{
  return FIntVer[3];
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TicsVerParser::FGetVerStr()
{
  UnicodeString RC = "";
  try
   {
     RC = IntToStr(FMajor)+"."+IntToStr(FMinor)+"."+IntToStr(FRelease)+"."+IntToStr(FBuild);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
__fastcall TUpdateDM::TUpdateDM(TComponent* Owner)
 : TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
bool __fastcall TUpdateDM::Load(UnicodeString ASrc, UnicodeString ADest)
{
  bool RC = false;
  TFileStream *FFS = new TFileStream(ADest, fmCreate);
  try
   {
     Request->Resource = ASrc;
     Request->Execute();
     FFS->Write(Response->RawBytes, Response->RawBytes.Length);
     RC = true;
   }
  __finally
   {
     delete FFS;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TUpdateDM::FSetURL(UnicodeString AUrl)
{
  Client->BaseURL = AUrl;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TUpdateDM::FGetURL()
{
  return Client->BaseURL;
}
//---------------------------------------------------------------------------
int __fastcall VersionCmp(UnicodeString AVersion1, UnicodeString AVersion2)
{
  // < 0  => AVersion1 < AVersion2
  // = 0  => AVersion1 == AVersion2
  // > 0  => AVersion1 > AVersion2
  int RC = 0;
  if (!AVersion1.Trim().Length() || !AVersion2.Trim().Length())
   RC = 1;
  else
   {
     TicsVerParser *FVer1 = new TicsVerParser(AVersion1);
     TicsVerParser *FVer2 = new TicsVerParser(AVersion2);
     try
      {
        for (int i = 0; i < 4; i++)
         {
           if (FVer1->Part[i] > FVer2->Part[i])
            {
              RC = 1; break;
            }
           else if (FVer1->Part[i] < FVer2->Part[i])
            {
              RC = -1; break;
            }
         }
      }
     __finally
      {
        delete FVer1;
        delete FVer2;
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetAPILastErrorFormatMessage()
{
  UnicodeString ErrMsg;
  LPVOID lpMsgBuf;
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, GetLastError(),
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
    (LPTSTR)& lpMsgBuf, 0, NULL);
  ErrMsg = static_cast<LPCTSTR>(lpMsgBuf);
  LocalFree(lpMsgBuf);
  return ErrMsg;
}
//---------------------------------------------------------------------------

