// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
// ---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("main_conv_check\LoginUnit.cpp", LoginForm);
USEFORM("main_conv_check\MainUnit.cpp", MainForm);
USEFORM("main_conv_check\SetDefAddr.cpp", DefAddrForm);
USEFORM("main_conv_check\DBSelect.cpp", DBSelectUnit);
USEFORM("main_conv_check\dsConnectUnit.cpp", ConnectDM); /* TDataModule: File Type */
USEFORM("main_conv_check\DMUnit.cpp", DM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    // Application->MainFormOnTaskBar = true;
    Application->Title = "�������� �������";
   Application->CreateForm(__classid(TDM), &DM);
   Application->CreateForm(__classid(TMainForm), &MainForm);
   Application->Run();
   }
  catch (Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw Exception("");
     }
    catch (Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
// ---------------------------------------------------------------------------
